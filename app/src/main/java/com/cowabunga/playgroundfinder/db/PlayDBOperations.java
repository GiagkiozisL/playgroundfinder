package com.cowabunga.playgroundfinder.db;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.utils.JsonParser;

import java.util.Arrays;


public class PlayDBOperations {

    public static void insertPlaygroundToDB(Context context, Playground playground) {

        ContentValues values = new ContentValues();
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_ID, playground.id);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_NAME, playground.name);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_ADDRESS, playground.address);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_LATITUDE, playground.latitude);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_LONGITUDE, playground.longitude);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_COUNTRY, playground.country);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_CITY, playground.city);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_STATE, playground.state);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_FORM_ADDRESS, playground.formattedAddress);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_CONTACT, playground.contact);
        values.put(PlaygroundTable.COLUMN_PLAYGROUND_CATEGORIES, JsonParser.categoriesToJson(playground.categories));

        Uri uri = context.getContentResolver().insert(PlayContentProvider.CONTENT_URI_PLAYGROUND, values);
        if (uri != null) {
            Log.i("PlayDBOperations", "employee inserted");
        }
    }

    public static void deletePlaygroundFromDB(Context context, String id) {

        String where = PlaygroundTable.COLUMN_PLAYGROUND_ID + "=?";
        String[] selectionArgs = {id};

        context.getContentResolver().delete(PlayContentProvider.CONTENT_URI_PLAYGROUND, where, selectionArgs);
        Log.d("PlayDBOperations", "deleted playground from db");
    }


}
