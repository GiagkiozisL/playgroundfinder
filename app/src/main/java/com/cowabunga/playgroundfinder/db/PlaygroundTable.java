package com.cowabunga.playgroundfinder.db;

import android.database.sqlite.SQLiteDatabase;

public class PlaygroundTable {

    // db Table
    public static final String TABLE_PLAYGROUND = "playgroundTable";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PLAYGROUND_ID = "playgroundId";
    public static final String COLUMN_PLAYGROUND_NAME = "name";
    public static final String COLUMN_PLAYGROUND_ADDRESS = "address";
    public static final String COLUMN_PLAYGROUND_LATITUDE = "latitude";
    public static final String COLUMN_PLAYGROUND_LONGITUDE = "longitude";
    public static final String COLUMN_PLAYGROUND_COUNTRY = "country";
    public static final String COLUMN_PLAYGROUND_CITY = "city";
    public static final String COLUMN_PLAYGROUND_STATE = "state";
    public static final String COLUMN_PLAYGROUND_FORM_ADDRESS = "formattedAddress";
    public static final String COLUMN_PLAYGROUND_CONTACT = "contact";
    public static final String COLUMN_PLAYGROUND_CATEGORIES = "categories";


    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_PLAYGROUND
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_PLAYGROUND_NAME + " text, "
            + COLUMN_PLAYGROUND_ID + " text, "
            + COLUMN_PLAYGROUND_ADDRESS + " text, "
            + COLUMN_PLAYGROUND_LATITUDE + " real, "
            + COLUMN_PLAYGROUND_LONGITUDE + " real, "
            + COLUMN_PLAYGROUND_FORM_ADDRESS + " integer, "
            + COLUMN_PLAYGROUND_CONTACT + " text, "
            + COLUMN_PLAYGROUND_COUNTRY + " text, "
            + COLUMN_PLAYGROUND_CATEGORIES + " text, "
            + COLUMN_PLAYGROUND_CITY + " text, "
            + COLUMN_PLAYGROUND_STATE + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
}
