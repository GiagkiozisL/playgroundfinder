package com.cowabunga.playgroundfinder.db;

import android.content.Context;
import android.database.Cursor;

import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.utils.JsonParser;


public class DBQueries {

    public static Playground parsePLaygroundFromCursor(Cursor cursor) {
        Playground playground = new Playground();

        playground.id = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_ID));
        playground.name = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_NAME));
        playground.address = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_ADDRESS));
        playground.latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_LATITUDE)));
        playground.longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_LONGITUDE)));
        playground.address = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_FORM_ADDRESS));
        playground.contact = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_CONTACT));
        playground.country = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_COUNTRY));
        String categories = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_CATEGORIES));

        try {
            playground.categories = JsonParser.categoriesFromJson(categories);
        } catch (Exception e) {
            e.printStackTrace();
        }

        playground.city = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_CITY));
        playground.state = cursor.getString(cursor.getColumnIndexOrThrow(PlaygroundTable.COLUMN_PLAYGROUND_STATE));

        return playground;
    }

    public static boolean isPLayIdFavorite(Context context, String id) {

        boolean isFavorite = false;

        String selection = PlaygroundTable.COLUMN_PLAYGROUND_ID + " =?";
        String[] selectionArgs = {id};

        Cursor cursor = context.getContentResolver().query(
                PlayContentProvider.CONTENT_URI_PLAYGROUND,
                PlayContentProvider.PLAYGROUND_PROJECTION,
                selection,
                selectionArgs,
                null);

        if (cursor != null && cursor.getCount() > 0) {
            isFavorite = true;
            cursor.close();
        }

        return isFavorite;

    }
}
