package com.cowabunga.playgroundfinder.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.Arrays;
import java.util.HashSet;


public class PlayContentProvider extends ContentProvider {

    //    database
    private ManDBHelper mDatabase;

    //    used for the Uri matcher
    private static final int PLAYGROUND = 10;

    public static final String AUTHORITY = "com.cowabunga.playgroundfinder.db.PlayContentProvider";

    private static final String BASE_PATH_PLAYGROUND = PlaygroundTable.TABLE_PLAYGROUND;

    public static final Uri CONTENT_URI_PLAYGROUND = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_PLAYGROUND);

    public static final String[] PLAYGROUND_PROJECTION = {
            PlaygroundTable.COLUMN_ID,
            PlaygroundTable.COLUMN_PLAYGROUND_NAME,
            PlaygroundTable.COLUMN_PLAYGROUND_ID,
            PlaygroundTable.COLUMN_PLAYGROUND_ADDRESS,
            PlaygroundTable.COLUMN_PLAYGROUND_LATITUDE,
            PlaygroundTable.COLUMN_PLAYGROUND_LONGITUDE,
            PlaygroundTable.COLUMN_PLAYGROUND_COUNTRY,
            PlaygroundTable.COLUMN_PLAYGROUND_CITY,
            PlaygroundTable.COLUMN_PLAYGROUND_STATE,
            PlaygroundTable.COLUMN_PLAYGROUND_FORM_ADDRESS,
            PlaygroundTable.COLUMN_PLAYGROUND_CONTACT,
            PlaygroundTable.COLUMN_PLAYGROUND_CATEGORIES
    };

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PLAYGROUND, PLAYGROUND);
    }

    @Override
    public boolean onCreate() {
        mDatabase = new ManDBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setDistinct(true);

        checkColumns(uri, projection);

        // Set the table
        if (uri.compareTo(CONTENT_URI_PLAYGROUND) == 0) {
            queryBuilder.setTables(PlaygroundTable.TABLE_PLAYGROUND);
        }

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case PLAYGROUND:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mDatabase.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
        long id;
        String basePath;
        switch (uriType) {
            case PLAYGROUND:
                id = sqlDB.insertWithOnConflict(PlaygroundTable.TABLE_PLAYGROUND, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                basePath = BASE_PATH_PLAYGROUND;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        System.out.println("row inserted " + id);
        return Uri.parse(basePath + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
        int rowsDeleted;
        String id;
        switch (uriType) {
            case PLAYGROUND:
                rowsDeleted = sqlDB.delete(PlaygroundTable.TABLE_PLAYGROUND, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
        int rowsUpdated;
        String id = null;
        switch (uriType) {
            case PLAYGROUND:
                rowsUpdated = sqlDB.update(PlaygroundTable.TABLE_PLAYGROUND,
                        values,
                        selection,
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        System.out.println("row updated " + uri);
        return rowsUpdated;
    }

    private void checkColumns(Uri uri, String[] projection) {

        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<>(Arrays.asList(projection));
            HashSet<String> availableColumns = createAvailableColumnsHash(uri);
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }

    private HashSet<String> createAvailableColumnsHash(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case PLAYGROUND:
                return new HashSet<>(Arrays.asList(PLAYGROUND_PROJECTION));
            default:
                return null;
        }
    }
}
