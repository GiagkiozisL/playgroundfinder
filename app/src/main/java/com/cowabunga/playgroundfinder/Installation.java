package com.cowabunga.playgroundfinder;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.cowabunga.playgroundfinder.application.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

public class Installation {

    private static final String INSTALLATION = "AF_INSTALLATION";

    private static String sID = Constants.uid;

    public static String id(Context paramContext) { // Byte code:
        //   0: ldc com/appsflyer/Installation
        //   2: monitorenter
        //   3: getstatic com/appsflyer/Installation.sID : Ljava/lang/String;
        //   6: ifnonnull -> 42
        //   9: new java/io/File
        //   12: dup
        //   13: aload_0
        //   14: invokevirtual getFilesDir : ()Ljava/io/File;
        //   17: ldc 'AF_INSTALLATION'
        //   19: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
        //   22: astore_2
        //   23: aload_2
        //   24: invokevirtual exists : ()Z
        //   27: ifne -> 35
        //   30: aload_2
        //   31: aload_0
        //   32: invokestatic writeInstallationFile : (Ljava/io/File;Landroid/content/Context;)V
        //   35: aload_2
        //   36: invokestatic readInstallationFile : (Ljava/io/File;)Ljava/lang/String;
        //   39: putstatic com/appsflyer/Installation.sID : Ljava/lang/String;
        //   42: getstatic com/appsflyer/Installation.sID : Ljava/lang/String;
        //   45: astore #4
        //   47: ldc com/appsflyer/Installation
        //   49: monitorexit
        //   50: aload #4
        //   52: areturn
        //   53: astore_3
        //   54: new java/lang/RuntimeException
        //   57: dup
        //   58: aload_3
        //   59: invokespecial <init> : (Ljava/lang/Throwable;)V
        //   62: athrow
        //   63: astore_1
        //   64: ldc com/appsflyer/Installation
        //   66: monitorexit
        //   67: aload_1
        //   68: athrow
        // Exception table:
        //   from	to	target	type
        //   3	23	63	finally
        //   23	35	53	java/lang/Exception
        //   23	35	63	finally
        //   35	42	53	java/lang/Exception
        //   35	42	63	finally
        //   42	47	63	finally
        //   54	63	63	finally }
        return sID;
    }
        private static String readInstallationFile(File paramFile) throws IOException {
            RandomAccessFile randomAccessFile = new RandomAccessFile(paramFile, "r");
            byte[] arrayOfByte = new byte[(int)randomAccessFile.length()];
            randomAccessFile.readFully(arrayOfByte);
            randomAccessFile.close();
            return new String(arrayOfByte);
        }

        private static void writeInstallationFile(File paramFile, Context paramContext) throws IOException, PackageManager.NameNotFoundException {
            FileOutputStream fileOutputStream = new FileOutputStream(paramFile);
            PackageInfo packageInfo = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
            fileOutputStream.write((packageInfo.firstInstallTime + "-" + Math.abs((new Random()).nextLong())).getBytes());
            fileOutputStream.close();
        }
}
