package com.cowabunga.playgroundfinder;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class AnimationFrameLayout extends FrameLayout {

    private int frameSize;
    private Drawable backDrawable;
    private int innerPadding;
    private int type = 0;
    private boolean autoStart = true;

    private int[] animations = new int[]{R.drawable.animation_list_avocado}; //, R.drawable.animation_list_corn, R.drawable.animation_list_lemon, R.drawable.animation_list_strawberry};

    public AnimationFrameLayout(@NonNull Context context) {
        super(context);
        startLoading(context);
    }

    public AnimationFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AnimationFrameLayout,0,0);
        try {
            frameSize = typedArray.getInt(R.styleable.AnimationFrameLayout_anim_frame_size, 230);
            backDrawable = typedArray.getDrawable(R.styleable.AnimationFrameLayout_anim_background_drawable);
            innerPadding = typedArray.getColor(R.styleable.AnimationFrameLayout_anim_inner_padding, 30);
            type = typedArray.getInt(R.styleable.AnimationFrameLayout_anim_type, 0);
            autoStart = typedArray.getBoolean(R.styleable.AnimationFrameLayout_auto_start, true);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            typedArray.recycle();
        }

        if (autoStart) {
            startLoading(context);
        }
    }

    public AnimationFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        startLoading(context);
    }

    public void startLoading(Context ctx) {

        int drawableResource = getAnimationDrawable(type);

        ImageView imageView = new ImageView(ctx);
        int imageSide = frameSize - innerPadding;
        LayoutParams imgParams = new LayoutParams(imageSide, imageSide);
        imgParams.gravity = Gravity.CENTER;
        imageView.setLayoutParams(imgParams);
        imageView.setAlpha(0.8F);

        imageView.setBackgroundResource(drawableResource);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();

        ImageView backImg = new ImageView(ctx);
        backImg.setImageDrawable(backDrawable);
        LayoutParams backParams = new LayoutParams(frameSize, frameSize);
        backParams.gravity = Gravity.CENTER;
        backImg.setLayoutParams(backParams);

        addView(backImg);
        addView(imageView);
    }

    public void stopLoading() {
        removeAllViews();
    }

    public void initView(Context ctx, int frameSize, int innerPadding, int type, Drawable backDrawable) {
        int drawableResource = getAnimationDrawable(type);

        ImageView imageView = new ImageView(ctx);
        int imageSide = frameSize - innerPadding;
        LayoutParams imgParams = new LayoutParams(imageSide, imageSide);
        imgParams.gravity = Gravity.CENTER;
        imageView.setLayoutParams(imgParams);
        imageView.setAlpha(0.4F);

        imageView.setBackgroundResource(drawableResource);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();

        ImageView backImg = new ImageView(ctx);
        backImg.setImageDrawable(backDrawable);
        LayoutParams backParams = new LayoutParams(frameSize, frameSize);
        backParams.gravity = Gravity.CENTER;
        backImg.setLayoutParams(backParams);

        addView(backImg);
        addView(imageView);

    }

    private int getAnimationDrawable(int type) {
        switch (type) {
            case 0:
                // ball
                return animations[0];
            case 1:
                // trambala
                return animations[0];
            default:
                return animations[0];
        }
    }

}
