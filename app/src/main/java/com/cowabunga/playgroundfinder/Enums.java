package com.cowabunga.playgroundfinder;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public class Enums {

    // region //// Font slyle /////
    public static final int REGULAR = 0;
    public static final int ITALIC = 1;
    public static final int BOLD = 2;
    public static final int BOLD_ITALIC = 3;

    @IntDef({ITALIC, REGULAR, BOLD, BOLD_ITALIC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FontStyle {}
    // endregion //// Font style /////

    // region //// font type ////
    public static final String montserrat = "montserrat";
    public static final String ROBOTO = "roboto";
    public static final String NOTO_SANS = "noto_sans";


    @StringDef({montserrat, ROBOTO, NOTO_SANS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FontType {}

    // endregion //// font type ////
}
