package com.cowabunga.playgroundfinder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Playground implements Serializable {

    public String id = "";
    public String name = "";
    public String address = "";
    public double latitude = 0.0;
    public double longitude = 0.0;
    public String postalCode = "";
    public String country = "";
    public String city = "";
    public String state = "";
    public String formattedAddress = "";
    public String contact = "";
    public double distance = 0.0; // in kilometers
    public List<String> categories = new ArrayList<>();

    public Playground() {}
}
