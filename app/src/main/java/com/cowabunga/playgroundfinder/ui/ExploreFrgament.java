package com.cowabunga.playgroundfinder.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cowabunga.playgroundfinder.AnimationFrameLayout;
import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.R;
import com.cowabunga.playgroundfinder.application.Constants;
import com.cowabunga.playgroundfinder.application.PLayGroundApplication;
import com.cowabunga.playgroundfinder.application.PlayPreferences;
import com.cowabunga.playgroundfinder.server.RequestManager;
import com.cowabunga.playgroundfinder.utils.JsonParser;
import com.cowabunga.playgroundfinder.utils.Utils;
import com.cowabunga.playgroundfinder.view.CustomTextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ExploreFrgament extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener, OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener {


    private static final String TAG = "ExploreFrgament";
    public static final String fragment_tag = "ExploreFrgament";
    private static final int DEFAULT_ZOOM_LEVEL = 10;

    private RequestManager requestManager;
    private Context ctx;
    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private LatLng mCurrentPosition;
    private Location currentLocation;

    private View infoWindowView;
    private CustomTextView infoWindowTitle;
    private CustomTextView infoWindowAddress;
    private CustomTextView infoWindowType;
    private ImageView infoWindowImg;

    private List<Playground> playgroundList;
    private AnimationFrameLayout loadingProgressBar;

    private SupportMapFragment mapFragment;
    private PlayPreferences preferences;
    private PLayGroundApplication application;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestManager = RequestManager.getInstance(ctx);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        loadingProgressBar = view.findViewById(R.id.map_progress_bar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        currentLocation = PLayGroundApplication.getInstance().getUserLocation();

        if (playgroundList == null) {
            playgroundList = new ArrayList<>();
            initGoogleMap();

            setupInfoWindowView();
            if (playgroundList != null && playgroundList.size() > 0) {
                fillInSpots();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        ctx = context;
        preferences = PlayPreferences.getInstance(ctx);
    }

    private void fillInSpots() {
        spreadUnitsOnMap();
    }

    private void initGoogleMap() {

        try {
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx) == ConnectionResult.SERVICE_MISSING) {
                Toast.makeText(ctx, R.string.google_services_missing_message, Toast.LENGTH_LONG).show();
                return;
            }

            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserLocation() {

        try {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            mLocationRequest.setInterval(1500);
            mLocationRequest.setFastestInterval(1000);
            mLocationRequest.setNumUpdates(3);

            mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMapOnCurrentLocation(Location location) {

        try {
            if (location != null && mMap != null) {
                mCurrentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                setupMap(mMap);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(mCurrentPosition));

                if (mMap != null && mCurrentPosition != null) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mCurrentPosition, (mMap.getCameraPosition().zoom - 0.02f)));
                }
                mLocationRequest = null;

                fetchPlaygrounds();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void setupMap(GoogleMap map) {
        if (mMap != null) {
            mMap.clear();
        }

        mMap = map;
        mMap.moveCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        mMap.setMyLocationEnabled(true);
    }

    private void spreadUnitsOnMap() {

        if (mMap == null)
            return;

        for (Playground unit : playgroundList) {

            try {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(unit.latitude, unit.longitude)))
                        .setTag(unit);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "found null lat - lon in " + unit.name + " item");
            }
        }

        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);
    }

    private void showProgressAniamtion() {
        loadingProgressBar.startLoading(ctx);
        loadingProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressAnimation() {
        loadingProgressBar.setVisibility(View.INVISIBLE);
        loadingProgressBar.stopLoading();
    }

    private void setupInfoWindowView() {
        infoWindowView = getLayoutInflater().inflate(R.layout.custom_marker_view, null);
        infoWindowView.setBackgroundColor(ContextCompat.getColor(ctx, android.R.color.transparent));

        infoWindowTitle = infoWindowView.findViewById(R.id.custom_marker_title);
        infoWindowAddress = infoWindowView.findViewById(R.id.custom_marker_address);
        infoWindowType = infoWindowView.findViewById(R.id.custom_marker_type);
        infoWindowImg = infoWindowView.findViewById(R.id.custom_marker_image);
    }

    private void fillInfoWindowWithData(Playground playground) {

        infoWindowTitle.setText(playground.name);
        infoWindowAddress.setText(playground.address.isEmpty() ? "-" : playground.address);

        List<String> categories = playground.categories;
        Log.d(TAG, "categories: " + categories.toString());

        try {
            infoWindowType.setText(playground.categories.get(0));
        } catch (Exception e) {
            infoWindowType.setText("-");
            e.printStackTrace();
        }

        if (categories.contains("Playground") || categories.contains("Daycare")) {
            infoWindowImg.setImageResource(R.drawable.playground);
        } else if (categories.contains("Parks")) {
            infoWindowImg.setImageResource(R.drawable.park1);
        } else if (categories.contains("Plaza")) {
            infoWindowImg.setImageResource(R.drawable.plaza);
        } else if (categories.contains("Theme Park")) {
            infoWindowImg.setImageResource(R.drawable.theme_park);
        } else {
            infoWindowImg.setImageResource(R.drawable.park2);
        }
    }

    private void fetchPlaygrounds() {

        LatLng currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        LatLng lastLatLng = new LatLng(Double.valueOf(preferences.getLastFetchLat()), Double.valueOf(preferences.getLastFetchLng()));
        boolean areClose = Utils.areLocationsNearby(currentLatLng, lastLatLng);
        if (areClose) {
            // fetch from prefs
            String lastPLayData = preferences.getLastPlayResults();
            try {
                JSONObject jsonObject = new JSONObject(lastPLayData);
                parsePlayResponse(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            makeSpotsRequest(Constants.playgroundId);
        }
    }

    private void parsePlayResponse(JSONObject response) {
        JSONObject responseObj = response.optJSONObject("response");
        JSONArray responseArray = responseObj.optJSONArray("venues");

        playgroundList = new ArrayList<>();

        for (int i = 0; i < responseArray.length();i++) {

            JSONObject playObj = responseArray.optJSONObject(i);
            Playground playground = JsonParser.parsePlayground(playObj);
            playgroundList.add(playground);
        }

        spreadUnitsOnMap();
        hideProgressAnimation();
    }

    public void makeSpotsRequest(String categoryId) {
        playgroundList = new ArrayList<>();
        showProgressAniamtion();

        preferences.setLastFetchTimestamp(System.currentTimeMillis());
        preferences.setLastFetchLat(currentLocation.getLatitude());
        preferences.setLastFetchLng(currentLocation.getLongitude());
        requestManager.createSpotsRequest(categoriesSuccessListener(), categoriesErrorListener(), currentLocation, categoryId, 5000);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");

        try {
            if (mGoogleApiClient.isConnected()) {

                /*** coordinates from current location ***/
                Location cityCenter = new Location("");
                cityCenter.setLatitude(currentLocation.getLatitude());
                cityCenter.setLongitude(currentLocation.getLongitude());
                setMapOnCurrentLocation(cityCenter);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(ctx, "Parsing location failed. Please check your google play services.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        marker.showInfoWindow();
        return true;
    }

    // OnMapReadyCallback
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getUserLocation();
    }

    // Volley callbacks
    private Response.Listener<JSONObject> categoriesSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());


                preferences.setLastPlayResults(response.toString());
                parsePlayResponse(response);

            }
        };
    }

    private Response.ErrorListener categoriesErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                hideProgressAnimation();
//                storesLoadFailed(error.networkResponse.statusCode);
            }
        };
    }

    @Override
    public View getInfoWindow(Marker marker) {
        Playground playground = (Playground) marker.getTag();
        if (playground != null) {
            fillInfoWindowWithData(playground);
        }
        return infoWindowView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    // GoogleMap.OnInfoWindowClickListener
    @Override
    public void onInfoWindowClick(Marker marker) {
        // go to details
        ((MainActivity)ctx).goToStoreDetailsActivity((Playground)marker.getTag());
    }

//    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//        ImageView bmImage;
//
//        public DownloadImageTask(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//        protected Bitmap doInBackground(String... urls) {
//            String urlStr = urls[0];
//            Bitmap bitmap = null;
//            try {
//                URL url = new URL(urlStr);
//                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
////                InputStream in = new java.net.URL(urldisplay).openStream();
////                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return bitmap;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }
}
