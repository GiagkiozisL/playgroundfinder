package com.cowabunga.playgroundfinder.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.R;
import com.cowabunga.playgroundfinder.db.DBQueries;
import com.cowabunga.playgroundfinder.db.PlayDBOperations;
import com.cowabunga.playgroundfinder.view.CustomTextView;
import com.cowabunga.playgroundfinder.view.RoundedCustomButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class PlaygroundDetailActivity extends AppCompatActivity implements OnMapReadyCallback {


    private CustomTextView nameTxtView, addressTxtView, distanceTxtView, typeTxtView;
    private RoundedCustomButton favBtn;
    private ImageView typeImageView;
    private Playground selectedPlayground;
    private boolean sthChanged = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playground_details);

        nameTxtView = findViewById(R.id.playground_detail_name);
        addressTxtView = findViewById(R.id.playground_detail_address);
        distanceTxtView = findViewById(R.id.playground_detail_distance);
        typeTxtView = findViewById(R.id.playground_detail_type);
        typeImageView = findViewById(R.id.playground_detail_img_view);
        favBtn = findViewById(R.id.playground_detail_favor_btn);

        if (getIntent() != null && getIntent().hasExtra("playground")) {
            Playground playground = (Playground) getIntent().getSerializableExtra("playground");
            // play with stuff

            if (playground == null) {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                return;
            }

            selectedPlayground = playground;

            boolean isFavorite = DBQueries.isPLayIdFavorite(this, selectedPlayground.id);
            styleFavBtn(isFavorite);
            favBtn.setOnClickListener(setupFavClickListener());

            nameTxtView.setText(selectedPlayground.name);

            String cleanAddress = selectedPlayground.formattedAddress.replaceAll("[|?*<\":>+\\[\\]/']", "");
            addressTxtView.setText(cleanAddress);
            distanceTxtView.setText(String.valueOf(selectedPlayground.distance) + " meter");
            initMap();

            handleTypeIcon(playground.categories);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                if (sthChanged) {
                    Intent intent = new Intent();
                    intent.putExtra("refreshDb", sthChanged);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                    return true;
                }

                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.playground_detail_map);
        mapFragment.getMapAsync(this);
    }

    private void handleTypeIcon(List<String> categories) {

        try {
            typeTxtView.setText(categories.get(0));
        } catch (Exception e) {
            typeTxtView.setText("-");
            e.printStackTrace();
        }

        if (categories.contains("Playground") || categories.contains("Daycare")) {
            typeImageView.setImageResource(R.drawable.playground);
        } else if (categories.contains("Parks")) {
            typeImageView.setImageResource(R.drawable.park1);
        } else if (categories.contains("Plaza")) {
            typeImageView.setImageResource(R.drawable.plaza);
        } else if (categories.contains("Theme Park")) {
            typeImageView.setImageResource(R.drawable.theme_park);
        } else {
            typeImageView.setImageResource(R.drawable.park2);
        }
    }

    private void openMap(String directions) {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + directions); //46.414382,10.013988");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    private void styleFavBtn(boolean isFav) {
        if (isFav) {
            favBtn.setTag("unfavorite");
            favBtn.setText("unfavorite");
            favBtn.setDrawableLeft(R.drawable.ic_heart_white_24dp);
        } else {
            favBtn.setTag("favorite");
            favBtn.setText("favorite");
            favBtn.setDrawableLeft(R.drawable.ic_heart_outline_white_24dp);
        }
    }

    private View.OnClickListener setupFavClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag = (String) v.getTag();
                switch (tag) {
                    case "favorite":
                        styleFavBtn(true);
                        PlayDBOperations.insertPlaygroundToDB(PlaygroundDetailActivity.this, selectedPlayground);
                        break;
                    case "unfavorite":
                        styleFavBtn(false);
                        PlayDBOperations.deletePlaygroundFromDB(PlaygroundDetailActivity.this, selectedPlayground.id);
                        break;
                }
                sthChanged = !sthChanged;
            }
        };
    }

    // OnMapReadyCallback
    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng storeCoordinates = null;
        if (selectedPlayground.latitude != 0.0 && selectedPlayground.longitude != 0.0) {
            storeCoordinates = new LatLng(selectedPlayground.latitude, selectedPlayground.longitude);
        }

        MarkerOptions markerOptions;
        if (storeCoordinates != null) {
            markerOptions = new MarkerOptions().position(storeCoordinates).title(selectedPlayground.name);

            googleMap.addMarker(markerOptions).showInfoWindow();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(storeCoordinates, 15.0f));
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                openMap(selectedPlayground.latitude + "," + selectedPlayground.longitude);
            }
        });
    }
}

