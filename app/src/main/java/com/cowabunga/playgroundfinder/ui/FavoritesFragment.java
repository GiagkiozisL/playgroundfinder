package com.cowabunga.playgroundfinder.ui;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.cowabunga.playgroundfinder.R;
import com.cowabunga.playgroundfinder.adapter.FavoritesAdapter;
import com.cowabunga.playgroundfinder.db.DBQueries;
import com.cowabunga.playgroundfinder.db.PlayContentProvider;

public class FavoritesFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String fragment_tag = "FavoritesFragment";

    private Context ctx;
    private LinearLayout emptyContainer;
    private ListView listView;
    private FavoritesAdapter favoritesAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);

        emptyContainer = view.findViewById(R.id.favorite_empty_container);
        listView = view.findViewById(R.id.favorite_list_view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        favoritesAdapter = new FavoritesAdapter(ctx);
        listView.setAdapter(favoritesAdapter);
        listView.setOnItemClickListener(this);
        startCursorLoader();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        ctx = context;
    }

    public void startCursorLoader() {
        CursorLoader cursorLoader = createCursorLoader();
        cursorLoader.registerListener(0, new Loader.OnLoadCompleteListener<Cursor>() {
            @Override
            public void onLoadComplete(@NonNull Loader<Cursor> loader, @Nullable Cursor cursor) {
                // not working
                favoritesAdapter.swapCursor(cursor);
            }
        });
        Cursor cursor = cursorLoader.loadInBackground();
        if (cursor != null) {
            if (cursor.getCount() == 0) {
                emptyContainer.setVisibility(View.VISIBLE);
            } else {
                emptyContainer.setVisibility(View.GONE);
                favoritesAdapter.swapCursor(cursor);
            }
        }
    }

    private CursorLoader createCursorLoader() {
        Uri uri = PlayContentProvider.CONTENT_URI_PLAYGROUND;
        String[] projection = PlayContentProvider.PLAYGROUND_PROJECTION;
        String selection = null;
        String[] selectionArgs = null;
        String sorted = null;

        return new CursorLoader(ctx,
                uri,
                projection,
                selection,
                selectionArgs,
                sorted);
    }


    // AdapterView.OnItemClickListener
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = (Cursor) favoritesAdapter.getItem(position);
        ((MainActivity) ctx).goToStoreDetailsActivity(DBQueries.parsePLaygroundFromCursor(cursor));
    }
}
