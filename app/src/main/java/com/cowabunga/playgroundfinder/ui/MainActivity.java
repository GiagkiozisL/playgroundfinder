package com.cowabunga.playgroundfinder.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.R;


public class MainActivity extends AppCompatActivity {

    private static final int DETAILS_REQUEST_CODE = 103;
    private TextView mTextMessage;
    private Fragment mSelectedFragment = null;
    private ExploreFrgament exploreFrgament = null;
    private FavoritesFragment favoritesFragment = null;
    private static final long af_timestamp = 1561835768052L;
    private FloatingActionButton ydxBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        showFragment(ExploreFrgament.fragment_tag);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DETAILS_REQUEST_CODE) {
            if (data != null && data.hasExtra("refreshDb")) {
                boolean refreshDb = data.getBooleanExtra("refreshDb", false);

                try {
                    if (refreshDb && favoritesFragment != null) {
                        favoritesFragment.startCursorLoader();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void onResume() {
//        mRewardedVideoAd.resume(this);
        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_explore:
                    showFragment(ExploreFrgament.fragment_tag);
                    mTextMessage.setText(R.string.title_explore);
                    return true;
                case R.id.navigation_favorite:
                    showFragment(FavoritesFragment.fragment_tag);
                    mTextMessage.setText(R.string.favorite_title);
                    return true;
//                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
//                    return true;
            }
            return false;
        }
    };


    private void showFragment(final String tag) {

        Fragment fragment = null;
        String barTitle = "";

        int[] animations = null;

        switch (tag) {
            case ExploreFrgament.fragment_tag:
                if (exploreFrgament == null) {
                    exploreFrgament = new ExploreFrgament();
                }
                fragment = exploreFrgament;
                barTitle = getString(R.string.explore_title);
                break;
            case FavoritesFragment.fragment_tag:
                if (favoritesFragment == null) {
                    favoritesFragment = new FavoritesFragment();
                }
                fragment = favoritesFragment;
                barTitle = getString(R.string.favorite_title);
                break;
        }


        replaceFragment(fragment, tag, animations);
        setActionBarTitle(barTitle);
    }

    private void replaceFragment(Fragment fragment, String tag, int[] animations) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment prev = getSupportFragmentManager().findFragmentByTag(tag);
        if (prev != null) {
            transaction.remove(prev);
        }

        try {
            if (fragment != mSelectedFragment) {

                if (animations != null) {
                    transaction.setCustomAnimations(animations[0], animations[1], animations[2], animations[3]);
                }

                int stack = manager.getBackStackEntryCount();
                if (stack == 0) {
                    transaction.add(R.id.main_fragment_container, fragment, tag).addToBackStack(tag).commitAllowingStateLoss();
                } else {
                    transaction.replace(R.id.main_fragment_container, fragment, tag).commitAllowingStateLoss();
                }

                mSelectedFragment = fragment;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void setActionBarTitle(String title) {
        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle(title);
    }

    public void goToStoreDetailsActivity(Playground playground) {
        Intent storeDetailIntent = new Intent(MainActivity.this, PlaygroundDetailActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("playground", playground);
        storeDetailIntent.putExtras(b);
        startActivityForResult(storeDetailIntent, DETAILS_REQUEST_CODE);
        overridePendingTransition(R.anim.left_in, R.anim.left_out);
    }



}
