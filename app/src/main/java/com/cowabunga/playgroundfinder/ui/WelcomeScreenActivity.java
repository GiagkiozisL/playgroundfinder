package com.cowabunga.playgroundfinder.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cowabunga.playgroundfinder.AnimationFrameLayout;
import com.cowabunga.playgroundfinder.R;
import com.cowabunga.playgroundfinder.application.Constants;
import com.cowabunga.playgroundfinder.utils.Utils;
import com.cowabunga.playgroundfinder.application.PLayGroundApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;


import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class WelcomeScreenActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private static final int LOCATION_REQUEST_CODE = 100;

    private GoogleApiClient googleApiClient;

    private Location userLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private PLayGroundApplication pLayGroundApplication;
    private LocationManager locationManager;

    private AnimationFrameLayout animationFrameLayout;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        animationFrameLayout = findViewById(R.id.welcome_progress_anim);
        animationFrameLayout.startLoading(this);

        pLayGroundApplication = PLayGroundApplication.getInstance();

        // start location fetch procedure
        startLocationProcedure();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    initGoogleApi();
                } else {
                    Toast.makeText(WelcomeScreenActivity.this, "Please enable location permission in app 'Settings'", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startLocationProcedure() {

        if (Build.VERSION.SDK_INT >= 23) {
            askLocationPermission();
        } else {
            initGoogleApi();
        }
    }

    private void askLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            initGoogleApi();
        } else {
            ActivityCompat.requestPermissions(WelcomeScreenActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(WelcomeScreenActivity.this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    private void goToMainActivity() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(WelcomeScreenActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        }, 1500);
    }

    private void initGoogleApi() {
        googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
        googleApiClient.connect();
    }

    // region //// GoogleApiClient.ConnectionCallbacks ////
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations, this can be null.
                            if (location == null) {
//                                location = getLocationFromGPSNet();
                                location = getLastKnownLocation();
                            }

                            if (location == null) { // if is still null..
                                Toast.makeText(WelcomeScreenActivity.this, "Could not fetch user location.", Toast.LENGTH_SHORT).show();

                                // stop loader
                                showAlertDialog("Could not fetch user location.");
                                return;
                            }

                            userLocation = location;
                            // store to in application
                            pLayGroundApplication.setUserLocation(userLocation);
//                            goToMainActivity();
                        }
                    });
        } catch (SecurityException se) {
            se.printStackTrace();
            showAlertDialog("Could not fetch user location.");
        } finally {
            animationFrameLayout.stopLoading();
            animationFrameLayout.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    // endregion //// GoogleApiClient.ConnectionCallbacks ////

    // GoogleApiClient.OnConnectionFailedListener
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showAlertDialog("Could not fetch user location.");
        animationFrameLayout.stopLoading();
        animationFrameLayout.setVisibility(View.INVISIBLE);
    }

    private Location getLastKnownLocation() throws SecurityException {
        locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (locationManager == null) {
            return null;
        }

        if (!Utils.isPermissionGranted(this, ACCESS_FINE_LOCATION) && Utils.isPermissionGranted(this, ACCESS_COARSE_LOCATION)) {
            return null;
        }

        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public Location getLocationFromGPSNet() throws SecurityException {
        Location location = null;
        try {
            locationManager = (LocationManager) WelcomeScreenActivity.this.getSystemService(LOCATION_SERVICE);

            if (locationManager == null)
                return location;

            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (!Utils.isPermissionGranted(this, ACCESS_FINE_LOCATION) && Utils.isPermissionGranted(this, ACCESS_COARSE_LOCATION)) {
                return null;
            }


            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
            }

            if(isGPSEnabled) {
                if(location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if(locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    private Location getLocationFromProvider(final Context context, final String provider) {

        if (!Utils.isPermissionGranted(context, ACCESS_FINE_LOCATION) && Utils.isPermissionGranted(context, ACCESS_COARSE_LOCATION)) {
            return null;
        }

        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            // noinspection ResourceType
            return locationManager.getLastKnownLocation(provider);
        } catch (SecurityException e) {
            Log.d("LocationSelectionAct","Failed to retrieve location from " + provider);
        } catch (IllegalArgumentException e) {
            Log.d("LocationSelectionAct","Failed to retrieve location: device has no " + provider);
        } catch (NullPointerException e) { // This happens on 4.2.2 on a few Android TV devices
            Log.d("LocationSelectionAct","Failed to retrieve location: device has no " + provider);
        }

        return null;
    }

    // LocationListener //// location listener callbacks ///
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
