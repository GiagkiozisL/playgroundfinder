package com.cowabunga.playgroundfinder.adapter;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.R;
import com.cowabunga.playgroundfinder.application.PLayGroundApplication;
import com.cowabunga.playgroundfinder.db.DBQueries;
import com.cowabunga.playgroundfinder.utils.Utils;
import com.cowabunga.playgroundfinder.view.CustomTextView;
import com.google.android.gms.maps.model.LatLng;

public class FavoritesAdapter extends CursorAdapter {

    private LayoutInflater mInflater;
    private Context ctx;
    private LatLng userLocation;

    public FavoritesAdapter(Context context) {
        super(context, null, 0);

        ctx = context;
        mInflater = LayoutInflater.from(ctx);
        Location location = PLayGroundApplication.getInstance().getUserLocation();
        userLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final View view = mInflater.inflate(R.layout.custom_playground_row, parent, false);

        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.typeImageView = view.findViewById(R.id.custom_playground_imageview);
        viewHolder.nameTxtView = view.findViewById(R.id.custom_playground_name_txt);
        viewHolder.addressTxtView = view.findViewById(R.id.custom_playground_address_txt);
        viewHolder.distanceTxtView = view.findViewById(R.id.custom_playground_distance_txt);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        final ViewHolder viewHolder = (ViewHolder) view.getTag();
        final Playground playground = DBQueries.parsePLaygroundFromCursor(cursor);

        viewHolder.nameTxtView.setText(playground.name);
        viewHolder.addressTxtView.setText(playground.address);
        final float distance = distanceBetween(userLocation.latitude, userLocation.longitude, playground.latitude, playground.longitude);
        playground.distance = (double) distance;
        viewHolder.distanceTxtView.setText(String.valueOf(distance) + " meters");
        viewHolder.typeImageView.setImageResource(Utils.getDrawbleByCategory(playground.categories));
    }

    private class ViewHolder {
        ImageView typeImageView;
        CustomTextView nameTxtView;
        CustomTextView addressTxtView;
        CustomTextView distanceTxtView;
    }

    private float distanceBetween(double startLat, double startLong, double endLat, double endLong) {
        float[] results = new float[1];
        try {
            Location.distanceBetween(startLat,
                    startLong,
                    endLat,
                    endLong,
                    results);
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0f;
        }

        return results[0];
    }
}
