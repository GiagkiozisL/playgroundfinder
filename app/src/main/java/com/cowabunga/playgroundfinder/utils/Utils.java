package com.cowabunga.playgroundfinder.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.cowabunga.playgroundfinder.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.List;
import java.util.Map;


public class Utils {

    private static final String TAG = "Utils";
    private static final float NEARBY_DISTANCE_IN_METERS = 20.000f;
    private static final String pkg_name = "com.cowabunga.playgroundfinder";
    private static final long firstInstallTime = 1561644834920L;
    private static final long af_timestamp = 1561835768052L;//// 1561644498147L;

    public static boolean isPermissionGranted(Context context, final String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static int getDrawbleByCategory(List<String> categories) {

        int drawable = R.drawable.park2;
        if (categories.contains("Playground") || categories.contains("Daycare")) {
            drawable = R.drawable.playground;
        } else if (categories.contains("Parks")) {
            drawable = R.drawable.park1;
        } else if (categories.contains("Plaza")) {
            drawable = R.drawable.plaza;
        } else if (categories.contains("Theme Park")) {
            drawable = R.drawable.theme_park;
        }

        return drawable;
    }

    public static boolean areLocationsNearby(LatLng locationA, LatLng locationB) {

        float[] results = new float[1];
        try {
            Location.distanceBetween(locationA.latitude, locationA.longitude, locationB.latitude, locationB.longitude, results);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        Log.d(TAG, "-------------- areLocationsNearby() --------------------");
        Log.d(TAG, "location A:" + locationA.latitude + "-" + locationA.longitude);
        Log.d(TAG, "location B:" + locationB.latitude + "-" + locationB.longitude);
        Log.d(TAG, "location between A & B is " + results[0]);
        Log.d(TAG, "RESULT ->>>> " + String.valueOf(results[0] <= NEARBY_DISTANCE_IN_METERS));

        return results[0] <= NEARBY_DISTANCE_IN_METERS;

    }

//    public static String constructChckSum(Context ctx) {
//        String checksum = "";
//
////        com.appsflyer.internal.C0045r.m158(ctx, r5);
//
//        long var12 = (new Date()).getTime();
//
//        checksum = MySecB.ˎ(ctx, af_timestamp);// todo var12 instead of 156..98
//        return MySupecB.ˎ(ctx, af_timestamp);// todo var12 instead of 156..98
//    }

    public static String getHashCode(Map<String, String> paramMap) {
        String str1 = (String)paramMap.get("appsflyerKey");
        String str2 = (String)paramMap.get("af_timestamp");
        String str3 = (String)paramMap.get("uid");
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.reset();
            messageDigest.update((str1.substring(0, 7) + str3.substring(0, 7) + str2.substring(-7 + str2.length())).getBytes("UTF-8"));
            return byteToHex(messageDigest.digest());
        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
            noSuchAlgorithmException.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            unsupportedEncodingException.printStackTrace();
            return null;
        }
    }

    private static String byteToHex(byte[] paramArrayOfByte) {
        Formatter formatter = new Formatter();
        int i = paramArrayOfByte.length;
        for (byte b = 0; b < i; b++) {
            byte b1 = paramArrayOfByte[b];
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Byte.valueOf(b1);
            formatter.format("%02x", arrayOfObject);
        }
        String str = formatter.toString();
        formatter.close();
        return str;
    }

    public native String getNativeCode(String paramString1, String paramString2, String paramString3);

}
