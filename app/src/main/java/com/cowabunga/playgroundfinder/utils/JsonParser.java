package com.cowabunga.playgroundfinder.utils;

import com.cowabunga.playgroundfinder.Playground;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public static Playground parsePlayground(JSONObject jsonObject) {

        Playground playground = new Playground();
        playground.id = jsonObject.optString("id");
        playground.name = jsonObject.optString("name");
        playground.contact = jsonObject.optString("contact");

        JSONObject locationObj = jsonObject.optJSONObject("location");
        playground.address = locationObj.optString("address");
        playground.latitude = locationObj.optDouble("lat");
        playground.longitude = locationObj.optDouble("lng");
        playground.distance = locationObj.optInt("distance");
        playground.country = locationObj.optString("cc");
        playground.city = locationObj.optString("city");
        playground.state = locationObj.optString("state");
        playground.formattedAddress = locationObj.optString("formattedAddress");

        JSONArray categoriesArray = jsonObject.optJSONArray("categories");

        for (int j = 0; j < categoriesArray.length(); j++) {
            playground.categories.add(categoriesArray.optJSONObject(j).optString("name"));
        }

        return playground;
    }

    public static List<String> categoriesFromJson(String arrayString) {

        List<String> categories = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(arrayString);
            if (jsonArray.length() > 0) {
                for (int i = 0; i <jsonArray.length(); i++) {
                    categories.add(jsonArray.get(i).toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return categories;
    }

    public static String categoriesToJson(List<String> categories) {

        if (categories.size() == 0) return "";

        JSONArray jsonArray = new JSONArray();

        try {
            for (int i = 0; i < categories.size(); i++) {
                jsonArray.put(categories.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonArray.toString();
    }
}
