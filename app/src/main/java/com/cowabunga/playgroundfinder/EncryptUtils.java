package com.cowabunga.playgroundfinder;

import java.security.MessageDigest;
import java.util.Formatter;

public class EncryptUtils {

    public static String sha1(String str) {
        String str2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.reset();
            instance.update(str.getBytes("UTF-8"));
            str2 = toHEX(instance.digest());
        } catch (Throwable e) {
            e.getStackTrace();
//            AFLogger.afErrorLog(new StringBuilder("Error turning ").append(str.substring(0, 6)).append(".. to SHA1").toString(), e);
        }
        return str2;
    }

    /* renamed from: ˊ */
    public static String md5(String str) {
        String str2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(str.getBytes("UTF-8"));
            str2 = toHEX(instance.digest());
        } catch (Throwable e) {
            e.getStackTrace();
//            AFLogger.afErrorLog(new StringBuilder("Error turning ").append(str.substring(0, 6)).append(".. to MD5").toString(), e);
        }
        return str2;
    }

    /* renamed from: ˋ or m190*/
    public static String sha256(String str) {
        String str2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                stringBuffer.append(Integer.toString((b & 255) + 256, 16).substring(1));
            }
            str2 = stringBuffer.toString();
        } catch (Throwable e) {
            e.getStackTrace();
//            AFLogger.afErrorLog(new StringBuilder("Error turning ").append(str.substring(0, 6)).append(".. to SHA-256").toString(), e);
        }
        return str2;
    }

    /* renamed from: ˋ */
    private static String toHEX(byte[] bArr) {
        Formatter formatter = new Formatter();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            formatter.format("%02x", new Object[]{Byte.valueOf(bArr[i])});
        }
        String obj = formatter.toString();
        formatter.close();
        return obj;
    }
}
