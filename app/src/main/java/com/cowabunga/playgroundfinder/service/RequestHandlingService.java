package com.cowabunga.playgroundfinder.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cowabunga.playgroundfinder.Playground;
import com.cowabunga.playgroundfinder.events.PlaygroundEvent;
import com.cowabunga.playgroundfinder.server.RequestManager;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RequestHandlingService extends Service {


    private final IBinder mBinder = new LocalBinder();
    private Context mContext;
    private RequestManager requestManager;

    private EventBus mEventBus = EventBus.getDefault();
    private PlaygroundEvent playgroundEvent = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        mContext = getApplicationContext();
        requestManager = RequestManager.getInstance(mContext);
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public RequestHandlingService getService() {
            return RequestHandlingService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    // region //// requests ////
    public void getCountriesRequest(Location location, String categoryId, long radius) {
        requestManager.createSpotsRequest(countriesSuccessListener(), countriesErrorListener(), location, categoryId, radius);
    }


    // endregion //// requests ////

    // region //// event bus actions ////
    private void sendPlaygroundEvent(List<Playground> playgrounds) {
        playgroundEvent = new PlaygroundEvent();
        playgroundEvent.setCiies(playgrounds);
        mEventBus.post(playgroundEvent);
    }
    // endregion //// event bus actions ////

    // region //// Volley Responses ////
    private Response.Listener<JSONObject> countriesSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


//                public int id = -1;
//                public String name = "";
//                public String address = "";
//                public double latitude = 0.0;
//                public double longitude = 0.0;
//                public String postalCode = "";
//                public String country = "";
//                public String city = "";
//                public String state = "";
//                public String formattedAddress = "";
//                public String contact = "";
//                public double distance = 0.0; // in kilometers
//                public List<String> categories = new ArrayList<>();

                List<Playground> playgrounds = new ArrayList<>();

//                JSONObject region = response.optJSONObject("region");
//
//                Iterator<?> keys = region.keys();
//                while (keys.hasNext()) {
//                    String objKey = (String) keys.next();
//                    try {
//                        JSONObject cityObj = new JSONObject(region.optString(objKey));
//                        int id = cityObj.optInt("term_id");
//                        String name = cityObj.optString("name");
//
//                        CityItem cityItem = new CityItem();
//                        cityItem.id = id;
//                        cityItem.name = name;
//
//                        cities.add(cityItem);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
                sendPlaygroundEvent(playgrounds);
            }
        };
    }

    private Response.ErrorListener countriesErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sendPlaygroundEvent(new ArrayList<Playground>());
            }
        };
    }


    // endregion //// Volley Responses ////

}
