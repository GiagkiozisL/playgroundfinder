package com.cowabunga.playgroundfinder.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.cowabunga.playgroundfinder.Enums;
import com.cowabunga.playgroundfinder.R;


public class CustomTextView extends AppCompatTextView {


    private static final float DEFAULT_TEXT_SIZE = 14f;

    private Context mContext;
    private int mBackgroundColor;
    private int mBackgroundDrawable;
    private int mTextColor;
    private String mText;
    private float mTextSize;
    private int mFontTypeface = 0;
    private int mFontIndex = 0;

    private String[] cuprumFontPaths = new String[]{"fonts/Montserrat-Regular.ttf",
            "fonts/Montserrat-Italic.ttf",
            "fonts/Montserrat-Bold.ttf",
            "fonts/Montserrat-BoldItalic.ttf"};


    private @Enums.FontStyle
    int mFontStyle = Enums.REGULAR;

    private @Enums.FontType
    String mFontType = Enums.NOTO_SANS;

    public CustomTextView(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomTextView, 0, 0);
        try {
            mBackgroundColor = typedArray.getColor(R.styleable.CustomTextView_customBackgroundColor, ContextCompat.getColor(context, android.R.color.transparent));
            mBackgroundDrawable = typedArray.getResourceId(R.styleable.CustomTextView_customBackgroundDrawable, 0);
            mText = typedArray.getString(R.styleable.CustomTextView_customText);
            mTextColor = typedArray.getColor(R.styleable.CustomTextView_customTextColor, ContextCompat.getColor(context, R.color.colorPrimary));
            mTextSize = typedArray.getFloat(R.styleable.CustomTextView_customTextSize, DEFAULT_TEXT_SIZE);
            mFontTypeface = typedArray.getInt(R.styleable.CustomTextView_customFontType, 0);
            mFontType = mFontTypeface == 0 ? Enums.montserrat : Enums.ROBOTO;
            mFontIndex = typedArray.getInt(R.styleable.CustomTextView_customFontStyle, mFontStyle);
        } finally {
            typedArray.recycle();
        }
        initView();
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    private OnLayoutListener onLayoutListener;
    public interface OnLayoutListener {
        void onLayoutChanged(View v);
    }

    public void setOnLayoutListener(OnLayoutListener listener) {
        onLayoutListener = listener;
    }

    private void initView() {

        final Typeface customFont = getTypefaceStyle();
        setTypeface(customFont);

        setText(mText);
        setTextColor(mTextColor);
        setTextSize(mTextSize);

        if (mBackgroundDrawable != 0)
            setBackgroundResource(mBackgroundDrawable);

        if (mBackgroundColor != 0)
            setBackgroundColor(mBackgroundColor);
    }

    private Typeface getTypefaceStyle() {

        String typePath = "";
        switch (mFontType) {
            case Enums.montserrat:
                typePath = cuprumFontPaths[mFontIndex];
                break;
            default:
                Log.d("CustomTextView", "ATTENTION!!! font type unknown!!");
                break;
        }

        if (typePath.isEmpty()) // if STILLS empty
            typePath = cuprumFontPaths[0];

        return Typeface.createFromAsset(mContext.getAssets(), typePath);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (onLayoutListener != null) {
            onLayoutListener.onLayoutChanged(this);
        }
    }
}
