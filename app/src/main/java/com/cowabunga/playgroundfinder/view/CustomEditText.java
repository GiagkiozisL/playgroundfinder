package com.cowabunga.playgroundfinder.view;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.cowabunga.playgroundfinder.application.Constants;


public class CustomEditText extends AppCompatEditText {


    public CustomEditText(Context context) {
        super(context);
        initView(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        final Typeface latoFont = Typeface.createFromAsset(context.getAssets(), Constants.base_montserrat_path);
        setTypeface(latoFont);
    }
}
