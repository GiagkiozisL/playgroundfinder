package com.cowabunga.playgroundfinder.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.Gravity;

import com.cowabunga.playgroundfinder.Enums;
import com.cowabunga.playgroundfinder.R;


public class RoundedCustomButton extends AppCompatButton {

    private static final int DEFAULT_CORNER_RADIUS = 5;
    private static final float DEFAULT_TEXT_SIZE = 9;

    private Context mContext;
    private float mRadius;
    private int mBackgroundColor;
    private String mText;
    private int mTextColor;
    private float mTextSize;
    private boolean mHasDrawableLeft;
    private int mDrawableLeft;
    private boolean mEnabled;
    private int mFontIndex = 0;
    private boolean isTransparent;

    private String[] cuprumFontPaths = new String[]{"fonts/Montserrat-Regular.ttf",
            "fonts/Montserrat-Italic.ttf",
            "fonts/Montserrat-Bold.ttf",
            "fonts/Montserrat-BoldItalic.ttf"};

    private @Enums.FontStyle
    int mFontStyle = Enums.REGULAR;

    public RoundedCustomButton(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public RoundedCustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RoundedCustomButton,0,0);
        try {
            mRadius = typedArray.getFloat(R.styleable.RoundedCustomButton_radius, DEFAULT_CORNER_RADIUS);
            mBackgroundColor = typedArray.getColor(R.styleable.RoundedCustomButton_backgroundColor, ContextCompat.getColor(context, R.color.colorPrimary));
            mText = typedArray.getString(R.styleable.RoundedCustomButton_text);
            mTextColor = typedArray.getColor(R.styleable.RoundedCustomButton_textColor, ContextCompat.getColor(context, android.R.color.white));
            mTextSize = typedArray.getFloat(R.styleable.RoundedCustomButton_textSize, DEFAULT_TEXT_SIZE);
            mDrawableLeft = typedArray.getResourceId(R.styleable.RoundedCustomButton_drawable_left, R.mipmap.ic_launcher);
            mHasDrawableLeft = typedArray.getBoolean(R.styleable.RoundedCustomButton_has_drawable, false);
            mEnabled = typedArray.getBoolean(R.styleable.RoundedCustomButton_enabled, true);
            mFontIndex = typedArray.getInt(R.styleable.RoundedCustomButton_font_style, mFontStyle);
            isTransparent = typedArray.getBoolean(R.styleable.RoundedCustomButton_trans, false);

        } finally {
            typedArray.recycle();
        }
        initView(context);
    }

    public RoundedCustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {

        mContext = context;

        String fontPath = cuprumFontPaths[mFontIndex];
        final Typeface customFont = Typeface.createFromAsset(context.getAssets(), fontPath);
        setTypeface(customFont);

        setText(mText);
        setTextColor(mTextColor);
        setTextSize(mTextSize);

        setGravity(Gravity.CENTER_VERTICAL| Gravity.CENTER_HORIZONTAL);

        if (mHasDrawableLeft)
            setCompoundDrawablesWithIntrinsicBounds(mDrawableLeft, 0, 0, 0);

        setEnabled(mEnabled);

        if (!isTransparent)
            setBackgroundDrawable(buttonStates());
    }

    public void setDrawableLeft(int drawableLeft) {
        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, 0, 0, 0);
    }


    private StateListDrawable buttonStates() {
        StateListDrawable states = new StateListDrawable();

        GradientDrawable enabledState = new GradientDrawable();
        enabledState.setCornerRadius(mRadius);
        enabledState.setColor(mBackgroundColor);

        GradientDrawable disabledState = new GradientDrawable();
        disabledState.setCornerRadius(mRadius);
        disabledState.setColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));

        states.addState(new int[] {android.R.attr.state_pressed, android.R.attr.state_enabled}, enabledState);
        states.addState(new int[] {android.R.attr.state_focused, android.R.attr.state_enabled}, enabledState);
        states.addState(new int[] {android.R.attr.state_enabled}, enabledState);
        states.addState(new int[] {-android.R.attr.state_enabled}, disabledState);

        return states;
    }
}
