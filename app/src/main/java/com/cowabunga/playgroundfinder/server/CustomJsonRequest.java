package com.cowabunga.playgroundfinder.server;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CustomJsonRequest extends JsonObjectRequest {


    public CustomJsonRequest(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        Map<String, String> headers = new HashMap<>();
//        headers.put("Content-Type", "application/json;charset=utf-8");
//        headers.put("Accept", "application/json;charset=utf-8");
        return headers;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//        response.headers.put("Content-Type", "application/json;charset=utf-8");
        return super.parseNetworkResponse(response);
    }
}
