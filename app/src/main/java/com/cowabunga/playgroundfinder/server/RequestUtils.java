package com.cowabunga.playgroundfinder.server;

import android.location.Location;

import com.cowabunga.playgroundfinder.application.Constants;

import org.json.JSONObject;

public class RequestUtils {

    public static final String client_id = "T5XNH5GKMSF2UJY5G0AJAZUPTEZHZWPPZ4IBVEIMEQTQSRX3";
    public static final String client_secret="GJ1HY2S30EFGP3Y2ZWFZ14DPLMIGZ3G0IAMRN5YKQVUEVMR4";
    public static final String v = "20180323";

    public static JSONObject getAuthParms() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("client_id", client_id);
            jsonObject.put("client_secret", client_secret);
            jsonObject.put("v", v);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static String appendSearchParams(String url, Location location, String categoryId, long radius) {

        StringBuilder stringbuilder = new StringBuilder();

        stringbuilder.append(url);
        stringbuilder.append("?client_id=");
        stringbuilder.append(client_id);
        stringbuilder.append("&client_secret=");
        stringbuilder.append(client_secret);
        stringbuilder.append("&v=");
        stringbuilder.append(v);
        stringbuilder.append("&categoryId=");
        stringbuilder.append(categoryId);
        stringbuilder.append("&radius=");
        stringbuilder.append(String.valueOf(radius));
        stringbuilder.append("&limit=");
        stringbuilder.append(50);
        stringbuilder.append("&ll=");
        stringbuilder.append(String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()));
        stringbuilder.append("&llAcc=");
        stringbuilder.append(location.getAccuracy());
        stringbuilder.append("&alt=");
        stringbuilder.append(location.getAltitude());

        return stringbuilder.toString();
    }
}
