package com.cowabunga.playgroundfinder.server;

import android.content.Context;
import android.location.Location;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.cowabunga.playgroundfinder.application.Constants;
import com.cowabunga.playgroundfinder.application.PLayGroundApplication;


import org.json.JSONObject;

public class RequestManager {

    private static Context mContext;
    private static RequestManager sInstance;
    private static RequestQueue mRequestQueue;

    private final String TAG = "RequestManager";
    private static final int TIMEOUT = 30000;
    private static final int MAX_RETRIES = 0;

    public static RequestManager getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new RequestManager(context);
        }
        return sInstance;
    }

    private RequestManager(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    private void addRequestToQueue(Request<?> request) {
        PLayGroundApplication.getInstance().addToRequestQueue(request, PLayGroundApplication.TAG);
    }

    public void createSpotsRequest(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, Location location, String categoryId, long radius) {
        String url = Constants.search_endpoint;

        String comleteUrl = RequestUtils.appendSearchParams(url, location, categoryId, radius);

        createRequest(Request.Method.GET,
                new JSONObject(),
                listener,
                errorListener,
                comleteUrl,
                false);
    }


    private void createRequest(int method, JSONObject params, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, String url, Boolean shouldCache) {
        CustomJsonRequest request = new CustomJsonRequest(
                method,
                url,
                params,
                listener,
                errorListener);

        request.setShouldCache(shouldCache);
        request.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, MAX_RETRIES, 0));
        addRequestToQueue(request);
    }
}
