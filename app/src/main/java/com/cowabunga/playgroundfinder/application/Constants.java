package com.cowabunga.playgroundfinder.application;

public class Constants {

    public static String playgroundId = "4bf58dd8d48988d1e7941735";

    public static String search_endpoint = "https://api.foursquare.com/v2/venues/search";
    public static String slider_endpoint = "https://veganguidegreece.com/api.php/wp_revslider_slides?filter=slider_id,eq,2";
    public static String category_endpoint = "https://veganguidegreece.com//wp-json/wp/v2/job-listings?job-categories=";
    public static String news_endpoint = "https://veganguidegreece.com/wp-json/wp/v2/posts";

    public static final String base_montserrat_path = "fonts/Montserrat-Regular.ttf";

    // SOny 1st July
    public static final String apps_flyer_key = "BPceQWK2joRcrcS7jkmGTo";
    public static final String af_timestamp = "1561967764596";
    public static final String uid = "1561967765551-6352830641884671194";
    public static final String ad_id = "6828fd23-7922-492e-95ed-ba29547c32fb";
    public static final String is_pre_installed = "false";

    public static final String appodeal_api_key = "e7a7ee04cba95a70f834743d9a1dd662";
    public static final String admob_app_id = "ca-app-pub-3890393115203761~6900213717";
    public static final String admob_video_unit_id = "ca-app-pub-3890393115203761/2769397010";
    public static final String test_admob_video_unit_id = "ca-app-pub-3940256099942544/5224354917";


}
