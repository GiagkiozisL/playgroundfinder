package com.cowabunga.playgroundfinder.application;

import android.content.Context;
import android.content.SharedPreferences;

public class PlayPreferences {

    private static PlayPreferences instance;
    private static final String SHARED_IDENTIFIER = "PlayPreferences";
    private static final String LAST_FETCH_TIMESTAMP = "application.PlayPreferences.LAST_FETCH_TIMESTAMP";
    private static final String LAST_FETCH_LAT = "application.PlayPreferences.LAST_FETCH_LAT";
    private static final String LAST_FETCH_LNG = "application.PlayPreferences.LAST_FETCH_LNG";
    private static final String LAST_PLAY_RESULTS = "application.PlayPreferences.LAST_PLAY_RESULTS";
    private final SharedPreferences prefs;

    public static synchronized PlayPreferences getInstance(Context context) {
        if (instance == null)
            instance = new PlayPreferences(context);

        return instance;
    }

    private PlayPreferences(Context context) {
        prefs = context.getSharedPreferences(SHARED_IDENTIFIER, Context.MODE_PRIVATE);
    }

    public void setLastFetchTimestamp(long value) {
        prefs.edit().putLong(LAST_FETCH_TIMESTAMP, value).apply();
    }

    public long getLastFetchTimestamp() {
        return prefs.getLong(LAST_FETCH_TIMESTAMP, 0);
    }

    public void setLastFetchLat(double value) {
        prefs.edit().putString(LAST_FETCH_LAT, String.valueOf(value)).apply();
    }

    public String getLastFetchLat() {
        return prefs.getString(LAST_FETCH_LAT, "0.0");
    }

    public void setLastFetchLng(double value) {
        prefs.edit().putString(LAST_FETCH_LNG, String.valueOf(value)).apply();
    }

    public String getLastFetchLng() {
        return prefs.getString(LAST_FETCH_LNG, "0.0");
    }

    public void setLastPlayResults(String value) {
        prefs.edit().putString(LAST_PLAY_RESULTS, value).apply();
    }

    public String getLastPlayResults() {
        return prefs.getString(LAST_PLAY_RESULTS, "");
    }



}
