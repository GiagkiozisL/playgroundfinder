package com.cowabunga.playgroundfinder;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;
import android.util.Log;

import com.cowabunga.playgroundfinder.application.Constants;
import com.cowabunga.playgroundfinder.utils.Utils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {


    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

//        Uri uri = new Uri.Builder().scheme("metrica").authority(appContext.getPackageName()).build();

        ArrayList arrayList = new ArrayList();
        try {
//            PackageInfo a = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 4096);
            PackageInfo a = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), 16384);
            Log.d("useAppContext", a.reqFeatures.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Log.d("useAppContext", "uri..: " + uri.toString());
        assertEquals("com.cowabunga.playgroundfinder", appContext.getPackageName());
    }

    @Test
    public void testPackageManagerVal() {

        Context ctx = InstrumentationRegistry.getTargetContext();
        try {
            String var1 = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).packageName;
            String var2 = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_ACTIVITIES).packageName;
            long firstInstall = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).firstInstallTime;
            Log.d("TEST", "var1:" + var1 + " ,var2:" + var2 + " ,firstInstall:" + String.valueOf(firstInstall));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCheckSum() {

        // "cksm_v1": "976a20034bfa89eabfcfa7b452b04f5315"
//        Context ctx = InstrumentationRegistry.getTargetContext();
//        String checkSum = "f64973e3925bc3043fa8b3927194bf3468";
//
//        String encrypted = Utils.constructChckSum(ctx);
//        Log.d("TEST", "encrypted: " + encrypted);
//        assertEquals(checkSum, encrypted);
    }

/**
 *
 *  generates "af_v" value
 *
 * */
    @Test
    public void getHashCode() {

        Map<String, String> map = new HashMap<>();
        map.put("appsflyerKey", Constants.apps_flyer_key);
        map.put("af_timestamp", Constants.af_timestamp);
        map.put("uid", Constants.uid);

        String hashCode = Utils.getHashCode(map);
        Log.d("TEST", "getHashCode: " + hashCode);
    }
}
