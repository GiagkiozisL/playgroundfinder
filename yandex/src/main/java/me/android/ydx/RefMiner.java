package me.android.ydx;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Pair;

import java.net.HttpURLConnection;
import java.net.URL;

public class RefMiner {


    private RefMiner.ReMinerlistener listener;
    public interface ReMinerlistener {
        void foundReferrer(String referrer, int redirects);
        void minerJobFailed(int status, String message);
    }

    private int redirects = 0;
    private String referrer = "";
    private MiningRefTask minerAsyncTask;

    public RefMiner() {}

    public void startMiner(String startUrl, RefMiner.ReMinerlistener listener) {
        this.listener = listener;
        doMinerJob(startUrl);
    }

    public void cancelMiner() {
        if (minerAsyncTask != null) {
            minerAsyncTask.cancel(true);
        }
    }

    private void doMinerJob(String url) {
        try {
            minerAsyncTask = new MiningRefTask();
            minerAsyncTask.execute(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleOutput(Pair<Boolean, String> redirectPair) {
        boolean redirect = redirectPair.first;
        String redirectUrl = redirectPair.second;

        if (!redirect || redirectUrl.startsWith("market")) {
            if (redirectUrl.contains("referrer")) {
                Uri uri = Uri.parse(redirectUrl);
                referrer = uri.getQueryParameter("referrer");
                listener.foundReferrer(referrer, redirects);
                return;
            } else {
                listener.minerJobFailed(-1, "No referrer found on last redirect");
                return;
            }
        }
        doMinerJob(redirectUrl);
    }

    class MiningRefTask extends AsyncTask<String, Void, Pair<Boolean, String>> {

        @Override
        protected Pair<Boolean, String> doInBackground(String... params) {

            String urlStr = params[0];
            boolean redirect = false;
            String redirectUrl = "";

            try {
                URL url = new URL(urlStr);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setInstanceFollowRedirects(false);
                HttpURLConnection.setFollowRedirects(false);

                int status = urlConnection.getResponseCode();

                if (status != HttpURLConnection.HTTP_OK) {
                    if (status == HttpURLConnection.HTTP_MOVED_TEMP
                            || status == HttpURLConnection.HTTP_MOVED_PERM
                            || status == HttpURLConnection.HTTP_SEE_OTHER)
                        redirect = true;
                }

                if (redirect) {
                    redirectUrl = urlConnection.getHeaderField("Location");
                    redirects++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Pair<>(redirect, redirectUrl);
        }

        @Override
        protected void onPostExecute(Pair<Boolean, String> redirectPair) {
            super.onPostExecute(redirectPair);
            handleOutput(redirectPair);
        }
    }
}
