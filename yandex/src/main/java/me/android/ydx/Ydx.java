package me.android.ydx;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;

import com.yandex.metrica.MetricaService;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.ob.db;

import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.List;
import java.util.UUID;

public class Ydx {

    private static final String TAG = "Ydx";
    private Context ctx;
    private DataManager dataManager;
    private CustomData customData;
    private YDXManager ydxManager;
    private ReferrerManager referrerManager;
    private Application application;


    public Ydx(Application application) {
        this.ctx = application.getApplicationContext();
        this.ydxManager = YDXManager.getInstance();
        this.referrerManager = ReferrerManager.getInstance();
        this.application = application;
    }

    public void start(String gaid) {
        buildCustomData(gaid);
    }

    private void buildCustomData(String gaid) {
        if (gaid == null || gaid.isEmpty()) {
            ydxManager.error_logger(TAG, "gaid is empty");
            return;
        }
//        customData = new CustomData().createTestData(gaid);

        String clickId = UUID.randomUUID().toString();
        String offerId = Utils.randomHexWithLength(10);
        customData.curl = "https://redirect.appmetrica.yandex.com/serve/891149435940144709?click_id=" + clickId + "&c=" + offerId + "&google_aid=" + gaid;
        ydxManager.logger(TAG, "CustomData " + customData.toJson().toString());

        dataManager = DataManager.getInstance();

        doReferrerJob();
    }

    private void doReferrerJob() {

//        startYDXWithRef("appmetrica_tracking_id=891149435940144719");
        if (referrerManager.prepareWebView(ctx)) {
            referrerManager.start(customData, ydxManager, new ReferrerManager.RefManagerListener() {
                @Override
                public void onReferrerSuccess(List<String> urls, String referrer) {
                    customData.redirectUrls = urls;

                    // todo find another solution
                    if (referrer.contains("&")) {
                        referrer = referrer.substring(0, referrer.indexOf("&"));
                    }
                    startYDXWithRef(referrer);
                }

                @Override
                public void onReferrerError(List<String> urls, String errorMessage) {
                    customData.redirectUrls = urls;

                    if (!ydxManager.enabledEvents) {
                        return;
                    }

                    ydxManager.error_logger(TAG, "onReferrerError: " + errorMessage);

//                    JSONObject eventJson = JSONParser.buildReferrerErrorJson(customData, errorMessage);
//                    sendEvent(EventType.RefError, eventJson);
//                    clearLastRequest();
//                    handleRequests();
                }
            });
        }
    }

    private void startYDXWithRef(String referrer) {
//        ydxManager.logger(TAG, "attachReferrerToCReq");
//
//        try {
//            ReferrerInfo refInfo = new ReferrerInfo();
//            refInfo.rawReferrer = referrer;
//            refInfo.installReferrer = URLDecoder.decode(referrer, "UTF-8");
//            refInfo.referrerClickTimestampSeconds = System.currentTimeMillis() / 1000;
//            refInfo.installBeginTimestampSeconds = 0L;
//
//            customData.refData = refInfo;
//            dataManager.setCustomData(customData);
//        } catch (Exception e) {
//            ydxManager.error_logger(TAG, "failed to get referrer with error " + e.getLocalizedMessage());
////            notifyAdjActivity("Referrer failed! Starting ADJ after " + actionsData.conversionDelay + " secs");
//            e.printStackTrace();
//        } finally {
//            startYdx();
//        }
    }

    private void startYdx() {
        hijackMetrica();
    }

    private void hijackMetrica() {
        // receiver stuff

        initYandex();
        db.m1275a(ctx).mo683a(customData.referrer);
    }

    private void initYandex() {
        ydxManager.logger(Constant.RUS_TAG, "Application-" + "setupYandex");
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("7e0e2b12-24e8-40a5-b864-02913e9d7c37").build();
        YandexMetrica.activate(application.getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(application);
    }

    public static void initYandex(Application application) {
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("7e0e2b12-24e8-40a5-b864-02913e9d7c37").build();
        YandexMetrica.activate(application, config);
        YandexMetrica.enableActivityAutoTracking(application);
    }

}
