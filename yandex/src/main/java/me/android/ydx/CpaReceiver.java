package me.android.ydx;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.vr;
import com.yandex.metrica.impl.ob.vz;
import com.yandex.metrica.impl.ob.yf;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CpaReceiver {

    /* renamed from: a */
    public static final Set<BroadcastReceiver> f32a = new HashSet();

    /* renamed from: B */
    private static final yk<BroadcastReceiver[]> f33b = new yg(new yf("Broadcast receivers"));

    public void onReceive(Context context, String referrer) {

        db.m1275a(context).mo683a(referrer);
//        if (mo99a(intent)) {
//            mo98a(context, intent);
//        }
//        vz a = vr.m4236a();
//        for (BroadcastReceiver broadcastReceiver : f32a) {
//            String format = String.format("Sending referrer to %s", new Object[]{broadcastReceiver.getClass().getName()});
//            if (a.mo2485c()) {
//                a.mo2477a(format);
//            }
//            broadcastReceiver.onReceive(context, intent);
//        }
    }



    /* access modifiers changed from: 0000 */
    /* renamed from: a */
//    public boolean mo99a(Intent intent) {
//        return "com.android.vending.INSTALL_REFERRER".equals(intent.getAction());
//    }
//
//    /* access modifiers changed from: 0000 */
//    /* renamed from: a */
//    public void mo98a(Context context, Intent intent) {
//        String stringExtra = intent.getStringExtra("referrer");
//        if (!TextUtils.isEmpty(stringExtra)) {
//            db.m1275a(context).mo683a(stringExtra);
//        }
//    }

    /* renamed from: a */
    static void addReceivers(BroadcastReceiver... broadcastReceiverArr) {
        f33b.a(broadcastReceiverArr);
        Collections.addAll(f32a, broadcastReceiverArr);
    }
}
