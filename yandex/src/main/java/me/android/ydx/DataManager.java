package me.android.ydx;

public class DataManager {

    private static DataManager instance;
    private CustomData customData;

    public static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    public DataManager() {}

    public void setCustomData(CustomData customData) {
        this.customData = customData;
    }

    public CustomData getCustomData() {
        return customData;
    }

    public void resetInstance() {
        instance = null;
    }
}
