package me.android.ydx;

import android.os.IBinder;

public interface ServiceConnection_ {

    void onServiceConnected(IBinder service);
    void onServiceDisconnected();
}
