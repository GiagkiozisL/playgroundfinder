package me.android.ydx;

import android.os.Build;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class CustomData {


    //https://report.appmetrica.yandex.net/report?encrypted_request=1
    // &deviceid=65fb55a0e3e9b33ca05945973eff8ffb
    // &uuid=c8b4bd8ffe8941f29e74537c7c64fdfe
    // &analytics_sdk_version_name=3.8.0
    // &app_version_name=1.3
    // &app_build_number=3
    // &os_version=5.1.1
    // &os_api_level=22
    // &analytics_sdk_build_number=66508
    // &analytics_sdk_build_type=public
    // &app_debuggable=0
    // &locale=en_US
    // &is_rooted=0
    // &app_framework=native
    // &attribution_id=1
    // &api_key_128=77ce423b-004c-485b-953c-c4b422358d5e
    // &app_id=com.cowabunga.playgroundfinder
    // &app_platform=android
    // &model=C6833
    // &manufacturer=Sony
    // &screen_width=1920
    // &screen_height=1080
    // &screen_dpi=320
    // &scalefactor=2.0
    // &device_type=phone
    // &android_id=79c3e92647f8c2c1
    // &adv_id=ae11c463-0e95-44c7-aeab-bb138faf54be
    // &limit_ad_tracking=1
    // &request_id=0

//    GET /analytics/startup
//    ?deviceid=65fb55a0e3e9b33ca05945973eff8ffb
//    &deviceid2=65fb55a0e3e9b33ca05945973eff8ffb
//    &adv_id=ae11c463-0e95-44c7-aeab-bb138faf54be
//    &app_platform=android
//    &protocol_version=2
//    &analytics_sdk_version_name=3.8.0
//    &model=C6833
//    &manufacturer=Sony
//    &os_version=5.1.1
//    &screen_width=1920&screen_height=1080
//    &screen_dpi=320
//    &scalefactor=2.0
//    &locale=en_US
//    &device_type=phone
//    &queries=1
//    &query_hosts=2
//    &features=ec%2Cpi%2Cs%2Cpc%2Cfc%2Cflc%2Cblc%2Cflbc%2Cblbc%2Ctrtlt%2Ca%2Cg%2Cwa%2Cwc%2Com%2Cca%2Csi%2Csm%2Cap%2Csl%2Cilc%2Cbc
//    &s=1
//    &app_id=cowabunga.bestradio.player
//    &flc=1
//    &app_debuggable=0
//    &sl=1
//    &blc=1
//    &detect_locale=1
//    &uuid=b08ee16a89724e038a1c05fed20bb39f
//    &time=1
//    &requests=1
//    &stat_sending=1
//    &permissions=1
//    &ilc=1&bc=1 HTTP/1.1

//    public String deviceId = ""; // md5(gaid)
    /// request data
    public String analytics_sdk_version = ""; // 3.8.0
    public String app_version_name = ""; // 1.3 version name
    public String app_build_number = ""; // 3 version code
    public String os_version = ""; //5.1.1
    public String os_api_level = ""; //22 Utils.getApiLevelFromVersion("5.1.1")
    public String analytics_sdk_build_number = ""; // 66508
    public String app_framework = ""; // "native" like platform extension
    public String api_key_128 = ""; // "77ce423b-004c-485b-953c-c4b422358d5e" app api key
    public String app_id = ""; // "com.cowabunga.playgroundfinder" packagename
    public String app_platform = ""; // android
    public String model = ""; //model
    public String manufacturer = ""; // manu
    public String android_id = ""; // 16 digits hex random
    public String adv_id = ""; // gaid
    public String protocol_version = ""; // 2
    public String serial = "";
    public int sdkApiLevel = 85; // 85
    public String uuid = "";
    public String deviceid = "";
    public String deviceid2 = "";


    public double lat = 0.0;
    public double lng = 0.0;

    /// engine data
    public String botPkgName = "";
    public long firstInstallTime = System.currentTimeMillis();
    public String userAgent = "";
    public int webGapSecs = 15;
    public List<String> redirectUrls = new ArrayList<>();
    public String curl = "";
    public String referrer = "";
    public String extras = "";

    public CustomData() {}

    public CustomData createTestData(String gaid, String ref) {
        analytics_sdk_version = "3.8.0"; // 3.8.0
        app_version_name = "1.6"; // 1.3 version name
        app_build_number = "6"; // 3 version code
        os_version = "5.1.1"; //5.1.1
        os_api_level = "22"; //22 Utils.getApiLevelFromVersion("5.1.1")
        analytics_sdk_build_number = "66508"; // 66508
        app_framework = "native"; // "native" like platform extension unity,xamarin,cordova,react,native
        api_key_128 = "77ce423b-004c-485b-953c-c4b422358d5e"; // "77ce423b-004c-485b-953c-c4b422358d5e" app api key
        app_id = "com.cowabunga.playgroundfinder"; // "com.cowabunga.playgroundfinder" packagename
        botPkgName = "com.cowabunga.playgroundf1nder";
        app_platform = "android"; // android
        model = "S5"; //model
        manufacturer = "Samsung"; // manu
        android_id = Utils.randomHexWithLength(16); // 16 digits hex random
        adv_id = gaid;//"78440f25-efe7-4904-a0f8-d04e34185097";//todo UUID.randomUUID().toString(); // gaid
        protocol_version = "2"; // 2
        sdkApiLevel = 85;
        serial = Utils.randomHexWithLength(8);
        uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase(Locale.US);
        referrer = ref;

        deviceid = Utils.md5(adv_id);
        deviceid2 = deviceid;
        try {
            extras = new JSONObject("{\"android.support.VERSION\":\"26.1.0\",\"com.google.android.geo.API_KEY\":\"AIzaSyCFAGCIq05rv52Iix4i56zNV41w21Sq1AE\",\"com.google.android.gms.version\":12451000}").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("analytics_sdk_version", analytics_sdk_version);
            jsonObject.put("app_version_name", app_version_name);
            jsonObject.put("app_build_number", app_build_number);
            jsonObject.put("os_version", os_version);
            jsonObject.put("os_api_level", os_api_level);
            jsonObject.put("analytics_sdk_build_number", analytics_sdk_build_number);
            jsonObject.put("app_framework", app_framework);
            jsonObject.put("api_key_128", api_key_128);
            jsonObject.put("app_id", app_id);
            jsonObject.put("app_platform", app_platform);
            jsonObject.put("model", model);
            jsonObject.put("manufacturer", manufacturer);
            jsonObject.put("android_id", android_id);
            jsonObject.put("adv_id", adv_id);
            jsonObject.put("protocol_version", protocol_version);
            jsonObject.put("serial", serial);
            jsonObject.put("deviceid", deviceid);
            jsonObject.put("deviceid2", deviceid2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

}
