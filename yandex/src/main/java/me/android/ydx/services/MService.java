package me.android.ydx.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

//import com.yandex.metrica.IMService;
//import com.yandex.metrica.MService;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.bc;
import com.yandex.metrica.impl.ob.bd;
import com.yandex.metrica.impl.ob.be;
import com.yandex.metrica.impl.ob.bt;
import com.yandex.metrica.impl.ob.cl;
import com.yandex.metrica.impl.ob.dr;
import com.yandex.metrica.impl.ob.dt;
import com.yandex.metrica.impl.ob.dz;
import com.yandex.metrica.impl.ob.vz;

import me.android.ydx.C0015c;
import me.android.ydx.Constant;
import me.android.ydx.ServiceConnection_;

public class MService {


    private static MService instance;
    private ServiceConnection_ connection_;

    private C0015c f34a = new C0015c() {
        public void mo109a(int i) {
            Log.d(Constant.RUS_TAG, "MService-" + "C0015c$mo109a");
//            MService.this.stopSelfResult(i);
        }
    };

    public bc f35b;
    private IBinder iBinder;

    private final IMetricaService.C0009a f36c = new IMetricaService.C0009a() {
        @Deprecated
        public void mo68a(String str, int i, String str2, Bundle bundle) throws RemoteException {
            Log.d(Constant.RUS_TAG, "MService-" + "C0009a$mo68a");
            MService.this.f35b.a(str, i, str2, bundle);
        }

        public void mo67a(Bundle bundle) throws RemoteException {
            Log.d(Constant.RUS_TAG, "MService-" + "C0009a$mo67a with bundle: " + bundle.toString());
            MService.this.f35b.a(bundle);
        }
    };

    static class C0013a extends Binder {
        C0013a() {
            Log.d(Constant.RUS_TAG, "MService-" + "C0013a");
        }
    }

    static class C0014b extends Binder {
        C0014b() {
            Log.d(Constant.RUS_TAG, "MService-" + "C0013a");
        }
    }

//    public interface C0015c {
//        void mo109a(int i);
//    }

    public static MService getInstance() {
        if (instance == null) {
            instance = new MService();
        }
        return instance;
    }

    public void setConnectionListener(ServiceConnection_ connection_) {
        this.connection_ = connection_;
    }

    public void onCreate(Context ctx) {
//        super.onCreate();
        Log.d(Constant.RUS_TAG, "MService-" + "onCreate");
        al.m325a(ctx);
        m83a(ctx.getResources().getConfiguration());
        vz.m4240a(ctx);
        this.f35b = new bd(new be(ctx, this.f34a));
        this.f35b.a();
        al.m324a().mo282a(new cl(this.f35b));
    }

    public void onStart(Intent intent, int startId) {
        Log.d(Constant.RUS_TAG, "MService-" + "onStart with intent: " + intent.toString() + " and startId: " + startId);
        this.f35b.a(intent, startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constant.RUS_TAG, "MService-" + "onStartCommand");
        this.f35b.a(intent, flags, startId);
        return Service.START_NOT_STICKY;
    }

    public void onBind(Intent intent) {
        String action = intent.getAction();
        Log.d(Constant.RUS_TAG, "MService-" + "onBind" + ", with action: " + action);
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            iBinder = new MService.C0014b();
        } else if ("com.yandex.metrica.ACTION_C_BG_L".equals(action)) {
            iBinder = new MService.C0013a();
        } else {
            iBinder = this.f36c;
        }
        this.f35b.a(intent);

        triggerConnected();
    }

    public void triggerConnected() {
        this.connection_.onServiceConnected(iBinder);
    }

    public void onRebind(Intent intent) {
//        super.onRebind(intent);
        Log.d(Constant.RUS_TAG, "MService-" + "onRebind with intent: " + intent.toString());
        this.f35b.b(intent);
    }

    public void onDestroy() {
        this.f35b.b();
        Log.d(Constant.RUS_TAG, "MService-" + "onDestroy");
//        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        Log.d(Constant.RUS_TAG, "MService-" + "onUnbind");
        this.f35b.c(intent);
        String action = intent.getAction();
        Log.d(Constant.RUS_TAG, "MService-" + "onUnbind" + ", with action: " + action);
        if ("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER".equals(action)) {
            return false;
        }
        if ("com.yandex.metrica.ACTION_C_BG_L".equals(action)) {
            return true;
        }
        if (!m84a(intent)) {
            return true;
        }
        return false;
    }

    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
        Log.d(Constant.RUS_TAG, "MService-" + "onConfigurationChanged");
        m83a(newConfig);
    }

    private void m83a(Configuration configuration) {
        Log.d(Constant.RUS_TAG, "MService-" + "m83a");
        dr.a().b((dt) new dz(bt.m724a(configuration.locale)));
    }

    private boolean m84a(Intent intent) {
        Log.d(Constant.RUS_TAG, "MService-" + "m84a");
        return intent == null || intent.getData() == null;
    }
}
