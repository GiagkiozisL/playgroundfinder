package me.android.ydx.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.de;
import com.yandex.metrica.impl.ob.df;
import com.yandex.metrica.impl.ob.jn;
import com.yandex.metrica.impl.ob.jr;
import com.yandex.metrica.impl.ob.jv;
import com.yandex.metrica.impl.ob.jy;
import com.yandex.metrica.impl.ob.jz;
import com.yandex.metrica.impl.ob.ka;

import java.util.HashMap;
import java.util.Map;

import me.android.ydx.Constant;

public class CService {

    private static CService instance;

    @NonNull
    private Map<String, jy> f19a = new HashMap();

    private jr f20b;

    @Nullable
    private String f21c;

    public static CService getInstance() {
        if (instance == null) {
            instance = new CService();
        }
        return instance;
    }

    public void onCreate(Context ctx) {
//        super.onCreate();
        Log.d(Constant.RUS_TAG, "CService-" + "onCreate");
        al.m325a(ctx);
//        this.f21c = String.format("[CService:%s]", new Object[]{getPackageName()});
        this.f20b = new jr();
        Context applicationContext = ctx;
        jv jvVar = new jv(applicationContext, this.f20b.mo1119a(), new jn(applicationContext));
        de deVar = null;
        if (cx.a(21)) {
            deVar = new de(applicationContext, new df(applicationContext));
        }
        this.f19a.put("com.yandex.metrica.configuration.ACTION_INIT", new ka(ctx, jvVar, deVar));
        this.f19a.put("com.yandex.metrica.configuration.ACTION_SCHEDULED_START", new jz(ctx, jvVar));
    }

    public void onStartCommand(Intent intent, int flags, int startId) {

        Bundle bundle = null;
        jy jyVar = (jy) this.f19a.get(intent == null ? null : intent.getAction());
        Log.d(Constant.RUS_TAG, "CService-" + "onStartCommand");
        if (intent != null) {
            Log.d(Constant.RUS_TAG, "CService-" + "action-> " + intent.getAction());
        }
        if (jyVar != null) {
            jr jrVar = this.f20b;
            if (intent != null) {
                bundle = intent.getExtras();
            }
            jrVar.mo1120a(jyVar, bundle);
        }
//         Service.START_NOT_STICKY;
    }

//    public IBinder onBind(Intent intent) {
//        Log.d(Constant.RUS_TAG, "CService-" + "onBind");
//        return null;
//    }
}
