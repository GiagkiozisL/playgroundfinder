package me.android.ydx;

import java.security.MessageDigest;
import java.util.Formatter;
import java.util.Random;

public class Utils {

    private static final String hex_chars ="0123456789abcdef";
    private static final String hex_letter_chars ="abcdef";

    public static String getApiLevelFromVersion(String version) {

        if (version.startsWith("9")) {
            return "28";
        }

        if (version.startsWith("8.1")) {
            return "27";
        }

        if (version.startsWith("8.0")) {
            return "26";
        }

        if (version.startsWith("7.1")) {
            return "25";
        }

        if (version.startsWith("7.0")) {
            return "24";
        }

        if (version.startsWith("6")) {
            return "23";
        }

        if (version.startsWith("5.0")) {
            return "21";
        }

        if (version.startsWith("5.1")) {
            return "22";
        }

        if (version.startsWith("4.4")) {
            return "19";
        }

        if (version.startsWith("4.3")) {
            return "18";
        }

        if (version.startsWith("4.2")) {
            return "17";
        }

        if (version.startsWith("4.1")) {
            return "16";
        }

        if (version.startsWith("4.0")) {
            return "15";
        }

        if (version.startsWith("3")) {
            return "13";
        }

        return version;

    }

    public static String randomHexWithLength(int length) {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder(length);

        for (int i = 0; i < length; ++i) {
            if (i == 0 || i == length - 1) {
                sb.append(hex_letter_chars.charAt(random.nextInt(hex_letter_chars.length()))); // first and last will be always a letter
            } else {
                sb.append(hex_chars.charAt(random.nextInt(hex_chars.length())));
            }

        }
        return sb.toString();
    }


    public static String sha1(String str) {
        String str2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.reset();
            instance.update(str.getBytes("UTF-8"));
            str2 = toHEX(instance.digest());
        } catch (Throwable e) {
            e.getStackTrace();
//            AFLogger.afErrorLog(new StringBuilder("Error turning ").append(str.substring(0, 6)).append(".. to SHA1").toString(), e);
        }
        return str2;
    }

    /* renamed from: ˊ */
    public static String md5(String str) {
        String str2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(str.getBytes("UTF-8"));
            str2 = toHEX(instance.digest());
        } catch (Throwable e) {
            e.getStackTrace();
//            AFLogger.afErrorLog(new StringBuilder("Error turning ").append(str.substring(0, 6)).append(".. to MD5").toString(), e);
        }
        return str2;
    }

    /* renamed from: ˋ or m190*/
    public static String sha256(String str) {
        String str2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                stringBuffer.append(Integer.toString((b & 255) + 256, 16).substring(1));
            }
            str2 = stringBuffer.toString();
        } catch (Throwable e) {
            e.getStackTrace();
//            AFLogger.afErrorLog(new StringBuilder("Error turning ").append(str.substring(0, 6)).append(".. to SHA-256").toString(), e);
        }
        return str2;
    }

    /* renamed from: ˋ */
    private static String toHEX(byte[] bArr) {
        Formatter formatter = new Formatter();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            formatter.format("%02x", new Object[]{Byte.valueOf(bArr[i])});
        }
        String obj = formatter.toString();
        formatter.close();
        return obj;
    }


    public static String capitalizeFirst(String myString) {
        return myString.substring(0, 1).toUpperCase() + myString.substring(1);
    }
}
