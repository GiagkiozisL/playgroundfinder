package me.android.ydx;

import android.util.Log;

public class YDXManager {

    // todo config before production
    private boolean proxyEnabled =  true;
    public boolean enabledEvents = true;
    public boolean broadcastUIEvents = false;
    private boolean log = true; // todo false


    private static YDXManager sInstance;
    private CustomData customRequest;


    private String bhIP = "";
    private String bhHost = "";
    private String dom = "";

    public static YDXManager getInstance() {
        if (sInstance == null) {
            sInstance = new YDXManager();
        }
        return sInstance;
    }

    private YDXManager() {

    }

    public void setCustomData(CustomData customRequest) {
        this.customRequest = customRequest;
    }

    public CustomData getCustomData() {
        return customRequest;
    }

    public void setBhIP(String bhIP) {
        this.bhIP = bhIP;
    }

    public String getBhIP() {
        return bhIP;
    }

    public String getBhHost() {
        return bhHost;
    }

    public void setBhHost(String bhHost) {
        this.bhHost = bhHost;
    }

    public void setDeveloperMode() {
        proxyEnabled = false;
        enabledEvents = false;

        // production
        broadcastUIEvents = true;
        log = true;
    }

    public void setDebugMode() {
        proxyEnabled = false;
        enabledEvents = true;

        // production
        broadcastUIEvents = true;
        log = true;
    }

    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    public void logger(String tag, String message) {
        if (log) {
            Log.d(tag, message);
        }
    }

    public void error_logger(String tag, String message) {
        if (log) {
            Log.e(tag, message);
        }
    }

}
