package me.android.ydx;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;

import com.yandex.metrica.ConfigurationService;

public class ServiceUtils {


    public static void rebindMetricaServiceWithAction(String action) {
//        MMetricaService.getInstance().onRebind(new Intent().setAction(action));
    }

    public static void startConfigurationServiceWithBundle(Bundle bundle, String pkgName, String action) {
        CConfigurationService.getInstance().onStartCommand(new Intent()
                .setComponent(new ComponentName(pkgName, "com.yandex.metrica.ConfigurationService"))
                .setAction(action)
                .putExtras(bundle));
    }
}
