package com.yandex.metrica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.yf;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;

import java.util.Currency;

public class Revenue {
    @NonNull
    public final Currency currency;
    @Nullable
    public final String payload;
    @Nullable
    @Deprecated
    public final Double price;
    @Nullable
    public final Long priceMicros;
    @Nullable
    public final String productID;
    @Nullable
    public final Integer quantity;
    @Nullable
    public final Receipt receipt;

    public static class Builder {

        /* renamed from: h */
        private static final yk<Currency> f49h = new yg(new yf("revenue currency"));
        @Nullable

        /* renamed from: a */
        Double f50a;
        @Nullable

        /* renamed from: B */
        Long f51b;
        @NonNull

        /* renamed from: a */
        Currency f52c;
        @Nullable

        /* renamed from: d */
        Integer f53d;
        @Nullable

        /* renamed from: e */
        String f54e;
        @Nullable

        /* renamed from: f */
        String f55f;
        @Nullable

        /* renamed from: g */
        Receipt f56g;

        Builder(double price, @NonNull Currency currency) {
            f49h.a(currency);
            this.f50a = Double.valueOf(price);
            this.f52c = currency;
        }

        Builder(long priceMicros, @NonNull Currency currency) {
            f49h.a(currency);
            this.f51b = Long.valueOf(priceMicros);
            this.f52c = currency;
        }

        @NonNull
        public Builder withQuantity(@Nullable Integer quantity) {
            this.f53d = quantity;
            return this;
        }

        @NonNull
        public Builder withProductID(@Nullable String productID) {
            this.f54e = productID;
            return this;
        }

        @NonNull
        public Builder withPayload(@Nullable String payload) {
            this.f55f = payload;
            return this;
        }

        @NonNull
        public Builder withReceipt(@Nullable Receipt receipt) {
            this.f56g = receipt;
            return this;
        }

        @NonNull
        public Revenue build() {
            return new Revenue(this);
        }
    }

    public static class Receipt {
        @Nullable
        public final String data;
        @Nullable
        public final String signature;

        public static class Builder {
            /* access modifiers changed from: private */
            @Nullable

            /* renamed from: a */
            public String f57a;
            /* access modifiers changed from: private */
            @Nullable

            /* renamed from: B */
            public String f58b;

            Builder() {
            }

            @NonNull
            public Builder withData(@Nullable String data) {
                this.f57a = data;
                return this;
            }

            @NonNull
            public Builder withSignature(@Nullable String signature) {
                this.f58b = signature;
                return this;
            }

            @NonNull
            public Receipt build() {
                return new Receipt(this);
            }
        }

        private Receipt(@NonNull Builder builder) {
            this.data = builder.f57a;
            this.signature = builder.f58b;
        }

        @NonNull
        public static Builder newBuilder() {
            return new Builder();
        }
    }

    private Revenue(@NonNull Builder builder) {
        this.price = builder.f50a;
        this.priceMicros = builder.f51b;
        this.currency = builder.f52c;
        this.quantity = builder.f53d;
        this.productID = builder.f54e;
        this.payload = builder.f55f;
        this.receipt = builder.f56g;
    }

    @Deprecated
    @NonNull
    public static Builder newBuilder(double price2, @NonNull Currency currency2) {
        return new Builder(price2, currency2);
    }

    @NonNull
    public static Builder newBuilderWithMicros(long priceMicros2, @NonNull Currency currency2) {
        return new Builder(priceMicros2, currency2);
    }
}
