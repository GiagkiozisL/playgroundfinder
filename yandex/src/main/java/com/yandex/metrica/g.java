package com.yandex.metrica;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.sa;
import com.yandex.metrica.profile.UserProfile;

import java.util.Map;

/* renamed from: com.yandex.metrica.g */
public class g {
    @NonNull

    /* renamed from: a */
    private final sa f92a;

    public g(@NonNull sa saVar) {
        this.f92a = saVar;
    }

    /* renamed from: a */
    public void mo156a() {
    }

    /* renamed from: a */
    public void mo167a(@NonNull String str) {
    }

    /* renamed from: a */
    public void mo168a(@NonNull String str, @Nullable String str2) {
    }

    /* renamed from: a */
    public void mo170a(@NonNull String str, @Nullable Map<String, Object> map) {
    }

    /* renamed from: a */
    public void mo169a(@NonNull String str, @Nullable Throwable th) {
    }

    /* renamed from: a */
    public void mo171a(@NonNull Throwable th) {
    }

    /* renamed from: a */
    public void mo157a(@Nullable Activity activity) {
    }

    /* renamed from: B */
    public void mo173b(@NonNull Activity activity) {
    }

    /* renamed from: B */
    public void mo175b(@Nullable String str) {
    }

    /* renamed from: a */
    public void mo166a(@NonNull UserProfile userProfile) {
    }

    /* renamed from: a */
    public void mo165a(@NonNull Revenue revenue) {
    }

    /* renamed from: a */
    public void mo159a(@NonNull Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        this.f92a.mo1988a(context, j.m4546a(yandexMetricaConfig));
    }

    /* renamed from: a */
    public void mo158a(@NonNull Application application) {
    }

    /* renamed from: a */
    public void mo177c(@NonNull String str) {
    }

    /* renamed from: a */
    public void mo176c(@NonNull Activity activity) {
    }

    /* renamed from: d */
    public void mo178d(@NonNull String str) {
    }

    /* renamed from: a */
    public void mo162a(@Nullable Location location) {
    }

    /* renamed from: a */
    public void mo172a(boolean z) {
    }

    /* renamed from: a */
    public void mo161a(@NonNull Context context, boolean z) {
    }

    /* renamed from: B */
    public void mo174b(@NonNull Context context, boolean z) {
    }

    /* renamed from: a */
    public void mo164a(@NonNull DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
    }

    /* renamed from: a */
    public void mo163a(@NonNull AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
    }

    /* renamed from: a */
    public void mo160a(@NonNull Context context, @NonNull f fVar) {
    }
}
