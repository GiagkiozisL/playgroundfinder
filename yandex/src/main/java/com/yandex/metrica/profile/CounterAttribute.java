package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qs;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.yk;

public final class CounterAttribute {

    /* renamed from: a */
    private final qt f3051a;

    CounterAttribute(@NonNull String key, @NonNull yk<String> keyValidator, @NonNull qn saver) {
        this.f3051a = new qt(key, keyValidator, saver);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withDelta(double value) {
        return new UserProfileUpdate<>(new qs(this.f3051a.mo1753a(), value));
    }
}
