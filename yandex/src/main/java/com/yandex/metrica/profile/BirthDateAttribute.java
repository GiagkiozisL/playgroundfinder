package com.yandex.metrica.profile;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.qo;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rb;
import com.yandex.metrica.impl.ob.rc;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.xt;
import com.yandex.metrica.impl.ob.yd;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class BirthDateAttribute {

    /* renamed from: a */
    private final qt f3049a = new qt("appmetrica_birth_date", new yd(), new rb());

    BirthDateAttribute() {
    }

    public UserProfileUpdate<? extends rf> withBirthDate(int year) {
        return mo2708a(m4595a(year), "yyyy", (qo) new qq(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(int year) {
        return mo2708a(m4595a(year), "yyyy", (qo) new ra(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDate(int year, int month) {
        return mo2708a(m4596a(year, month), "yyyy-MM", (qo) new qq(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(int year, int month) {
        return mo2708a(m4596a(year, month), "yyyy-MM", (qo) new ra(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDate(int year, int month, int dayOfMonth) {
        return mo2708a(m4597a(year, month, dayOfMonth), "yyyy-MM-dd", (qo) new qq(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(int year, int month, int dayOfMonth) {
        return mo2708a(m4597a(year, month, dayOfMonth), "yyyy-MM-dd", (qo) new ra(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withAge(int age) {
        return mo2708a(m4595a(Calendar.getInstance(Locale.US).get(1) - age), "yyyy", (qo) new qq(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withAgeIfUndefined(int age) {
        return mo2708a(m4595a(Calendar.getInstance(Locale.US).get(1) - age), "yyyy", (qo) new ra(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDate(@NonNull Calendar date) {
        return mo2708a(date, "yyyy-MM-dd", (qo) new qq(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withBirthDateIfUndefined(@NonNull Calendar date) {
        return mo2708a(date, "yyyy-MM-dd", (qo) new ra(this.f3049a.mo1754b()));
    }

    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(0, this.f3049a.mo1753a(), new yd(), new rb()));
    }

    /* renamed from: a */
    private Calendar m4595a(int i) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        return gregorianCalendar;
    }

    /* renamed from: a */
    private Calendar m4596a(int i, int i2) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        gregorianCalendar.set(2, i2 - 1);
        gregorianCalendar.set(5, 1);
        return gregorianCalendar;
    }

    /* renamed from: a */
    private Calendar m4597a(int i, int i2, int i3) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(1, i);
        gregorianCalendar.set(2, i2 - 1);
        gregorianCalendar.set(5, i3);
        return gregorianCalendar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @SuppressLint({"SimpleDateFormat"})
    /* renamed from: a */
    public UserProfileUpdate<? extends rf> mo2708a(@NonNull Calendar calendar, @NonNull String str, @NonNull qo qoVar) {
        return new UserProfileUpdate<>(new rc(this.f3049a.mo1753a(), new SimpleDateFormat(str).format(calendar.getTime()), new xt(), new yd(), qoVar));
    }
}
