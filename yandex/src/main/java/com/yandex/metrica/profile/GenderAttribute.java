package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rb;
import com.yandex.metrica.impl.ob.rc;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.xt;
import com.yandex.metrica.impl.ob.yd;

public class GenderAttribute {

    /* renamed from: a */
    private final qt f3052a = new qt("appmetrica_gender", new yd(), new rb());

    public enum Gender {
        MALE("M"),
        FEMALE("F"),
        OTHER("O");
        
        private final String mStringValue;

        private Gender(String stringValue) {
            this.mStringValue = stringValue;
        }

        public String getStringValue() {
            return this.mStringValue;
        }
    }

    GenderAttribute() {
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(@NonNull Gender value) {
        return new UserProfileUpdate<>(new rc(this.f3052a.mo1753a(), value.getStringValue(), new xt(), this.f3052a.mo1755c(), new qq(this.f3052a.mo1754b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(@NonNull Gender value) {
        return new UserProfileUpdate<>(new rc(this.f3052a.mo1753a(), value.getStringValue(), new xt(), this.f3052a.mo1755c(), new ra(this.f3052a.mo1754b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(0, this.f3052a.mo1753a(), this.f3052a.mo1755c(), this.f3052a.mo1754b()));
    }
}
