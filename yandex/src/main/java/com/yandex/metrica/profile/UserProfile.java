package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rf;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class UserProfile {
    @NonNull

    /* renamed from: a */
    private final List<UserProfileUpdate<? extends rf>> f3056a;

    public static class Builder {

        /* renamed from: a */
        private final LinkedList<UserProfileUpdate<? extends rf>> f3057a = new LinkedList<>();

        Builder() {
        }

        public Builder apply(@NonNull UserProfileUpdate<? extends rf> userProfileUpdate) {
            this.f3057a.add(userProfileUpdate);
            return this;
        }

        @NonNull
        public UserProfile build() {
            return new UserProfile(this.f3057a);
        }
    }

    private UserProfile(@NonNull List<UserProfileUpdate<? extends rf>> userProfileUpdates) {
        this.f3056a = Collections.unmodifiableList(userProfileUpdates);
    }

    @NonNull
    public List<UserProfileUpdate<? extends rf>> getUserProfileUpdates() {
        return this.f3056a;
    }

    @NonNull
    public static Builder newBuilder() {
        return new Builder();
    }
}
