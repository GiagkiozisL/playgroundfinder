package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rc;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.yb;
import com.yandex.metrica.impl.ob.yk;

public class StringAttribute {

    /* renamed from: a */
    private final yb<String> f3054a;

    /* renamed from: B */
    private final qt f3055b;

    StringAttribute(@NonNull String key, @NonNull yb<String> trimmer, @NonNull yk<String> keyValidator, @NonNull qn saver) {
        this.f3055b = new qt(key, keyValidator, saver);
        this.f3054a = trimmer;
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(@NonNull String value) {
        return new UserProfileUpdate<>(new rc(this.f3055b.mo1753a(), value, this.f3054a, this.f3055b.mo1755c(), new qq(this.f3055b.mo1754b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(@NonNull String value) {
        return new UserProfileUpdate<>(new rc(this.f3055b.mo1753a(), value, this.f3054a, this.f3055b.mo1755c(), new ra(this.f3055b.mo1754b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(0, this.f3055b.mo1753a(), this.f3055b.mo1755c(), this.f3055b.mo1754b()));
    }
}
