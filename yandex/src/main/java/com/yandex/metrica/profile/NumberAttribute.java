package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qu;
import com.yandex.metrica.impl.ob.qv;
import com.yandex.metrica.impl.ob.qx;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.xs;
import com.yandex.metrica.impl.ob.yk;

public final class NumberAttribute {

    /* renamed from: a */
    private final qt f3053a;

    NumberAttribute(@NonNull String key, @NonNull yk<String> keyValidator, @NonNull qn saver) {
        this.f3053a = new qt(key, keyValidator, saver);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(double value) {
        return new UserProfileUpdate<>(new qx(this.f3053a.mo1753a(), value, new qu(), new qq(new qv(new xs(100)))));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(double value) {
        return new UserProfileUpdate<>(new qx(this.f3053a.mo1753a(), value, new qu(), new ra(new qv(new xs(100)))));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(1, this.f3053a.mo1753a(), new qu(), new qv(new xs(100))));
    }
}
