package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.qn;
import com.yandex.metrica.impl.ob.qp;
import com.yandex.metrica.impl.ob.qq;
import com.yandex.metrica.impl.ob.qt;
import com.yandex.metrica.impl.ob.qz;
import com.yandex.metrica.impl.ob.ra;
import com.yandex.metrica.impl.ob.rf;
import com.yandex.metrica.impl.ob.yk;

public class BooleanAttribute {

    /* renamed from: a */
    private final qt f3050a;

    BooleanAttribute(@NonNull String key, @NonNull yk<String> keyValidator, @NonNull qn saver) {
        this.f3050a = new qt(key, keyValidator, saver);
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValue(boolean value) {
        return new UserProfileUpdate<>(new qp(this.f3050a.mo1753a(), value, this.f3050a.mo1755c(), new qq(this.f3050a.mo1754b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueIfUndefined(boolean value) {
        return new UserProfileUpdate<>(new qp(this.f3050a.mo1753a(), value, this.f3050a.mo1755c(), new ra(this.f3050a.mo1754b())));
    }

    @NonNull
    public UserProfileUpdate<? extends rf> withValueReset() {
        return new UserProfileUpdate<>(new qz(3, this.f3050a.mo1753a(), this.f3050a.mo1755c(), this.f3050a.mo1754b()));
    }
}
