package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rf;

public class UserProfileUpdate<T extends rf> {
    @NonNull

    /* renamed from: a */
    final T f3058a;

    UserProfileUpdate(@NonNull T userProfileUpdatePatcher) {
        this.f3058a = userProfileUpdatePatcher;
    }

    @NonNull
    public T getUserProfileUpdatePatcher() {
        return this.f3058a;
    }
}
