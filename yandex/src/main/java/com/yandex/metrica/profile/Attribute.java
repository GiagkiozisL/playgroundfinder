package com.yandex.metrica.profile;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.qu;
import com.yandex.metrica.impl.ob.qv;
import com.yandex.metrica.impl.ob.xs;
import com.yandex.metrica.impl.ob.xz;

public class Attribute {
    @NonNull
    public static StringAttribute customString(@NonNull String key) {
        return new StringAttribute(key, new xz(200, "String attribute \"" + key + "\""), new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static NumberAttribute customNumber(@NonNull String key) {
        return new NumberAttribute(key, new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static BooleanAttribute customBoolean(@NonNull String key) {
        return new BooleanAttribute(key, new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static CounterAttribute customCounter(@NonNull String key) {
        return new CounterAttribute(key, new qu(), new qv(new xs(100)));
    }

    @NonNull
    public static GenderAttribute gender() {
        return new GenderAttribute();
    }

    @NonNull
    public static BirthDateAttribute birthDate() {
        return new BirthDateAttribute();
    }

    @NonNull
    public static NotificationsEnabledAttribute notificationsEnabled() {
        return new NotificationsEnabledAttribute();
    }

    @NonNull
    public static NameAttribute name() {
        return new NameAttribute();
    }
}
