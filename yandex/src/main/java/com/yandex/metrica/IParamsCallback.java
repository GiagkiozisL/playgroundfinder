package com.yandex.metrica;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.cx;

import java.util.Map;

public interface IParamsCallback {
    public static final String APP_METRICA_DEVICE_ID_HASH = "appmetrica_device_id_hash";
    public static final String YANDEX_MOBILE_METRICA_CLIDS = "yandex_mobile_metrica_clids";
    public static final String YANDEX_MOBILE_METRICA_DEVICE_ID = "yandex_mobile_metrica_device_id";
    public static final String YANDEX_MOBILE_METRICA_GET_AD_URL = "yandex_mobile_metrica_get_ad_url";
    public static final String YANDEX_MOBILE_METRICA_REPORT_AD_URL = "yandex_mobile_metrica_report_ad_url";
    public static final String YANDEX_MOBILE_METRICA_UUID = "yandex_mobile_metrica_uuid";

    public enum Reason {
        UNKNOWN,
        NETWORK,
        INVALID_RESPONSE,
        INCONSISTENT_CLIDS
    }

    public static class Result {
        @Nullable
        private final Map<String, String> clids;
        @Nullable
        private final String deviceId;
        @Nullable
        private final String deviceIdHash;
        @Nullable
        private final String getUrl;
        @Nullable
        private final String reportUrl;
        @Nullable
        private final String uuid;

        public Result(@Nullable String deviceId2, @Nullable String uuid2, @Nullable String reportUrl2, @Nullable String getUrl2, @Nullable String deviceIdHash2, @Nullable Map<String, String> clids2) {
            this.deviceId = deviceId2;
            this.uuid = uuid2;
            this.reportUrl = reportUrl2;
            this.getUrl = getUrl2;
            this.deviceIdHash = deviceIdHash2;
            this.clids = clids2;
        }

        @Nullable
        public String getDeviceId() {
            return this.deviceId;
        }

        @Nullable
        public String getUuid() {
            return this.uuid;
        }

        @Nullable
        public String getReportUrl() {
            return this.reportUrl;
        }

        @Nullable
        public String getGetUrl() {
            return this.getUrl;
        }

        @Nullable
        public String getDeviceIdHash() {
            return this.deviceIdHash;
        }

        @Nullable
        public Map<String, String> getClids() {
            return this.clids;
        }

        public boolean hasDeviceId() {
            return !TextUtils.isEmpty(this.deviceId);
        }

        public boolean hasUuid() {
            return !TextUtils.isEmpty(this.uuid);
        }

        public boolean hasReportUrl() {
            return !TextUtils.isEmpty(this.reportUrl);
        }

        public boolean hasGetUrl() {
            return !TextUtils.isEmpty(this.getUrl);
        }

        public boolean hasDeviceIdHash() {
            return !TextUtils.isEmpty(this.deviceIdHash);
        }

        public boolean hasClids() {
            return !cx.m1193a((Map) this.clids);
        }
    }

    void onReceive(Result result);

    void onRequestError(Reason reason, Result result);
}
