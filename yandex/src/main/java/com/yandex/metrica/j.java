package com.yandex.metrica;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.cx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.j */
public class j extends YandexMetricaConfig {
    @Nullable

    /* renamed from: a */
    public final String a;
    @Nullable

    /* renamed from: B */
    public final Map<String, String> b;
    @Nullable

    /* renamed from: a */
    public final String c;
    @Nullable

    /* renamed from: d */
    public final List<String> d;
    @Nullable

    /* renamed from: e */
    public final Integer e;
    @Nullable

    /* renamed from: f */
    public final Integer f;
    @Nullable

    /* renamed from: g */
    public final Integer g;
    @Nullable

    /* renamed from: h */
    public final Map<String, String> h;
    @Nullable

    /* renamed from: i */
    public final Map<String, String> i;
    @Nullable

    /* renamed from: j */
    public final Boolean j;
    @Nullable

    /* renamed from: k */
    public final Boolean k;
    @Nullable

    /* renamed from: l */
    public final Boolean l;
    @Nullable

    /* renamed from: m */
    public final com.yandex.metrica.e m;
    @Nullable

    /* renamed from: n */
    public final c n;

    /* renamed from: com.yandex.metrica.j$a */
    public static final class C1231a {
        @Nullable

        /* renamed from: a */
        public String a;
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: B */
        public Builder b;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a */
        public String c;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: d */
        public List<String> d;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: e */
        public Integer e;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: f */
        public Map<String, String> f;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: g */
        public Integer g;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: h */
        public Integer h;
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: i */
        public LinkedHashMap<String, String> i = new LinkedHashMap<>();
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: j */
        public LinkedHashMap<String, String> j = new LinkedHashMap<>();
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: k */
        public Boolean k;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: l */
        public Boolean l;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: m */
        public com.yandex.metrica.e m;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: n */
        public Boolean n;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: o */
        public c o;

        protected C1231a(@NonNull String str) {
            this.b = YandexMetricaConfig.newConfigBuilder(str);
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2688a(@NonNull String str) {
            this.b.withAppVersion(str);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2683a(int i) {
            this.b.withSessionTimeout(i);
            return this;
        }

        @NonNull
        /* renamed from: B */
        public C1231a mo2694b(@Nullable String str) {
            this.a = str;
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2692a(boolean z) {
            this.b.withCrashReporting(z);
            return this;
        }

        @NonNull
        /* renamed from: B */
        public C1231a mo2696b(boolean z) {
            this.b.withNativeCrashReporting(z);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2700c(boolean z) {
            this.n = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2682a() {
            this.b.withLogs();
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2684a(@Nullable Location location) {
            this.b.withLocation(location);
            return this;
        }

        @NonNull
        /* renamed from: d */
        public C1231a mo2702d(boolean z) {
            this.b.withLocationTracking(z);
            return this;
        }

        @NonNull
        /* renamed from: e */
        public C1231a mo2704e(boolean z) {
            this.b.withInstalledAppCollecting(z);
            return this;
        }

        @NonNull
        /* renamed from: f */
        public C1231a mo2705f(boolean z) {
            this.b.withStatisticsSending(z);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2689a(@NonNull String str, @Nullable String str2) {
            this.i.put(str, str2);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2699c(@Nullable String str) {
            this.c = str;
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2690a(@Nullable List<String> list) {
            this.d = list;
            return this;
        }

        @NonNull
        /* renamed from: B */
        public C1231a mo2693b(int i) {
            if (i < 0) {
                throw new IllegalArgumentException(String.format(Locale.US, "Invalid %1$s. %1$s should be positive.", new Object[]{"App Build Number"}));
            }
            this.e = Integer.valueOf(i);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2691a(@Nullable Map<String, String> map, @Nullable Boolean bool) {
            this.k = bool;
            this.f = map;
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2698c(int i) {
            this.h = Integer.valueOf(i);
            return this;
        }

        @NonNull
        /* renamed from: d */
        public C1231a mo2701d(int i) {
            this.g = Integer.valueOf(i);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2685a(@Nullable PreloadInfo preloadInfo) {
            this.b.withPreloadInfo(preloadInfo);
            return this;
        }

        @NonNull
        /* renamed from: g */
        public C1231a mo2706g(boolean z) {
            this.b.handleFirstActivationAsUpdate(z);
            return this;
        }

        @NonNull
        /* renamed from: B */
        public C1231a mo2695b(@NonNull String str, @Nullable String str2) {
            this.j.put(str, str2);
            return this;
        }

        @NonNull
        /* renamed from: h */
        public C1231a mo2707h(boolean z) {
            this.l = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2687a(@NonNull com.yandex.metrica.e eVar) {
            this.m = eVar;
            return this;
        }

        @NonNull
        /* renamed from: e */
        public C1231a mo2703e(int i) {
            this.b.withMaxReportsInDatabaseCount(i);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C1231a mo2686a(@Nullable c cVar) {
            this.o = cVar;
            return this;
        }

        @NonNull
        /* renamed from: B */
        public com.yandex.metrica.j mo2697b() {
            return new j(this);
        }
    }

    public j(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        super(yandexMetricaConfig);
        this.a = null;
        this.b = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.c = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.d = null;
        this.h = null;
        this.m = null;
        this.l = null;
        this.n = null;
    }

    @NonNull
    /* renamed from: a */
    public static com.yandex.metrica.j m4546a(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        if (yandexMetricaConfig instanceof com.yandex.metrica.j) {
            return (com.yandex.metrica.j) yandexMetricaConfig;
        }
        return new j(yandexMetricaConfig);
    }

    @NonNull
    /* renamed from: a */
    public static C1231a m4545a(@NonNull String str) {
        return new C1231a(str);
    }

    @NonNull
    /* renamed from: a */
    public static C1231a m4544a(@NonNull com.yandex.metrica.j jVar) {
        C1231a a = m4548b(jVar).mo2690a((List<String>) new ArrayList<String>());
        if (cx.m1189a((Object) jVar.a)) {
            a.mo2699c(jVar.a);
        }
        if (cx.m1189a((Object) jVar.b) && cx.m1189a((Object) jVar.j)) {
            a.mo2691a(jVar.b, jVar.j);
        }
        if (cx.m1189a((Object) jVar.e)) {
            a.mo2693b(jVar.e.intValue());
        }
        if (cx.m1189a((Object) jVar.f)) {
            a.mo2701d(jVar.f.intValue());
        }
        if (cx.m1189a((Object) jVar.g)) {
            a.mo2698c(jVar.g.intValue());
        }
        if (cx.m1189a((Object) jVar.c)) {
            a.mo2694b(jVar.c);
        }
        if (cx.m1189a((Object) jVar.i)) {
            for (Entry entry : jVar.i.entrySet()) {
                a.mo2695b((String) entry.getKey(), (String) entry.getValue());
            }
        }
        if (cx.m1189a((Object) jVar.k)) {
            a.mo2707h(jVar.k.booleanValue());
        }
        if (cx.m1189a((Object) jVar.d)) {
            a.mo2690a(jVar.d);
        }
        if (cx.m1189a((Object) jVar.h)) {
            for (Entry entry2 : jVar.h.entrySet()) {
                a.mo2689a((String) entry2.getKey(), (String) entry2.getValue());
            }
        }
        if (cx.m1189a((Object) jVar.m)) {
            a.mo2687a(jVar.m);
        }
        if (cx.m1189a((Object) jVar.l)) {
            a.mo2700c(jVar.l.booleanValue());
        }
        return a;
    }

    @NonNull
    /* renamed from: B */
    public static C1231a m4548b(@NonNull YandexMetricaConfig yandexMetricaConfig) {
        C1231a a = m4545a(yandexMetricaConfig.apiKey);
        if (cx.m1189a((Object) yandexMetricaConfig.appVersion)) {
            a.mo2688a(yandexMetricaConfig.appVersion);
        }
        if (cx.m1189a((Object) yandexMetricaConfig.sessionTimeout)) {
            a.mo2683a(yandexMetricaConfig.sessionTimeout.intValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.crashReporting)) {
            a.mo2692a(yandexMetricaConfig.crashReporting.booleanValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.nativeCrashReporting)) {
            a.mo2696b(yandexMetricaConfig.nativeCrashReporting.booleanValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.location)) {
            a.mo2684a(yandexMetricaConfig.location);
        }
        if (cx.m1189a((Object) yandexMetricaConfig.locationTracking)) {
            a.mo2702d(yandexMetricaConfig.locationTracking.booleanValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.installedAppCollecting)) {
            a.mo2704e(yandexMetricaConfig.installedAppCollecting.booleanValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.logs) && yandexMetricaConfig.logs.booleanValue()) {
            a.mo2682a();
        }
        if (cx.m1189a((Object) yandexMetricaConfig.preloadInfo)) {
            a.mo2685a(yandexMetricaConfig.preloadInfo);
        }
        if (cx.m1189a((Object) yandexMetricaConfig.firstActivationAsUpdate)) {
            a.mo2706g(yandexMetricaConfig.firstActivationAsUpdate.booleanValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.statisticsSending)) {
            a.mo2705f(yandexMetricaConfig.statisticsSending.booleanValue());
        }
        if (cx.m1189a((Object) yandexMetricaConfig.maxReportsInDatabaseCount)) {
            a.mo2703e(yandexMetricaConfig.maxReportsInDatabaseCount.intValue());
        }
        m4547a(yandexMetricaConfig, a);
        return a;
    }

    /* renamed from: a */
    private static void m4547a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull C1231a aVar) {
        if (yandexMetricaConfig instanceof com.yandex.metrica.j) {
            com.yandex.metrica.j jVar = (com.yandex.metrica.j) yandexMetricaConfig;
            if (cx.m1189a((Object) jVar.d)) {
                aVar.mo2690a(jVar.d);
            }
            if (cx.m1189a((Object) jVar.n)) {
                aVar.mo2686a(jVar.n);
            }
        }
    }

    private j(@NonNull C1231a var1) {
        super(var1.b);
        this.e = var1.e;
        List var2 = var1.d;
        this.d = var2 == null ? null : Collections.unmodifiableList(var2);
        this.a = var1.c;
        Map var3 = var1.f;
        this.b = var3 == null ? null : Collections.unmodifiableMap(var3);
        this.g = var1.h;
        this.f = var1.g;
        this.c = var1.a;
        this.h = var1.i == null ? null : Collections.unmodifiableMap(var1.i);
        this.i = var1.j == null ? null : Collections.unmodifiableMap(var1.j);
        this.j = var1.k;
        this.k = var1.l;
        this.m = var1.m;
        this.l = var1.n;
        this.n = var1.o;
    }
}
