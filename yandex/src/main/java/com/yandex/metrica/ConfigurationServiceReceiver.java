package com.yandex.metrica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.jl;

import me.android.ydx.Constant;

public class ConfigurationServiceReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }
        String action = intent.getAction();
        Log.d(Constant.RUS_TAG, "ConfigurationServiceReceiver-" + "onReceive, with action:" + action);
        if ("com.yandex.metrica.configuration.service.PLC".equals(action) && cx.a(26)) {
            new jl(context).mo1109b(intent.getExtras());
        }
    }
}
