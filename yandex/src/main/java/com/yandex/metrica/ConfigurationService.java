package com.yandex.metrica;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.de;
import com.yandex.metrica.impl.ob.df;
import com.yandex.metrica.impl.ob.jn;
import com.yandex.metrica.impl.ob.jr;
import com.yandex.metrica.impl.ob.jv;
import com.yandex.metrica.impl.ob.jy;
import com.yandex.metrica.impl.ob.jz;
import com.yandex.metrica.impl.ob.ka;

import java.util.HashMap;
import java.util.Map;

import me.android.ydx.Constant;

public class ConfigurationService extends Service {
    @NonNull

    /* renamed from: a */
    private Map<String, jy> f19a = new HashMap();

    /* renamed from: B */
    private jr f20b;
    @Nullable

    /* renamed from: a */
    private String f21c;

    public void onCreate() {
        super.onCreate();
        Log.d(Constant.RUS_TAG, "ConfigurationService-" + "onCreate");
        al.m325a(getApplicationContext());
//        this.f21c = String.format("[ConfigurationService:%s]", new Object[]{getPackageName()});
        this.f20b = new jr();
        Context applicationContext = getApplicationContext();
        jv jvVar = new jv(applicationContext, this.f20b.mo1119a(), new jn(applicationContext));
        de deVar = null;
        if (cx.a(21)) {
            deVar = new de(applicationContext, new df(applicationContext));
        }
        this.f19a.put("com.yandex.metrica.configuration.ACTION_INIT", new ka(getApplicationContext(), jvVar, deVar));
        this.f19a.put("com.yandex.metrica.configuration.ACTION_SCHEDULED_START", new jz(getApplicationContext(), jvVar));
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle bundle = null;
        jy jyVar = (jy) this.f19a.get(intent == null ? null : intent.getAction());
        Log.d(Constant.RUS_TAG, "ConfigurationService-" + "onStartCommand");
        if (intent != null) {
            Log.d(Constant.RUS_TAG, "ConfigurationService-" + "action-> " + intent.getAction());
        }
        if (jyVar != null) {
            jr jrVar = this.f20b;
            if (intent != null) {
                bundle = intent.getExtras();
            }
            jrVar.mo1120a(jyVar, bundle);
        }
        return Service.START_NOT_STICKY;
    }

    public IBinder onBind(Intent intent) {
        Log.d(Constant.RUS_TAG, "ConfigurationService-" + "onBind");
        return null;
    }
}
