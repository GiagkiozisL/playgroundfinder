package com.yandex.metrica;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.xh;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: com.yandex.metrica.a */
public class a {
    @NonNull

    /* renamed from: a */
    private final xh f74a;

    /* renamed from: B */
    private final long f75b;

    /* renamed from: a */
    private final Set<C0020a> c;

    /* renamed from: d */
    private final Runnable f77d;

    /* renamed from: e */
    private boolean f78e;

    /* renamed from: com.yandex.metrica.a$a */
    public interface C0020a {
        /* renamed from: a */
        void a();

        /* renamed from: B */
        void mo145b();
    }

    public a(long j) {
        this(j, db.k().b());
    }

    a(long j, @NonNull xh xhVar) {
        this.c = new HashSet();
        this.f77d = new Runnable() {
            public void run() {
                a.this.d();
            }
        };
        this.f78e = true;
        this.f74a = xhVar;
        this.f75b = j;
    }

    /* renamed from: a */
    public void mo141a() {
        if (this.f78e) {
            this.f78e = false;
            this.f74a.b(this.f77d);
            c();
        }
    }

    /* renamed from: B */
    public void mo142b() {
        if (!this.f78e) {
            this.f78e = true;
            this.f74a.a(this.f77d, this.f75b);
        }
    }

    /* renamed from: a */
//    private void m112c() {
//        for (C0020a a : new HashSet(this.a)) {
//            a.a();
//        }
//    }
//
//    /* access modifiers changed from: private */
//    /* renamed from: d */
//    public void m113d() {
//        for (C0020a b : new HashSet(this.a)) {
//            b.mo145b();
//        }
//    }

    private void c() {
        HashSet var1 = new HashSet(this.c);
        Iterator var2 = var1.iterator();

        while(var2.hasNext()) {
            com.yandex.metrica.a.C0020a var3 = (com.yandex.metrica.a.C0020a)var2.next();
            var3.a();
        }
    }

    private void d() {
        HashSet var1 = new HashSet(this.c);
        Iterator var2 = var1.iterator();

        while(var2.hasNext()) {
            com.yandex.metrica.a.C0020a var3 = (com.yandex.metrica.a.C0020a)var2.next();
            var3.mo145b();
        }

    }
}
