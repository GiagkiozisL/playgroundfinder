package com.yandex.metrica;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.impl.ob.da;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.p */
public final class p {

    /* renamed from: a */
    private static final List<String> f3048a = Arrays.asList(new String[]{"yandex_mobile_metrica_uuid", "yandex_mobile_metrica_device_id", "appmetrica_device_id_hash", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url"});

    @Deprecated
    /* renamed from: a */
    public static void m4592a(IIdentifierCallback callback) {
        m4593a(callback, f3048a);
    }

    @Deprecated
    /* renamed from: a */
    public static void m4593a(IIdentifierCallback callback, @NonNull List<String> identifiers) {
        da.m1261a(callback, (List<String>) new ArrayList<String>(identifiers));
    }

    /* renamed from: a */
    public static void m4590a(Context context, IIdentifierCallback callback, @NonNull List<String> identifiers) {
        da.m1259a(context, callback, new ArrayList(identifiers));
    }

    /* renamed from: a */
    public static void m4591a(Context context, IIdentifierCallback callback, @NonNull String... identifiers) {
        m4590a(context, callback, Arrays.asList(identifiers));
    }

    /* renamed from: a */
    public static void m4589a(Context context, IIdentifierCallback callback) {
        m4590a(context, callback, f3048a);
    }

    @NonNull
    /* renamed from: u */
    public static String m4594u(@NonNull String sdkName) {
        return da.m1257a(sdkName);
    }

    @Nullable
    public static Boolean plat() {
        try {
            return (Boolean) da.m1269d().get();
        } catch (Throwable th) {
            return null;
        }
    }

    public static boolean iifa() {
        return da.m1262a();
    }

    @Nullable
    public static String pgai() {
        try {
            return (String) da.m1264b().get();
        } catch (Throwable th) {
            return null;
        }
    }

    @NonNull
    public static DeviceInfo gdi(@NonNull Context context) {
        return da.m1255a(context);
    }

    @NonNull
    public static String gcni(@NonNull Context context) {
        return da.m1263b(context);
    }

    @Nullable
    @Deprecated
    public static String guid() {
        return da.m1267c();
    }

    @Nullable
    public static String guid(@NonNull Context context) {
        return da.m1268d(context);
    }

    @Nullable
    public static String gdid(@NonNull Context context) {
        return da.m1270e(context);
    }

    @NonNull
    public static String mpn(Context context) {
        return da.m1272f(context);
    }

    public static void rce(int type, String name, String value, Map<String, String> environment) {
        da.m1258a(type, name, value, environment);
    }

    @NonNull
    public static String gmsvn(int apiLevel) {
        return da.m1256a(apiLevel);
    }

    public static void seb() {
        da.m1271e();
    }

    @Deprecated
    @NonNull
    public static YandexMetricaConfig cpcwh(YandexMetricaConfig config, String h) {
        return da.m1253a(config, h);
    }

    @Deprecated
    public static void rolu(Context context, Object registrant) {
        da.m1260a(context, registrant);
    }

    @Deprecated
    public static void urolu(Context context, Object registrant) {
        da.m1265b(context, registrant);
    }

    @Nullable
    @Deprecated
    public static Location glkl(Context context) {
        return da.m1273g(context);
    }

    @Nullable
    @Deprecated
    public static Integer gbc(Context context) {
        return da.m1266c(context.getApplicationContext());
    }
}
