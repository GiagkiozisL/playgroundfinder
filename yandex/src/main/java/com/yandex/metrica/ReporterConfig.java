package com.yandex.metrica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;
import com.yandex.metrica.impl.ob.yl;

public class ReporterConfig {
    @NonNull
    public final String apiKey;
    @Nullable
    public final Boolean logs;
    @Nullable
    public final Integer maxReportsInDatabaseCount;
    @Nullable
    public final Integer sessionTimeout;
    @Nullable
    public final Boolean statisticsSending;

    public static class Builder {

        /* renamed from: a */
        private static final yk<String> f43a = new yg(new yl());
        /* access modifiers changed from: private */

        /* renamed from: B */
        public final String f44b;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a */
        public Integer f45c;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: d */
        public Boolean f46d;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: e */
        public Boolean f47e;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: f */
        public Integer f48f;

        Builder(@NonNull String apiKey) {
            f43a.a(apiKey);
            this.f44b = apiKey;
        }

        @NonNull
        public Builder withSessionTimeout(int sessionTimeout) {
            this.f45c = Integer.valueOf(sessionTimeout);
            return this;
        }

        @NonNull
        public Builder withLogs() {
            this.f46d = Boolean.valueOf(true);
            return this;
        }

        @NonNull
        public Builder withStatisticsSending(boolean enabled) {
            this.f47e = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withMaxReportsInDatabaseCount(int maxReportsInDatabaseCount) {
            this.f48f = Integer.valueOf(maxReportsInDatabaseCount);
            return this;
        }

        @NonNull
        public ReporterConfig build() {
            return new ReporterConfig(this);
        }
    }

    ReporterConfig(@NonNull Builder builder) {
        this.apiKey = builder.f44b;
        this.sessionTimeout = builder.f45c;
        this.logs = builder.f46d;
        this.statisticsSending = builder.f47e;
        this.maxReportsInDatabaseCount = builder.f48f;
    }

    ReporterConfig(@NonNull ReporterConfig config) {
        this.apiKey = config.apiKey;
        this.sessionTimeout = config.sessionTimeout;
        this.logs = config.logs;
        this.statisticsSending = config.statisticsSending;
        this.maxReportsInDatabaseCount = config.maxReportsInDatabaseCount;
    }

    public static Builder createBuilderFromConfig(@NonNull ReporterConfig config) {
        Builder newConfigBuilder = newConfigBuilder(config.apiKey);
        if (cx.m1189a((Object) config.sessionTimeout)) {
            newConfigBuilder.withSessionTimeout(config.sessionTimeout.intValue());
        }
        if (cx.m1189a((Object) config.logs) && config.logs.booleanValue()) {
            newConfigBuilder.withLogs();
        }
        if (cx.m1189a((Object) config.statisticsSending)) {
            newConfigBuilder.withStatisticsSending(config.statisticsSending.booleanValue());
        }
        if (cx.m1189a((Object) config.maxReportsInDatabaseCount)) {
            newConfigBuilder.withMaxReportsInDatabaseCount(config.maxReportsInDatabaseCount.intValue());
        }
        return newConfigBuilder;
    }

    @NonNull
    public static Builder newConfigBuilder(@NonNull String apiKey2) {
        return new Builder(apiKey2);
    }
}
