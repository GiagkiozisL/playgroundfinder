package com.yandex.metrica;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;

import me.android.ydx.Constant;

public interface IMetricaService extends IInterface {

    /* renamed from: com.yandex.metrica.IMetricaService$a */
    public static abstract class C0009a extends Binder implements IMetricaService {

        /* renamed from: com.yandex.metrica.IMetricaService$a$a */
        private static class C0010a implements IMetricaService {

            /* renamed from: a */
            private IBinder f31a;

            C0010a(IBinder iBinder) {
                this.f31a = iBinder;
            }

            public IBinder asBinder() {
                return this.f31a;
            }

            /* renamed from: a */
            public void mo68a(String str, int i, String str2, Bundle bundle) throws RemoteException {
                Log.d(Constant.RUS_TAG, "IMetricaService$m68a");
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.yandex.metrica.IMetricaService");
                    obtain.writeString(str);
                    obtain.writeInt(i);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f31a.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            /* renamed from: a */
            public void mo67a(Bundle bundle) throws RemoteException {
                Log.d(Constant.RUS_TAG, "IMetricaService$mo67a");
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.yandex.metrica.IMetricaService");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f31a.transact(2, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }

        public C0009a() {
            Log.d(Constant.RUS_TAG, "IMetricaService$attachInterface");
            attachInterface(this, "com.yandex.metrica.IMetricaService");
        }

        /* renamed from: a */
        public static IMetricaService getMetricaServiceIF(IBinder iBinder) {
            Log.d(Constant.RUS_TAG, "IMetricaService$getMetricaServiceIF");
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.yandex.metrica.IMetricaService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IMetricaService)) {
                return new C0010a(iBinder);
            }
            return (IMetricaService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            Log.d(Constant.RUS_TAG, "IMetricaService$onTransact");
            Bundle bundle = null;
            String str = "com.yandex.metrica.IMetricaService";
            switch (code) {
                case 1:
                    data.enforceInterface(str);
                    String readString = data.readString();
                    int readInt = data.readInt();
                    String readString2 = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    mo68a(readString, readInt, readString2, bundle);
                    return true;
                case 2:
                    data.enforceInterface(str);
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    mo67a(bundle);
                    return true;
                case 1598968902:
                    reply.writeString(str);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    /* renamed from: a */
    void mo67a(Bundle bundle) throws RemoteException;

    /* renamed from: a */
    void mo68a(String str, int i, String str2, Bundle bundle) throws RemoteException;
}
