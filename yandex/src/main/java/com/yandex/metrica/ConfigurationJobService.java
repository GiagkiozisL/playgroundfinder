package com.yandex.metrica;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.app.job.JobWorkItem;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.SparseArray;

import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.de;
import com.yandex.metrica.impl.ob.dg;
import com.yandex.metrica.impl.ob.jl;
import com.yandex.metrica.impl.ob.jr;
import com.yandex.metrica.impl.ob.jv;
import com.yandex.metrica.impl.ob.jw;
import com.yandex.metrica.impl.ob.jx;
import com.yandex.metrica.impl.ob.jy;
import com.yandex.metrica.impl.ob.jz;
import com.yandex.metrica.impl.ob.ka;

import java.util.HashMap;
import java.util.Map;

@TargetApi(26)
public class ConfigurationJobService extends JobService {
    @NonNull

    /* renamed from: a */
    SparseArray<jy> f8a = new SparseArray<>();
    @NonNull

    /* renamed from: B */
    Map<String, jy> f9b = new HashMap();

    /* renamed from: a */
    private jr f10c;
    @Nullable

    /* renamed from: d */
    private String f11d;

    public void onCreate() {
        super.onCreate();
        al.m325a(getApplicationContext());
        Context applicationContext = getApplicationContext();
//        this.f11d = String.format(Locale.US, "[ConfigurationJobService:%s]", new Object[]{applicationContext.getPackageName()});
        this.f10c = new jr();
        jv jvVar = new jv(getApplicationContext(), this.f10c.mo1119a(), new jl(applicationContext));
        de deVar = new de(applicationContext, new dg(applicationContext));
        this.f8a.append(1512302345, new jz(getApplicationContext(), jvVar));
        this.f8a.append(1512302346, new ka(getApplicationContext(), jvVar, deVar));
        this.f9b.put("com.yandex.metrica.configuration.service.PLC", new jx(applicationContext, this.f10c.mo1119a()));
    }

    public boolean onStartJob(@Nullable JobParameters params) {
        if (params == null) {
            return false;
        }
        try {
            if (!complexJob(params.getJobId())) {
                return m10b(params);
            }
            m7a(params);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m7a(@NonNull final JobParameters jobParameters) {
        this.f10c.mo1119a().a((Runnable) new Runnable() {
            public void run() {
                ConfigurationJobService.this.m11c(jobParameters);
            }
        });
    }

    /* renamed from: B */
    private boolean m10b(@NonNull final JobParameters jobParameters) {
        jy jyVar = (jy) this.f8a.get(jobParameters.getJobId());
        if (jyVar == null) {
            return false;
        }
        this.f10c.mo1121a(jyVar, jobParameters.getTransientExtras(), new jw() {
            /* renamed from: a */
            public void a() {
                try {
                    ConfigurationJobService.this.jobFinished(jobParameters, false);
                } catch (Throwable th) {
                }
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m11c(@NonNull final JobParameters jobParameters) {
        while (true) {
            try {
                final JobWorkItem dequeueWork = jobParameters.dequeueWork();
                if (dequeueWork != null) {
                    Intent intent = dequeueWork.getIntent();
                    if (intent != null) {
                        jy jyVar = (jy) this.f9b.get(intent.getAction());
                        if (jyVar != null) {
                            this.f10c.mo1121a(jyVar, intent.getExtras(), new jw() {
                                /* renamed from: a */
                                public void a() {
                                    try {
                                        jobParameters.completeWork(dequeueWork);
                                        ConfigurationJobService.this.m7a(jobParameters);
                                    } catch (Throwable th) {
                                    }
                                }
                            });
                        } else {
                            jobParameters.completeWork(dequeueWork);
                        }
                    } else {
                        jobParameters.completeWork(dequeueWork);
                    }
                } else {
                    return;
                }
            } catch (Throwable th) {
                jobFinished(jobParameters, true);
                return;
            }
        }
    }

    public boolean complexJob(int jobId) {
        return jobId == 1512302347;
    }

    public boolean onStopJob(@Nullable JobParameters params) {
        if (params == null || !complexJob(params.getJobId())) {
            return false;
        }
        return true;
    }
}
