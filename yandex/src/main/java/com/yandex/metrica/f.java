package com.yandex.metrica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.cx;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.f */
public class f extends ReporterConfig {
    @Nullable

    /* renamed from: a */
    public final Integer f85a;
    @Nullable

    /* renamed from: B */
    public final Integer f86b;

    /* renamed from: a */
    public final Map<String, String> f87c;

    /* renamed from: com.yandex.metrica.f$a */
    public static class C0026a {

        /* renamed from: a */
        Builder f88a;

        /* renamed from: B */
        Integer f89b;

        /* renamed from: a */
        Integer f90c;

        /* renamed from: d */
        LinkedHashMap<String, String> f91d = new LinkedHashMap<>();

        public C0026a(String str) {
            this.f88a = ReporterConfig.newConfigBuilder(str);
        }

        @NonNull
        /* renamed from: a */
        public C0026a mo149a(int i) {
            this.f88a.withSessionTimeout(i);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C0026a mo148a() {
            this.f88a.withLogs();
            return this;
        }

        @NonNull
        /* renamed from: B */
        public C0026a mo152b(int i) {
            this.f89b = Integer.valueOf(i);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C0026a mo154c(int i) {
            this.f90c = Integer.valueOf(i);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C0026a mo150a(String str, String str2) {
            this.f91d.put(str, str2);
            return this;
        }

        @NonNull
        /* renamed from: a */
        public C0026a mo151a(boolean z) {
            this.f88a.withStatisticsSending(z);
            return this;
        }

        @NonNull
        /* renamed from: d */
        public C0026a mo155d(int i) {
            this.f88a.withMaxReportsInDatabaseCount(i);
            return this;
        }

        @NonNull
        /* renamed from: B */
        public f mo153b() {
            return new f(this);
        }
    }

    f(@NonNull C0026a aVar) {
        super(aVar.f88a);
        this.f86b = aVar.f89b;
        this.f85a = aVar.f90c;
        this.f87c = aVar.f91d == null ? null : Collections.unmodifiableMap(aVar.f91d);
    }

    private f(ReporterConfig reporterConfig) {
        super(reporterConfig);
        if (reporterConfig instanceof f) {
            f fVar = (f) reporterConfig;
            this.f85a = fVar.f85a;
            this.f86b = fVar.f86b;
            this.f87c = fVar.f87c;
            return;
        }
        this.f85a = null;
        this.f86b = null;
        this.f87c = null;
    }

    /* renamed from: a */
    public static f m122a(@NonNull ReporterConfig reporterConfig) {
        return new f(reporterConfig);
    }

    /* renamed from: a */
    public static C0026a m120a(@NonNull f fVar) {
        C0026a a = m121a(fVar.apiKey);
        if (cx.m1189a((Object) fVar.sessionTimeout)) {
            a.mo149a(fVar.sessionTimeout.intValue());
        }
        if (cx.m1189a((Object) fVar.logs) && fVar.logs.booleanValue()) {
            a.mo148a();
        }
        if (cx.m1189a((Object) fVar.statisticsSending)) {
            a.mo151a(fVar.statisticsSending.booleanValue());
        }
        if (cx.m1189a((Object) fVar.maxReportsInDatabaseCount)) {
            a.mo155d(fVar.maxReportsInDatabaseCount.intValue());
        }
        if (cx.m1189a((Object) fVar.f85a)) {
            a.mo154c(fVar.f85a.intValue());
        }
        if (cx.m1189a((Object) fVar.f86b)) {
            a.mo152b(fVar.f86b.intValue());
        }
        if (cx.m1189a((Object) fVar.f87c)) {
            for (Entry entry : fVar.f87c.entrySet()) {
                a.mo150a((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return a;
    }

    /* renamed from: a */
    public static C0026a m121a(@NonNull String str) {
        return new C0026a(str);
    }
}
