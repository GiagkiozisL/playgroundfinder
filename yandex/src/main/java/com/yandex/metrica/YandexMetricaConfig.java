package com.yandex.metrica;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.az;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;
import com.yandex.metrica.impl.ob.yl;

public class YandexMetricaConfig {
    @NonNull
    public final String apiKey;
    @Nullable
    public final String appVersion;
    @Nullable
    public final Boolean crashReporting;
    @Nullable
    public final Boolean firstActivationAsUpdate;
    @Nullable
    public final Boolean installedAppCollecting;
    @Nullable
    public final Location location;
    @Nullable
    public final Boolean locationTracking;
    @Nullable
    public final Boolean logs;
    @Nullable
    public final Integer maxReportsInDatabaseCount;
    @Nullable
    public final Boolean nativeCrashReporting;
    @Nullable
    public final PreloadInfo preloadInfo;
    @Nullable
    public final Integer sessionTimeout;
    @Nullable
    public final Boolean statisticsSending;

    public static class Builder {

        /* renamed from: a */
        private static final yk<String> f60a = new yg(new yl());
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: B */
        public final String f61b;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a */
        public String f62c;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: d */
        public Integer f63d;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: e */
        public Boolean f64e;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: f */
        public Boolean f65f;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: g */
        public Location f66g;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: h */
        public Boolean f67h;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: i */
        public Boolean f68i;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: j */
        public Boolean f69j;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: k */
        public PreloadInfo f70k;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: l */
        public Boolean f71l;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: m */
        public Boolean f72m;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: n */
        public Integer f73n;

        protected Builder(@NonNull String apiKey) {
            f60a.a(apiKey);
            this.f61b = apiKey;
        }

        @NonNull
        public Builder withAppVersion(@Nullable String appVersion) {
            this.f62c = appVersion;
            return this;
        }

        @NonNull
        public Builder withSessionTimeout(int sessionTimeout) {
            this.f63d = Integer.valueOf(sessionTimeout);
            return this;
        }

        @NonNull
        public Builder withCrashReporting(boolean enabled) {
            this.f64e = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withNativeCrashReporting(boolean enabled) {
            this.f65f = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withLogs() {
            this.f69j = Boolean.valueOf(true);
            return this;
        }

        @NonNull
        public Builder withLocation(@Nullable Location location) {
            this.f66g = location;
            return this;
        }

        @NonNull
        public Builder withLocationTracking(boolean enabled) {
            this.f67h = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withInstalledAppCollecting(boolean enabled) {
            this.f68i = Boolean.valueOf(enabled);
            return this;
        }

        @NonNull
        public Builder withPreloadInfo(@Nullable PreloadInfo preloadInfo) {
            this.f70k = preloadInfo;
            return this;
        }

        @NonNull
        public Builder handleFirstActivationAsUpdate(boolean value) {
            this.f71l = Boolean.valueOf(value);
            return this;
        }

        @NonNull
        public Builder withStatisticsSending(boolean value) {
            this.f72m = Boolean.valueOf(value);
            return this;
        }

        @NonNull
        public Builder withMaxReportsInDatabaseCount(int value) {
            this.f73n = Integer.valueOf(value);
            return this;
        }

        @NonNull
        public YandexMetricaConfig build() {
            return new YandexMetricaConfig(this);
        }
    }

    @NonNull
    public static Builder newConfigBuilder(@NonNull String apiKey2) {
        return new Builder(apiKey2);
    }

    public static YandexMetricaConfig fromJson(String json) {
        return new az().mo339a(json);
    }

    protected YandexMetricaConfig(@NonNull Builder builder) {
        this.apiKey = builder.f61b;
        this.appVersion = builder.f62c;
        this.sessionTimeout = builder.f63d;
        this.crashReporting = builder.f64e;
        this.nativeCrashReporting = builder.f65f;
        this.location = builder.f66g;
        this.locationTracking = builder.f67h;
        this.installedAppCollecting = builder.f68i;
        this.logs = builder.f69j;
        this.preloadInfo = builder.f70k;
        this.firstActivationAsUpdate = builder.f71l;
        this.statisticsSending = builder.f72m;
        this.maxReportsInDatabaseCount = builder.f73n;
    }

    protected YandexMetricaConfig(@NonNull YandexMetricaConfig source) {
        this.apiKey = source.apiKey;
        this.appVersion = source.appVersion;
        this.sessionTimeout = source.sessionTimeout;
        this.crashReporting = source.crashReporting;
        this.nativeCrashReporting = source.nativeCrashReporting;
        this.location = source.location;
        this.locationTracking = source.locationTracking;
        this.installedAppCollecting = source.installedAppCollecting;
        this.logs = source.logs;
        this.preloadInfo = source.preloadInfo;
        this.firstActivationAsUpdate = source.firstActivationAsUpdate;
        this.statisticsSending = source.statisticsSending;
        this.maxReportsInDatabaseCount = source.maxReportsInDatabaseCount;
    }

    @NonNull
    public static Builder createBuilderFromConfig(@NonNull YandexMetricaConfig source) {
        Builder newConfigBuilder = newConfigBuilder(source.apiKey);
        if (cx.m1189a((Object) source.appVersion)) {
            newConfigBuilder.withAppVersion(source.appVersion);
        }
        if (cx.m1189a((Object) source.sessionTimeout)) {
            newConfigBuilder.withSessionTimeout(source.sessionTimeout.intValue());
        }
        if (cx.m1189a((Object) source.crashReporting)) {
            newConfigBuilder.withCrashReporting(source.crashReporting.booleanValue());
        }
        if (cx.m1189a((Object) source.nativeCrashReporting)) {
            newConfigBuilder.withNativeCrashReporting(source.nativeCrashReporting.booleanValue());
        }
        if (cx.m1189a((Object) source.location)) {
            newConfigBuilder.withLocation(source.location);
        }
        if (cx.m1189a((Object) source.locationTracking)) {
            newConfigBuilder.withLocationTracking(source.locationTracking.booleanValue());
        }
        if (cx.m1189a((Object) source.installedAppCollecting)) {
            newConfigBuilder.withInstalledAppCollecting(source.installedAppCollecting.booleanValue());
        }
        if (cx.m1189a((Object) source.logs) && source.logs.booleanValue()) {
            newConfigBuilder.withLogs();
        }
        if (cx.m1189a((Object) source.preloadInfo)) {
            newConfigBuilder.withPreloadInfo(source.preloadInfo);
        }
        if (cx.m1189a((Object) source.firstActivationAsUpdate)) {
            newConfigBuilder.handleFirstActivationAsUpdate(source.firstActivationAsUpdate.booleanValue());
        }
        if (cx.m1189a((Object) source.statisticsSending)) {
            newConfigBuilder.withStatisticsSending(source.statisticsSending.booleanValue());
        }
        if (cx.m1189a((Object) source.maxReportsInDatabaseCount)) {
            newConfigBuilder.withMaxReportsInDatabaseCount(source.maxReportsInDatabaseCount.intValue());
        }
        return newConfigBuilder;
    }

    public String toJson() {
        return new az().mo340a(this);
    }
}
