package com.yandex.metrica.impl.ob;

import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.oa.C0697a;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: com.yandex.metrica.impl.ob.hf */
public class hf extends hd {
    @NonNull

    /* renamed from: a */
    private final MyPackageManager myPkgManager;

    public hf(en enVar) {
        this(enVar, new MyPackageManager());
    }

    @VisibleForTesting
    public hf(en enVar, @NonNull MyPackageManager myPackageManagerVar) {
        super(enVar);
        this.myPkgManager = myPackageManagerVar;
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        en a = mo973a();
        if (a.mo834u().mo1284d() && a.mo833t()) {
            lw y = a.mo838y();
            HashSet b = mo974b();
            try {
                ArrayList c = mo975c();
                if (vj.a(b, c)) {
                    a.mo826m();
                } else {
                    JSONArray jSONArray = new JSONArray();
                    Iterator it = c.iterator();
                    while (it.hasNext()) {
                        jSONArray.put(((ob) it.next()).mo1533a());
                    }
                    a.mo818e().mo888d(w.m4277a(wVar, new JSONObject().put("features", jSONArray).toString()));
                    y.mo1327b(jSONArray.toString());
                }
            } catch (Throwable th) {
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    /* renamed from: B */
    public HashSet<ob> mo974b() {
        String e = mo973a().mo838y().mo1338e();
        if (TextUtils.isEmpty(e)) {
            return null;
        }
        try {
            HashSet hashSet = new HashSet();
            JSONArray jSONArray = new JSONArray(e);
            for (int i = 0; i < jSONArray.length(); i++) {
                hashSet.add(new ob(jSONArray.getJSONObject(i)));
            }
            return hashSet;
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    /* renamed from: a */
    public ArrayList<ob> mo975c() {
        try {
            en a = mo973a();
            PackageInfo a2 = this.myPkgManager.getPackageInfo(a.mo824k(), a.mo824k().getPackageName(), 16384);
            ArrayList<ob> arrayList = new ArrayList<>();
            oa a3 = C0697a.m2646a();
            if (a2 == null || a2.reqFeatures == null) {
                return arrayList;
            }
            for (FeatureInfo b : a2.reqFeatures) {
                arrayList.add(a3.mo1531b(b));
            }
            return arrayList;
        } catch (Throwable th) {
            return null;
        }
    }
}
