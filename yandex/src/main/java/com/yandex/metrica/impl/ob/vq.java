package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.vq */
public class vq {

    /* renamed from: com.yandex.metrica.impl.ob.vq$a */
    public static class a extends JSONObject {
        public a() {
        }

        public a(String str) throws JSONException {
            super(str);
        }

        /* renamed from: a */
        public String a_(String str) {
            String str2 = "";
            if (!super.has(str)) {
                return str2;
            }
            try {
                return super.getString(str);
            } catch (Throwable th) {
                return str2;
            }
        }

        /* renamed from: B */
        public String mo2498b(String str) {
            return super.has(str) ? a_(str) : "";
        }

        /* renamed from: a */
        public Object mo2496a(String str, Object obj) {
            try {
                return super.get(str);
            } catch (Throwable th) {
                return obj;
            }
        }

        /* renamed from: a */
        public boolean c(String str) {
            try {
                return NULL != super.get(str);
            } catch (Throwable th) {
                return false;
            }
        }

        @Nullable
        /* renamed from: d */
        public Long mo2500d(String str) {
            try {
                return Long.valueOf(getLong(str));
            } catch (Throwable th) {
                return null;
            }
        }

        @Nullable
        /* renamed from: e */
        public Boolean mo2501e(String str) {
            try {
                return Boolean.valueOf(getBoolean(str));
            } catch (Throwable th) {
                return null;
            }
        }
    }

    @VisibleForTesting
    /* renamed from: a */
    public static Object a(Object var0) {
        if (var0 == null) {
            return null;
        } else {
            try {
                ArrayList var2;
                if (var0.getClass().isArray()) {
                    int var8 = Array.getLength(var0);
                    var2 = new ArrayList(var8);

                    for(int var10 = 0; var10 < var8; ++var10) {
                        var2.add(a(Array.get(var0, var10)));
                    }

                    return new JSONArray(var2);
                } else {
                    Iterator var3;
                    if (!(var0 instanceof Collection)) {
                        if (var0 instanceof Map) {
                            Map var7 = (Map)var0;
                            LinkedHashMap var9 = new LinkedHashMap();
                            var3 = var7.entrySet().iterator();

                            while(var3.hasNext()) {
                                Entry var11 = (Entry)var3.next();
                                String var5 = var11.getKey().toString();
                                if (var5 != null) {
                                    var9.put(var5, a(var11.getValue()));
                                }
                            }

                            return new JSONObject(var9);
                        } else {
                            return var0;
                        }
                    } else {
                        Collection var1 = (Collection)var0;
                        var2 = new ArrayList(var1.size());
                        var3 = var1.iterator();

                        while(var3.hasNext()) {
                            Object var4 = var3.next();
                            var2.add(a(var4));
                        }

                        return new JSONArray(var2);
                    }
                }
            } catch (Throwable var6) {
                return null;
            }
        }
//        if (obj == null) {
//            return null;
//        }
//        try {
//            if (obj.getClass().isArray()) {
//                int length = Array.getLength(obj);
//                ArrayList arrayList = new ArrayList(length);
//                for (int i = 0; i < length; i++) {
//                    arrayList.add(aaa(Array.get(obj, i)));
//                }
//                return new JSONArray(arrayList);
//            } else if (obj instanceof Collection) {
//                Collection<Object> collection = (Collection) obj;
//                ArrayList arrayList2 = new ArrayList(collection.size());
//                for (Object aaa : collection) {
//                    arrayList2.add(aaa(aaa));
//                }
//                return new JSONArray(arrayList2);
//            } else if (!(obj instanceof Map)) {
//                return obj;
//            } else {
//                Map map = (Map) obj;
//                LinkedHashMap linkedHashMap = new LinkedHashMap();
//                for (Entry entry : map.entrySet()) {
//                    String obj2 = entry.getKey().toString();
//                    if (obj2 != null) {
//                        linkedHashMap.put(obj2, aaa(entry.getValue()));
//                    }
//                }
//                return new JSONObject(linkedHashMap);
//            }
//        } catch (Throwable th) {
//            return null;
//        }
    }

    @Nullable
    /* renamed from: a */
    public static String m4214a(@Nullable Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return "";
        }
        return m4225b((Map) map);
    }

    @Nullable
    /* renamed from: B */
    public static String m4225b(@Nullable Map map) {
        if (cx.m1193a(map)) {
            return null;
        }
        if (cx.a(19)) {
            return new JSONObject(map).toString();
        }
        return a((Object) map).toString();
    }

    @Nullable
    /* renamed from: a */
    public static String m4213a(List<String> list) {
        if (cx.a((Collection) list)) {
            return null;
        }
        if (cx.a(19)) {
            return new JSONArray(list).toString();
        }
        return a((Object) list).toString();
    }

    @Nullable
    /* renamed from: a */
    public static HashMap<String, String> m4215a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return m4216a(new JSONObject(str));
            } catch (Throwable th) {
            }
        }
        return null;
    }

    @NonNull
    /* renamed from: B */
    public static HashMap<String, String> m4226b(@NonNull String str) throws JSONException {
        return m4216a(new JSONObject(str));
    }

    @Nullable
    /* renamed from: a */
    public static List<String> m4228c(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            ArrayList arrayList = new ArrayList(jSONArray.length());
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    arrayList.add(jSONArray.getString(i));
                    i++;
                } catch (Throwable th) {
                    return arrayList;
                }
            }
            return arrayList;
        } catch (Throwable th2) {
            return null;
        }
    }

    @Nullable
    /* renamed from: a */
    public static HashMap<String, String> m4216a(JSONObject jSONObject) {
        if (JSONObject.NULL.equals(jSONObject)) {
            return null;
        }
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            String optString = jSONObject.optString(str);
            if (optString != null) {
                hashMap.put(str, optString);
            }
        }
        return hashMap;
    }

    @Nullable
    /* renamed from: a */
    public static JSONObject m4221a(@NonNull JSONObject jSONObject, @NonNull cz czVar) throws JSONException {
        Float f;
        Float f2;
        Double d;
        jSONObject.put("lat", czVar.getLatitude());
        jSONObject.put("lon", czVar.getLongitude());
        jSONObject.putOpt("timestamp", Long.valueOf(czVar.getTime()));
        jSONObject.putOpt("precision", czVar.hasAccuracy() ? Float.valueOf(czVar.getAccuracy()) : null);
        String str = "direction";
        if (czVar.hasBearing()) {
            f = Float.valueOf(czVar.getBearing());
        } else {
            f = null;
        }
        jSONObject.putOpt(str, f);
        String str2 = "speed";
        if (czVar.hasSpeed()) {
            f2 = Float.valueOf(czVar.getSpeed());
        } else {
            f2 = null;
        }
        jSONObject.putOpt(str2, f2);
        String str3 = "altitude";
        if (czVar.hasAltitude()) {
            d = Double.valueOf(czVar.getAltitude());
        } else {
            d = null;
        }
        jSONObject.putOpt(str3, d);
        jSONObject.putOpt("provider", cu.m1159c(czVar.getProvider(), null));
        jSONObject.putOpt("original_provider", czVar.mo676a());
        return jSONObject;
    }

    @Nullable
    /* renamed from: a */
    public static Long m4211a(JSONObject jSONObject, String str) {
        Long l = null;
        if (jSONObject == null || !jSONObject.has(str)) {
            return l;
        }
        try {
            return Long.valueOf(jSONObject.getLong(str));
        } catch (Throwable th) {
            return l;
        }
    }

    @Nullable
    /* renamed from: B */
    public static Integer m4224b(JSONObject jSONObject, String str) {
        Integer num = null;
        if (jSONObject == null || !jSONObject.has(str)) {
            return num;
        }
        try {
            return Integer.valueOf(jSONObject.getInt(str));
        } catch (Throwable th) {
            return num;
        }
    }

    @Nullable
    /* renamed from: a */
    public static Boolean m4227c(JSONObject jSONObject, String str) {
        Boolean bool = null;
        if (jSONObject == null || !jSONObject.has(str)) {
            return bool;
        }
        try {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        } catch (Throwable th) {
            return bool;
        }
    }

    /* renamed from: a */
    public static boolean m4222a(JSONObject jSONObject, String str, boolean z) {
        Boolean c = m4227c(jSONObject, str);
        return c == null ? z : c.booleanValue();
    }

    @Nullable
    /* renamed from: d */
    public static Float m4229d(JSONObject jSONObject, String str) {
        Float f = null;
        if (jSONObject == null || !jSONObject.has(str)) {
            return f;
        }
        try {
            return Float.valueOf((float) jSONObject.getDouble(str));
        } catch (Throwable th) {
            return f;
        }
    }

    @Nullable
    /* renamed from: a */
    public static byte[] m4223a(@NonNull JSONObject jSONObject, @NonNull String str, byte[] bArr) {
        String optString = jSONObject.optString(str, null);
        if (optString == null) {
            return bArr;
        }
        try {
            return cu.m1163e(optString);
        } catch (Throwable th) {
            return bArr;
        }
    }

    /* renamed from: a */
    public static List<String> m4217a(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }

    /* renamed from: a */
    public static JSONArray m4218a(uu[] uuVarArr) {
        JSONArray jSONArray = new JSONArray();
        if (uuVarArr != null) {
            for (uu a : uuVarArr) {
                try {
                    jSONArray.put(m4220a(a));
                } catch (Throwable th) {
                }
            }
        }
        return jSONArray;
    }

    /* renamed from: a */
    public static JSONObject m4220a(uu uuVar) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("cell_id", uuVar.mo2421e());
        jSONObject.put("signal_strength", uuVar.mo2416a());
        jSONObject.put("lac", uuVar.mo2420d());
        jSONObject.put("country_code", uuVar.mo2418b());
        jSONObject.put("operator_id", uuVar.mo2419c());
        jSONObject.put("operator_name", uuVar.mo2422f());
        jSONObject.put("is_connected", uuVar.mo2424h());
        jSONObject.put("cell_type", uuVar.mo2425i());
        jSONObject.put("pci", uuVar.mo2426j());
        jSONObject.put("last_visible_time_offset", uuVar.mo2427k());
        return jSONObject;
    }

    /* renamed from: a */
    public static JSONObject m4219a() throws JSONException {
        return new JSONObject().put("stat_sending", new JSONObject().put("disabled", true));
    }
}
