package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.wd */
public class wd {

    /* renamed from: a */
    private volatile long f2929a;

    /* renamed from: B */
    private ly f2930b;

    /* renamed from: a */
    private wh f2931c;

    /* renamed from: com.yandex.metrica.impl.ob.wd$a */
    private static class C1156a {

        /* renamed from: a */
        static wd f2932a = new wd();
    }

    /* renamed from: a */
    public static wd m4332a() {
        return C1156a.f2932a;
    }

    private wd() {
    }

    /* renamed from: B */
    public synchronized long mo2557b() {
        return this.f2929a;
    }

    /* renamed from: a */
    public synchronized void mo2555a(@NonNull Context context) {
        mo2556a(new ly(ld.m2146a(context).mo1238c()), (wh) new wg());
    }

    /* renamed from: a */
    public synchronized void mo2554a(long j, @Nullable Long l) {
        boolean z = true;
        synchronized (this) {
            this.f2929a = (j - this.f2931c.a()) / 1000;
            if (this.f2930b.mo1381c(true)) {
                if (l != null) {
                    long abs = Math.abs(j - this.f2931c.a());
                    ly lyVar = this.f2930b;
                    if (abs <= TimeUnit.SECONDS.toMillis(l.longValue())) {
                        z = false;
                    }
                    lyVar.mo1384d(z);
                } else {
                    this.f2930b.mo1384d(false);
                }
            }
            this.f2930b.mo1369a(this.f2929a);
            this.f2930b.mo1364q();
        }
    }

    /* renamed from: a */
    public synchronized void mo2558c() {
        this.f2930b.mo1384d(false);
        this.f2930b.mo1364q();
    }

    /* renamed from: d */
    public synchronized boolean mo2559d() {
        return this.f2930b.mo1381c(true);
    }

    @VisibleForTesting
    /* renamed from: a */
    public void mo2556a(ly lyVar, wh whVar) {
        this.f2930b = lyVar;
        this.f2929a = this.f2930b.mo1378c(0);
        this.f2931c = whVar;
    }
}
