package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0879j;

/* renamed from: com.yandex.metrica.impl.ob.ne */
public class ne implements mq<ub, C0879j> {
    @NonNull
    /* renamed from: a */
    public C0879j b(@NonNull ub ubVar) {
        C0879j jVar = new C0879j();
        jVar.f2201b = ubVar.f2708a;
        jVar.f2202c = ubVar.f2709b;
        jVar.f2203d = vj.a(ubVar.f2710c);
        jVar.f2204e = ubVar.f2711d;
        jVar.f2205f = ubVar.f2712e;
        return jVar;
    }

    @NonNull
    /* renamed from: a */
    public ub a(@NonNull C0879j jVar) {
        return new ub(jVar.f2201b, jVar.f2202c, vj.a(jVar.f2203d), jVar.f2204e, jVar.f2205f);
    }
}
