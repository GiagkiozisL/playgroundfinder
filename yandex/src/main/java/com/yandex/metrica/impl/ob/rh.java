package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;
import java.util.Arrays;

/* renamed from: com.yandex.metrica.impl.ob.rh */
public interface rh {

    /* renamed from: com.yandex.metrica.impl.ob.rh$a */
    final class a extends e {

        /* renamed from: l */
        private static volatile a[] f1908l;

        /* renamed from: B */
        public int f1909b;

        /* renamed from: a */
        public int f1910c;

        /* renamed from: d */
        public int f1911d;

        /* renamed from: e */
        public int f1912e;

        /* renamed from: f */
        public int f1913f;

        /* renamed from: g */
        public String f1914g;

        /* renamed from: h */
        public boolean f1915h;

        /* renamed from: i */
        public int f1916i;

        /* renamed from: j */
        public int f1917j;

        /* renamed from: k */
        public long f1918k;

        /* renamed from: d */
        public static a[] m3062d() {
            if (f1908l == null) {
                synchronized (com.yandex.metrica.impl.ob.c.a) {
                    if (f1908l == null) {
                        f1908l = new a[0];
                    }
                }
            }
            return f1908l;
        }

        public a() {
            mo1787e();
        }

        /* renamed from: e */
        public a mo1787e() {
            this.f1909b = -1;
            this.f1910c = 0;
            this.f1911d = -1;
            this.f1912e = -1;
            this.f1913f = -1;
            this.f1914g = "";
            this.f1915h = false;
            this.f1916i = 0;
            this.f1917j = -1;
            this.f1918k = 0;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            if (this.f1909b != -1) {
                bVar.mo361b(1, this.f1909b);
            }
            if (this.f1910c != 0) {
                bVar.mo366c(2, this.f1910c);
            }
            if (this.f1911d != -1) {
                bVar.mo361b(3, this.f1911d);
            }
            if (this.f1912e != -1) {
                bVar.mo361b(4, this.f1912e);
            }
            if (this.f1913f != -1) {
                bVar.mo361b(5, this.f1913f);
            }
            if (!this.f1914g.equals("")) {
                bVar.mo351a(6, this.f1914g);
            }
            if (this.f1915h) {
                bVar.mo352a(7, this.f1915h);
            }
            if (this.f1916i != 0) {
                bVar.mo348a(8, this.f1916i);
            }
            if (this.f1917j != -1) {
                bVar.mo361b(9, this.f1917j);
            }
            if (this.f1918k != 0) {
                bVar.mo349a(10, this.f1918k);
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f1909b != -1) {
                c += com.yandex.metrica.impl.ob.b.m449e(1, this.f1909b);
            }
            if (this.f1910c != 0) {
                c += com.yandex.metrica.impl.ob.b.m453f(2, this.f1910c);
            }
            if (this.f1911d != -1) {
                c += com.yandex.metrica.impl.ob.b.m449e(3, this.f1911d);
            }
            if (this.f1912e != -1) {
                c += com.yandex.metrica.impl.ob.b.m449e(4, this.f1912e);
            }
            if (this.f1913f != -1) {
                c += com.yandex.metrica.impl.ob.b.m449e(5, this.f1913f);
            }
            if (!this.f1914g.equals("")) {
                c += com.yandex.metrica.impl.ob.b.m437b(6, this.f1914g);
            }
            if (this.f1915h) {
                c += com.yandex.metrica.impl.ob.b.m438b(7, this.f1915h);
            }
            if (this.f1916i != 0) {
                c += com.yandex.metrica.impl.ob.b.m445d(8, this.f1916i);
            }
            if (this.f1917j != -1) {
                c += com.yandex.metrica.impl.ob.b.m449e(9, this.f1917j);
            }
            if (this.f1918k != 0) {
                return c + com.yandex.metrica.impl.ob.b.d(10, this.f1918k);
            }
            return c;
        }

        /* renamed from: B */
        public a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case 8:
                        this.f1909b = aVar.mo231k();
                        continue;
                    case 16:
                        this.f1910c = aVar.mo232l();
                        continue;
                    case 24:
                        this.f1911d = aVar.mo231k();
                        continue;
                    case 32:
                        this.f1912e = aVar.mo231k();
                        continue;
                    case 40:
                        this.f1913f = aVar.mo231k();
                        continue;
                    case 50:
                        this.f1914g = aVar.mo229i();
                        continue;
                    case 56:
                        this.f1915h = aVar.mo228h();
                        continue;
                    case 64:
                        int g = aVar.mo225g();
                        switch (g) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                this.f1916i = g;
                                break;
                            default:
                                continue;
                        }
                    case 72:
                        this.f1917j = aVar.mo231k();
                        continue;
                    case 80:
                        this.f1918k = aVar.mo221e();
                        continue;
                    default:
                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rh$B */
    final class b extends e {

        /* renamed from: B */
        public C0820b[] f1919b;

        /* renamed from: a */
        public C0819a[] f1920c;

        /* renamed from: com.yandex.metrica.impl.ob.rh$B$a */
        public static final class C0819a extends e {

            /* renamed from: g */
            private static volatile C0819a[] f1921g;

            /* renamed from: B */
            public long f1922b;

            /* renamed from: a */
            public long f1923c;

            /* renamed from: d */
            public a[] f1924d;

            /* renamed from: e */
            public d[] f1925e;

            /* renamed from: f */
            public long f1926f;

            /* renamed from: d */
            public static C0819a[] m3073d() {
                if (f1921g == null) {
                    synchronized (com.yandex.metrica.impl.ob.c.a) {
                        if (f1921g == null) {
                            f1921g = new C0819a[0];
                        }
                    }
                }
                return f1921g;
            }

            public C0819a() {
                mo1791e();
            }

            /* renamed from: e */
            public C0819a mo1791e() {
                this.f1922b = 0;
                this.f1923c = 0;
                this.f1924d = rh.a.m3062d();
                this.f1925e = d.d();
                this.f1926f = 0;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo349a(1, this.f1922b);
                bVar.mo349a(2, this.f1923c);
                if (this.f1924d != null && this.f1924d.length > 0) {
                    for (a aVar : this.f1924d) {
                        if (aVar != null) {
                            bVar.mo350a(3, (e) aVar);
                        }
                    }
                }
                if (this.f1925e != null && this.f1925e.length > 0) {
                    for (d dVar : this.f1925e) {
                        if (dVar != null) {
                            bVar.mo350a(4, (e) dVar);
                        }
                    }
                }
                if (this.f1926f != 0) {
                    bVar.mo349a(5, this.f1926f);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int d = com.yandex.metrica.impl.ob.b.d(2, this.f1923c) + super.mo741c() + com.yandex.metrica.impl.ob.b.d(1, this.f1922b);
                if (this.f1924d != null && this.f1924d.length > 0) {
                    for (a aVar : this.f1924d) {
                        if (aVar != null) {
                            d += com.yandex.metrica.impl.ob.b.b(3, (e) aVar);
                        }
                    }
                }
                if (this.f1925e != null && this.f1925e.length > 0) {
                    for (rh.d dVar : this.f1925e) {
                        if (dVar != null) {
                            d += com.yandex.metrica.impl.ob.b.b(4, (e) dVar);
                        }
                    }
                }
                if (this.f1926f != 0) {
                    return d + com.yandex.metrica.impl.ob.b.d(5, this.f1926f);
                }
                return d;
            }

            /* renamed from: B */
            public C0819a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                int length2;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f1922b = aVar.mo221e();
                            continue;
                        case 16:
                            this.f1923c = aVar.mo221e();
                            continue;
                        case 26:
                            int b = com.yandex.metrica.impl.ob.g.b(aVar, 26);
                            if (this.f1924d == null) {
                                length2 = 0;
                            } else {
                                length2 = this.f1924d.length;
                            }
                            rh.a[] aVarArr = new a[(b + length2)];
                            if (length2 != 0) {
                                System.arraycopy(this.f1924d, 0, aVarArr, 0, length2);
                            }
                            while (length2 < aVarArr.length - 1) {
                                aVarArr[length2] = new a();
                                aVar.mo215a((e) aVarArr[length2]);
                                aVar.mo213a();
                                length2++;
                            }
                            aVarArr[length2] = new a();
                            aVar.mo215a((e) aVarArr[length2]);
                            this.f1924d = aVarArr;
                            continue;
                        case 34:
                            int b2 = com.yandex.metrica.impl.ob.g.b(aVar, 34);
                            if (this.f1925e == null) {
                                length = 0;
                            } else {
                                length = this.f1925e.length;
                            }
                            d[] dVarArr = new d[(b2 + length)];
                            if (length != 0) {
                                System.arraycopy(this.f1925e, 0, dVarArr, 0, length);
                            }
                            while (length < dVarArr.length - 1) {
                                dVarArr[length] = new d();
                                aVar.mo215a((e) dVarArr[length]);
                                aVar.mo213a();
                                length++;
                            }
                            dVarArr[length] = new d();
                            aVar.mo215a((e) dVarArr[length]);
                            this.f1925e = dVarArr;
                            continue;
                        case 40:
                            this.f1926f = aVar.mo221e();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$B$B */
        public static final class C0820b extends e {

            /* renamed from: n */
            private static volatile C0820b[] f1927n;

            /* renamed from: B */
            public long f1928b;

            /* renamed from: a */
            public long f1929c;

            /* renamed from: d */
            public long f1930d;

            /* renamed from: e */
            public double f1931e;

            /* renamed from: f */
            public double f1932f;

            /* renamed from: g */
            public int f1933g;

            /* renamed from: h */
            public int f1934h;

            /* renamed from: i */
            public int f1935i;

            /* renamed from: j */
            public int f1936j;

            /* renamed from: k */
            public int f1937k;

            /* renamed from: l */
            public int f1938l;

            /* renamed from: m */
            public long f1939m;

            /* renamed from: d */
            public static C0820b[] m3079d() {
                if (f1927n == null) {
                    synchronized (com.yandex.metrica.impl.ob.c.a) {
                        if (f1927n == null) {
                            f1927n = new C0820b[0];
                        }
                    }
                }
                return f1927n;
            }

            public C0820b() {
                mo1793e();
            }

            /* renamed from: e */
            public C0820b mo1793e() {
                this.f1928b = 0;
                this.f1929c = 0;
                this.f1930d = 0;
                this.f1931e = 0.0d;
                this.f1932f = 0.0d;
                this.f1933g = 0;
                this.f1934h = 0;
                this.f1935i = 0;
                this.f1936j = 0;
                this.f1937k = 0;
                this.f1938l = 0;
                this.f1939m = 0;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo349a(1, this.f1928b);
                bVar.mo349a(2, this.f1929c);
                if (this.f1930d != 0) {
                    bVar.mo349a(3, this.f1930d);
                }
                bVar.mo346a(4, this.f1931e);
                bVar.mo346a(5, this.f1932f);
                if (this.f1933g != 0) {
                    bVar.mo361b(6, this.f1933g);
                }
                if (this.f1934h != 0) {
                    bVar.mo361b(7, this.f1934h);
                }
                if (this.f1935i != 0) {
                    bVar.mo361b(8, this.f1935i);
                }
                if (this.f1936j != 0) {
                    bVar.mo348a(9, this.f1936j);
                }
                if (this.f1937k != 0) {
                    bVar.mo348a(10, this.f1937k);
                }
                if (this.f1938l != 0) {
                    bVar.mo348a(11, this.f1938l);
                }
                if (this.f1939m != 0) {
                    bVar.mo349a(12, this.f1939m);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c() + com.yandex.metrica.impl.ob.b.d(1, this.f1928b) + com.yandex.metrica.impl.ob.b.d(2, this.f1929c);
                if (this.f1930d != 0) {
                    c += com.yandex.metrica.impl.ob.b.d(3, this.f1930d);
                }
                int b = c + com.yandex.metrica.impl.ob.b.m434b(4, this.f1931e) + com.yandex.metrica.impl.ob.b.m434b(5, this.f1932f);
                if (this.f1933g != 0) {
                    b += com.yandex.metrica.impl.ob.b.m449e(6, this.f1933g);
                }
                if (this.f1934h != 0) {
                    b += com.yandex.metrica.impl.ob.b.m449e(7, this.f1934h);
                }
                if (this.f1935i != 0) {
                    b += com.yandex.metrica.impl.ob.b.m449e(8, this.f1935i);
                }
                if (this.f1936j != 0) {
                    b += com.yandex.metrica.impl.ob.b.m445d(9, this.f1936j);
                }
                if (this.f1937k != 0) {
                    b += com.yandex.metrica.impl.ob.b.m445d(10, this.f1937k);
                }
                if (this.f1938l != 0) {
                    b += com.yandex.metrica.impl.ob.b.m445d(11, this.f1938l);
                }
                if (this.f1939m != 0) {
                    return b + com.yandex.metrica.impl.ob.b.d(12, this.f1939m);
                }
                return b;
            }

            /* renamed from: B */
            public C0820b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f1928b = aVar.mo221e();
                            continue;
                        case 16:
                            this.f1929c = aVar.mo221e();
                            continue;
                        case 24:
                            this.f1930d = aVar.mo221e();
                            continue;
                        case 33:
                            this.f1931e = aVar.mo218c();
                            continue;
                        case 41:
                            this.f1932f = aVar.mo218c();
                            continue;
                        case 48:
                            this.f1933g = aVar.mo231k();
                            continue;
                        case 56:
                            this.f1934h = aVar.mo231k();
                            continue;
                        case 64:
                            this.f1935i = aVar.mo231k();
                            continue;
                        case 72:
                            this.f1936j = aVar.mo225g();
                            continue;
                        case 80:
                            int g = aVar.mo225g();
                            switch (g) {
                                case 0:
                                case 1:
                                case 2:
                                    this.f1937k = g;
                                    break;
                                default:
                                    continue;
                            }
                        case 88:
                            int g2 = aVar.mo225g();
                            switch (g2) {
                                case 0:
                                case 1:
                                    this.f1938l = g2;
                                    break;
                                default:
                                    continue;
                            }
                        case 96:
                            this.f1939m = aVar.mo221e();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public b() {
            mo1789d();
        }

        /* renamed from: d */
        public b mo1789d() {
            this.f1919b = C0820b.m3079d();
            this.f1920c = C0819a.m3073d();
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            if (this.f1919b != null && this.f1919b.length > 0) {
                for (C0820b bVar2 : this.f1919b) {
                    if (bVar2 != null) {
                        bVar.mo350a(1, (e) bVar2);
                    }
                }
            }
            if (this.f1920c != null && this.f1920c.length > 0) {
                for (C0819a aVar : this.f1920c) {
                    if (aVar != null) {
                        bVar.mo350a(2, (e) aVar);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f1919b != null && this.f1919b.length > 0) {
                for (C0820b bVar : this.f1919b) {
                    if (bVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(1, (e) bVar);
                    }
                }
            }
            if (this.f1920c != null && this.f1920c.length > 0) {
                for (C0819a aVar : this.f1920c) {
                    if (aVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(2, (e) aVar);
                    }
                }
            }
            return c;
        }

        /* renamed from: B */
        public b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            int length;
            int length2;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        int b = com.yandex.metrica.impl.ob.g.b(aVar, 10);
                        if (this.f1919b == null) {
                            length2 = 0;
                        } else {
                            length2 = this.f1919b.length;
                        }
                        C0820b[] bVarArr = new C0820b[(b + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.f1919b, 0, bVarArr, 0, length2);
                        }
                        while (length2 < bVarArr.length - 1) {
                            bVarArr[length2] = new C0820b();
                            aVar.mo215a((e) bVarArr[length2]);
                            aVar.mo213a();
                            length2++;
                        }
                        bVarArr[length2] = new C0820b();
                        aVar.mo215a((e) bVarArr[length2]);
                        this.f1919b = bVarArr;
                        continue;
                    case 18:
                        int b2 = com.yandex.metrica.impl.ob.g.b(aVar, 18);
                        if (this.f1920c == null) {
                            length = 0;
                        } else {
                            length = this.f1920c.length;
                        }
                        C0819a[] aVarArr = new C0819a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.f1920c, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0819a();
                            aVar.mo215a((e) aVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        aVarArr[length] = new C0819a();
                        aVar.mo215a((e) aVarArr[length]);
                        this.f1920c = aVarArr;
                        continue;
                    default:
                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rh$a */
    final class c extends e {

        /* renamed from: B */
        public e[] f1940b;

        /* renamed from: a */
        public C0825d f1941c;

        /* renamed from: d */
        public C0822a[] f1942d;

        /* renamed from: e */
        public C0824c[] f1943e;

        /* renamed from: f */
        public String[] f1944f;

        /* renamed from: g */
        public C0833f[] f1945g;

        /* renamed from: h */
        public String[] f1946h;

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$a */
        public static final class C0822a extends com.yandex.metrica.impl.ob.e {

            /* renamed from: d */
            private static volatile C0822a[] f1947d;

            /* renamed from: B */
            public String f1948b;

            /* renamed from: a */
            public String f1949c;

            /* renamed from: d */
            public static C0822a[] m3090d() {
                if (f1947d == null) {
                    synchronized (com.yandex.metrica.impl.ob.c.a) {
                        if (f1947d == null) {
                            f1947d = new C0822a[0];
                        }
                    }
                }
                return f1947d;
            }

            public C0822a() {
                mo1797e();
            }

            /* renamed from: e */
            public C0822a mo1797e() {
                this.f1948b = "";
                this.f1949c = "";
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo351a(1, this.f1948b);
                bVar.mo351a(2, this.f1949c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f1948b) + com.yandex.metrica.impl.ob.b.m437b(2, this.f1949c);
            }

            /* renamed from: B */
            public C0822a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f1948b = aVar.mo229i();
                            continue;
                        case 18:
                            this.f1949c = aVar.mo229i();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$B */
        public static final class b extends com.yandex.metrica.impl.ob.e {

            /* renamed from: B */
            public double b;

            /* renamed from: a */
            public double c;

            /* renamed from: d */
            public long d;

            /* renamed from: e */
            public int e;

            /* renamed from: f */
            public int f;

            /* renamed from: g */
            public int g;

            /* renamed from: h */
            public int h;

            /* renamed from: i */
            public int i;

            /* renamed from: j */
            public String j;

            public b() {
                mo1799d();
            }

            /* renamed from: d */
            public b mo1799d() {
                this.b = 0.0d;
                this.c = 0.0d;
                this.d = 0;
                this.e = 0;
                this.f = 0;
                this.g = 0;
                this.h = 0;
                this.i = 0;
                this.j = "";
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo346a(1, this.b);
                bVar.mo346a(2, this.c);
                if (this.d != 0) {
                    bVar.mo349a(3, this.d);
                }
                if (this.e != 0) {
                    bVar.mo361b(4, this.e);
                }
                if (this.f != 0) {
                    bVar.mo361b(5, this.f);
                }
                if (this.g != 0) {
                    bVar.mo361b(6, this.g);
                }
                if (this.h != 0) {
                    bVar.mo348a(7, this.h);
                }
                if (this.i != 0) {
                    bVar.mo348a(8, this.i);
                }
                if (!this.j.equals("")) {
                    bVar.mo351a(9, this.j);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m434b(1, this.b) + com.yandex.metrica.impl.ob.b.m434b(2, this.c);
                if (this.d != 0) {
                    c += com.yandex.metrica.impl.ob.b.d(3, this.d);
                }
                if (this.e != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(4, this.e);
                }
                if (this.f != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(5, this.f);
                }
                if (this.g != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(6, this.g);
                }
                if (this.h != 0) {
                    c += com.yandex.metrica.impl.ob.b.m445d(7, this.h);
                }
                if (this.i != 0) {
                    c += com.yandex.metrica.impl.ob.b.m445d(8, this.i);
                }
                if (!this.j.equals("")) {
                    return c + com.yandex.metrica.impl.ob.b.m437b(9, this.j);
                }
                return c;
            }

            /* renamed from: B */
            public b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 9:
                            this.b = aVar.mo218c();
                            continue;
                        case 17:
                            this.c = aVar.mo218c();
                            continue;
                        case 24:
                            this.d = aVar.mo221e();
                            continue;
                        case 32:
                            this.e = aVar.mo231k();
                            continue;
                        case 40:
                            this.f = aVar.mo231k();
                            continue;
                        case 48:
                            this.g = aVar.mo231k();
                            continue;
                        case 56:
                            this.h = aVar.mo225g();
                            continue;
                        case 64:
                            int g = aVar.mo225g();
                            switch (g) {
                                case 0:
                                case 1:
                                case 2:
                                    this.i = g;
                                    break;
                                default:
                                    continue;
                            }
                        case 74:
                            this.j = aVar.mo229i();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$a */
        public static final class C0824c extends com.yandex.metrica.impl.ob.e {

            /* renamed from: d */
            private static volatile C0824c[] f1959d;

            /* renamed from: B */
            public String f1960b;

            /* renamed from: a */
            public String f1961c;

            /* renamed from: d */
            public static C0824c[] m3101d() {
                if (f1959d == null) {
                    synchronized (com.yandex.metrica.impl.ob.c.a) {
                        if (f1959d == null) {
                            f1959d = new C0824c[0];
                        }
                    }
                }
                return f1959d;
            }

            public C0824c() {
                mo1801e();
            }

            /* renamed from: e */
            public C0824c mo1801e() {
                this.f1960b = "";
                this.f1961c = "";
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo351a(1, this.f1960b);
                bVar.mo351a(2, this.f1961c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f1960b) + com.yandex.metrica.impl.ob.b.m437b(2, this.f1961c);
            }

            /* renamed from: B */
            public C0824c mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f1960b = aVar.mo229i();
                            continue;
                        case 18:
                            this.f1961c = aVar.mo229i();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$d */
        public static final class C0825d extends com.yandex.metrica.impl.ob.e {

            /* renamed from: B */
            public String f1962b;

            /* renamed from: a */
            public String f1963c;

            /* renamed from: d */
            public String f1964d;

            /* renamed from: e */
            public int f1965e;

            /* renamed from: f */
            public String f1966f;

            /* renamed from: g */
            public String f1967g;

            /* renamed from: h */
            public boolean f1968h;

            /* renamed from: i */
            public int f1969i;

            /* renamed from: j */
            public String f1970j;

            /* renamed from: k */
            public String f1971k;

            /* renamed from: l */
            public String f1972l;

            /* renamed from: m */
            public int f1973m;

            /* renamed from: n */
            public C0826a[] f1974n;

            /* renamed from: o */
            public String f1975o;

            /* renamed from: com.yandex.metrica.impl.ob.rh$a$d$a */
            public static final class C0826a extends com.yandex.metrica.impl.ob.e {

                /* renamed from: d */
                private static volatile C0826a[] f1976d;

                /* renamed from: B */
                public String f1977b;

                /* renamed from: a */
                public long f1978c;

                /* renamed from: d */
                public static C0826a[] m3112d() {
                    if (f1976d == null) {
                        synchronized (com.yandex.metrica.impl.ob.c.a) {
                            if (f1976d == null) {
                                f1976d = new C0826a[0];
                            }
                        }
                    }
                    return f1976d;
                }

                public C0826a() {
                    mo1805e();
                }

                /* renamed from: e */
                public C0826a mo1805e() {
                    this.f1977b = "";
                    this.f1978c = 0;
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    bVar.mo351a(1, this.f1977b);
                    bVar.mo349a(2, this.f1978c);
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    return super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f1977b) + com.yandex.metrica.impl.ob.b.d(2, this.f1978c);
                }

                /* renamed from: B */
                public C0826a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                this.f1977b = aVar.mo229i();
                                continue;
                            case 16:
                                this.f1978c = aVar.mo221e();
                                continue;
                            default:
                                if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            public C0825d() {
                mo1803d();
            }

            /* renamed from: d */
            public C0825d mo1803d() {
                this.f1962b = "";
                this.f1963c = "";
                this.f1964d = "";
                this.f1965e = 0;
                this.f1966f = "";
                this.f1967g = "";
                this.f1968h = false;
                this.f1969i = 0;
                this.f1970j = "";
                this.f1971k = "";
                this.f1972l = "";
                this.f1973m = 0;
                this.f1974n = C0826a.m3112d();
                this.f1975o = "";
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (!this.f1962b.equals("")) {
                    bVar.mo351a(1, this.f1962b);
                }
                if (!this.f1963c.equals("")) {
                    bVar.mo351a(2, this.f1963c);
                }
                if (!this.f1964d.equals("")) {
                    bVar.mo351a(4, this.f1964d);
                }
                if (this.f1965e != 0) {
                    bVar.mo361b(5, this.f1965e);
                }
                if (!this.f1966f.equals("")) {
                    bVar.mo351a(10, this.f1966f);
                }
                if (!this.f1967g.equals("")) {
                    bVar.mo351a(15, this.f1967g);
                }
                if (this.f1968h) {
                    bVar.mo352a(17, this.f1968h);
                }
                if (this.f1969i != 0) {
                    bVar.mo361b(18, this.f1969i);
                }
                if (!this.f1970j.equals("")) {
                    bVar.mo351a(19, this.f1970j);
                }
                if (!this.f1971k.equals("")) {
                    bVar.mo351a(20, this.f1971k);
                }
                if (!this.f1972l.equals("")) {
                    bVar.mo351a(21, this.f1972l);
                }
                if (this.f1973m != 0) {
                    bVar.mo361b(22, this.f1973m);
                }
                if (this.f1974n != null && this.f1974n.length > 0) {
                    for (C0826a aVar : this.f1974n) {
                        if (aVar != null) {
                            bVar.mo350a(23, (com.yandex.metrica.impl.ob.e) aVar);
                        }
                    }
                }
                if (!this.f1975o.equals("")) {
                    bVar.mo351a(24, this.f1975o);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (!this.f1962b.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(1, this.f1962b);
                }
                if (!this.f1963c.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(2, this.f1963c);
                }
                if (!this.f1964d.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(4, this.f1964d);
                }
                if (this.f1965e != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(5, this.f1965e);
                }
                if (!this.f1966f.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(10, this.f1966f);
                }
                if (!this.f1967g.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(15, this.f1967g);
                }
                if (this.f1968h) {
                    c += com.yandex.metrica.impl.ob.b.m438b(17, this.f1968h);
                }
                if (this.f1969i != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(18, this.f1969i);
                }
                if (!this.f1970j.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(19, this.f1970j);
                }
                if (!this.f1971k.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(20, this.f1971k);
                }
                if (!this.f1972l.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(21, this.f1972l);
                }
                if (this.f1973m != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(22, this.f1973m);
                }
                if (this.f1974n != null && this.f1974n.length > 0) {
                    int i = c;
                    for (C0826a aVar : this.f1974n) {
                        if (aVar != null) {
                            i += com.yandex.metrica.impl.ob.b.b(23, (com.yandex.metrica.impl.ob.e) aVar);
                        }
                    }
                    c = i;
                }
                if (!this.f1975o.equals("")) {
                    return c + com.yandex.metrica.impl.ob.b.m437b(24, this.f1975o);
                }
                return c;
            }

            /* renamed from: B */
            public C0825d mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f1962b = aVar.mo229i();
                            continue;
                        case 18:
                            this.f1963c = aVar.mo229i();
                            continue;
                        case 34:
                            this.f1964d = aVar.mo229i();
                            continue;
                        case 40:
                            this.f1965e = aVar.mo231k();
                            continue;
                        case 82:
                            this.f1966f = aVar.mo229i();
                            continue;
                        case 122:
                            this.f1967g = aVar.mo229i();
                            continue;
                        case 136:
                            this.f1968h = aVar.mo228h();
                            continue;
                        case 144:
                            this.f1969i = aVar.mo231k();
                            continue;
                        case 154:
                            this.f1970j = aVar.mo229i();
                            continue;
                        case 162:
                            this.f1971k = aVar.mo229i();
                            continue;
                        case 170:
                            this.f1972l = aVar.mo229i();
                            continue;
                        case 176:
                            this.f1973m = aVar.mo231k();
                            continue;
                        case 186:
                            int b = com.yandex.metrica.impl.ob.g.b(aVar, 186);
                            if (this.f1974n == null) {
                                length = 0;
                            } else {
                                length = this.f1974n.length;
                            }
                            C0826a[] aVarArr = new C0826a[(b + length)];
                            if (length != 0) {
                                System.arraycopy(this.f1974n, 0, aVarArr, 0, length);
                            }
                            while (length < aVarArr.length - 1) {
                                aVarArr[length] = new C0826a();
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length]);
                                aVar.mo213a();
                                length++;
                            }
                            aVarArr[length] = new C0826a();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length]);
                            this.f1974n = aVarArr;
                            continue;
                        case 194:
                            this.f1975o = aVar.mo229i();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$e */
        public static final class e extends com.yandex.metrica.impl.ob.e {

            /* renamed from: e */
            private static volatile e[] f1979e;

            /* renamed from: B */
            public long b;

            /* renamed from: a */
            public b c;

            /* renamed from: d */
            public a[] d;

            /* renamed from: com.yandex.metrica.impl.ob.rh$a$e$a */
            public static final class a extends com.yandex.metrica.impl.ob.e {

                /* renamed from: r */
                private static volatile a[] f1983r;

                /* renamed from: B */
                public long f1984b;

                /* renamed from: a */
                public long f1985c;

                /* renamed from: d */
                public int f1986d;

                /* renamed from: e */
                public String f1987e;

                /* renamed from: f */
                public byte[] f1988f;

                /* renamed from: g */
                public rh.c.b f1989g;

                /* renamed from: h */
                public C0830b f1990h;

                /* renamed from: i */
                public String f1991i;

                /* renamed from: j */
                public C0829a f1992j;

                /* renamed from: k */
                public int f1993k;

                /* renamed from: l */
                public int f1994l;

                /* renamed from: m */
                public int f1995m;

                /* renamed from: n */
                public byte[] f1996n;

                /* renamed from: o */
                public int f1997o;

                /* renamed from: p */
                public long f1998p;

                /* renamed from: q */
                public long f1999q;

                /* renamed from: com.yandex.metrica.impl.ob.rh$a$e$a$a */
                public static final class C0829a extends com.yandex.metrica.impl.ob.e {

                    /* renamed from: B */
                    public String f2000b;

                    /* renamed from: a */
                    public String f2001c;

                    /* renamed from: d */
                    public String f2002d;

                    public C0829a() {
                        mo1811d();
                    }

                    /* renamed from: d */
                    public C0829a mo1811d() {
                        this.f2000b = "";
                        this.f2001c = "";
                        this.f2002d = "";
                        this.f754a = -1;
                        return this;
                    }

                    /* renamed from: a */
                    public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                        bVar.mo351a(1, this.f2000b);
                        if (!this.f2001c.equals("")) {
                            bVar.mo351a(2, this.f2001c);
                        }
                        if (!this.f2002d.equals("")) {
                            bVar.mo351a(3, this.f2002d);
                        }
                        super.mo739a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public int mo741c() {
                        int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2000b);
                        if (!this.f2001c.equals("")) {
                            c += com.yandex.metrica.impl.ob.b.m437b(2, this.f2001c);
                        }
                        if (!this.f2002d.equals("")) {
                            return c + com.yandex.metrica.impl.ob.b.m437b(3, this.f2002d);
                        }
                        return c;
                    }

                    /* renamed from: B */
                    public C0829a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                        while (true) {
                            int a = aVar.mo213a();
                            switch (a) {
                                case 0:
                                    break;
                                case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                    this.f2000b = aVar.mo229i();
                                    continue;
                                case 18:
                                    this.f2001c = aVar.mo229i();
                                    continue;
                                case 26:
                                    this.f2002d = aVar.mo229i();
                                    continue;
                                default:
                                    if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
//                        return this;
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.rh$a$e$a$B */
                public static final class C0830b extends com.yandex.metrica.impl.ob.e {

                    /* renamed from: B */
                    public rh.a[] f2003b;

                    /* renamed from: a */
                    public d[] f2004c;

                    /* renamed from: d */
                    public int f2005d;

                    /* renamed from: e */
                    public String f2006e;

                    /* renamed from: f */
                    public C0831a f2007f;

                    /* renamed from: com.yandex.metrica.impl.ob.rh$a$e$a$B$a */
                    public static final class C0831a extends com.yandex.metrica.impl.ob.e {

                        /* renamed from: B */
                        public String f2008b;

                        /* renamed from: a */
                        public int f2009c;

                        public C0831a() {
                            mo1815d();
                        }

                        /* renamed from: d */
                        public C0831a mo1815d() {
                            this.f2008b = "";
                            this.f2009c = 0;
                            this.f754a = -1;
                            return this;
                        }

                        /* renamed from: a */
                        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                            bVar.mo351a(1, this.f2008b);
                            if (this.f2009c != 0) {
                                bVar.mo348a(2, this.f2009c);
                            }
                            super.mo739a(bVar);
                        }

                        /* access modifiers changed from: protected */
                        /* renamed from: a */
                        public int mo741c() {
                            int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2008b);
                            if (this.f2009c != 0) {
                                return c + com.yandex.metrica.impl.ob.b.m445d(2, this.f2009c);
                            }
                            return c;
                        }

                        /* renamed from: B */
                        public C0831a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                            while (true) {
                                int a = aVar.mo213a();
                                switch (a) {
                                    case 0:
                                        break;
                                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                        this.f2008b = aVar.mo229i();
                                        continue;
                                    case 16:
                                        int g = aVar.mo225g();
                                        switch (g) {
                                            case 0:
                                            case 1:
                                            case 2:
                                                this.f2009c = g;
                                                break;
                                            default:
                                                continue;
                                        }
                                    default:
                                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                            break;
                                        } else {
                                            continue;
                                        }
                                }
                            }
//                            return this;
                        }
                    }

                    public C0830b() {
                        mo1813d();
                    }

                    /* renamed from: d */
                    public C0830b mo1813d() {
                        this.f2003b = rh.a.m3062d();
                        this.f2004c = rh.d.d();
                        this.f2005d = 2;
                        this.f2006e = "";
                        this.f2007f = null;
                        this.f754a = -1;
                        return this;
                    }

                    /* renamed from: a */
                    public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                        if (this.f2003b != null && this.f2003b.length > 0) {
                            for (rh.a aVar : this.f2003b) {
                                if (aVar != null) {
                                    bVar.mo350a(1, (com.yandex.metrica.impl.ob.e) aVar);
                                }
                            }
                        }
                        if (this.f2004c != null && this.f2004c.length > 0) {
                            for (d dVar : this.f2004c) {
                                if (dVar != null) {
                                    bVar.mo350a(2, (com.yandex.metrica.impl.ob.e) dVar);
                                }
                            }
                        }
                        if (this.f2005d != 2) {
                            bVar.mo348a(3, this.f2005d);
                        }
                        if (!this.f2006e.equals("")) {
                            bVar.mo351a(4, this.f2006e);
                        }
                        if (this.f2007f != null) {
                            bVar.mo350a(5, (com.yandex.metrica.impl.ob.e) this.f2007f);
                        }
                        super.mo739a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public int mo741c() {
                        int c = super.mo741c();
                        if (this.f2003b != null && this.f2003b.length > 0) {
                            for (rh.a aVar : this.f2003b) {
                                if (aVar != null) {
                                    c += com.yandex.metrica.impl.ob.b.b(1, (com.yandex.metrica.impl.ob.e) aVar);
                                }
                            }
                        }
                        if (this.f2004c != null && this.f2004c.length > 0) {
                            for (d dVar : this.f2004c) {
                                if (dVar != null) {
                                    c += com.yandex.metrica.impl.ob.b.b(2, (com.yandex.metrica.impl.ob.e) dVar);
                                }
                            }
                        }
                        if (this.f2005d != 2) {
                            c += com.yandex.metrica.impl.ob.b.m445d(3, this.f2005d);
                        }
                        if (!this.f2006e.equals("")) {
                            c += com.yandex.metrica.impl.ob.b.m437b(4, this.f2006e);
                        }
                        if (this.f2007f != null) {
                            return c + com.yandex.metrica.impl.ob.b.b(5, (com.yandex.metrica.impl.ob.e) this.f2007f);
                        }
                        return c;
                    }

                    /* renamed from: B */
                    public C0830b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                        int length;
                        int length2;
                        while (true) {
                            int a = aVar.mo213a();
                            switch (a) {
                                case 0:
                                    break;
                                case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                    int b = com.yandex.metrica.impl.ob.g.b(aVar, 10);
                                    if (this.f2003b == null) {
                                        length2 = 0;
                                    } else {
                                        length2 = this.f2003b.length;
                                    }
                                    rh.a[] aVarArr = new rh.a[(b + length2)];
                                    if (length2 != 0) {
                                        System.arraycopy(this.f2003b, 0, aVarArr, 0, length2);
                                    }
                                    while (length2 < aVarArr.length - 1) {
                                        aVarArr[length2] = new rh.a();
                                        aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length2]);
                                        aVar.mo213a();
                                        length2++;
                                    }
                                    aVarArr[length2] = new rh.a();
                                    aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length2]);
                                    this.f2003b = aVarArr;
                                    continue;
                                case 18:
                                    int b2 = com.yandex.metrica.impl.ob.g.b(aVar, 18);
                                    if (this.f2004c == null) {
                                        length = 0;
                                    } else {
                                        length = this.f2004c.length;
                                    }
                                    d[] dVarArr = new d[(b2 + length)];
                                    if (length != 0) {
                                        System.arraycopy(this.f2004c, 0, dVarArr, 0, length);
                                    }
                                    while (length < dVarArr.length - 1) {
                                        dVarArr[length] = new d();
                                        aVar.mo215a((com.yandex.metrica.impl.ob.e) dVarArr[length]);
                                        aVar.mo213a();
                                        length++;
                                    }
                                    dVarArr[length] = new d();
                                    aVar.mo215a((com.yandex.metrica.impl.ob.e) dVarArr[length]);
                                    this.f2004c = dVarArr;
                                    continue;
                                case 24:
                                    int g = aVar.mo225g();
                                    switch (g) {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                        case 5:
                                        case 6:
                                        case YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT /*7*/:
                                        case 8:
                                        case 9:
                                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                        case 11:
                                        case 12:
                                            this.f2005d = g;
                                            break;
                                        default:
                                            continue;
                                    }
                                case 34:
                                    this.f2006e = aVar.mo229i();
                                    continue;
                                case 42:
                                    if (this.f2007f == null) {
                                        this.f2007f = new C0831a();
                                    }
                                    aVar.mo215a((com.yandex.metrica.impl.ob.e) this.f2007f);
                                    continue;
                                default:
                                    if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
//                        return this;
                    }
                }

                /* renamed from: d */
                public static a[] m3124d() {
                    if (f1983r == null) {
                        synchronized (com.yandex.metrica.impl.ob.c.a) {
                            if (f1983r == null) {
                                f1983r = new a[0];
                            }
                        }
                    }
                    return f1983r;
                }

                public a() {
                    mo1809e();
                }

                /* renamed from: e */
                public a mo1809e() {
                    this.f1984b = 0;
                    this.f1985c = 0;
                    this.f1986d = 0;
                    this.f1987e = "";
                    this.f1988f = com.yandex.metrica.impl.ob.g.h;
                    this.f1989g = null;
                    this.f1990h = null;
                    this.f1991i = "";
                    this.f1992j = null;
                    this.f1993k = 0;
                    this.f1994l = 0;
                    this.f1995m = -1;
                    this.f1996n = com.yandex.metrica.impl.ob.g.h;
                    this.f1997o = -1;
                    this.f1998p = 0;
                    this.f1999q = 0;
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    bVar.mo349a(1, this.f1984b);
                    bVar.mo349a(2, this.f1985c);
                    bVar.mo361b(3, this.f1986d);
                    if (!this.f1987e.equals("")) {
                        bVar.mo351a(4, this.f1987e);
                    }
                    if (!Arrays.equals(this.f1988f, com.yandex.metrica.impl.ob.g.h)) {
                        bVar.mo353a(5, this.f1988f);
                    }
                    if (this.f1989g != null) {
                        bVar.mo350a(6, (com.yandex.metrica.impl.ob.e) this.f1989g);
                    }
                    if (this.f1990h != null) {
                        bVar.mo350a(7, (com.yandex.metrica.impl.ob.e) this.f1990h);
                    }
                    if (!this.f1991i.equals("")) {
                        bVar.mo351a(8, this.f1991i);
                    }
                    if (this.f1992j != null) {
                        bVar.mo350a(9, (com.yandex.metrica.impl.ob.e) this.f1992j);
                    }
                    if (this.f1993k != 0) {
                        bVar.mo361b(10, this.f1993k);
                    }
                    if (this.f1994l != 0) {
                        bVar.mo348a(12, this.f1994l);
                    }
                    if (this.f1995m != -1) {
                        bVar.mo348a(13, this.f1995m);
                    }
                    if (!Arrays.equals(this.f1996n, com.yandex.metrica.impl.ob.g.h)) {
                        bVar.mo353a(14, this.f1996n);
                    }
                    if (this.f1997o != -1) {
                        bVar.mo348a(15, this.f1997o);
                    }
                    if (this.f1998p != 0) {
                        bVar.mo349a(16, this.f1998p);
                    }
                    if (this.f1999q != 0) {
                        bVar.mo349a(17, this.f1999q);
                    }
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    int c = super.mo741c() + com.yandex.metrica.impl.ob.b.d(1, this.f1984b) + com.yandex.metrica.impl.ob.b.d(2, this.f1985c) + com.yandex.metrica.impl.ob.b.m449e(3, this.f1986d);
                    if (!this.f1987e.equals("")) {
                        c += com.yandex.metrica.impl.ob.b.m437b(4, this.f1987e);
                    }
                    if (!Arrays.equals(this.f1988f, com.yandex.metrica.impl.ob.g.h)) {
                        c += com.yandex.metrica.impl.ob.b.b(5, this.f1988f);
                    }
                    if (this.f1989g != null) {
                        c += com.yandex.metrica.impl.ob.b.b(6, (com.yandex.metrica.impl.ob.e) this.f1989g);
                    }
                    if (this.f1990h != null) {
                        c += com.yandex.metrica.impl.ob.b.b(7, (com.yandex.metrica.impl.ob.e) this.f1990h);
                    }
                    if (!this.f1991i.equals("")) {
                        c += com.yandex.metrica.impl.ob.b.m437b(8, this.f1991i);
                    }
                    if (this.f1992j != null) {
                        c += com.yandex.metrica.impl.ob.b.b(9, (com.yandex.metrica.impl.ob.e) this.f1992j);
                    }
                    if (this.f1993k != 0) {
                        c += com.yandex.metrica.impl.ob.b.m449e(10, this.f1993k);
                    }
                    if (this.f1994l != 0) {
                        c += com.yandex.metrica.impl.ob.b.m445d(12, this.f1994l);
                    }
                    if (this.f1995m != -1) {
                        c += com.yandex.metrica.impl.ob.b.m445d(13, this.f1995m);
                    }
                    if (!Arrays.equals(this.f1996n, com.yandex.metrica.impl.ob.g.h)) {
                        c += com.yandex.metrica.impl.ob.b.b(14, this.f1996n);
                    }
                    if (this.f1997o != -1) {
                        c += com.yandex.metrica.impl.ob.b.m445d(15, this.f1997o);
                    }
                    if (this.f1998p != 0) {
                        c += com.yandex.metrica.impl.ob.b.d(16, this.f1998p);
                    }
                    if (this.f1999q != 0) {
                        return c + com.yandex.metrica.impl.ob.b.d(17, this.f1999q);
                    }
                    return c;
                }

                /* renamed from: B */
                public a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case 8:
                                this.f1984b = aVar.mo221e();
                                continue;
                            case 16:
                                this.f1985c = aVar.mo221e();
                                continue;
                            case 24:
                                this.f1986d = aVar.mo231k();
                                continue;
                            case 34:
                                this.f1987e = aVar.mo229i();
                                continue;
                            case 42:
                                this.f1988f = aVar.mo230j();
                                continue;
                            case 50:
                                if (this.f1989g == null) {
                                    this.f1989g = new c.b();
                                }
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) this.f1989g);
                                continue;
                            case 58:
                                if (this.f1990h == null) {
                                    this.f1990h = new C0830b();
                                }
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) this.f1990h);
                                continue;
                            case 66:
                                this.f1991i = aVar.mo229i();
                                continue;
                            case 74:
                                if (this.f1992j == null) {
                                    this.f1992j = new C0829a();
                                }
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) this.f1992j);
                                continue;
                            case 80:
                                this.f1993k = aVar.mo231k();
                                continue;
                            case 96:
                                int g = aVar.mo225g();
                                switch (g) {
                                    case 0:
                                    case 1:
                                        this.f1994l = g;
                                        break;
                                    default:
                                        continue;
                                }
                            case 104:
                                int g2 = aVar.mo225g();
                                switch (g2) {
                                    case -1:
                                    case 0:
                                    case 1:
                                        this.f1995m = g2;
                                        break;
                                    default:
                                        continue;
                                }
                            case 114:
                                this.f1996n = aVar.mo230j();
                                continue;
                            case 120:
                                int g3 = aVar.mo225g();
                                switch (g3) {
                                    case -1:
                                    case 0:
                                    case 1:
                                        this.f1997o = g3;
                                        break;
                                    default:
                                        continue;
                                }
                            case 128:
                                this.f1998p = aVar.mo221e();
                                continue;
                            case 136:
                                this.f1999q = aVar.mo221e();
                                continue;
                            default:
                                if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            /* renamed from: com.yandex.metrica.impl.ob.rh$a$e$B */
            public static final class b extends com.yandex.metrica.impl.ob.e {

                /* renamed from: B */
                public g f2010b;

                /* renamed from: a */
                public String f2011c;

                /* renamed from: d */
                public int d;

                public b() {
                    mo1817d();
                }

                /* renamed from: d */
                public b mo1817d() {
                    this.f2010b = null;
                    this.f2011c = "";
                    this.d = 0;
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    if (this.f2010b != null) {
                        bVar.mo350a(1, (com.yandex.metrica.impl.ob.e) this.f2010b);
                    }
                    bVar.mo351a(2, this.f2011c);
                    if (this.d != 0) {
                        bVar.mo348a(5, this.d);
                    }
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    int c = super.mo741c();
                    if (this.f2010b != null) {
                        c += com.yandex.metrica.impl.ob.b.b(1, (com.yandex.metrica.impl.ob.e) this.f2010b);
                    }
                    int b = c + com.yandex.metrica.impl.ob.b.m437b(2, this.f2011c);
                    if (this.d != 0) {
                        return b + com.yandex.metrica.impl.ob.b.m445d(5, this.d);
                    }
                    return b;
                }

                /* renamed from: B */
                public b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                if (this.f2010b == null) {
                                    this.f2010b = new g();
                                }
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) this.f2010b);
                                continue;
                            case 18:
                                this.f2011c = aVar.mo229i();
                                continue;
                            case 40:
                                int g = aVar.mo225g();
                                switch (g) {
                                    case 0:
                                    case 1:
                                    case 2:
                                        this.d = g;
                                        break;
                                    default:
                                        continue;
                                }
                            default:
                                if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            /* renamed from: d */
            public static e[] m3118d() {
                if (f1979e == null) {
                    synchronized (com.yandex.metrica.impl.ob.c.a) {
                        if (f1979e == null) {
                            f1979e = new e[0];
                        }
                    }
                }
                return f1979e;
            }

            public e() {
                mo1807e();
            }

            /* renamed from: e */
            public e mo1807e() {
                this.b = 0;
                this.c = null;
                this.d = a.m3124d();
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo349a(1, this.b);
                if (this.c != null) {
                    bVar.mo350a(2, (com.yandex.metrica.impl.ob.e) this.c);
                }
                if (this.d != null && this.d.length > 0) {
                    for (a aVar : this.d) {
                        if (aVar != null) {
                            bVar.mo350a(3, (com.yandex.metrica.impl.ob.e) aVar);
                        }
                    }
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c() + com.yandex.metrica.impl.ob.b.d(1, this.b);
                if (this.c != null) {
                    c += com.yandex.metrica.impl.ob.b.b(2, (com.yandex.metrica.impl.ob.e) this.c);
                }
                if (this.d == null || this.d.length <= 0) {
                    return c;
                }
                int i = c;
                for (a aVar : this.d) {
                    if (aVar != null) {
                        i += com.yandex.metrica.impl.ob.b.b(3, (com.yandex.metrica.impl.ob.e) aVar);
                    }
                }
                return i;
            }

            /* renamed from: B */
            public e mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.b = aVar.mo221e();
                            continue;
                        case 18:
                            if (this.c == null) {
                                this.c = new b();
                            }
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) this.c);
                            continue;
                        case 26:
                            int b = com.yandex.metrica.impl.ob.g.b(aVar, 26);
                            if (this.d == null) {
                                length = 0;
                            } else {
                                length = this.d.length;
                            }
                            e.a[] aVarArr = new a[(b + length)];
                            if (length != 0) {
                                System.arraycopy(this.d, 0, aVarArr, 0, length);
                            }
                            while (length < aVarArr.length - 1) {
                                aVarArr[length] = new a();
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length]);
                                aVar.mo213a();
                                length++;
                            }
                            aVarArr[length] = new a();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length]);
                            this.d = aVarArr;
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$f */
        public static final class C0833f extends com.yandex.metrica.impl.ob.e {

            /* renamed from: g */
            private static volatile C0833f[] f2013g;

            /* renamed from: B */
            public int f2014b;

            /* renamed from: a */
            public int f2015c;

            /* renamed from: d */
            public String f2016d;

            /* renamed from: e */
            public boolean f2017e;

            /* renamed from: f */
            public String f2018f;

            /* renamed from: d */
            public static C0833f[] m3150d() {
                if (f2013g == null) {
                    synchronized (com.yandex.metrica.impl.ob.c.a) {
                        if (f2013g == null) {
                            f2013g = new C0833f[0];
                        }
                    }
                }
                return f2013g;
            }

            public C0833f() {
                mo1819e();
            }

            /* renamed from: e */
            public C0833f mo1819e() {
                this.f2014b = 0;
                this.f2015c = 0;
                this.f2016d = "";
                this.f2017e = false;
                this.f2018f = "";
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (this.f2014b != 0) {
                    bVar.mo361b(1, this.f2014b);
                }
                if (this.f2015c != 0) {
                    bVar.mo361b(2, this.f2015c);
                }
                if (!this.f2016d.equals("")) {
                    bVar.mo351a(3, this.f2016d);
                }
                if (this.f2017e) {
                    bVar.mo352a(4, this.f2017e);
                }
                if (!this.f2018f.equals("")) {
                    bVar.mo351a(5, this.f2018f);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (this.f2014b != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(1, this.f2014b);
                }
                if (this.f2015c != 0) {
                    c += com.yandex.metrica.impl.ob.b.m449e(2, this.f2015c);
                }
                if (!this.f2016d.equals("")) {
                    c += com.yandex.metrica.impl.ob.b.m437b(3, this.f2016d);
                }
                if (this.f2017e) {
                    c += com.yandex.metrica.impl.ob.b.m438b(4, this.f2017e);
                }
                if (!this.f2018f.equals("")) {
                    return c + com.yandex.metrica.impl.ob.b.m437b(5, this.f2018f);
                }
                return c;
            }

            /* renamed from: B */
            public C0833f mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2014b = aVar.mo231k();
                            continue;
                        case 16:
                            this.f2015c = aVar.mo231k();
                            continue;
                        case 26:
                            this.f2016d = aVar.mo229i();
                            continue;
                        case 32:
                            this.f2017e = aVar.mo228h();
                            continue;
                        case 42:
                            this.f2018f = aVar.mo229i();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$a$g */
        public static final class g extends com.yandex.metrica.impl.ob.e {

            /* renamed from: B */
            public long f2019b;

            /* renamed from: a */
            public int f2020c;

            /* renamed from: d */
            public long f2021d;

            /* renamed from: e */
            public boolean f2022e;

            public g() {
                mo1821d();
            }

            /* renamed from: d */
            public g mo1821d() {
                this.f2019b = 0;
                this.f2020c = 0;
                this.f2021d = 0;
                this.f2022e = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo349a(1, this.f2019b);
                bVar.mo366c(2, this.f2020c);
                if (this.f2021d != 0) {
                    bVar.b(3, this.f2021d);
                }
                if (this.f2022e) {
                    bVar.mo352a(4, this.f2022e);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c() + com.yandex.metrica.impl.ob.b.d(1, this.f2019b) + com.yandex.metrica.impl.ob.b.m453f(2, this.f2020c);
                if (this.f2021d != 0) {
                    c += com.yandex.metrica.impl.ob.b.m450e(3, this.f2021d);
                }
                if (this.f2022e) {
                    return c + com.yandex.metrica.impl.ob.b.m438b(4, this.f2022e);
                }
                return c;
            }

            /* renamed from: B */
            public g mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2019b = aVar.mo221e();
                            continue;
                        case 16:
                            this.f2020c = aVar.mo232l();
                            continue;
                        case 24:
                            this.f2021d = aVar.mo223f();
                            continue;
                        case 32:
                            this.f2022e = aVar.mo228h();
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public c() {
            mo1795d();
        }

        /* renamed from: d */
        public c mo1795d() {
            this.f1940b = e.m3118d();
            this.f1941c = null;
            this.f1942d = C0822a.m3090d();
            this.f1943e = C0824c.m3101d();
            this.f1944f = com.yandex.metrica.impl.ob.g.f;
            this.f1945g = C0833f.m3150d();
            this.f1946h = com.yandex.metrica.impl.ob.g.f;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            if (this.f1940b != null && this.f1940b.length > 0) {
                for (e eVar : this.f1940b) {
                    if (eVar != null) {
                        bVar.mo350a(3, (com.yandex.metrica.impl.ob.e) eVar);
                    }
                }
            }
            if (this.f1941c != null) {
                bVar.mo350a(4, (com.yandex.metrica.impl.ob.e) this.f1941c);
            }
            if (this.f1942d != null && this.f1942d.length > 0) {
                for (C0822a aVar : this.f1942d) {
                    if (aVar != null) {
                        bVar.mo350a(7, (com.yandex.metrica.impl.ob.e) aVar);
                    }
                }
            }
            if (this.f1943e != null && this.f1943e.length > 0) {
                for (C0824c cVar : this.f1943e) {
                    if (cVar != null) {
                        bVar.mo350a(8, (com.yandex.metrica.impl.ob.e) cVar);
                    }
                }
            }
            if (this.f1944f != null && this.f1944f.length > 0) {
                for (String str : this.f1944f) {
                    if (str != null) {
                        bVar.mo351a(9, str);
                    }
                }
            }
            if (this.f1945g != null && this.f1945g.length > 0) {
                for (C0833f fVar : this.f1945g) {
                    if (fVar != null) {
                        bVar.mo350a(10, (com.yandex.metrica.impl.ob.e) fVar);
                    }
                }
            }
            if (this.f1946h != null && this.f1946h.length > 0) {
                for (String str2 : this.f1946h) {
                    if (str2 != null) {
                        bVar.mo351a(11, str2);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f1940b != null && this.f1940b.length > 0) {
                for (e eVar : this.f1940b) {
                    if (eVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(3, (com.yandex.metrica.impl.ob.e) eVar);
                    }
                }
            }
            if (this.f1941c != null) {
                c += com.yandex.metrica.impl.ob.b.b(4, (com.yandex.metrica.impl.ob.e) this.f1941c);
            }
            if (this.f1942d != null && this.f1942d.length > 0) {
                for (C0822a aVar : this.f1942d) {
                    if (aVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(7, (com.yandex.metrica.impl.ob.e) aVar);
                    }
                }
            }
            if (this.f1943e != null && this.f1943e.length > 0) {
                for (C0824c cVar : this.f1943e) {
                    if (cVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(8, (com.yandex.metrica.impl.ob.e) cVar);
                    }
                }
            }
            if (this.f1944f != null && this.f1944f.length > 0) {
                int i = 0;
                int i2 = 0;
                for (String str : this.f1944f) {
                    if (str != null) {
                        i2++;
                        i += com.yandex.metrica.impl.ob.b.m441b(str);
                    }
                }
                c = (i2 * 1) + c + i;
            }
            if (this.f1945g != null && this.f1945g.length > 0) {
                for (C0833f fVar : this.f1945g) {
                    if (fVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(10, (com.yandex.metrica.impl.ob.e) fVar);
                    }
                }
            }
            if (this.f1946h == null || this.f1946h.length <= 0) {
                return c;
            }
            int i3 = 0;
            int i4 = 0;
            for (String str2 : this.f1946h) {
                if (str2 != null) {
                    i4++;
                    i3 += com.yandex.metrica.impl.ob.b.m441b(str2);
                }
            }
            return c + i3 + (i4 * 1);
        }

        /* renamed from: B */
        public c mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            int length;
            int length2;
            int length3;
            int length4;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case 26:
                        int b = com.yandex.metrica.impl.ob.g.b(aVar, 26);
                        if (this.f1940b == null) {
                            length4 = 0;
                        } else {
                            length4 = this.f1940b.length;
                        }
                        e[] eVarArr = new e[(b + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.f1940b, 0, eVarArr, 0, length4);
                        }
                        while (length4 < eVarArr.length - 1) {
                            eVarArr[length4] = new e();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) eVarArr[length4]);
                            aVar.mo213a();
                            length4++;
                        }
                        eVarArr[length4] = new e();
                        aVar.mo215a((com.yandex.metrica.impl.ob.e) eVarArr[length4]);
                        this.f1940b = eVarArr;
                        continue;
                    case 34:
                        if (this.f1941c == null) {
                            this.f1941c = new C0825d();
                        }
                        aVar.mo215a((com.yandex.metrica.impl.ob.e) this.f1941c);
                        continue;
                    case 58:
                        int b2 = com.yandex.metrica.impl.ob.g.b(aVar, 58);
                        if (this.f1942d == null) {
                            length3 = 0;
                        } else {
                            length3 = this.f1942d.length;
                        }
                        C0822a[] aVarArr = new C0822a[(b2 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.f1942d, 0, aVarArr, 0, length3);
                        }
                        while (length3 < aVarArr.length - 1) {
                            aVarArr[length3] = new C0822a();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length3]);
                            aVar.mo213a();
                            length3++;
                        }
                        aVarArr[length3] = new C0822a();
                        aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length3]);
                        this.f1942d = aVarArr;
                        continue;
                    case 66:
                        int b3 = com.yandex.metrica.impl.ob.g.b(aVar, 66);
                        if (this.f1943e == null) {
                            length2 = 0;
                        } else {
                            length2 = this.f1943e.length;
                        }
                        C0824c[] cVarArr = new C0824c[(b3 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.f1943e, 0, cVarArr, 0, length2);
                        }
                        while (length2 < cVarArr.length - 1) {
                            cVarArr[length2] = new C0824c();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) cVarArr[length2]);
                            aVar.mo213a();
                            length2++;
                        }
                        cVarArr[length2] = new C0824c();
                        aVar.mo215a((com.yandex.metrica.impl.ob.e) cVarArr[length2]);
                        this.f1943e = cVarArr;
                        continue;
                    case 74:
                        int b4 = com.yandex.metrica.impl.ob.g.b(aVar, 74);
                        int length5 = this.f1944f == null ? 0 : this.f1944f.length;
                        String[] strArr = new String[(b4 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.f1944f, 0, strArr, 0, length5);
                        }
                        while (length5 < strArr.length - 1) {
                            strArr[length5] = aVar.mo229i();
                            aVar.mo213a();
                            length5++;
                        }
                        strArr[length5] = aVar.mo229i();
                        this.f1944f = strArr;
                        continue;
                    case 82:
                        int b5 = com.yandex.metrica.impl.ob.g.b(aVar, 82);
                        if (this.f1945g == null) {
                            length = 0;
                        } else {
                            length = this.f1945g.length;
                        }
                        C0833f[] fVarArr = new C0833f[(b5 + length)];
                        if (length != 0) {
                            System.arraycopy(this.f1945g, 0, fVarArr, 0, length);
                        }
                        while (length < fVarArr.length - 1) {
                            fVarArr[length] = new C0833f();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) fVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        fVarArr[length] = new C0833f();
                        aVar.mo215a((com.yandex.metrica.impl.ob.e) fVarArr[length]);
                        this.f1945g = fVarArr;
                        continue;
                    case YandexMetricaDefaultValues.DEFAULT_DISPATCH_PERIOD_SECONDS /*90*/:
                        int b6 = com.yandex.metrica.impl.ob.g.b(aVar, 90);
                        int length6 = this.f1946h == null ? 0 : this.f1946h.length;
                        String[] strArr2 = new String[(b6 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.f1946h, 0, strArr2, 0, length6);
                        }
                        while (length6 < strArr2.length - 1) {
                            strArr2[length6] = aVar.mo229i();
                            aVar.mo213a();
                            length6++;
                        }
                        strArr2[length6] = aVar.mo229i();
                        this.f1946h = strArr2;
                        continue;
                    default:
                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rh$d */
    final class d extends e {

        /* renamed from: g */
        private static volatile d[] f2023g;

        /* renamed from: B */
        public String f2024b;

        /* renamed from: a */
        public int f2025c;

        /* renamed from: d */
        public String f2026d;

        /* renamed from: e */
        public boolean f2027e;

        /* renamed from: f */
        public long f2028f;

        /* renamed from: d */
        public static d[] d() {
            if (f2023g == null) {
                synchronized (com.yandex.metrica.impl.ob.c.a) {
                    if (f2023g == null) {
                        f2023g = new d[0];
                    }
                }
            }
            return f2023g;
        }

        public d() {
            mo1823e();
        }

        /* renamed from: e */
        public d mo1823e() {
            this.f2024b = "";
            this.f2025c = 0;
            this.f2026d = "";
            this.f2027e = false;
            this.f2028f = 0;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            bVar.mo351a(1, this.f2024b);
            if (this.f2025c != 0) {
                bVar.mo366c(2, this.f2025c);
            }
            if (!this.f2026d.equals("")) {
                bVar.mo351a(3, this.f2026d);
            }
            if (this.f2027e) {
                bVar.mo352a(4, this.f2027e);
            }
            if (this.f2028f != 0) {
                bVar.mo349a(5, this.f2028f);
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2024b);
            if (this.f2025c != 0) {
                c += com.yandex.metrica.impl.ob.b.m453f(2, this.f2025c);
            }
            if (!this.f2026d.equals("")) {
                c += com.yandex.metrica.impl.ob.b.m437b(3, this.f2026d);
            }
            if (this.f2027e) {
                c += com.yandex.metrica.impl.ob.b.m438b(4, this.f2027e);
            }
            if (this.f2028f != 0) {
                return c + com.yandex.metrica.impl.ob.b.d(5, this.f2028f);
            }
            return c;
        }

        /* renamed from: B */
        public d mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        this.f2024b = aVar.mo229i();
                        continue;
                    case 16:
                        this.f2025c = aVar.mo232l();
                        continue;
                    case 26:
                        this.f2026d = aVar.mo229i();
                        continue;
                    case 32:
                        this.f2027e = aVar.mo228h();
                        continue;
                    case 40:
                        this.f2028f = aVar.mo221e();
                        continue;
                    default:
                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }
}
