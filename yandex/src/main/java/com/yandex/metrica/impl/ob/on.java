package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

import org.json.JSONArray;

/* renamed from: com.yandex.metrica.impl.ob.on */
public class on {
    @Nullable

    /* renamed from: a */
    private Long f1613a;

    /* renamed from: B */
    private long f1614b;

    /* renamed from: a */
    private long f1615c;
    @Nullable

    /* renamed from: d */
    private JSONArray f1616d;
    @Nullable

    /* renamed from: e */
    private JSONArray f1617e;

    @Nullable
    /* renamed from: a */
    public Long mo1560a() {
        return this.f1613a;
    }

    /* renamed from: a */
    public void mo1562a(@Nullable Long l) {
        this.f1613a = l;
    }

    /* renamed from: B */
    public long mo1564b() {
        return this.f1614b;
    }

    /* renamed from: a */
    public void mo1561a(long j) {
        this.f1614b = j;
    }

    @Nullable
    /* renamed from: a */
    public JSONArray mo1567c() {
        return this.f1616d;
    }

    /* renamed from: a */
    public void mo1563a(@Nullable JSONArray jSONArray) {
        this.f1616d = jSONArray;
    }

    @Nullable
    /* renamed from: d */
    public JSONArray mo1568d() {
        return this.f1617e;
    }

    /* renamed from: B */
    public void mo1566b(@Nullable JSONArray jSONArray) {
        this.f1617e = jSONArray;
    }

    /* renamed from: e */
    public long mo1569e() {
        return this.f1615c;
    }

    /* renamed from: B */
    public void mo1565b(long j) {
        this.f1615c = j;
    }
}
