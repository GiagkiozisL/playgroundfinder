package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.hg */
public class hg extends hd {

    /* renamed from: a */
    private lu f1009a;

    /* renamed from: B */
    private wd f1010b;

    public hg(en enVar) {
        this(enVar, enVar.mo834u(), wd.m4332a());
    }

    @VisibleForTesting
    hg(en enVar, lu luVar, wd wdVar) {
        super(enVar);
        this.f1009a = luVar;
        this.f1010b = wdVar;
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        en a = mo973a();
        st i = a.mo822i();
        if (!this.f1009a.mo1284d()) {
            if (!this.f1009a.mo1282c()) {
                if (i.mo2139P()) {
                    this.f1010b.mo2558c();
                }
                String e = wVar.mo2531e();
                this.f1009a.mo1281c(e);
                a.mo818e().mo881a(w.m4288e(wVar).mo1767c(e));
                this.f1009a.mo1277a(i.mo2140Q());
                mo973a().mo839z().mo1010a();
            }
            this.f1009a.mo1279b();
        }
        return false;
    }
}
