package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.v.C1115b;

import org.json.JSONException;
import org.json.JSONObject;

import me.android.ydx.CustomData;
import me.android.ydx.DataManager;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.nt */
public class nt {

    /* renamed from: a */
    private final String f1548a;

    /* renamed from: B */
    private final String f1549b;

    /* renamed from: a */
    private final String f1550c;

    /* renamed from: d */
    private final Point f1551d;

    @SuppressLint({"NewApi", "HardwareIds", "ObsoleteSdkInt"})
    public nt(@NonNull Context context, @Nullable String str, @NonNull pr prVar) {
        CustomData cData = DataManager.getInstance().getCustomData();
        this.f1548a = cData.manufacturer; //Build.MANUFACTURER;
        this.f1549b = cData.model; //Build.MODEL;
        this.f1550c = cData.serial;// m2623a(context, str, prVar);
        C1115b bVar = v.getInstance(context).f2870f;
        this.f1551d = new Point(bVar.f2880a, bVar.f2881b);
    }

//    @SuppressLint({"HardwareIds", "ObsoleteSdkInt", "MissingPermission", "NewApi"})
//    @NonNull
//    /* renamed from: a */
//    private String m2623a(@NonNull Context context, @Nullable String str, @NonNull pr prVar) {
//        if (cx.a(28)) {
//            if (prVar.mo1654d(context)) {
//                try {
//                    return Build.getSerial();
//                } catch (Throwable th) {
//                }
//            }
//            return wk.m4373b(str, "");
//        } else if (cx.a(8)) {
//            return Build.SERIAL;
//        } else {
//            return wk.m4373b(str, "");
//        }
//    }

    @NonNull
    /* renamed from: a */
    public String mo1518a() {
        return this.f1550c;
    }

    public nt(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        this.f1548a = jSONObject.getString("manufacturer");
        this.f1549b = jSONObject.getString("model");
        this.f1550c = jSONObject.getString("serial");
        this.f1551d = new Point(jSONObject.getInt("width"), jSONObject.getInt("height"));
    }

    /* renamed from: B */
    public JSONObject mo1519b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("manufacturer", this.f1548a);
        jSONObject.put("model", this.f1549b);
        jSONObject.put("serial", this.f1550c);
        jSONObject.put("width", this.f1551d.x);
        jSONObject.put("height", this.f1551d.y);
        return jSONObject;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        nt ntVar = (nt) o;
        if (this.f1548a != null) {
            if (!this.f1548a.equals(ntVar.f1548a)) {
                return false;
            }
        } else if (ntVar.f1548a != null) {
            return false;
        }
        if (this.f1549b != null) {
            if (!this.f1549b.equals(ntVar.f1549b)) {
                return false;
            }
        } else if (ntVar.f1549b != null) {
            return false;
        }
        if (this.f1551d != null) {
            z = this.f1551d.equals(ntVar.f1551d);
        } else if (ntVar.f1551d != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        if (this.f1548a != null) {
            i = this.f1548a.hashCode();
        } else {
            i = 0;
        }
        int i4 = i * 31;
        if (this.f1549b != null) {
            i2 = this.f1549b.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i2 + i4) * 31;
        if (this.f1551d != null) {
            i3 = this.f1551d.hashCode();
        }
        return i5 + i3;
    }

    public String toString() {
        return "DeviceSnapshot{mManufacturer='" + this.f1548a + '\'' + ", mModel='" + this.f1549b + '\'' + ", mSerial='" + this.f1550c + '\'' + ", mScreenSize=" + this.f1551d + '}';
    }
}
