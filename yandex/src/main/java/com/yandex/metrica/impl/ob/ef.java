package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.st.C0987d;

/* renamed from: com.yandex.metrica.impl.ob.ef */
public class ef extends en {
    public ef(@NonNull Context context, @NonNull uk ukVar, @NonNull bl blVar, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull C0987d dVar, @NonNull un unVar) {
        this(context, ekVar, new C0325a(), new cw(), new eo(context, ekVar, aVar, unVar, ukVar, dVar, blVar, al.m324a().mo292j().mo2632g(), cx.c(context, ekVar.mo791b())));
    }

    @VisibleForTesting
    ef(@NonNull Context context, @NonNull ek ekVar, @NonNull C0325a aVar, @NonNull cw cwVar, @NonNull eo eoVar) {
        super(context, ekVar, aVar, cwVar, eoVar);
    }

    @NonNull
    /* renamed from: a */
    public C0008a mo767a() {
        return C0008a.APPMETRICA;
    }
}
