package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.sq */
public class sq extends sn {
    @NonNull

    /* renamed from: a */
    private String f2450a;

    /* renamed from: com.yandex.metrica.impl.ob.sq$a */
    protected static abstract class C0980a<T extends sq, A extends C0972a> extends C0973b<T, A> {

        /* renamed from: a */
        private final MyPackageManager f2451c;

        protected C0980a(@NonNull Context context, @NonNull String pkgName) {
            this(context, pkgName, new MyPackageManager());
        }

        @VisibleForTesting
        protected C0980a(@NonNull Context context, @NonNull String pkgName, @NonNull MyPackageManager myPackageManagerVar) {
            super(context, pkgName);
            this.f2451c = myPackageManagerVar;
        }

        @NonNull
        /* renamed from: B */
        public T c(@NonNull c<A> cVar) {
            T t = (T) super.a(cVar);
            m3626a(t);
            return t;
        }

        /* renamed from: a */
        private void m3626a(@NonNull T t) {
//            String packageName = this.ctx.getPackageName();
            String packageName = DataManager.getInstance().getCustomData().botPkgName;
            ApplicationInfo b = this.f2451c.mo2661b(this.ctx, this.pkgName, 0);
            if (b != null) {
                t.mo2127l(m3625a(b));
            } else if (TextUtils.equals(packageName, this.pkgName)) {
                t.mo2127l(m3625a(this.ctx.getApplicationInfo()));
            } else {
                t.mo2127l("0");
            }
        }

        @NonNull
        /* renamed from: a */
        private String m3625a(@NonNull ApplicationInfo applicationInfo) {
            return (applicationInfo.flags & 2) != 0 ? "1" : "0";
        }
    }

    @NonNull
    /* renamed from: E */
    public String mo2126E() {
        return this.f2450a;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: l */
    public void mo2127l(@NonNull String str) {
        this.f2450a = str;
    }

    public String toString() {
        return "CoreRequestConfig{mAppDebuggable='" + this.f2450a + '\'' + super.toString() + '}';
    }
}
