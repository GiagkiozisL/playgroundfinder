package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.wz */
public enum wz {
    NONE(0),
    EXTERNALLY_ENCRYPTED_EVENT_CRYPTER(1),
    AES_VALUE_ENCRYPTION(2);
    

    /* renamed from: d */
    private final int f2957d;

    private wz(int i) {
        this.f2957d = i;
    }

    /* renamed from: a */
    public int mo2592a() {
        return this.f2957d;
    }

    /* renamed from: a */
    public static wz m4406a(int i) {
        wz[] values;
        for (wz wzVar : values()) {
            if (wzVar.mo2592a() == i) {
                return wzVar;
            }
        }
        return null;
    }
}
