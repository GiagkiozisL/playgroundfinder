package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.js */
public class js {
    @NonNull

    /* renamed from: a */
    private final Context f1166a;
    @NonNull

    /* renamed from: B */
    private final MyPackageManager f1167b;

    public js(@NonNull Context context) {
        this(context, new MyPackageManager());
    }

    /* renamed from: a */
    public int mo1123a() {
        int i = 1;
        try {
            Log.d(Constant.RUS_TAG, "js$mo1123a query intent with action ACTION_INIT");
            return Math.max(i, this.f1167b.queryIntentServices(this.f1166a, new Intent().setAction("com.yandex.metrica.configuration.ACTION_INIT"), 128).size());
        } catch (Throwable th) {
            th.printStackTrace();
            return i;
        }
    }

    @VisibleForTesting
    js(@NonNull Context context, @NonNull MyPackageManager myPackageManagerVar) {
        this.f1166a = context;
        this.f1167b = myPackageManagerVar;
    }
}
