package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.wp */
public class wp<K, V> {

    /* renamed from: a */
    private final HashMap<K, Collection<V>> f2940a = new HashMap<>();

    /* renamed from: a */
    public int a() {
        int i = 0;
        Iterator it = this.f2940a.values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((Collection) it.next()).size() + i2;
        }
    }

    @Nullable
    /* renamed from: a */
    public Collection<V> mo2571a(@Nullable K k) {
        return (Collection) this.f2940a.get(k);
    }

    @Nullable
    /* renamed from: a */
    public Collection<V> mo2572a(@Nullable K k, @Nullable V v) {
        Collection a;
        Collection collection = (Collection) this.f2940a.get(k);
        if (collection == null) {
            a = m4379c();
        } else {
            a = m4378a(collection);
        }
        a.add(v);
        return (Collection) this.f2940a.put(k, a);
    }

    @Nullable
    /* renamed from: B */
    public Collection<V> b(@Nullable K k) {
        return (Collection) this.f2940a.remove(k);
    }

    @Nullable
    /* renamed from: B */
    public Collection<V> b(@Nullable K k, @Nullable V v) {
        Collection collection = (Collection) this.f2940a.get(k);
        if (collection == null || !collection.remove(v)) {
            return null;
        }
        return m4378a(collection);
    }

    @NonNull
    /* renamed from: a */
    private Collection<V> m4379c() {
        return new ArrayList();
    }

    @NonNull
    /* renamed from: a */
    private Collection<V> m4378a(@NonNull Collection<V> collection) {
        return new ArrayList(collection);
    }

    @NonNull
    /* renamed from: B */
    public Set<? extends Entry<K, ? extends Collection<V>>> b() {
        return this.f2940a.entrySet();
    }

    public String toString() {
        return this.f2940a.toString();
    }
}
