package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.aw */
public final class aw {

    /* renamed from: a */
    private static volatile aw f262a;

    /* renamed from: B */
    private static final Object f263b = new Object();
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public String f264c;

    /* renamed from: a */
    public static aw m403a(@NonNull Context context) {
        if (f262a == null) {
            synchronized (f263b) {
                if (f262a == null) {
                    f262a = new aw(context.getApplicationContext());
                }
            }
        }
        return f262a;
    }

    private aw(Context context) {
        this.f264c = bt.m724a(context.getResources().getConfiguration().locale);
        dr.a().a(this, dz.class, dv.m1388a((du<dz>) new du<dz>() {
            /* renamed from: a */
            public void a(dz dzVar) {
                aw.this.f264c = dzVar.f753a;
            }
        }).mo736a());
    }

    @NonNull
    /* renamed from: a */
    public String mo327a() {
        return this.f264c;
    }
}
