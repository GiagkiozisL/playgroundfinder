package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.ConfigurationServiceReceiver;

@TargetApi(26)
/* renamed from: com.yandex.metrica.impl.ob.dg */
public class dg implements dh {
    @NonNull

    /* renamed from: a */
    private final dd f713a;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public final dk f714b;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final di f715c;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: d */
    public final PendingIntent f716d;

    @SuppressLint("WrongConstant")
    public dg(@NonNull Context context) {
        this(new dd(context), new dk(), new di(), PendingIntent.getBroadcast(context.getApplicationContext(), 7695436, new Intent("com.yandex.metrica.configuration.service.PLC").setClass(context, ConfigurationServiceReceiver.class), 134217728));
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: a */
    public synchronized void a(@NonNull final tt ttVar) {
        BluetoothLeScanner a = this.f713a.mo692a();
        if (a != null) {
            a();
            Integer num = (Integer) cx.a((wo<BluetoothLeScanner, Integer>) new wo<BluetoothLeScanner, Integer>() {
                /* renamed from: a */
                public Integer a(BluetoothLeScanner bluetoothLeScanner) throws Exception {
                    return Integer.valueOf(bluetoothLeScanner.startScan(dg.this.f715c.mo703a(ttVar.b), dg.this.f714b.mo706a(ttVar.a), dg.this.f716d));
                }
            }, a, "startScan", "BluetoothLeScanner");
        }
    }

    @SuppressLint({"MissingPermission"})
    /* renamed from: a */
    public synchronized void a() {
        BluetoothLeScanner a = this.f713a.mo692a();
        if (a != null) {
            cx.a((wn<BluetoothLeScanner>) new wn<BluetoothLeScanner>() {
                /* renamed from: a */
                public void a(BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.stopScan(dg.this.f716d);
                }
            }, a, "stopScan", "BluetoothLeScanner");
        }
    }

    @VisibleForTesting
    public dg(@NonNull dd ddVar, @NonNull dk dkVar, @NonNull di diVar, @NonNull PendingIntent pendingIntent) {
        this.f713a = ddVar;
        this.f714b = dkVar;
        this.f715c = diVar;
        this.f716d = pendingIntent;
    }
}
