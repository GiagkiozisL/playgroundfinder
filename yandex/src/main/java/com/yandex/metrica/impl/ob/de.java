package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.de */
public class de {
    @NonNull

    /* renamed from: a */
    private final tw f699a;
    @NonNull

    /* renamed from: B */
    private final dh f700b;
    @NonNull

    /* renamed from: a */
    private final dc f701c;

    /* renamed from: d */
    private Boolean f702d;

    public de(@NonNull Context context, @NonNull dh dhVar) {
        this(dhVar, new dc(context), new tw());
    }

    /* renamed from: a */
    public void mo695a(@NonNull Context context) {
        uk a = this.f699a.mo2320a(context);
        tt ttVar = a.bleCollectingConfig;
        if (ttVar != null && this.f701c.mo690a(a, ttVar)) {
            if (!this.f701c.mo691b(a, ttVar)) {
                this.f700b.a();
                this.f702d = Boolean.valueOf(false);
            } else if (vi.m4193b(this.f702d)) {
                this.f700b.a(a.bleCollectingConfig);
                this.f702d = Boolean.valueOf(true);
            }
        }
    }

    @VisibleForTesting
    public de(@NonNull dh dhVar, @NonNull dc dcVar, @NonNull tw twVar) {
        this.f700b = dhVar;
        this.f701c = dcVar;
        this.f699a = twVar;
    }
}
