package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rr.a.b.Aa;
import com.yandex.metrica.impl.ob.rr.a.b.Aa.C0867a;
import com.yandex.metrica.impl.ob.rr.a.b.Aa.C0868b;
import com.yandex.metrica.impl.ob.rr.a.b.Aa.C0869c;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.co */
public class co {

    /* renamed from: a */
    private static final Map<String, Integer> f574a;

    /* renamed from: B */
    private static final Map<String, Integer> f575b;

    /* renamed from: a */
    private static final Map<String, Integer> f576c;

    /* renamed from: d */
    private static final Map<String, Integer> f577d;
    @NonNull

    /* renamed from: e */
    private final nh f578e;

    static {
        HashMap hashMap = new HashMap(3);
        hashMap.put("all_matches", Integer.valueOf(1));
        hashMap.put("first_match", Integer.valueOf(2));
        hashMap.put("match_lost", Integer.valueOf(3));
        f574a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap(2);
        hashMap2.put("aggressive", Integer.valueOf(1));
        hashMap2.put("sticky", Integer.valueOf(2));
        f575b = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap(3);
        hashMap3.put("one", Integer.valueOf(1));
        hashMap3.put("few", Integer.valueOf(2));
        hashMap3.put("max", Integer.valueOf(3));
        f576c = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("low_power", Integer.valueOf(1));
        hashMap4.put("balanced", Integer.valueOf(2));
        hashMap4.put("low_latency", Integer.valueOf(3));
        f577d = Collections.unmodifiableMap(hashMap4);
    }

    public co() {
        this(new nh());
    }

    @NonNull
    /* renamed from: a */
    public tt mo617a(@NonNull JSONObject jSONObject) {
        return this.f578e.a(m990b(jSONObject));
    }

    @NonNull
    /* renamed from: B */
    private rr.a.b m990b(@NonNull JSONObject jSONObject) {
        rr.a.b bVar = new rr.a.b();
        JSONObject optJSONObject = jSONObject.optJSONObject("ble_collecting");
        if (optJSONObject != null) {
            bVar.b = m991c(optJSONObject.optJSONObject("scan_settings"));
            bVar.c = m989a(optJSONObject.optJSONArray("filters"));
            bVar.d = wk.m4368a(vq.m4211a(optJSONObject, "same_beacon_min_reporting_interval"), TimeUnit.SECONDS, bVar.d);
            bVar.e = wk.m4368a(vq.m4211a(optJSONObject, "first_delay_seconds"), TimeUnit.SECONDS, bVar.e);
        } else {
            bVar.b = new rr.a.b.B();
        }
        return bVar;
    }

    @NonNull
    /* renamed from: a */
    private rr.a.b.B m991c(@Nullable JSONObject jSONObject) {
        rr.a.b.B bVar = new rr.a.b.B();
        if (jSONObject != null) {
            Integer a = m988a(jSONObject, "callback_type", f574a);
            if (a != null) {
                bVar.b = a.intValue();
            }
            Integer a2 = m988a(jSONObject, "match_mode", f575b);
            if (a2 != null) {
                bVar.c = a2.intValue();
            }
            Integer a3 = m988a(jSONObject, "num_of_matches", f576c);
            if (a3 != null) {
                bVar.d = a3.intValue();
            }
            Integer a4 = m988a(jSONObject, "scan_mode", f577d);
            if (a4 != null) {
                bVar.e = a4.intValue();
            }
            bVar.f = wk.m4368a(vq.m4211a(jSONObject, "report_delay"), TimeUnit.SECONDS, bVar.f);
        }
        return bVar;
    }

    @NonNull
    /* renamed from: a */
    private Aa[] m989a(@Nullable JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                Aa d = m992d(jSONArray.optJSONObject(i));
                if (d != null) {
                    arrayList.add(d);
                }
            }
        }
        return (Aa[]) arrayList.toArray(new Aa[arrayList.size()]);
    }

    @Nullable
    /* renamed from: d */
    private Aa m992d(@Nullable JSONObject jSONObject) {
        Aa aVar;
        boolean z = true;
        if (jSONObject != null) {
            aVar = new Aa();
            String optString = jSONObject.optString("device_address", null);
            if (optString != null) {
                aVar.f2138b = optString;
                z = false;
            }
            String optString2 = jSONObject.optString("device_name", null);
            if (optString2 != null) {
                aVar.f2139c = optString2;
                z = false;
            }
            C0867a e = m993e(jSONObject.optJSONObject("manufacturer_data"));
            if (e != null) {
                aVar.f2140d = e;
                z = false;
            }
            C0868b f = m994f(jSONObject.optJSONObject("service_data"));
            if (f != null) {
                aVar.f2141e = f;
                z = false;
            }
            C0869c g = m995g(jSONObject.optJSONObject("service_uuid"));
            if (g != null) {
                aVar.f2142f = g;
                z = false;
            }
        } else {
            aVar = null;
        }
        if (z) {
            return null;
        }
        return aVar;
    }

    @Nullable
    /* renamed from: e */
    private C0867a m993e(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        Integer b = vq.m4224b(jSONObject, "id");
        if (b == null) {
            return null;
        }
        C0867a aVar = new C0867a();
        aVar.f2143b = b.intValue();
        aVar.f2144c = vq.m4223a(jSONObject, "data", aVar.f2144c);
        aVar.f2145d = vq.m4223a(jSONObject, "data_mask", aVar.f2145d);
        return aVar;
    }

    @Nullable
    /* renamed from: f */
    private C0868b m994f(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("uuid", null);
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        C0868b bVar = new C0868b();
        bVar.f2146b = optString;
        bVar.f2147c = vq.m4223a(jSONObject, "data", bVar.f2147c);
        bVar.f2148d = vq.m4223a(jSONObject, "data_mask", bVar.f2148d);
        return bVar;
    }

    @Nullable
    /* renamed from: g */
    private C0869c m995g(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("uuid", null);
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        C0869c cVar = new C0869c();
        cVar.f2149b = optString;
        cVar.f2150c = jSONObject.optString("data_mask", cVar.f2150c);
        return cVar;
    }

    @Nullable
    /* renamed from: a */
    private Integer m988a(@NonNull JSONObject jSONObject, @NonNull String str, Map<String, Integer> map) {
        if (jSONObject.has(str)) {
            return (Integer) map.get(jSONObject.optString(str));
        }
        return null;
    }

    @VisibleForTesting
    public co(@NonNull nh nhVar) {
        this.f578e = nhVar;
    }
}
