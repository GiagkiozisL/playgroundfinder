package com.yandex.metrica.impl.interact;

import android.content.Context;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.uu;
import com.yandex.metrica.impl.ob.vd;
import com.yandex.metrica.impl.ob.vf;
import com.yandex.metrica.impl.ob.vg;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class CellularNetworkInfo {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f122a = "";

    public CellularNetworkInfo(Context context) {
        new vd(context, db.k().b()).mo2414a((vg) new vg() {
            /* renamed from: a */
            public void a(vf vfVar) {
                String str;
                String str2;
                String str3;
                String str4;
                String str5 = null;
                uu b = vfVar.mo2472b();
                if (b != null) {
                    String g = b.mo2423g();
                    String f = b.mo2422f();
                    Integer c = b.mo2419c();
                    Integer b2 = b.mo2418b();
                    Integer e = b.mo2421e();
                    Integer d = b.mo2420d();
                    Integer a = b.mo2416a();
                    HashMap hashMap = new HashMap();
                    hashMap.put("network_type", g);
                    hashMap.put("operator_name", f);
                    hashMap.put("country_code", b2 != null ? String.valueOf(b2) : null);
                    String str6 = "operator_id";
                    if (c != null) {
                        str = String.valueOf(c);
                    } else {
                        str = null;
                    }
                    hashMap.put(str6, str);
                    String str7 = "cell_id";
                    if (e != null) {
                        str2 = String.valueOf(e);
                    } else {
                        str2 = null;
                    }
                    hashMap.put(str7, str2);
                    String str8 = "lac";
                    if (d != null) {
                        str3 = String.valueOf(d);
                    } else {
                        str3 = null;
                    }
                    hashMap.put(str8, str3);
                    String str9 = "signal_strength";
                    if (a != null) {
                        str5 = String.valueOf(a);
                    }
                    hashMap.put(str9, str5);
                    StringBuilder sb = new StringBuilder();
                    String str10 = "";

                    Iterator var13 = hashMap.entrySet().iterator();
                    while(var13.hasNext()) {
                        Entry var14 = (Entry)var13.next();
                        String var15 = (String)var14.getValue();
                        if (!TextUtils.isEmpty(var15)) {
                            sb.append(str10);
                            sb.append((String)var14.getKey());
                            sb.append("=");
                            sb.append(var15);
                            str4 = "&";
                        }
                    }
//                    for (Entry entry : hashMap.entrySet()) {
//                        String str11 = (String) entry.getValue();
//                        if (!TextUtils.isEmpty(str11)) {
//                            sb.append(str10);
//                            sb.append((String) entry.getKey());
//                            sb.append("=");
//                            sb.append(str11);
//                            str4 = "&";
//                        } else {
//                            str4 = str10;
//                        }
//                        str10 = str4;
//                    }
                    CellularNetworkInfo.this.f122a = sb.toString();
                }
            }
        });
    }

    public String getCelluralInfo() {
        return this.f122a;
    }
}
