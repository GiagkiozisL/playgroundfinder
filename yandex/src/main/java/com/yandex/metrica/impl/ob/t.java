package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.t */
public class t {
    @Nullable

    /* renamed from: a */
    private Long f2526a;
    @NonNull

    /* renamed from: B */
    private wg f2527b;

    public t() {
        this(new wg());
    }

    /* renamed from: a */
    public void mo2216a() {
        this.f2526a = Long.valueOf(this.f2527b.c());
    }

    @VisibleForTesting
    t(@NonNull wg wgVar) {
        this.f2527b = wgVar;
    }
}
