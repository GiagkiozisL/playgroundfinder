package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a;
import com.yandex.metrica.impl.ob.rr.a.C0876h;
import com.yandex.metrica.impl.ob.uk.C1077a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.nf */
public class nf implements mq<uk, a> {

    /* renamed from: a */
    private ms f1502a = new ms();

    /* renamed from: B */
    private mn f1503b = new mn();

    /* renamed from: a */
    private ne f1504c = new ne();

    /* renamed from: d */
    private mz f1505d = new mz();

    /* renamed from: e */
    private nb f1506e = new nb();

    /* renamed from: f */
    private mr f1507f = new mr();

    /* renamed from: g */
    private mx f1508g = new mx();

    /* renamed from: h */
    private ng f1509h = new ng();

    /* renamed from: i */
    private nc f1510i = new nc();

    /* renamed from: j */
    private mt f1511j = new mt();

    /* renamed from: k */
    private nh f1512k = new nh();

    @NonNull
    /* renamed from: a */
    public a b(@NonNull uk ukVar) {
        a aVar = new a();
        aVar.f2094C = ukVar.obtainServerTime;
        aVar.f2095D = ukVar.firstStartupServerTime;
        if (ukVar.uuid != null) {
            aVar.f2100b = ukVar.uuid;
        }
        if (ukVar.deviceId != null) {
            aVar.f2123y = ukVar.deviceId;
        }
        if (ukVar.deviceID2 != null) {
            aVar.f2092A = ukVar.deviceID2;
        }
        if (ukVar.deviceIDHash != null) {
            aVar.f2124z = ukVar.deviceIDHash;
        }
        if (ukVar.hostUrlsFromStartup != null) {
            aVar.f2106h = (String[]) ukVar.hostUrlsFromStartup.toArray(new String[ukVar.hostUrlsFromStartup.size()]);
        }
        if (ukVar.hostUrlsFromClient != null) {
            aVar.f2107i = (String[]) ukVar.hostUrlsFromClient.toArray(new String[ukVar.hostUrlsFromClient.size()]);
        }
        if (ukVar.reportUrls != null) {
            aVar.f2102d = (String[]) ukVar.reportUrls.toArray(new String[ukVar.reportUrls.size()]);
        }
        if (ukVar.locationUrls != null) {
            aVar.f2105g = (String[]) ukVar.locationUrls.toArray(new String[ukVar.locationUrls.size()]);
        }
        if (ukVar.diagnosticUrls != null) {
            aVar.f2118t = (String[]) ukVar.diagnosticUrls.toArray(new String[ukVar.diagnosticUrls.size()]);
        }
        if (ukVar.foregroundLocationCollectionConfig != null) {
            aVar.f2109k = this.f1502a.b(ukVar.foregroundLocationCollectionConfig);
        }
        if (ukVar.backgroundLocationCollectionConfig != null) {
            aVar.f2110l = this.f1503b.b(ukVar.backgroundLocationCollectionConfig);
        }
        if (ukVar.socketConfig != null) {
            aVar.f2111m = this.f1504c.b(ukVar.socketConfig);
        }
        if (ukVar.permissionsCollectingConfig != null) {
            aVar.f2097F = this.f1505d.b(ukVar.permissionsCollectingConfig);
        }
        if (ukVar.encodedClidsFromResponse != null) {
            aVar.f2113o = ukVar.encodedClidsFromResponse;
        }
        if (ukVar.getAdUrl != null) {
            aVar.f2103e = ukVar.getAdUrl;
        }
        if (ukVar.reportAdUrl != null) {
            aVar.f2104f = ukVar.reportAdUrl;
        }
        if (ukVar.sdkListUrl != null) {
            aVar.f2093B = ukVar.sdkListUrl;
        }
        if (ukVar.distributionReferrer != null) {
            aVar.f2116r = ukVar.distributionReferrer;
        }
        aVar.f2108j = this.f1507f.b(ukVar.collectingFlags);
        if (ukVar.lastStartupRequestClids != null) {
            aVar.f2114p = ukVar.lastStartupRequestClids;
        }
        aVar.f2115q = ukVar.startupResponseClidsMatchClientClids;
        aVar.f2101c = ukVar.t;
        aVar.f2120v = ukVar.hadFirstStartup;
        if (ukVar.w != null) {
            aVar.f2112n = m2556a(ukVar.w);
        }
        if (ukVar.countryInit != null) {
            aVar.f2117s = ukVar.countryInit;
        }
        if (ukVar.permissions != null) {
            aVar.f2121w = this.f1508g.b(ukVar.permissions);
        }
        if (ukVar.sdkFingerprintingConfig != null) {
            aVar.f2122x = this.f1510i.b(ukVar.sdkFingerprintingConfig);
        }
        if (ukVar.statSending != null) {
            aVar.f2119u = this.f1509h.b(ukVar.statSending);
        }
        aVar.f2096E = ukVar.outdated;
        if (ukVar.identityLightCollectingConfig != null) {
            aVar.f2098G = this.f1511j.b(ukVar.identityLightCollectingConfig);
        }
        if (ukVar.bleCollectingConfig != null) {
            aVar.f2099H = this.f1512k.b(ukVar.bleCollectingConfig);
        }
        return aVar;
    }

    @NonNull
    /* renamed from: a */
    public uk a(@NonNull a aVar) {
        C1077a c = new C1077a(this.f1507f.a(aVar.f2108j)).mo2370a(aVar.f2100b).setDeviceId(aVar.f2123y).mo2379c(aVar.f2092A).mo2382d(aVar.f2124z).mo2390h(aVar.f2113o).mo2384e(aVar.f2103e).mo2371a(Arrays.asList(aVar.f2102d)).mo2376b(Arrays.asList(aVar.f2105g)).mo2383d(Arrays.asList(aVar.f2107i)).mo2380c(Arrays.asList(aVar.f2106h)).mo2386f(aVar.f2104f).mo2388g(aVar.f2093B).mo2385e(Arrays.asList(aVar.f2118t)).mo2392j(aVar.f2116r).mo2391i(aVar.f2114p).mo2377b(aVar.f2115q).mo2361a(aVar.f2101c).mo2372a(aVar.f2120v).mo2387f(m2555a(aVar.f2112n)).mo2374b(aVar.f2094C).mo2378c(aVar.f2095D).mo2393k(aVar.f2117s).mo2381c(aVar.f2096E);
        if (aVar.f2109k != null) {
            c.mo2363a(this.f1502a.a(aVar.f2109k));
        }
        if (aVar.f2110l != null) {
            c.mo2362a(this.f1503b.a(aVar.f2110l));
        }
        if (aVar.f2111m != null) {
            c.mo2368a(this.f1504c.a(aVar.f2111m));
        }
        if (aVar.f2097F != null) {
            c.mo2366a(this.f1505d.a(aVar.f2097F));
        }
        if (aVar.f2121w != null) {
            c.mo2389g(this.f1508g.a(aVar.f2121w));
        }
        if (aVar.f2122x != null) {
            c.mo2367a(this.f1510i.a(aVar.f2122x));
        }
        if (aVar.f2119u != null) {
            c.mo2369a(this.f1509h.a(aVar.f2119u));
        }
        if (aVar.f2098G != null) {
            c.mo2365a(this.f1511j.a(aVar.f2098G));
        }
        if (aVar.f2099H != null) {
            c.mo2364a(this.f1512k.a(aVar.f2099H));
        }
        return c.mo2373a();
    }

    @NonNull
    /* renamed from: a */
    private C0876h[] m2556a(@NonNull List<cq.a> list) {
        C0876h[] hVarArr = new C0876h[list.size()];
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return hVarArr;
            }
            hVarArr[i2] = this.f1506e.b((cq.a) it.next());
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    private List<cq.a> m2555a(@NonNull C0876h[] hVarArr) {
        ArrayList arrayList = new ArrayList(hVarArr.length);
        for (C0876h a : hVarArr) {
            arrayList.add(this.f1506e.a(a));
        }
        return arrayList;
    }
}
