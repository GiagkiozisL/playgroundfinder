package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import me.android.ydx.DataManager;

public class qa {

    private Context f1756a;
    private String pkgName;

    public qa(Context context) {
        this.f1756a = context;
        this.pkgName = DataManager.getInstance().getCustomData().app_id;
    }

    public void mo1671a() {
        SharedPreferences a = ql.m2970a(this.f1756a, "_bidoptpreferences");
        if (a.getAll().size() > 0) {
            m2868b(a);
            m2865a(a);
            a.edit().clear().apply();
        }
    }

    private void m2865a(SharedPreferences var1) {
        Map var2 = var1.getAll();
        if (var2.size() > 0) {
            List var3 = m2864a(var2, qi.f1830e.mo1742a());
            Iterator var4 = var3.iterator();

            while(var4.hasNext()) {
                String var5 = (String)var4.next();
                String var6 = var1.getString((new qk(qi.f1830e.mo1742a(), var5)).mo1744b(), (String)null);
                qi var7 = new qi(this.f1756a, var5);
                if (!TextUtils.isEmpty(var6) && TextUtils.isEmpty(var7.mo1732b((String)null))) {
                    var7.mo1739i(var6).mo1699j();
                }
            }
        }
//        Map all = sharedPreferences.getAll();
//        if (all.size() > 0) {
//            for (String str : m2864a(all, qi.f1830e.mo1742a())) {
//                String string = sharedPreferences.getString(new qk(qi.f1830e.mo1742a(), str).mo1744b(), null);
//                qi qiVar = new qi(this.f1756a, str);
//                if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(qiVar.mo1732b(null))) {
//                    qiVar.mo1739i(string).mo1699j();
//                }
//            }
//        }
    }

    private List<String> m2864a(Map<String, ?> map, String str) {
        ArrayList arrayList = new ArrayList();
        for (String str2 : map.keySet()) {
            if (str2.startsWith(str)) {
                arrayList.add(str2.replace(str, ""));
            }
        }
        return arrayList;
    }

    private void m2868b(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString(qi.f1829d.mo1742a(), null);
        qi qiVar = new qi(this.f1756a);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(qiVar.mo1731a((String) null))) {
            qiVar.mo1740j(string).mo1699j();
            sharedPreferences.edit().remove(qi.f1829d.mo1742a()).apply();
        }
    }

    public void mo1672b() {
        lf e = ld.m2146a(this.f1756a).mo1240e();
        SharedPreferences a = ql.m2970a(this.f1756a, "_startupserviceinfopreferences");
        m2869b(e, a);
        m2870c(e, a);
//        m2867a(e, this.f1756a.getPackageName());
        m2867a(e, pkgName);
        m2866a(e, a);
    }

    private void m2866a(lf lfVar, SharedPreferences sharedPreferences) {
        for (String a : m2864a(sharedPreferences.getAll(), qi.f1830e.mo1742a())) {
            m2867a(lfVar, a);
        }
    }

    private void m2867a(lf lfVar, String pkgName) {
        lz lzVar = new lz(lfVar, pkgName);
        qi qiVar = new qi(this.f1756a, pkgName);
        String b = qiVar.mo1732b(null);
        if (!TextUtils.isEmpty(b)) {
            lzVar.mo1398a(b);
        }
        String a = qiVar.mo1684a();
        if (!TextUtils.isEmpty(a)) {
            lzVar.mo1409h(a);
        }
        String d = qiVar.mo1735d(null);
        if (!TextUtils.isEmpty(d)) {
            lzVar.mo1408g(d);
        }
        String f = qiVar.mo1737f(null);
        if (!TextUtils.isEmpty(f)) {
            lzVar.mo1406e(f);
        }
        String g = qiVar.mo1738g(null);
        if (!TextUtils.isEmpty(g)) {
            lzVar.mo1405d(g);
        }
        String c = qiVar.mo1734c(null);
        if (!TextUtils.isEmpty(c)) {
            lzVar.mo1407f(c);
        }
        long a2 = qiVar.mo1730a(-1);
        if (a2 != -1) {
            lzVar.mo1397a(a2);
        }
        String e = qiVar.mo1736e(null);
        if (!TextUtils.isEmpty(e)) {
            lzVar.mo1404c(e);
        }
        lzVar.mo1364q();
        qiVar.mo1733b();
    }

    private void m2869b(lf lfVar, SharedPreferences sharedPreferences) {
        lz lzVar = new lz(lfVar, null);
        String string = sharedPreferences.getString(qi.f1829d.mo1742a(), null);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(lzVar.mo1401a().deviceId)) {
            lzVar.mo1402b(string).mo1364q();
            sharedPreferences.edit().remove(qi.f1829d.mo1742a()).apply();
        }
    }

    private void m2870c(lf lfVar, SharedPreferences sharedPreferences) {
//        lz lzVar = new lz(lfVar, this.f1756a.getPackageName());
        lz lzVar = new lz(lfVar, this.pkgName);
        boolean z = sharedPreferences.getBoolean(qi.f1831f.mo1742a(), false);
        if (z) {
            lzVar.mo1400a(z).mo1364q();
        }
    }
}
