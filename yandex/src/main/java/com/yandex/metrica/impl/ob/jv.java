package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.jt.C0513a;
import com.yandex.metrica.impl.ob.np.C0678a;

public class jv {
    @NonNull

    private final Context ctx;
    @NonNull

    private final jt f1176b;

    @NonNull
    public final ju f1177c;

    @NonNull
    private final C0518a f1178d;

    @NonNull
    private final jq f1179e;

    public static class C0518a {
        public oc mo1135a(@NonNull Context context) {
            return ((uk) C0678a.m2599a(uk.class).a(context).a()).backgroundLocationCollectionConfig;
        }
    }

    public jv(@NonNull Context context, @NonNull xh xhVar, @NonNull jp jpVar) {
        this(context, xhVar, jpVar, new ju(context));
    }

    public void mo1133a() {
        m2000a(this.f1178d.mo1135a(this.ctx));
    }

    private void m2000a(@Nullable oc ocVar) {
        if (ocVar != null) {
            boolean z = ocVar.f1577k;
            boolean z2 = ocVar.f1564c;
            Long a = this.f1179e.mo1118a(ocVar.f1565d);
            if (!z || a == null || a.longValue() <= 0) {
                m2001b();
            } else {
                this.f1176b.mo1126a(a.longValue(), z2);
            }
        }
    }

    private void m2001b() {
        this.f1176b.mo1124a();
    }

    public void mo1134a(@Nullable final jw jwVar) {
        oc a = this.f1178d.mo1135a(this.ctx);
        if (a != null) {
            long j = a.f1562a;
            if (j > 0) {
                this.f1177c.mo1130a();
                this.f1176b.mo1125a(j, (C0513a) new C0513a() {
                    public void mo1128a() {
                        jv.this.f1177c.mo1129a();
                        jv.this.m2002b(jwVar);
                    }
                });
            } else {
                m2002b(jwVar);
            }
        } else {
            m2002b(jwVar);
        }
        m2000a(a);
    }

    public void m2002b(@Nullable jw jwVar) {
        if (jwVar != null) {
            jwVar.a();
        }
    }

    private jv(@NonNull Context context, @NonNull xh xhVar, @NonNull jp jpVar, @NonNull ju juVar) {
        this(context, new jt(xhVar, jpVar), juVar, new C0518a(), new jq(context));
    }

    @VisibleForTesting
    jv(@NonNull Context context, @NonNull jt jtVar, @NonNull ju juVar, @NonNull C0518a aVar, @NonNull jq jqVar) {
        this.ctx = context;
        this.f1176b = jtVar;
        this.f1177c = juVar;
        this.f1178d = aVar;
        this.f1179e = jqVar;
    }
}
