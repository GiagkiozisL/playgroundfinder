package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.ag */
public class ag {
    @NonNull

    /* renamed from: a */
    private final ct f210a;
    @NonNull

    /* renamed from: B */
    private final o f211b;

    public ag(@NonNull Context context) {
        this(new ct(context, "com.yandex.android.appmetrica.build_id"), new o(context, "com.yandex.android.appmetrica.is_offline"));
    }

    @VisibleForTesting
    ag(@NonNull ct ctVar, @NonNull o oVar) {
        this.f210a = ctVar;
        this.f211b = oVar;
    }

    @Nullable
    /* renamed from: a */
    public String mo261a() {
        return (String) this.f210a.mo603a();
    }

    @Nullable
    /* renamed from: B */
    public Boolean mo262b() {
        return (Boolean) this.f211b.mo603a();
    }
}
