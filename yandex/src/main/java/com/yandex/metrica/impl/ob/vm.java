package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.vm */
public class vm {
    /* renamed from: a */
    public byte[] mo2492a(@NonNull byte[] bArr) throws IOException {
        return am.compressSeed(bArr);
    }

    /* renamed from: B */
    public byte[] mo2493b(@NonNull byte[] bArr) throws IOException {
        return am.compressSeed_(bArr);
    }
}
