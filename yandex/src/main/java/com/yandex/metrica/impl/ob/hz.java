package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.hz */
public abstract class hz<BaseHandler> {

    /* renamed from: a */
    private final ig f1025a;

    /* renamed from: a */
    public abstract void mo977a(@NonNull List<BaseHandler> list);

    public hz(ig igVar) {
        this.f1025a = igVar;
    }

    /* renamed from: a */
    public ig mo984a() {
        return this.f1025a;
    }
}
