package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.Random;

/* renamed from: com.yandex.metrica.impl.ob.vt */
public class vt {
    @NonNull

    /* renamed from: a */
    private final Random f2906a;

    public vt() {
        this(new Random());
    }

    public vt(@NonNull Random random) {
        this.f2906a = random;
    }

    /* renamed from: a */
    public long mo2502a(long j, long j2) {
        long j3 = 0;
        if (j >= j2) {
            throw new IllegalArgumentException("min should be less than max");
        }
        long nextLong = this.f2906a.nextLong();
        if (nextLong != Long.MIN_VALUE) {
            j3 = nextLong < 0 ? -nextLong : nextLong;
        }
        return (j3 % (j2 - j)) + j;
    }
}
