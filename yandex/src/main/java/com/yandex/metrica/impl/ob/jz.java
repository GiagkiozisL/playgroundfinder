package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.jz */
public class jz extends jy {
    @NonNull

    /* renamed from: a */
    private final jv f1188a;

    public jz(@NonNull Context context, @NonNull jv jvVar) {
        super(context);
        this.f1188a = jvVar;
    }

    /* renamed from: a */
    public void mo1136a(@Nullable Bundle bundle, @Nullable jw jwVar) {
        this.f1188a.mo1134a(jwVar);
    }
}
