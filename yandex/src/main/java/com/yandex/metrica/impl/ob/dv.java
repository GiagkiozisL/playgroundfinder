package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.dv */
public class dv<T extends dt> {
    @NonNull

    /* renamed from: a */
    private final du<T> f746a;
    @Nullable

    /* renamed from: B */
    private final ds<T> f747b;

    /* renamed from: com.yandex.metrica.impl.ob.dv$a */
    public static final class C0292a<T extends dt> {
        @NonNull

        /* renamed from: a */
        final du<T> f748a;
        @Nullable

        /* renamed from: B */
                ds<T> f749b;

        C0292a(@NonNull du<T> duVar) {
            this.f748a = duVar;
        }

        @NonNull
        /* renamed from: a */
        public C0292a<T> mo735a(@NonNull ds<T> dsVar) {
            this.f749b = dsVar;
            return this;
        }

        @NonNull
        /* renamed from: a */
        public dv<T> mo736a() {
            return new dv<>(this);
        }
    }

    private dv(@NonNull C0292a aVar) {
        this.f746a = aVar.f748a;
        this.f747b = aVar.f749b;
    }

    /* renamed from: a */
    public void mo733a(@NonNull dt dtVar) {
        this.f746a.a((T)dtVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public final boolean mo734b(@NonNull dt dtVar) {
        if (this.f747b == null) {
            return false;
        }
        return this.f747b.a((T)dtVar);
    }

    @NonNull
    /* renamed from: a */
    public static <T extends dt> C0292a<T> m1388a(@NonNull du<T> duVar) {
        return new C0292a<>(duVar);
    }
}
