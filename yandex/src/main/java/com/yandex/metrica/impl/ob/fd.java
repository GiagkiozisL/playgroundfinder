package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.fd */
public class fd<COMPONENT extends ew & et> implements eq<fc> {
    @NonNull

    /* renamed from: a */
    private final ft<COMPONENT> f888a;

    public fd(@NonNull ft<COMPONENT> ftVar) {
        this.f888a = ftVar;
    }

    /* renamed from: a */
    public fc b(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar) {
        return new fc(context, ekVar, egVar, this.f888a);
    }
}
