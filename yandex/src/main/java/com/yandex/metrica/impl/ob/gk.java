package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.gk */
public class gk {
    @Nullable

    /* renamed from: a */
    private Long f971a;

    /* renamed from: B */
    private int f972b;
    @NonNull

    /* renamed from: a */
    private wh f973c;

    /* renamed from: com.yandex.metrica.impl.ob.gk$a */
    public static class C0392a {

        /* renamed from: a */
        public final long f974a;

        /* renamed from: B */
        public final long f975b;

        /* renamed from: a */
        public final int f976c;

        public C0392a(long j, long j2, int i) {
            this.f974a = j;
            this.f976c = i;
            this.f975b = j2;
        }
    }

    public gk() {
        this(new wg());
    }

    public gk(@NonNull wh whVar) {
        this.f973c = whVar;
    }

    /* renamed from: a */
    public C0392a mo951a() {
        if (this.f971a == null) {
            this.f971a = Long.valueOf(this.f973c.b());
        }
        C0392a aVar = new C0392a(this.f971a.longValue(), this.f971a.longValue(), this.f972b);
        this.f972b++;
        return aVar;
    }
}
