package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.IParamsCallback;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.yandex.metrica.impl.ob.tu */
public class tu implements tx {

    /* renamed from: a */
    private static final IIdentifierCallback f2652a = new IIdentifierCallback() {
        public void onReceive(Map<String, String> map) {
        }

        public void onRequestError(Reason reason) {
        }
    };
    @NonNull

    /* renamed from: B */
    private final AtomicReference<IIdentifierCallback> f2653b;

    public tu(@NonNull IIdentifierCallback iIdentifierCallback) {
        this.f2653b = new AtomicReference<>(iIdentifierCallback);
    }

    /* renamed from: a */
    public void a(Map<String, String> map) {
        ((IIdentifierCallback) this.f2653b.getAndSet(f2652a)).onReceive(map);
    }

    /* renamed from: a */
    public void a(@NonNull IParamsCallback.Reason reason, Map<String, String> map) {
        ((IIdentifierCallback) this.f2653b.getAndSet(f2652a)).onRequestError(m3851a(reason));
    }

    @NonNull
    /* renamed from: a */
    private Reason m3851a(@NonNull IParamsCallback.Reason reason) {
        switch (reason) {
            case NETWORK:
                return Reason.NETWORK;
            case INVALID_RESPONSE:
                return Reason.INVALID_RESPONSE;
            default:
                return Reason.UNKNOWN;
        }
    }
}
