package com.yandex.metrica.impl.ob;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.yandex.metrica.impl.ob.xm */
public class xm implements ThreadFactory {

    /* renamed from: a */
    private static final AtomicInteger f2972a = new AtomicInteger(0);

    /* renamed from: B */
    private final String f2973b;

    public xm(String str) {
        this.f2973b = str;
    }

    /* renamed from: a */
    public xl newThread(Runnable runnable) {
        return new xl(runnable, m4446c());
    }

    /* renamed from: a */
    private String m4446c() {
        return m4444a(this.f2973b);
    }

    /* renamed from: a */
    public xk mo2612a() {
        return new xk(m4446c());
    }

    /* renamed from: a */
    public static xl m4443a(String str, Runnable runnable) {
        return new xm(str).newThread(runnable);
    }

    /* renamed from: a */
    public static String m4444a(String str) {
        return str + "-" + m4445b();
    }

    /* renamed from: B */
    public static int m4445b() {
        return f2972a.incrementAndGet();
    }
}
