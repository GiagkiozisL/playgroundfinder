package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.em.C0315a;
import com.yandex.metrica.impl.ob.ff.C0351a;
import com.yandex.metrica.impl.ob.jd.C0484a;
import com.yandex.metrica.impl.ob.st.C0984a;
import com.yandex.metrica.impl.ob.st.C0986c;
import com.yandex.metrica.impl.ob.st.C0987d;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.eo */
class eo {
    @NonNull

    /* renamed from: a */
    protected final Context f851a;
    @NonNull

    /* renamed from: B */
    private final C0328a f852b;
    @NonNull

    /* renamed from: a */
    private final C0329b f853c;
    @NonNull

    /* renamed from: d */
    private final ek f854d;
    @NonNull

    /* renamed from: e */
    private final C0306a f855e;
    @NonNull

    /* renamed from: f */
    private final un f856f;
    @NonNull

    /* renamed from: g */
    private final uk f857g;
    @NonNull

    /* renamed from: h */
    private final C0987d f858h;
    @NonNull

    /* renamed from: i */
    private final wy f859i;
    @NonNull

    /* renamed from: j */
    private final bl f860j;
    @NonNull

    /* renamed from: k */
    private final xh f861k;

    /* renamed from: l */
    private final int f862l;

    /* renamed from: com.yandex.metrica.impl.ob.eo$a */
    static class C0328a {
        @Nullable

        /* renamed from: a */
        private final String f865a;

        C0328a(@Nullable String str) {
            this.f865a = str;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public vz mo860a() {
            return vr.m4237a(this.f865a);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public vp mo861b() {
            return vr.m4239b(this.f865a);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.eo$B */
    static class C0329b {
        @NonNull

        /* renamed from: a */
        private final ek f866a;
        @NonNull

        /* renamed from: B */
        private final ld f867b;

        C0329b(@NonNull Context context, @NonNull ek ekVar) {
            this(ekVar, ld.m2146a(context));
        }

        @VisibleForTesting
        C0329b(@NonNull ek ekVar, @NonNull ld ldVar) {
            this.f866a = ekVar;
            this.f867b = ldVar;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        /* renamed from: a */
        public lu mo862a() {
            return new lu(this.f867b.mo1236b(this.f866a));
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        /* renamed from: B */
        public lw mo863b() {
            return new lw(this.f867b.mo1236b(this.f866a));
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        /* renamed from: a */
        public ly mo864c() {
            return new ly(this.f867b.mo1238c());
        }
    }

    eo(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull un unVar, @NonNull uk ukVar, @NonNull C0987d dVar, @NonNull bl blVar, @NonNull xh xhVar, int i) {
        this(context, ekVar, aVar, unVar, ukVar, dVar, blVar, xhVar, new wy(context), i, new C0328a(aVar.f771d), new C0329b(context, ekVar));
    }

    @VisibleForTesting
    eo(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull un unVar, @NonNull uk ukVar, @NonNull C0987d dVar, @NonNull bl blVar, @NonNull xh xhVar, @NonNull wy wyVar, int i, @NonNull C0328a aVar2, @NonNull C0329b bVar) {
        this.f851a = context;
        this.f854d = ekVar;
        this.f855e = aVar;
        this.f856f = unVar;
        this.f857g = ukVar;
        this.f858h = dVar;
        this.f860j = blVar;
        this.f861k = xhVar;
        this.f859i = wyVar;
        this.f862l = i;
        this.f852b = aVar2;
        this.f853c = bVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public C0328a mo844a() {
        return this.f852b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: B */
    public C0329b mo854b() {
        return this.f853c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public io mo856c() {
        return new io(this.f851a, this.f854d, this.f862l);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public kz mo848a(@NonNull en enVar) {
        return new kz(enVar, ld.m2146a(this.f851a).mo1233a(this.f854d));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: B */
    public cc<en> mo853b(@NonNull en enVar) {
        return new cc<>(enVar, this.f856f.mo2410a(), this.f860j, this.f861k);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public fe mo855c(@NonNull en enVar) {
        return new fe(new C0986c(enVar, this.f858h), this.f857g, new C0984a(this.f855e));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public jd mo847a(@NonNull en enVar, @NonNull lw lwVar, @NonNull C0484a aVar) {
        return new jd(enVar, new jc(lwVar), aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public ff mo845a(@NonNull lw lwVar, @NonNull jd jdVar, @NonNull kz kzVar, @NonNull i iVar, @NonNull final cc ccVar) {
        return new ff(lwVar, jdVar, kzVar, iVar, this.f859i, this.f862l, new C0351a() {
            /* renamed from: a */
            public void mo859a() {
                ccVar.mo558b();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: d */
    public gp mo857d(@NonNull en enVar) {
        return new gp(enVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public gs<hd, en> mo846a(@NonNull en enVar, @NonNull gp gpVar) {
        return new gs<>(gpVar, enVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: e */
    public C0315a mo858e(@NonNull en enVar) {
        return new C0315a(enVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public nu mo849a(@NonNull kz kzVar) {
        return new nu(kzVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public nz mo851a(@NonNull kz kzVar, @NonNull fe feVar) {
        return new nz(kzVar, feVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public nx mo850a(@NonNull List<nv> list, @NonNull ny nyVar) {
        return new nx(list, nyVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public q mo852a(@NonNull lw lwVar) {
        return new q(this.f851a, lwVar);
    }
}
