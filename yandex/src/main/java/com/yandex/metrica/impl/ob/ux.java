package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.telephony.CellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.ux */
class ux extends ut implements p {
    /* access modifiers changed from: private */
    @Nullable

    /* renamed from: a */
    public final TelephonyManager f2842a;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public PhoneStateListener f2843b;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public boolean f2844c;

    /* renamed from: d */
    private uk f2845d;

    /* renamed from: e */
    private final C0740a<vf> f2846e;

    /* renamed from: f */
    private final C0740a<uu[]> f2847f;
    @NonNull

    /* renamed from: g */
    private final xh f2848g;

    /* renamed from: h */
    private final Context f2849h;

    /* renamed from: i */
    private final uw f2850i;

    /* renamed from: j */
    private final vc f2851j;

    /* renamed from: k */
    private final uz f2852k;
    @NonNull

    /* renamed from: l */
    private final px f2853l;
    @NonNull

    /* renamed from: m */
    private pr f2854m;

    /* renamed from: com.yandex.metrica.impl.ob.ux$a */
    private class C1106a extends PhoneStateListener {
        private C1106a() {
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            ux.this.m4094c(signalStrength);
        }
    }

    protected ux(@NonNull Context context, @NonNull xh xhVar) {
        this(context, new px(), xhVar);
    }

    protected ux(@NonNull Context context, @NonNull px pxVar, @NonNull xh xhVar) {
        this(context, pxVar, new pr(pxVar.mo1666a()), xhVar);
    }

    protected ux(@NonNull Context context, @NonNull px pxVar, @NonNull pr prVar, @NonNull xh xhVar) {
        TelephonyManager telephonyManager;
        this.f2844c = false;
        this.f2846e = new C0740a<>();
        this.f2847f = new C0740a<>();
        this.f2849h = context;
        try {
            telephonyManager = (TelephonyManager) context.getSystemService("phone");
        } catch (Throwable th) {
            telephonyManager = null;
        }
        this.f2842a = telephonyManager;
        this.f2848g = xhVar;
        this.f2848g.a((Runnable) new Runnable() {
            public void run() {
                ux.this.f2843b = new C1106a();
            }
        });
        this.f2850i = new uw(this, prVar);
        this.f2851j = new vc(this, prVar);
        this.f2852k = new uz(this, prVar);
        this.f2853l = pxVar;
        this.f2854m = prVar;
    }

    /* renamed from: a */
    public synchronized void a() {
        this.f2848g.a((Runnable) new Runnable() {
            public void run() {
                if (!ux.this.f2844c) {
                    ux.this.f2844c = true;
                    if (ux.this.f2843b != null && ux.this.f2842a != null) {
                        try {
                            ux.this.f2842a.listen(ux.this.f2843b, 256);
                        } catch (Throwable th) {
                        }
                    }
                }
            }
        });
    }

    /* renamed from: B */
    public synchronized void b() {
        this.f2848g.a((Runnable) new Runnable() {
            public void run() {
                if (ux.this.f2844c) {
                    ux.this.f2844c = false;
                    dr.a().mo726a((Object) ux.this);
                    if (ux.this.f2843b != null && ux.this.f2842a != null) {
                        try {
                            ux.this.f2842a.listen(ux.this.f2843b, 0);
                        } catch (Throwable th) {
                        }
                    }
                }
            }
        });
    }

    /* renamed from: a */
    public synchronized void mo2414a(vg vgVar) {
        if (vgVar != null) {
            vgVar.a(mo2445e());
        }
    }

    /* renamed from: a */
    public synchronized void mo2413a(uv uvVar) {
        if (uvVar != null) {
            uvVar.a(m4095j());
        }
    }

    @Nullable
    /* renamed from: a */
    public TelephonyManager mo2443c() {
        return this.f2842a;
    }

    /* renamed from: d */
    public Context mo2444d() {
        return this.f2849h;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: e */
    public synchronized vf mo2445e() {
        vf vfVar;
        if (this.f2846e.mo1623b() || this.f2846e.mo1624c()) {
            vf vfVar2 = new vf(this.f2850i, this.f2851j, this.f2852k);
            uu b = vfVar2.mo2472b();
            if (b != null && b.mo2416a() == null && !this.f2846e.mo1623b()) {
                uu b2 = ((vf) this.f2846e.mo1620a()).mo2472b();
                if (b2 != null) {
                    vfVar2.mo2472b().mo2417a(b2.mo2416a());
                }
            }
            this.f2846e.mo1621a(vfVar2);
            vfVar = vfVar2;
        } else {
            vfVar = (vf) this.f2846e.mo1620a();
        }
        return vfVar;
    }

    /* renamed from: j */
    private synchronized uu[] m4095j() {
        uu[] uuVarArr;
        if (this.f2847f.mo1623b() || this.f2847f.mo1624c()) {
            uuVarArr = mo2446f();
            this.f2847f.mo1621a(uuVarArr);
        } else {
            uuVarArr = (uu[]) this.f2847f.mo1620a();
        }
        return uuVarArr;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    @NonNull
    /* renamed from: f */
    public uu[] mo2446f() {
        ArrayList arrayList = new ArrayList();
        if (cx.a(17) && this.f2854m.mo1650a(this.f2849h)) {
            try {
                List allCellInfo = this.f2842a.getAllCellInfo();
                if (!cx.a((Collection) allCellInfo)) {
                    for (int i = 0; i < allCellInfo.size(); i++) {
                        uu a = m4087a((CellInfo) allCellInfo.get(i));
                        if (a != null) {
                            arrayList.add(a);
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
        if (arrayList.size() >= 1) {
            return (uu[]) arrayList.toArray(new uu[arrayList.size()]);
        }
        uu b = mo2445e().mo2472b();
        if (b == null) {
            return new uu[0];
        }
        return new uu[]{b};
    }

    @Nullable
    @TargetApi(17)
    /* renamed from: a */
    private uu m4087a(CellInfo cellInfo) {
        return uu.m4040a(cellInfo);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void m4094c(SignalStrength signalStrength) {
        if (!this.f2846e.mo1623b() && !this.f2846e.mo1624c()) {
            uu b = ((vf) this.f2846e.mo1620a()).mo2472b();
            if (b != null) {
                b.mo2417a(Integer.valueOf(m4085a(signalStrength)));
            }
        }
    }

    @VisibleForTesting
    /* renamed from: a */
    static int m4085a(SignalStrength signalStrength) {
        if (signalStrength.isGsm()) {
            return m4091b(signalStrength);
        }
        int cdmaDbm = signalStrength.getCdmaDbm();
        int evdoDbm = signalStrength.getEvdoDbm();
        if (-120 != evdoDbm) {
            return -120 == cdmaDbm ? evdoDbm : Math.min(cdmaDbm, evdoDbm);
        }
        return cdmaDbm;
    }

    @VisibleForTesting
    /* renamed from: B */
    static int m4091b(SignalStrength signalStrength) {
        int gsmSignalStrength = signalStrength.getGsmSignalStrength();
        if (99 == gsmSignalStrength) {
            return -1;
        }
        return (gsmSignalStrength * 2) - 113;
    }

    /* renamed from: k */
    private synchronized boolean m4096k() {
        return this.f2845d != null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public synchronized boolean mo2447g() {
        return m4096k() && this.f2845d.collectingFlags.f2669o;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: h */
    public synchronized boolean mo2448h() {
        return m4096k() && this.f2845d.collectingFlags.f2668n;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: i */
    public synchronized boolean mo2449i() {
        return m4096k() && this.f2845d.collectingFlags.f2667m;
    }

    /* renamed from: a */
    public void mo2412a(@NonNull uk ukVar) {
        this.f2845d = ukVar;
        this.f2853l.mo1667a(ukVar);
        this.f2854m.mo1649a(this.f2853l.mo1666a());
    }

    /* renamed from: a */
    public void mo2415a(boolean z) {
        this.f2853l.mo1668a(z);
        this.f2854m.mo1649a(this.f2853l.mo1666a());
    }
}
