package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.yi */
public final class yi {

    /* renamed from: a */
    private final Class<? extends yk> f3008a;

    /* renamed from: B */
    private final boolean f3009b;

    /* renamed from: a */
    private final String f3010c;

    private yi(@NonNull yk<?> ykVar, boolean z, @NonNull String str) {
        this.f3008a = ykVar.getClass();
        this.f3009b = z;
        this.f3010c = str;
    }

    /* renamed from: a */
    public final boolean a() {
        return this.f3009b;
    }

    @NonNull
    /* renamed from: B */
    public final String b() {
        return this.f3010c;
    }

    /* renamed from: a */
    public static final yi a(@NonNull yk<?> ykVar) {
        return new yi(ykVar, true, "");
    }

    /* renamed from: a */
    public static final yi a(@NonNull yk<?> ykVar, @NonNull String str) {
        return new yi(ykVar, false, str);
    }
}
