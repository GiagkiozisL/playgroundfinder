package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.vo */
public class vo {
    @Nullable
    /* renamed from: a */
    public String mo2495a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return cu.m1152a(am.m361c(str.getBytes()));
    }

    @NonNull
    /* renamed from: a */
    public String genUUID() {
        return DataManager.getInstance().getCustomData().uuid;
//        return UUID.randomUUID().toString().replace("-", "").toLowerCase(Locale.US);
    }
}
