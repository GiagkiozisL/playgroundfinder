package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.YandexMetricaConfig;

/* renamed from: com.yandex.metrica.impl.ob.sd */
public class sd extends sf {

    /* renamed from: g */
    private static final yk<YandexMetricaConfig> f2379g = new yg(new yf("Config"));

    /* renamed from: h */
    private static final yk<String> f2380h = new yg(new ye("Native crash"));

    /* renamed from: i */
    private static final yk<Activity> f2381i = new yg(new yf("Activity"));

    /* renamed from: j */
    private static final yk<Application> f2382j = new yg(new yf("Application"));

    /* renamed from: k */
    private static final yk<Context> f2383k = new yg(new yf("Context"));

    /* renamed from: l */
    private static final yk<DeferredDeeplinkParametersListener> f2384l = new yg(new yf("Deeplink listener"));

    /* renamed from: m */
    private static final yk<AppMetricaDeviceIDListener> f2385m = new yg(new yf("DeviceID listener"));

    /* renamed from: n */
    private static final yk<ReporterConfig> f2386n = new yg(new yf("Reporter Config"));

    /* renamed from: o */
    private static final yk<String> f2387o = new yg(new ye("Deeplink"));
    @Deprecated

    /* renamed from: p */
    private static final yk<String> f2388p = new yg(new ye("Referral url"));

    /* renamed from: q */
    private static final yk<String> f2389q = new yg(new yl());

    /* renamed from: a */
    public void mo2058a(String str) {
        f2380h.a(str);
    }

    /* renamed from: a */
    public void mo2050a(@NonNull Application application) {
        f2382j.a(application);
    }

    /* renamed from: a */
    public void mo2049a(@NonNull Activity activity) {
        f2381i.a(activity);
    }

    /* renamed from: B */
    public void mo2061b(@NonNull String str) {
        f2387o.a(str);
    }

    @Deprecated
    /* renamed from: a */
    public void mo2062c(@NonNull String str) {
        f2388p.a(str);
    }

    /* renamed from: a */
    public void mo2055a(@Nullable Location location) {
    }

    /* renamed from: a */
    public void mo2059a(boolean z) {
    }

    /* renamed from: a */
    public void mo2054a(@NonNull Context context, boolean z) {
        f2383k.a(context);
    }

    /* renamed from: a */
    public void mo2057a(@NonNull DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        f2384l.a(deferredDeeplinkParametersListener);
    }

    /* renamed from: a */
    public void mo2056a(@NonNull AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        f2385m.a(appMetricaDeviceIDListener);
    }

    /* renamed from: B */
    public void mo2060b(@NonNull Context context, boolean z) {
        f2383k.a(context);
    }

    /* renamed from: a */
    public void mo2053a(@NonNull Context context, @NonNull String str) {
        f2383k.a(context);
        f2389q.a(str);
    }

    /* renamed from: a */
    public void mo2051a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        f2383k.a(context);
        f2386n.a(reporterConfig);
    }

    /* renamed from: a */
    public void mo2052a(@NonNull Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        f2383k.a(context);
        f2379g.a(yandexMetricaConfig);
    }
}
