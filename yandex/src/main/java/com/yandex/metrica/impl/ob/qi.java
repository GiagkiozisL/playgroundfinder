package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qi */
public class qi extends qd {

    /* renamed from: d */
    static final qk f1829d = new qk("PREF_KEY_DEVICE_ID_");

    /* renamed from: e */
    static final qk f1830e = new qk("PREF_KEY_UID_");

    /* renamed from: f */
    static final qk f1831f = new qk("STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_");

    /* renamed from: g */
    static final qk f1832g = new qk("PREF_KEY_PINNING_UPDATE_URL");

    /* renamed from: h */
    private static final qk f1833h = new qk("PREF_KEY_HOST_URL_");

    /* renamed from: i */
    private static final qk f1834i = new qk("PREF_KEY_REPORT_URL_");

    /* renamed from: j */
    private static final qk f1835j = new qk("PREF_KEY_GET_AD_URL");

    /* renamed from: k */
    private static final qk f1836k = new qk("PREF_KEY_REPORT_AD_URL");

    /* renamed from: l */
    private static final qk f1837l = new qk("PREF_KEY_STARTUP_OBTAIN_TIME_");

    /* renamed from: m */
    private static final qk f1838m = new qk("PREF_KEY_STARTUP_ENCODED_CLIDS_");

    /* renamed from: n */
    private static final qk f1839n = new qk("PREF_KEY_DISTRIBUTION_REFERRER_");

    /* renamed from: o */
    private static final qk f1840o = new qk("PREF_KEY_EASY_COLLECTING_ENABLED_");

    /* renamed from: p */
    private qk f1841p;

    /* renamed from: q */
    private qk f1842q;

    /* renamed from: r */
    private qk f1843r;

    /* renamed from: s */
    private qk f1844s;

    /* renamed from: t */
    private qk f1845t;

    /* renamed from: u */
    private qk f1846u;

    /* renamed from: v */
    private qk f1847v;

    /* renamed from: w */
    private qk f1848w;

    /* renamed from: x */
    private qk f1849x;

    /* renamed from: y */
    private qk f1850y;

    public qi(Context context) {
        this(context, null);
    }

    public qi(Context context, String str) {
        super(context, str);
        this.f1841p = new qk(f1829d.mo1742a());
        this.f1842q = new qk(f1830e.mo1742a(), mo1698i());
        this.f1843r = new qk(f1833h.mo1742a(), mo1698i());
        this.f1844s = new qk(f1834i.mo1742a(), mo1698i());
        this.f1845t = new qk(f1835j.mo1742a(), mo1698i());
        this.f1846u = new qk(f1836k.mo1742a(), mo1698i());
        this.f1847v = new qk(f1837l.mo1742a(), mo1698i());
        this.f1848w = new qk(f1838m.mo1742a(), mo1698i());
        this.f1849x = new qk(f1839n.mo1742a(), mo1698i());
        this.f1850y = new qk(f1840o.mo1742a(), mo1698i());
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_startupserviceinfopreferences";
    }

    /* renamed from: a */
    public long mo1730a(long j) {
        return this.f1766c.getLong(this.f1847v.mo1744b(), j);
    }

    /* renamed from: a */
    public String mo1731a(String str) {
        return this.f1766c.getString(this.f1841p.mo1744b(), str);
    }

    /* renamed from: B */
    public String mo1732b(String str) {
        return this.f1766c.getString(this.f1842q.mo1744b(), str);
    }

    /* renamed from: a */
    public String mo1734c(String str) {
        return this.f1766c.getString(this.f1843r.mo1744b(), str);
    }

    /* renamed from: d */
    public String mo1735d(String str) {
        return this.f1766c.getString(this.f1848w.mo1744b(), str);
    }

    /* renamed from: e */
    public String mo1736e(String str) {
        return this.f1766c.getString(this.f1844s.mo1744b(), str);
    }

    /* renamed from: f */
    public String mo1737f(String str) {
        return this.f1766c.getString(this.f1845t.mo1744b(), str);
    }

    /* renamed from: g */
    public String mo1738g(String str) {
        return this.f1766c.getString(this.f1846u.mo1744b(), str);
    }

    /* renamed from: a */
    public String mo1684a() {
        return this.f1766c.getString(this.f1849x.mo1742a(), null);
    }

    /* renamed from: i */
    public qi mo1739i(String str) {
        return (qi) mo1695a(this.f1842q.mo1744b(), str);
    }

    /* renamed from: j */
    public qi mo1740j(String str) {
        return (qi) mo1695a(this.f1841p.mo1744b(), str);
    }

    /* renamed from: a */
    public static void m2950a(Context context) {
        ql.m2970a(context, "_startupserviceinfopreferences").edit().remove(f1829d.mo1742a()).apply();
    }

    /* renamed from: B */
    public void mo1733b() {
        mo1697h(this.f1841p.mo1744b()).mo1697h(this.f1842q.mo1744b()).mo1697h(this.f1843r.mo1744b()).mo1697h(this.f1844s.mo1744b()).mo1697h(this.f1845t.mo1744b()).mo1697h(this.f1846u.mo1744b()).mo1697h(this.f1847v.mo1744b()).mo1697h(this.f1850y.mo1744b()).mo1697h(this.f1848w.mo1744b()).mo1697h(this.f1849x.mo1742a()).mo1697h(f1831f.mo1742a()).mo1697h(f1832g.mo1742a()).mo1699j();
    }
}
