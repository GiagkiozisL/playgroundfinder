package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.fv */
class fv implements fq, ft<ez> {
    fv() {
    }

    @NonNull
    /* renamed from: a */
    public fp a(@NonNull Context context, @NonNull fu fuVar, @NonNull fn fnVar, @NonNull eg egVar) {
        return new fw(context, fuVar.mo924a(new fb(fnVar.mo908b(), fnVar.mo907a()), egVar, new fd(this)));
    }

    @NonNull
    /* renamed from: a */
    public ez d(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        return new ez(context, ulVar.mo2406e(), blVar, ekVar, aVar, al.m324a().mo285c(), al.m324a().mo290h(), new uo(ulVar));
    }

    @NonNull
    /* renamed from: B */
    public ge c(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        return new ge(context, ekVar, blVar, aVar, ulVar.mo2406e(), new uo(ulVar), C0008a.MAIN);
    }
}
