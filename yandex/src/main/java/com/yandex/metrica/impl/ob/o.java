package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.o */
public class o extends cf<Boolean> {
    public o(@NonNull Context context, @NonNull String str) {
        super(context, str, "bool");
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public Boolean mo604b(int i) {
        return Boolean.valueOf(this.f533a.getResources().getBoolean(i));
    }
}
