package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.od */
class od {
    @Nullable

    /* renamed from: a */
    private oh f1566a;
    @NonNull

    /* renamed from: B */
    private final lh f1567b;
    @NonNull

    /* renamed from: a */
    private final lg f1568c;

    public od(@Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar) {
        this.f1566a = ohVar;
        this.f1567b = lhVar;
        this.f1568c = lgVar;
    }

    /* renamed from: a */
    public void mo1538a() {
        if (this.f1566a != null) {
            m2651b(this.f1566a);
            m2652c(this.f1566a);
        }
    }

    /* renamed from: B */
    private void m2651b(@NonNull oh ohVar) {
        if (this.f1567b.mo1177a() > ((long) ohVar.f1576j)) {
            this.f1567b.mo1185c((int) (((float) ohVar.f1576j) * 0.1f));
        }
    }

    /* renamed from: a */
    private void m2652c(@NonNull oh ohVar) {
        if (this.f1568c.mo1177a() > ((long) ohVar.f1576j)) {
            this.f1568c.mo1185c((int) (((float) ohVar.f1576j) * 0.1f));
        }
    }

    /* renamed from: a */
    public void mo1539a(@Nullable oh ohVar) {
        this.f1566a = ohVar;
    }
}
