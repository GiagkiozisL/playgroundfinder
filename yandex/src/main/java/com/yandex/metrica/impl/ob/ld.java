package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import androidx.annotation.NonNull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ld */
public class ld {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a */
    private static volatile ld f1296a;

    /* renamed from: B */
    private final Map<String, lc> f1297b;

    /* renamed from: a */
    private final Map<String, lf> f1298c;

    /* renamed from: d */
    private final Map<String, kx> f1299d;
    @NonNull

    /* renamed from: e */
    private final la f1300e;

    /* renamed from: f */
    private final Context f1301f;

    /* renamed from: g */
    private lc f1302g;

    /* renamed from: h */
    private kx f1303h;

    /* renamed from: i */
    private lf f1304i;

    /* renamed from: j */
    private lf f1305j;

    /* renamed from: k */
    private lf f1306k;

    /* renamed from: l */
    private lh f1307l;

    /* renamed from: m */
    private lg f1308m;

    /* renamed from: n */
    private li f1309n;

    /* renamed from: a */
    public static ld m2146a(Context context) {
        if (f1296a == null) {
            synchronized (ld.class) {
                if (f1296a == null) {
                    f1296a = new ld(context.getApplicationContext());
                }
            }
        }
        return f1296a;
    }

    public ld(Context context) {
        this(context, lq.m2213a());
    }

    public ld(Context context, @NonNull la laVar) {
        this.f1297b = new HashMap();
        this.f1298c = new HashMap();
        this.f1299d = new HashMap();
        this.f1301f = context;
        this.f1300e = laVar;
    }

    /* renamed from: a */
    public synchronized lc mo1233a(ek ekVar) {
        lc lcVar;
        String d = m2150d(ekVar);
        lcVar = (lc) this.f1297b.get(d);
        if (lcVar == null) {
            lcVar = mo1234a(d, this.f1300e.mo1216a());
            this.f1297b.put(d, lcVar);
        }
        return lcVar;
    }

    /* renamed from: a */
    public synchronized lc mo1232a() {
        if (this.f1302g == null) {
            this.f1302g = mo1234a("metrica_data.db", this.f1300e.mo1217b());
        }
        return this.f1302g;
    }

    /* renamed from: B */
    public synchronized lf mo1236b(ek ekVar) {
        lf lfVar;
        String ekVar2 = ekVar.toString();
        lfVar = (lf) this.f1298c.get(ekVar2);
        if (lfVar == null) {
            lfVar = new lf(mo1233a(ekVar), "preferences");
            this.f1298c.put(ekVar2, lfVar);
        }
        return lfVar;
    }

    @NonNull
    /* renamed from: a */
    public synchronized kx mo1237c(@NonNull ek ekVar) {
        kx kxVar;
        String ekVar2 = ekVar.toString();
        kxVar = (kx) this.f1299d.get(ekVar2);
        if (kxVar == null) {
            kxVar = new kx(new lp(mo1233a(ekVar)), "binary_data");
            this.f1299d.put(ekVar2, kxVar);
        }
        return kxVar;
    }

    /* renamed from: B */
    public synchronized kx mo1235b() {
        if (this.f1303h == null) {
            this.f1303h = new kx(new lp(mo1232a()), "binary_data");
        }
        return this.f1303h;
    }

    /* renamed from: a */
    public synchronized lf mo1238c() {
        if (this.f1304i == null) {
            this.f1304i = new lf(mo1232a(), "preferences");
        }
        return this.f1304i;
    }

    /* renamed from: d */
    public synchronized li mo1239d() {
        if (this.f1309n == null) {
            this.f1309n = new li(mo1232a(), "permissions");
        }
        return this.f1309n;
    }

    /* renamed from: e */
    public synchronized lf mo1240e() {
        if (this.f1305j == null) {
            this.f1305j = new lf(mo1232a(), "startup");
        }
        return this.f1305j;
    }

    /* renamed from: f */
    public synchronized lf mo1241f() {
        if (this.f1306k == null) {
            this.f1306k = new lf("preferences", (lm) new lo(this.f1301f, m2147a("metrica_client_data.db")));
        }
        return this.f1306k;
    }

    /* renamed from: g */
    public synchronized lh mo1242g() {
        if (this.f1307l == null) {
            this.f1307l = new lh(this.f1301f, mo1232a());
        }
        return this.f1307l;
    }

    /* renamed from: h */
    public synchronized lg mo1243h() {
        if (this.f1308m == null) {
            this.f1308m = new lg(this.f1301f, mo1232a());
        }
        return this.f1308m;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public lc mo1234a(String str, lj ljVar) {
        return new lc(this.f1301f, m2147a(str), ljVar);
    }

    /* renamed from: a */
    private String m2147a(String str) {
        if (cx.a(21)) {
            return m2149b(str);
        }
        return str;
    }

    @TargetApi(21)
    /* renamed from: B */
    private String m2149b(String str) {
        try {
            File noBackupFilesDir = this.f1301f.getNoBackupFilesDir();
            File file = new File(noBackupFilesDir, str);
            if (!file.exists() && m2148a(noBackupFilesDir, str)) {
                m2148a(noBackupFilesDir, str + "-journal");
                m2148a(noBackupFilesDir, str + "-shm");
                m2148a(noBackupFilesDir, str + "-wal");
            }
            return file.getAbsolutePath();
        } catch (Throwable th) {
            return str;
        }
    }

    /* renamed from: a */
    private boolean m2148a(@NonNull File file, @NonNull String str) {
        File databasePath = this.f1301f.getDatabasePath(str);
        if (databasePath == null || !databasePath.exists()) {
            return false;
        }
        return databasePath.renameTo(new File(file, str));
    }

    /* renamed from: d */
    private static String m2150d(ek ekVar) {
        return "db_metrica_" + ekVar;
    }
}
