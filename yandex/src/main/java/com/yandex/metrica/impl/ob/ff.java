package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.ff */
public class ff {
    @NonNull

    /* renamed from: a */
    private final lw f889a;
    @NonNull

    /* renamed from: B */
    private jd f890b;
    @NonNull

    /* renamed from: a */
    private kz f891c;
    @NonNull

    /* renamed from: d */
    private final wy f892d;
    @NonNull

    /* renamed from: e */
    private final i f893e;
    @NonNull

    /* renamed from: f */
    private final er f894f;
    @NonNull

    /* renamed from: g */
    private C0351a f895g;
    @NonNull

    /* renamed from: h */
    private final wh f896h;

    /* renamed from: i */
    private final int f897i;

    /* renamed from: j */
    private long f898j;

    /* renamed from: k */
    private long f899k;

    /* renamed from: l */
    private int f900l;

    /* renamed from: com.yandex.metrica.impl.ob.ff$a */
    public interface C0351a {
        /* renamed from: a */
        void mo859a();
    }

    public ff(@NonNull lw lwVar, @NonNull jd jdVar, @NonNull kz kzVar, @NonNull i iVar, @NonNull wy wyVar, int i, @NonNull C0351a aVar) {
        this(lwVar, jdVar, kzVar, iVar, wyVar, i, aVar, new er(lwVar), new wg());
    }

    @VisibleForTesting
    public ff(@NonNull lw lwVar, @NonNull jd jdVar, @NonNull kz kzVar, @NonNull i iVar, @NonNull wy wyVar, int i, @NonNull C0351a aVar, @NonNull er erVar, @NonNull wh whVar) {
        this.f889a = lwVar;
        this.f890b = jdVar;
        this.f891c = kzVar;
        this.f893e = iVar;
        this.f892d = wyVar;
        this.f897i = i;
        this.f894f = erVar;
        this.f896h = whVar;
        this.f895g = aVar;
        this.f898j = this.f889a.mo1318a(0);
        this.f899k = this.f889a.mo1324b();
        this.f900l = this.f889a.mo1328c();
    }

    /* renamed from: a */
    public void mo881a(w wVar) {
        this.f890b.mo1076c(wVar);
    }

    /* renamed from: B */
    public void mo884b(w wVar) {
        mo889e(wVar);
        m1588f();
    }

    /* renamed from: a */
    public void mo885c(w wVar) {
        mo889e(wVar);
        mo880a();
    }

    /* renamed from: d */
    public void mo888d(w wVar) {
        mo889e(wVar);
        mo883b();
    }

    /* renamed from: e */
    public void mo889e(w wVar) {
        mo882a(wVar, this.f890b.mo1077d(wVar));
    }

    /* renamed from: f */
    public void mo891f(@NonNull w wVar) {
        mo882a(wVar, this.f890b.mo1078e(wVar));
    }

    @VisibleForTesting
    /* renamed from: a */
    public void mo882a(@NonNull w wVar, @NonNull je jeVar) {
        if (TextUtils.isEmpty(wVar.mo2538l())) {
            wVar.mo1763a(this.f889a.mo1340f());
        }
        wVar.mo1768d(this.f889a.mo1342h());
        je jeVar2 = jeVar;
        this.f891c.mo1203a(this.f892d.mo2590a(wVar).a(wVar), wVar.mo2533g(), jeVar2, this.f893e.mo987b(), this.f894f);
        this.f895g.mo859a();
    }

    /* renamed from: f */
    private void m1588f() {
        this.f898j = this.f896h.b();
        this.f889a.mo1326b(this.f898j).mo1364q();
    }

    /* renamed from: a */
    public void mo880a() {
        this.f899k = this.f896h.b();
        this.f889a.mo1330c(this.f899k).mo1364q();
    }

    /* renamed from: B */
    public void mo883b() {
        this.f900l = this.f897i;
        this.f889a.mo1329c(this.f900l).mo1364q();
    }

    /* renamed from: a */
    public boolean mo886c() {
        return this.f896h.b() - this.f898j > ja.f1103a;
    }

    /* renamed from: d */
    public long mo887d() {
        return this.f899k;
    }

    /* renamed from: e */
    public boolean mo890e() {
        return this.f900l < this.f897i;
    }
}
