package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.PackageInfo;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.pz */
public class pz implements pt {
    @NonNull

    /* renamed from: a */
    private final Context f1749a;
    @NonNull

    /* renamed from: B */
    private final String f1750b;
    @NonNull

    /* renamed from: a */
    private final MyPackageManager f1751c;

    public pz(@NonNull Context context) {
        this(context, context.getPackageName(), new MyPackageManager());
    }

    public pz(@NonNull Context context, @NonNull String str, @NonNull MyPackageManager myPackageManagerVar) {
        this.f1749a = context;
        this.f1750b = str;
        this.f1751c = myPackageManagerVar;
    }

    @NonNull
    /* renamed from: a */
    public List<pu> a() {
        ArrayList arrayList = new ArrayList();
        PackageInfo a = this.f1751c.getPackageInfo(this.f1749a, this.f1750b, 4096);
        if (a != null) {
            for (String puVar : a.requestedPermissions) {
                arrayList.add(new pu(puVar, true));
            }
        }
        return arrayList;
    }
}
