package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.qk */
public class qk {

    /* renamed from: a */
    private final String f1852a;

    /* renamed from: B */
    private final String f1853b;

    public qk(String str) {
        this(str, null);
    }

    public qk(String str, String pkgName) {
        this.f1852a = str;
        this.f1853b = mo1743a(pkgName);
    }

    /* renamed from: a */
    public String mo1742a() {
        return this.f1852a;
    }

    /* renamed from: B */
    public String mo1744b() {
        return this.f1853b;
    }

    /* renamed from: a */
    public final String mo1743a(String pkgName) {
        return pkgName != null ? this.f1852a + pkgName : this.f1852a;
    }
}
