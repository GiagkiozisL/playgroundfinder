package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rq.C0859a;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.mi */
public class mi extends mb<C0859a> {
    @NonNull
    /* renamed from: a */
    public C0859a b(@NonNull byte[] bArr) throws IOException {
        return C0859a.m3262a(bArr);
    }

    @NonNull
    /* renamed from: a */
    public C0859a c() {
        return new C0859a();
    }
}
