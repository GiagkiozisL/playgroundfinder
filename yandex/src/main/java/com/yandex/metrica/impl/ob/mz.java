package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0875g;

/* renamed from: com.yandex.metrica.impl.ob.mz */
public class mz implements mq<tz, C0875g> {

    @NonNull
    /* renamed from: a */
    public C0875g b(@NonNull tz tzVar) {
        C0875g gVar = new C0875g();
        gVar.f2185b = tzVar.f2702a;
        gVar.f2186c = tzVar.f2703b;
        return gVar;
    }

    @NonNull
    /* renamed from: a */
    public tz a(@NonNull C0875g gVar) {
        return new tz(gVar.f2185b, gVar.f2186c);
    }
}
