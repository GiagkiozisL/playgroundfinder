package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;

import com.yandex.metrica.impl.ob.cq.a.a_enum;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.rs */
public class rs {

    /* renamed from: a */
    private static final Map<a_enum, bt.a> a_map = Collections.unmodifiableMap(new HashMap<a_enum, bt.a>() {
        {
            put(a_enum.CELL, bt.a.CELL);
            put(a_enum.WIFI, bt.a.WIFI);
        }
    });
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public final Context b;
    @NonNull

    /* renamed from: a */
    private final mf<a> c;
    @NonNull

    /* renamed from: d */
    private final xh d;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: e */
    public final tj e;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: f */
    public final cs f;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: g */
    public final vn g;

    /* renamed from: h */
    private a h;

    /* renamed from: i */
    private boolean i;

    /* renamed from: com.yandex.metrica.impl.ob.rs$a */
    public static class a {
        @NonNull

        /* renamed from: a */
        private final List<aa> f2222a;
        @NonNull

        /* renamed from: B */
        private final LinkedHashMap<String, Object> f2223b = new LinkedHashMap<>();

        /* renamed from: com.yandex.metrica.impl.ob.rs$a$a */
        public static class aa {
            @NonNull

            /* renamed from: a */
            public final String a;
            @NonNull

            /* renamed from: B */
            public final String b;
            @NonNull

            /* renamed from: a */
            public final String c;
            @NonNull

            /* renamed from: d */
            public final wp<String, String> d;

            /* renamed from: e */
            public final long e;
            @NonNull

            /* renamed from: f */
            public final List<bt.a> f;

            public aa(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull wp<String, String> wpVar, long j, @NonNull List<bt.a> list) {
                this.a = str;
                this.b = str2;
                this.c = str3;
                this.e = j;
                this.f = list;
                this.d = wpVar;
            }

            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }
                return this.a.equals(((aa) o).a);
            }

            public int hashCode() {
                return this.a.hashCode();
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rs$a$B */
        public static class b {
            @Nullable

            /* renamed from: a */
            byte[] f2230a;
            @Nullable

            /* renamed from: B */
            byte[] f2231b;
            /* access modifiers changed from: private */
            @NonNull

            /* renamed from: a */
            public final a.aa f2232c;
            @Nullable

            /* renamed from: d */
            private aa f2233d;
            @Nullable

            /* renamed from: e */
            private bt.a f2234e;
            @Nullable

            /* renamed from: f */
            private Integer f2235f;
            @Nullable

            /* renamed from: g */
            private Map<String, List<String>> f2236g;
            @Nullable

            /* renamed from: h */
            private Throwable f2237h;

            /* renamed from: com.yandex.metrica.impl.ob.rs$a$B$a */
            public enum aa {
                OFFLINE,
                INCOMPATIBLE_NETWORK_TYPE,
                COMPLETE,
                ERROR
            }

            public b(@NonNull a.aa aVar) {
                this.f2232c = aVar;
            }

            @NonNull
            /* renamed from: a */
            public a.aa a() {
                return this.f2232c;
            }

            @Nullable
            /* renamed from: B */
            public aa mo1918b() {
                return this.f2233d;
            }

            /* renamed from: a */
            public void a(@NonNull aa aVar) {
                this.f2233d = aVar;
            }

            @Nullable
            /* renamed from: a */
            public bt.a mo1920c() {
                return this.f2234e;
            }

            /* renamed from: a */
            public void a(@Nullable bt.a aVar) {
                this.f2234e = aVar;
            }

            @Nullable
            /* renamed from: d */
            public Integer mo1921d() {
                return this.f2235f;
            }

            /* renamed from: a */
            public void a(@Nullable Integer num) {
                this.f2235f = num;
            }

            @Nullable
            /* renamed from: e */
            public byte[] mo1922e() {
                return this.f2230a;
            }

            /* renamed from: a */
            public void mo1917a(@Nullable byte[] bArr) {
                this.f2230a = bArr;
            }

            @Nullable
            /* renamed from: f */
            public Map<String, List<String>> mo1923f() {
                return this.f2236g;
            }

            /* renamed from: a */
            public void a(@Nullable Map<String, List<String>> map) {
                this.f2236g = map;
            }

            @Nullable
            /* renamed from: g */
            public Throwable mo1924g() {
                return this.f2237h;
            }

            /* renamed from: a */
            public void a(@Nullable Throwable th) {
                this.f2237h = th;
            }

            @Nullable
            /* renamed from: h */
            public byte[] mo1925h() {
                return this.f2231b;
            }

            /* renamed from: B */
            public void mo1919b(@Nullable byte[] bArr) {
                this.f2231b = bArr;
            }
        }

        public a(@NonNull List<aa> list, @NonNull List<String> list2) {
            this.f2222a = list;
            if (!cx.a((Collection) list2)) {
                for (String put : list2) {
                    this.f2223b.put(put, new Object());
                }
            }
        }

        /* renamed from: a */
        public boolean mo1906a(@NonNull aa aVar) {
            if (this.f2223b.get(aVar.a) != null || this.f2222a.contains(aVar)) {
                return false;
            }
            this.f2222a.add(aVar);
            return true;
        }

        @NonNull
        /* renamed from: a */
        public Set<String> mo1905a() {
            HashSet hashSet = new HashSet();
            int i = 0;
            Iterator it = this.f2223b.keySet().iterator();
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    break;
                }
                hashSet.add((String) it.next());
                i = i2 + 1;
                if (i > 1000) {
                    break;
                }
            }
            return hashSet;
        }

        @NonNull
        /* renamed from: B */
        public List<aa> b() {
            return this.f2222a;
        }

        /* renamed from: B */
        public void b(@NonNull aa aVar) {
            this.f2223b.put(aVar.a, new Object());
            this.f2222a.remove(aVar);
        }
    }

    public rs(@NonNull Context context, @NonNull mf<a> mfVar, @NonNull cs csVar, @NonNull tj tjVar, @NonNull xh xhVar) {
        this(context, mfVar, csVar, tjVar, xhVar, new vk());
    }

    @VisibleForTesting
    public rs(@NonNull Context context, @NonNull mf<a> mfVar, @NonNull cs csVar, @NonNull tj tjVar, @NonNull xh xhVar, @NonNull vn vnVar) {
        this.i = false;
        this.b = context;
        this.c = mfVar;
        this.f = csVar;
        this.e = tjVar;
        this.h = (a) this.c.a();
        this.d = xhVar;
        this.g = vnVar;
    }

    /* renamed from: a */
    public synchronized void a() {
        this.d.a((Runnable) new Runnable() {
            public void run() {
                rs.this.m3384b();
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: B */
    public void m3384b() {
        if (!this.i) {
            this.h = (a) this.c.a();
            c();
            this.i = true;
        }
    }

    /* renamed from: a */
    private void c() {
        Iterator var1 = this.h.b().iterator();

        while(var1.hasNext()) {
            a.aa var2 = (a.aa)var1.next();
            this.b(var2);
        }
//        for (a aaa : this.h.aaa()) {
////            aaa(aaa);
////        }
    }

    /* renamed from: a */
    public synchronized void a(@NonNull final uk var1) {
        final List var2 = var1.w;
        this.d.a(new Runnable() {
            public void run() {
                rs.this.a(var2, var1.t);
            }
        });
//        final List<com.yandex.metrica.impl.ob.a_map> list = ukVar.w;
//        this.d.a_map((Runnable) new Runnable() {
//            public void run() {
//                rs.this.a_map(list, ukVar.t);
//            }
//        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
//    public void a_map(@Nullable List<com.yandex.metrica.impl.ob.a_map> list, long j) {
//        if (!cx.a_map((Collection) list)) {
//            for (com.yandex.metrica.impl.ob.a_map aVar : list) {
//                if (!(aVar.a_map == null || aVar.aaa == null || aVar.a == null || aVar.e == null || aVar.e.longValue() < 0 || cx.a_map((Collection) aVar.f))) {
//                    a_map(new C0887a(aVar.a_map, aVar.aaa, aVar.a, a_map(aVar.d), TimeUnit.SECONDS.toMillis(aVar.e.longValue() + j), aaa(aVar.f)));
//                }
//            }
//        }
//    }

    private void a(@Nullable List<cq.a> var1, long var2) {
        if (!cx.a(var1)) {
            Iterator var4 = var1.iterator();

            while(var4.hasNext()) {
                com.yandex.metrica.impl.ob.cq.a var5 = (com.yandex.metrica.impl.ob.cq.a)var4.next();
                if (var5.a != null && var5.b != null && var5.c != null && var5.e != null && var5.e >= 0L && !cx.a(var5.f)) {
                    this.a(new rs.a.aa(var5.a, var5.b, var5.c, this.a(var5.d), TimeUnit.SECONDS.toMillis(var2 + var5.e), this.b(var5.f)));
                }
            }
        }

    }

    /* renamed from: a */
    private wp<String, String> a(List<Pair<String, String>> list) {
        wp<String, String> wpVar = new wp<>();
        for (Pair pair : list) {
            wpVar.mo2572a((String)pair.first, (String) pair.second);
        }
        return wpVar;
    }

    /* renamed from: a */
    private boolean a(@NonNull rs.a.aa aVar) {
        boolean a = this.h.mo1906a(aVar);
        if (a) {
            b(aVar);
            this.e.mo2240a(aVar);
        }
        m3389d();
        return a;
    }

    /* renamed from: B */
    private void b(@NonNull final a.aa var1) {

        long var2 = Math.max(var1.e - System.currentTimeMillis(), 0L);
        this.d.a(new Runnable() {
            public void run() {
                if (!rs.this.f.c()) {
                    rs.this.e.b(var1);
                    a.b var1x = new a.b(var1);
                    com.yandex.metrica.impl.ob.bt.a var2 = rs.this.g.a(rs.this.b);
                    var1x.a(var2);
                    if (var2 == com.yandex.metrica.impl.ob.bt.a.OFFLINE) {
                        var1x.a(rs.a.b.aa.OFFLINE);
                    } else if (!var1.f.contains(var2)) {
                        var1x.a(rs.a.b.aa.INCOMPATIBLE_NETWORK_TYPE);
                    } else {
                        var1x.a(rs.a.b.aa.ERROR);

                        try {
                            HttpURLConnection var3 = (HttpURLConnection)(new URL(var1.b)).openConnection();
                            Iterator var4 = var1.d.b().iterator();

                            while(var4.hasNext()) {
                                Entry var5 = (Entry)var4.next();
                                var3.setRequestProperty((String)var5.getKey(), TextUtils.join(",", (Iterable)var5.getValue()));
                            }

                            var3.setInstanceFollowRedirects(true);
                            var3.setRequestMethod(var1.c);
                            var3.setConnectTimeout(com.yandex.metrica.impl.ob.pj.a.a);
                            var3.setReadTimeout(com.yandex.metrica.impl.ob.pj.a.a);
                            var3.connect();
                            int var7 = var3.getResponseCode();
                            var1x.a(rs.a.b.aa.COMPLETE);
                            var1x.a(var7);
                            am.a(var3, var1x, "[ProvidedRequestService]", 102400);
                            var1x.a(var3.getHeaderFields());
                        } catch (Throwable var6) {
                            var1x.a(var6);
                        }
                    }

                    rs.this.a(var1x);
                }

            }
        }, Math.max(com.yandex.metrica.impl.ob.h.a, var2));

//        this.d.a(new Runnable() {
//            public void run() {
//                if (!rs.this.f.a()) {
//                    rs.this.e.aaa(aVar);
//                    C0888b bVar = new C0888b(aVar);
//                    a a = rs.this.g.a(rs.this.aaa);
//                    bVar.a(a);
//                    if (a == a.OFFLINE) {
//                        bVar.a_map(a.OFFLINE);
//                    } else if (!aVar.f.contains(a)) {
//                        bVar.a_map(a.INCOMPATIBLE_NETWORK_TYPE);
//                    } else {
//                        bVar.a_map(a.ERROR);
//                        try {
//                            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(aVar.aaa).openConnection();
//                            for (Entry entry : aVar.d.aaa()) {
//                                httpURLConnection.setRequestProperty((String) entry.getKey(), TextUtils.join(",", (Iterable) entry.getValue()));
//                            }
//                            httpURLConnection.setInstanceFollowRedirects(true);
//                            httpURLConnection.setRequestMethod(aVar.a);
//                            httpURLConnection.setConnectTimeout(a_map.a_map);
//                            httpURLConnection.setReadTimeout(a_map.a_map);
//                            httpURLConnection.connect();
//                            int responseCode = httpURLConnection.getResponseCode();
//                            bVar.a_map(a.COMPLETE);
//                            bVar.a_map(Integer.valueOf(responseCode));
//                            am.a_map(httpURLConnection, bVar, "[ProvidedRequestService]", 102400);
//                            bVar.a(httpURLConnection.getHeaderFields());
//                        } catch (Throwable th) {
//                            bVar.a(th);
//                        }
//                    }
//                    rs.this.a_map(bVar);
//                }
//            }
//        }, Math.max(h.a_map, Math.max(aVar.e - System.currentTimeMillis(), 0)));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void a(@NonNull rs.a.b bVar) {
        this.h.b(bVar.f2232c);
        m3389d();
        this.e.mo2241a(bVar);
    }

    /* renamed from: d */
    private void m3389d() {
        this.c.a(this.h);
    }

    @NonNull
    /* renamed from: B */
    private List<bt.a> b(@NonNull List<a_enum> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (a_enum aVar : list) {
            arrayList.add(a_map.get(aVar));
        }
        return arrayList;
    }
}
