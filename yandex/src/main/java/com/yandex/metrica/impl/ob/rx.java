package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;

import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.d;
import com.yandex.metrica.f;
import com.yandex.metrica.profile.UserProfile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.rx */
public class rx<B extends se> implements IReporter {
    @NonNull

    /* renamed from: a */
    final B f2261a;
    @NonNull

    /* renamed from: B */
    final xh f2262b;
    @NonNull

    /* renamed from: a */
    final sc f2263c;
    @NonNull

    /* renamed from: d */
    private final sa f2264d;
    @NonNull

    /* renamed from: e */
    private final Context f2265e;
    @NonNull

    /* renamed from: f */
    private final f f2266f;

    rx(@NonNull xh xhVar, @NonNull Context context, @NonNull String str, @NonNull B b) {
        this(xhVar, context, str, b, new sa(), new sc());
    }

    @VisibleForTesting
    rx(@NonNull xh xhVar, @NonNull Context context, @NonNull String str, @NonNull B b, @NonNull sa saVar, @NonNull sc scVar) {
        this.f2262b = xhVar;
        this.f2265e = context;
        this.f2266f = f.m121a(str).mo153b();
        this.f2261a = b;
        this.f2264d = saVar;
        this.f2263c = scVar;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    /* renamed from: a */
    public final d mo1939a() {
        return this.f2264d.mo1987a(this.f2265e).mo684b(this.f2266f);
    }

    /* renamed from: a */
    public void mo1941a(@NonNull final String str) {
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1942b(f.m121a(str).mo153b());
            }
        });
    }

    /* renamed from: a */
    public void mo1940a(@NonNull final ReporterConfig reporterConfig) {
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1942b(rx.this.f2263c.mo2047a(f.m122a(reporterConfig)));
            }
        });
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: B */
    public void mo1942b(@NonNull f fVar) {
        this.f2264d.mo1987a(this.f2265e).mo681a(fVar);
    }

    public void reportEvent(@NonNull final String eventName) {
        this.f2261a.reportEvent(eventName);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().reportEvent(eventName);
            }
        });
    }

    public void reportEvent(@NonNull final String eventName, @Nullable final String jsonValue) {
        this.f2261a.reportEvent(eventName, jsonValue);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().reportEvent(eventName, jsonValue);
            }
        });
    }

    public void reportEvent(@NonNull final String eventName, @Nullable Map<String, Object> attributes) {
        final ArrayList arrayList;
        this.f2261a.reportEvent(eventName, attributes);
        if (attributes == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList(attributes.entrySet());
        }
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                if (arrayList != null) {
                    Iterator var2 = arrayList.iterator();

                    while(var2.hasNext()) {
                        Entry var3x = (Entry)var2.next();
                        linkedHashMap.put(var3x.getKey(), var3x.getValue());
                    }
//                    for (Entry entry : arrayList) {
//                        linkedHashMap.put(entry.getKey(), entry.getValue());
//                    }
                }
                rx.this.mo1939a().reportEvent(eventName, (Map<String, Object>) linkedHashMap);
            }
        });
    }

    public void reportError(@NonNull final String message, @Nullable final Throwable error) {
        this.f2261a.reportError(message, error);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().reportError(message, error);
            }
        });
    }

    public void reportUnhandledException(@NonNull final Throwable exception) {
        this.f2261a.reportUnhandledException(exception);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().reportUnhandledException(exception);
            }
        });
    }

    public void resumeSession() {
        this.f2261a.resumeSession();
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().resumeSession();
            }
        });
    }

    public void pauseSession() {
        this.f2261a.pauseSession();
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().pauseSession();
            }
        });
    }

    public void setUserProfileID(@Nullable final String profileID) {
        this.f2261a.setUserProfileID(profileID);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().setUserProfileID(profileID);
            }
        });
    }

    public void reportUserProfile(@NonNull final UserProfile profile) {
        this.f2261a.reportUserProfile(profile);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().reportUserProfile(profile);
            }
        });
    }

    public void reportRevenue(@NonNull final Revenue revenue) {
        this.f2261a.reportRevenue(revenue);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().reportRevenue(revenue);
            }
        });
    }

    public void setStatisticsSending(final boolean enabled) {
        this.f2261a.setStatisticsSending(enabled);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().setStatisticsSending(enabled);
            }
        });
    }

    public void sendEventsBuffer() {
        this.f2261a.sendEventsBuffer();
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.mo1939a().sendEventsBuffer();
            }
        });
    }
}
