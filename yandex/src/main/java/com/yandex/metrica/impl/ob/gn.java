package com.yandex.metrica.impl.ob;

import java.util.Collections;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.gn */
public class gn<BaseHandler> extends go<BaseHandler> {

    /* renamed from: a */
    private final List<BaseHandler> f981a;

    public gn(List<BaseHandler> list) {
        this.f981a = Collections.unmodifiableList(list);
    }

    /* renamed from: a */
    public List<? extends BaseHandler> mo956a() {
        return this.f981a;
    }
}
