package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.fi */
public class fi extends en {

    /* renamed from: B */
    private final String f902b;

    /* renamed from: a */
    private final cs f903c;

    public fi(@NonNull Context context, @NonNull ek ekVar, @NonNull bl blVar, @NonNull C0306a aVar, @NonNull cs csVar, @NonNull uk ukVar, @NonNull un unVar) {
        super(context, ukVar, blVar, ekVar, aVar, new fg(csVar), unVar);
        this.f902b = ekVar.mo790a();
        this.f903c = csVar;
    }

    /* renamed from: a */
    public synchronized void a(@NonNull C0306a aVar) {
        super.a(aVar);
        this.f903c.mo651a(this.f902b, aVar.f780m);
    }
}
