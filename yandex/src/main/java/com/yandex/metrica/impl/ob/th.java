package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.th */
public class th {
    @NonNull

    /* renamed from: a */
    public final String f2572a;
    @NonNull

    /* renamed from: B */
    public final List<String> f2573b;

    public th(@NonNull String str, @NonNull List<String> list) {
        this.f2572a = str;
        this.f2573b = list;
    }

    public String toString() {
        return "SdkItem{name='" + this.f2572a + '\'' + ", classes=" + this.f2573b + '}';
    }
}
