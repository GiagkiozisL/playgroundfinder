package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0878i;

/* renamed from: com.yandex.metrica.impl.ob.nc */
public class nc implements mq<ua, C0878i> {
    @NonNull
    /* renamed from: a */
    public C0878i b(@NonNull ua uaVar) {
        C0878i iVar = new C0878i();
        iVar.f2197b = uaVar.f2704a;
        iVar.f2198c = uaVar.f2705b;
        iVar.f2199d = uaVar.f2706c;
        iVar.f2200e = uaVar.f2707d;
        return iVar;
    }

    @NonNull
    /* renamed from: a */
    public ua a(@NonNull C0878i iVar) {
        return new ua(iVar.f2197b, iVar.f2198c, iVar.f2199d, iVar.f2200e);
    }
}
