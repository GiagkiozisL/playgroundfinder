package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.gt */
public abstract class gt {
    @NonNull

    /* renamed from: a */
    private final ei f991a;

    /* renamed from: a */
    public abstract boolean mo962a(@NonNull w wVar, @NonNull fs fsVar);

    gt(@NonNull ei eiVar) {
        this.f991a = eiVar;
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: a */
    public ei mo961a() {
        return this.f991a;
    }
}
