package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.eg.C0306a;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.fc */
public class fc<COMPONENT extends ew & et> implements ep, ev, uh {
    @NonNull

    /* renamed from: a */
    private final Context f878a;
    @NonNull

    /* renamed from: B */
    private final ek f879b;
    @NonNull

    /* renamed from: a */
    private final bl f880c;
    @NonNull

    /* renamed from: d */
    private final ft<COMPONENT> f881d;
    @NonNull

    /* renamed from: e */
    private final ul f882e;
    @NonNull

    /* renamed from: f */
    private final fh f883f;
    @Nullable

    /* renamed from: g */
    private COMPONENT f884g;
    @Nullable

    /* renamed from: h */
    private eu f885h;

    /* renamed from: i */
    private List<uh> f886i;
    @NonNull

    /* renamed from: j */
    private final el<fp> f887j;

    public fc(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar, @NonNull ft<COMPONENT> ftVar) {
        this(context, ekVar, egVar, new fh(egVar.f766b), ftVar, new bm(), new el(), uc.m3891a());
    }

    public fc(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar, @NonNull fh fhVar, @NonNull ft<COMPONENT> ftVar, @NonNull bm bmVar, @NonNull el<fp> elVar, @NonNull uc ucVar) {
        this.f886i = new ArrayList();
        this.f878a = context;
        this.f879b = ekVar;
        this.f883f = fhVar;
        this.f880c = bmVar.mo444a(this.f878a, this.f879b);
        this.f881d = ftVar;
        this.f887j = elVar;
        this.f882e = ucVar.mo2329a(this.f878a, mo874a(), this, egVar.f765a);
    }

    /* renamed from: a */
    public void mo877a(@NonNull w wVar, @NonNull eg egVar) {
        ew d;
        m1574b();
        if (af.m291d(wVar.mo2533g())) {
            d = m1576e();
        } else {
            d = m1575d();
        }
        if (!af.m280a(wVar.mo2533g())) {
            mo875a(egVar.f766b);
        }
        d.a(wVar);
    }

    /* renamed from: B */
    private void m1574b() {
        m1576e().a();
    }

    /* renamed from: a */
    public synchronized void mo875a(@NonNull C0306a aVar) {
        this.f883f.mo894a(aVar);
        if (this.f885h != null) {
            this.f885h.a(aVar);
        }
        if (this.f884g != null) {
            ((et) this.f884g).a(aVar);
        }
    }

    /* renamed from: a */
    public synchronized void mo876a(@NonNull fp fpVar) {
        this.f887j.mo797a(fpVar);
    }

    /* renamed from: B */
    public synchronized void mo878b(@NonNull fp fpVar) {
        this.f887j.mo798b(fpVar);
    }

    /* renamed from: d */
    private COMPONENT m1575d() {
        if (this.f884g == null) {
            synchronized (this) {
                this.f884g = this.f881d.d(this.f878a, this.f879b, this.f883f.mo893a(), this.f880c, this.f882e);
                this.f886i.add(this.f884g);
            }
        }
        return this.f884g;
    }

    /* renamed from: e */
    private eu m1576e() {
        if (this.f885h == null) {
            synchronized (this) {
                this.f885h = this.f881d.c(this.f878a, this.f879b, this.f883f.mo893a(), this.f880c, this.f882e);
                this.f886i.add(this.f885h);
            }
        }
        return this.f885h;
    }

    @VisibleForTesting
    @NonNull
    /* renamed from: a */
    public final ek mo874a() {
        return this.f879b;
    }

    /* renamed from: a */
    public synchronized void a(@Nullable uk ukVar) {
        for (uh a : this.f886i) {
            a.a(ukVar);
        }
    }

    /* renamed from: a */
    public synchronized void a(@NonNull ue ueVar, @Nullable uk ukVar) {
        for (uh a : this.f886i) {
            a.a(ueVar, ukVar);
        }
    }

    /* renamed from: a */
    public void c() {
        if (this.f884g != null) {
            ((ep) this.f884g).c();
        }
        if (this.f885h != null) {
            this.f885h.c();
        }
    }

    /* renamed from: a */
    public void a(@NonNull eg egVar) {
        this.f882e.mo2397a(egVar.f765a);
        mo875a(egVar.f766b);
    }
}
