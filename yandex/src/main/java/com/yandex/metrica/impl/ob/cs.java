package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashSet;

/* renamed from: com.yandex.metrica.impl.ob.cs */
public class cs {

    /* renamed from: a */
    private final C0232a f631a;
    @Nullable

    /* renamed from: B */
    private Boolean f632b;

    /* renamed from: a */
    private final HashSet<String> f633c = new HashSet<>();

    /* renamed from: d */
    private final HashSet<String> f634d = new HashSet<>();

    /* renamed from: com.yandex.metrica.impl.ob.cs$a */
    public interface C0232a {
        @Nullable
        /* renamed from: a */
        Boolean mo656a();

        /* renamed from: a */
        void mo657a(boolean z);
    }

    /* renamed from: com.yandex.metrica.impl.ob.cs$B */
    public static class C0233b implements C0232a {

        /* renamed from: a */
        private final ly f635a;

        public C0233b(@NonNull ly lyVar) {
            this.f635a = lyVar;
        }

        /* renamed from: a */
        public void mo657a(boolean z) {
            this.f635a.mo1389e(z).mo1364q();
        }

        @Nullable
        /* renamed from: a */
        public Boolean mo656a() {
            return this.f635a.mo1395h();
        }
    }

    public cs(@NonNull C0232a aVar) {
        this.f631a = aVar;
        this.f632b = this.f631a.mo656a();
    }

    /* renamed from: a */
    public synchronized void mo650a(@Nullable Boolean bool) {
        if (cx.m1189a((Object) bool) || this.f632b == null) {
            this.f632b = Boolean.valueOf(vi.m4194c(bool));
            this.f631a.mo657a(this.f632b.booleanValue());
        }
    }

    /* renamed from: a */
    public synchronized void mo651a(@NonNull String str, @Nullable Boolean bool) {
        if (cx.m1189a((Object) bool) || (!this.f634d.contains(str) && !this.f633c.contains(str))) {
            if (wk.m4371a(bool, true)) {
                this.f634d.add(str);
                this.f633c.remove(str);
            } else {
                this.f633c.add(str);
                this.f634d.remove(str);
            }
        }
    }

    /* renamed from: a */
    public synchronized boolean mo652a() {
        boolean booleanValue;
        if (this.f632b != null) {
            booleanValue = this.f632b.booleanValue();
        } else if (!this.f634d.isEmpty() || !this.f633c.isEmpty()) {
            booleanValue = false;
        } else {
            booleanValue = true;
        }
        return booleanValue;
    }

    /* renamed from: B */
    public synchronized boolean mo653b() {
        return this.f632b == null ? this.f634d.isEmpty() : this.f632b.booleanValue();
    }

    /* renamed from: a */
    public synchronized boolean c() {
        return m1138e();
    }

    /* renamed from: d */
    public synchronized boolean mo655d() {
        return m1138e();
    }

    /* renamed from: e */
    private boolean m1138e() {
        if (this.f632b != null) {
            return this.f632b.booleanValue();
        }
        if (!this.f633c.isEmpty() || this.f634d.isEmpty()) {
            return true;
        }
        return false;
    }
}
