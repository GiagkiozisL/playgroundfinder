package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.qt */
public final class qt {
    @NonNull

    /* renamed from: a */
    private final yk<String> f1856a;
    @NonNull

    /* renamed from: B */
    private final qn f1857b;
    @NonNull

    /* renamed from: a */
    private final String f1858c;

    public qt(@NonNull String str, @NonNull yk<String> ykVar, @NonNull qn qnVar) {
        this.f1858c = str;
        this.f1856a = ykVar;
        this.f1857b = qnVar;
    }

    @NonNull
    /* renamed from: a */
    public String mo1753a() {
        return this.f1858c;
    }

    @NonNull
    /* renamed from: B */
    public qn mo1754b() {
        return this.f1857b;
    }

    @NonNull
    /* renamed from: a */
    public yk<String> mo1755c() {
        return this.f1856a;
    }
}
