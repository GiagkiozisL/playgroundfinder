package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.tm */
class tm extends to {
    @NonNull

    /* renamed from: a */
    private ub f2579c;
    @NonNull

    /* renamed from: d */
    private tn f2580d;

    tm(@NonNull Socket socket, @NonNull Uri uri, @NonNull tr trVar, @NonNull ub ubVar, @NonNull tn tnVar) {
        super(socket, uri, trVar);
        this.f2579c = ubVar;
        this.f2580d = tnVar;
    }

    /* renamed from: a */
    public void a() {
        if (this.f2579c.f2709b.equals(this.b.getQueryParameter("t"))) {
            try {
                final byte[] b = mo2247b();
                a("HTTP/1.1 200 OK", (Map<String, String>) new HashMap<String, String>() {
                    {
                        put("Content-Type", "text/plain; charset=utf-8");
                        put("Access-Control-Allow-Origin", "*");
                        put("Access-Control-Allow-Methods", "GET");
                        put("Content-Length", String.valueOf(b.length));
                    }
                }, b);
            } catch (Throwable th) {
            }
        } else {
            this.a.a("request_with_wrong_token");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public byte[] mo2247b() throws JSONException {
        String desiredBody = this.f2580d.mo2248a();
        Log.d(Constant.RUS_TAG, "desired body before encode && encrypt: " + desiredBody);
        return Base64.encode(new wt().a(desiredBody.getBytes()), 0);
    }
}
