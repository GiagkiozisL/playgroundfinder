package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.IReporter;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.he */
public class he extends hd {
    @NonNull

    /* renamed from: a */
    private final IReporter f1007a;

    public he(@NonNull en enVar, @NonNull IReporter iReporter) {
        super(enVar);
        this.f1007a = iReporter;
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        kd a = kd.m2021a(wVar.mo2533g());
        HashMap hashMap = new HashMap();
        hashMap.put("type", a.f1195a);
        hashMap.put("delivery_method", a.f1196b);
        this.f1007a.reportEvent("crash_saved", (Map<String, Object>) hashMap);
        return false;
    }
}
