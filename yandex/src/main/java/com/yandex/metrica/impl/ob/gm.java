package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.ArrayList;

/* renamed from: com.yandex.metrica.impl.ob.gm */
public class gm extends gr<gt> {

    /* renamed from: a */
    private final hx f979a;

    public gm(ei eiVar) {
        this.f979a = new hx(eiVar);
    }

    /* renamed from: a */
    public go<gt> mo955a(int i) {
        ArrayList arrayList = new ArrayList();
        switch (C0058a.m298a(i)) {
            case EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST:
                arrayList.add(this.f979a.mo980a());
                break;
            case EVENT_TYPE_STARTUP:
                arrayList.add(this.f979a.mo981b());
                break;
            case EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES:
                arrayList.add(this.f979a.mo982c());
                break;
            case EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG:
                arrayList.add(this.f979a.mo983d());
                break;
        }
        return new gn(arrayList);
    }
}
