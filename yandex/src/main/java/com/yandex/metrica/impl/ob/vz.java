package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rh.c.e;
import com.yandex.metrica.impl.ob.rh.c.e.a;

/* renamed from: com.yandex.metrica.impl.ob.vz */
public class vz extends vs {

    /* renamed from: a */
    private static final int[] f2914a = {3, 6, 4};

    /* renamed from: B */
    private static final vz f2915b = new vz();

    public vz(@Nullable String str) {
        super(str);
    }

    public vz() {
        this("");
    }

    /* renamed from: h */
    public static vz m4266h() {
        return f2915b;
    }

    /* renamed from: f */
    public String mo2489f() {
        return "AppMetrica";
    }

    /* renamed from: a */
    public void mo2518a(w wVar, String str) {
        if (af.m285b(wVar.mo2533g())) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(wVar.mo2528d());
            if (af.m289c(wVar.mo2533g()) && !TextUtils.isEmpty(wVar.mo2531e())) {
                sb.append(" with value ");
                sb.append(wVar.mo2531e());
            }
            mo2477a(sb.toString());
        }
    }

    /* renamed from: a */
    public void mo2516a(a aVar, String str) {
        if (m4264a(aVar)) {
            mo2477a(str + ": " + m4265b(aVar));
        }
    }

    /* renamed from: a */
    private boolean m4264a(a aVar) {
        for (int i : f2914a) {
            if (aVar.f1986d == i) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: B */
    private String m4265b(a aVar) {
        if (aVar.f1986d == 3 && TextUtils.isEmpty(aVar.f1987e)) {
            return "Native crash of app";
        }
        if (aVar.f1986d != 4) {
            return aVar.f1987e;
        }
        StringBuilder sb = new StringBuilder(aVar.f1987e);
        if (aVar.f1988f != null) {
            String str = new String(aVar.f1988f);
            if (!TextUtils.isEmpty(str)) {
                sb.append(" with value ");
                sb.append(str);
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    public void mo2517a(e eVar, String str) {
        for (e.a a : eVar.d) {
            mo2516a(a, str);
        }
    }
}
