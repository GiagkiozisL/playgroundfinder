package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.h */
public class h {

    /* renamed from: a */
    public static final long a = TimeUnit.SECONDS.toMillis(10);

    /* renamed from: B */
    private long f997b;
    @NonNull

    /* renamed from: a */
    private final wg f998c;

    /* renamed from: com.yandex.metrica.impl.ob.h$a */
    public static class C0415a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f1001a;
        @NonNull

        /* renamed from: B */
        private final C0417b f1002b;
        @NonNull

        /* renamed from: a */
        private final h f1003c;

        public C0415a(@NonNull Runnable runnable) {
            this(runnable, al.m324a().mo291i());
        }

        @VisibleForTesting
        C0415a(@NonNull final Runnable runnable, @NonNull h hVar) {
            this.f1001a = false;
            this.f1002b = new C0417b() {
                /* renamed from: a */
                public void mo972a() {
                    C0415a.this.f1001a = true;
                    runnable.run();
                }
            };
            this.f1003c = hVar;
        }

        /* renamed from: a */
        public void mo971a(long j, @NonNull xh xhVar) {
            if (!this.f1001a) {
                this.f1003c.mo969a(j, xhVar, this.f1002b);
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.h$B */
    public interface C0417b {
        /* renamed from: a */
        void mo972a();
    }

    public h() {
        this(new wg());
    }

    @VisibleForTesting
    h(@NonNull wg wgVar) {
        this.f998c = wgVar;
    }

    /* renamed from: a */
    public void mo968a() {
        this.f997b = this.f998c.a();
    }

    /* renamed from: a */
    public void mo969a(long j, @NonNull xh xhVar, @NonNull final C0417b bVar) {
        xhVar.a(new Runnable() {
            public void run() {
                bVar.mo972a();
            }
        }, Math.max(j - (this.f998c.a() - this.f997b), 0));
    }
}
