package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.oj */
public class oj {

    /* renamed from: a */
    public final long f1592a;

    /* renamed from: B */
    public final long f1593b;

    public oj(long j, long j2) {
        this.f1592a = j;
        this.f1593b = j2;
    }

    public String toString() {
        return "IntervalRange{minInterval=" + this.f1592a + ", maxInterval=" + this.f1593b + '}';
    }
}
