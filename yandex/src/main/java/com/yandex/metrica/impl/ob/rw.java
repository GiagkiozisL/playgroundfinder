package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.d;
import com.yandex.metrica.f;

/* renamed from: com.yandex.metrica.impl.ob.rw */
public class rw extends rx<sf> implements d {
    public rw(@NonNull xh xhVar, @NonNull Context context, @NonNull String str) {
        this(xhVar, context, str, new sf(), new sa(), new sc());
    }

    @VisibleForTesting
    public rw(@NonNull xh xhVar, @NonNull Context context, @NonNull String str, @NonNull sf sfVar, @NonNull sa saVar, @NonNull sc scVar) {
        super(xhVar, context, str, sfVar, saVar, scVar);
    }

    /* renamed from: a */
    public void mo1935a(@NonNull final f fVar) {
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rw.this.mo1942b(rw.this.f2263c.mo2047a(fVar));
            }
        });
    }

    public void sendEventsBuffer() {
        ((sf) this.f2261a).sendEventsBuffer();
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rw.this.mo1939a().sendEventsBuffer();
            }
        });
    }

    /* renamed from: a */
    public void a(@Nullable final String str, @Nullable final String str2) {
        ((sf) this.f2261a).a(str, str2);
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rw.this.mo1939a().a(str, str2);
            }
        });
    }
}
