package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.ConfigurationJobService;

@TargetApi(26)
/* renamed from: com.yandex.metrica.impl.ob.jl */
public class jl implements jm, jp {
    @NonNull

    /* renamed from: a */
    private final Context f1141a;
    @Nullable

    /* renamed from: B */
    private final JobScheduler f1142b;

    @SuppressLint("WrongConstant")
    public jl(@NonNull Context context) {
        this(context, (JobScheduler) context.getSystemService("jobscheduler"));
    }

    /* renamed from: a */
    @SuppressLint("WrongConstant")
    public void a(long j, boolean z) {
        final Builder minimumLatency = new Builder(1512302345, new ComponentName(this.f1141a.getPackageName(), ConfigurationJobService.class.getName())).setMinimumLatency(j);
        if (z) {
            minimumLatency.setOverrideDeadline(j);
        }
        cx.a((wn<JobScheduler>) new wn<JobScheduler>() {
            /* renamed from: a */
            public void a(JobScheduler jobScheduler) throws Throwable {
                if (jobScheduler.schedule(minimumLatency.build()) != 1) {
                }
            }
        }, this.f1142b, "scheduling wakeup in [ConfigurationJobServiceController]", "JobScheduler");
    }

    /* renamed from: a */
    public void a() {
        cx.a((wn<JobScheduler>) new wn<JobScheduler>() {
            /* renamed from: a */
            public void a(JobScheduler jobScheduler) {
                jobScheduler.cancel(1512302345);
            }
        }, this.f1142b, "cancelling scheduled wakeup in [ConfigurationJobServiceController]", "JobScheduler");
    }

    /* renamed from: a */
    @SuppressLint("WrongConstant")
    public void a(@NonNull Bundle bundle) {
        final JobInfo build = new Builder(1512302346, new ComponentName(this.f1141a.getPackageName(), ConfigurationJobService.class.getName())).setTransientExtras(bundle).setOverrideDeadline(10).build();
        cx.a((wn<JobScheduler>) new wn<JobScheduler>() {
            /* renamed from: a */
            public void a(JobScheduler jobScheduler) throws Throwable {
                if (jobScheduler.schedule(build) != 1) {
                }
            }
        }, this.f1142b, "launching [ConfigurationJobServiceController] command", "JobScheduler");
    }

    /* renamed from: B */
    public void mo1109b(@Nullable Bundle bundle) {
        Intent intent = new Intent("com.yandex.metrica.configuration.service.PLC");
        if (bundle == null) {
            bundle = new Bundle();
        }
        final JobWorkItem jobWorkItem = new JobWorkItem(intent.putExtras(bundle));
        final JobInfo build = new Builder(1512302347, new ComponentName(this.f1141a.getPackageName(), ConfigurationJobService.class.getName())).setOverrideDeadline(10).build();
        cx.a((wn<JobScheduler>) new wn<JobScheduler>() {
            /* renamed from: a */
            public void a(JobScheduler jobScheduler) {
                jobScheduler.enqueue(build, jobWorkItem);
            }
        }, this.f1142b, "ble callback", "JobScheduler");
    }

    @VisibleForTesting
    jl(@NonNull Context context, @Nullable JobScheduler jobScheduler) {
        this.f1141a = context;
        this.f1142b = jobScheduler;
    }
}
