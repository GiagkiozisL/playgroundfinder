package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.yj */
public class yj implements yk<List<yi>> {
    /* renamed from: a */
    public yi a(@Nullable List<yi> list) {
        boolean z;
        LinkedList linkedList = new LinkedList();
        boolean z2 = true;
        Iterator it = list.iterator();
        while (true) {
            z = z2;
            if (!it.hasNext()) {
                break;
            }
            yi yiVar = (yi) it.next();
            if (!yiVar.a()) {
                linkedList.add(yiVar.b());
                z2 = false;
            } else {
                z2 = z;
            }
        }
        if (z) {
            return yi.a(this);
        }
        return yi.a(this, TextUtils.join(", ", linkedList));
    }
}
