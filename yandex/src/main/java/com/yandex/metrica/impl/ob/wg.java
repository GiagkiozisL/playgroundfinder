package com.yandex.metrica.impl.ob;

import android.os.SystemClock;

/* renamed from: com.yandex.metrica.impl.ob.wg */
public class wg implements wh {
    /* renamed from: a */
    public long a() {
        return System.currentTimeMillis();
    }

    /* renamed from: B */
    public long b() {
        return System.currentTimeMillis() / 1000;
    }

    /* renamed from: a */
    public long c() {
        return SystemClock.elapsedRealtime();
    }

    /* renamed from: d */
    public long mo2568d() {
        return System.nanoTime();
    }
}
