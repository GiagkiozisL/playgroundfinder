package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.iz */
public class iz {
    @NonNull

    /* renamed from: a */
    private final jh f1086a;
    @Nullable

    /* renamed from: B */
    private final Long f1087b;
    @Nullable

    /* renamed from: a */
    private final Long f1088c;
    @Nullable

    /* renamed from: d */
    private final Integer f1089d;
    @Nullable

    /* renamed from: e */
    private final Long f1090e;
    @Nullable

    /* renamed from: f */
    private final Boolean f1091f;
    @Nullable

    /* renamed from: g */
    private final Long f1092g;
    @Nullable

    /* renamed from: h */
    private final Long f1093h;

    /* renamed from: com.yandex.metrica.impl.ob.iz$a */
    static final class C0474a {
        @Nullable

        /* renamed from: a */
        public Long f1094a;
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: B */
        public jh f1095b;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: a */
        public Long f1096c;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: d */
        public Long f1097d;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: e */
        public Integer f1098e;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: f */
        public Long f1099f;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: g */
        public Boolean f1100g;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: h */
        public Long f1101h;

        private C0474a(jb jbVar) {
            this.f1095b = jbVar.mo1067a();
            this.f1098e = jbVar.mo1068b();
        }

        /* renamed from: a */
        public C0474a mo1059a(Long l) {
            this.f1096c = l;
            return this;
        }

        /* renamed from: B */
        public C0474a mo1061b(Long l) {
            this.f1097d = l;
            return this;
        }

        /* renamed from: a */
        public C0474a mo1062c(Long l) {
            this.f1099f = l;
            return this;
        }

        /* renamed from: a */
        public C0474a mo1058a(Boolean bool) {
            this.f1100g = bool;
            return this;
        }

        /* renamed from: d */
        public C0474a mo1063d(Long l) {
            this.f1101h = l;
            return this;
        }

        /* renamed from: e */
        public C0474a mo1064e(Long l) {
            this.f1094a = l;
            return this;
        }

        /* renamed from: a */
        public iz mo1060a() {
            return new iz(this);
        }
    }

    private iz(C0474a aVar) {
        this.f1086a = aVar.f1095b;
        this.f1089d = aVar.f1098e;
        this.f1087b = aVar.f1096c;
        this.f1088c = aVar.f1097d;
        this.f1090e = aVar.f1099f;
        this.f1091f = aVar.f1100g;
        this.f1092g = aVar.f1101h;
        this.f1093h = aVar.f1094a;
    }

    /* renamed from: a */
    public static final C0474a m1867a(jb jbVar) {
        return new C0474a(jbVar);
    }

    /* renamed from: a */
    public jh mo1052a() {
        return this.f1086a;
    }

    /* renamed from: a */
    public long mo1051a(long j) {
        return this.f1087b == null ? j : this.f1087b.longValue();
    }

    /* renamed from: B */
    public long mo1054b(long j) {
        return this.f1088c == null ? j : this.f1088c.longValue();
    }

    /* renamed from: a */
    public int mo1050a(int i) {
        return this.f1089d == null ? i : this.f1089d.intValue();
    }

    /* renamed from: a */
    public long mo1055c(long j) {
        return this.f1090e == null ? j : this.f1090e.longValue();
    }

    /* renamed from: a */
    public boolean mo1053a(boolean z) {
        return this.f1091f == null ? z : this.f1091f.booleanValue();
    }

    /* renamed from: d */
    public long mo1056d(long j) {
        return this.f1092g == null ? j : this.f1092g.longValue();
    }

    /* renamed from: e */
    public long mo1057e(long j) {
        return this.f1093h == null ? j : this.f1093h.longValue();
    }
}
