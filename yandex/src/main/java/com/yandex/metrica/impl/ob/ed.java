package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.CounterConfiguration;

/* renamed from: com.yandex.metrica.impl.ob.ed */
public class ed {
    @Nullable

    /* renamed from: a */
    private final ee f760a;
    @NonNull

    /* renamed from: B */
    private final CounterConfiguration f761b;

    public ed(@NonNull Bundle bundle) {
        this.f760a = ee.m1404a(bundle);
        this.f761b = CounterConfiguration.m20c(bundle);
    }

    public ed(@NonNull ee eeVar, @NonNull CounterConfiguration counterConfiguration) {
        this.f760a = eeVar;
        this.f761b = counterConfiguration;
    }

    @NonNull
    /* renamed from: g */
    public ee mo743g() {
        return this.f760a;
    }

    @NonNull
    /* renamed from: h */
    public CounterConfiguration mo744h() {
        return this.f761b;
    }

    public String toString() {
        return "ClientConfiguration{mProcessConfiguration=" + this.f760a + ", mCounterConfiguration=" + this.f761b + '}';
    }
}
