package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.fu */
public class fu implements ep {

    /* renamed from: a */
    private final HashMap<String, fc> f931a = new HashMap<>();

    /* renamed from: B */
    private final HashMap<String, ei> f932b = new HashMap<>();

    /* renamed from: a */
    private final Context c;

    public fu(@NonNull Context context) {
        this.c = context.getApplicationContext();
    }

    @Nullable
    /* renamed from: a */
    public synchronized fc mo923a(@NonNull ek ekVar) {
        return (fc) this.f931a.get(ekVar.toString());
    }

    @NonNull
    /* renamed from: a */
    public synchronized fc mo924a(@NonNull ek ekVar, @NonNull eg egVar, @NonNull eq<fc> eqVar) {
        return (fc) m1640a(ekVar, egVar, eqVar, this.f931a);
    }

    @NonNull
    /* renamed from: B */
    public synchronized ei mo925b(@NonNull ek ekVar, @NonNull eg egVar, @NonNull eq<ei> eqVar) {
        return (ei) m1640a(ekVar, egVar, eqVar, this.f932b);
    }

    @NonNull
    /* renamed from: a */
    private <T extends ev> T m1640a(@NonNull ek var1, @NonNull eg var2, @NonNull eq<T> var3, @NonNull Map<String, T> var4) {
//        ev t = (ev) map.get(ekVar.toString());
//        if (t == null) {
//            T b = eqVar.b(this.a, ekVar, egVar);
//            map.put(ekVar.toString(), b);
//            return b;
//        }
//        t.a(egVar);
//        return t;
        ev var5 = (ev)var4.get(var1.toString());
        if (var5 == null) {
            var5 = var3.b(this.c, var1, var2);
            var4.put(var1.toString(), (T) var5);
        } else {
            var5.a(var2);
        }

        return (T) var5;
    }

    /* renamed from: a */
    public synchronized void c() {
        for (ep c : this.f931a.values()) {
            c.c();
        }
        for (ep c2 : this.f932b.values()) {
            c2.c();
        }
        this.f931a.clear();
        this.f932b.clear();
    }
}
