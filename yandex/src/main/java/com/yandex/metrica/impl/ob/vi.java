package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.vi */
public class vi {
    /* renamed from: a */
    public static boolean m4192a(@Nullable Boolean bool) {
        return Boolean.TRUE.equals(bool);
    }

    /* renamed from: B */
    public static boolean m4193b(@Nullable Boolean bool) {
        return !m4192a(bool);
    }

    /* renamed from: a */
    public static boolean m4194c(@Nullable Boolean bool) {
        return Boolean.FALSE.equals(bool);
    }
}
