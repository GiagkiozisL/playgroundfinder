package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/* renamed from: com.yandex.metrica.impl.ob.xh */
public interface xh extends xj {
    /* renamed from: a */
    <T> Future<T> a(Callable<T> callable);

    /* renamed from: a */
    void a(@NonNull Runnable runnable);

    /* renamed from: a */
    void a(@NonNull Runnable runnable, long j);

    /* renamed from: B */
    void b(@NonNull Runnable runnable);
}
