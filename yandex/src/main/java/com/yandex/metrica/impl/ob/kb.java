package com.yandex.metrica.impl.ob;

import android.os.FileObserver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.kb */
public class kb extends FileObserver {

    /* renamed from: a */
    private final wm<File> f1192a;

    /* renamed from: B */
    private final File f1193b;

    public kb(@NonNull File file, @NonNull wm<File> wmVar) {
        super(file.getAbsolutePath(), 8);
        this.f1192a = wmVar;
        this.f1193b = file;
    }

    public void onEvent(int event, @Nullable String path) {
        if (event == 8 && !TextUtils.isEmpty(path)) {
            this.f1192a.a(new File(this.f1193b, path));
        }
    }
}
