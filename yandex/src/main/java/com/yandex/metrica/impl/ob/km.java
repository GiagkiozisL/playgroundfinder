package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rg.C0810a;
import com.yandex.metrica.impl.ob.rg.C0814e;

import java.util.Iterator;

/* renamed from: com.yandex.metrica.impl.ob.km */
public class km implements mq<kg, C0810a> {
    @NonNull

    /* renamed from: a */
    private kr f1221a;

    public km(@NonNull kr krVar) {
        this.f1221a = krVar;
    }

    @NonNull
    /* renamed from: a */
    public C0810a b(@NonNull kg kgVar) {
        C0810a aVar = new C0810a();
        aVar.f1880b = this.f1221a.b(kgVar.f1202a);
        aVar.f1881c = new C0814e[kgVar.f1203b.size()];
        int i = 0;
        Iterator it = kgVar.f1203b.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return aVar;
            }
            aVar.f1881c[i2] = this.f1221a.b((kk) it.next());
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    public kg a(@NonNull C0810a aVar) {
        throw new UnsupportedOperationException();
    }
}
