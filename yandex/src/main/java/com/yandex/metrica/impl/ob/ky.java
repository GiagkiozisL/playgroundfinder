package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.CounterConfiguration.C0008a;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ky */
public class ky {
    @NonNull

    /* renamed from: a */
    private final Context a;
    @NonNull

    /* renamed from: B */
    private final C0008a f1252b;
    @Nullable

    /* renamed from: a */
    private final ck f1253c;

    /* renamed from: com.yandex.metrica.impl.ob.ky$a */
    static class a {
        @Nullable

        /* renamed from: a */
        public final List<ContentValues> f1254a;

        /* renamed from: B */
        public final int f1255b;

        a(@Nullable List<ContentValues> list, int i) {
            this.f1254a = list;
            this.f1255b = i;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ky$B */
    public enum C0552b {
        BAD_REQUEST("bad_request"),
        DB_OVERFLOW("db_overflow");

        /* access modifiers changed from: private */

        /* renamed from: a */
        public final String f1259c;

        private C0552b(String str) {
            this.f1259c = str;
        }
    }

    public ky(@NonNull Context context, @NonNull C0008a aVar) {
        this(context, aVar, al.m324a().mo294l());
    }

    @VisibleForTesting
    ky(@NonNull Context context, @NonNull C0008a aVar, @Nullable ck ckVar) {
        this.a = context;
        this.f1252b = aVar;
        this.f1253c = ckVar;
    }

    @Nullable
    /* renamed from: a */
    private List<ContentValues> a(@Nullable Cursor cursor) {
        ArrayList arrayList = null;
        if (cursor != null && cursor.getCount() > 0) {
            arrayList = new ArrayList(cursor.getCount());
            while (cursor.moveToNext()) {
                ContentValues contentValues = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                arrayList.add(contentValues);
            }
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public ky.a mo1193a(@NonNull SQLiteDatabase sQLiteDatabase, @NonNull String str, @NonNull String str2, @NonNull C0552b bVar, @Nullable String str3, boolean z) {
        List a = m2092a(sQLiteDatabase, str, str2);
        int i = 0;
        if (!cx.a((Collection) a)) {
            try {
                i = sQLiteDatabase.delete(str, str2, null);
            } catch (Throwable th) {
            }
            if (z) {
                m2093b(a, bVar, str3, i);
            }
            if (i != a.size()) {
            }
        } else {
            HashMap hashMap = new HashMap();
            hashMap.put("table_name", str);
            hashMap.put("api_key", String.valueOf(str3));
            tl.a(this.a).reportEvent("select_rows_to_delete_failed", (Map<String, Object>) hashMap);
        }
        return new a(a, i);
    }

    private List<ContentValues> m2092a(@NonNull SQLiteDatabase var1, @NonNull String var2, @NonNull String var3) {

        List var4 = null;
        Cursor var5 = null;

        try {
            var5 = var1.rawQuery(String.format("SELECT %s, %s, %s FROM %s WHERE %s", "global_number", "type", "number_of_type", "reports", var3), (String[])null);
            var4 = this.a(var5);
        } catch (Throwable var10) {
            tl.a(this.a).reportError("select_rows_to_delete_exception", var10);
        } finally {
            cx.a(var5);
        }

        return var4;

    }

    @Nullable
    /* renamed from: a */
    private w m2090a(@NonNull List<ContentValues> list, @NonNull C0552b bVar, @Nullable String str, int i) {
        vz a;
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONArray jSONArray2 = new JSONArray();
            JSONArray jSONArray3 = new JSONArray();
            for (ContentValues contentValues : list) {
                Integer asInteger = contentValues.getAsInteger("global_number");
                Integer asInteger2 = contentValues.getAsInteger("type");
                Integer asInteger3 = contentValues.getAsInteger("number_of_type");
                if (!(asInteger == null || asInteger2 == null || asInteger3 == null)) {
                    jSONArray.put(asInteger);
                    jSONArray2.put(asInteger2);
                    jSONArray3.put(asInteger3);
                }
            }
            jSONObject.put("global_number", jSONArray).put("event_type", jSONArray2).put("number_of_type", jSONArray3);
            JSONObject put = new JSONObject().put("details", new JSONObject().put("reason", bVar.f1259c).put("cleared", jSONObject).put("actual_deleted_number", i));
            if (str == null) {
                a = vr.m4236a();
            } else {
                a = vr.m4237a(str);
            }
            return af.m297i(put.toString(), a);
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: B */
    private void m2093b(@NonNull List<ContentValues> list, @NonNull C0552b bVar, @Nullable String str, int i) {
        if (str != null && this.f1253c != null) {
            cj a = this.f1253c.mo609a(str, this.f1252b);
            if (a != null) {
                w a2 = m2090a(list, bVar, str, i);
                if (a2 != null) {
                    a.mo608a(a2);
                }
            }
        }
    }
}
