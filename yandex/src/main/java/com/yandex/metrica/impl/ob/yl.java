package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.UUID;

/* renamed from: com.yandex.metrica.impl.ob.yl */
public class yl implements yk<String> {
    /* renamed from: a */
    public yi a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return yi.a(this, "ApiKey is empty. Please, read official documentation how to obtain one: https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/concepts/android-initialize-docpage/");
        }
        try {
            UUID.fromString(str);
            return yi.a(this);
        } catch (Throwable th) {
            return yi.a(this, "Invalid ApiKey=" + str + ". " + "Please, read official documentation how to obtain one:" + " " + "https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/concepts/android-initialize-docpage/");
        }
    }
}
