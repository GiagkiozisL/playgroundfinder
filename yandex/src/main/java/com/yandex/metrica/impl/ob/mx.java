package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0874f;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.mx */
public class mx implements mq<List<pu>, C0874f[]> {


    @NonNull
    /* renamed from: a */
    public C0874f[] b(@NonNull List<pu> list) {
        C0874f[] fVarArr = new C0874f[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return fVarArr;
            }
            fVarArr[i2] = m2472a((pu) list.get(i2));
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    public List<pu> a(@NonNull C0874f[] fVarArr) {
        ArrayList arrayList = new ArrayList(fVarArr.length);
        for (C0874f a : fVarArr) {
            arrayList.add(m2471a(a));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    private C0874f m2472a(@NonNull pu puVar) {
        C0874f fVar = new C0874f();
        fVar.f2183b = puVar.name;
        fVar.f2184c = puVar.granted;
        return fVar;
    }

    @NonNull
    /* renamed from: a */
    private pu m2471a(@NonNull C0874f fVar) {
        return new pu(fVar.f2183b, fVar.f2184c);
    }

//    @NonNull
//    @Override
//    public Object a(@NonNull Object o) {
//        return null;
//    }
//
}
