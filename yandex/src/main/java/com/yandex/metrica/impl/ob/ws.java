package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Base64;

/* renamed from: com.yandex.metrica.impl.ob.ws */
public class ws implements wx {
    @NonNull

    /* renamed from: a */
    private final Crypto crypto;

    ws(@NonNull Context context) {
        this(new wq(context));
    }

    ws(@NonNull wq wqVar) {
        this(new Crypto("AES/CBC/PKCS5Padding", wqVar.getPkgMd5Bts(), wqVar.getPkgReverseMd5Bts()));
    }

    @VisibleForTesting
    ws(@NonNull Crypto cryptoVar) {
        this.crypto = cryptoVar;
    }

    @NonNull
    /* renamed from: a */
    public ww a(@NonNull w wVar) {
        String e = wVar.mo2531e();
        String str = null;
        if (!TextUtils.isEmpty(e)) {
            try {
                byte[] a = this.crypto.encrypt(e.getBytes("UTF-8"));
                if (a != null) {
                    str = Base64.encodeToString(a, 0);
                }
            } catch (Throwable th) {
            }
        }
        return new ww(wVar.mo1767c(str), mo2583a());
    }

    @NonNull
    /* renamed from: a */
    public byte[] a(@Nullable byte[] bArr) {
        byte[] bArr2 = new byte[0];
        if (bArr == null || bArr.length <= 0) {
            return bArr2;
        }
        try {
            return this.crypto.decrypt(Base64.decode(bArr, 0));
        } catch (Throwable th) {
            return bArr2;
        }
    }

    @NonNull
    /* renamed from: a */
    public wz mo2583a() {
        return wz.AES_VALUE_ENCRYPTION;
    }
}
