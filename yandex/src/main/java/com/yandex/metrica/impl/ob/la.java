package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.SparseArray;

import com.yandex.metrica.impl.ob.lj.C0569a;
import com.yandex.metrica.impl.ob.lq.C0577a;
import com.yandex.metrica.impl.ob.lq.C0580b;
import com.yandex.metrica.impl.ob.lq.C0582d;
import com.yandex.metrica.impl.ob.lq.C0586h;

import java.util.HashMap;

/* renamed from: com.yandex.metrica.impl.ob.la */
public class la {
    @NonNull

    /* renamed from: a */
    private final lb f1281a;
    @NonNull

    /* renamed from: B */
    private final le f1282b;
    @NonNull

    /* renamed from: a */
    private final C0569a f1283c;

    public la(@NonNull lb lbVar, @NonNull le leVar) {
        this(lbVar, leVar, new C0569a());
    }

    public la(@NonNull lb lbVar, @NonNull le leVar, @NonNull C0569a aVar) {
        this.f1281a = lbVar;
        this.f1282b = leVar;
        this.f1283c = aVar;
    }

    /* renamed from: a */
    public lj mo1216a() {
        return this.f1283c.mo1266a("main", this.f1281a.mo1221c(), this.f1281a.mo1222d(), this.f1281a.mo1219a(), new ll("main", this.f1282b.mo1244a()));
    }

    /* renamed from: B */
    public lj mo1217b() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", C0582d.f1353a);
        hashMap.put("binary_data", C0580b.f1352a);
        hashMap.put("startup", C0586h.f1353a);
        hashMap.put("l_dat", C0577a.f1347a);
        hashMap.put("lbs_dat", C0577a.f1347a);
        return this.f1283c.mo1266a("metrica.db", this.f1281a.mo1225g(), this.f1281a.mo1226h(), this.f1281a.mo1220b(), new ll("metrica.db", hashMap));
    }

    /* renamed from: a */
    public lj mo1218c() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", C0582d.f1353a);
        return this.f1283c.mo1266a("client storage", this.f1281a.mo1223e(), this.f1281a.mo1224f(), new SparseArray(), new ll("metrica.db", hashMap));
    }
}
