package com.yandex.metrica.impl.ob;

import androidx.annotation.VisibleForTesting;
import android.util.Log;

/* renamed from: com.yandex.metrica.impl.ob.vh */
public abstract class vh {

    /* renamed from: a */
    private volatile boolean f2900a = false;

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public abstract String mo2486d(String str, Object[] objArr);

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public abstract String mo2489f();

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public abstract String mo2490g();

    /* renamed from: a */
    public void mo2474a() {
        this.f2900a = true;
    }

    /* renamed from: B */
    public void mo2480b() {
        this.f2900a = false;
    }

    /* renamed from: a */
    public boolean mo2485c() {
        return this.f2900a;
    }

    public vh(boolean z) {
        this.f2900a = z;
    }

    /* renamed from: a */
    public void mo2477a(String str) {
        mo2475a(4, str);
    }

    /* renamed from: B */
    public void mo2481b(String str) {
        mo2475a(5, str);
    }

    /* renamed from: a */
    public void mo2483c(String str) {
        mo2475a(6, str);
    }

    /* renamed from: a */
    public void mo2478a(String str, Object... objArr) {
        mo2476a(4, str, objArr);
    }

    /* renamed from: B */
    public void mo2482b(String str, Object... objArr) {
        mo2476a(5, str, objArr);
    }

    /* renamed from: a */
    public void mo2484c(String str, Object... objArr) {
        Log.println(5, mo2489f(), m4174e(str, objArr));
    }

    /* renamed from: a */
    public void mo2479a(Throwable th, String str, Object... objArr) {
        Log.println(6, mo2489f(), m4174e(str, objArr) + "\n" + Log.getStackTraceString(th));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2475a(int i, String str) {
        if (mo2487d()) {
            Log.println(i, mo2489f(), m4172d(str));
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2476a(int i, String str, Object... objArr) {
        if (mo2487d()) {
            Log.println(i, mo2489f(), m4174e(str, objArr));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public boolean mo2487d() {
        return this.f2900a;
    }

    /* renamed from: d */
    private String m4172d(String str) {
        return mo2490g() + m4173e(str);
    }

    /* renamed from: e */
    private String m4174e(String str, Object[] objArr) {
        try {
            return mo2490g() + mo2486d(m4173e(str), objArr);
        } catch (Throwable th) {
            return mo2488e();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: e */
    public String mo2488e() {
        return mo2490g();
    }

    /* renamed from: e */
    private static String m4173e(String str) {
        return str == null ? "" : str;
    }
}
