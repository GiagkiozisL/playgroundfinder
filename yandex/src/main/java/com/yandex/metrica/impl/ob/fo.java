package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: com.yandex.metrica.impl.ob.fo */
public class fo {

    /* renamed from: a */
    private final Object f917a;

    /* renamed from: B */
    private final fu f918b;

    /* renamed from: a */
    private final HashMap<fn, fp> f919c;

    /* renamed from: d */
    private final wp<C0361a, fn> f920d;
    @NonNull

    /* renamed from: e */
    private final Context f921e;

    /* renamed from: f */
    private volatile int f922f;
    @NonNull

    /* renamed from: g */
    private final fr f923g;

    /* renamed from: com.yandex.metrica.impl.ob.fo$a */
    private static final class C0361a {
        @NonNull

        /* renamed from: a */
        private final String f924a;
        @Nullable

        /* renamed from: B */
        private final Integer f925b;
        @Nullable

        /* renamed from: a */
        private final String f926c;

        C0361a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
            this.f924a = str;
            this.f925b = num;
            this.f926c = str2;
        }

        C0361a(@NonNull fn fnVar) {
            this(fnVar.mo908b(), fnVar.mo909c(), fnVar.mo910d());
        }

        public int hashCode() {
            int i;
            int i2 = 0;
            int hashCode = this.f924a.hashCode() * 31;
            if (this.f925b != null) {
                i = this.f925b.hashCode();
            } else {
                i = 0;
            }
            int i3 = (i + hashCode) * 31;
            if (this.f926c != null) {
                i2 = this.f926c.hashCode();
            }
            return i3 + i2;
        }

        public boolean equals(Object o) {
            boolean z = true;
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            C0361a aVar = (C0361a) o;
            if (!this.f924a.equals(aVar.f924a)) {
                return false;
            }
            if (this.f925b != null) {
                if (!this.f925b.equals(aVar.f925b)) {
                    return false;
                }
            } else if (aVar.f925b != null) {
                return false;
            }
            if (this.f926c != null) {
                z = this.f926c.equals(aVar.f926c);
            } else if (aVar.f926c != null) {
                z = false;
            }
            return z;
        }
    }

    public fo(@NonNull Context context, @NonNull fu fuVar) {
        this(context, fuVar, new fr());
    }

    @VisibleForTesting
    fo(@NonNull Context context, @NonNull fu fuVar, @NonNull fr frVar) {
        this.f917a = new Object();
        this.f919c = new HashMap<>();
        this.f920d = new wp<>();
        this.f922f = 0;
        this.f921e = context.getApplicationContext();
        this.f918b = fuVar;
        this.f923g = frVar;
    }

    /* renamed from: a */
    public fp mo916a(@NonNull fn fnVar, @NonNull eg egVar) {
        fp fpVar;
        synchronized (this.f917a) {
            fpVar = (fp) this.f919c.get(fnVar);
            if (fpVar == null) {
                fpVar = this.f923g.mo921a(fnVar).a(this.f921e, this.f918b, fnVar, egVar);
                this.f919c.put(fnVar, fpVar);
                this.f920d.mo2572a(new C0361a(fnVar), fnVar);
                this.f922f++;
            }
        }
        return fpVar;
    }

    /* renamed from: a */
    public void mo917a(@NonNull String str, int i, String str2) {
        m1625a(str, Integer.valueOf(i), str2);
    }

    /* renamed from: a */
    private void m1625a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
        synchronized (this.f917a) {
            Collection<fn> b = this.f920d.b(new C0361a(str, num, str2));
            if (!cx.a((Collection) b)) {
                this.f922f -= b.size();
                ArrayList arrayList = new ArrayList(b.size());
                for (fn remove : b) {
                    arrayList.add(this.f919c.remove(remove));
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((fp) it.next()).a();
                }
            }
        }
    }

    /* renamed from: a */
    public int mo915a() {
        return this.f922f;
    }
}
