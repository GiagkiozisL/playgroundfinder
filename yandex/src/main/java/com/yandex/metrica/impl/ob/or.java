package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.or */
class or {

    /* renamed from: a */
    static final long f1654a = TimeUnit.SECONDS.toMillis(1);
    @NonNull

    /* renamed from: B */
    private Context f1655b;
    @NonNull

    /* renamed from: a */
    private Looper f1656c;
    @Nullable

    /* renamed from: d */
    private LocationManager f1657d;
    @NonNull

    /* renamed from: e */
    private LocationListener f1658e;
    @NonNull

    /* renamed from: f */
    private pr f1659f;

    public or(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull LocationListener locationListener, @NonNull pr prVar) {
        this.f1655b = context;
        this.f1656c = looper;
        this.f1657d = locationManager;
        this.f1658e = locationListener;
        this.f1659f = prVar;
    }

    /* renamed from: a */
    public void mo1593a() {
        if (this.f1659f.mo1652b(this.f1655b)) {
            m2735a("passive", 0.0f, f1654a, this.f1658e, this.f1656c);
        }
    }

    /* renamed from: B */
    public void mo1594b() {
        if (this.f1657d != null) {
            try {
                this.f1657d.removeUpdates(this.f1658e);
            } catch (Throwable th) {
            }
        }
    }

    /* renamed from: a */
    private void m2735a(String str, float f, long j, @NonNull LocationListener locationListener, @NonNull Looper looper) {
        if (this.f1657d != null) {
            try {
                this.f1657d.requestLocationUpdates(str, j, f, locationListener, looper);
            } catch (Throwable th) {
            }
        }
    }
}
