package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;
import java.util.Arrays;

/* renamed from: com.yandex.metrica.impl.ob.rr */
public interface rr {

    /* renamed from: com.yandex.metrica.impl.ob.rr$a */
    public static final class a extends e {

        /* renamed from: A */
        public String f2092A;

        /* renamed from: B */
        public String f2093B;

        /* renamed from: C */
        public long f2094C;

        /* renamed from: D */
        public long f2095D;

        /* renamed from: E */
        public boolean f2096E;

        /* renamed from: F */
        public C0875g f2097F;

        /* renamed from: G */
        public C0872d f2098G;

        /* renamed from: H */
        public b f2099H;

        /* renamed from: B */
        public String f2100b;

        /* renamed from: a */
        public long f2101c;

        /* renamed from: d */
        public String[] f2102d;

        /* renamed from: e */
        public String f2103e;

        /* renamed from: f */
        public String f2104f;

        /* renamed from: g */
        public String[] f2105g;

        /* renamed from: h */
        public String[] f2106h;

        /* renamed from: i */
        public String[] f2107i;

        /* renamed from: j */
        public C0871c f2108j;

        /* renamed from: k */
        public C0873e f2109k;

        /* renamed from: l */
        public C0863a f2110l;

        /* renamed from: m */
        public C0879j f2111m;

        /* renamed from: n */
        public C0876h[] f2112n;

        /* renamed from: o */
        public String f2113o;

        /* renamed from: p */
        public String f2114p;

        /* renamed from: q */
        public boolean f2115q;

        /* renamed from: r */
        public String f2116r;

        /* renamed from: s */
        public String f2117s;

        /* renamed from: t */
        public String[] f2118t;

        /* renamed from: u */
        public C0880k f2119u;

        /* renamed from: v */
        public boolean f2120v;

        /* renamed from: w */
        public C0874f[] f2121w;

        /* renamed from: x */
        public C0878i f2122x;

        /* renamed from: y */
        public String f2123y;

        /* renamed from: z */
        public String f2124z;

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$a */
        public static final class C0863a extends e {

            /* renamed from: B */
            public C0873e f2125b;

            /* renamed from: a */
            public long f2126c;

            /* renamed from: d */
            public long f2127d;

            /* renamed from: e */
            public boolean f2128e;

            /* renamed from: f */
            public C0864a[] f2129f;

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$a$a */
            public static final class C0864a extends e {

                /* renamed from: d */
                private static volatile C0864a[] f2130d;

                /* renamed from: B */
                public long f2131b;

                /* renamed from: a */
                public long f2132c;

                /* renamed from: d */
                public static C0864a[] m3285d() {
                    if (f2130d == null) {
                        synchronized (c.a) {
                            if (f2130d == null) {
                                f2130d = new C0864a[0];
                            }
                        }
                    }
                    return f2130d;
                }

                public C0864a() {
                    mo1867e();
                }

                /* renamed from: e */
                public C0864a mo1867e() {
                    this.f2131b = 0;
                    this.f2132c = 0;
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    bVar.b(1, this.f2131b);
                    bVar.b(2, this.f2132c);
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    return super.mo741c() + com.yandex.metrica.impl.ob.b.m450e(1, this.f2131b) + com.yandex.metrica.impl.ob.b.m450e(2, this.f2132c);
                }

                /* renamed from: B */
                public C0864a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case 8:
                                this.f2131b = aVar.mo223f();
                                continue;
                            case 16:
                                this.f2132c = aVar.mo223f();
                                continue;
                            default:
                                if (!g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            public C0863a() {
                mo1865d();
            }

            /* renamed from: d */
            public C0863a mo1865d() {
                this.f2125b = null;
                this.f2126c = 60000;
                this.f2127d = 3600000;
                this.f2128e = false;
                this.f2129f = C0864a.m3285d();
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (this.f2125b != null) {
                    bVar.mo350a(1, (e) this.f2125b);
                }
                bVar.b(2, this.f2126c);
                bVar.b(3, this.f2127d);
                bVar.mo352a(4, this.f2128e);
                if (this.f2129f != null && this.f2129f.length > 0) {
                    for (C0864a aVar : this.f2129f) {
                        if (aVar != null) {
                            bVar.mo350a(5, (e) aVar);
                        }
                    }
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (this.f2125b != null) {
                    c += com.yandex.metrica.impl.ob.b.b(1, (e) this.f2125b);
                }
                int b = com.yandex.metrica.impl.ob.b.m438b(4, this.f2128e) + c + com.yandex.metrica.impl.ob.b.m450e(2, this.f2126c) + com.yandex.metrica.impl.ob.b.m450e(3, this.f2127d);
                if (this.f2129f != null && this.f2129f.length > 0) {
                    for (C0864a aVar : this.f2129f) {
                        if (aVar != null) {
                            b += com.yandex.metrica.impl.ob.b.b(5, (e) aVar);
                        }
                    }
                }
                return b;
            }

            /* renamed from: B */
            public C0863a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            if (this.f2125b == null) {
                                this.f2125b = new C0873e();
                            }
                            aVar.mo215a((e) this.f2125b);
                            continue;
                        case 16:
                            this.f2126c = aVar.mo223f();
                            continue;
                        case 24:
                            this.f2127d = aVar.mo223f();
                            continue;
                        case 32:
                            this.f2128e = aVar.mo228h();
                            continue;
                        case 42:
                            int b = g.b(aVar, 42);
                            if (this.f2129f == null) {
                                length = 0;
                            } else {
                                length = this.f2129f.length;
                            }
                            C0864a[] aVarArr = new C0864a[(b + length)];
                            if (length != 0) {
                                System.arraycopy(this.f2129f, 0, aVarArr, 0, length);
                            }
                            while (length < aVarArr.length - 1) {
                                aVarArr[length] = new C0864a();
                                aVar.mo215a((e) aVarArr[length]);
                                aVar.mo213a();
                                length++;
                            }
                            aVarArr[length] = new C0864a();
                            aVar.mo215a((e) aVarArr[length]);
                            this.f2129f = aVarArr;
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$B */
        public static final class b extends e {

            /* renamed from: B */
            public B b;

            /* renamed from: a */
            public Aa[] c;

            /* renamed from: d */
            public long d;

            /* renamed from: e */
            public long e;

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$B$a */
            public static final class Aa extends e {

                /* renamed from: g */
                private static volatile Aa[] f2137g;

                /* renamed from: B */
                public String f2138b;

                /* renamed from: a */
                public String f2139c;

                /* renamed from: d */
                public C0867a f2140d;

                /* renamed from: e */
                public C0868b f2141e;

                /* renamed from: f */
                public C0869c f2142f;

                /* renamed from: com.yandex.metrica.impl.ob.rr$a$B$a$a */
                public static final class C0867a extends e {

                    /* renamed from: B */
                    public int f2143b;

                    /* renamed from: a */
                    public byte[] f2144c;

                    /* renamed from: d */
                    public byte[] f2145d;

                    public C0867a() {
                        mo1873d();
                    }

                    /* renamed from: d */
                    public C0867a mo1873d() {
                        this.f2143b = 0;
                        this.f2144c = g.h;
                        this.f2145d = g.h;
                        this.f754a = -1;
                        return this;
                    }

                    /* renamed from: a */
                    public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                        bVar.mo348a(1, this.f2143b);
                        if (!Arrays.equals(this.f2144c, g.h)) {
                            bVar.mo353a(2, this.f2144c);
                        }
                        if (!Arrays.equals(this.f2145d, g.h)) {
                            bVar.mo353a(3, this.f2145d);
                        }
                        super.mo739a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public int mo741c() {
                        int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m445d(1, this.f2143b);
                        if (!Arrays.equals(this.f2144c, g.h)) {
                            c += com.yandex.metrica.impl.ob.b.b(2, this.f2144c);
                        }
                        if (!Arrays.equals(this.f2145d, g.h)) {
                            return c + com.yandex.metrica.impl.ob.b.b(3, this.f2145d);
                        }
                        return c;
                    }

                    /* renamed from: B */
                    public C0867a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                        while (true) {
                            int a = aVar.mo213a();
                            switch (a) {
                                case 0:
                                    break;
                                case 8:
                                    this.f2143b = aVar.mo225g();
                                    continue;
                                case 18:
                                    this.f2144c = aVar.mo230j();
                                    continue;
                                case 26:
                                    this.f2145d = aVar.mo230j();
                                    continue;
                                default:
                                    if (!g.a(aVar, a)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
//                        return this;
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.rr$a$B$a$B */
                public static final class C0868b extends e {

                    /* renamed from: B */
                    public String f2146b;

                    /* renamed from: a */
                    public byte[] f2147c;

                    /* renamed from: d */
                    public byte[] f2148d;

                    public C0868b() {
                        mo1875d();
                    }

                    /* renamed from: d */
                    public C0868b mo1875d() {
                        this.f2146b = "";
                        this.f2147c = g.h;
                        this.f2148d = g.h;
                        this.f754a = -1;
                        return this;
                    }

                    /* renamed from: a */
                    public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                        bVar.mo351a(1, this.f2146b);
                        if (!Arrays.equals(this.f2147c, g.h)) {
                            bVar.mo353a(2, this.f2147c);
                        }
                        if (!Arrays.equals(this.f2148d, g.h)) {
                            bVar.mo353a(3, this.f2148d);
                        }
                        super.mo739a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public int mo741c() {
                        int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2146b);
                        if (!Arrays.equals(this.f2147c, g.h)) {
                            c += com.yandex.metrica.impl.ob.b.b(2, this.f2147c);
                        }
                        if (!Arrays.equals(this.f2148d, g.h)) {
                            return c + com.yandex.metrica.impl.ob.b.b(3, this.f2148d);
                        }
                        return c;
                    }

                    /* renamed from: B */
                    public C0868b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                        while (true) {
                            int a = aVar.mo213a();
                            switch (a) {
                                case 0:
                                    break;
                                case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                    this.f2146b = aVar.mo229i();
                                    continue;
                                case 18:
                                    this.f2147c = aVar.mo230j();
                                    continue;
                                case 26:
                                    this.f2148d = aVar.mo230j();
                                    continue;
                                default:
                                    if (!g.a(aVar, a)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
//                        return this;
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.rr$a$B$a$a */
                public static final class C0869c extends e {

                    /* renamed from: B */
                    public String f2149b;

                    /* renamed from: a */
                    public String f2150c;

                    public C0869c() {
                        mo1877d();
                    }

                    /* renamed from: d */
                    public C0869c mo1877d() {
                        this.f2149b = "";
                        this.f2150c = "";
                        this.f754a = -1;
                        return this;
                    }

                    /* renamed from: a */
                    public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                        bVar.mo351a(1, this.f2149b);
                        if (!this.f2150c.equals("")) {
                            bVar.mo351a(2, this.f2150c);
                        }
                        super.mo739a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public int mo741c() {
                        int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2149b);
                        if (!this.f2150c.equals("")) {
                            return c + com.yandex.metrica.impl.ob.b.m437b(2, this.f2150c);
                        }
                        return c;
                    }

                    /* renamed from: B */
                    public C0869c mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                        while (true) {
                            int a = aVar.mo213a();
                            switch (a) {
                                case 0:
                                    break;
                                case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                    this.f2149b = aVar.mo229i();
                                    continue;
                                case 18:
                                    this.f2150c = aVar.mo229i();
                                    continue;
                                default:
                                    if (!g.a(aVar, a)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
//                        return this;
                    }
                }

                /* renamed from: d */
                public static Aa[] m3296d() {
                    if (f2137g == null) {
                        synchronized (com.yandex.metrica.impl.ob.c.a) {
                            if (f2137g == null) {
                                f2137g = new Aa[0];
                            }
                        }
                    }
                    return f2137g;
                }

                public Aa() {
                    mo1871e();
                }

                /* renamed from: e */
                public Aa mo1871e() {
                    this.f2138b = "";
                    this.f2139c = "";
                    this.f2140d = null;
                    this.f2141e = null;
                    this.f2142f = null;
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    if (!this.f2138b.equals("")) {
                        bVar.mo351a(1, this.f2138b);
                    }
                    if (!this.f2139c.equals("")) {
                        bVar.mo351a(2, this.f2139c);
                    }
                    if (this.f2140d != null) {
                        bVar.mo350a(3, (e) this.f2140d);
                    }
                    if (this.f2141e != null) {
                        bVar.mo350a(4, (e) this.f2141e);
                    }
                    if (this.f2142f != null) {
                        bVar.mo350a(5, (e) this.f2142f);
                    }
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    int c = super.mo741c();
                    if (!this.f2138b.equals("")) {
                        c += com.yandex.metrica.impl.ob.b.m437b(1, this.f2138b);
                    }
                    if (!this.f2139c.equals("")) {
                        c += com.yandex.metrica.impl.ob.b.m437b(2, this.f2139c);
                    }
                    if (this.f2140d != null) {
                        c += com.yandex.metrica.impl.ob.b.b(3, (e) this.f2140d);
                    }
                    if (this.f2141e != null) {
                        c += com.yandex.metrica.impl.ob.b.b(4, (e) this.f2141e);
                    }
                    if (this.f2142f != null) {
                        return c + com.yandex.metrica.impl.ob.b.b(5, (e) this.f2142f);
                    }
                    return c;
                }

                /* renamed from: B */
                public Aa mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                this.f2138b = aVar.mo229i();
                                continue;
                            case 18:
                                this.f2139c = aVar.mo229i();
                                continue;
                            case 26:
                                if (this.f2140d == null) {
                                    this.f2140d = new C0867a();
                                }
                                aVar.mo215a((e) this.f2140d);
                                continue;
                            case 34:
                                if (this.f2141e == null) {
                                    this.f2141e = new C0868b();
                                }
                                aVar.mo215a((e) this.f2141e);
                                continue;
                            case 42:
                                if (this.f2142f == null) {
                                    this.f2142f = new C0869c();
                                }
                                aVar.mo215a((e) this.f2142f);
                                continue;
                            default:
                                if (!g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$B$B */
            public static final class B extends e {

                /* renamed from: B */
                public int b;

                /* renamed from: a */
                public int c;

                /* renamed from: d */
                public int d;

                /* renamed from: e */
                public int e;

                /* renamed from: f */
                public long f;

                public B() {
                    mo1879d();
                }

                /* renamed from: d */
                public B mo1879d() {
                    this.b = 1;
                    this.c = 2;
                    this.d = 3;
                    this.e = 1;
                    this.f = 300000;
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    if (this.b != 1) {
                        bVar.mo348a(1, this.b);
                    }
                    if (this.c != 2) {
                        bVar.mo348a(2, this.c);
                    }
                    if (this.d != 3) {
                        bVar.mo348a(3, this.d);
                    }
                    if (this.e != 1) {
                        bVar.mo348a(4, this.e);
                    }
                    if (this.f != 300000) {
                        bVar.b(5, this.f);
                    }
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    int c = super.mo741c();
                    if (this.b != 1) {
                        c += com.yandex.metrica.impl.ob.b.m445d(1, this.b);
                    }
                    if (this.c != 2) {
                        c += com.yandex.metrica.impl.ob.b.m445d(2, this.c);
                    }
                    if (this.d != 3) {
                        c += com.yandex.metrica.impl.ob.b.m445d(3, this.d);
                    }
                    if (this.e != 1) {
                        c += com.yandex.metrica.impl.ob.b.m445d(4, this.e);
                    }
                    if (this.f != 300000) {
                        return c + com.yandex.metrica.impl.ob.b.m450e(5, this.f);
                    }
                    return c;
                }

                /* renamed from: B */
                public B mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case 8:
                                int g = aVar.mo225g();
                                switch (g) {
                                    case 1:
                                    case 2:
                                    case 3:
                                        this.b = g;
                                        break;
                                    default:
                                        continue;
                                }
                            case 16:
                                int g2 = aVar.mo225g();
                                switch (g2) {
                                    case 1:
                                    case 2:
                                        this.c = g2;
                                        break;
                                    default:
                                        continue;
                                }
                            case 24:
                                int g3 = aVar.mo225g();
                                switch (g3) {
                                    case 1:
                                    case 2:
                                    case 3:
                                        this.d = g3;
                                        break;
                                    default:
                                        continue;
                                }
                            case 32:
                                int g4 = aVar.mo225g();
                                switch (g4) {
                                    case 1:
                                    case 2:
                                    case 3:
                                        this.e = g4;
                                        break;
                                    default:
                                        continue;
                                }
                            case 40:
                                this.f = aVar.mo223f();
                                continue;
                            default:
                                if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            public b() {
                mo1869d();
            }

            /* renamed from: d */
            public a.b mo1869d() {
                this.b = null;
                this.c = Aa.m3296d();
                this.d = 1200000;
                this.e = 259200000;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (this.b != null) {
                    bVar.mo350a(1, (e) this.b);
                }
                if (this.c != null && this.c.length > 0) {
                    for (Aa aVar : this.c) {
                        if (aVar != null) {
                            bVar.mo350a(2, (e) aVar);
                        }
                    }
                }
                bVar.b(3, this.d);
                bVar.b(4, this.e);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (this.b != null) {
                    c += com.yandex.metrica.impl.ob.b.b(1, (e) this.b);
                }
                if (this.c != null && this.c.length > 0) {
                    int i = c;
                    for (Aa aVar : this.c) {
                        if (aVar != null) {
                            i += com.yandex.metrica.impl.ob.b.b(2, (e) aVar);
                        }
                    }
                    c = i;
                }
                return c + com.yandex.metrica.impl.ob.b.m450e(3, this.d) + com.yandex.metrica.impl.ob.b.m450e(4, this.e);
            }

            /* renamed from: B */
            public a.b mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            if (this.b == null) {
                                this.b = new B();
                            }
                            aVar.mo215a((e) this.b);
                            continue;
                        case 18:
                            int b = g.b(aVar, 18);
                            if (this.c == null) {
                                length = 0;
                            } else {
                                length = this.c.length;
                            }
                            Aa[] aVarArr = new Aa[(b + length)];
                            if (length != 0) {
                                System.arraycopy(this.c, 0, aVarArr, 0, length);
                            }
                            while (length < aVarArr.length - 1) {
                                aVarArr[length] = new Aa();
                                aVar.mo215a((e) aVarArr[length]);
                                aVar.mo213a();
                                length++;
                            }
                            aVarArr[length] = new Aa();
                            aVar.mo215a((e) aVarArr[length]);
                            this.c = aVarArr;
                            continue;
                        case 24:
                            this.d = aVar.mo223f();
                            continue;
                        case 32:
                            this.e = aVar.mo223f();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$a */
        public static final class C0871c extends e {

            /* renamed from: B */
            public boolean f2156b;

            /* renamed from: a */
            public boolean f2157c;

            /* renamed from: d */
            public boolean f2158d;

            /* renamed from: e */
            public boolean f2159e;

            /* renamed from: f */
            public boolean f2160f;

            /* renamed from: g */
            public boolean f2161g;

            /* renamed from: h */
            public boolean f2162h;

            /* renamed from: i */
            public boolean f2163i;

            /* renamed from: j */
            public boolean f2164j;

            /* renamed from: k */
            public boolean f2165k;

            /* renamed from: l */
            public boolean f2166l;

            /* renamed from: m */
            public boolean f2167m;

            /* renamed from: n */
            public boolean f2168n;

            /* renamed from: o */
            public boolean f2169o;

            /* renamed from: p */
            public boolean f2170p;

            /* renamed from: q */
            public boolean f2171q;

            public C0871c() {
                mo1881d();
            }

            /* renamed from: d */
            public C0871c mo1881d() {
                this.f2156b = false;
                this.f2157c = false;
                this.f2158d = false;
                this.f2159e = false;
                this.f2160f = false;
                this.f2161g = false;
                this.f2162h = false;
                this.f2163i = false;
                this.f2164j = false;
                this.f2165k = false;
                this.f2166l = false;
                this.f2167m = false;
                this.f2168n = false;
                this.f2169o = false;
                this.f2170p = true;
                this.f2171q = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo352a(1, this.f2156b);
                bVar.mo352a(2, this.f2157c);
                bVar.mo352a(3, this.f2158d);
                bVar.mo352a(4, this.f2159e);
                bVar.mo352a(5, this.f2160f);
                bVar.mo352a(6, this.f2161g);
                bVar.mo352a(8, this.f2162h);
                bVar.mo352a(9, this.f2163i);
                bVar.mo352a(10, this.f2164j);
                bVar.mo352a(11, this.f2165k);
                bVar.mo352a(12, this.f2166l);
                bVar.mo352a(13, this.f2167m);
                bVar.mo352a(14, this.f2168n);
                bVar.mo352a(15, this.f2169o);
                bVar.mo352a(16, this.f2170p);
                bVar.mo352a(17, this.f2171q);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m438b(1, this.f2156b) + com.yandex.metrica.impl.ob.b.m438b(2, this.f2157c) + com.yandex.metrica.impl.ob.b.m438b(3, this.f2158d) + com.yandex.metrica.impl.ob.b.m438b(4, this.f2159e) + com.yandex.metrica.impl.ob.b.m438b(5, this.f2160f) + com.yandex.metrica.impl.ob.b.m438b(6, this.f2161g) + com.yandex.metrica.impl.ob.b.m438b(8, this.f2162h) + com.yandex.metrica.impl.ob.b.m438b(9, this.f2163i) + com.yandex.metrica.impl.ob.b.m438b(10, this.f2164j) + com.yandex.metrica.impl.ob.b.m438b(11, this.f2165k) + com.yandex.metrica.impl.ob.b.m438b(12, this.f2166l) + com.yandex.metrica.impl.ob.b.m438b(13, this.f2167m) + com.yandex.metrica.impl.ob.b.m438b(14, this.f2168n) + com.yandex.metrica.impl.ob.b.m438b(15, this.f2169o) + com.yandex.metrica.impl.ob.b.m438b(16, this.f2170p) + com.yandex.metrica.impl.ob.b.m438b(17, this.f2171q);
            }

            /* renamed from: B */
            public C0871c mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2156b = aVar.mo228h();
                            continue;
                        case 16:
                            this.f2157c = aVar.mo228h();
                            continue;
                        case 24:
                            this.f2158d = aVar.mo228h();
                            continue;
                        case 32:
                            this.f2159e = aVar.mo228h();
                            continue;
                        case 40:
                            this.f2160f = aVar.mo228h();
                            continue;
                        case 48:
                            this.f2161g = aVar.mo228h();
                            continue;
                        case 64:
                            this.f2162h = aVar.mo228h();
                            continue;
                        case 72:
                            this.f2163i = aVar.mo228h();
                            continue;
                        case 80:
                            this.f2164j = aVar.mo228h();
                            continue;
                        case 88:
                            this.f2165k = aVar.mo228h();
                            continue;
                        case 96:
                            this.f2166l = aVar.mo228h();
                            continue;
                        case 104:
                            this.f2167m = aVar.mo228h();
                            continue;
                        case 112:
                            this.f2168n = aVar.mo228h();
                            continue;
                        case 120:
                            this.f2169o = aVar.mo228h();
                            continue;
                        case 128:
                            this.f2170p = aVar.mo228h();
                            continue;
                        case 136:
                            this.f2171q = aVar.mo228h();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$d */
        public static final class C0872d extends e {

            /* renamed from: B */
            public long f2172b;

            public C0872d() {
                mo1883d();
            }

            /* renamed from: d */
            public C0872d mo1883d() {
                this.f2172b = 900;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.b(1, this.f2172b);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m450e(1, this.f2172b);
            }

            /* renamed from: B */
            public C0872d mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2172b = aVar.mo223f();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$e */
        public static final class C0873e extends e {

            /* renamed from: B */
            public long f2173b;

            /* renamed from: a */
            public float f2174c;

            /* renamed from: d */
            public int f2175d;

            /* renamed from: e */
            public int f2176e;

            /* renamed from: f */
            public long f2177f;

            /* renamed from: g */
            public int f2178g;

            /* renamed from: h */
            public boolean f2179h;

            /* renamed from: i */
            public boolean f2180i;

            /* renamed from: j */
            public long f2181j;

            public C0873e() {
                mo1885d();
            }

            /* renamed from: d */
            public C0873e mo1885d() {
                this.f2173b = 5000;
                this.f2174c = 10.0f;
                this.f2175d = 20;
                this.f2176e = 200;
                this.f2177f = 60000;
                this.f2178g = YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT_UPPER_BOUND;
                this.f2179h = false;
                this.f2180i = false;
                this.f2181j = 60000;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.b(1, this.f2173b);
                bVar.mo347a(2, this.f2174c);
                bVar.mo348a(3, this.f2175d);
                bVar.mo348a(4, this.f2176e);
                bVar.b(5, this.f2177f);
                bVar.mo348a(6, this.f2178g);
                bVar.mo352a(7, this.f2179h);
                bVar.mo352a(8, this.f2180i);
                bVar.b(9, this.f2181j);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m450e(1, this.f2173b) + com.yandex.metrica.impl.ob.b.m435b(2, this.f2174c) + com.yandex.metrica.impl.ob.b.m445d(3, this.f2175d) + com.yandex.metrica.impl.ob.b.m445d(4, this.f2176e) + com.yandex.metrica.impl.ob.b.m450e(5, this.f2177f) + com.yandex.metrica.impl.ob.b.m445d(6, this.f2178g) + com.yandex.metrica.impl.ob.b.m438b(7, this.f2179h) + com.yandex.metrica.impl.ob.b.m438b(8, this.f2180i) + com.yandex.metrica.impl.ob.b.m450e(9, this.f2181j);
            }

            /* renamed from: B */
            public C0873e mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2173b = aVar.mo223f();
                            continue;
                        case 21:
                            this.f2174c = aVar.mo219d();
                            continue;
                        case 24:
                            this.f2175d = aVar.mo225g();
                            continue;
                        case 32:
                            this.f2176e = aVar.mo225g();
                            continue;
                        case 40:
                            this.f2177f = aVar.mo223f();
                            continue;
                        case 48:
                            this.f2178g = aVar.mo225g();
                            continue;
                        case 56:
                            this.f2179h = aVar.mo228h();
                            continue;
                        case 64:
                            this.f2180i = aVar.mo228h();
                            continue;
                        case 72:
                            this.f2181j = aVar.mo223f();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$f */
        public static final class C0874f extends e {

            /* renamed from: d */
            private static volatile C0874f[] f2182d;

            /* renamed from: B */
            public String f2183b;

            /* renamed from: a */
            public boolean f2184c;

            /* renamed from: d */
            public static C0874f[] m3337d() {
                if (f2182d == null) {
                    synchronized (c.a) {
                        if (f2182d == null) {
                            f2182d = new C0874f[0];
                        }
                    }
                }
                return f2182d;
            }

            public C0874f() {
                mo1887e();
            }

            /* renamed from: e */
            public C0874f mo1887e() {
                this.f2183b = "";
                this.f2184c = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo351a(1, this.f2183b);
                bVar.mo352a(2, this.f2184c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2183b) + com.yandex.metrica.impl.ob.b.m438b(2, this.f2184c);
            }

            /* renamed from: B */
            public C0874f mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2183b = aVar.mo229i();
                            continue;
                        case 16:
                            this.f2184c = aVar.mo228h();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$g */
        public static final class C0875g extends e {

            /* renamed from: B */
            public long f2185b;

            /* renamed from: a */
            public long f2186c;

            public C0875g() {
                mo1889d();
            }

            /* renamed from: d */
            public C0875g mo1889d() {
                this.f2185b = 86400;
                this.f2186c = 432000;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.b(1, this.f2185b);
                bVar.b(2, this.f2186c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m450e(1, this.f2185b) + com.yandex.metrica.impl.ob.b.m450e(2, this.f2186c);
            }

            /* renamed from: B */
            public C0875g mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2185b = aVar.mo223f();
                            continue;
                        case 16:
                            this.f2186c = aVar.mo223f();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$h */
        public static final class C0876h extends e {

            /* renamed from: h */
            private static volatile C0876h[] f2187h;

            /* renamed from: B */
            public String f2188b;

            /* renamed from: a */
            public String f2189c;

            /* renamed from: d */
            public String f2190d;

            /* renamed from: e */
            public C0877a[] f2191e;

            /* renamed from: f */
            public long f2192f;

            /* renamed from: g */
            public int[] f2193g;

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$h$a */
            public static final class C0877a extends e {

                /* renamed from: d */
                private static volatile C0877a[] f2194d;

                /* renamed from: B */
                public String f2195b;

                /* renamed from: a */
                public String f2196c;

                /* renamed from: d */
                public static C0877a[] m3354d() {
                    if (f2194d == null) {
                        synchronized (c.a) {
                            if (f2194d == null) {
                                f2194d = new C0877a[0];
                            }
                        }
                    }
                    return f2194d;
                }

                public C0877a() {
                    mo1893e();
                }

                /* renamed from: e */
                public C0877a mo1893e() {
                    this.f2195b = "";
                    this.f2196c = "";
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    bVar.mo351a(1, this.f2195b);
                    bVar.mo351a(2, this.f2196c);
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
                    return super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2195b) + com.yandex.metrica.impl.ob.b.m437b(2, this.f2196c);
                }

                /* renamed from: B */
                public C0877a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                this.f2195b = aVar.mo229i();
                                continue;
                            case 18:
                                this.f2196c = aVar.mo229i();
                                continue;
                            default:
                                if (!g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            /* renamed from: d */
            public static C0876h[] m3348d() {
                if (f2187h == null) {
                    synchronized (c.a) {
                        if (f2187h == null) {
                            f2187h = new C0876h[0];
                        }
                    }
                }
                return f2187h;
            }

            public C0876h() {
                mo1891e();
            }

            /* renamed from: e */
            public C0876h mo1891e() {
                this.f2188b = "";
                this.f2189c = "";
                this.f2190d = "";
                this.f2191e = C0877a.m3354d();
                this.f2192f = 0;
                this.f2193g = g.a;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo351a(1, this.f2188b);
                bVar.mo351a(2, this.f2189c);
                bVar.mo351a(3, this.f2190d);
                if (this.f2191e != null && this.f2191e.length > 0) {
                    for (C0877a aVar : this.f2191e) {
                        if (aVar != null) {
                            bVar.mo350a(4, (e) aVar);
                        }
                    }
                }
                bVar.b(5, this.f2192f);
                if (this.f2193g != null && this.f2193g.length > 0) {
                    for (int a : this.f2193g) {
                        bVar.mo348a(6, a);
                    }
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int b = com.yandex.metrica.impl.ob.b.m437b(3, this.f2190d) + super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.f2188b) + com.yandex.metrica.impl.ob.b.m437b(2, this.f2189c);
                if (this.f2191e != null && this.f2191e.length > 0) {
                    for (C0877a aVar : this.f2191e) {
                        if (aVar != null) {
                            b += com.yandex.metrica.impl.ob.b.b(4, (e) aVar);
                        }
                    }
                }
                int e = b + com.yandex.metrica.impl.ob.b.m450e(5, this.f2192f);
                if (this.f2193g == null || this.f2193g.length <= 0) {
                    return e;
                }
                int i = 0;
                for (int d : this.f2193g) {
                    i += com.yandex.metrica.impl.ob.b.m444d(d);
                }
                return e + i + (this.f2193g.length * 1);
            }

            /* renamed from: B */
            public C0876h mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                int i;
                int length2;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2188b = aVar.mo229i();
                            continue;
                        case 18:
                            this.f2189c = aVar.mo229i();
                            continue;
                        case 26:
                            this.f2190d = aVar.mo229i();
                            continue;
                        case 34:
                            int b = g.b(aVar, 34);
                            if (this.f2191e == null) {
                                length2 = 0;
                            } else {
                                length2 = this.f2191e.length;
                            }
                            C0877a[] aVarArr = new C0877a[(b + length2)];
                            if (length2 != 0) {
                                System.arraycopy(this.f2191e, 0, aVarArr, 0, length2);
                            }
                            while (length2 < aVarArr.length - 1) {
                                aVarArr[length2] = new C0877a();
                                aVar.mo215a((e) aVarArr[length2]);
                                aVar.mo213a();
                                length2++;
                            }
                            aVarArr[length2] = new C0877a();
                            aVar.mo215a((e) aVarArr[length2]);
                            this.f2191e = aVarArr;
                            continue;
                        case 40:
                            this.f2192f = aVar.mo223f();
                            continue;
                        case 48:
                            int b2 = g.b(aVar, 48);
                            int[] iArr = new int[b2];
                            int i2 = 0;
                            int i3 = 0;
                            while (i2 < b2) {
                                if (i2 != 0) {
                                    aVar.mo213a();
                                }
                                int g = aVar.mo225g();
                                switch (g) {
                                    case 1:
                                    case 2:
                                        i = i3 + 1;
                                        iArr[i3] = g;
                                        break;
                                    default:
                                        i = i3;
                                        break;
                                }
                                i2++;
                                i3 = i;
                            }
                            if (i3 != 0) {
                                int length3 = this.f2193g == null ? 0 : this.f2193g.length;
                                if (length3 != 0 || i3 != iArr.length) {
                                    int[] iArr2 = new int[(length3 + i3)];
                                    if (length3 != 0) {
                                        System.arraycopy(this.f2193g, 0, iArr2, 0, length3);
                                    }
                                    System.arraycopy(iArr, 0, iArr2, length3, i3);
                                    this.f2193g = iArr2;
                                    break;
                                } else {
                                    this.f2193g = iArr;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        case 50:
                            int d = aVar.d(aVar.mo234n());
                            int t = aVar.mo240t();
                            int i4 = 0;
                            while (aVar.mo238r() > 0) {
                                switch (aVar.mo225g()) {
                                    case 1:
                                    case 2:
                                        i4++;
                                        break;
                                }
                            }
                            if (i4 != 0) {
                                aVar.mo224f(t);
                                if (this.f2193g == null) {
                                    length = 0;
                                } else {
                                    length = this.f2193g.length;
                                }
                                int[] iArr3 = new int[(i4 + length)];
                                if (length != 0) {
                                    System.arraycopy(this.f2193g, 0, iArr3, 0, length);
                                }
                                while (aVar.mo238r() > 0) {
                                    int g2 = aVar.mo225g();
                                    switch (g2) {
                                        case 1:
                                        case 2:
                                            int i5 = length + 1;
                                            iArr3[length] = g2;
                                            length = i5;
                                            break;
                                    }
                                }
                                this.f2193g = iArr3;
                            }
                            aVar.mo222e(d);
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$i */
        public static final class C0878i extends e {

            /* renamed from: B */
            public long f2197b;

            /* renamed from: a */
            public long f2198c;

            /* renamed from: d */
            public long f2199d;

            /* renamed from: e */
            public long f2200e;

            public C0878i() {
                mo1895d();
            }

            /* renamed from: d */
            public C0878i mo1895d() {
                this.f2197b = 432000000;
                this.f2198c = 86400000;
                this.f2199d = 10000;
                this.f2200e = 3600000;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (this.f2197b != 432000000) {
                    bVar.b(1, this.f2197b);
                }
                if (this.f2198c != 86400000) {
                    bVar.b(2, this.f2198c);
                }
                if (this.f2199d != 10000) {
                    bVar.b(3, this.f2199d);
                }
                if (this.f2200e != 3600000) {
                    bVar.b(4, this.f2200e);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (this.f2197b != 432000000) {
                    c += com.yandex.metrica.impl.ob.b.m450e(1, this.f2197b);
                }
                if (this.f2198c != 86400000) {
                    c += com.yandex.metrica.impl.ob.b.m450e(2, this.f2198c);
                }
                if (this.f2199d != 10000) {
                    c += com.yandex.metrica.impl.ob.b.m450e(3, this.f2199d);
                }
                if (this.f2200e != 3600000) {
                    return c + com.yandex.metrica.impl.ob.b.m450e(4, this.f2200e);
                }
                return c;
            }

            /* renamed from: B */
            public C0878i mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2197b = aVar.mo223f();
                            continue;
                        case 16:
                            this.f2198c = aVar.mo223f();
                            continue;
                        case 24:
                            this.f2199d = aVar.mo223f();
                            continue;
                        case 32:
                            this.f2200e = aVar.mo223f();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$j */
        public static final class C0879j extends e {

            /* renamed from: B */
            public long f2201b;

            /* renamed from: a */
            public String f2202c;

            /* renamed from: d */
            public int[] f2203d;

            /* renamed from: e */
            public long f2204e;

            /* renamed from: f */
            public int f2205f;

            public C0879j() {
                mo1897d();
            }

            /* renamed from: d */
            public C0879j mo1897d() {
                this.f2201b = 0;
                this.f2202c = "";
                this.f2203d = g.a;
                this.f2204e = 259200;
                this.f2205f = 10;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.b(1, this.f2201b);
                bVar.mo351a(2, this.f2202c);
                if (this.f2203d != null && this.f2203d.length > 0) {
                    for (int a : this.f2203d) {
                        bVar.mo348a(3, a);
                    }
                }
                bVar.b(4, this.f2204e);
                bVar.mo348a(5, this.f2205f);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int i;
                int i2;
                int i3 = 0;
                int c = super.mo741c() + com.yandex.metrica.impl.ob.b.m450e(1, this.f2201b) + com.yandex.metrica.impl.ob.b.m437b(2, this.f2202c);
                if (this.f2203d == null || this.f2203d.length <= 0) {
                    i = c;
                } else {
                    int i4 = 0;
                    while (true) {
                        i2 = i3;
                        if (i4 >= this.f2203d.length) {
                            break;
                        }
                        i3 = com.yandex.metrica.impl.ob.b.m444d(this.f2203d[i4]) + i2;
                        i4++;
                    }
                    i = c + i2 + (this.f2203d.length * 1);
                }
                return i + com.yandex.metrica.impl.ob.b.m450e(4, this.f2204e) + com.yandex.metrica.impl.ob.b.m445d(5, this.f2205f);
            }

            /* renamed from: B */
            public C0879j mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2201b = aVar.mo223f();
                            continue;
                        case 18:
                            this.f2202c = aVar.mo229i();
                            continue;
                        case 24:
                            int b = g.b(aVar, 24);
                            int length = this.f2203d == null ? 0 : this.f2203d.length;
                            int[] iArr = new int[(b + length)];
                            if (length != 0) {
                                System.arraycopy(this.f2203d, 0, iArr, 0, length);
                            }
                            while (length < iArr.length - 1) {
                                iArr[length] = aVar.mo225g();
                                aVar.mo213a();
                                length++;
                            }
                            iArr[length] = aVar.mo225g();
                            this.f2203d = iArr;
                            continue;
                        case 26:
                            int d = aVar.d(aVar.mo234n());
                            int t = aVar.mo240t();
                            int i = 0;
                            while (aVar.mo238r() > 0) {
                                aVar.mo225g();
                                i++;
                            }
                            aVar.mo224f(t);
                            int length2 = this.f2203d == null ? 0 : this.f2203d.length;
                            int[] iArr2 = new int[(i + length2)];
                            if (length2 != 0) {
                                System.arraycopy(this.f2203d, 0, iArr2, 0, length2);
                            }
                            while (length2 < iArr2.length) {
                                iArr2[length2] = aVar.mo225g();
                                length2++;
                            }
                            this.f2203d = iArr2;
                            aVar.mo222e(d);
                            continue;
                        case 32:
                            this.f2204e = aVar.mo223f();
                            continue;
                        case 40:
                            this.f2205f = aVar.mo225g();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$k */
        public static final class C0880k extends e {

            /* renamed from: B */
            public long f2206b;

            public C0880k() {
                mo1899d();
            }

            /* renamed from: d */
            public C0880k mo1899d() {
                this.f2206b = 18000000;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.b(1, this.f2206b);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + com.yandex.metrica.impl.ob.b.m450e(1, this.f2206b);
            }

            /* renamed from: B */
            public C0880k mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2206b = aVar.mo223f();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public a() {
            mo1863d();
        }

        /* renamed from: d */
        public a mo1863d() {
            this.f2100b = "";
            this.f2101c = 0;
            this.f2102d = g.f;
            this.f2103e = "";
            this.f2104f = "";
            this.f2105g = g.f;
            this.f2106h = g.f;
            this.f2107i = g.f;
            this.f2108j = null;
            this.f2109k = null;
            this.f2110l = null;
            this.f2111m = null;
            this.f2112n = C0876h.m3348d();
            this.f2113o = "";
            this.f2114p = "";
            this.f2115q = false;
            this.f2116r = "";
            this.f2117s = "";
            this.f2118t = g.f;
            this.f2119u = null;
            this.f2120v = false;
            this.f2121w = C0874f.m3337d();
            this.f2122x = null;
            this.f2123y = "";
            this.f2124z = "";
            this.f2092A = "";
            this.f2093B = "";
            this.f2094C = 0;
            this.f2095D = 0;
            this.f2096E = false;
            this.f2097F = null;
            this.f2098G = null;
            this.f2099H = null;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            if (!this.f2100b.equals("")) {
                bVar.mo351a(1, this.f2100b);
            }
            bVar.b(3, this.f2101c);
            if (this.f2102d != null && this.f2102d.length > 0) {
                for (String str : this.f2102d) {
                    if (str != null) {
                        bVar.mo351a(4, str);
                    }
                }
            }
            if (!this.f2103e.equals("")) {
                bVar.mo351a(5, this.f2103e);
            }
            if (!this.f2104f.equals("")) {
                bVar.mo351a(6, this.f2104f);
            }
            if (this.f2105g != null && this.f2105g.length > 0) {
                for (String str2 : this.f2105g) {
                    if (str2 != null) {
                        bVar.mo351a(7, str2);
                    }
                }
            }
            if (this.f2106h != null && this.f2106h.length > 0) {
                for (String str3 : this.f2106h) {
                    if (str3 != null) {
                        bVar.mo351a(8, str3);
                    }
                }
            }
            if (this.f2107i != null && this.f2107i.length > 0) {
                for (String str4 : this.f2107i) {
                    if (str4 != null) {
                        bVar.mo351a(9, str4);
                    }
                }
            }
            if (this.f2108j != null) {
                bVar.mo350a(10, (e) this.f2108j);
            }
            if (this.f2109k != null) {
                bVar.mo350a(11, (e) this.f2109k);
            }
            if (this.f2110l != null) {
                bVar.mo350a(12, (e) this.f2110l);
            }
            if (this.f2111m != null) {
                bVar.mo350a(13, (e) this.f2111m);
            }
            if (this.f2112n != null && this.f2112n.length > 0) {
                for (C0876h hVar : this.f2112n) {
                    if (hVar != null) {
                        bVar.mo350a(14, (e) hVar);
                    }
                }
            }
            if (!this.f2113o.equals("")) {
                bVar.mo351a(15, this.f2113o);
            }
            if (!this.f2114p.equals("")) {
                bVar.mo351a(16, this.f2114p);
            }
            bVar.mo352a(17, this.f2115q);
            if (!this.f2116r.equals("")) {
                bVar.mo351a(18, this.f2116r);
            }
            if (!this.f2117s.equals("")) {
                bVar.mo351a(19, this.f2117s);
            }
            if (this.f2118t != null && this.f2118t.length > 0) {
                for (String str5 : this.f2118t) {
                    if (str5 != null) {
                        bVar.mo351a(20, str5);
                    }
                }
            }
            if (this.f2119u != null) {
                bVar.mo350a(21, (e) this.f2119u);
            }
            if (this.f2120v) {
                bVar.mo352a(22, this.f2120v);
            }
            if (this.f2121w != null && this.f2121w.length > 0) {
                for (C0874f fVar : this.f2121w) {
                    if (fVar != null) {
                        bVar.mo350a(23, (e) fVar);
                    }
                }
            }
            if (this.f2122x != null) {
                bVar.mo350a(24, (e) this.f2122x);
            }
            if (!this.f2123y.equals("")) {
                bVar.mo351a(25, this.f2123y);
            }
            if (!this.f2124z.equals("")) {
                bVar.mo351a(26, this.f2124z);
            }
            if (!this.f2093B.equals("")) {
                bVar.mo351a(27, this.f2093B);
            }
            bVar.b(28, this.f2094C);
            bVar.b(29, this.f2095D);
            if (this.f2096E) {
                bVar.mo352a(30, this.f2096E);
            }
            if (!this.f2092A.equals("")) {
                bVar.mo351a(31, this.f2092A);
            }
            if (this.f2097F != null) {
                bVar.mo350a(32, (e) this.f2097F);
            }
            if (this.f2098G != null) {
                bVar.mo350a(33, (e) this.f2098G);
            }
            if (this.f2099H != null) {
                bVar.mo350a(34, (e) this.f2099H);
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int i;
            int c = super.mo741c();
            if (!this.f2100b.equals("")) {
                c += com.yandex.metrica.impl.ob.b.m437b(1, this.f2100b);
            }
            int e = c + com.yandex.metrica.impl.ob.b.m450e(3, this.f2101c);
            if (this.f2102d == null || this.f2102d.length <= 0) {
                i = e;
            } else {
                int i2 = 0;
                int i3 = 0;
                for (String str : this.f2102d) {
                    if (str != null) {
                        i3++;
                        i2 += com.yandex.metrica.impl.ob.b.m441b(str);
                    }
                }
                i = e + i2 + (i3 * 1);
            }
            if (!this.f2103e.equals("")) {
                i += com.yandex.metrica.impl.ob.b.m437b(5, this.f2103e);
            }
            if (!this.f2104f.equals("")) {
                i += com.yandex.metrica.impl.ob.b.m437b(6, this.f2104f);
            }
            if (this.f2105g != null && this.f2105g.length > 0) {
                int i4 = 0;
                int i5 = 0;
                for (String str2 : this.f2105g) {
                    if (str2 != null) {
                        i5++;
                        i4 += com.yandex.metrica.impl.ob.b.m441b(str2);
                    }
                }
                i = i + i4 + (i5 * 1);
            }
            if (this.f2106h != null && this.f2106h.length > 0) {
                int i6 = 0;
                int i7 = 0;
                for (String str3 : this.f2106h) {
                    if (str3 != null) {
                        i7++;
                        i6 += com.yandex.metrica.impl.ob.b.m441b(str3);
                    }
                }
                i = i + i6 + (i7 * 1);
            }
            if (this.f2107i != null && this.f2107i.length > 0) {
                int i8 = 0;
                int i9 = 0;
                for (String str4 : this.f2107i) {
                    if (str4 != null) {
                        i9++;
                        i8 += com.yandex.metrica.impl.ob.b.m441b(str4);
                    }
                }
                i = i + i8 + (i9 * 1);
            }
            if (this.f2108j != null) {
                i += com.yandex.metrica.impl.ob.b.b(10, (com.yandex.metrica.impl.ob.e) this.f2108j);
            }
            if (this.f2109k != null) {
                i += com.yandex.metrica.impl.ob.b.b(11, (com.yandex.metrica.impl.ob.e) this.f2109k);
            }
            if (this.f2110l != null) {
                i += com.yandex.metrica.impl.ob.b.b(12, (com.yandex.metrica.impl.ob.e) this.f2110l);
            }
            if (this.f2111m != null) {
                i += com.yandex.metrica.impl.ob.b.b(13, (com.yandex.metrica.impl.ob.e) this.f2111m);
            }
            if (this.f2112n != null && this.f2112n.length > 0) {
                int i10 = i;
                for (C0876h hVar : this.f2112n) {
                    if (hVar != null) {
                        i10 += com.yandex.metrica.impl.ob.b.b(14, (com.yandex.metrica.impl.ob.e) hVar);
                    }
                }
                i = i10;
            }
            if (!this.f2113o.equals("")) {
                i += com.yandex.metrica.impl.ob.b.m437b(15, this.f2113o);
            }
            if (!this.f2114p.equals("")) {
                i += com.yandex.metrica.impl.ob.b.m437b(16, this.f2114p);
            }
            int b = i + com.yandex.metrica.impl.ob.b.m438b(17, this.f2115q);
            if (!this.f2116r.equals("")) {
                b += com.yandex.metrica.impl.ob.b.m437b(18, this.f2116r);
            }
            if (!this.f2117s.equals("")) {
                b += com.yandex.metrica.impl.ob.b.m437b(19, this.f2117s);
            }
            if (this.f2118t != null && this.f2118t.length > 0) {
                int i11 = 0;
                int i12 = 0;
                for (String str5 : this.f2118t) {
                    if (str5 != null) {
                        i12++;
                        i11 += com.yandex.metrica.impl.ob.b.m441b(str5);
                    }
                }
                b = b + i11 + (i12 * 2);
            }
            if (this.f2119u != null) {
                b += com.yandex.metrica.impl.ob.b.b(21, (com.yandex.metrica.impl.ob.e) this.f2119u);
            }
            if (this.f2120v) {
                b += com.yandex.metrica.impl.ob.b.m438b(22, this.f2120v);
            }
            if (this.f2121w != null && this.f2121w.length > 0) {
                for (C0874f fVar : this.f2121w) {
                    if (fVar != null) {
                        b += com.yandex.metrica.impl.ob.b.b(23, (com.yandex.metrica.impl.ob.e) fVar);
                    }
                }
            }
            if (this.f2122x != null) {
                b += com.yandex.metrica.impl.ob.b.b(24, (com.yandex.metrica.impl.ob.e) this.f2122x);
            }
            if (!this.f2123y.equals("")) {
                b += com.yandex.metrica.impl.ob.b.m437b(25, this.f2123y);
            }
            if (!this.f2124z.equals("")) {
                b += com.yandex.metrica.impl.ob.b.m437b(26, this.f2124z);
            }
            if (!this.f2093B.equals("")) {
                b += com.yandex.metrica.impl.ob.b.m437b(27, this.f2093B);
            }
            int e2 = b + com.yandex.metrica.impl.ob.b.m450e(28, this.f2094C) + com.yandex.metrica.impl.ob.b.m450e(29, this.f2095D);
            if (this.f2096E) {
                e2 += com.yandex.metrica.impl.ob.b.m438b(30, this.f2096E);
            }
            if (!this.f2092A.equals("")) {
                e2 += com.yandex.metrica.impl.ob.b.m437b(31, this.f2092A);
            }
            if (this.f2097F != null) {
                e2 += com.yandex.metrica.impl.ob.b.b(32, (com.yandex.metrica.impl.ob.e) this.f2097F);
            }
            if (this.f2098G != null) {
                e2 += com.yandex.metrica.impl.ob.b.b(33, (com.yandex.metrica.impl.ob.e) this.f2098G);
            }
            if (this.f2099H != null) {
                return e2 + com.yandex.metrica.impl.ob.b.b(34, (com.yandex.metrica.impl.ob.e) this.f2099H);
            }
            return e2;
        }

        /* renamed from: B */
        public a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            int length;
            int length2;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        this.f2100b = aVar.mo229i();
                        continue;
                    case 24:
                        this.f2101c = aVar.mo223f();
                        continue;
                    case 34:
                        int b = g.b(aVar, 34);
                        int length3 = this.f2102d == null ? 0 : this.f2102d.length;
                        String[] strArr = new String[(b + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.f2102d, 0, strArr, 0, length3);
                        }
                        while (length3 < strArr.length - 1) {
                            strArr[length3] = aVar.mo229i();
                            aVar.mo213a();
                            length3++;
                        }
                        strArr[length3] = aVar.mo229i();
                        this.f2102d = strArr;
                        continue;
                    case 42:
                        this.f2103e = aVar.mo229i();
                        continue;
                    case 50:
                        this.f2104f = aVar.mo229i();
                        continue;
                    case 58:
                        int b2 = g.b(aVar, 58);
                        int length4 = this.f2105g == null ? 0 : this.f2105g.length;
                        String[] strArr2 = new String[(b2 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.f2105g, 0, strArr2, 0, length4);
                        }
                        while (length4 < strArr2.length - 1) {
                            strArr2[length4] = aVar.mo229i();
                            aVar.mo213a();
                            length4++;
                        }
                        strArr2[length4] = aVar.mo229i();
                        this.f2105g = strArr2;
                        continue;
                    case 66:
                        int b3 = g.b(aVar, 66);
                        int length5 = this.f2106h == null ? 0 : this.f2106h.length;
                        String[] strArr3 = new String[(b3 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.f2106h, 0, strArr3, 0, length5);
                        }
                        while (length5 < strArr3.length - 1) {
                            strArr3[length5] = aVar.mo229i();
                            aVar.mo213a();
                            length5++;
                        }
                        strArr3[length5] = aVar.mo229i();
                        this.f2106h = strArr3;
                        continue;
                    case 74:
                        int b4 = g.b(aVar, 74);
                        int length6 = this.f2107i == null ? 0 : this.f2107i.length;
                        String[] strArr4 = new String[(b4 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.f2107i, 0, strArr4, 0, length6);
                        }
                        while (length6 < strArr4.length - 1) {
                            strArr4[length6] = aVar.mo229i();
                            aVar.mo213a();
                            length6++;
                        }
                        strArr4[length6] = aVar.mo229i();
                        this.f2107i = strArr4;
                        continue;
                    case 82:
                        if (this.f2108j == null) {
                            this.f2108j = new C0871c();
                        }
                        aVar.mo215a((e) this.f2108j);
                        continue;
                    case YandexMetricaDefaultValues.DEFAULT_DISPATCH_PERIOD_SECONDS /*90*/:
                        if (this.f2109k == null) {
                            this.f2109k = new C0873e();
                        }
                        aVar.mo215a((e) this.f2109k);
                        continue;
                    case 98:
                        if (this.f2110l == null) {
                            this.f2110l = new C0863a();
                        }
                        aVar.mo215a((e) this.f2110l);
                        continue;
                    case 106:
                        if (this.f2111m == null) {
                            this.f2111m = new C0879j();
                        }
                        aVar.mo215a((e) this.f2111m);
                        continue;
                    case 114:
                        int b5 = g.b(aVar, 114);
                        if (this.f2112n == null) {
                            length2 = 0;
                        } else {
                            length2 = this.f2112n.length;
                        }
                        C0876h[] hVarArr = new C0876h[(b5 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.f2112n, 0, hVarArr, 0, length2);
                        }
                        while (length2 < hVarArr.length - 1) {
                            hVarArr[length2] = new C0876h();
                            aVar.mo215a((e) hVarArr[length2]);
                            aVar.mo213a();
                            length2++;
                        }
                        hVarArr[length2] = new C0876h();
                        aVar.mo215a((e) hVarArr[length2]);
                        this.f2112n = hVarArr;
                        continue;
                    case 122:
                        this.f2113o = aVar.mo229i();
                        continue;
                    case 130:
                        this.f2114p = aVar.mo229i();
                        continue;
                    case 136:
                        this.f2115q = aVar.mo228h();
                        continue;
                    case 146:
                        this.f2116r = aVar.mo229i();
                        continue;
                    case 154:
                        this.f2117s = aVar.mo229i();
                        continue;
                    case 162:
                        int b6 = g.b(aVar, 162);
                        int length7 = this.f2118t == null ? 0 : this.f2118t.length;
                        String[] strArr5 = new String[(b6 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.f2118t, 0, strArr5, 0, length7);
                        }
                        while (length7 < strArr5.length - 1) {
                            strArr5[length7] = aVar.mo229i();
                            aVar.mo213a();
                            length7++;
                        }
                        strArr5[length7] = aVar.mo229i();
                        this.f2118t = strArr5;
                        continue;
                    case 170:
                        if (this.f2119u == null) {
                            this.f2119u = new C0880k();
                        }
                        aVar.mo215a((e) this.f2119u);
                        continue;
                    case 176:
                        this.f2120v = aVar.mo228h();
                        continue;
                    case 186:
                        int b7 = g.b(aVar, 186);
                        if (this.f2121w == null) {
                            length = 0;
                        } else {
                            length = this.f2121w.length;
                        }
                        C0874f[] fVarArr = new C0874f[(b7 + length)];
                        if (length != 0) {
                            System.arraycopy(this.f2121w, 0, fVarArr, 0, length);
                        }
                        while (length < fVarArr.length - 1) {
                            fVarArr[length] = new C0874f();
                            aVar.mo215a((e) fVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        fVarArr[length] = new C0874f();
                        aVar.mo215a((e) fVarArr[length]);
                        this.f2121w = fVarArr;
                        continue;
                    case 194:
                        if (this.f2122x == null) {
                            this.f2122x = new C0878i();
                        }
                        aVar.mo215a((e) this.f2122x);
                        continue;
                    case 202:
                        this.f2123y = aVar.mo229i();
                        continue;
                    case 210:
                        this.f2124z = aVar.mo229i();
                        continue;
                    case 218:
                        this.f2093B = aVar.mo229i();
                        continue;
                    case 224:
                        this.f2094C = aVar.mo223f();
                        continue;
                    case 232:
                        this.f2095D = aVar.mo223f();
                        continue;
                    case 240:
                        this.f2096E = aVar.mo228h();
                        continue;
                    case 250:
                        this.f2092A = aVar.mo229i();
                        continue;
                    case 258:
                        if (this.f2097F == null) {
                            this.f2097F = new C0875g();
                        }
                        aVar.mo215a((e) this.f2097F);
                        continue;
                    case 266:
                        if (this.f2098G == null) {
                            this.f2098G = new C0872d();
                        }
                        aVar.mo215a((e) this.f2098G);
                        continue;
                    case 274:
                        if (this.f2099H == null) {
                            this.f2099H = new b();
                        }
                        aVar.mo215a((e) this.f2099H);
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }

        /* renamed from: a */
        public static a m3274a(byte[] bArr) throws d {
            return (a) e.m1393a(new a(), bArr);
        }
    }
}
