package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.pq */
class pq implements pp {

    /* renamed from: a */
    private final boolean f1741a;

    pq(boolean z) {
        this.f1741a = z;
    }

    /* renamed from: a */
    public boolean a(@NonNull String str) {
        if ("android.permission.ACCESS_FINE_LOCATION".equals(str) || "android.permission.ACCESS_COARSE_LOCATION".equals(str)) {
            return this.f1741a;
        }
        return true;
    }

    public String toString() {
        return "LocationFlagStrategy{mEnabled=" + this.f1741a + '}';
    }
}
