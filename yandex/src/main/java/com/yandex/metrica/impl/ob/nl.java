package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.tt.b.a_enum;
import com.yandex.metrica.impl.ob.tt.b.b_enum;
import com.yandex.metrica.impl.ob.tt.b.c_enum;
import com.yandex.metrica.impl.ob.tt.b.d_enum;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"UseSparseArrays"})
/* renamed from: com.yandex.metrica.impl.ob.nl */
public class nl implements mv<tt.b, rr.a.b.B> {

    /* renamed from: a */
    private static final Map<Integer, a_enum> a;

    /* renamed from: B */
    private static final Map<a_enum, Integer> b;

    /* renamed from: a */
    private static final Map<Integer, b_enum> c;

    /* renamed from: d */
    private static final Map<b_enum, Integer> d;

    /* renamed from: e */
    private static final Map<Integer, c_enum> e;

    /* renamed from: f */
    private static final Map<c_enum, Integer> f;

    /* renamed from: g */
    private static final Map<Integer, d_enum> g;

    /* renamed from: h */
    private static final Map<d_enum, Integer> h;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(Integer.valueOf(1), a_enum.ALL_MATCHES);
        hashMap.put(Integer.valueOf(2), a_enum.FIRST_MATCH);
        hashMap.put(Integer.valueOf(3), a_enum.MATCH_LOST);
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(a_enum.ALL_MATCHES, Integer.valueOf(1));
        hashMap2.put(a_enum.FIRST_MATCH, Integer.valueOf(2));
        hashMap2.put(a_enum.MATCH_LOST, Integer.valueOf(3));
        b = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put(Integer.valueOf(1), b_enum.AGGRESSIVE);
        hashMap3.put(Integer.valueOf(2), b_enum.STICKY);
        c = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put(b_enum.AGGRESSIVE, Integer.valueOf(1));
        hashMap4.put(b_enum.STICKY, Integer.valueOf(2));
        d = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put(Integer.valueOf(1), c_enum.ONE_AD);
        hashMap5.put(Integer.valueOf(2), c_enum.FEW_AD);
        hashMap5.put(Integer.valueOf(3), c_enum.MAX_AD);
        e = Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        hashMap6.put(c_enum.ONE_AD, Integer.valueOf(1));
        hashMap6.put(c_enum.FEW_AD, Integer.valueOf(2));
        hashMap6.put(c_enum.MAX_AD, Integer.valueOf(3));
        f = Collections.unmodifiableMap(hashMap6);
        HashMap hashMap7 = new HashMap();
        hashMap7.put(Integer.valueOf(1), d_enum.LOW_POWER);
        hashMap7.put(Integer.valueOf(2), d_enum.BALANCED);
        hashMap7.put(Integer.valueOf(3), d_enum.LOW_LATENCY);
        g = Collections.unmodifiableMap(hashMap7);
        HashMap hashMap8 = new HashMap();
        hashMap8.put(d_enum.LOW_POWER, Integer.valueOf(1));
        hashMap8.put(d_enum.BALANCED, Integer.valueOf(2));
        hashMap8.put(d_enum.LOW_LATENCY, Integer.valueOf(3));
        h = Collections.unmodifiableMap(hashMap8);
    }

    @NonNull
    /* renamed from: a */
    public rr.a.b.B a(@NonNull tt.b bVar) {
        rr.a.b.B bVar2 = new rr.a.b.B();
        bVar2.b = ((Integer) b.get(bVar.f2632a)).intValue();
        bVar2.c = ((Integer) d.get(bVar.f2633b)).intValue();
        bVar2.d = ((Integer) f.get(bVar.f2634c)).intValue();
        bVar2.e = ((Integer) h.get(bVar.f2635d)).intValue();
        bVar2.f = bVar.f2636e;
        return bVar2;
    }

    @NonNull
    /* renamed from: a */
    public tt.b a(@NonNull rr.a.b.B bVar) {
        return new tt.b((a_enum) a.get(Integer.valueOf(bVar.b)), (b_enum) c.get(Integer.valueOf(bVar.c)), (c_enum) e.get(Integer.valueOf(bVar.d)), (d_enum) g.get(Integer.valueOf(bVar.e)), bVar.f);
    }

    @NonNull
    @Override
    public Object a(@NonNull Object o) {
        return null;
    }

    @NonNull
    @Override
    public Object b(@NonNull Object o) {
        return null;
    }
}
