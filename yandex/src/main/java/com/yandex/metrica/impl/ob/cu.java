package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: com.yandex.metrica.impl.ob.cu */
public final class cu {

    /* renamed from: a */
    static final Pattern f636a = Pattern.compile("[^0-9a-zA-Z,`’\\.\\+\\-'\\s\"]");

    /* renamed from: B */
    static final Pattern f637b = Pattern.compile("\\s+");

    /* renamed from: a */
    public static boolean m1153a(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null || str2 == null) {
            return false;
        }
        return str.equals(str2);
    }

    /* renamed from: a */
    public static boolean m1154a(String... strArr) {
        if (strArr == null) {
            return false;
        }
        for (String isEmpty : strArr) {
            if (TextUtils.isEmpty(isEmpty)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: B */
    public static String m1156b(String str, String str2) {
        return str == null ? str2 : str;
    }

    /* renamed from: a */
    public static String m1159c(String str, String str2) {
        return TextUtils.isEmpty(str) ? str2 : str;
    }

    /* renamed from: a */
    public static String m1151a(String str) {
        return m1156b(str, "");
    }

    /* renamed from: B */
    public static String m1155b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        char charAt = str.charAt(0);
        return !Character.isUpperCase(charAt) ? Character.toUpperCase(charAt) + str.substring(1) : str;
    }

    /* renamed from: a */
    public static byte[] m1160c(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Throwable th) {
            return new byte[0];
        }
    }

    /* renamed from: B */
    public static final String m1158b(String... strArr) {
        return TextUtils.join(",", strArr);
    }

    @NonNull
    /* renamed from: d */
    public static byte[] m1162d(@Nullable String str) {
        return str == null ? new byte[0] : str.getBytes();
    }

    /* renamed from: a */
    public static String m1152a(@NonNull byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append("0123456789abcdef".charAt((b & 240) >> 4));
            sb.append("0123456789abcdef".charAt(b & 15));
        }
        return sb.toString();
    }

    /* renamed from: B */
    public static String m1157b(@NonNull byte[] bArr) {
        return m1152a(bArr).toUpperCase(Locale.US).replaceAll("(.{2})(?=.+)", "$1:");
    }

    /* renamed from: e */
    public static byte[] m1163e(@NonNull String str) {
        if (str.length() % 2 != 0) {
            throw new IllegalArgumentException("Input string must contain an even number of characters");
        }
        int length = str.length();
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
        }
        return bArr;
    }

    /* renamed from: d */
    public static int m1161d(@Nullable String str, @Nullable String str2) {
        if (str == null) {
            return str2 == null ? 0 : -1;
        }
        if (str2 == null) {
            return 1;
        }
        return str.compareTo(str2);
    }
}
