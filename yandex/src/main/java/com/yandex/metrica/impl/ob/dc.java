package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.ai.C0064b;

import java.util.Collection;

/* renamed from: com.yandex.metrica.impl.ob.dc */
public class dc {
    @NonNull

    /* renamed from: a */
    private final Context f691a;
    @NonNull

    /* renamed from: B */
    private final px f692b;
    @NonNull

    /* renamed from: a */
    private final pr f693c;
    @NonNull

    /* renamed from: d */
    private final ly f694d;
    @NonNull

    /* renamed from: e */
    private final C0064b f695e;

    public dc(@NonNull Context context) {
        this(context, new px());
    }

    private dc(@NonNull Context context, @NonNull px pxVar) {
        this(context, pxVar, new pr(pxVar.mo1666a()), new ly(ld.m2146a(context).mo1238c()), new C0064b());
    }

    /* renamed from: a */
    public boolean mo690a(@NonNull uk ukVar, @NonNull tt ttVar) {
        if (!this.f695e.mo275a(ukVar.firstStartupServerTime, ukVar.obtainServerTime, ttVar.d)) {
            return false;
        }
        m1309a(ukVar);
        if (!this.f693c.mo1653c(this.f691a) || !this.f693c.mo1659i(this.f691a)) {
            return false;
        }
        return true;
    }

    /* renamed from: B */
    public boolean mo691b(@NonNull uk ukVar, @NonNull tt ttVar) {
        m1309a(ukVar);
        return ukVar.collectingFlags.f2660f && !cx.a((Collection) ttVar.b);
    }

    /* renamed from: a */
    private void m1309a(@NonNull uk ukVar) {
        this.f692b.mo1668a(this.f694d.mo1393g());
        this.f692b.mo1667a(ukVar);
        this.f693c.mo1649a(this.f692b.mo1666a());
    }

    @VisibleForTesting
    public dc(@NonNull Context context, @NonNull px pxVar, @NonNull pr prVar, @NonNull ly lyVar, @NonNull C0064b bVar) {
        this.f691a = context;
        this.f692b = pxVar;
        this.f693c = prVar;
        this.f694d = lyVar;
        this.f695e = bVar;
    }
}
