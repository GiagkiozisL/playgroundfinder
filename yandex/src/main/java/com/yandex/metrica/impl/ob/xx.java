package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.xx */
public class xx {
    @NonNull

    /* renamed from: a */
    private final xw f2996a;
    @NonNull

    /* renamed from: B */
    private final xv f2997b;

    public xx(@NonNull vz vzVar, @NonNull String str) {
        this(new xw(30, 50, 4000, str, vzVar), new xv(4500, str, vzVar));
    }

    @VisibleForTesting
    xx(@NonNull xw xwVar, @NonNull xv xvVar) {
        this.f2996a = xwVar;
        this.f2997b = xvVar;
    }

    /* renamed from: a */
    public boolean mo2646a(@Nullable vx vxVar, @NonNull String str, @Nullable String str2) {
        if (vxVar != null) {
            String a = this.f2996a.mo2642a().a(str);
            String a2 = this.f2996a.mo2644b().a(str2);
            if (vxVar.containsKey(a)) {
                String str3 = (String) vxVar.get(a);
                if (a2 == null || !a2.equals(str3)) {
                    return mo2647a(vxVar, a, a2, str3);
                }
            } else if (a2 != null) {
                return mo2647a(vxVar, a, a2, null);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public synchronized boolean mo2647a(@NonNull vx vxVar, @NonNull String str, @Nullable String str2, @Nullable String str3) {
        boolean z;
        if (vxVar.size() >= this.f2996a.mo2645c().mo2639a() && (this.f2996a.mo2645c().mo2639a() != vxVar.size() || !vxVar.containsKey(str))) {
            this.f2996a.mo2643a(str);
            z = false;
        } else if (!this.f2997b.mo2641a(vxVar, str, str2)) {
            vxVar.put(str, str2);
            z = true;
        } else {
            this.f2997b.mo2640a(str);
            z = false;
        }
        return z;
    }
}
