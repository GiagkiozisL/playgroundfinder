package com.yandex.metrica.impl.interact;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.bt;
import com.yandex.metrica.impl.ob.dr;
import com.yandex.metrica.impl.ob.du;
import com.yandex.metrica.impl.ob.dv;
import com.yandex.metrica.impl.ob.dz;
import com.yandex.metrica.impl.ob.v;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class DeviceInfo {

    /* renamed from: a */
    private static final Object f124a = new Object();

    /* renamed from: B */
    private static volatile DeviceInfo f125b;
    public final String appPlatform;
    public final String deviceRootStatus;
    public final List<String> deviceRootStatusMarkers;
    public final String deviceType;
    public String locale;
    public final String manufacturer;
    public final String model;
    public final String osVersion;
    public final String platform;
    public final String platformDeviceId;
    public final float scaleFactor;
    public final int screenDpi;
    public final int screenHeight;
    public final int screenWidth;

    public static DeviceInfo getInstance(@NonNull Context context) {
        if (f125b == null) {
            synchronized (f124a) {
                if (f125b == null) {
                    f125b = new DeviceInfo(context, v.getInstance(context));
                }
            }
        }
        return f125b;
    }

    @VisibleForTesting
    DeviceInfo(@NonNull Context context, @NonNull v deviceInfo) {
        this.platform = deviceInfo.app_platform;
        this.appPlatform = deviceInfo.app_platform;
        this.platformDeviceId = deviceInfo.getAndroidId();
        this.manufacturer = deviceInfo.manufacturer;
        this.model = deviceInfo.model;
        this.osVersion = deviceInfo.app_version_name;
        this.screenWidth = deviceInfo.f2870f.f2880a;
        this.screenHeight = deviceInfo.f2870f.f2881b;
        this.screenDpi = deviceInfo.f2870f.f2882c;
        this.scaleFactor = deviceInfo.f2870f.f2883d;
        this.deviceType = deviceInfo.device_type;
        this.deviceRootStatus = "0";// deviceInfo.is_rooted;
        this.deviceRootStatusMarkers = new ArrayList(deviceInfo.f2873i);
        this.locale = bt.m724a(context.getResources().getConfiguration().locale);
        dr.a().a(this, dz.class, dv.m1388a((du<dz>) new du<dz>() {
            /* renamed from: a */
            public void a(dz dzVar) {
                DeviceInfo.this.locale = dzVar.f753a;
            }
        }).mo736a());
    }
}
