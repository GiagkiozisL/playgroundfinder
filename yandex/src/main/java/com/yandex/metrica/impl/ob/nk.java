package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rr.a.b.Aa;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.nk */
public class nk implements mq<List<tt.a>, Aa[]> {
    @NonNull

    /* renamed from: a */
    private final nj a;

    public nk() {
        this(new nj());
    }

    @NonNull
    /* renamed from: a */
    public List<tt.a> a(@NonNull Aa[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (Aa a : aVarArr) {
            arrayList.add(this.a.a(a));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public com.yandex.metrica.impl.ob.rr.a.b.Aa[] b(@NonNull List<tt.a> list) {
        Aa[] aVarArr = new Aa[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return aVarArr;
            }
            aVarArr[i2] = this.a.b((tt.a) list.get(i2));
            i = i2 + 1;
        }
    }

    @VisibleForTesting
    nk(@NonNull nj njVar) {
        this.a = njVar;
    }

//    @NonNull
//    @Override
//    public tt.a a(@NonNull Aa a) {
//        return null;
//    }
//
//    @NonNull
//    @Override
//    public Aa b(@NonNull tt.a a) {
//        return null;
//    }
}
