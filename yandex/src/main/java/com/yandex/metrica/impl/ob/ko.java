package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.ko */
public class ko {
    @NonNull
    /* renamed from: a */
    public Integer mo1154a(@Nullable Boolean bool) {
        if (Boolean.TRUE.equals(bool)) {
            return Integer.valueOf(1);
        }
        if (Boolean.FALSE.equals(bool)) {
            return Integer.valueOf(0);
        }
        return Integer.valueOf(-1);
    }
}
