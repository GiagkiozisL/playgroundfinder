package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.wu */
public class wu implements xb {

    /* renamed from: a */
    private wt f2948a;

    public wu() {
        this(new wt());
    }

    @Nullable
    /* renamed from: a */
    public byte[] a(@Nullable byte[] seedBts) {
        return this.f2948a.a(seedBts);
    }

    @NonNull
    /* renamed from: a */
    public xc a() {
        return xc.AES_RSA;
    }

    @VisibleForTesting
    wu(wt wtVar) {
        this.f2948a = wtVar;
    }
}
