package com.yandex.metrica.impl.ac;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.ah;

/* renamed from: com.yandex.metrica.impl.ac.NativeCrashesHelper */
public class NativeCrashesHelper {

    /* renamed from: a */
    private String f96a;

    /* renamed from: B */
    private final Context f97b;

    /* renamed from: a */
    private boolean f98c;

    /* renamed from: d */
    private boolean f99d;
    @NonNull

    /* renamed from: e */
    private final ah f100e;

    private static native void cancelSetUpNativeUncaughtExceptionHandler();

    private static native void logsEnabled(boolean z);

    private static native void setUpNativeUncaughtExceptionHandler(String str);

    public NativeCrashesHelper(@NonNull Context context) {
        this(context, new ah());
    }

    @VisibleForTesting
    NativeCrashesHelper(@NonNull Context context, @NonNull ah fileProvider) {
        this.f97b = context;
        this.f100e = fileProvider;
    }

    /* renamed from: a */
    public synchronized void mo187a(boolean z) {
        if (z) {
            m162c();
        } else {
            m164e();
        }
    }

    /* renamed from: B */
    private void m160b() {
        if (!this.f99d && mo188a()) {
            m161b(false);
            this.f96a = this.f100e.mo263a(this.f97b).getAbsolutePath() + "/" + "YandexMetricaNativeCrashes";
        }
        this.f99d = true;
    }

    /* renamed from: a */
    private void m162c() {
        try {
            m160b();
            if (m163d()) {
                setUpNativeUncaughtExceptionHandler(this.f96a);
                this.f98c = true;
            }
        } catch (Throwable th) {
            this.f98c = false;
        }
    }

    /* renamed from: d */
    private boolean m163d() {
        return this.f96a != null;
    }

    /* renamed from: e */
    private void m164e() {
        try {
            if (m163d() && this.f98c) {
                cancelSetUpNativeUncaughtExceptionHandler();
            }
        } catch (Throwable th) {
        }
        this.f98c = false;
    }

    /* renamed from: B */
    private boolean m161b(boolean z) {
        try {
            logsEnabled(z);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public boolean mo188a() {
        try {
            System.loadLibrary("YandexMetricaNativeModule");
            return true;
        } catch (Throwable th) {
            return false;
        }
    }
}
