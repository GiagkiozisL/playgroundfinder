package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rm.C0846a;
import com.yandex.metrica.impl.ob.rm.C0846a.C0848b;

import java.util.ArrayList;

/* renamed from: com.yandex.metrica.impl.ob.mm */
public class mm implements mq<pn, C0846a> {
    @NonNull

    /* renamed from: a */
    private final mo f1482a;

    public mm() {
        this(new mo());
    }

    @VisibleForTesting
    mm(@NonNull mo moVar) {
        this.f1482a = moVar;
    }

    @NonNull
    /* renamed from: a */
    public C0846a b(@NonNull pn pnVar) {
        int i = 0;
        C0846a aVar = new C0846a();
        aVar.f2056b = new C0848b[pnVar.f1734a.size()];
        int i2 = 0;
        for (pu a : pnVar.f1734a) {
            aVar.f2056b[i2] = m2436a(a);
            i2++;
        }
        if (pnVar.f1735b != null) {
            aVar.f2057c = this.f1482a.b(pnVar.f1735b);
        }
        aVar.f2058d = new String[pnVar.f1736c.size()];
        for (String str : pnVar.f1736c) {
            aVar.f2058d[i] = str;
            i++;
        }
        return aVar;
    }

    @NonNull
    /* renamed from: a */
    public pn a(@NonNull C0846a aVar) {
        ArrayList arrayList = new ArrayList();
        for (C0848b a : aVar.f2056b) {
            arrayList.add(m2435a(a));
        }
        l lVar = null;
        if (aVar.f2057c != null) {
            lVar = this.f1482a.a(aVar.f2057c);
        }
        ArrayList arrayList2 = new ArrayList();
        for (String add : aVar.f2058d) {
            arrayList2.add(add);
        }
        return new pn(arrayList, lVar, arrayList2);
    }

    /* renamed from: a */
    private C0848b m2436a(@NonNull pu puVar) {
        C0848b bVar = new C0848b();
        bVar.f2062b = puVar.name;
        bVar.f2063c = puVar.granted;
        return bVar;
    }

    /* renamed from: a */
    private pu m2435a(@NonNull C0848b bVar) {
        return new pu(bVar.f2062b, bVar.f2063c);
    }
}
