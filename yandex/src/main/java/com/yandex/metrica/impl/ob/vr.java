package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.vr */
public abstract class vr {

    /* renamed from: a */
    private static Map<String, vz> f2902a = new HashMap();

    /* renamed from: B */
    private static Map<String, vp> f2903b = new HashMap();

    @NonNull
    /* renamed from: a */
    public static vz m4237a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return m4236a();
        }
        if (!f2902a.containsKey(str)) {
            f2902a.put(str, new vz(str));
        }
        return (vz) f2902a.get(str);
    }

    @NonNull
    /* renamed from: B */
    public static vp m4239b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return m4238b();
        }
        if (!f2903b.containsKey(str)) {
            f2903b.put(str, new vp(str));
        }
        return (vp) f2903b.get(str);
    }

    @NonNull
    /* renamed from: a */
    public static vz m4236a() {
        return vz.m4266h();
    }

    @NonNull
    /* renamed from: B */
    public static vp m4238b() {
        return vp.m4208h();
    }
}
