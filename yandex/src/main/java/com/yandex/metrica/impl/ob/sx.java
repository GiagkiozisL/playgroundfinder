package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ac.a.c;

/* renamed from: com.yandex.metrica.impl.ob.sx */
public class sx extends sy<ph> {

    /* renamed from: a */
    private long f2522a;

    /* renamed from: a */
    public void mo2210a(@NonNull Builder builder, @NonNull ph phVar) {
        String a;
        super.mo2210a(builder, phVar);
        builder.appendPath("location");
        builder.appendQueryParameter("deviceid", phVar.getDeviceId());
        builder.appendQueryParameter("device_type", phVar.getDeviceType());
        builder.appendQueryParameter("uuid", phVar.getUUId());
        builder.appendQueryParameter("analytics_sdk_version_name", phVar.getSDKVersionName());
        builder.appendQueryParameter("analytics_sdk_build_number", phVar.getSDKVersionNumber());
        builder.appendQueryParameter("analytics_sdk_build_type", phVar.getSDKBuildType());
        builder.appendQueryParameter("app_version_name", phVar.getAppVersionName());
        builder.appendQueryParameter("app_build_number", phVar.getAppVersionCode());
        builder.appendQueryParameter("os_version", phVar.getOsVersion());
        builder.appendQueryParameter("os_api_level", String.valueOf(phVar.getApiLevel()));
        builder.appendQueryParameter("is_rooted", phVar.getIsRooted());
        builder.appendQueryParameter("app_framework", phVar.getAppFramework());
        builder.appendQueryParameter("app_id", phVar.getAppId());
        builder.appendQueryParameter("app_platform", phVar.getAppPlatform());
        builder.appendQueryParameter("android_id", phVar.getAndroidId());
        builder.appendQueryParameter("request_id", String.valueOf(this.f2522a));
        c D = phVar.mo2077D();
        String str = D == null ? "" : D.advId;
        String str2 = "adv_id";
        if (str == null) {
            str = "";
        }
        builder.appendQueryParameter(str2, str);
        String str3 = "limit_ad_tracking";
        if (D == null) {
            a = "";
        } else {
            a = "1";//mo2211a(D.limitTracking);
        }
        builder.appendQueryParameter(str3, a);
    }

    /* renamed from: a */
    public void mo2208a(long j) {
        this.f2522a = j;
    }
}
