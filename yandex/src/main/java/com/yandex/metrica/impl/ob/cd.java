package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.RemoteException;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ac.NativeCrashesHelper;
import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.ce.C0192c;
import com.yandex.metrica.impl.ob.ce.d;
import com.yandex.metrica.impl.ob.rl.a;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import me.android.ydx.Constant;

public class cd implements ao {

    private final Context f500a;

    private bb f501b;

    private final NativeCrashesHelper f502c;

    private ax f503d;
    @NonNull

    private final u f504e;

    private ug f505f;

    private final kt f506g;
    @NonNull

    private final kn f507h;

    private final ce f508i;

    cd(ee eeVar, Context context, xh xhVar) {
        this(eeVar, context, new NativeCrashesHelper(context), new bb(context, xhVar), new kt(), new kn());
    }

    @VisibleForTesting
    cd(ee eeVar, Context context, @NonNull NativeCrashesHelper nativeCrashesHelper, @NonNull bb bbVar, @NonNull kt ktVar, @NonNull kn knVar) {
        this.f501b = bbVar;
        this.f500a = context;
        this.f502c = nativeCrashesHelper;
        this.f504e = new u(eeVar);
        this.f506g = ktVar;
        this.f507h = knVar;
        this.f508i = new ce(this);
    }

    public void mo563a(@Nullable ax axVar) {
        this.f503d = axVar;
    }

    public void mo571a(ug ugVar) {
        this.f505f = ugVar;
        this.f504e.mo548b(ugVar);
    }

    public void mo581a(boolean z, bz bzVar) {
        this.f502c.mo187a(z);
    }

    public void mo573a(@Nullable Boolean bool, @Nullable Boolean bool2) {
        Log.d(Constant.RUS_TAG, "cd$mo573a");
        if (cx.m1189a((Object) bool)) {
            this.f504e.mo744h().mo24a(bool);
        }
        if (cx.m1189a((Object) bool2)) {
            this.f504e.mo744h().mo44g(bool2);
        }
        mo572a(w.m4290t(), (bz) this.f504e);
    }

    public void mo575a(String str, bz bzVar) {
        mo572a(af.m272a(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, str, m877c(bzVar)), bzVar);
    }

    public void mo586c() {
        this.f501b.mo384g();
    }

    public void mo587d() {
        this.f501b.mo385h();
    }

    public w m875b(w wVar, bz bzVar) {
        if (wVar.mo2533g() == C0058a.EVENT_TYPE_EXCEPTION_USER.mo259a() || wVar.mo2533g() == C0058a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.mo259a()) {
            wVar.mo2530e(bzVar.mo551e());
        }
        return wVar;
    }

    public void mo572a(w wVar, bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo572a");
        mo562a(m875b(wVar, bzVar), bzVar, null);
    }

    public Future<Void> mo562a(w wVar, final bz bzVar, final Map<String, Object> map) {
        Log.d(Constant.RUS_TAG, "cd$mo562a");
        this.f501b.mo381d();
        d dVar = new d(wVar, bzVar);
        if (!cx.m1193a((Map) map)) {
            dVar.mo596a((C0192c) new C0192c() {
                public w mo588a(w wVar) {
                    return cd.this.m875b(wVar.mo1767c(vq.m4225b(map)), bzVar);
                }
            });
        }
        return m874a(dVar);
    }

    public void mo579a(@NonNull List<String> list, @NonNull ResultReceiver resultReceiver, @Nullable Map<String, String> map) {
        mo572a(af.m271a(C0058a.EVENT_TYPE_STARTUP, vr.m4236a()).mo2523a(new ap(list, map, resultReceiver)), (bz) this.f504e);
    }

    public void mo574a(String str) {
        mo572a(af.m294f(str, vr.m4236a()), (bz) this.f504e);
    }

    public void mo570a(@Nullable sj sjVar) {
        mo572a(af.m274a(sjVar, vr.m4236a()), (bz) this.f504e);
    }

    public void mo564a(bz bzVar) {
        mo572a(af.m273a(bzVar.mo552f(), m877c(bzVar)), bzVar);
    }

    public void mo578a(List<String> list) {
        this.f504e.mo743g().mo748a(list);
    }

    public void mo580a(Map<String, String> map) {
        this.f504e.mo743g().mo749a(map);
    }

    public void mo584b(String str) {
        this.f504e.mo743g().mo747a(str);
    }

    public void mo566a(@NonNull kh khVar, @NonNull bz bzVar) {
        mo572a(af.m279a(e.m1395a((e) this.f507h.b(khVar)), m877c(bzVar)), bzVar);
    }

    public void mo576a(@NonNull String str, @NonNull kl klVar, @NonNull bz bzVar) {
        mo572a(af.m278a(str, e.m1395a((e) this.f506g.b(klVar)), m877c(bzVar)), bzVar);
    }

    public void mo567a(@NonNull kl klVar, bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo567a");
        this.f501b.mo381d();
        r b = af.m282b(klVar.f1216a, e.m1395a((e) this.f506g.b(klVar)), m877c(bzVar));
        b.mo2530e(bzVar.mo551e());
        try {
            m874a(new d(b, bzVar).mo597a(b.mo1765a()).mo598a(true)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void mo568a(n nVar) {
        this.f501b.mo381d();
    }

    public void mo583b(n nVar) {
        this.f501b.mo380c();
    }

    public void a(IMetricaService iMetricaService, w wVar, bz bzVar) throws RemoteException {
        Log.d(Constant.RUS_TAG, "cd$a");
        m876b(iMetricaService, wVar, bzVar);
        m878e();
    }

    public void mo577a(String str, String str2, bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo577a");
        m874a(new d(r.m3001a(str, str2), bzVar));
    }

    public void mo582b(bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo582b");
        m874a(new d(r.m3004b(), bzVar));
    }

    public void mo569a(@NonNull final a aVar, @NonNull bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo569a");
        m874a(new d(r.m3007c(), bzVar).mo596a((C0192c) new C0192c() {
            public w mo588a(w wVar) {
                return wVar.mo1767c(new String(Base64.encode(e.m1395a((e) aVar), 0)));
            }
        }));
    }

    public void mo585b(@Nullable final String str, @NonNull bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo585b");
        m874a(new d(r.m3000a(str, m877c(bzVar)), bzVar).mo596a((C0192c) new C0192c() {
            public w mo588a(w wVar) {
                return wVar.mo1767c(str);
            }
        }));
    }

    public void mo565a(@NonNull final cg cgVar, @NonNull bz bzVar) {
        Log.d(Constant.RUS_TAG, "cd$mo565a");
        m874a(new d(r.m2999a(m877c(bzVar)), bzVar).mo596a((C0192c) new C0192c() {
            public w mo588a(w wVar) {
                Pair a = cgVar.mo605a();
                return wVar.mo1767c(new String(Base64.encode((byte[]) a.first, 0))).mo2527c(((Integer) a.second).intValue());
            }
        }));
    }

    private void m878e() {
        if (this.f503d == null || this.f503d.mo1466f()) {
            this.f501b.mo380c();
        }
    }

    private static void m876b(IMetricaService iMetricaService, w wVar, bz bzVar) throws RemoteException {
        Log.d(Constant.RUS_TAG, "cd$m876b");
        iMetricaService.mo67a(wVar.mo2519a(bzVar.mo547b()));
    }

    private Future<Void> m874a(d dVar) {
        Log.d(Constant.RUS_TAG, "cd$m874a");
        dVar.mo595a().mo543a(this.f505f);
        return this.f508i.mo589a(dVar);
    }

    public bb a() {
        return this.f501b;
    }

    public Context b() {
        return this.f500a;
    }

    @NonNull
    private vz m877c(@NonNull bz bzVar) {
        return vr.m4237a(bzVar.mo744h().mo39e());
    }
}
