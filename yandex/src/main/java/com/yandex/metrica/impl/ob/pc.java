package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rh.b.C0820b;

/* renamed from: com.yandex.metrica.impl.ob.pc */
public class pc {
    @NonNull

    /* renamed from: a */
    private final og f1708a;
    @NonNull

    /* renamed from: B */
    private final pb f1709b;

    public pc(@NonNull og ogVar, @NonNull pb pbVar) {
        this.f1708a = ogVar;
        this.f1709b = pbVar;
    }

    @Nullable
    /* renamed from: a */
    public C0820b mo1628a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            os a = this.f1708a.mo1542a(j, str);
            if (a != null) {
                return this.f1709b.mo1627a(a);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
