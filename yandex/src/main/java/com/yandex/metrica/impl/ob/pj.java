package com.yandex.metrica.impl.ob;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.pj */
public abstract class pj {

    /* renamed from: a */
    private final String f1732a;

    /* renamed from: com.yandex.metrica.impl.ob.pj$a */
    public static final class a {

        /* renamed from: a */
        public static final int a = ((int) TimeUnit.SECONDS.toMillis(30));
    }

    /* renamed from: B */
    public abstract boolean mo1638b();

    public pj(String str) {
        this.f1732a = str;
    }

    /* renamed from: a */
    public HttpURLConnection a() throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f1732a).openConnection();
        httpURLConnection.setConnectTimeout(a.a);
        httpURLConnection.setReadTimeout(a.a);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestProperty("Accept", "application/json");
        httpURLConnection.setRequestProperty("User-Agent", ci.m942a("com.yandex.mobile.metrica.sdk"));
        return httpURLConnection;
    }
}
