package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.pu */
public class pu {
    @NonNull

    /* renamed from: a */
    public final String name;

    /* renamed from: B */
    public final boolean granted;

    public pu(@NonNull String str, boolean z) {
        this.name = str;
        this.granted = z;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        pu puVar = (pu) o;
        if (this.granted == puVar.granted) {
            return this.name.equals(puVar.name);
        }
        return false;
    }

    public int hashCode() {
        return (this.granted ? 1 : 0) + (this.name.hashCode() * 31);
    }

    public String toString() {
        return "PermissionState{name='" + this.name + '\'' + ", granted=" + this.granted + '}';
    }
}
