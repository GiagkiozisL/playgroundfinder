package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rg.C0814e;

/* renamed from: com.yandex.metrica.impl.ob.kr */
public class kr implements mq<kk, C0814e> {
    @NonNull

    /* renamed from: a */
    private kp f1225a = new kp();

    @NonNull
    /* renamed from: a */
    public C0814e b(@NonNull kk kkVar) {
        C0814e eVar = new C0814e();
        eVar.f1900f = kkVar.f1214e == null ? -1 : kkVar.f1214e.intValue();
        eVar.f1899e = kkVar.f1213d;
        eVar.f1897c = kkVar.f1211b;
        eVar.f1896b = kkVar.f1210a;
        eVar.f1898d = kkVar.f1212c;
        eVar.f1901g = this.f1225a.b(kkVar.f1215f);
        return eVar;
    }

    @NonNull
    /* renamed from: a */
    public kk a(@NonNull C0814e eVar) {
        throw new UnsupportedOperationException();
    }
}
