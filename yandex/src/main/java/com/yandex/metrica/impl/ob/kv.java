package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Base64;

import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.r.C0802a;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.kv */
public class kv {

    /* renamed from: a */
    private final byte[] f1234a;

    /* renamed from: B */
    private final String f1235b;

    /* renamed from: a */
    private final int f1236c;
    @NonNull

    /* renamed from: d */
    private final HashMap<C0802a, Integer> f1237d;

    /* renamed from: e */
    private final String f1238e;

    /* renamed from: f */
    private final Integer f1239f;

    /* renamed from: g */
    private final String f1240g;

    /* renamed from: h */
    private final String f1241h;
    @NonNull

    /* renamed from: i */
    private final C0008a f1242i;

    public kv(@NonNull String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        JSONObject jSONObject2 = jSONObject.getJSONObject("event");
        this.f1234a = Base64.decode(jSONObject2.getString("jvm_crash"), 0);
        this.f1235b = jSONObject2.getString("name");
        this.f1236c = jSONObject2.getInt("bytes_truncated");
        String optString = jSONObject2.optString("trimmed_fields");
        this.f1237d = new HashMap<>();
        if (optString != null) {
            try {
                HashMap var5 = vq.m4215a(optString);
                if (var5 != null) {
                    Iterator var6 = var5.entrySet().iterator();

                    while(var6.hasNext()) {
                        Entry var7 = (Entry)var6.next();
                        this.f1237d.put(com.yandex.metrica.impl.ob.r.C0802a.valueOf((String)var7.getKey()), Integer.parseInt((String)var7.getValue()));
                    }
                }
            } catch (Throwable var8) {
            }
//            try {
//                HashMap a = vq.m4215a(optString);
//                if (a != null) {
//                    for (Entry entry : a.entrySet()) {
//                        this.f1237d.put(C0802a.valueOf((String) entry.getKey()), Integer.valueOf(Integer.parseInt((String) entry.getValue())));
//                    }
//                }
//            } catch (Throwable th) {
//            }
        }
        JSONObject jSONObject3 = jSONObject.getJSONObject("process_configuration");
        this.f1238e = jSONObject3.getString("package_name");
        this.f1239f = Integer.valueOf(jSONObject3.getInt("pid"));
        this.f1240g = jSONObject3.getString("psid");
        JSONObject jSONObject4 = jSONObject.getJSONObject("reporter_configuration");
        this.f1241h = jSONObject4.getString("api_key");
        this.f1242i = m2061a(jSONObject4);
    }

    public kv(@NonNull w wVar, @NonNull ed edVar, @Nullable HashMap<C0802a, Integer> hashMap) {
        this.f1234a = wVar.mo2532f();
        this.f1235b = wVar.mo2528d();
        this.f1236c = wVar.mo2541o();
        if (hashMap != null) {
            this.f1237d = hashMap;
        } else {
            this.f1237d = new HashMap<>();
        }
        ee g = edVar.mo743g();
        this.f1238e = g.mo759h();
        this.f1239f = g.mo756e();
        this.f1240g = g.mo757f();
        CounterConfiguration h = edVar.mo744h();
        this.f1241h = h.mo39e();
        this.f1242i = h.mo54q();
    }

    /* renamed from: a */
    public byte[] mo1167a() {
        return this.f1234a;
    }

    /* renamed from: B */
    public String mo1168b() {
        return this.f1235b;
    }

    /* renamed from: a */
    public int mo1169c() {
        return this.f1236c;
    }

    @NonNull
    /* renamed from: d */
    public HashMap<C0802a, Integer> mo1170d() {
        return this.f1237d;
    }

    /* renamed from: e */
    public Integer mo1171e() {
        return this.f1239f;
    }

    /* renamed from: f */
    public String mo1172f() {
        return this.f1240g;
    }

    /* renamed from: g */
    public String mo1173g() {
        return this.f1238e;
    }

    /* renamed from: h */
    public String mo1174h() {
        return this.f1241h;
    }

    @NonNull
    /* renamed from: i */
    public C0008a mo1175i() {
        return this.f1242i;
    }

    /* renamed from: j */
    public String mo1176j() throws JSONException {
        HashMap hashMap = new HashMap();
        for (Entry entry : this.f1237d.entrySet()) {
            hashMap.put(((C0802a) entry.getKey()).name(), entry.getValue());
        }
        return new JSONObject().put("process_configuration", new JSONObject().put("pid", this.f1239f).put("psid", this.f1240g).put("package_name", this.f1238e)).put("reporter_configuration", new JSONObject().put("api_key", this.f1241h).put("reporter_type", this.f1242i.mo61a())).put("event", new JSONObject().put("jvm_crash", Base64.encodeToString(this.f1234a, 0)).put("name", this.f1235b).put("bytes_truncated", this.f1236c).put("trimmed_fields", vq.m4225b((Map) hashMap))).toString();
    }

    @Deprecated
    @NonNull
    /* renamed from: a */
    private C0008a m2061a(@NonNull JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("reporter_type")) {
            return C0008a.m72a(jSONObject.getString("reporter_type"));
        }
        if (jSONObject.getBoolean("is_commutation")) {
            return C0008a.COMMUTATION;
        }
        return C0008a.MAIN;
    }
}
