package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.fq */
public interface fq<C extends fp> {
    @NonNull
    /* renamed from: a */
    C a(@NonNull Context context, @NonNull fu fuVar, @NonNull fn fnVar, @NonNull eg egVar);
}
