package com.yandex.metrica.impl.ob;

import android.util.Log;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

import me.android.ydx.Constant;

public final class rj extends e {

    public String installReferrer;
    public long installBeginTimestampSeconds;
    public long eferrerClickTimestampSeconds;

    public rj() {
        mo1827d();
    }

    public rj mo1827d() {
        this.installReferrer = "";
        this.installBeginTimestampSeconds = 0;
        this.eferrerClickTimestampSeconds = 0;
        this.f754a = -1;
        return this;
    }

    public void mo739a(b bVar) throws IOException {
        Log.d(Constant.RUS_TAG, "rj referrer:" + installReferrer);
        Log.d(Constant.RUS_TAG, "rj installBeginTimestampSeconds:" + installBeginTimestampSeconds);
        Log.d(Constant.RUS_TAG, "rj eferrerClickTimestampSeconds:" + eferrerClickTimestampSeconds);
        if (!this.installReferrer.equals("")) {
            bVar.mo351a(1, this.installReferrer);
        }
        if (this.installBeginTimestampSeconds != 0) {
            bVar.mo349a(2, this.installBeginTimestampSeconds);
        }
        if (this.eferrerClickTimestampSeconds != 0) {
            bVar.mo349a(3, this.eferrerClickTimestampSeconds);
        }
        super.mo739a(bVar);
    }

    public int mo741c() {
        int c = super.mo741c();
        if (!this.installReferrer.equals("")) {
            c += b.m437b(1, this.installReferrer);
        }
        if (this.installBeginTimestampSeconds != 0) {
            c += b.d(2, this.installBeginTimestampSeconds);
        }
        if (this.eferrerClickTimestampSeconds != 0) {
            return c + b.d(3, this.eferrerClickTimestampSeconds);
        }
        return c;
    }

    public rj mo738a(a aVar) throws IOException {
        while (true) {
            int a = aVar.mo213a();
            switch (a) {
                case 0:
                    break;
                case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                    this.installReferrer = aVar.mo229i();
                    Log.d(Constant.RUS_TAG, "referrer from reflection is: " + this.installReferrer);
                    continue;
                case 16:
                    this.installBeginTimestampSeconds = aVar.mo221e();
                    continue;
                case 24:
                    this.eferrerClickTimestampSeconds = aVar.mo221e();
                    continue;
                default:
                    if (!g.a(aVar, a)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
//        return this;
    }

    public static rj m3172a(byte[] bArr) throws d {
        return (rj) e.m1393a(new rj(), bArr);
    }
}
