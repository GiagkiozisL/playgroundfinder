package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rg.C0811b;

/* renamed from: com.yandex.metrica.impl.ob.kn */
public class kn implements mq<kh, C0811b> {
    @NonNull

    /* renamed from: a */
    private final km f1222a;
    @NonNull

    /* renamed from: B */
    private final ko f1223b;

    public kn() {
        this(new km(new kr()), new ko());
    }

    @VisibleForTesting
    kn(@NonNull km kmVar, @NonNull ko koVar) {
        this.f1222a = kmVar;
        this.f1223b = koVar;
    }

    @NonNull
    /* renamed from: a */
    public C0811b b(@NonNull kh khVar) {
        C0811b bVar = new C0811b();
        bVar.f1882b = this.f1222a.b(khVar.f1204a);
        if (khVar.f1205b != null) {
            bVar.f1883c = khVar.f1205b;
        }
        bVar.f1884d = this.f1223b.mo1154a(khVar.f1206c).intValue();
        return bVar;
    }

    @NonNull
    /* renamed from: a */
    public kh a(@NonNull C0811b bVar) {
        throw new UnsupportedOperationException();
    }
}
