package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.b;
import com.yandex.metrica.impl.ac.a;

import java.util.Locale;

import me.android.ydx.CustomData;
import me.android.ydx.DataManager;

public class sn {

//    private String f2420a;

    private v v_instance;

    private String analytics_sdk_version;// = "3.8.0";
    private String analytics_sdk_build_number;// = "66508";
    private String buildType;
    private String app_platform;
    private String protocol_version;
    private String app_framework;
    private String f2428i;
    private String f2429j;
    private String f2430k;
    private String deviceid;
    private String deviceId2;
    private String f2433n;
    private String android_id;
    private a.c f2435p;
    private String f2436q;
    private String os_version;
    private String f2437r;
    private uk f2438s;
    private CustomData customData;


    public interface d<T extends sn, D> {
        @NonNull
        T a(D d);
    }

    public sn() {
        this.customData = DataManager.getInstance().getCustomData();
        this.buildType = TextUtils.isEmpty("") ? "public" : "public_";
        this.app_platform = customData.app_platform;// "android";
        this.protocol_version = customData.protocol_version;// "2";
        this.app_framework = customData.app_framework; //ci.m945b();
        this.f2436q = b.PHONE.name().toLowerCase(Locale.US);
        this.analytics_sdk_version = customData.analytics_sdk_version;
        this.analytics_sdk_build_number = customData.analytics_sdk_build_number;
        this.os_version = customData.os_version;
    }

    public String getAppId() {
        return customData.app_id;// this.f2420a;
    }

    /* access modifiers changed from: protected */
//    public void mo2081b(String str) {
//        this.f2420a = str;
//    }

    /* access modifiers changed from: protected */
    public uk mo2085e() {
        return this.f2438s;
    }

    public synchronized boolean mo2088f() {
        boolean z = true;
        synchronized (this) {
            if (cu.m1154a(getUUId(), getDeviceId(), this.f2433n)) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void mo2080a(v vVar) {
        this.v_instance = vVar;
    }

    /* access modifiers changed from: protected */
    public void mo2079a(uk ukVar) {
        this.f2438s = ukVar;
    }

    @NonNull
    public String getManufacturer() {
        return wk.m4373b(this.v_instance.manufacturer, "");
    }

    public String getProtocolVersion() {
//        return "2";
        return protocol_version;
    }

    public String getSDKVersionName() {
//        return "3.8.0";
        return analytics_sdk_version;
    }

    public String getSDKVersionNumber() {
//        return "66508";
        return analytics_sdk_build_number;
    }

    public String getSDKBuildType() {
        return this.buildType;
    }

    public String getAppPlatform() {
//        return "android";
        return app_platform;
    }

    @NonNull
    public String getModel() {
        return this.v_instance.model;
    }

    @NonNull
    public String getOsVersion() {
        return this.v_instance.os_version;
    }

    public int getApiLevel() {
        return this.v_instance.os_api_level;
    }

    /* access modifiers changed from: protected */
    public void setVersionCode(@Nullable String str) {
//        if (!TextUtils.isEmpty(str)) {
            this.f2429j = customData.app_build_number;
//        }
    }

    public String getAppVersionCode() {
        return wk.m4373b(this.f2429j, "");
    }

    public String getAppVersionName() {
        return customData.app_version_name;
//        return wk.m4373b(this.f2428i, "");
    }

    /* access modifiers changed from: protected */
    public void setVersionName(@Nullable String str) {
//        if (!TextUtils.isEmpty(str)) {
            this.f2428i = customData.app_version_name;
//        }
    }

    @NonNull
    public synchronized String getDeviceId() {
        return customData.deviceid;
//        return wk.m4373b(this.deviceid, "");
    }

    @NonNull
    public synchronized String mo2106s() {
        return wk.m4373b(this.deviceId2, "");
    }

    @NonNull
    public synchronized String getUUId() {
        return customData.uuid;
//        return wk.m4373b(this.f2430k, "");
    }

    /* access modifiers changed from: protected */
    public synchronized void setUUID(@Nullable String str) {
//        if (!TextUtils.isEmpty(str)) {
            this.f2430k = customData.uuid;
//        }
    }

    /* access modifiers changed from: protected */
    public synchronized void setDeviceId(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.deviceid = customData.deviceid;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void setDeviceId2(@Nullable String str) {
//        if (!TextUtils.isEmpty(str)) {
            this.deviceId2 = customData.deviceid2;
//        }
    }

    /* access modifiers changed from: protected */
    public synchronized void mo2092h(String str) {
        this.f2433n = str;
    }

    public void setAndroidId(String str) {
//        this.android_id = str;
    }

    @NonNull
    public String getIsRooted() {
        return "0"; //this.v_instance.is_rooted;
    }

    @NonNull
    public String getAppFramework() {
        return customData.app_framework;
    }

    public int getScreenWidth() {
        return this.v_instance.f2870f.f2880a;
    }

    public int getScreenHeight() {
        return this.v_instance.f2870f.f2881b;
    }

    public int getScreenDpi() {
        return this.v_instance.f2870f.f2882c;
    }

    public float getScaleFactore() {
        return this.v_instance.f2870f.f2883d;
    }

    @NonNull
    public String A() {
        return wk.m4373b(this.f2437r, "");
    }

    /* access modifiers changed from: 0000 */
    public final void mo2096j(String str) {
        this.f2437r = str;
    }

    @NonNull
    public String getAndroidId() {
        return customData.android_id;
    }

    public String getAdvId() {
        return customData.adv_id;
    }

    @NonNull
    public String getDeviceType() {
        return wk.m4373b(this.f2436q, b.PHONE.name().toLowerCase(Locale.US));
    }

    /* access modifiers changed from: 0000 */
    public void mo2098k(String str) {
        this.f2436q = str;
    }

    public a.c mo2077D() {
        return this.f2435p;
    }

    /* access modifiers changed from: protected */
    public void setAdvertisingInfos(a.c cVar) {
        this.f2435p = cVar;
    }


    ////////////////
    public static abstract class C0972a<I, O> implements sm<I, O> {
        @Nullable

        public final String f2439c;
        @Nullable

        public final String f2440d;
        @Nullable

        public final String f2441e;

        public C0972a(@Nullable String str, @Nullable String str2, @Nullable String str3) {
            this.f2439c = str;
            this.f2440d = str2;
            this.f2441e = str3;
        }
    }

    protected static abstract class C0973b<T extends sn, A extends C0972a> implements d<T, sn.c<A>> {
        @NonNull

        final Context ctx;
        @NonNull

        final String pkgName;

        /* access modifiers changed from: protected */
        @NonNull
        public abstract T b();

        protected C0973b(@NonNull Context context, @NonNull String str) {
            this.ctx = context;
            this.pkgName = str;
        }

        @NonNull
        public T a(@NonNull c cVar) {
            T b = b();
            v a = v.getInstance(this.ctx);
            b.mo2080a(a);
            b.mo2079a(cVar.f2444a);
            b.mo2098k(mo2114a(this.ctx, ((C0972a) cVar.b).f2439c));
            b.setAndroidId(wk.m4373b(a.getAndroidId(cVar.f2444a), ""));
            m3604c(b, cVar);
            m3602a(b, this.pkgName, ((C0972a) cVar.b).f2440d, this.ctx);
            m3603b(b, this.pkgName, ((C0972a) cVar.b).f2441e, this.ctx);
//            b.mo2081b(this.pkgName);
            b.setAdvertisingInfos(com.yandex.metrica.impl.ac.a.m167a().mo192c(this.ctx));
            b.mo2096j(aw.m403a(this.ctx).mo327a());
            return b;
        }

        private void m3602a(@NonNull T t, @NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (TextUtils.isEmpty(str2)) {
                str2 = cx.b(context, str);
            }
            t.setVersionName(str2);
        }

        private void m3603b(@NonNull T t, @NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (TextUtils.isEmpty(str2)) {
                str2 = cx.a(context, str);
            }
            t.setVersionCode(str2);
        }

        private synchronized void m3604c(@NonNull T t, @NonNull c cVar) {
            t.setUUID(mo946a(cVar));
            mo2115a(t, cVar);
            mo2116b(t, cVar);
        }

        /* access modifiers changed from: 0000 */
        public void mo2115a(T t, @NonNull c cVar) {
            t.setDeviceId(cVar.f2444a.deviceId);
            t.mo2092h(cVar.f2444a.deviceIDHash);
        }

        /* access modifiers changed from: 0000 */
        public void mo2116b(T t, @NonNull c cVar) {
            t.setDeviceId2(cVar.f2444a.deviceID2);
        }

        private String mo946a(@NonNull c cVar) {
            return cVar.f2444a.uuid;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public String mo2114a(@NonNull Context context, @Nullable String str) {
            return str == null ? v.getInstance(context).device_type : str;
        }
    }

    public static class c<A> {
        @NonNull

        public final uk f2444a;
        @NonNull

        public final A b;

        public c(@NonNull uk ukVar, A a) {
            this.f2444a = ukVar;
            this.b = a;
        }
    }
}
