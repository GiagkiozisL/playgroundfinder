package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.j;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import me.android.ydx.CustomData;
import me.android.ydx.DataManager;

public class ee implements Parcelable {
    public static final Creator<ee> CREATOR = new Creator<ee>() {
        public ee createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(x.class.getClassLoader());
            return new ee((ContentValues) readBundle.getParcelable("CFG_KEY_PROCESS_ENVIRONMENT"), (ResultReceiver) readBundle.getParcelable("CFG_KEY_PROCESS_ENVIRONMENT_RECEIVER"));
        }

        public ee[] newArray(int i) {
            return new ee[i];
        }
    };

    public static final String f762a = UUID.randomUUID().toString();
    @NonNull

    private final ContentValues contentValues;
    @Nullable

    private ResultReceiver f764c;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("CFG_KEY_PROCESS_ENVIRONMENT", this.contentValues);
        bundle.putParcelable("CFG_KEY_PROCESS_ENVIRONMENT_RECEIVER", this.f764c);
        dest.writeBundle(bundle);
    }

    public String toString() {
        return "ProcessConfiguration{mParamsMapping=" + this.contentValues + ", mDataResultReceiver=" + this.f764c + '}';
    }

    @Nullable
    public static ee m1404a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        try {
            return (ee) bundle.getParcelable("PROCESS_CFG_OBJ");
        } catch (Throwable th) {
            return null;
        }
    }

    public ee(Context context, @Nullable ResultReceiver resultReceiver) {
        CustomData customData = DataManager.getInstance().getCustomData();
        this.contentValues = new ContentValues();
        this.contentValues.put("PROCESS_CFG_PROCESS_ID", Process.myPid());
        this.contentValues.put("PROCESS_CFG_PROCESS_SESSION_ID", f762a);
        this.contentValues.put("PROCESS_CFG_SDK_API_LEVEL", customData.sdkApiLevel); //85));
        this.contentValues.put("PROCESS_CFG_PACKAGE_NAME", customData.app_id); // context.getPackageName());
        this.f764c = resultReceiver;
    }

    public ee(ee eeVar) {
        synchronized (eeVar) {
            this.contentValues = new ContentValues(eeVar.contentValues);
            this.f764c = eeVar.f764c;
        }
    }

    public ee(@NonNull ContentValues contentValues, @Nullable ResultReceiver resultReceiver) {
        if (contentValues == null) {
            contentValues = new ContentValues();
        }
        this.contentValues = contentValues;
        this.f764c = resultReceiver;
    }

    public void mo746a(@Nullable j jVar) {
        if (jVar != null) {
            synchronized (this) {
                m1405b(jVar);
                m1406c(jVar);
                m1407d(jVar);
            }
        }
    }

    private void m1405b(@NonNull j jVar) {
        if (cx.m1189a((Object) jVar.d)) {
            mo748a(jVar.d);
        }
    }

    private void m1406c(@NonNull j jVar) {
        if (cx.m1189a((Object) jVar.b)) {
            mo749a(we.m4343c(jVar.b));
        }
    }

    private void m1407d(@NonNull j jVar) {
        if (cx.m1189a((Object) jVar.c)) {
            mo747a(jVar.c);
        }
    }

    public boolean mo750a() {
        return this.contentValues.containsKey("PROCESS_CFG_CUSTOM_HOSTS");
    }

    @Nullable
    public List<String> mo751b() {
        String asString = this.contentValues.getAsString("PROCESS_CFG_CUSTOM_HOSTS");
        if (TextUtils.isEmpty(asString)) {
            return null;
        }
        return vq.m4228c(asString);
    }

    public synchronized void mo748a(@Nullable List<String> list) {
        this.contentValues.put("PROCESS_CFG_CUSTOM_HOSTS", vq.m4213a(list));
    }

    @Nullable
    public Map<String, String> mo753c() {
        return vq.m4215a(this.contentValues.getAsString("PROCESS_CFG_CLIDS"));
    }

    public synchronized void mo749a(@Nullable Map<String, String> map) {
        this.contentValues.put("PROCESS_CFG_CLIDS", vq.m4225b((Map) map));
    }

    @Nullable
    public String mo754d() {
        return this.contentValues.getAsString("PROCESS_CFG_DISTRIBUTION_REFERRER");
    }

    public synchronized void mo747a(@Nullable String str) {
        this.contentValues.put("PROCESS_CFG_DISTRIBUTION_REFERRER", str);
    }

    @NonNull
    public Integer mo756e() {
        return this.contentValues.getAsInteger("PROCESS_CFG_PROCESS_ID");
    }

    @NonNull
    public String mo757f() {
        return this.contentValues.getAsString("PROCESS_CFG_PROCESS_SESSION_ID");
    }

    public int mo758g() {
        return this.contentValues.getAsInteger("PROCESS_CFG_SDK_API_LEVEL").intValue();
    }

    @NonNull
    public String mo759h() {
        return this.contentValues.getAsString("PROCESS_CFG_PACKAGE_NAME");
    }

    @Nullable
    public ResultReceiver mo760i() {
        return this.f764c;
    }

    public synchronized void mo752b(@NonNull Bundle bundle) {
        bundle.putParcelable("PROCESS_CFG_OBJ", this);
    }
}
