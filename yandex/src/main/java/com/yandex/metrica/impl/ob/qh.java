package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qh */
public class qh extends qd {

    /* renamed from: d */
    private static final qk f1827d = new qk("PREF_KEY_OFFSET");

    /* renamed from: e */
    private qk f1828e = new qk(f1827d.mo1742a(), null);

    public qh(Context context, String str) {
        super(context, str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_servertimeoffset";
    }

    /* renamed from: a */
    public long mo1729a(int i) {
        return this.f1766c.getLong(this.f1828e.mo1744b(), (long) i);
    }

    /* renamed from: a */
    public void mo1684a() {
        mo1697h(this.f1828e.mo1744b()).mo1699j();
    }
}
