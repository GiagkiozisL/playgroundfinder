package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.ob */
public final class ob {

    /* renamed from: a */
    private final String f1559a;

    /* renamed from: B */
    private final int f1560b;

    /* renamed from: a */
    private final boolean f1561c;

    public ob(@NonNull JSONObject jSONObject) throws JSONException {
        this.f1559a = jSONObject.getString("name");
        this.f1561c = jSONObject.getBoolean("required");
        this.f1560b = jSONObject.optInt("version", -1);
    }

    public ob(String str, int i, boolean z) {
        this.f1559a = str;
        this.f1560b = i;
        this.f1561c = z;
    }

    public ob(String str, boolean z) {
        this(str, -1, z);
    }

    /* renamed from: a */
    public JSONObject mo1533a() throws JSONException {
        JSONObject put = new JSONObject().put("name", this.f1559a).put("required", this.f1561c);
        if (this.f1560b != -1) {
            put.put("version", this.f1560b);
        }
        return put;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ob obVar = (ob) o;
        if (this.f1560b != obVar.f1560b || this.f1561c != obVar.f1561c) {
            return false;
        }
        if (this.f1559a != null) {
            z = this.f1559a.equals(obVar.f1559a);
        } else if (obVar.f1559a != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.f1559a != null) {
            i = this.f1559a.hashCode();
        } else {
            i = 0;
        }
        int i3 = ((i * 31) + this.f1560b) * 31;
        if (this.f1561c) {
            i2 = 1;
        }
        return i3 + i2;
    }
}
