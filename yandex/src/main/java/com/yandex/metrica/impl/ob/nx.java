package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.yandex.metrica.impl.ob.nx */
public class nx {
    @NonNull

    /* renamed from: a */
    private final List<nv> f1554a;
    @NonNull

    /* renamed from: B */
    private final ny f1555b;

    /* renamed from: a */
    private final AtomicBoolean f1556c = new AtomicBoolean(true);

    public nx(@NonNull List<nv> list, @NonNull ny nyVar) {
        this.f1554a = list;
        this.f1555b = nyVar;
    }

    /* renamed from: a */
    public void mo1526a() {
        if (this.f1556c.get()) {
            m2632d();
        }
    }

    /* renamed from: B */
    public void mo1527b() {
        this.f1556c.set(true);
    }

    /* renamed from: a */
    public void mo1528c() {
        this.f1556c.set(false);
    }

    /* renamed from: d */
    private void m2632d() {
        boolean z;
        if (this.f1554a.isEmpty()) {
            m2633e();
            return;
        }
        boolean z2 = false;
        Iterator it = this.f1554a.iterator();
        while (true) {
            z = z2;
            if (!it.hasNext()) {
                break;
            }
            z2 = ((nv) it.next()).a() | z;
        }
        if (z) {
            m2633e();
        }
    }

    /* renamed from: e */
    private void m2633e() {
        this.f1555b.g();
    }
}
