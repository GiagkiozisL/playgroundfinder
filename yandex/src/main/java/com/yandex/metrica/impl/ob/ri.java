package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.ri */
public final class ri extends e {

    /* renamed from: B */
    public byte[] f2029b;

    /* renamed from: a */
    public long f2030c;

    /* renamed from: d */
    public long f2031d;

    public ri() {
        mo1825d();
    }

    /* renamed from: d */
    public ri mo1825d() {
        this.f2029b = g.h;
        this.f2030c = 0;
        this.f2031d = 0;
        this.f754a = -1;
        return this;
    }

    /* renamed from: a */
    public void mo739a(b bVar) throws IOException {
        bVar.mo353a(1, this.f2029b);
        if (this.f2030c != 0) {
            bVar.mo349a(2, this.f2030c);
        }
        if (this.f2031d != 0) {
            bVar.mo349a(3, this.f2031d);
        }
        super.mo739a(bVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo741c() {
        int c = super.mo741c() + b.b(1, this.f2029b);
        if (this.f2030c != 0) {
            c += b.d(2, this.f2030c);
        }
        if (this.f2031d != 0) {
            return c + b.d(3, this.f2031d);
        }
        return c;
    }

    /* renamed from: B */
    public ri mo738a(a aVar) throws IOException {
        while (true) {
            int a = aVar.mo213a();
            switch (a) {
                case 0:
                    break;
                case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                    this.f2029b = aVar.mo230j();
                    continue;
                case 16:
                    this.f2030c = aVar.mo221e();
                    continue;
                case 24:
                    this.f2031d = aVar.mo221e();
                    continue;
                default:
                    if (!g.a(aVar, a)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
//        return this;
    }
}
