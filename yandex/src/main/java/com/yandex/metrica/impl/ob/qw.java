package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rl.a.Bb;
import com.yandex.metrica.impl.ob.rl.a.Cc;

/* renamed from: com.yandex.metrica.impl.ob.qw */
public abstract class qw implements qm, rf {
    @NonNull

    /* renamed from: a */
    private final String f1860a;

    /* renamed from: B */
    private final int f1861b;
    @NonNull

    /* renamed from: a */
    private final yk<String> f1862c;
    @NonNull

    /* renamed from: d */
    private final qo f1863d;
    @NonNull

    /* renamed from: e */
    private vz f1864e = vr.m4236a();

    qw(int i, @NonNull String str, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        this.f1861b = i;
        this.f1860a = str;
        this.f1862c = ykVar;
        this.f1863d = qoVar;
    }

    @NonNull
    /* renamed from: a */
    public String mo1758c() {
        return this.f1860a;
    }

    /* renamed from: d */
    public int mo1759d() {
        return this.f1861b;
    }

    @NonNull
    /* renamed from: e */
    public qo mo1760e() {
        return this.f1863d;
    }

    @NonNull
    /* renamed from: a */
    public final rl.a.Aa a() {
        rl.a.Aa aVar = new rl.a.Aa();
        aVar.f2047c = mo1759d();
        aVar.f2046b = mo1758c().getBytes();
        aVar.f2049e = new Cc();
        aVar.f2048d = new Bb();
        return aVar;
    }

    /* renamed from: a */
    public void a(@NonNull vz vzVar) {
        this.f1864e = vzVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public boolean mo1761f() {
        yi a = this.f1862c.a(mo1758c());
        if (a.a()) {
            return true;
        }
        if (this.f1864e.mo2485c()) {
            this.f1864e.mo2481b("Attribute " + mo1758c() + " of type " + rd.m3021a(mo1759d()) + " is skipped because " + a.b());
        }
        return false;
    }
}
