package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import android.util.Base64;

/* renamed from: com.yandex.metrica.impl.ob.ly */
public class ly extends lx {

    /* renamed from: a */
    static final qk f1404a = new qk("LOCATION_TRACKING_ENABLED");
    @Deprecated

    /* renamed from: B */
    public static final qk f1405b = new qk("COLLECT_INSTALLED_APPS");

    /* renamed from: a */
    public static final qk f1406c = new qk("INSTALLED_APP_COLLECTING");

    /* renamed from: d */
    static final qk f1407d = new qk("REFERRER");

    /* renamed from: e */
    static final qk f1408e = new qk("REFERRER_FROM_PLAY_SERVICES");

    /* renamed from: f */
    static final qk f1409f = new qk("REFERRER_HANDLED");

    /* renamed from: g */
    static final qk f1410g = new qk("REFERRER_HOLDER_STATE");

    /* renamed from: h */
    static final qk f1411h = new qk("PREF_KEY_OFFSET");

    /* renamed from: i */
    static final qk f1412i = new qk("UNCHECKED_TIME");

    /* renamed from: j */
    static final qk f1413j = new qk("L_REQ_NUM");

    /* renamed from: k */
    static final qk f1414k = new qk("L_ID");

    /* renamed from: l */
    static final qk f1415l = new qk("LBS_ID");

    /* renamed from: m */
    static final qk f1416m = new qk("STATISTICS_RESTRICTED_IN_MAIN");

    /* renamed from: n */
    static final qk f1417n = new qk("SDKFCE");

    /* renamed from: o */
    static final qk f1418o = new qk("FST");

    /* renamed from: p */
    static final qk f1419p = new qk("LSST");

    /* renamed from: r */
    static final qk f1420r = new qk("FSDKFCO");

    /* renamed from: s */
    static final qk f1421s = new qk("SRSDKFC");

    /* renamed from: t */
    static final qk f1422t = new qk("LSDKFCAT");

    /* renamed from: u */
    static final qk f1423u = new qk("LAST_IDENTITY_LIGHT_SEND_TIME");

    /* renamed from: v */
    private static final qk f1424v = new qk("LAST_MIGRATION_VERSION");

    public ly(lf lfVar) {
        super(lfVar);
    }

    /* renamed from: a */
    public boolean mo1373a() {
        return mo1359b(f1406c.mo1744b(), false);
    }

    /* renamed from: B */
    public String mo1376b() {
        return mo1361c(f1407d.mo1744b(), null);
    }

    /* renamed from: a */
    public sj mo1380c() {
        return m2339b(mo1361c(f1408e.mo1744b(), null));
    }

    /* renamed from: B */
    private sj m2339b(String str) {
        sj sjVar = null;
        if (str == null) {
            return sjVar;
        }
        try {
            return sj.m3551a(Base64.encode(str.getBytes(), 0));
        } catch (d e) {
            return sjVar;
        }
    }

    /* renamed from: d */
    public boolean mo1385d() {
        return mo1359b(f1409f.mo1744b(), false);
    }

    /* renamed from: a */
    public ly mo1372a(boolean z) {
        return (ly) mo1353a(f1406c.mo1744b(), z);
    }

    /* renamed from: a */
    public ly mo1371a(String str) {
        return (ly) mo1357b(f1407d.mo1744b(), str);
    }

    /* renamed from: a */
    public ly mo1370a(sj sjVar) {
        return (ly) mo1357b(f1408e.mo1744b(), m2340b(sjVar));
    }

    /* renamed from: B */
    private String m2340b(@Nullable sj sjVar) {
        if (sjVar == null) {
            return null;
        }
        return new String(Base64.encode(sjVar.mo2068a(), 0));
    }

    /* renamed from: e */
    public ly mo1386e() {
        return (ly) mo1353a(f1409f.mo1744b(), true);
    }

    /* renamed from: f */
    public ly mo1391f() {
        return (ly) mo1365r(f1407d.mo1744b()).mo1365r(f1408e.mo1744b());
    }

    /* renamed from: a */
    public int mo1368a(int i) {
        return mo1355b(f1424v.mo1744b(), i);
    }

    /* renamed from: B */
    public ly mo1375b(int i) {
        return (ly) mo1350a(f1424v.mo1744b(), i);
    }

    /* renamed from: B */
    public void mo1377b(boolean z) {
        mo1353a(f1404a.mo1744b(), z).mo1364q();
    }

    /* renamed from: g */
    public boolean mo1393g() {
        return mo1359b(f1404a.mo1744b(), false);
    }

    /* renamed from: a */
    public long mo1378c(int i) {
        return mo1356b(f1411h.mo1744b(), (long) i);
    }

    /* renamed from: a */
    public ly mo1369a(long j) {
        return (ly) mo1351a(f1411h.mo1744b(), j);
    }

    /* renamed from: B */
    public long mo1374b(long j) {
        return mo1356b(f1413j.mo1744b(), j);
    }

    /* renamed from: a */
    public ly mo1379c(long j) {
        return (ly) mo1351a(f1413j.mo1744b(), j);
    }

    /* renamed from: d */
    public long mo1383d(long j) {
        return mo1356b(f1414k.mo1744b(), j);
    }

    /* renamed from: e */
    public ly mo1388e(long j) {
        return (ly) mo1351a(f1414k.mo1744b(), j);
    }

    /* renamed from: f */
    public long mo1390f(long j) {
        return mo1356b(f1415l.mo1744b(), j);
    }

    /* renamed from: g */
    public ly mo1392g(long j) {
        return (ly) mo1351a(f1415l.mo1744b(), j);
    }

    /* renamed from: a */
    public boolean mo1381c(boolean z) {
        return mo1359b(f1412i.mo1744b(), z);
    }

    /* renamed from: d */
    public ly mo1384d(boolean z) {
        return (ly) mo1353a(f1412i.mo1744b(), z);
    }

    /* renamed from: d */
    public int mo1382d(int i) {
        return mo1355b(f1410g.mo1744b(), i);
    }

    /* renamed from: e */
    public ly mo1387e(int i) {
        return (ly) mo1350a(f1410g.mo1744b(), i);
    }

    @Nullable
    /* renamed from: h */
    public Boolean mo1395h() {
        if (mo1367t(f1416m.mo1744b())) {
            return Boolean.valueOf(mo1359b(f1416m.mo1744b(), true));
        }
        return null;
    }

    /* renamed from: e */
    public ly mo1389e(boolean z) {
        return (ly) mo1353a(f1416m.mo1744b(), z);
    }

    /* renamed from: h */
    public long mo1394h(long j) {
        return mo1356b(f1423u.mo1744b(), j);
    }

    /* renamed from: i */
    public ly mo1396i(long j) {
        return (ly) mo1351a(f1423u.mo1744b(), j);
    }
}
