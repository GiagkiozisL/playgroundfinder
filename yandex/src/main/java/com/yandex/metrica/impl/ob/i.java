package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Pair;

/* renamed from: com.yandex.metrica.impl.ob.i */
public class i {

    /* renamed from: a */
    private vx f1026a;

    /* renamed from: B */
    private long f1027b;

    /* renamed from: a */
    private boolean f1028c;
    @NonNull

    /* renamed from: d */
    private final xx f1029d;

    /* renamed from: com.yandex.metrica.impl.ob.i$a */
    public static final class a {

        /* renamed from: a */
        public final String a;

        /* renamed from: B */
        public final long f1031b;

        public a(String str, long j) {
            this.a = str;
            this.f1031b = j;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            a aVar = (a) o;
            if (this.f1031b != aVar.f1031b) {
                return false;
            }
            if (this.a != null) {
                if (this.a.equals(aVar.a)) {
                    return true;
                }
            } else if (aVar.a == null) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return ((this.a != null ? this.a.hashCode() : 0) * 31) + ((int) (this.f1031b ^ (this.f1031b >>> 32)));
        }
    }

    public i(String str, long j, @NonNull vz vzVar) {
        this(str, j, new xx(vzVar, "App Environment"));
    }

    @VisibleForTesting
    i(String str, long j, @NonNull xx xxVar) {
        this.f1027b = j;
        try {
            this.f1026a = new vx(str);
        } catch (Throwable th) {
            this.f1026a = new vx();
            this.f1027b = 0;
        }
        this.f1029d = xxVar;
    }

    /* renamed from: a */
    public synchronized void mo985a() {
        this.f1026a = new vx();
        this.f1027b = 0;
    }

    /* renamed from: a */
    public synchronized void mo986a(@NonNull Pair<String, String> pair) {
        if (this.f1029d.mo2646a(this.f1026a, (String) pair.first, (String) pair.second)) {
            this.f1028c = true;
        }
    }

    /* renamed from: B */
    public synchronized a mo987b() {
        if (this.f1028c) {
            this.f1027b++;
            this.f1028c = false;
        }
        return new a(this.f1026a.toString(), this.f1027b);
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder("Map size ");
        sb.append(this.f1026a.size());
        sb.append(". Is changed ");
        sb.append(this.f1028c);
        sb.append(". Current revision ");
        sb.append(this.f1027b);
        return sb.toString();
    }
}
