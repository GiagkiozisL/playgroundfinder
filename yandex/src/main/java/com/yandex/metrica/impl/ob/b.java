package com.yandex.metrica.impl.ob;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/* renamed from: com.yandex.metrica.impl.ob.B */
public final class b {

    /* renamed from: a */
    private final byte[] f282a;

    /* renamed from: B */
    private final int f283b;

    /* renamed from: a */
    private int f284c;

    /* renamed from: com.yandex.metrica.impl.ob.B$a */
    public static class C0090a extends IOException {
        C0090a(int i, int i2) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space (pos " + i + " limit " + i2 + ").");
        }
    }

    private b(byte[] bArr, int i, int i2) {
        this.f282a = bArr;
        this.f284c = i;
        this.f283b = i + i2;
    }

    /* renamed from: a */
    public static b m431a(byte[] bArr, int i, int i2) {
        return new b(bArr, i, i2);
    }

    /* renamed from: a */
    public void mo346a(int i, double d) throws IOException {
        mo371g(i, 1);
        mo343a(d);
    }

    /* renamed from: a */
    public void mo347a(int i, float f) throws IOException {
        mo371g(i, 5);
        mo344a(f);
    }

    /* renamed from: a */
    public void mo349a(int i, long j) throws IOException {
        mo371g(i, 0);
        mo354a(j);
    }

    /* renamed from: B */
    public void b(int i, long j) throws IOException {
        mo371g(i, 0);
        mo363b(j);
    }

    /* renamed from: a */
    public void mo348a(int i, int i2) throws IOException {
        mo371g(i, 0);
        mo345a(i2);
    }

    /* renamed from: a */
    public void mo352a(int i, boolean z) throws IOException {
        mo371g(i, 0);
        mo357a(z);
    }

    /* renamed from: a */
    public void mo351a(int i, String str) throws IOException {
        mo371g(i, 2);
        mo356a(str);
    }

    /* renamed from: a */
    public void mo350a(int i, e eVar) throws IOException {
        mo371g(i, 2);
        mo355a(eVar);
    }

    /* renamed from: a */
    public void mo353a(int i, byte[] bArr) throws IOException {
        mo371g(i, 2);
        mo358a(bArr);
    }

    /* renamed from: B */
    public void mo361b(int i, int i2) throws IOException {
        mo371g(i, 0);
        mo360b(i2);
    }

    /* renamed from: a */
    public void mo366c(int i, int i2) throws IOException {
        mo371g(i, 0);
        mo365c(i2);
    }

    /* renamed from: a */
    public void mo367c(int i, long j) throws IOException {
        mo371g(i, 0);
        mo368c(j);
    }

    /* renamed from: a */
    public void mo343a(double d) throws IOException {
        mo374i(Double.doubleToLongBits(d));
    }

    /* renamed from: a */
    public void mo344a(float f) throws IOException {
        mo375k(Float.floatToIntBits(f));
    }

    /* renamed from: a */
    public void mo354a(long j) throws IOException {
        mo372g(j);
    }

    /* renamed from: B */
    public void mo363b(long j) throws IOException {
        mo372g(j);
    }

    /* renamed from: a */
    public void mo345a(int i) throws IOException {
        if (i >= 0) {
            mo373i(i);
        } else {
            mo372g((long) i);
        }
    }

    /* renamed from: a */
    public void mo357a(boolean z) throws IOException {
        mo370g(z ? 1 : 0);
    }

    /* renamed from: a */
    public void mo356a(String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        mo373i(bytes.length);
        mo369c(bytes);
    }

    /* renamed from: a */
    public void mo355a(e eVar) throws IOException {
        mo373i(eVar.mo737a());
        eVar.mo739a(this);
    }

    /* renamed from: a */
    public void mo358a(byte[] bArr) throws IOException {
        mo373i(bArr.length);
        mo369c(bArr);
    }

    /* renamed from: B */
    public void mo360b(int i) throws IOException {
        mo373i(i);
    }

    /* renamed from: a */
    public void mo365c(int i) throws IOException {
        mo373i(m460l(i));
    }

    /* renamed from: a */
    public void mo368c(long j) throws IOException {
        mo372g(m459j(j));
    }

    /* renamed from: B */
    public static int m434b(int i, double d) {
        return m456h(i) + m432b(d);
    }

    /* renamed from: B */
    public static int m435b(int i, float f) {
        return m456h(i) + m433b(f);
    }

    /* renamed from: d */
    public static int d(int i, long j) {
        return m456h(i) + m447d(j);
    }

    /* renamed from: e */
    public static int m450e(int i, long j) {
        return m456h(i) + m451e(j);
    }

    /* renamed from: d */
    public static int m445d(int i, int i2) {
        return m456h(i) + m444d(i2);
    }

    /* renamed from: B */
    public static int m438b(int i, boolean z) {
        return m456h(i) + m442b(z);
    }

    /* renamed from: B */
    public static int m437b(int i, String str) {
        return m456h(i) + m441b(str);
    }

    /* renamed from: B */
    public static int b(int i, e eVar) {
        return m456h(i) + m440b(eVar);
    }

    /* renamed from: B */
    public static int b(int i, byte[] bArr) {
        return m456h(i) + m443b(bArr);
    }

    /* renamed from: e */
    public static int m449e(int i, int i2) {
        return m456h(i) + m448e(i2);
    }

    /* renamed from: f */
    public static int m453f(int i, int i2) {
        return m456h(i) + m452f(i2);
    }

    /* renamed from: f */
    public static int m454f(int i, long j) {
        return m456h(i) + m455f(j);
    }

    /* renamed from: B */
    public static int m432b(double d) {
        return 8;
    }

    /* renamed from: B */
    public static int m433b(float f) {
        return 4;
    }

    /* renamed from: d */
    public static int m447d(long j) {
        return m457h(j);
    }

    /* renamed from: e */
    public static int m451e(long j) {
        return m457h(j);
    }

    /* renamed from: d */
    public static int m444d(int i) {
        if (i >= 0) {
            return m458j(i);
        }
        return 10;
    }

    /* renamed from: B */
    public static int m442b(boolean z) {
        return 1;
    }

    /* renamed from: B */
    public static int m441b(String str) {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            return bytes.length + m458j(bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.");
        }
    }

    /* renamed from: B */
    public static int m440b(e eVar) {
        int b = eVar.mo740b();
        return b + m458j(b);
    }

    /* renamed from: B */
    public static int m443b(byte[] bArr) {
        return m458j(bArr.length) + bArr.length;
    }

    /* renamed from: e */
    public static int m448e(int i) {
        return m458j(i);
    }

    /* renamed from: f */
    public static int m452f(int i) {
        return m458j(m460l(i));
    }

    /* renamed from: f */
    public static int m455f(long j) {
        return m457h(m459j(j));
    }

    /* renamed from: a */
    public int mo341a() {
        return this.f283b - this.f284c;
    }

    /* renamed from: B */
    public void mo359b() {
        if (mo341a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* renamed from: a */
    public void mo342a(byte b) throws IOException {
        if (this.f284c == this.f283b) {
            throw new C0090a(this.f284c, this.f283b);
        }
        byte[] bArr = this.f282a;
        int i = this.f284c;
        this.f284c = i + 1;
        bArr[i] = b;
    }

    /* renamed from: g */
    public void mo370g(int i) throws IOException {
        mo342a((byte) i);
    }

    /* renamed from: a */
    public void mo369c(byte[] bArr) throws IOException {
        mo364b(bArr, 0, bArr.length);
    }

    /* renamed from: B */
    public void mo364b(byte[] bArr, int i, int i2) throws IOException {
        if (this.f283b - this.f284c >= i2) {
            System.arraycopy(bArr, i, this.f282a, this.f284c, i2);
            this.f284c += i2;
            return;
        }
        throw new C0090a(this.f284c, this.f283b);
    }

    /* renamed from: g */
    public void mo371g(int i, int i2) throws IOException {
        mo373i(g.a(i, i2));
    }

    /* renamed from: h */
    public static int m456h(int i) {
        return m458j(g.a(i, 0));
    }

    /* renamed from: i */
    public void mo373i(int i) throws IOException {
        while ((i & -128) != 0) {
            mo370g((i & 127) | 128);
            i >>>= 7;
        }
        mo370g(i);
    }

    /* renamed from: j */
    public static int m458j(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        if ((-268435456 & i) == 0) {
            return 4;
        }
        return 5;
    }

    /* renamed from: g */
    public void mo372g(long j) throws IOException {
        while ((-128 & j) != 0) {
            mo370g((((int) j) & 127) | 128);
            j >>>= 7;
        }
        mo370g((int) j);
    }

    /* renamed from: h */
    public static int m457h(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        if ((Long.MIN_VALUE & j) == 0) {
            return 9;
        }
        return 10;
    }

    /* renamed from: k */
    public void mo375k(int i) throws IOException {
        mo370g(i & 255);
        mo370g((i >> 8) & 255);
        mo370g((i >> 16) & 255);
        mo370g((i >> 24) & 255);
    }

    /* renamed from: i */
    public void mo374i(long j) throws IOException {
        mo370g(((int) j) & 255);
        mo370g(((int) (j >> 8)) & 255);
        mo370g(((int) (j >> 16)) & 255);
        mo370g(((int) (j >> 24)) & 255);
        mo370g(((int) (j >> 32)) & 255);
        mo370g(((int) (j >> 40)) & 255);
        mo370g(((int) (j >> 48)) & 255);
        mo370g(((int) (j >> 56)) & 255);
    }

    /* renamed from: l */
    public static int m460l(int i) {
        return (i << 1) ^ (i >> 31);
    }

    /* renamed from: j */
    public static long m459j(long j) {
        return (j << 1) ^ (j >> 63);
    }
}
