package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.text.TextUtils;

import com.yandex.metrica.PreloadInfo;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.YandexMetricaConfig.Builder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.az */
public class az {
    /* renamed from: a */
    public String mo340a(YandexMetricaConfig yandexMetricaConfig) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("apikey", yandexMetricaConfig.apiKey);
            jSONObject.put("app_version", yandexMetricaConfig.appVersion);
            jSONObject.put("session_timeout", yandexMetricaConfig.sessionTimeout);
            jSONObject.put("location", m425a(yandexMetricaConfig.location));
            jSONObject.put("preload_info", m426a(yandexMetricaConfig.preloadInfo));
            jSONObject.put("collect_apps", yandexMetricaConfig.installedAppCollecting);
            jSONObject.put("logs", yandexMetricaConfig.logs);
            jSONObject.put("crash_enabled", yandexMetricaConfig.crashReporting);
            jSONObject.put("crash_native_enabled", yandexMetricaConfig.nativeCrashReporting);
            jSONObject.put("location_enabled", yandexMetricaConfig.locationTracking);
            jSONObject.put("max_reports_in_db_count", yandexMetricaConfig.maxReportsInDatabaseCount);
            return jSONObject.toString();
        } catch (Throwable th) {
            return "";
        }
    }

    /* renamed from: a */
    public YandexMetricaConfig mo339a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            Builder newConfigBuilder = YandexMetricaConfig.newConfigBuilder(jSONObject.getString("apikey"));
            if (jSONObject.has("app_version")) {
                newConfigBuilder.withAppVersion(jSONObject.optString("app_version"));
            }
            if (jSONObject.has("session_timeout")) {
                newConfigBuilder.withSessionTimeout(jSONObject.getInt("session_timeout"));
            }
            newConfigBuilder.withLocation(m428c(jSONObject.optString("location")));
            newConfigBuilder.withPreloadInfo(m427b(jSONObject.optString("preload_info")));
            if (jSONObject.has("collect_apps")) {
                newConfigBuilder.withInstalledAppCollecting(jSONObject.optBoolean("collect_apps"));
            }
            if (jSONObject.has("logs") && jSONObject.optBoolean("logs")) {
                newConfigBuilder.withLogs();
            }
            if (jSONObject.has("crash_enabled")) {
                newConfigBuilder.withCrashReporting(jSONObject.optBoolean("crash_enabled"));
            }
            if (jSONObject.has("crash_native_enabled")) {
                newConfigBuilder.withNativeCrashReporting(jSONObject.optBoolean("crash_native_enabled"));
            }
            if (jSONObject.has("location_enabled")) {
                newConfigBuilder.withLocationTracking(jSONObject.optBoolean("location_enabled"));
            }
            if (jSONObject.has("max_reports_in_db_count")) {
                newConfigBuilder.withMaxReportsInDatabaseCount(jSONObject.optInt("max_reports_in_db_count"));
            }
            return newConfigBuilder.build();
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    private String m426a(PreloadInfo preloadInfo) {
        String str = null;
        if (preloadInfo == null) {
            return str;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("trackid", preloadInfo.getTrackingId());
            jSONObject.put("params", vq.m4225b(preloadInfo.getAdditionalParams()));
            return jSONObject.toString();
        } catch (Throwable th) {
            return str;
        }
    }

    /* renamed from: B */
    private PreloadInfo m427b(String str) throws JSONException {
        String str2 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        if (jSONObject.has("trackid")) {
            str2 = jSONObject.optString("trackid");
        }
        PreloadInfo.Builder newBuilder = PreloadInfo.newBuilder(str2);
        HashMap a = vq.m4215a(jSONObject.optString("params"));
        if (a != null && a.size() > 0) {
            Iterator var7 = a.entrySet().iterator();

            while(var7.hasNext()) {
                Entry var8 = (Entry)var7.next();
                newBuilder.setAdditionalParams((String)var8.getKey(), (String)var8.getValue());
            }
//            for (Entry entry : a.entrySet()) {
//                newBuilder.setAdditionalParams((String) entry.getKey(), (String) entry.getValue());
//            }
        }
        return newBuilder.build();
    }

    /* renamed from: a */
    private String m425a(Location location) {
        String str = null;
        if (location == null) {
            return str;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("provider", location.getProvider());
            jSONObject.put("time", location.getTime());
            jSONObject.put("accuracy", (double) location.getAccuracy());
            jSONObject.put("alt", location.getAltitude());
            jSONObject.put("lng", location.getLongitude());
            jSONObject.put("lat", location.getLatitude());
            return jSONObject.toString();
        } catch (Throwable th) {
            return str;
        }
    }

    /* renamed from: a */
    private Location m428c(String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("provider")) {
                str2 = jSONObject.optString("provider");
            } else {
                str2 = null;
            }
            Location location = new Location(str2);
            location.setLongitude(jSONObject.getDouble("lng"));
            location.setLatitude(jSONObject.getDouble("lat"));
            location.setTime(jSONObject.optLong("time"));
            location.setAccuracy((float) jSONObject.optDouble("accuracy"));
            location.setAltitude((double) ((float) jSONObject.optDouble("alt")));
            return location;
        } catch (Throwable th) {
            return null;
        }
    }
}
