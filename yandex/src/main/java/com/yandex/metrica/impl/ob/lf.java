package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.lf */
public class lf {

    /* renamed from: a */
    public static final String f1311a = lf.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: B */
    public final Map<String, Object> b;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Map<String, Object> f1313c;

    /* renamed from: d */
    private final String f1314d;

    /* renamed from: e */
    private final C0563a f1315e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public volatile boolean f1316f;

    /* renamed from: g */
    private final lm g;

    /* renamed from: com.yandex.metrica.impl.ob.lf$a */
    private class C0563a extends xl {
        public C0563a(String str) {
            super(str);
        }

        public void run() {
            HashMap hashMap;
            synchronized (lf.this.b) {
                lf.this.c();
                lf.this.f1316f = true;
                lf.this.b.notifyAll();
            }
            while (c()) {
                synchronized (this) {
                    if (lf.this.f1313c.size() == 0) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                        }
                    }
                    hashMap = new HashMap(lf.this.f1313c);
                    lf.this.f1313c.clear();
                }
                if (hashMap.size() > 0) {
                    lf.this.m2166a((Map<String, Object>) hashMap);
                    hashMap.clear();
                }
            }
        }
    }

    public lf(lc lcVar, String str) {
        this(str, (lm) new lp(lcVar));
    }

    protected lf(String str, lm lmVar) {
        this.b = new HashMap();
        this.f1313c = new HashMap();
        this.g = lmVar;
        this.f1314d = str;
        this.f1315e = new C0563a(String.format(Locale.US, "YMM-DW-%s", new Object[]{Integer.valueOf(xm.m4445b())}));
        this.f1315e.start();
    }

    public void c() {
        Cursor var1 = null;
        SQLiteDatabase var2 = null;

        try {
            var2 = this.g.a();
            if (var2 != null) {
                var1 = var2.query(this.a(), new String[]{"key", "value", "type"}, (String)null, (String[])null, (String)null, (String)null, (String)null);

                while(var1.moveToNext()) {
                    String var3 = var1.getString(var1.getColumnIndex("key"));
                    String var4 = var1.getString(var1.getColumnIndex("value"));
                    int var5 = var1.getInt(var1.getColumnIndex("type"));
                    if (!TextUtils.isEmpty(var3)) {
                        Object var6 = null;
                        switch(var5) {
                            case 1:
                                var6 = "true".equals(var4) ? Boolean.TRUE : ("false".equals(var4) ? Boolean.FALSE : null);
                                break;
                            case 2:
                                var6 = vy.c(var4);
                                break;
                            case 3:
                                var6 = vy.a(var4);
                                break;
                            case 4:
                                var6 = var4;
                                break;
                            case 5:
                                var6 = vy.b(var4);
                        }

                        if (var6 != null) {
                            this.b.put(var3, var6);
                        }
                    }
                }
            }
        } catch (Throwable var10) {
        } finally {
            cx.a(var1);
            this.g.a(var2);
        }

    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String a() {
        return this.f1314d;
    }

    /* renamed from: B */
    public void mo1256b() {
        synchronized (this.f1315e) {
            this.f1315e.notifyAll();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2166a(Map<String, Object> map) {
        ContentValues[] contentValuesArr = new ContentValues[map.size()];
        int i = 0;
        Iterator it = map.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                Entry entry = (Entry) it.next();
                ContentValues contentValues = new ContentValues();
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                contentValues.put("key", str);
                if (value == this) {
                    contentValues.putNull("value");
                } else if (value instanceof String) {
                    contentValues.put("value", (String) value);
                    contentValues.put("type", Integer.valueOf(4));
                } else if (value instanceof Long) {
                    contentValues.put("value", (Long) value);
                    contentValues.put("type", Integer.valueOf(3));
                } else if (value instanceof Integer) {
                    contentValues.put("value", (Integer) value);
                    contentValues.put("type", Integer.valueOf(2));
                } else if (value instanceof Boolean) {
                    contentValues.put("value", String.valueOf(((Boolean) value).booleanValue()));
                    contentValues.put("type", Integer.valueOf(1));
                } else if (value instanceof Float) {
                    contentValues.put("value", (Float) value);
                    contentValues.put("type", Integer.valueOf(5));
                }
                contentValuesArr[i2] = contentValues;
                i = i2 + 1;
            } else {
                m2167a(contentValuesArr);
                return;
            }
        }
    }

    /* renamed from: a */
    private void m2167a(ContentValues[] contentValuesArr) {
        SQLiteDatabase sQLiteDatabase = null;
        if (contentValuesArr != null) {
            try {
                sQLiteDatabase = this.g.a();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.beginTransaction();
                    for (ContentValues contentValues : contentValuesArr) {
                        if (contentValues.getAsString("value") == null) {
                            sQLiteDatabase.delete(a(), "key = ?", new String[]{contentValues.getAsString("key")});
                        } else {
                            sQLiteDatabase.insertWithOnConflict(a(), null, contentValues, 5);
                        }
                    }
                    sQLiteDatabase.setTransactionSuccessful();
                }
            } catch (Throwable th) {
            } finally {
                cx.m1184a(sQLiteDatabase);
                this.g.a(sQLiteDatabase);
            }
        }
    }

    @Nullable
    /* renamed from: a */
    public String mo1249a(String str, String str2) {
        Object c = m2170c(str);
        if (c instanceof String) {
            return (String) c;
        }
        return str2;
    }

    /* renamed from: a */
    public int mo1245a(String str, int i) {
        Object c = m2170c(str);
        if (c instanceof Integer) {
            return ((Integer) c).intValue();
        }
        return i;
    }

    /* renamed from: a */
    public long mo1246a(String str, long j) {
        Object c = m2170c(str);
        if (c instanceof Long) {
            return ((Long) c).longValue();
        }
        return j;
    }

    /* renamed from: a */
    public boolean mo1251a(String str, boolean z) {
        Object c = m2170c(str);
        if (c instanceof Boolean) {
            return ((Boolean) c).booleanValue();
        }
        return z;
    }

    /* renamed from: a */
    public lf mo1247a(String str) {
        synchronized (this.b) {
            m2173d();
            this.b.remove(str);
        }
        synchronized (this.f1315e) {
            this.f1313c.put(str, this);
            this.f1315e.notifyAll();
        }
        return this;
    }

    /* renamed from: B */
    public synchronized lf mo1254b(String str, String str2) {
        mo1250a(str, (Object) str2);
        return this;
    }

    /* renamed from: B */
    public lf mo1253b(String str, long j) {
        mo1250a(str, (Object) Long.valueOf(j));
        return this;
    }

    /* renamed from: B */
    public synchronized lf mo1252b(String str, int i) {
        mo1250a(str, (Object) Integer.valueOf(i));
        return this;
    }

    /* renamed from: B */
    public lf mo1255b(String str, boolean z) {
        mo1250a(str, (Object) Boolean.valueOf(z));
        return this;
    }

    /* renamed from: B */
    public boolean mo1257b(@NonNull String str) {
        boolean containsKey;
        synchronized (this.b) {
            m2173d();
            containsKey = this.b.containsKey(str);
        }
        return containsKey;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo1250a(String str, Object obj) {
        synchronized (this.b) {
            m2173d();
            this.b.put(str, obj);
        }
        synchronized (this.f1315e) {
            this.f1313c.put(str, obj);
            this.f1315e.notifyAll();
        }
    }

    /* renamed from: a */
    private Object m2170c(String str) {
        Object obj;
        synchronized (this.b) {
            m2173d();
            obj = this.b.get(str);
        }
        return obj;
    }

    /* renamed from: d */
    private void m2173d() {
        if (!this.f1316f) {
            try {
                this.b.wait();
            } catch (InterruptedException e) {
            }
        }
    }
}
