package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rr.a.C0863a;
import com.yandex.metrica.impl.ob.rr.a.C0863a.C0864a;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.mn */
public class mn implements mq<oc, C0863a> {
    @NonNull

    /* renamed from: a */
    private final ms f1483a;
    @NonNull

    /* renamed from: B */
    private final mu f1484b;

    public mn() {
        this(new ms(), new mu());
    }

    @NonNull
    /* renamed from: a */
    public C0863a b(@NonNull oc ocVar) {
        C0864a[] a;
        C0863a aVar = new C0863a();
        aVar.f2125b = this.f1483a.b((oh) ocVar);
        aVar.f2127d = ocVar.f1563b;
        aVar.f2126c = ocVar.f1562a;
        aVar.f2128e = ocVar.f1564c;
        if (ocVar.f1565d == null) {
            a = new C0864a[0];
        } else {
            a = this.f1484b.b(ocVar.f1565d);
        }
        aVar.f2129f = a;
        return aVar;
    }

    @NonNull
    /* renamed from: a */
    public oc a(@NonNull C0863a aVar) {
        List a;
        oh a2 = this.f1483a.a(aVar.f2125b);
        long j = a2.f1571e;
        float f = a2.f1572f;
        int i = a2.f1573g;
        int i2 = a2.f1574h;
        long j2 = a2.f1575i;
        int i3 = a2.f1576j;
        boolean z = a2.f1577k;
        long j3 = aVar.f2126c;
        long j4 = aVar.f2127d;
        long j5 = a2.f1578l;
        boolean z2 = a2.f1579m;
        boolean z3 = aVar.f2128e;
        if (cx.m1195a(aVar.f2129f)) {
            a = null;
        } else {
            a = this.f1484b.a(aVar.f2129f);
        }
        return new oc(j, f, i, i2, j2, i3, z, j3, j4, j5, z2, z3, a);
    }

    @VisibleForTesting
    mn(@NonNull ms msVar, @NonNull mu muVar) {
        this.f1483a = msVar;
        this.f1484b = muVar;
    }
}
