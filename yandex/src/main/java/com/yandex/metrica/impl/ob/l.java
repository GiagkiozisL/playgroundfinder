package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.l */
public class l {
    @Nullable

    /* renamed from: a */
    public final A_enum f1274a;
    @Nullable

    /* renamed from: B */
    public final Boolean f1275b;

    /* renamed from: com.yandex.metrica.impl.ob.l$a */
    public enum A_enum {
        ACTIVE,
        WORKING_SET,
        FREQUENT,
        RARE
    }

    public l(@Nullable A_enum aVar, @Nullable Boolean bool) {
        this.f1274a = aVar;
        this.f1275b = bool;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        l lVar = (l) o;
        if (this.f1274a != lVar.f1274a) {
            return false;
        }
        if (this.f1275b != null) {
            return this.f1275b.equals(lVar.f1275b);
        }
        if (lVar.f1275b != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.f1274a != null) {
            i = this.f1274a.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        if (this.f1275b != null) {
            i2 = this.f1275b.hashCode();
        }
        return i3 + i2;
    }
}
