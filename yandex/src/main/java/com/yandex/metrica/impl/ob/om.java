package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.om */
public class om {
    @NonNull

    /* renamed from: a */
    private final lg f1611a;
    @NonNull

    /* renamed from: B */
    private final og f1612b;

    public om(@NonNull Context context) {
        this(ld.m2146a(context).mo1243h(), new og(context));
    }

    /* renamed from: a */
    public void mo1559a(@NonNull on onVar) {
        String a = this.f1612b.mo1543a(onVar);
        if (!TextUtils.isEmpty(a)) {
            this.f1611a.mo1184b(onVar.mo1564b(), a);
        }
    }

    @VisibleForTesting
    om(@NonNull lg lgVar, @NonNull og ogVar) {
        this.f1611a = lgVar;
        this.f1612b = ogVar;
    }
}
