package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.lx */
public abstract class lx {

    /* renamed from: q */
    public static final String f1401q = lx.class.getSimpleName();

    /* renamed from: a */
    private final lf f1402a;

    /* renamed from: B */
    private final String pkganame;

    public lx(lf lfVar) {
        this(lfVar, null);
    }

    public lx(lf lfVar, String pkganame) {
        this.f1402a = lfVar;
        this.pkganame = pkganame;
    }

    /* renamed from: p */
    public String getPkgName() {
        return this.pkganame;
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public qk mo1363q(String str) {
        return new qk(str, getPkgName());
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public <T extends lx> T mo1357b(String str, String str2) {
        synchronized (this) {
            this.f1402a.mo1254b(str, str2);
        }
        return (T) this;
    }

    /* renamed from: a */
    public <T extends lx> T mo1351a(String str, long j) {
        synchronized (this) {
            this.f1402a.mo1253b(str, j);
        }
        return (T) this;
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: a */
    public <T extends lx> T mo1350a(String str, int i) {
        synchronized (this) {
            this.f1402a.mo1252b(str, i);
        }
        return (T) this;
    }

    /* renamed from: a */
    public <T extends lx> T mo1353a(String str, boolean z) {
        synchronized (this) {
            this.f1402a.mo1255b(str, z);
        }
        return (T) this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends lx> T mo1354a(String str, String[] strArr) {
        String str2 = null;
        try {
            JSONArray jSONArray = new JSONArray();
            for (String put : strArr) {
                jSONArray.put(put);
            }
            str2 = jSONArray.toString();
        } catch (Throwable th) {
        }
        this.f1402a.mo1254b(str, str2);
        return (T) this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends lx> T mo1352a(String str, List<String> list) {
        return mo1354a(str, (String[]) list.toArray(new String[list.size()]));
    }

    /* renamed from: r */
    public <T extends lx> T mo1365r(String str) {
        synchronized (this) {
            this.f1402a.mo1247a(str);
        }
        return (T) this;
    }

    /* renamed from: q */
    public void mo1364q() {
        synchronized (this) {
            this.f1402a.mo1256b();
        }
    }

    /* renamed from: B */
    public long mo1356b(String str, long j) {
        return this.f1402a.mo1246a(str, j);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public int mo1355b(String str, int i) {
        return this.f1402a.mo1245a(str, i);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: s */
    public String mo1366s(@NonNull String str) {
        return this.f1402a.mo1249a(str, (String) null);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: a */
    public String mo1361c(@NonNull String str, @Nullable String str2) {
        return this.f1402a.mo1249a(str, str2);
    }

    /* renamed from: B */
    public boolean mo1359b(String str, boolean z) {
        return this.f1402a.mo1251a(str, z);
    }

    /* renamed from: t */
    public boolean mo1367t(@NonNull String str) {
        return this.f1402a.mo1257b(str);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public String[] mo1360b(String str, String[] strArr) {
        String a = this.f1402a.mo1249a(str, (String) null);
        if (!TextUtils.isEmpty(a)) {
            try {
                JSONArray jSONArray = new JSONArray(a);
                strArr = new String[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    strArr[i] = jSONArray.optString(i);
                }
            } catch (Throwable th) {
            }
        }
        return strArr;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: B */
    public List<String> mo1358b(@NonNull String str, @Nullable List<String> list) {
        String[] strArr;
        if (list == null) {
            strArr = null;
        } else {
            strArr = (String[]) list.toArray(new String[list.size()]);
        }
        String[] b = mo1360b(str, strArr);
        if (b == null) {
            return null;
        }
        return Arrays.asList(b);
    }
}
