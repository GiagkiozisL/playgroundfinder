package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.f;

/* renamed from: com.yandex.metrica.impl.ob.cb */
public class cb {
    @NonNull

    /* renamed from: a */
    private String f495a;
    @NonNull

    /* renamed from: B */
    private ca f496b;

    public cb(@NonNull ca caVar, @NonNull String str) {
        this.f496b = caVar;
        this.f495a = str;
    }

    @NonNull
    /* renamed from: a */
    public an mo556a() {
        return (an) this.f496b.mo555b(f.m121a(this.f495a).mo153b());
    }
}
