package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.xz */
public class xz extends xq<String> {
    @VisibleForTesting(otherwise = 3)
    /* renamed from: a */
    public /* bridge */ /* synthetic */ int mo2635a() {
        return super.mo2635a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    /* renamed from: B */
    public /* bridge */ /* synthetic */ String mo2636b() {
        return super.mo2636b();
    }

    public xz(int i, @NonNull String str) {
        this(i, str, vz.m4266h());
    }

    public xz(int i, @NonNull String str, @NonNull vz vzVar) {
        super(i, str, vzVar);
    }

    @Nullable
    /* renamed from: a */
    public String a(@Nullable String str) {
        if (str == null || str.length() <= mo2635a()) {
            return str;
        }
        String substring = str.substring(0, mo2635a());
        if (this.f2984a.mo2485c()) {
            this.f2984a.mo2482b("\"%s\" %s size exceeded limit of %d characters", mo2636b(), str, Integer.valueOf(mo2635a()));
        }
        return substring;
    }
}
