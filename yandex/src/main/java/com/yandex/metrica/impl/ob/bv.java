package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Base64;
import android.util.SparseArray;

import com.yandex.metrica.i;
import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.cy.C0248a;
import com.yandex.metrica.impl.ob.rh.c.C0824c;
import com.yandex.metrica.impl.ob.rh.c.C0833f;
import com.yandex.metrica.impl.ob.rh.c.b;
import com.yandex.metrica.impl.ob.rh.c.e;
import com.yandex.metrica.impl.ob.rh.c.e.a.C0829a;
import com.yandex.metrica.impl.ob.rh.c.e.a.C0830b;
import com.yandex.metrica.impl.ob.rh.c.e.a.C0830b.C0831a;
import com.yandex.metrica.impl.ob.rh.c.g;
import com.yandex.metrica.impl.ob.rh.d;
import com.yandex.metrica.impl.ob.vq.a;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.bv */
public final class bv {

    /* renamed from: a */
    private static Map<jh, Integer> f424a;

    /* renamed from: B */
    private static SparseArray<jh> f425b;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static Map<aj, Integer> f426c;

    /* renamed from: com.yandex.metrica.impl.ob.bv$a */
    static class C0157a extends C0161e {
        C0157a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String mo500a() {
            return "";
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$B */
    static class C0158b extends c {
        C0158b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return Base64.decode(this.f431b, 0);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$a */
    static class c {

        /* renamed from: u */
        private static final Map<C0058a, Class<?>> f428u;

        /* renamed from: v */
        private static final Map<C0058a, Integer> f429v;

        /* renamed from: a */
        protected String f430a;

        /* renamed from: B */
        protected String f431b;

        /* renamed from: a */
        protected int f432c;

        /* renamed from: d */
        protected int f433d;

        /* renamed from: e */
        protected int f434e;

        /* renamed from: f */
        protected long f435f;

        /* renamed from: g */
        protected String f436g;

        /* renamed from: h */
        protected String f437h;

        /* renamed from: i */
        protected String f438i;

        /* renamed from: j */
        protected Integer f439j;

        /* renamed from: k */
        protected Integer f440k;

        /* renamed from: l */
        protected String f441l;

        /* renamed from: m */
        protected String f442m;

        /* renamed from: n */
        protected int f443n;

        /* renamed from: o */
        protected int f444o;

        /* renamed from: p */
        protected String f445p;

        /* renamed from: q */
        protected String f446q;

        /* renamed from: r */
        protected String f447r;

        /* renamed from: s */
        protected wz f448s;

        /* renamed from: t */
        protected aj f449t;

        static {
            HashMap hashMap = new HashMap();
            hashMap.put(C0058a.EVENT_TYPE_REGULAR, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_SEND_REFERRER, C0167k.class);
            hashMap.put(C0058a.EVENT_TYPE_ALIVE, C0163g.class);
            hashMap.put(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, C0166j.class);
            hashMap.put(C0058a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, C0166j.class);
            hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_USER, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, C0162f.class);
            hashMap.put(C0058a.EVENT_TYPE_IDENTITY, C0165i.class);
            hashMap.put(C0058a.EVENT_TYPE_STATBOX, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_SET_USER_INFO, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_REPORT_USER_INFO, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, C0162f.class);
            hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, C0162f.class);
            hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, C0162f.class);
            hashMap.put(C0058a.EVENT_TYPE_ANR, C0162f.class);
            hashMap.put(C0058a.EVENT_TYPE_START, C0163g.class);
            hashMap.put(C0058a.EVENT_TYPE_CUSTOM_EVENT, C0160d.class);
            hashMap.put(C0058a.EVENT_TYPE_APP_OPEN, C0161e.class);
            hashMap.put(C0058a.EVENT_TYPE_PERMISSIONS, C0157a.class);
            hashMap.put(C0058a.EVENT_TYPE_APP_FEATURES, C0157a.class);
            hashMap.put(C0058a.EVENT_TYPE_SEND_USER_PROFILE, C0158b.class);
            hashMap.put(C0058a.EVENT_TYPE_SEND_REVENUE_EVENT, C0158b.class);
            f428u = Collections.unmodifiableMap(hashMap);
            HashMap hashMap2 = new HashMap();
            hashMap2.put(C0058a.EVENT_TYPE_INIT, Integer.valueOf(1));
            hashMap2.put(C0058a.EVENT_TYPE_REGULAR, Integer.valueOf(4));
            hashMap2.put(C0058a.EVENT_TYPE_SEND_REFERRER, Integer.valueOf(5));
            hashMap2.put(C0058a.EVENT_TYPE_ALIVE, Integer.valueOf(7));
            hashMap2.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED, Integer.valueOf(3));
            hashMap2.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, Integer.valueOf(26));
            hashMap2.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, Integer.valueOf(26));
            hashMap2.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, Integer.valueOf(26));
            hashMap2.put(C0058a.EVENT_TYPE_ANR, Integer.valueOf(25));
            hashMap2.put(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, Integer.valueOf(3));
            hashMap2.put(C0058a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, Integer.valueOf(3));
            hashMap2.put(C0058a.EVENT_TYPE_EXCEPTION_USER, Integer.valueOf(6));
            hashMap2.put(C0058a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, Integer.valueOf(27));
            hashMap2.put(C0058a.EVENT_TYPE_IDENTITY, Integer.valueOf(8));
            hashMap2.put(C0058a.EVENT_TYPE_IDENTITY_LIGHT, Integer.valueOf(28));
            hashMap2.put(C0058a.EVENT_TYPE_STATBOX, Integer.valueOf(11));
            hashMap2.put(C0058a.EVENT_TYPE_SET_USER_INFO, Integer.valueOf(12));
            hashMap2.put(C0058a.EVENT_TYPE_REPORT_USER_INFO, Integer.valueOf(12));
            hashMap2.put(C0058a.EVENT_TYPE_FIRST_ACTIVATION, Integer.valueOf(13));
            hashMap2.put(C0058a.EVENT_TYPE_START, Integer.valueOf(2));
            hashMap2.put(C0058a.EVENT_TYPE_APP_OPEN, Integer.valueOf(16));
            hashMap2.put(C0058a.EVENT_TYPE_APP_UPDATE, Integer.valueOf(17));
            hashMap2.put(C0058a.EVENT_TYPE_PERMISSIONS, Integer.valueOf(18));
            hashMap2.put(C0058a.EVENT_TYPE_APP_FEATURES, Integer.valueOf(19));
            hashMap2.put(C0058a.EVENT_TYPE_SEND_USER_PROFILE, Integer.valueOf(20));
            hashMap2.put(C0058a.EVENT_TYPE_SEND_REVENUE_EVENT, Integer.valueOf(21));
            hashMap2.put(C0058a.EVENT_TYPE_CLEANUP, Integer.valueOf(29));
            f429v = Collections.unmodifiableMap(hashMap2);
        }

        /* renamed from: a */
        static c m767a(int i, boolean z) {
            c cVar;
            C0058a a = C0058a.m298a(i);
            Class a2 = m768a(a, z);
            Integer num = (Integer) f429v.get(a);
            try {
                cVar = (c) a2.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (Throwable th) {
                cVar = new c();
            }
            return cVar.mo506a(num);
        }

        /* renamed from: a */
        private static Class<?> m768a(C0058a aVar, boolean z) {
            switch (aVar) {
                case EVENT_TYPE_INIT:
                case EVENT_TYPE_FIRST_ACTIVATION:
                case EVENT_TYPE_APP_UPDATE:
                    if (z) {
                        return C0161e.class;
                    }
                    return C0164h.class;
                default:
                    return (Class) f428u.get(aVar);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo507a(String str) {
            this.f430a = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public c mo510b(String str) {
            this.f431b = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo502a(int i) {
            this.f432c = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public c mo508b(int i) {
            this.f433d = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo511c(int i) {
            this.f434e = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo503a(long j) {
            this.f435f = j;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo512c(String str) {
            this.f436g = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public c mo515d(String str) {
            this.f438i = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: e */
        public c mo518e(String str) {
            this.f437h = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo506a(Integer num) {
            this.f439j = num;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: f */
        public c mo521f(String str) {
            this.f446q = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public c mo509b(Integer num) {
            this.f440k = num;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: g */
        public c mo522g(String str) {
            this.f441l = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: h */
        public c mo523h(String str) {
            this.f442m = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public c mo514d(int i) {
            this.f443n = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: e */
        public c mo517e(int i) {
            this.f444o = i;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: i */
        public c mo524i(String str) {
            this.f445p = str;
            return this;
        }

        /* renamed from: j */
        public c mo525j(String str) {
            this.f447r = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public c mo505a(wz wzVar) {
            this.f448s = wzVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        /* renamed from: a */
        public c mo504a(@NonNull aj ajVar) {
            this.f449t = ajVar;
            return this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String mo500a() {
            return this.f430a;
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return this.f431b == null ? new byte[0] : cu.m1160c(this.f431b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer mo513c() {
            return this.f439j;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public String mo516d() {
            return this.f441l;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: e */
        public e.a mo519e() {
            e.a aVar = new e.a();
            C0830b a = bv.m746a(this.f444o, this.f445p, this.f438i, this.f437h, this.f446q);
            b d = bv.m762d(this.f436g);
            C0829a f = bv.m764f(this.f442m);
            if (a != null) {
                aVar.f1990h = a;
            }
            if (d != null) {
                aVar.f1989g = d;
            }
            if (mo500a() != null) {
                aVar.f1987e = mo500a();
            }
            if (mo501b() != null) {
                aVar.f1988f = mo501b();
            }
            if (mo516d() != null) {
                aVar.f1991i = mo516d();
            }
            if (f != null) {
                aVar.f1992j = f;
            }
            aVar.f1986d = mo513c().intValue();
            aVar.f1984b = (long) this.f432c;
            aVar.f1998p = (long) this.f433d;
            aVar.f1999q = (long) this.f434e;
            aVar.f1985c = this.f435f;
            aVar.f1993k = this.f443n;
            aVar.f1994l = mo520f();
            aVar.f1995m = bv.m763e(this.f436g);
            aVar.f1996n = this.f447r == null ? new byte[0] : this.f447r.getBytes();
            Integer num = (Integer) bv.f426c.get(this.f449t);
            if (num != null) {
                aVar.f1997o = num.intValue();
            }
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: f */
        public int mo520f() {
            return 0;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$d */
    static class C0160d extends C0161e {
        C0160d() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer mo513c() {
            return this.f440k;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$e */
    static class C0161e extends c {

        /* renamed from: u */
        private wy f450u;

        C0161e() {
            this(new wy(al.m324a().mo284b()));
        }

        C0161e(@NonNull wy wyVar) {
            this.f450u = wyVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return this.f450u.mo2591a(this.f448s).a(super.mo501b());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$f */
    static class C0162f extends c {

        /* renamed from: u */
        private wy f451u;

        C0162f() {
            this(new wy(al.m324a().mo284b()));
        }

        C0162f(@NonNull wy wyVar) {
            this.f451u = wyVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return this.f451u.mo2591a(this.f448s).a(Base64.decode(super.mo501b(), 0));
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$g */
    static class C0163g extends c {
        C0163g() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String mo500a() {
            return "";
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return new byte[0];
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$h */
    static class C0164h extends c {
        C0164h() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return new byte[0];
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$i */
    static class C0165i extends C0161e {
        C0165i() {
        }

        /* renamed from: f */
        public int mo520f() {
            return this.f448s == wz.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER ? 1 : 0;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$j */
    static class C0166j extends c {
        C0166j() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            return cu.m1160c(am.m358c(this.f431b));
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.bv$k */
    static class C0167k extends c {
        C0167k() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public byte[] mo501b() {
            try {
                sj a = sj.m3551a(Base64.decode(this.f431b, 0));
                ri riVar = new ri();
                riVar.f2029b = a.installReferrer == null ? new byte[0] : a.installReferrer.getBytes();
                riVar.f2031d = a.eferrerClickTimestampSeconds;
                riVar.f2030c = a.installBeginTimestampSeconds;
                return com.yandex.metrica.impl.ob.e.m1395a((com.yandex.metrica.impl.ob.e) riVar);
            } catch (com.yandex.metrica.impl.ob.d e) {
                return new byte[0];
            }
        }
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(jh.FOREGROUND, Integer.valueOf(0));
        hashMap.put(jh.BACKGROUND, Integer.valueOf(1));
        f424a = Collections.unmodifiableMap(hashMap);
        SparseArray<jh> sparseArray = new SparseArray<>();
        sparseArray.put(0, jh.FOREGROUND);
        sparseArray.put(1, jh.BACKGROUND);
        f425b = sparseArray;
        HashMap hashMap2 = new HashMap();
        hashMap2.put(aj.FIRST_OCCURRENCE, Integer.valueOf(1));
        hashMap2.put(aj.NON_FIRST_OCCURENCE, Integer.valueOf(0));
        hashMap2.put(aj.UNKNOWN, Integer.valueOf(-1));
        f426c = Collections.unmodifiableMap(hashMap2);
    }

    /* renamed from: a */
    public static g a(ContentValues contentValues) {
        return m751a(contentValues.getAsLong("start_time"), contentValues.getAsLong("server_time_offset"), contentValues.getAsBoolean("obtained_before_first_sync"));
    }

    /* renamed from: a */
    public static C0833f m748a(vb vbVar) {
        C0833f fVar = new C0833f();
        if (vbVar.mo2461a() != null) {
            fVar.f2014b = vbVar.mo2461a().intValue();
        }
        if (vbVar.mo2462b() != null) {
            fVar.f2015c = vbVar.mo2462b().intValue();
        }
        if (!TextUtils.isEmpty(vbVar.mo2464d())) {
            fVar.f2016d = vbVar.mo2464d();
        }
        fVar.f2017e = vbVar.mo2463c();
        if (!TextUtils.isEmpty(vbVar.mo2465e())) {
            fVar.f2018f = vbVar.mo2465e();
        }
        return fVar;
    }

    /* renamed from: a */
    public static jh a(int i) {
        return (jh) f425b.get(i);
    }

    /* renamed from: a */
    public static d[] m756a(String str) {
        boolean z = false;
        try {
            return m757a(new JSONArray(str));
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    public static d[] m757a(JSONArray jSONArray) {
        d[] dVarArr = null;
        try {
            dVarArr = new d[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                dVarArr[i] = m752a(jSONArray.getJSONObject(i));
            }
        } catch (Throwable th) {
        }
        return dVarArr;
    }

    /* renamed from: a */
    public static d m752a(JSONObject jSONObject) throws JSONException {
        try {
            d dVar = new d();
            dVar.f2024b = jSONObject.getString("mac");
            dVar.f2025c = jSONObject.getInt("signal_strength");
            dVar.f2026d = jSONObject.getString("ssid");
            dVar.f2027e = jSONObject.optBoolean("is_connected");
            dVar.f2028f = jSONObject.optLong("last_visible_offset_seconds", 0);
            return dVar;
        } catch (Throwable th) {
            d dVar2 = new d();
            dVar2.f2024b = jSONObject.getString("mac");
            return dVar2;
        }
    }

    /* renamed from: B */
    static C0831a m759b(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                C0831a aVar = new C0831a();
                aVar.f2008b = jSONObject.optString("ssid");
                switch (jSONObject.optInt("state", -1)) {
                    case 0:
                    case 1:
                    case 2:
                    case 4:
                        aVar.f2009c = 1;
                        return aVar;
                    case 3:
                        aVar.f2009c = 2;
                        return aVar;
                    default:
                        return aVar;
                }
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* renamed from: a */
    public static g m749a(long j) {
        g gVar = new g();
        gVar.f2019b = j;
        gVar.f2020c = wi.m4358a(j);
        return gVar;
    }

    /* renamed from: a */
    public static g m751a(Long l, Long l2, Boolean bool) {
        g a = m749a(l.longValue());
        if (l2 != null) {
            a.f2021d = l2.longValue();
        }
        if (bool != null) {
            a.f2022e = bool.booleanValue();
        }
        return a;
    }

    /* renamed from: a */
    public static e.b a(String str, int i, g gVar) {
        e.b bVar = new e.b();
        bVar.f2010b = gVar;
        bVar.f2011c = str;
        bVar.d = i;
        return bVar;
    }

    /* renamed from: a */
    static int a(jh jhVar) {
        return ((Integer) f424a.get(jhVar)).intValue();
    }

    /* renamed from: a */
    public static rh.a[] m761c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return m760b(new JSONArray(str));
            }
            return null;
        } catch (Throwable th) {
            return null;
        }
    }

    @Nullable
    /* renamed from: B */
    public static rh.a[] m760b(JSONArray jSONArray) {
        rh.a[] aVarArr = null;
        try {
            aVarArr = new rh.a[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (jSONObject != null) {
                    aVarArr[i] = m758b(jSONObject);
                }
            }
        } catch (Throwable th) {
        }
        return aVarArr;
    }

    @VisibleForTesting
    @NonNull
    /* renamed from: B */
    static rh.a m758b(JSONObject jSONObject) {
        rh.a aVar = new rh.a();
        if (jSONObject.has("signal_strength")) {
            int optInt = jSONObject.optInt("signal_strength", aVar.f1910c);
            if (optInt != -1) {
                aVar.f1910c = optInt;
            }
        }
        if (jSONObject.has("cell_id")) {
            aVar.f1909b = jSONObject.optInt("cell_id", aVar.f1909b);
        }
        if (jSONObject.has("lac")) {
            aVar.f1911d = jSONObject.optInt("lac", aVar.f1911d);
        }
        if (jSONObject.has("country_code")) {
            aVar.f1912e = jSONObject.optInt("country_code", aVar.f1912e);
        }
        if (jSONObject.has("operator_id")) {
            aVar.f1913f = jSONObject.optInt("operator_id", aVar.f1913f);
        }
        if (jSONObject.has("operator_name")) {
            aVar.f1914g = jSONObject.optString("operator_name", aVar.f1914g);
        }
        if (jSONObject.has("is_connected")) {
            aVar.f1915h = jSONObject.optBoolean("is_connected", aVar.f1915h);
        }
        aVar.f1916i = jSONObject.optInt("cell_type", 0);
        if (jSONObject.has("pci")) {
            aVar.f1917j = jSONObject.optInt("pci", aVar.f1917j);
        }
        if (jSONObject.has("last_visible_time_offset")) {
            aVar.f1918k = jSONObject.optLong("last_visible_time_offset", aVar.f1918k);
        }
        return aVar;
    }

    public static b m762d(String var0) {

        com.yandex.metrica.impl.ob.rh.c.b var1 = null;

        try {
            if (!TextUtils.isEmpty(var0)) {
                com.yandex.metrica.impl.ob.vq.a var2 = new com.yandex.metrica.impl.ob.vq.a(var0);
                if (var2.c("lon") && var2.c("lat")) {
                    var1 = new com.yandex.metrica.impl.ob.rh.c.b();
                    var1.c = var2.getDouble("lon");
                    var1.b = var2.getDouble("lat");
                    var1.h = var2.optInt("altitude");
                    var1.f = var2.optInt("direction");
                    var1.e = var2.optInt("precision");
                    var1.g = var2.optInt("speed");
                    var1.d = var2.optLong("timestamp") / 1000L;
                    if (var2.c("provider")) {
                        String var3 = var2.a_("provider");
                        if ("gps".equals(var3)) {
                            var1.i = 1;
                        } else if ("network".equals(var3)) {
                            var1.i = 2;
                        }
                    }

                    if (var2.c("original_provider")) {
                        var1.j = var2.a_("original_provider");
                    }
                }
            }
        } catch (Throwable var4) {
        }

        return var1;
          }

    @VisibleForTesting
    /* renamed from: e */
    static int m763e(String str) {
        try {
            return new ko().mo1154a(Boolean.valueOf(new a(str).getBoolean("enabled"))).intValue();
        } catch (Throwable th) {
            return -1;
        }
    }

    /* renamed from: a */
    public static C0830b m746a(int i, String str, String str2, String str3, String str4) {
        C0830b bVar = new C0830b();
        bVar.f2005d = i;
        if (str != null) {
            bVar.f2006e = str;
        }
        rh.a[] c = m761c(str3);
        if (c != null) {
            bVar.f2003b = c;
        }
        bVar.f2004c = m756a(str2);
        if (!TextUtils.isEmpty(str4)) {
            bVar.f2007f = m759b(str4);
        }
        return bVar;
    }

    /* renamed from: f */
    public static C0829a m764f(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                i a = wj.m4363a(str);
                C0829a aVar = new C0829a();
                aVar.f2000b = a.mo179a();
                if (!TextUtils.isEmpty(a.mo182b())) {
                    aVar.f2001c = a.mo182b();
                }
                if (cx.m1193a(a.mo184c())) {
                    return aVar;
                }
                aVar.f2002d = vq.m4225b(a.mo184c());
                return aVar;
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* renamed from: a */
    public static void m754a(e eVar) {
    }

    /* renamed from: a */
    public static C0824c[] m755a(Context context) {
        List b = cy.m1204a(context).mo668b();
        if (cx.a((Collection) b)) {
            return null;
        }
        C0824c[] cVarArr = new C0824c[b.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= b.size()) {
                return cVarArr;
            }
            C0824c cVar = new C0824c();
            C0248a aVar = (C0248a) b.get(i2);
            cVar.f1960b = aVar.f666a;
            cVar.f1961c = aVar.f667b;
            cVarArr[i2] = cVar;
            i = i2 + 1;
        }
    }
}
