package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rm.C0846a;
import com.yandex.metrica.impl.ob.ro.C0852a;
import com.yandex.metrica.impl.ob.rp.a;
import com.yandex.metrica.impl.ob.rq.C0859a;

/* renamed from: com.yandex.metrica.impl.ob.no */
public class no {

    private static final String transformation = "AES/CBC/PKCS5Padding";
    /* renamed from: a */
    private wq f1527a;

    public no(@NonNull Context context) {
        this(new wq(context));
    }

    @VisibleForTesting
    no(wq wqVar) {
        this.f1527a = wqVar;
    }

    /* renamed from: a */
    public me<rr.a> mo1497a() {
        return new mc(new mj(), new Crypto(transformation, this.f1527a.getPkgMd5Bts(), this.f1527a.getPkgReverseMd5Bts()));
    }

    @NonNull
    /* renamed from: B */
    public me<C0852a> mo1498b() {
        return new mc(new md(), new Crypto(transformation, this.f1527a.getPkgMd5Bts(), this.f1527a.getPkgReverseMd5Bts()));
    }

    @NonNull
    /* renamed from: a */
    public me<C0846a> mo1499c() {
        return new mc(new ma(), new Crypto(transformation, this.f1527a.getPkgMd5Bts(), this.f1527a.getPkgReverseMd5Bts()));
    }

    @NonNull
    /* renamed from: d */
    public me<C0859a> mo1500d() {
        return new mc(new mi(), new Crypto(transformation, this.f1527a.getPkgMd5Bts(), this.f1527a.getPkgReverseMd5Bts()));
    }

    /* renamed from: e */
    public me<a> mo1501e() {
        return new mc(new mh(), new Crypto(transformation, this.f1527a.getPkgMd5Bts(), this.f1527a.getPkgReverseMd5Bts()));
    }
}
