package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.ac */
public class ac<C extends ge> extends cv<C> {
    @NonNull

    /* renamed from: d */
    private final gk f147d;
    @NonNull

    /* renamed from: e */
    private final lw f148e;

    /* renamed from: f */
    private boolean f149f = false;

    public ac(@NonNull C c, @NonNull uq uqVar, @NonNull bl blVar, @NonNull gk gkVar, @NonNull lw lwVar) {
        super(c, uqVar, blVar);
        this.f147d = gkVar;
        this.f148e = lwVar;
    }

    /* renamed from: a */
    public void mo253a(@NonNull w wVar) {
        if (!this.f149f) {
            super.mo660f();
            this.f639b.a((bo) new gg((gj) mo661g(), wVar, this.f147d, this.f148e));
        }
    }

    public void close() {
        this.f149f = true;
    }
}
