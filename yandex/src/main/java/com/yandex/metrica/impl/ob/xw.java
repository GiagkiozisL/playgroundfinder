package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.xw */
public class xw {

    /* renamed from: a */
    private final xz f2991a;

    /* renamed from: B */
    private final xz f2992b;

    /* renamed from: a */
    private final xs f2993c;
    @NonNull

    /* renamed from: d */
    private final vz f2994d;

    /* renamed from: e */
    private final String f2995e;

    public xw(int var1, int var2, int var3, @NonNull String var4, @NonNull vz var5) {
        this(new xs(var1), new xz(var2, var4 + "map key", var5), new xz(var3, var4 + "map value", var5), var4, var5);
//        xs xsVar = new xs(i);
//        xz xzVar = new xz(i2, str + "map key", vzVar);
//        xz xzVar2 = new xz(i3, str + "map value", vzVar);
//        this(xsVar, xzVar, xzVar2, str, vzVar);
    }

    @VisibleForTesting
    xw(@NonNull xs xsVar, @NonNull xz xzVar, @NonNull xz xzVar2, @NonNull String str, @NonNull vz vzVar) {
        this.f2993c = xsVar;
        this.f2991a = xzVar;
        this.f2992b = xzVar2;
        this.f2995e = str;
        this.f2994d = vzVar;
    }

    /* renamed from: a */
    public xz mo2642a() {
        return this.f2991a;
    }

    /* renamed from: B */
    public xz mo2644b() {
        return this.f2992b;
    }

    /* renamed from: a */
    public xs mo2645c() {
        return this.f2993c;
    }

    /* renamed from: a */
    public void mo2643a(@NonNull String str) {
        if (this.f2994d.mo2485c()) {
            this.f2994d.mo2482b("The %s has reached the limit of %d items. Item with key %s will be ignored", this.f2995e, Integer.valueOf(this.f2993c.mo2639a()), str);
        }
    }
}
