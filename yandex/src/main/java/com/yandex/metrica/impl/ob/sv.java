package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.sv */
public class sv implements sw<gi> {
    /* renamed from: a */
    public void mo2207a(@NonNull Builder builder, @NonNull gi giVar) {
        builder.appendPath("diagnostic");
        builder.appendQueryParameter("deviceid", giVar.getDeviceId());
        builder.appendQueryParameter("uuid", giVar.getUUId());
        builder.appendQueryParameter("app_platform", giVar.getAppPlatform());
        builder.appendQueryParameter("analytics_sdk_version_name", giVar.getSDKVersionName());
        builder.appendQueryParameter("analytics_sdk_build_number", giVar.getSDKVersionNumber());
        builder.appendQueryParameter("analytics_sdk_build_type", giVar.getSDKBuildType());
        builder.appendQueryParameter("app_version_name", giVar.getAppVersionName());
        builder.appendQueryParameter("app_build_number", giVar.getAppVersionCode());
        builder.appendQueryParameter("model", giVar.getModel());
        builder.appendQueryParameter("manufacturer", giVar.getManufacturer());
        builder.appendQueryParameter("os_version", giVar.getOsVersion());
        builder.appendQueryParameter("os_api_level", String.valueOf(giVar.getApiLevel()));
        builder.appendQueryParameter("screen_width", String.valueOf(giVar.getScreenWidth()));
        builder.appendQueryParameter("screen_height", String.valueOf(giVar.getScreenHeight()));
        builder.appendQueryParameter("screen_dpi", String.valueOf(giVar.getScreenDpi()));
        builder.appendQueryParameter("scalefactor", String.valueOf(giVar.getScaleFactore()));
        builder.appendQueryParameter("locale", giVar.A());
        builder.appendQueryParameter("device_type", giVar.getDeviceType());
        builder.appendQueryParameter("app_id", giVar.getAppId());
        builder.appendQueryParameter("api_key_128", giVar.getApiKey128());
        builder.appendQueryParameter("app_debuggable", "0"); //giVar.mo2126E());
        builder.appendQueryParameter("is_rooted", giVar.getIsRooted());
        builder.appendQueryParameter("app_framework", giVar.getAppFramework());
    }
}
