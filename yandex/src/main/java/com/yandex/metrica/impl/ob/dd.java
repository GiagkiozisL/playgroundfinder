package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.dd */
public class dd {
    @NonNull

    /* renamed from: a */
    private final Context f696a;

    public dd(@NonNull Context context) {
        this.f696a = context;
    }

    @Nullable
    /* renamed from: a */
    @SuppressWarnings({"WrongConstant", "MissingPermission"})
    public BluetoothLeScanner mo692a() {
        BluetoothManager bluetoothManager = (BluetoothManager) this.f696a.getSystemService("bluetooth");
        if (bluetoothManager == null) {
            return null;
        }
        BluetoothAdapter bluetoothAdapter = (BluetoothAdapter) cx.a((wo<BluetoothManager, BluetoothAdapter>) new wo<BluetoothManager, BluetoothAdapter>() {
            /* renamed from: a */
            public BluetoothAdapter a(BluetoothManager bluetoothManager) throws Throwable {
                return bluetoothManager.getAdapter();
            }
        }, bluetoothManager, "getting adapter", "BluetoothManager");
        if (bluetoothAdapter == null) {
            return null;
        }
        if (!bluetoothAdapter.isEnabled()) {
            return null;
        }
        return (BluetoothLeScanner) cx.a((wo<BluetoothAdapter, BluetoothLeScanner>) new wo<BluetoothAdapter, BluetoothLeScanner>() {
            /* renamed from: a */
            public BluetoothLeScanner a(BluetoothAdapter bluetoothAdapter) throws Throwable {
                return bluetoothAdapter.getBluetoothLeScanner();
            }
        }, bluetoothAdapter, "Get bluetooth LE scanner", "BluetoothAdapter");
    }
}
