package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.yandex.metrica.impl.ob.nz */
public class nz implements nv, nw {
    @NonNull

    /* renamed from: a */
    private final fe f1557a;

    /* renamed from: B */
    private AtomicLong f1558b;

    public nz(@NonNull kz kzVar, @NonNull fe feVar) {
        this.f1557a = feVar;
        this.f1558b = new AtomicLong(kzVar.mo1205b());
        kzVar.mo1202a((nw) this);
    }

    /* renamed from: a */
    public boolean a() {
        return this.f1558b.get() >= ((long) ((st) this.f1557a.mo2124d()).mo2144U());
    }

    /* renamed from: a */
    public void a(@NonNull List<Integer> list) {
        this.f1558b.addAndGet((long) list.size());
    }

    /* renamed from: B */
    public void b(@NonNull List<Integer> list) {
        this.f1558b.addAndGet((long) (-list.size()));
    }
}
