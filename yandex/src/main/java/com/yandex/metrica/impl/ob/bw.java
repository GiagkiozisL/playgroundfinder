package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.ch.C0197a;

/* renamed from: com.yandex.metrica.impl.ob.bw */
public class bw {
    @NonNull
    /* renamed from: a */
    public static String m804a(int i) {
        C0197a a = ch.m941a(i);
        return a != null ? a.f543a : ch.f541a.f543a;
    }
}
