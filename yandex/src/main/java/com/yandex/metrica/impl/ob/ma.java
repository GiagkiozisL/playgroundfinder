package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rm.C0846a;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.ma */
public class ma extends mb<C0846a> {
    @NonNull
    /* renamed from: a */
    public C0846a b(@NonNull byte[] bArr) throws IOException {
        return C0846a.m3209a(bArr);
    }

    @NonNull
    /* renamed from: a */
    public C0846a c() {
        return new C0846a();
    }
}
