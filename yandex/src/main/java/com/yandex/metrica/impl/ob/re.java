package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.SparseArray;

import com.yandex.metrica.impl.ob.rl.a;
import com.yandex.metrica.impl.ob.rl.a.Aa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: com.yandex.metrica.impl.ob.re */
public class re {

    /* renamed from: a */
    private static final int[] a = {0, 1, 2, 3};

    /* renamed from: B */
    private final SparseArray<HashMap<String, Aa>> b;

    /* renamed from: a */
    private int c;

    public re() {
        this(a);
    }

    @VisibleForTesting
    re(int[] iArr) {
        this.b = new SparseArray<>();
        this.c = 0;
        for (int put : iArr) {
            this.b.put(put, new HashMap());
        }
    }

    @Nullable
    /* renamed from: a */
    public Aa mo1770a(int i, @NonNull String str) {
        return (Aa) ((HashMap) this.b.get(i)).get(str);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1771a(@NonNull rl.a.Aa aVar) {
        ((HashMap) this.b.get(aVar.f2047c)).put(new String(aVar.f2046b), aVar);
    }

    /* renamed from: a */
    public int mo1769a() {
        return this.c;
    }

    /* renamed from: B */
    public void mo1772b() {
        this.c++;
    }

    @NonNull
    /* renamed from: a */
    public rl.a mo1773c() {
        rl.a var1 = new a();
        ArrayList var2 = new ArrayList();

        for(int var3 = 0; var3 < this.b.size(); ++var3) {
            HashMap var4 = (HashMap)this.b.get(this.b.keyAt(var3));
            Iterator var5 = var4.values().iterator();

            while(var5.hasNext()) {
                com.yandex.metrica.impl.ob.a var6 = (com.yandex.metrica.impl.ob.a)var5.next();
                var2.add(var6);
            }
        }

        var1.b = (Aa[])var2.toArray(new com.yandex.metrica.impl.ob.a[var2.size()]);
        return var1;
//        a aVar = new a();
//        ArrayList arrayList = new ArrayList();
//        int i = 0;
//        while (true) {
//            int i2 = i;
//            if (i2 < this.b.size()) {
//                for (Aa add : ((HashMap) this.b.get(this.b.keyAt(i2))).values()) {
//                    arrayList.add(add);
//                }
//                i = i2 + 1;
//            } else {
//                aVar.b = (Aa[]) arrayList.toArray(new Aa[arrayList.size()]);
//                return aVar;
//            }
//        }
    }
}
