package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.rl.a.Aa;

/* renamed from: com.yandex.metrica.impl.ob.qv */
public class qv implements qn {
    @NonNull

    /* renamed from: a */
    private final xs f1859a;

    public qv(@NonNull xs xsVar) {
        this.f1859a = xsVar;
    }

    @Nullable
    /* renamed from: a */
    public Aa a(@NonNull re reVar, @NonNull Aa aVar) {
        if (reVar.mo1769a() == this.f1859a.mo2639a()) {
            if (reVar.mo1770a(aVar.f2047c, new String(aVar.f2046b)) != null) {
                reVar.mo1771a(aVar);
            }
        } else if (reVar.mo1769a() < this.f1859a.mo2639a()) {
            reVar.mo1771a(aVar);
            reVar.mo1772b();
        }
        return aVar;
    }
}
