package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.f;
import com.yandex.metrica.g;
import com.yandex.metrica.j;
import com.yandex.metrica.profile.UserProfile;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.sb */
public final class sb extends ru<rx<se>> {

    /* renamed from: B */
    private final sd f2326b;

    /* renamed from: a */
    private final sc f2327c;

    public sb(@NonNull xh xhVar) {
        this(new sa(), xhVar, new ry<rx<se>>(xhVar) {
            /* access modifiers changed from: protected */
            /* renamed from: a */
            public rx<se> mo1959a(@NonNull xh xhVar, @NonNull Context context, @NonNull String str) {
                return new rx<>(xhVar, context, str, new se());
            }
        }, new sd(), new sc());
    }

    private sb(@NonNull sa saVar, @NonNull xh xhVar, @NonNull ry<rx<se>> ryVar, @NonNull sd sdVar, @NonNull sc scVar) {
        this(saVar, xhVar, ryVar, sdVar, scVar, new rt(saVar), new g(saVar));
    }

    @VisibleForTesting
    sb(@NonNull sa saVar, @NonNull xh xhVar, @NonNull ry<rx<se>> ryVar, @NonNull sd sdVar, @NonNull sc scVar, @NonNull rt rtVar, @NonNull g gVar) {
        super(saVar, xhVar, ryVar, rtVar, gVar);
        this.f2327c = scVar;
        this.f2326b = sdVar;
    }

    /* renamed from: a */
    public void mo2003a(@NonNull final Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        this.f2326b.mo2052a(context, yandexMetricaConfig);
        final j a = this.f2327c.mo2048a(j.m4546a(yandexMetricaConfig));
        this.f2244a.mo159a(context, (YandexMetricaConfig) a);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                Log.d(Constant.RUS_TAG, "sb$mo2003a run()");
                sb.this.mo1930c().mo1993b(context, j.m4546a(a));
            }
        });
        mo1930c().mo1992b();
    }

    /* renamed from: e */
    public void mo2022e() {
        mo1931d().a();
        this.f2326b.sendEventsBuffer();
        this.f2244a.mo156a();
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().sendEventsBuffer();
            }
        });
    }

    /* renamed from: a */
    public void mo2000a(@Nullable final Activity activity) {
        mo1931d().a();
        this.f2326b.resumeSession();
        this.f2244a.mo157a(activity);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo332b(activity);
            }
        });
    }

    /* renamed from: B */
    public void mo2016b(@Nullable final Activity activity) {
        mo1931d().a();
        this.f2326b.pauseSession();
        this.f2244a.mo173b(activity);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo333c(activity);
            }
        });
    }

    /* renamed from: a */
    public void mo2001a(@NonNull final Application application) {
        mo1931d().a();
        this.f2326b.mo2050a(application);
        this.f2244a.mo158a(application);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo330a(application, sb.this.mo1928a());
            }
        });
    }

    /* renamed from: a */
    public void mo2010a(@NonNull final String str) {
        mo1931d().a();
        this.f2326b.reportEvent(str);
        this.f2244a.mo167a(str);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().reportEvent(str);
            }
        });
    }

    /* renamed from: a */
    public void mo2011a(@NonNull final String str, @Nullable final String str2) {
        mo1931d().a();
        this.f2326b.reportEvent(str, str2);
        this.f2244a.mo168a(str, str2);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().reportEvent(str, str2);
            }
        });
    }

    /* renamed from: a */
    public void mo2013a(@NonNull final String str, @Nullable Map<String, Object> map) {
        final List a;
        mo1931d().a();
        this.f2326b.reportEvent(str, map);
        this.f2244a.mo170a(str, map);
        if (map == null) {
            a = null;
        } else {
            a = m3495a(map);
        }
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                if (a != null) {
                    Iterator var2 = a.iterator();

                    while(var2.hasNext()) {
                        Entry var3x = (Entry)var2.next();
                        linkedHashMap.put(var3x.getKey(), var3x.getValue());
                    }
//                    for (Entry entry : a) {
//                        linkedHashMap.put(entry.getKey(), entry.getValue());
//                    }
                }
                sb.this.mo1930c().mo1997e().reportEvent(str, (Map<String, Object>) linkedHashMap);
            }
        });
    }

    /* renamed from: a */
    public void mo2012a(@NonNull final String str, @Nullable final Throwable th) {
        mo1931d().a();
        this.f2326b.reportError(str, th);
        this.f2244a.mo169a(str, th);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().reportError(str, th);
            }
        });
    }

    /* renamed from: a */
    public void mo2014a(@NonNull final Throwable th) {
        mo1931d().a();
        this.f2326b.reportUnhandledException(th);
        this.f2244a.mo171a(th);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().reportUnhandledException(th);
            }
        });
    }

    /* renamed from: B */
    public void mo2018b(@NonNull final String seed) {
        mo1931d().a();
        this.f2326b.mo2058a(seed);
        this.f2244a.mo177c(seed);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo1461c(seed);
            }
        });
    }

    /* renamed from: a */
    public void mo2019c(@NonNull final Activity activity) {
        mo1931d().a();
        this.f2326b.mo2049a(activity);
        this.f2244a.mo176c(activity);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo329a(activity);
            }
        });
    }

    /* renamed from: a */
    public void mo2020c(@NonNull final String str) {
        mo1931d().a();
        this.f2326b.mo2061b(str);
        this.f2244a.mo178d(str);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo335e(str);
            }
        });
    }

    @Deprecated
    /* renamed from: d */
    public void mo2021d(@NonNull final String str) {
        mo1931d().a();
        this.f2326b.mo2062c(str);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().mo336f(str);
            }
        });
    }

    /* renamed from: a */
    public void mo2005a(@Nullable final Location location) {
        this.f2326b.mo2055a(location);
        this.f2244a.mo162a(location);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1990a(location);
            }
        });
    }

    /* renamed from: a */
    public void mo2015a(final boolean z) {
        this.f2326b.mo2059a(z);
        this.f2244a.mo172a(z);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1991a(z);
            }
        });
    }

    /* renamed from: a */
    public void mo2004a(@NonNull final Context context, final boolean z) {
        this.f2326b.mo2054a(context, z);
        this.f2244a.mo161a(context, z);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1989a(context, z);
            }
        });
    }

    /* renamed from: B */
    public void mo2017b(@NonNull final Context context, final boolean z) {
        this.f2326b.mo2060b(context, z);
        this.f2244a.mo174b(context, z);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1994b(context, z);
            }
        });
    }

    /* renamed from: e */
    public void mo2023e(@Nullable final String str) {
        mo1931d().a();
        this.f2326b.setUserProfileID(str);
        this.f2244a.mo175b(str);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().setUserProfileID(str);
            }
        });
    }

    /* renamed from: a */
    public void mo2009a(@NonNull final UserProfile userProfile) {
        mo1931d().a();
        this.f2326b.reportUserProfile(userProfile);
        this.f2244a.mo166a(userProfile);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().reportUserProfile(userProfile);
            }
        });
    }

    /* renamed from: a */
    public void mo2008a(@NonNull final Revenue revenue) {
        mo1931d().a();
        this.f2326b.reportRevenue(revenue);
        this.f2244a.mo165a(revenue);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1997e().reportRevenue(revenue);
            }
        });
    }

    /* renamed from: a */
    public void mo2007a(@NonNull final DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        mo1931d().a();
        this.f2326b.mo2057a(deferredDeeplinkParametersListener);
        this.f2244a.mo164a(deferredDeeplinkParametersListener);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1998f().mo679a(deferredDeeplinkParametersListener);
            }
        });
    }

    /* renamed from: a */
    public void mo2006a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        mo1931d().a();
        this.f2326b.mo2056a(appMetricaDeviceIDListener);
        this.f2244a.mo163a(appMetricaDeviceIDListener);
        mo1928a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.mo1930c().mo1998f().mo678a(appMetricaDeviceIDListener);
            }
        });
    }

    @NonNull
    /* renamed from: a */
    public IReporter mo1999a(@NonNull Context context, @NonNull String str) {
        this.f2326b.mo2053a(context, str);
        return mo1929b().mo1958a(context, str);
    }

    /* renamed from: a */
    public void mo2002a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        this.f2326b.mo2051a(context, reporterConfig);
        f a = this.f2327c.mo2047a(f.m122a(reporterConfig));
        this.f2244a.mo160a(context, a);
        mo1929b().mo1957a(context, (ReporterConfig) a);
    }

    @NonNull
    /* renamed from: a */
    private <K, V> List<Entry<K, V>> m3495a(@NonNull Map<K, V> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Entry simpleEntry : map.entrySet()) {
            arrayList.add(new SimpleEntry(simpleEntry));
        }
        return arrayList;
    }
}
