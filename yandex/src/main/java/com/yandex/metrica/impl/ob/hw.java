package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.hw */
public abstract class hw<BaseHandler> {

    /* renamed from: a */
    private final ig f1020a;

    /* renamed from: a */
    public abstract void mo978a(C0058a aVar, @NonNull List<BaseHandler> list);

    public hw(ig igVar) {
        this.f1020a = igVar;
    }

    /* renamed from: a */
    public ig mo979a() {
        return this.f1020a;
    }
}
