package com.yandex.metrica.impl.ob;

import android.os.ParcelUuid;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.tt */
public class tt {
    @NonNull

    /* renamed from: a */
    public final b a;
    @NonNull

    /* renamed from: B */
    public final List<tt.a> b;

    /* renamed from: a */
    public final long c;

    /* renamed from: d */
    public final long d;

    /* renamed from: com.yandex.metrica.impl.ob.tt$a */
    public static class a {
        @Nullable

        /* renamed from: a */
        public final String f2619a;
        @Nullable

        /* renamed from: B */
        public final String f2620b;
        @Nullable

        /* renamed from: a */
        public final C1041a f2621c;
        @Nullable

        /* renamed from: d */
        public final C1042b f2622d;
        @Nullable

        /* renamed from: e */
        public final C1043c f2623e;

        /* renamed from: com.yandex.metrica.impl.ob.tt$a$a */
        public static class C1041a {

            /* renamed from: a */
            public final int f2624a;
            @Nullable

            /* renamed from: B */
            public final byte[] f2625b;
            @Nullable

            /* renamed from: a */
            public final byte[] f2626c;

            public C1041a(int i, @Nullable byte[] bArr, @Nullable byte[] bArr2) {
                this.f2624a = i;
                this.f2625b = bArr;
                this.f2626c = bArr2;
            }

            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }
                C1041a aVar = (C1041a) o;
                if (this.f2624a != aVar.f2624a || !Arrays.equals(this.f2625b, aVar.f2625b)) {
                    return false;
                }
                return Arrays.equals(this.f2626c, aVar.f2626c);
            }

            public int hashCode() {
                return (((this.f2624a * 31) + Arrays.hashCode(this.f2625b)) * 31) + Arrays.hashCode(this.f2626c);
            }

            public String toString() {
                return "ManufacturerData{manufacturerId=" + this.f2624a + ", data=" + Arrays.toString(this.f2625b) + ", dataMask=" + Arrays.toString(this.f2626c) + '}';
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.tt$a$B */
        public static class C1042b {
            @NonNull

            /* renamed from: a */
            public final ParcelUuid f2627a;
            @Nullable

            /* renamed from: B */
            public final byte[] f2628b;
            @Nullable

            /* renamed from: a */
            public final byte[] f2629c;

            public C1042b(@NonNull String str, @Nullable byte[] bArr, @Nullable byte[] bArr2) {
                this.f2627a = ParcelUuid.fromString(str);
                this.f2628b = bArr;
                this.f2629c = bArr2;
            }

            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }
                C1042b bVar = (C1042b) o;
                if (!this.f2627a.equals(bVar.f2627a) || !Arrays.equals(this.f2628b, bVar.f2628b)) {
                    return false;
                }
                return Arrays.equals(this.f2629c, bVar.f2629c);
            }

            public int hashCode() {
                return (((this.f2627a.hashCode() * 31) + Arrays.hashCode(this.f2628b)) * 31) + Arrays.hashCode(this.f2629c);
            }

            public String toString() {
                return "ServiceData{uuid=" + this.f2627a + ", data=" + Arrays.toString(this.f2628b) + ", dataMask=" + Arrays.toString(this.f2629c) + '}';
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.tt$a$a */
        public static class C1043c {
            @NonNull

            /* renamed from: a */
            public final ParcelUuid f2630a;
            @Nullable

            /* renamed from: B */
            public final ParcelUuid f2631b;

            public C1043c(@NonNull ParcelUuid parcelUuid, @Nullable ParcelUuid parcelUuid2) {
                this.f2630a = parcelUuid;
                this.f2631b = parcelUuid2;
            }

            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }
                C1043c cVar = (C1043c) o;
                if (!this.f2630a.equals(cVar.f2630a)) {
                    return false;
                }
                if (this.f2631b != null) {
                    return this.f2631b.equals(cVar.f2631b);
                }
                if (cVar.f2631b != null) {
                    return false;
                }
                return true;
            }

            public int hashCode() {
                return (this.f2631b != null ? this.f2631b.hashCode() : 0) + (this.f2630a.hashCode() * 31);
            }

            public String toString() {
                return "ServiceUuid{uuid=" + this.f2630a + ", uuidMask=" + this.f2631b + '}';
            }
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable C1041a aVar, @Nullable C1042b bVar, @Nullable C1043c cVar) {
            this.f2619a = str;
            this.f2620b = str2;
            this.f2621c = aVar;
            this.f2622d = bVar;
            this.f2623e = cVar;
        }

        public boolean equals(Object o) {
            boolean z = true;
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            tt.a aVar = (tt.a) o;
            if (this.f2619a != null) {
                if (!this.f2619a.equals(aVar.f2619a)) {
                    return false;
                }
            } else if (aVar.f2619a != null) {
                return false;
            }
            if (this.f2620b != null) {
                if (!this.f2620b.equals(aVar.f2620b)) {
                    return false;
                }
            } else if (aVar.f2620b != null) {
                return false;
            }
            if (this.f2621c != null) {
                if (!this.f2621c.equals(aVar.f2621c)) {
                    return false;
                }
            } else if (aVar.f2621c != null) {
                return false;
            }
            if (this.f2622d != null) {
                if (!this.f2622d.equals(aVar.f2622d)) {
                    return false;
                }
            } else if (aVar.f2622d != null) {
                return false;
            }
            if (this.f2623e != null) {
                z = this.f2623e.equals(aVar.f2623e);
            } else if (aVar.f2623e != null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i;
            int i2;
            int i3;
            int i4 = 0;
            int hashCode = (this.f2619a != null ? this.f2619a.hashCode() : 0) * 31;
            if (this.f2620b != null) {
                i = this.f2620b.hashCode();
            } else {
                i = 0;
            }
            int i5 = (i + hashCode) * 31;
            if (this.f2621c != null) {
                i2 = this.f2621c.hashCode();
            } else {
                i2 = 0;
            }
            int i6 = (i2 + i5) * 31;
            if (this.f2622d != null) {
                i3 = this.f2622d.hashCode();
            } else {
                i3 = 0;
            }
            int i7 = (i3 + i6) * 31;
            if (this.f2623e != null) {
                i4 = this.f2623e.hashCode();
            }
            return i7 + i4;
        }

        public String toString() {
            return "Filter{deviceAddress='" + this.f2619a + '\'' + ", deviceName='" + this.f2620b + '\'' + ", data=" + this.f2621c + ", serviceData=" + this.f2622d + ", serviceUuid=" + this.f2623e + '}';
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.tt$B */
    public static class b {
        @NonNull

        /* renamed from: a */
        public final a_enum f2632a;
        @NonNull

        /* renamed from: B */
        public final b_enum f2633b;
        @NonNull

        /* renamed from: a */
        public final c_enum f2634c;
        @NonNull

        /* renamed from: d */
        public final d_enum f2635d;

        /* renamed from: e */
        public final long f2636e;

        /* renamed from: com.yandex.metrica.impl.ob.tt$B$a */
        public enum a_enum {
            ALL_MATCHES,
            FIRST_MATCH,
            MATCH_LOST
        }

        /* renamed from: com.yandex.metrica.impl.ob.tt$B$B */
        public enum b_enum {
            AGGRESSIVE,
            STICKY
        }

        /* renamed from: com.yandex.metrica.impl.ob.tt$B$a */
        public enum c_enum {
            ONE_AD,
            FEW_AD,
            MAX_AD
        }

        /* renamed from: com.yandex.metrica.impl.ob.tt$B$d */
        public enum d_enum {
            LOW_POWER,
            BALANCED,
            LOW_LATENCY
        }

        public b(@NonNull a_enum aVar, @NonNull b_enum bVar, @NonNull c_enum cVar, @NonNull d_enum dVar, long j) {
            this.f2632a = aVar;
            this.f2633b = bVar;
            this.f2634c = cVar;
            this.f2635d = dVar;
            this.f2636e = j;
        }

        public boolean equals(Object o) {
            boolean z = true;
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            b bVar = (b) o;
            if (this.f2636e != bVar.f2636e || this.f2632a != bVar.f2632a || this.f2633b != bVar.f2633b || this.f2634c != bVar.f2634c) {
                return false;
            }
            if (this.f2635d != bVar.f2635d) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return (((((((this.f2632a.hashCode() * 31) + this.f2633b.hashCode()) * 31) + this.f2634c.hashCode()) * 31) + this.f2635d.hashCode()) * 31) + ((int) (this.f2636e ^ (this.f2636e >>> 32)));
        }

        public String toString() {
            return "Settings{callbackType=" + this.f2632a + ", matchMode=" + this.f2633b + ", numOfMatches=" + this.f2634c + ", scanMode=" + this.f2635d + ", reportDelay=" + this.f2636e + '}';
        }
    }

    public tt(@NonNull b bVar, @NonNull List<tt.a> list, long j, long j2) {
        this.a = bVar;
        this.b = list;
        this.c = j;
        this.d = j2;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        tt ttVar = (tt) o;
        if (this.c == ttVar.c && this.d == ttVar.d && this.a.equals(ttVar.a)) {
            return this.b.equals(ttVar.b);
        }
        return false;
    }

    public int hashCode() {
        return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31) + ((int) (this.d ^ (this.d >>> 32)));
    }

    public String toString() {
        return "BleCollectingConfig{settings=" + this.a + ", scanFilters=" + this.b + ", sameBeaconMinReportingInterval=" + this.c + ", firstDelay=" + this.d + '}';
    }
}
