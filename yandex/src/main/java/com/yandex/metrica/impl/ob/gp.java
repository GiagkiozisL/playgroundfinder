package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.gp */
public class gp extends gr<hd> {

    /* renamed from: a */
    private final ig f982a;

    /* renamed from: B */
    private Map<C0058a, hz<hd>> f983b;// = m1711b();
    @Nullable

    /* renamed from: a */
    private hw<hd> f984c; // = new hv(this.f982a);
    @Nullable

    /* renamed from: d */
    private hw<hd> d;

    public gp(en enVar) {
        this.f982a = new ig(enVar);
        this.f984c = new hv(this.f982a);
        this.f983b = m1711b();
    }

    /* renamed from: B */
    private HashMap<C0058a, hz<hd>> m1711b() {
        HashMap<C0058a, hz<hd>> hashMap = new HashMap<>();
        hashMap.put(C0058a.EVENT_TYPE_ACTIVATION, new hu(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_START, new ij(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_REGULAR, new id(this.f982a));
        ib ibVar = new ib(this.f982a);
        hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_USER, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_SEND_REFERRER, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_STATBOX, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_CUSTOM_EVENT, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_APP_OPEN, new iff(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_PURGE_BUFFER, new ic(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, new ii(this.f982a, this.f982a.mo1002l()));
        hashMap.put(C0058a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, new hy(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, new il(this.f982a));
        ik ikVar = new ik(this.f982a);
        hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED, ikVar);
        hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, ikVar);
        hashMap.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, ikVar);
        hashMap.put(C0058a.EVENT_TYPE_ANR, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_IDENTITY, new ia(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_SET_USER_INFO, new ih(this.f982a));
        hashMap.put(C0058a.EVENT_TYPE_REPORT_USER_INFO, new ii(this.f982a, this.f982a.mo997g()));
        hashMap.put(C0058a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, new ii(this.f982a, this.f982a.mo999i()));
        hashMap.put(C0058a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, new ii(this.f982a, this.f982a.mo1000j()));
        hashMap.put(C0058a.EVENT_TYPE_SEND_USER_PROFILE, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_SET_USER_PROFILE_ID, new ii(this.f982a, this.f982a.mo1005o()));
        hashMap.put(C0058a.EVENT_TYPE_SEND_REVENUE_EVENT, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_IDENTITY_LIGHT, ibVar);
        hashMap.put(C0058a.EVENT_TYPE_CLEANUP, ibVar);
        return hashMap;
    }

    /* renamed from: a */
    public void mo958a(C0058a aVar, hz<hd> hzVar) {
        this.f983b.put(aVar, hzVar);
    }

    /* renamed from: a */
    public ig mo957a() {
        return this.f982a;
    }

    /* renamed from: a */
    public go<hd> mo955a(int i) {
        LinkedList linkedList = new LinkedList();
        C0058a a = C0058a.m298a(i);
        if (this.f984c != null) {
            this.f984c.mo978a(a, linkedList);
        }
        hz hzVar = (hz) this.f983b.get(a);
        if (hzVar != null) {
            hzVar.mo977a(linkedList);
        }
        if (this.d != null) {
            this.d.mo978a(a, linkedList);
        }
        return new gn(linkedList);
    }
}
