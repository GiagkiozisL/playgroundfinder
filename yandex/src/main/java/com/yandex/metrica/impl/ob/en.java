package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.em.C0315a;
import com.yandex.metrica.impl.ob.i.a;
import com.yandex.metrica.impl.ob.jd.C0484a;
import com.yandex.metrica.impl.ob.st.C0987d;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.en */
public class en implements et, ew, ny {

    /* renamed from: a */
    protected ly f826a;

    /* renamed from: B */
    private final Context f827b;

    /* renamed from: a */
    private final ek f828c;

    /* renamed from: d */
    private final lw f829d;

    /* renamed from: e */
    private final lu f830e;

    /* renamed from: f */
    private final cc f831f;

    /* renamed from: g */
    private final kz f832g;

    /* renamed from: h */
    private final gs f833h;

    /* renamed from: i */
    private final gp f834i;

    /* renamed from: j */
    private final i f835j;
    @NonNull

    /* renamed from: k */
    private final C0325a f836k;

    /* renamed from: l */
    private volatile jd f837l;

    /* renamed from: m */
    private final fe f838m;
    @NonNull

    /* renamed from: n */
    private final io f839n;
    @NonNull

    /* renamed from: o */
    private final vz f840o;
    @NonNull

    /* renamed from: p */
    private final vp f841p;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: q */
    public final ff f842q;
    @NonNull

    /* renamed from: r */
    private final C0315a f843r;
    @NonNull

    /* renamed from: s */
    private final nx f844s;
    @NonNull

    /* renamed from: t */
    private final nu f845t;
    @NonNull

    /* renamed from: u */
    private final nz f846u;
    @NonNull

    /* renamed from: v */
    private final q f847v;
    @NonNull

    /* renamed from: w */
    private final cw f848w;

    /* renamed from: com.yandex.metrica.impl.ob.en$a */
    static class C0325a {

        /* renamed from: a */
        private final HashMap<String, i> f850a = new HashMap<>();

        C0325a() {
        }

        /* renamed from: a */
        public synchronized i mo841a(@NonNull ek ekVar, @NonNull vz vzVar, lw lwVar) {
            i iVar;
            iVar = (i) this.f850a.get(ekVar.toString());
            if (iVar == null) {
                a d = lwVar.mo1332d();
                iVar = new i(d.a, d.f1031b, vzVar);
                this.f850a.put(ekVar.toString(), iVar);
            }
            return iVar;
        }

        /* renamed from: a */
        public synchronized boolean mo842a(a aVar, lw lwVar) {
            boolean z;
            if (aVar.f1031b > lwVar.mo1332d().f1031b) {
                lwVar.mo1320a(aVar).mo1364q();
                z = true;
            } else {
                z = false;
            }
            return z;
        }

        /* renamed from: B */
        public synchronized void mo843b(a aVar, lw lwVar) {
            lwVar.mo1320a(aVar).mo1364q();
        }
    }

    /* renamed from: d */
    public jd mo817d() {
        return this.f837l;
    }

    @NonNull
    /* renamed from: e */
    public ff mo818e() {
        return this.f842q;
    }

    public en(@NonNull Context context, @NonNull uk ukVar, @NonNull bl blVar, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull C0987d dVar, @NonNull un unVar) {
        this(context, ekVar, new C0325a(), new cw(), new eo(context, ekVar, aVar, unVar, ukVar, dVar, blVar, al.m324a().mo292j().mo2632g(), cx.c(context, ekVar.mo791b())));
    }

    @VisibleForTesting
    en(@NonNull Context context, @NonNull ek ekVar, @NonNull C0325a aVar, @NonNull cw cwVar, @NonNull eo eoVar) {
        this.f827b = context.getApplicationContext();
        this.f828c = ekVar;
        this.f836k = aVar;
        this.f848w = cwVar;
        this.f840o = eoVar.mo844a().mo860a();
        this.f841p = eoVar.mo844a().mo861b();
        this.f829d = eoVar.mo854b().mo863b();
        this.f830e = eoVar.mo854b().mo862a();
        this.f826a = eoVar.mo854b().mo864c();
        this.f835j = aVar.mo841a(this.f828c, this.f840o, this.f829d);
        this.f839n = eoVar.mo856c();
        this.f832g = eoVar.mo848a(this);
        this.f831f = eoVar.mo853b(this);
        this.f838m = eoVar.mo855c(this);
        this.f843r = eoVar.mo858e(this);
        this.f846u = eoVar.mo851a(this.f832g, this.f838m);
        this.f845t = eoVar.mo849a(this.f832g);
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.f846u);
        arrayList.add(this.f845t);
        this.f844s = eoVar.mo850a((List<nv>) arrayList, (ny) this);
        m1478D();
        this.f837l = eoVar.mo847a(this, this.f829d, new C0484a() {
            /* renamed from: a */
            public void mo840a(@NonNull w wVar, @NonNull je jeVar) {
                en.this.f842q.mo882a(wVar, jeVar);
            }
        });
        if (this.f841p.mo2485c()) {
            this.f841p.mo2478a("Read app environment for component %s. Value: %s", this.f828c.toString(), this.f835j.mo987b().a);
        }
        this.f842q = eoVar.mo845a(this.f829d, this.f837l, this.f832g, this.f835j, this.f831f);
        this.f834i = eoVar.mo857d(this);
        this.f833h = eoVar.mo846a(this, this.f834i);
        this.f847v = eoVar.mo852a(this.f829d);
        this.f832g.mo1198a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public gp mo819f() {
        return this.f834i;
    }

    /* renamed from: a */
    public void a(@NonNull w wVar) {
        if (this.f840o.mo2485c()) {
            this.f840o.mo2518a(wVar, "Event received on service");
        }
        if (cx.m1191a(this.f828c.mo790a())) {
            this.f833h.mo959b(wVar);
        }
    }

    /* renamed from: a */
    public synchronized void a(@NonNull C0306a aVar) {
        this.f838m.a(aVar);
        m1480b(aVar);
    }

    /* renamed from: g */
    public synchronized void g() {
        this.f831f.mo659e();
    }

    @Nullable
    /* renamed from: h */
    public String mo821h() {
        return this.f829d.mo1340f();
    }

    @NonNull
    /* renamed from: i */
    public st mo822i() {
        return (st) this.f838m.mo2124d();
    }

    /* renamed from: j */
    public kz mo823j() {
        return this.f832g;
    }

    /* renamed from: B */
    public ek b() {
        return this.f828c;
    }

    /* renamed from: a */
    public synchronized void a(@Nullable uk ukVar) {
        this.f838m.mo2121a(ukVar);
        this.f844s.mo1526a();
    }

    /* renamed from: a */
    public synchronized void a(@NonNull ue ueVar, @Nullable uk ukVar) {
    }

    /* renamed from: k */
    public Context mo824k() {
        return this.f827b;
    }

    @NonNull
    /* renamed from: l */
    public vz mo825l() {
        return this.f840o;
    }

    /* renamed from: m */
    public void mo826m() {
        this.f842q.mo883b();
    }

    /* renamed from: B */
    public void mo815b(w wVar) {
        this.f835j.mo986a(wVar.mo2537k());
        a b = this.f835j.mo987b();
        if (this.f836k.mo842a(b, this.f829d) && this.f840o.mo2485c()) {
            this.f840o.mo2478a("Save new app environment for %s. Value: %s", b(), b.a);
        }
    }

    /* renamed from: n */
    public void mo827n() {
        this.f835j.mo985a();
        this.f836k.mo843b(this.f835j.mo987b(), this.f829d);
    }

    /* renamed from: a */
    public void mo814a(String str) {
        this.f829d.mo1321a(str).mo1364q();
    }

    /* renamed from: o */
    public void mo828o() {
        this.f829d.mo1333d(mo829p() + 1).mo1364q();
        this.f838m.mo2119a();
    }

    /* renamed from: p */
    public int mo829p() {
        return this.f829d.mo1343i();
    }

    /* renamed from: q */
    public boolean mo830q() {
        return this.f842q.mo886c() && mo822i().mo2088f();
    }

    /* renamed from: r */
    public boolean mo831r() {
        st i = mo822i();
        return i.mo2131H() && i.mo2088f() && this.f848w.mo662a(this.f842q.mo887d(), i.mo2133J(), "need to check permissions");
    }

    /* renamed from: s */
    public boolean mo832s() {
        st i = mo822i();
        return i.mo2131H() && this.f848w.mo662a(this.f842q.mo887d(), i.mo2134K(), "should force send permissions");
    }

    /* renamed from: t */
    public boolean mo833t() {
        return this.f842q.mo890e() && mo822i().mo2132I() && mo822i().mo2088f();
    }

    /* renamed from: u */
    public lu mo834u() {
        return this.f830e;
    }

    @Deprecated
    /* renamed from: v */
    public final qc mo835v() {
        return new qc(this.f827b, this.f828c.mo790a());
    }

    /* renamed from: w */
    public boolean mo836w() {
        return this.f826a.mo1373a();
    }

    /* renamed from: x */
    public boolean mo837x() {
        if (!(this.f830e.mo1280b(false) && this.f838m.mo2122b().startupResponseClidsMatchClientClids)) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public void c() {
        cx.a((Closeable) this.f831f);
        cx.a((Closeable) this.f832g);
    }

    /* renamed from: D */
    private void m1478D() {
        int libraryApiLevel = YandexMetrica.getLibraryApiLevel();
        if (this.f829d.mo1341g() < ((long) libraryApiLevel)) {
            this.f843r.mo800a(new qb(mo835v())).mo799a();
            this.f829d.mo1334d((long) libraryApiLevel).mo1364q();
        }
    }

    /* renamed from: B */
    private void m1480b(@NonNull C0306a aVar) {
        if (vi.m4192a(aVar.f779l)) {
            this.f840o.mo2474a();
        } else if (vi.m4194c(aVar.f779l)) {
            this.f840o.mo2480b();
        }
    }

    /* renamed from: y */
    public lw mo838y() {
        return this.f829d;
    }

    /* renamed from: B */
    public void mo816b(@Nullable String str) {
        this.f829d.mo1335d(str).mo1364q();
    }

    @NonNull
    /* renamed from: z */
    public io mo839z() {
        return this.f839n;
    }

    @NonNull
    /* renamed from: A */
    public q mo810A() {
        return this.f847v;
    }

    @Nullable
    /* renamed from: B */
    public String mo811B() {
        return this.f829d.mo1342h();
    }

    @NonNull
    /* renamed from: C */
    public nx mo812C() {
        return this.f844s;
    }

    @NonNull
    /* renamed from: a */
    public C0008a mo767a() {
        return C0008a.MANUAL;
    }
}
