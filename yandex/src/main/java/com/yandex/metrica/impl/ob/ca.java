package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.util.Log;

import com.yandex.metrica.d;
import com.yandex.metrica.f;
import com.yandex.metrica.j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ca */
class ca {
    @NonNull

    /* renamed from: a */
    private Context f487a;
    @NonNull

    /* renamed from: B */
    private ee f488b;
    @NonNull

    /* renamed from: a */
    private cd f489c;
    @NonNull

    /* renamed from: d */
    private Handler f490d;
    @NonNull

    /* renamed from: e */
    private uf f491e;

    /* renamed from: f */
    private Map<String, d> f492f = new HashMap();

    /* renamed from: g */
    private final yk<String> f493g = new yg(new ym(this.f492f));

    /* renamed from: h */
    private final List<String> f494h = Arrays.asList(new String[]{"20799a27-fa80-4b36-b2db-0f8141f24180", "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"});

    public ca(@NonNull Context context, @NonNull ee eeVar, @NonNull cd cdVar, @NonNull Handler handler, @NonNull uf ufVar) {
        this.f487a = context;
        this.f488b = eeVar;
        this.f489c = cdVar;
        this.f490d = handler;
        this.f491e = ufVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public ax mo553a(@NonNull j jVar, boolean z) {
        this.f493g.a(jVar.apiKey);
        ax axVar = new ax(this.f487a, this.f488b, jVar, this.f489c, this.f491e, new cb(this, "20799a27-fa80-4b36-b2db-0f8141f24180"), new cb(this, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"));
        m864a((n) axVar);
        axVar.mo331a(jVar, z);
        axVar.mo1451a();
        this.f489c.mo563a(axVar);
        this.f492f.put(jVar.apiKey, axVar);
        return axVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public synchronized void mo554a(@NonNull f fVar) {
        if (this.f492f.containsKey(fVar.apiKey)) {
            vz a = vr.m4237a(fVar.apiKey);
            if (a.mo2485c()) {
                a.mo2482b("Reporter with apiKey=%s already exists.", fVar.apiKey);
            }
        } else {
            mo555b(fVar);
            Log.i("AppMetrica", "Activate reporter with APIKey " + cx.m1197b(fVar.apiKey));
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: B */
    public synchronized d mo555b(@NonNull f fVar) {
        d dVar;
        dVar = (d) this.f492f.get(fVar.apiKey);
        if (dVar == null) {
            if (!this.f494h.contains(fVar.apiKey)) {
                this.f491e.mo2346c();
            }
            ay ayVar = new ay(this.f487a, this.f488b, fVar, this.f489c);
            m864a((n) ayVar);
            ayVar.mo1451a();
            this.f492f.put(fVar.apiKey, ayVar);
            dVar = ayVar;
        }
        return dVar;
    }

    /* renamed from: a */
    private void m864a(@NonNull n nVar) {
        nVar.mo1453a(new at(this.f490d, nVar));
        nVar.mo1454a((ug) this.f491e);
    }
}
