package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.SparseArray;

/* renamed from: com.yandex.metrica.impl.ob.lj */
public class lj {

    /* renamed from: a */
    private final String f1321a;

    /* renamed from: B */
    private final lr f1322b;

    /* renamed from: a */
    private final lr f1323c;

    /* renamed from: d */
    private final SparseArray<lr> f1324d;

    /* renamed from: e */
    private final lk f1325e;

    /* renamed from: com.yandex.metrica.impl.ob.lj$a */
    public static class C0569a {
        /* renamed from: a */
        public lj mo1266a(@NonNull String str, @NonNull lr lrVar, @NonNull lr lrVar2, @NonNull SparseArray<lr> sparseArray, @NonNull lk lkVar) {
            return new lj(str, lrVar, lrVar2, sparseArray, lkVar);
        }
    }

    private lj(String str, lr lrVar, lr lrVar2, SparseArray<lr> sparseArray, lk lkVar) {
        this.f1321a = str;
        this.f1322b = lrVar;
        this.f1323c = lrVar2;
        this.f1324d = sparseArray;
        this.f1325e = lkVar;
    }

    /* renamed from: a */
    public void mo1261a(SQLiteDatabase sQLiteDatabase) {
        try {
            if (this.f1325e != null && !this.f1325e.a(sQLiteDatabase)) {
                mo1265c(sQLiteDatabase);
            }
        } catch (Throwable th) {
        }
    }

    /* renamed from: B */
    public void mo1264b(SQLiteDatabase sQLiteDatabase) {
        mo1263a(this.f1322b, sQLiteDatabase);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo1263a(lr lrVar, SQLiteDatabase sQLiteDatabase) {
        try {
            lrVar.mo1273a(sQLiteDatabase);
        } catch (Throwable th) {
        }
    }

    /* renamed from: a */
    public void mo1262a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        boolean z;
        boolean z2 = true;
        if (i2 > i) {
            int i3 = i + 1;
            while (i3 <= i2) {
                try {
                    lr lrVar = (lr) this.f1324d.get(i3);
                    if (lrVar != null) {
                        lrVar.mo1273a(sQLiteDatabase);
                    }
                    i3++;
                } catch (Throwable th) {
                    z = true;
                }
            }
            z = false;
        } else {
            z = true;
        }
        if (this.f1325e.a(sQLiteDatabase)) {
            z2 = false;
        }
        if (z || z2) {
            mo1265c(sQLiteDatabase);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo1265c(SQLiteDatabase sQLiteDatabase) {
        m2195b(this.f1323c, sQLiteDatabase);
        mo1263a(this.f1322b, sQLiteDatabase);
    }

    /* renamed from: B */
    private void m2195b(lr lrVar, SQLiteDatabase sQLiteDatabase) {
        try {
            lrVar.mo1273a(sQLiteDatabase);
        } catch (Throwable th) {
        }
    }
}
