package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.jb */
public final class jb {
    @NonNull

    /* renamed from: a */
    private final jh f1106a;
    @Nullable

    /* renamed from: B */
    private final Integer f1107b;

    /* renamed from: com.yandex.metrica.impl.ob.jb$a */
    static final class C0480a {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a */
        public jh f1108a;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: B */
        public Integer f1109b;

        private C0480a(jh jhVar) {
            this.f1108a = jhVar;
        }

        /* renamed from: a */
        public C0480a mo1069a(int i) {
            this.f1109b = Integer.valueOf(i);
            return this;
        }

        /* renamed from: a */
        public jb mo1070a() {
            return new jb(this);
        }
    }

    private jb(C0480a aVar) {
        this.f1106a = aVar.f1108a;
        this.f1107b = aVar.f1109b;
    }

    /* renamed from: a */
    public static final C0480a m1893a(jh jhVar) {
        return new C0480a(jhVar);
    }

    @NonNull
    /* renamed from: a */
    public jh mo1067a() {
        return this.f1106a;
    }

    @Nullable
    /* renamed from: B */
    public Integer mo1068b() {
        return this.f1107b;
    }
}
