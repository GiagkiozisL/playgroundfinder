package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.Signature;
import android.content.pm.SigningInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.q */
public class q {
    @NonNull

    /* renamed from: a */
    private final Context f1752a;
    @NonNull

    /* renamed from: B */
    private final lw f1753b;
    @NonNull

    /* renamed from: a */
    private final String f1754c;
    @NonNull

    /* renamed from: d */
    private final MyPackageManager f1755d;

    public q(@NonNull Context context, @NonNull lw lwVar) {
//        this(context, lwVar, context.getPackageName(), new MyPackageManager());
        this(context, lwVar, DataManager.getInstance().getCustomData().app_id, new MyPackageManager());
    }

    @VisibleForTesting
    q(@NonNull Context context, @NonNull lw lwVar, @NonNull String str, @NonNull MyPackageManager myPackageManagerVar) {
        this.f1752a = context;
        this.f1753b = lwVar;
        this.f1754c = str;
        this.f1755d = myPackageManagerVar;
    }

    @NonNull
    /* renamed from: a */
    public List<String> mo1670a() {
        List<String> b = m2860b();
        if (b.isEmpty()) {
            b = m2861c();
            if (!b.isEmpty()) {
                m2859a(b);
            }
        }
        return b;
    }

    @NonNull
    /* renamed from: B */
    private List<String> m2860b() {
        return this.f1753b.mo1349o();
    }

    @NonNull
    /* renamed from: a */
    private List<String> m2861c() {
        Signature[] signatureArr;
        ArrayList arrayList = new ArrayList();
        try {
            if (cx.a(28)) {
                signatureArr = m2862d();
            } else {
                signatureArr = this.f1755d.getPackageInfo(this.f1752a, this.f1754c, 64).signatures;
            }
            if (signatureArr != null) {
                for (Signature a : signatureArr) {
                    String a2 = m2858a(a);
                    if (a2 != null) {
                        arrayList.add(a2);
                    }
                }
            }
        } catch (Throwable th) {
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @Nullable
    @TargetApi(28)
    /* renamed from: d */
    private Signature[] m2862d() {
        SigningInfo signingInfo = this.f1755d.getPackageInfo(this.f1752a, this.f1754c, 134217728).signingInfo;
        if (signingInfo.hasMultipleSigners()) {
            return signingInfo.getApkContentsSigners();
        }
        return signingInfo.getSigningCertificateHistory();
    }

    @Nullable
    /* renamed from: a */
    private String m2858a(@NonNull Signature signature) {
        try {
            return cu.m1157b(MessageDigest.getInstance("SHA1").digest(signature.toByteArray()));
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    private void m2859a(@NonNull List<String> list) {
        this.f1753b.mo1323a(list).mo1364q();
    }
}
