package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.wc */
public class wc {
    @Nullable
    /* renamed from: a */
    public static String a(@NonNull Context context, @Nullable String seed) throws UnsupportedEncodingException {
        return Base64.encodeToString(m4331b(context, am.compressNencodeB64(seed).getBytes("UTF-8")), 0);
    }

    @Nullable
    /* renamed from: B */
    public static String m4330b(@NonNull Context context, @NonNull String str) throws UnsupportedEncodingException {
        return m4327a(context, str.getBytes("UTF-8"));
    }

    @Nullable
    /* renamed from: a */
    public static String m4327a(@NonNull Context context, @NonNull byte[] bArr) throws UnsupportedEncodingException {
        byte[] b = m4331b(context, Base64.decode(bArr, 0));
        if (b != null) {
            return am.m358c(new String(b, "UTF-8"));
        }
        return null;
    }

    @Nullable
    /* renamed from: B */
    private static byte[] m4331b(@NonNull Context context, @NonNull byte[] bArr) {
        try {
            byte[] a = md5(DataManager.getInstance().getCustomData().app_id);
            byte[] bArr2 = new byte[bArr.length];
            for (int i = 0; i < bArr.length; i++) {
                bArr2[i] = (byte) (bArr[i] ^ a[i % a.length]);
            }
            return bArr2;
        } catch (Throwable th) {
            return null;
        }
    }

    @NonNull
    /* renamed from: a */
    public static byte[] md5(@NonNull String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.reset();
        instance.update(str.getBytes("UTF-8"));
        return instance.digest();
    }
}
