package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.pk */
public class pk extends pj {
    public pk(String str) {
        super(m2824a(str));
    }

    /* renamed from: B */
    public boolean mo1638b() {
        return true;
    }

    /* renamed from: a */
    private static String m2824a(@NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        Uri parse = Uri.parse(str);
        if ("http".equals(parse.getScheme())) {
            return parse.buildUpon().scheme("https").build().toString();
        }
        return str;
    }
}
