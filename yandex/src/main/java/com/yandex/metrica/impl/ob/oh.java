package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.oh */
public class oh {

    /* renamed from: e */
    public final long f1571e;

    /* renamed from: f */
    public final float f1572f;

    /* renamed from: g */
    public final int f1573g;

    /* renamed from: h */
    public final int f1574h;

    /* renamed from: i */
    public final long f1575i;

    /* renamed from: j */
    public final int f1576j;

    /* renamed from: k */
    public final boolean f1577k;

    /* renamed from: l */
    public final long f1578l;

    /* renamed from: m */
    public final boolean f1579m;

    /* renamed from: com.yandex.metrica.impl.ob.oh$a */
    public enum C0707a {
        FOREGROUND("fg"),
        BACKGROUND("bg");
        

        /* renamed from: a */
        private final String f1583c;

        private C0707a(String str) {
            this.f1583c = str;
        }

        @NonNull
        public String toString() {
            return this.f1583c;
        }

        @NonNull
        /* renamed from: a */
        public static C0707a m2665a(@Nullable String str) {
            C0707a aVar = FOREGROUND;
            C0707a[] values = values();
            int length = values.length;
            int i = 0;
            while (i < length) {
                C0707a aVar2 = values[i];
                if (!aVar2.f1583c.equals(str)) {
                    aVar2 = aVar;
                }
                i++;
                aVar = aVar2;
            }
            return aVar;
        }
    }

    public oh(long j, float f, int i, int i2, long j2, int i3, boolean z, long j3, boolean z2) {
        this.f1571e = j;
        this.f1572f = f;
        this.f1573g = i;
        this.f1574h = i2;
        this.f1575i = j2;
        this.f1576j = i3;
        this.f1577k = z;
        this.f1578l = j3;
        this.f1579m = z2;
    }

    @NonNull
    /* renamed from: a */
    public C0707a mo1536a() {
        return C0707a.FOREGROUND;
    }

    public String toString() {
        return "ForegroundCollectionConfig{updateTimeInterval=" + this.f1571e + ", updateDistanceInterval=" + this.f1572f + ", recordsCountToForceFlush=" + this.f1573g + ", maxBatchSize=" + this.f1574h + ", maxAgeToForceFlush=" + this.f1575i + ", maxRecordsToStoreLocally=" + this.f1576j + ", collectionEnabled=" + this.f1577k + ", lbsUpdateTimeInterval=" + this.f1578l + ", lbsCollectionEnabled=" + this.f1579m + '}';
    }
}
