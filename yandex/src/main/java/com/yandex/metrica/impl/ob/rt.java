package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.rt */
public class rt extends yg<Void> {
    public rt(@NonNull final sa var1) {

        super(new yk<Void>() {
            public yi a(@Nullable Void var1x) {
                return var1.c() ? yi.a(this) : yi.a(this, "YandexMetrica isn't initialized. Use YandexMetrica#activate(android.content.Context, String) method to activate.");
            }
        });
//        super(new yk<Void>() {
//            /* renamed from: a */
//            public yi a(@Nullable Void voidR) {
//                if (sa.this.a()) {
//                    return yi.a(this);
//                }
//                return yi.a(this, "YandexMetrica isn't initialized. Use YandexMetrica#activate(android.content.Context, String) method to activate.");
//            }
//        });
    }

    /* renamed from: a */
    public yi a() {
        return super.a(null);
    }
}
