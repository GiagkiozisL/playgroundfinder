package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.interact.DeviceInfo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/* renamed from: com.yandex.metrica.impl.ob.da */
public final class da {

    /* renamed from: a */
    static rz f669a = new rz(db.m1276a());

    @Deprecated
    /* renamed from: a */
    public static void m1261a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        f669a.mo1969a(iIdentifierCallback, list);
    }

    /* renamed from: a */
    public static void m1259a(@NonNull Context context, @NonNull IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        f669a.mo1967a(context, iIdentifierCallback, list);
    }

    /* renamed from: a */
    public static boolean m1262a() {
        return f669a.mo1970a();
    }

    @Nullable
    /* renamed from: B */
    public static Future<String> m1264b() {
        return f669a.mo1972b();
    }

    @NonNull
    /* renamed from: a */
    public static DeviceInfo m1255a(Context context) {
        return f669a.mo1963a(context);
    }

    @NonNull
    /* renamed from: B */
    public static String m1263b(Context context) {
        return f669a.mo1971b(context);
    }

    @Nullable
    /* renamed from: a */
    public static Integer m1266c(Context context) {
        return f669a.mo1974c(context);
    }

    @Nullable
    @Deprecated
    /* renamed from: a */
    public static String m1267c() {
        return f669a.mo1976d();
    }

    @Nullable
    /* renamed from: d */
    public static String m1268d(@NonNull Context context) {
        return f669a.mo1977d(context);
    }

    @Nullable
    /* renamed from: e */
    public static String m1270e(@NonNull Context context) {
        return f669a.mo1978e(context);
    }

    @NonNull
    /* renamed from: f */
    public static String m1272f(@NonNull Context context) {
        return f669a.mo1980f(context);
    }

    /* renamed from: a */
    public static void m1258a(int i, String str, String str2, Map<String, String> map) {
        f669a.mo1966a(i, str, str2, map);
    }

    @Nullable
    /* renamed from: d */
    public static Future<Boolean> m1269d() {
        return f669a.mo1975c();
    }

    /* renamed from: e */
    public static void m1271e() {
        f669a.mo1979e();
    }

    @NonNull
    /* renamed from: a */
    public static String m1257a(@Nullable String str) {
        return f669a.mo1965a(str);
    }

    @NonNull
    /* renamed from: a */
    public static String m1256a(int i) {
        return f669a.mo1964a(i);
    }

    @NonNull
    /* renamed from: a */
    public static YandexMetricaConfig m1253a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return f669a.mo1961a(yandexMetricaConfig, str);
    }

    @NonNull
    /* renamed from: a */
    public static YandexMetricaConfig m1254a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return f669a.mo1962a(yandexMetricaConfig, list);
    }

    /* renamed from: a */
    public static void m1260a(@NonNull Context context, @NonNull Object obj) {
        f669a.mo1968a(context, obj);
    }

    /* renamed from: B */
    public static void m1265b(@NonNull Context context, @NonNull Object obj) {
        f669a.mo1973b(context, obj);
    }

    @Nullable
    /* renamed from: g */
    public static Location m1273g(Context context) {
        return f669a.mo1981g(context);
    }
}
