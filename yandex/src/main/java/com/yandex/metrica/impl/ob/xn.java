package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.concurrent.Executor;

/* renamed from: com.yandex.metrica.impl.ob.xn */
public class xn {
    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public xg mo2615a() {
        return new xg("YMM-MC");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: B */
    public Executor mo2617b() {
        return new xp();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public xg mo2618c() {
        return new xg("YMM-MSTE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: d */
    public xg mo2619d() {
        return new xg("YMM-TP");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: e */
    public xg mo2620e() {
        return new xg("YMM-UH-1");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: f */
    public xg mo2621f() {
        return new xg("YMM-CSE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: g */
    public xg mo2622g() {
        return new xg("YMM-CTH");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: h */
    public xg mo2623h() {
        return new xg("YMM-SDCT");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: i */
    public xg mo2624i() {
        return new xg("YMM-DE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public xl mo2616a(@NonNull Runnable runnable) {
        return xm.m4443a("YMM-IB", runnable);
    }
}
