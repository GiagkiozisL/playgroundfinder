package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.os.ParcelUuid;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.SparseArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.dj */
public class dj {
    @NonNull

    /* renamed from: a */
    private final wh f720a;
    @NonNull

    /* renamed from: B */
    private final wf f721b;

    public dj() {
        this(new wg(), new wf());
    }

    /* renamed from: a */
    public JSONObject mo704a(int i) throws JSONException {
        return new JSONObject().put("error_code", i);
    }

    /* renamed from: a */
    public JSONObject mo705a(@NonNull ScanResult scanResult, @Nullable Integer num) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (num != null) {
            jSONObject.put("callback_type", num);
        }
        jSONObject.put("event_timestamp", this.f720a.b());
        jSONObject.put("rssi", scanResult.getRssi());
        jSONObject.put("observed_scan_result_offset", this.f721b.mo2560a(scanResult.getTimestampNanos(), TimeUnit.NANOSECONDS));
        if (cx.a(26)) {
            jSONObject.put("ad_ssid", scanResult.getAdvertisingSid());
            jSONObject.put("periodic_ad_interval", scanResult.getPeriodicAdvertisingInterval());
            jSONObject.put("primary_phy", scanResult.getPrimaryPhy());
            jSONObject.put("secondary_phy", scanResult.getSecondaryPhy());
            jSONObject.put("tx_power", scanResult.getTxPower());
        }
        ScanRecord scanRecord = scanResult.getScanRecord();
        if (scanRecord != null) {
            m1346a(scanRecord, jSONObject);
        }
        BluetoothDevice device = scanResult.getDevice();
        if (device != null) {
            m1345a(device, jSONObject);
        }
        return jSONObject;
    }

    /* renamed from: a */
    private void m1345a(@NonNull BluetoothDevice bluetoothDevice, @NonNull JSONObject jSONObject) throws JSONException {
        jSONObject.put("device_address", bluetoothDevice.getAddress());
    }

    /* renamed from: a */
    private void m1346a(ScanRecord scanRecord, @NonNull JSONObject jSONObject) throws JSONException {
        jSONObject.put("device_name", scanRecord.getDeviceName());
        if (scanRecord.getManufacturerSpecificData() != null) {
            jSONObject.put("manufacturer_data", m1343a(scanRecord.getManufacturerSpecificData()));
        }
        if (scanRecord.getServiceData() != null) {
            jSONObject.put("service_data", m1344a(scanRecord.getServiceData()));
        }
        jSONObject.put("packet_tx_power_level", scanRecord.getTxPowerLevel());
    }

    /* renamed from: a */
    private JSONObject m1343a(@NonNull SparseArray<byte[]> sparseArray) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= sparseArray.size()) {
                return jSONObject;
            }
            jSONObject.put(String.valueOf(sparseArray.keyAt(i2)), cu.m1152a((byte[]) sparseArray.valueAt(i2)));
            i = i2 + 1;
        }
    }

    /* renamed from: a */
    private JSONObject m1344a(@NonNull Map<ParcelUuid, byte[]> map) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : map.entrySet()) {
            jSONObject.put(((ParcelUuid) entry.getKey()).toString(), cu.m1152a((byte[]) entry.getValue()));
        }
        return jSONObject;
    }

    @VisibleForTesting
    dj(@NonNull wh whVar, @NonNull wf wfVar) {
        this.f720a = whVar;
        this.f721b = wfVar;
    }
}
