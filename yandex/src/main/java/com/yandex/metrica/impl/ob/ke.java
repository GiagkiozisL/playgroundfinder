package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.ke */
public class ke implements Runnable {
    @NonNull

    /* renamed from: a */
    private final Context f1197a;
    @NonNull

    /* renamed from: B */
    private final File f1198b;
    @NonNull

    /* renamed from: a */
    private final wm<kv> f1199c;

    public ke(@NonNull Context context, @NonNull File file, @NonNull wm<kv> wmVar) {
        this.f1197a = context;
        this.f1198b = file;
        this.f1199c = wmVar;
    }

    public void run() {
        if (this.f1198b.exists()) {
            try {
                mo1143a(am.m340a(this.f1197a, this.f1198b));
                try {
                    if (!this.f1198b.delete()) {
                    }
                    return;
                } catch (Throwable th) {
                    return;
                }
            } catch (Throwable th2) {
            }
        } else {
            return;
        }
//        throw th;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo1143a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                this.f1199c.a(new kv(str));
            } catch (Throwable th) {
            }
        }
    }
}
