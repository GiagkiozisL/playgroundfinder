package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rh.b;
import com.yandex.metrica.impl.ob.rh.b.C0819a;
import com.yandex.metrica.impl.ob.rh.b.C0820b;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.pf */
public class pf {
    @NonNull

    /* renamed from: a */
    private final lh f1721a;
    @NonNull

    /* renamed from: B */
    private final lg f1722b;
    @NonNull

    /* renamed from: a */
    private final pc f1723c;
    @NonNull

    /* renamed from: d */
    private final pa f1724d;

    public pf(@NonNull Context context) {
        this(ld.m2146a(context).mo1242g(), ld.m2146a(context).mo1243h(), new og(context), new pb(), new oz());
    }

    /* renamed from: a */
    public pe mo1630a(int i) {
        long j = -1;
        Map b = this.f1721a.b(i);
        Map b2 = this.f1722b.b(i);
        rh.b bVar = new b();
        bVar.f1919b = m2803a(b);
        bVar.f1920c = m2804b(b2);
        long longValue = b.isEmpty() ? -1 : ((Long) Collections.max(b.keySet())).longValue();
        if (!b2.isEmpty()) {
            j = ((Long) Collections.max(b2.keySet())).longValue();
        }
        return new pe(longValue, j, bVar);
    }

    /* renamed from: a */
    public void mo1631a(pe peVar) {
        if (peVar.f1718a >= 0) {
            this.f1721a.mo1181b(peVar.f1718a);
        }
        if (peVar.f1719b >= 0) {
            this.f1722b.mo1181b(peVar.f1719b);
        }
    }

    /* renamed from: a */
    private C0820b[] m2803a(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            C0820b a = this.f1723c.mo1628a(((Long) entry.getKey()).longValue(), (String) entry.getValue());
            if (a != null) {
                arrayList.add(a);
            }
        }
        return (C0820b[]) arrayList.toArray(new C0820b[arrayList.size()]);
    }

    /* renamed from: B */
    private C0819a[] m2804b(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            C0819a a = this.f1724d.mo1626a(((Long) entry.getKey()).longValue(), (String) entry.getValue());
            if (a != null) {
                arrayList.add(a);
            }
        }
        return (C0819a[]) arrayList.toArray(new C0819a[arrayList.size()]);
    }

    @VisibleForTesting
    pf(@NonNull lh lhVar, @NonNull lg lgVar, @NonNull og ogVar, @NonNull pb pbVar, @NonNull oz ozVar) {
        this(lhVar, lgVar, new pc(ogVar, pbVar), new pa(ogVar, ozVar));
    }

    @VisibleForTesting
    pf(@NonNull lh lhVar, @NonNull lg lgVar, @NonNull pc pcVar, @NonNull pa paVar) {
        this.f1721a = lhVar;
        this.f1722b = lgVar;
        this.f1723c = pcVar;
        this.f1724d = paVar;
    }
}
