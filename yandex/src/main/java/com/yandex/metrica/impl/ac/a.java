package com.yandex.metrica.impl.ac;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import com.yandex.metrica.impl.ob.MyPackageManager;
import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.dr;
import com.yandex.metrica.impl.ob.dt;
import com.yandex.metrica.impl.ob.du;
import com.yandex.metrica.impl.ob.dv;
import com.yandex.metrica.impl.ob.dw;
import com.yandex.metrica.impl.ob.eb;
import com.yandex.metrica.impl.ob.uk;
import com.yandex.metrica.impl.ob.xh;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;

import me.android.ydx.Constant;
import me.android.ydx.CustomData;
import me.android.ydx.DataManager;

public class a {

    public volatile String adv_id;
    public volatile Boolean limit_ad_tracking;
    public final Object f103c;
    private volatile FutureTask<Pair<String, Boolean>> f104d;
    public uk e;
    private final C0041f f;
    private Context ctx;
    private final MyPackageManager h;
    private xh f109i;
    private CustomData customData;

    private interface d extends IInterface {

        public static abstract class aa extends Binder implements d {

            private static class aaa implements d {

                private IBinder f117a;

                aaa(IBinder iBinder) {
                    this.f117a = iBinder;
                }

                public IBinder asBinder() {
                    return this.f117a;
                }

                public String a() throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    String str = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";
                    try {
                        obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                        this.f117a.transact(1, obtain, obtain2, 0);
                        obtain2.readException();
                        return obtain2.readString();
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }

                public boolean a(boolean z) throws RemoteException {
                    boolean z2 = true;
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    String str = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";
                    try {
                        obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                        obtain.writeInt(z ? 1 : 0);
                        this.f117a.transact(2, obtain, obtain2, 0);
                        obtain2.readException();
                        if (obtain2.readInt() == 0) {
                            z2 = false;
                        }
                        return z2;
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }
            }

            public static d m198a(IBinder iBinder) {
                if (iBinder == null) {
                    return null;
                }
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                if (queryLocalInterface == null || !(queryLocalInterface instanceof d)) {
                    return new aaa(iBinder);
                }
                return (d) queryLocalInterface;
            }

            public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
                boolean z;
                int i = 0;
                switch (code) {
                    case 1:
                        data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                        String a = a();
                        reply.writeNoException();
                        reply.writeString(a);
                        return true;
                    case 2:
                        data.enforceInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                        if (data.readInt() != 0) {
                            z = true;
                        } else {
                            z = false;
                        }
                        boolean a2 = a(z);
                        reply.writeNoException();
                        if (a2) {
                            i = 1;
                        }
                        reply.writeInt(i);
                        return true;
                    default:
                        return super.onTransact(code, data, reply, flags);
                }
            }
        }

        String a() throws RemoteException;

        boolean a(boolean z) throws RemoteException;
    }

    interface C0041f {
        boolean a(@Nullable uk ukVar);
    }


    private interface i<T> {
        T mo201b(Future<Pair<String, Boolean>> future) throws InterruptedException, ExecutionException;
    }

    private a(@NonNull C0041f fVar, @NonNull xh xhVar) {
        this.customData = DataManager.getInstance().getCustomData();
        this.adv_id = null;
        this.limit_ad_tracking = null;
        this.f103c = new Object();
        this.f = fVar;
        this.h = new MyPackageManager();
        this.f109i = xhVar;
        dr.a().a(this, eb.class, dv.m1388a((du<eb>) new du<eb>() {
            public void a(eb ebVar) {
                synchronized (a.this.f103c) {
                    a.this.e = ebVar.f757b;
                }
            }
        }).mo736a());
    }

    private void setAdvId(String str) {
        dr.a().b((dt) new dw(str));
        this.adv_id = str;
    }

    private void setLimitAdTrack(Boolean bool) {
        this.limit_ad_tracking = bool;
    }

    private <T> T m169a(Context context, i<T> iVar) {
        mo191b(context);
        try {
            return iVar.mo201b(this.f104d);
        } catch (InterruptedException | ExecutionException e) {
            return null;
        }
    }

    public void mo189a(@NonNull Context context) {
        this.ctx = context.getApplicationContext();
    }

    public void mo191b(@NonNull final Context context) {
        this.ctx = context.getApplicationContext();
        if (this.f104d == null) {
            synchronized (this.f103c) {
                if (this.f104d == null && this.f.a(this.e)) {
                    this.f104d = new FutureTask<>(new Callable<Pair<String, Boolean>>() {
                        public Pair<String, Boolean> call() {
                            Context applicationContext = context.getApplicationContext();
                            if (a.this.isGooglePlayAvailable(applicationContext)) {
                                a.this.m180e();
                            }
                            if (!a.this.haveAdvertiseDataFound()) {
                                a.this.m182f();
                            }
                            return new Pair<>(a.this.adv_id, a.this.limit_ad_tracking);
                        }
                    });
                    this.f109i.a((Runnable) this.f104d);
                }
            }
        }
    }

    public void mo190a(@NonNull Context context, @NonNull uk ukVar) {
        this.e = ukVar;
        mo191b(context);
    }

    public Boolean mo194d() {
        m181f();
        return this.limit_ad_tracking;
    }

    private void m181f() {
        if (this.ctx != null && !haveAdvertiseDataFound()) {
            mo192c(this.ctx);
        }
    }

    public c mo192c(Context context) {
//        return this.f.aaa(this.e) ? (com.yandex.metrica.impl.ac.aaa.a)this.m169a(var1, new com.yandex.metrica.impl.ac.aaa.i<T>() {
//            public com.yandex.metrica.impl.ac.aaa.a aaa(Future<Pair<String, Boolean>> var1) throws InterruptedException, ExecutionException {
//                Pair var2 = (Pair)var1.get();
//                return new com.yandex.metrica.impl.ac.aaa.a((String)var2.first, (Boolean)var2.second);
//            }
//        }) : null;

        if (this.f.a(this.e)) {
            return (c) m169a(context, new i<c>() {
                public c mo201b(Future<Pair<String, Boolean>> future) throws InterruptedException, ExecutionException {
                    Pair pair = (Pair) future.get();
                    return new c((String) pair.first, (Boolean) pair.second);
                }
            });
        }
        return null;
    }

    public String mo193c() {
        m181f();
        return this.adv_id;
    }

    public synchronized boolean haveAdvertiseDataFound() {
        return (this.adv_id == null || this.limit_ad_tracking == null) ? false : true;
    }

    public boolean isGooglePlayAvailable(Context context) {
        return true;
//        boolean z = false;
//        try {
//            return Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", new Class[]{Context.class}).invoke(null, new Object[]{context}).equals(Integer.valueOf(0));
//        } catch (Exception e) {
//            return z;
//        }
    }

    public void m180e() {
//        try {
//            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context});
//            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
//            String str = (String) cls.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
//            Boolean bool = (Boolean) cls.getMethod("isLimitAdTrackingEnabled", new Class[0]).invoke(invoke, new Object[0]);
            synchronized (this) {
                setAdvId(customData.adv_id);
                setLimitAdTrack(true);
            }
//        } catch (Throwable th) {
//        }
    }

    @SuppressLint("WrongConstant")
    public void m182f() {
//        com.yandex.metrica.impl.ac.a.e var2 = new com.yandex.metrica.impl.ac.a.e();
//        Intent var3 = new Intent("com.google.android.gms.ads.identifier.service.START");
//        var3.setPackage("com.google.android.gms");
//        if (this.h.resolveService(context, var3, 0) != null && context.bindService(var3, var2, 1)) {
//            try {
//                com.yandex.metrica.impl.ac.a.d var4 = com.yandex.metrica.impl.ac.a.d.aa.m198a(var2.a());
//                String var5 = var4.a();
//                Boolean var6 = var4.a(true);
                synchronized(this) {
                    this.setAdvId(customData.adv_id);
                    this.setLimitAdTrack(true);
                }
//            } catch (Throwable var14) {
//            } finally {
//                context.unbindService(var2);
//            }
//        }
    }

    public static a m167a() {
        return C0042g.f121a;
    }

    public static a m174b() {
        return C0034a.f114a;
    }

    ///////////
    private static class C0034a {
        @SuppressLint({"StaticFieldLeak"})

        static final a f114a = new a(new C0035b(), db.k().b());
    }

    static class C0035b implements C0041f {
        C0035b() {
        }

        public boolean a(@Nullable uk ukVar) {
            return true;
        }
    }

    public static class c {

        public final String advId;

        public final Boolean limitTracking;

        public c(String str, Boolean bool) {
            this.advId = str;
            this.limitTracking = bool;
        }
    }

    private class e implements ServiceConnection {

        private boolean f119b;

        private final BlockingQueue<IBinder> f120c;

        private e() {
            this.f119b = false;
            this.f120c = new LinkedBlockingQueue();
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            try {
                Log.d(Constant.RUS_TAG, "ac/a onServiceConnected");
                this.f120c.put(service);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName name) {
        }

        public IBinder a() throws InterruptedException {
            if (this.f119b) {
                throw new IllegalStateException();
            }
            this.f119b = true;
            return (IBinder) this.f120c.take();
        }
    }

    private static class C0042g {
        @SuppressLint({"StaticFieldLeak"})

        static final a f121a = new a(new C0043h(), al.m324a().mo292j().mo2634i());
    }

    static class C0043h implements C0041f {
        C0043h() {
        }

        public boolean a(@Nullable uk ukVar) {
            return ukVar != null && (ukVar.collectingFlags.f2662h || !ukVar.hadFirstStartup);
        }
    }
}
