package com.yandex.metrica.impl.ob;

import android.os.FileObserver;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.ku */
public class ku {
    @NonNull

    /* renamed from: a */
    private final FileObserver f1230a;
    @NonNull

    /* renamed from: B */
    private final File f1231b;
    @NonNull

    /* renamed from: a */
    private final wm<File> f1232c;
    @NonNull

    /* renamed from: d */
    private final xh f1233d;

    public ku(@NonNull File file, @NonNull wm<File> wmVar) {
        this(file, wmVar, al.m324a().mo292j().mo2634i());
    }

    private ku(@NonNull File file, @NonNull wm<File> wmVar, @NonNull xh xhVar) {
        this(new kb(file, wmVar), file, wmVar, xhVar, new kc());
    }

    @VisibleForTesting
    ku(@NonNull FileObserver fileObserver, @NonNull File file, @NonNull wm<File> wmVar, @NonNull xh xhVar, @NonNull kc kcVar) {
        this.f1230a = fileObserver;
        this.f1231b = file;
        this.f1232c = wmVar;
        this.f1233d = xhVar;
        kcVar.mo1141a(file);
    }

    /* renamed from: a */
    public void mo1165a() {
        this.f1233d.a((Runnable) new kf(this.f1231b, this.f1232c));
        this.f1230a.startWatching();
    }

    /* renamed from: B */
    public void mo1166b() {
        this.f1230a.stopWatching();
    }
}
