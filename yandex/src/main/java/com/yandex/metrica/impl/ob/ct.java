package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.ct */
public class ct extends cf<String> {
    public ct(@NonNull Context context, @NonNull String str) {
        super(context, str, "string");
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public String mo604b(int i) {
        return this.f533a.getString(i);
    }
}
