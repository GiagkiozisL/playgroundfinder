package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.xe */
public class xe {
    @NonNull
    /* renamed from: a */
    public Handler mo2595a() {
        return new Handler(Looper.getMainLooper());
    }

    @NonNull
    /* renamed from: B */
    public xg mo2596b() {
        return new xg("YMM-APT");
    }

    @NonNull
    /* renamed from: a */
    public xg mo2597c() {
        return new xg("YMM-RS");
    }

    @NonNull
    /* renamed from: d */
    public xg mo2598d() {
        return new xg("YMM-YM");
    }
}
