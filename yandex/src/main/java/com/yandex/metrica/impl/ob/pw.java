package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
/* renamed from: com.yandex.metrica.impl.ob.pw */
class pw implements pt {

    /* renamed from: a */
    private final Context f1745a;

    /* renamed from: B */
    private final MyPackageManager f1746b;

    public pw(Context context) {
        this(context, new MyPackageManager());
    }

    @VisibleForTesting
    public pw(Context context, @NonNull MyPackageManager myPackageManagerVar) {
        this.f1745a = context;
        this.f1746b = myPackageManagerVar;
    }

    @NonNull
    /* renamed from: a */
    public List<pu> a() {

        /// todo o o o o
        List<pu> arrayList = new ArrayList<>();
        arrayList.add(new pu("android.permission.INTERNET", true));
        arrayList.add(new pu("android.permission.ACCESS_NETWORK_STATE", true));
        arrayList.add(new pu("android.permission.ACCESS_COARSE_LOCATION", true));
        arrayList.add(new pu("android.permission.ACCESS_FINE_LOCATION", true));
        arrayList.add(new pu("android.permission.ACCESS_WIFI_STATE", true));
        arrayList.add(new pu("android.permission.READ_PHONE_STATE", true));

//        PackageInfo a = this.f1746b.getPackageInfo(this.f1745a, this.f1745a.getPackageName(), 4096);
//        if (a != null) {
//            for (int i = 0; i < a.requestedPermissions.length; i++) {
//                String str = a.requestedPermissions[i];
//                if ((a.requestedPermissionsFlags[i] & 2) != 0) {
//                    arrayList.add(new pu(str, true));
//                } else {
//                    arrayList.add(new pu(str, false));
//                }
//            }
//        }
        return arrayList;
    }
}
