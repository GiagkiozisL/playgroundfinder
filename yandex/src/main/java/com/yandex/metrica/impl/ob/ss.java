package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ss */
public class ss {

    /* renamed from: a */
    private final Map<String, String> aHardMap = new HashMap();

    public ss() {
        this.aHardMap.put("android_id", "a");
        this.aHardMap.put("background_location_collection", "blc");
        this.aHardMap.put("background_lbs_collection", "blbc");
        this.aHardMap.put("easy_collecting", "ec");
        this.aHardMap.put("access_point", "ap");
        this.aHardMap.put("cells_around", "ca");
        this.aHardMap.put("google_aid", "g");
        this.aHardMap.put("own_macs", "om");
        this.aHardMap.put("sim_imei", "sm");
        this.aHardMap.put("sim_info", "si");
        this.aHardMap.put("wifi_around", "wa");
        this.aHardMap.put("wifi_connected", "wc");
        this.aHardMap.put("features_collecting", "fc");
        this.aHardMap.put("foreground_location_collection", "flc");
        this.aHardMap.put("foreground_lbs_collection", "flbc");
        this.aHardMap.put("package_info", "pi");
        this.aHardMap.put("permissions_collecting", "pc");
        this.aHardMap.put("sdk_list", "sl");
        this.aHardMap.put("socket", "s");
        this.aHardMap.put("telephony_restricted_to_location_tracking", "trtlt");
        this.aHardMap.put("identity_light_collecting", "ilc");
        this.aHardMap.put("ble_collecting", "bc");
    }

    @NonNull
    /* renamed from: a */
    public String getHardMap(@NonNull String str) {
        if (this.aHardMap.containsKey(str)) {
            return (String) this.aHardMap.get(str);
        }
        return str;
    }
}
