package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.yandex.metrica.impl.ob.kw */
public abstract class kw {
    @NonNull

    /* renamed from: a */
    private final lc f1243a;
    @NonNull

    /* renamed from: B */
    private final ly f1244b;
    @NonNull

    /* renamed from: a */
    private final AtomicLong f1245c;
    @NonNull

    /* renamed from: d */
    private final AtomicLong f1246d;
    @NonNull

    /* renamed from: e */
    private final AtomicLong f1247e;
    @NonNull

    /* renamed from: f */
    private final ContentValues f = new ContentValues();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract long mo1186c(long j);

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract ly mo1189d(long j);

    /* renamed from: e */
    public abstract String e();

    kw(@NonNull lc lcVar, @NonNull ly lyVar) {
        this.f1243a = lcVar;
        this.f1244b = lyVar;
        this.f1245c = new AtomicLong(m2073f());
        this.f1246d = new AtomicLong(mo1178a(Long.MAX_VALUE));
        this.f1247e = new AtomicLong(mo1186c(-1));
    }

    /* renamed from: a */
    public long mo1177a() {
        return this.f1245c.get();
    }

    /* renamed from: B */
    public long mo1182b() {
        return this.f1246d.get();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public ly mo1187c() {
        return this.f1244b;
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: d */
    public lc d() {
        return this.f1243a;
    }

    /* renamed from: f */
    private long m2073f() {
        try {
            SQLiteDatabase readableDatabase = d().getReadableDatabase();
            if (readableDatabase != null) {
                return vl.a(readableDatabase, e());
            }
        } catch (Throwable th) {
        }
        return 0;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1180a(long j, String str) {
        this.f1245c.incrementAndGet();
        this.f1247e.incrementAndGet();
        mo1189d(this.f1247e.get()).mo1364q();
        if (this.f1246d.get() > j) {
            this.f1246d.set(j);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1179a(int i) {
        this.f1245c.getAndAdd((long) (-i));
        this.f1246d.set(mo1178a(Long.MAX_VALUE));
    }

    /* renamed from: a */
    public long mo1178a(long j) {
        Cursor cursor = null;
        String format = String.format(Locale.US, "Select min(%s) from %s", new Object[]{"timestamp", e()});
        try {
            SQLiteDatabase readableDatabase = d().getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.rawQuery(format, null);
                if (cursor.moveToFirst()) {
                    long j2 = cursor.getLong(0);
                    if (j2 != 0) {
                        j = j2;
                    }
                }
            }
        } catch (Throwable th) {
        } finally {
            cx.m1183a(cursor);
        }
        return j;
    }

    /* renamed from: B */
    public synchronized void mo1184b(long j, String str) {
        try {
            SQLiteDatabase writableDatabase = d().getWritableDatabase();
            if (writableDatabase != null) {
                if (writableDatabase.insert(e(), null, m2072c(j, str)) != -1) {
                    mo1180a(j, str);
                }
            }
        } catch (Throwable th) {
        }
    }

    @NonNull
    /* renamed from: a */
    private ContentValues m2072c(long j, @NonNull String str) {
        this.f.clear();
        this.f.put("incremental_id", Long.valueOf(this.f1247e.get() + 1));
        this.f.put("timestamp", Long.valueOf(j));
        this.f.put("data", str);
        return this.f;
    }


    public synchronized java.util.Map<Long, String> b(int var1) {

        LinkedHashMap var2 = new LinkedHashMap();
        Cursor var3 = null;

        try {
            SQLiteDatabase var4 = this.d().getReadableDatabase();
            if (var4 != null) {
                var3 = var4.query(this.e(), new String[]{"incremental_id", "data"}, (String)null, (String[])null, (String)null, (String)null, "incremental_id ASC", String.valueOf(var1));

                while(var3.moveToNext()) {
                    this.f.clear();
                    vl.a(var3, this.f);
                    vj.a(var2, this.f.getAsLong("incremental_id"), this.f.getAsString("data"));
                }
            }
        } catch (Throwable var8) {
        } finally {
            cx.a(var3);
        }

        return var2;

    }

    /* renamed from: B */
    public synchronized int mo1181b(long j) {
        int i = 0;
        synchronized (this) {
            String format = String.format(Locale.US, "%s <= ?", new Object[]{"incremental_id"});
            try {
                SQLiteDatabase writableDatabase = d().getWritableDatabase();
                if (writableDatabase != null) {
                    i = writableDatabase.delete(e(), format, new String[]{String.valueOf(j)});
                    if (i > 0) {
                        mo1179a(i);
                    }
                }
            } catch (Throwable th) {
            }
        }
        return i;
    }

    /* renamed from: a */
    public synchronized int mo1185c(int i) {
        int i2 = 0;
        synchronized (this) {
            if (i >= 1) {
                String format = String.format(Locale.US, "%1$s <= (select max(%1$s) from (select %1$s from %2$s order by %1$s limit ?))", new Object[]{"incremental_id", e()});
                try {
                    SQLiteDatabase writableDatabase = d().getWritableDatabase();
                    if (writableDatabase != null) {
                        i2 = writableDatabase.delete(e(), format, new String[]{String.valueOf(i)});
                        if (i2 > 0) {
                            mo1179a(i2);
                        }
                    }
                } catch (Throwable th) {
                }
            }
        }
        return i2;
    }
}
