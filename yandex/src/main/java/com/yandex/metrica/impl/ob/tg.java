package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.tg */
public class tg {
    @NonNull

    /* renamed from: a */
    private final mf<te> f2569a;
    @NonNull

    /* renamed from: B */
    private final tf f2570b;
    @NonNull

    /* renamed from: a */
    private final Context f2571c;

    public tg(@NonNull Context context, @NonNull mf<te> mfVar) {
        this(context, mfVar, new tf());
    }

    public tg(@NonNull Context context, @NonNull mf<te> mfVar, @NonNull tf tfVar) {
        this.f2569a = mfVar;
        this.f2571c = context;
        this.f2570b = tfVar;
    }

    /* renamed from: a */
    public void mo2237a() {
        tl.a(this.f2571c).reportEvent("sdk_list", this.f2570b.mo2236b(this.f2570b.mo2235a(((te) this.f2569a.a()).f2564a)).toString());
    }
}
