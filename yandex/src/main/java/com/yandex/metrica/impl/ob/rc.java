package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.rc */
public class rc extends qr<String> {

    /* renamed from: a */
    private final yb<String> f1875a;

    public rc(@NonNull String str, @NonNull String str2, @NonNull yb<String> ybVar, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(0, str, str2, ykVar, qoVar);
        this.f1875a = ybVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1750a(@NonNull rl.a.Aa aVar) {
        String str = (String) this.f1875a.a(mo1752b());
        aVar.f2049e.f2052b = str == null ? new byte[0] : str.getBytes();
    }
}
