package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.vj */
public class vj {
    /* renamed from: a */
    public static boolean a(@Nullable Collection<?> collection, @Nullable Collection<?> collection2) {
        HashSet hashSet;
        Collection<?> collection3;
        if (collection == null && collection2 == null) {
            return true;
        }
        if (collection == null || collection2 == null) {
            return false;
        }
        if (collection.size() != collection2.size()) {
            return false;
        }
        if (collection instanceof HashSet) {
            collection3 = collection2;
            hashSet = (HashSet) collection;
        } else if (collection2 instanceof HashSet) {
            collection3 = collection;
            hashSet = (HashSet) collection2;
        } else {
            hashSet = new HashSet(collection);
            collection3 = collection2;
        }
        for (Object contains : collection3) {
            if (!hashSet.contains(contains)) {
                return false;
            }
        }
        return true;
    }

    @NonNull
    /* renamed from: a */
    public static List<Integer> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(Integer.valueOf(valueOf));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public static int[] a(@NonNull List<Integer> list) {
        int i = 0;
        int[] iArr = new int[list.size()];
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return iArr;
            }
            iArr[i2] = ((Integer) it.next()).intValue();
            i = i2 + 1;
        }
    }

    /* renamed from: a */
    public static <K, V> void a(@NonNull Map<K, V> map, @Nullable K k, @Nullable V v) {
        if (k != null && v != null) {
            map.put(k, v);
        }
    }
}
