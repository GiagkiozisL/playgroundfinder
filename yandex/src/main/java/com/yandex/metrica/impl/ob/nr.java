package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.nr */
public class nr {
    @NonNull

    /* renamed from: a */
    private final nq f1542a;

    /* renamed from: B */
    private final nt f1543b;

    /* renamed from: a */
    private final long f1544c;

    /* renamed from: d */
    private final boolean f1545d;

    /* renamed from: e */
    private final long f1546e;

    public nr(@NonNull JSONObject jSONObject, long j) throws JSONException {
        this.f1542a = new nq(jSONObject.optString("device_id", null), jSONObject.optString("device_id_hash", null));
        if (jSONObject.has("device_snapshot_key")) {
            this.f1543b = new nt(jSONObject.optString("device_snapshot_key", null));
        } else {
            this.f1543b = null;
        }
        this.f1544c = jSONObject.optLong("last_elections_time", -1);
        this.f1545d = m2608d();
        this.f1546e = j;
    }

    public nr(@NonNull nq nqVar, @NonNull nt ntVar, long j) {
        this.f1542a = nqVar;
        this.f1543b = ntVar;
        this.f1544c = j;
        this.f1545d = m2608d();
        this.f1546e = -1;
    }

    /* renamed from: a */
    public String mo1508a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("device_id", this.f1542a.f1540a);
        jSONObject.put("device_id_hash", this.f1542a.f1541b);
        if (this.f1543b != null) {
            jSONObject.put("device_snapshot_key", this.f1543b.mo1519b());
        }
        jSONObject.put("last_elections_time", this.f1544c);
        return jSONObject.toString();
    }

    @NonNull
    /* renamed from: B */
    public nq mo1509b() {
        return this.f1542a;
    }

    @Nullable
    /* renamed from: a */
    public nt mo1510c() {
        return this.f1543b;
    }

    /* renamed from: d */
    private boolean m2608d() {
        if (this.f1544c <= -1 || System.currentTimeMillis() - this.f1544c >= 604800000) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Credentials{mIdentifiers=" + this.f1542a + ", mDeviceSnapshot=" + this.f1543b + ", mLastElectionsTime=" + this.f1544c + ", mFresh=" + this.f1545d + ", mLastModified=" + this.f1546e + '}';
    }
}
