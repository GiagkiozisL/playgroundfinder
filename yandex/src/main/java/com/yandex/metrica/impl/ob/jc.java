package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.jc */
public class jc {

    /* renamed from: a */
    private final lw f1110a;

    public jc(@NonNull lw lwVar) {
        this.f1110a = lwVar;
    }

    /* renamed from: a */
    public long mo1071a() {
        long j = 10000000000L;
        long j2 = this.f1110a.mo1344j();
        if (j2 >= 10000000000L) {
            j = 1 + j2;
        }
        this.f1110a.mo1337e(j).mo1364q();
        return j;
    }
}
