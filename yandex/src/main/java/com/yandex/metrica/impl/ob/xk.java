package com.yandex.metrica.impl.ob;

import android.os.HandlerThread;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.xk */
public class xk extends HandlerThread implements xj {

    /* renamed from: a */
    private volatile boolean f2970a = true;

    public xk(@NonNull String str) {
        super(str);
    }

    /* renamed from: a */
    public synchronized boolean c() {
        return this.f2970a;
    }
}
