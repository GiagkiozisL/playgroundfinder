package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rg.C0813d;

import java.util.Iterator;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.kp */
public class kp implements mq<List<StackTraceElement>, C0813d[]> {
    @NonNull

    /* renamed from: a */
    private kq f1224a = new kq();

    @NonNull
    @Override
    /* renamed from: a */
    public C0813d[] b(@NonNull List<StackTraceElement> list) {
        C0813d[] dVarArr = new C0813d[list.size()];
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return dVarArr;
            }
            dVarArr[i2] = this.f1224a.b((StackTraceElement) it.next());
            i = i2 + 1;
        }
    }

    @NonNull
    @Override
    /* renamed from: a */
    public List<StackTraceElement> a(C0813d[] dVarArr) {
        throw new UnsupportedOperationException();
    }
}
