package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.is */
public class is {
    @NonNull

    /* renamed from: a */
    private final iq f1058a;
    @NonNull

    /* renamed from: B */
    private final ir f1059b;
    @NonNull

    /* renamed from: a */
    private final kx f1060c;
    @NonNull

    /* renamed from: d */
    private final String f1061d;

    public is(@NonNull Context context, @NonNull ek ekVar) {
        this(new ir(), new iq(), ld.m2146a(context).mo1237c(ekVar), "event_hashes");
    }

    @VisibleForTesting
    is(@NonNull ir irVar, @NonNull iq iqVar, @NonNull kx kxVar, @NonNull String str) {
        this.f1059b = irVar;
        this.f1058a = iqVar;
        this.f1060c = kxVar;
        this.f1061d = str;
    }

    @NonNull
    /* renamed from: a */
    public ip mo1028a() {
        try {
            byte[] a = this.f1060c.mo1192a(this.f1061d);
            if (cx.nullOrEmpty(a)) {
                return this.f1058a.a(this.f1059b.c());
            }
            return this.f1058a.a(this.f1059b.b(a));
        } catch (Throwable th) {
            return this.f1058a.a(this.f1059b.c());
        }
    }

    /* renamed from: a */
    public void mo1029a(@NonNull ip ipVar) {
        this.f1060c.mo1191a(this.f1061d, this.f1059b.a(this.f1058a.b(ipVar)));
    }
}
