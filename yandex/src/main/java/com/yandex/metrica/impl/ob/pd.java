package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.util.Collection;

/* renamed from: com.yandex.metrica.impl.ob.pd */
public class pd extends bp<sx> {
    @NonNull

    /* renamed from: j */
    private Context f1710j;
    @NonNull

    /* renamed from: k */
    private ph f1711k;
    @NonNull

    /* renamed from: l */
    private final cs f1712l;
    @NonNull

    /* renamed from: m */
    private oh f1713m;
    @NonNull

    /* renamed from: n */
    private ly f1714n;
    @NonNull

    /* renamed from: o */
    private final pf f1715o;

    /* renamed from: p */
    private long f1716p;

    /* renamed from: q */
    private pe f1717q;

    public pd(@NonNull Context context, @NonNull ph phVar, @NonNull cs csVar) {
        this(context, phVar, csVar, new ly(ld.m2146a(context).mo1238c()), new sx(), new pf(context));
    }

    /* renamed from: a */
    public boolean mo461a() {
        if (!this.f1712l.mo655d() && !TextUtils.isEmpty(this.f1711k.getDeviceId()) && !TextUtils.isEmpty(this.f1711k.getUUId()) && !cx.a((Collection) mo481s())) {
            return m2793H();
        }
        return false;
    }

    /* renamed from: B */
    public boolean mo463b() {
        boolean b = super.mo463b();
        m2795J();
        return b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo451a(@NonNull Builder builder) {
        ((sx) this.f388i).mo2210a(builder, this.f1711k);
    }

    /* access modifiers changed from: protected */
    /* renamed from: E */
    public void mo489E() {
        m2792G();
    }

    /* access modifiers changed from: protected */
    /* renamed from: F */
    public void mo490F() {
        m2792G();
    }

    /* renamed from: G */
    private void m2792G() {
        this.f1715o.mo1631a(this.f1717q);
    }

    /* renamed from: t */
    public boolean mo482t() {
        return (400 != mo473k()) & super.mo482t();
    }

    /* renamed from: H */
    private boolean m2793H() {
        this.f1717q = this.f1715o.mo1630a(this.f1713m.f1574h);
        if (!this.f1717q.mo1629a()) {
            return mo491c(e.m1395a((e) this.f1717q.f1720c));
        }
        return false;
    }

    /* renamed from: I */
    private void m2794I() {
        this.f1716p = this.f1714n.mo1374b(-1) + 1;
        ((sx) this.f388i).mo2208a(this.f1716p);
    }

    /* renamed from: J */
    private void m2795J() {
        this.f1714n.mo1379c(this.f1716p).mo1364q();
    }

    @VisibleForTesting
    pd(@NonNull Context context, @NonNull ph phVar, @NonNull cs csVar, @NonNull ly lyVar, @NonNull sx sxVar, @NonNull pf pfVar) {
        super(sxVar);
        this.f1710j = context;
        this.f1711k = phVar;
        this.f1712l = csVar;
        this.f1713m = this.f1711k.mo1633a();
        this.f1714n = lyVar;
        this.f1715o = pfVar;
        m2794I();
        mo457a(this.f1711k.mo1634b());
    }
}
