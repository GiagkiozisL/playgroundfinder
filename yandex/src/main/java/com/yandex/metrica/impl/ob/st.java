package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.impl.ob.eg.C0306a;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.st */
public class st extends sq {

    /* renamed from: a */
    private boolean f2468a;

    /* renamed from: B */
    private Location f2469b;

    /* renamed from: a */
    private boolean f2470c;

    /* renamed from: d */
    private boolean f2471d;

    /* renamed from: e */
    private boolean f2472e;

    /* renamed from: f */
    private int f2473f;

    /* renamed from: g */
    private int f2474g;

    /* renamed from: h */
    private boolean f2475h;

    /* renamed from: i */
    private int f2476i;

    /* renamed from: j */
    private Boolean f2477j;

    /* renamed from: k */
    private C0987d f2478k;

    /* renamed from: l */
    private String f2479l;

    /* renamed from: m */
    private boolean f2480m;

    /* renamed from: n */
    private boolean f2481n;

    /* renamed from: o */
    private boolean f2482o;

    /* renamed from: p */
    private boolean f2483p;

    /* renamed from: q */
    private String f2484q;

    /* renamed from: r */
    private List<String> f2485r;

    /* renamed from: s */
    private boolean f2486s;

    /* renamed from: t */
    private int f2487t;

    /* renamed from: u */
    private long f2488u;

    /* renamed from: v */
    private long f2489v;

    /* renamed from: w */
    private boolean f2490w;

    /* renamed from: x */
    private long f2491x;
    @Nullable

    /* renamed from: y */
    private List<String> f2492y;

    /* renamed from: com.yandex.metrica.impl.ob.st$a */
    public static final class C0984a extends C0972a<C0306a, C0984a> {
        @Nullable

        /* renamed from: a */
        public final String f2493a;
        @Nullable

        /* renamed from: B */
        public final Location f2494b;

        /* renamed from: f */
        public final boolean f2495f;

        /* renamed from: g */
        public final boolean f2496g;

        /* renamed from: h */
        public final boolean f2497h;

        /* renamed from: i */
        public final int f2498i;

        /* renamed from: j */
        public final int f2499j;

        /* renamed from: k */
        public final int f2500k;

        /* renamed from: l */
        public final boolean f2501l;

        /* renamed from: m */
        public final boolean f2502m;

        /* renamed from: n */
        public final boolean f2503n;
        @Nullable

        /* renamed from: o */
        public final Map<String, String> f2504o;

        /* renamed from: p */
        public final int f2505p;

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo2176a(@Nullable Location location, @Nullable Location location2) {
            boolean z = true;
            if (location == location2) {
                return true;
            }
            if (location == null || location2 == null || location.getTime() != location2.getTime()) {
                return false;
            }
            if ((cx.a(17) && location.getElapsedRealtimeNanos() != location2.getElapsedRealtimeNanos()) || Double.compare(location2.getLatitude(), location.getLatitude()) != 0 || Double.compare(location2.getLongitude(), location.getLongitude()) != 0 || Double.compare(location2.getAltitude(), location.getAltitude()) != 0 || Float.compare(location2.getSpeed(), location.getSpeed()) != 0 || Float.compare(location2.getBearing(), location.getBearing()) != 0 || Float.compare(location2.getAccuracy(), location.getAccuracy()) != 0) {
                return false;
            }
            if (cx.a(26) && (Float.compare(location2.getVerticalAccuracyMeters(), location.getVerticalAccuracyMeters()) != 0 || Float.compare(location2.getSpeedAccuracyMetersPerSecond(), location.getSpeedAccuracyMetersPerSecond()) != 0 || Float.compare(location2.getBearingAccuracyDegrees(), location.getBearingAccuracyDegrees()) != 0)) {
                return false;
            }
            if (location.getProvider() != null) {
                if (!location.getProvider().equals(location2.getProvider())) {
                    return false;
                }
            } else if (location2.getProvider() != null) {
                return false;
            }
            if (location.getExtras() != null) {
                z = location.getExtras().equals(location2.getExtras());
            } else if (location2.getExtras() != null) {
                z = false;
            }
            return z;
        }

        public C0984a(@NonNull C0306a aVar) {
            this(aVar.f768a, aVar.f769b, aVar.f770c, aVar.f771d, aVar.f772e, aVar.f773f, aVar.f774g, aVar.f775h, aVar.f781n, aVar.f776i, aVar.f777j, aVar.f778k, aVar.f779l, aVar.f780m, aVar.f782o, aVar.f783p);
        }

        C0984a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map, @Nullable Integer num4) {
            super(str, str2, str3);
            this.f2493a = str4;
            this.f2495f = wk.m4371a(bool, true);
            this.f2494b = location;
            this.f2496g = wk.m4371a(bool2, false);
            this.f2497h = wk.m4371a(bool3, false);
            this.f2503n = wk.m4371a(bool4, false);
            this.f2498i = Math.max(10, wk.m4366a(num, 10));
            this.f2499j = wk.m4366a(num2, 7);
            this.f2500k = wk.m4366a(num3, 90);
            this.f2501l = wk.m4371a(bool5, false);
            this.f2502m = wk.m4371a(bool6, true);
            this.f2504o = map;
            this.f2505p = wk.m4366a(num4, (int) YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_IN_DATABASE_COUNT);
        }

        @NonNull
        /* renamed from: a */
        public C0984a b(@NonNull C0306a aVar) {
            return new C0984a((String) wk.m4369a(aVar.f768a, this.f2439c), (String) wk.m4369a(aVar.f769b, this.f2440d), (String) wk.m4369a(aVar.f770c, this.f2441e), (String) wk.m4369a(aVar.f771d, this.f2493a), (Boolean) wk.m4369a(aVar.f772e, Boolean.valueOf(this.f2495f)), (Location) wk.m4369a(aVar.f773f, this.f2494b), (Boolean) wk.m4369a(aVar.f774g, Boolean.valueOf(this.f2496g)), (Boolean) wk.m4369a(aVar.f775h, Boolean.valueOf(this.f2497h)), aVar.f781n, (Integer) wk.m4369a(aVar.f776i, Integer.valueOf(this.f2498i)), (Integer) wk.m4369a(aVar.f777j, Integer.valueOf(this.f2499j)), (Integer) wk.m4369a(aVar.f778k, Integer.valueOf(this.f2500k)), (Boolean) wk.m4369a(aVar.f779l, Boolean.valueOf(this.f2501l)), (Boolean) wk.m4369a(aVar.f780m, Boolean.valueOf(this.f2502m)), (Map) wk.m4369a(aVar.f782o, this.f2504o), (Integer) wk.m4369a(aVar.f783p, Integer.valueOf(this.f2505p)));
        }

        /* renamed from: B */
        public boolean a(@NonNull C0306a aVar) {
            if (aVar.f768a != null && !aVar.f768a.equals(this.f2439c)) {
                return false;
            }
            if (aVar.f769b != null && !aVar.f769b.equals(this.f2440d)) {
                return false;
            }
            if (aVar.f770c != null && !aVar.f770c.equals(this.f2441e)) {
                return false;
            }
            if (aVar.f772e != null && this.f2495f != aVar.f772e.booleanValue()) {
                return false;
            }
            if (aVar.f774g != null && this.f2496g != aVar.f774g.booleanValue()) {
                return false;
            }
            if (aVar.f775h != null && this.f2497h != aVar.f775h.booleanValue()) {
                return false;
            }
            if (aVar.f776i != null && this.f2498i != aVar.f776i.intValue()) {
                return false;
            }
            if (aVar.f777j != null && this.f2499j != aVar.f777j.intValue()) {
                return false;
            }
            if (aVar.f778k != null && this.f2500k != aVar.f778k.intValue()) {
                return false;
            }
            if (aVar.f779l != null && this.f2501l != aVar.f779l.booleanValue()) {
                return false;
            }
            if (aVar.f780m != null && this.f2502m != aVar.f780m.booleanValue()) {
                return false;
            }
            if (aVar.f781n != null && this.f2503n != aVar.f781n.booleanValue()) {
                return false;
            }
            if (aVar.f771d != null && (this.f2493a == null || !this.f2493a.equals(aVar.f771d))) {
                return false;
            }
            if (aVar.f782o != null && (this.f2504o == null || !this.f2504o.equals(aVar.f782o))) {
                return false;
            }
            if (aVar.f783p != null && this.f2505p != aVar.f783p.intValue()) {
                return false;
            }
            if (aVar.f773f == null || mo2176a(this.f2494b, aVar.f773f)) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.st$B */
    public static abstract class C0985b implements C0987d {
        @NonNull

        /* renamed from: a */
        protected final cs f2506a;

        public C0985b(@NonNull cs csVar) {
            this.f2506a = csVar;
        }

        /* renamed from: a */
        public boolean mo892a(@Nullable Boolean bool) {
            return wk.m4371a(bool, true);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.st$a */
    public static class C0986c extends C0980a<st, C0984a> {
        @NonNull

        /* renamed from: a */
        private final en f2507c;
        @NonNull

        /* renamed from: d */
        private final C0987d f2508d;

        public C0986c(@NonNull en enVar, @NonNull C0987d dVar) {
            super(enVar.mo824k(), enVar.b().mo791b());
            this.f2507c = enVar;
            this.f2508d = dVar;
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public st b() {
            return new st();
        }

        @NonNull
        /* renamed from: a */
        public st a(@NonNull c cVar) {
            st stVar = (st) super.c(cVar);
            stVar.m3634m(((C0984a) cVar.b).f2493a);
            stVar.m3633k(this.f2507c.mo837x());
            stVar.mo2167d(this.f2507c.mo829p());
            stVar.mo2161b(this.f2507c.mo810A().mo1670a());
            stVar.mo2169e(((C0984a) cVar.b).f2495f);
            stVar.mo2153a(((C0984a) cVar.b).f2494b);
            stVar.mo2170f(((C0984a) cVar.b).f2496g);
            stVar.mo2171g(((C0984a) cVar.b).f2497h);
            stVar.mo2151a(((C0984a) cVar.b).f2498i);
            stVar.mo2164c(((C0984a) cVar.b).f2499j);
            stVar.mo2159b(((C0984a) cVar.b).f2500k);
            stVar.mo2173i(((C0984a) cVar.b).f2501l);
            stVar.mo2172h(((C0984a) cVar.b).f2503n);
            stVar.a(Boolean.valueOf(((C0984a) cVar.b).f2502m), this.f2508d);
            stVar.mo2165c((long) ((C0984a) cVar.b).f2505p);
            a(stVar, cVar.f2444a, ((C0984a) cVar.b).f2504o);
            return stVar;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: a */
        public void a(@NonNull st stVar, @NonNull uk ukVar, @Nullable Map<String, String> map) {
            a(stVar, ukVar);
            b(stVar, ukVar);
            stVar.mo2155a(ukVar.encodedClidsFromResponse);
            stVar.mo2174j(a(map, we.m4340a(ukVar.lastStartupRequestClids)));
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean a(@Nullable Map<String, String> map, @Nullable Map<String, String> map2) {
            return map == null || map.isEmpty() || map.equals(map2);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void a(st stVar, uk ukVar) {
            stVar.mo2156a(ukVar.reportUrls);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public void b(st stVar, uk ukVar) {
            stVar.mo2157a(ukVar.collectingFlags.f2655a);
            stVar.mo2162b(ukVar.collectingFlags.f2656b);
            stVar.mo2166c(ukVar.collectingFlags.f2657c);
            if (ukVar.permissionsCollectingConfig != null) {
                stVar.mo2152a(ukVar.permissionsCollectingConfig.f2702a);
                stVar.mo2160b(ukVar.permissionsCollectingConfig.f2703b);
            }
            stVar.mo2168d(ukVar.collectingFlags.f2658d);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.st$d */
    public interface C0987d {
        /* renamed from: a */
        boolean mo892a(@Nullable Boolean bool);
    }

    @VisibleForTesting
    st() {
    }

    @NonNull
    /* renamed from: a */
    public String mo2150a() {
        return wk.m4373b(this.f2484q, "");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2155a(String str) {
        this.f2484q = str;
    }

    /* renamed from: a */
    public void mo2156a(List<String> list) {
        this.f2485r = list;
    }

    /* renamed from: B */
    public List<String> mo2158b() {
        return this.f2485r;
    }

    /* renamed from: a */
    public String getApiKey128() {
        return this.f2479l;
    }

    /* renamed from: F */
    public boolean mo2129F() {
        return this.f2480m;
    }

    /* renamed from: a */
    public void mo2157a(boolean z) {
        this.f2480m = z;
    }

    /* renamed from: G */
    public boolean mo2130G() {
        return this.f2481n;
    }

    /* renamed from: H */
    public boolean mo2131H() {
        return this.f2482o;
    }

    /* renamed from: I */
    public boolean mo2132I() {
        return this.f2483p;
    }

    /* renamed from: a */
    public void mo2152a(long j) {
        this.f2488u = j;
    }

    /* renamed from: J */
    public long mo2133J() {
        return this.f2488u;
    }

    /* renamed from: B */
    public void mo2160b(long j) {
        this.f2489v = j;
    }

    /* renamed from: K */
    public long mo2134K() {
        return this.f2489v;
    }

    /* renamed from: B */
    public void mo2162b(boolean z) {
        this.f2481n = z;
    }

    /* renamed from: a */
    public void mo2166c(boolean z) {
        this.f2482o = z;
    }

    /* renamed from: d */
    public void mo2168d(boolean z) {
        this.f2483p = z;
    }

    /* renamed from: L */
    public boolean mo2135L() {
        return mo2088f() && !cx.a((Collection) mo2158b()) && mo2149Z();
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public void m3634m(String str) {
        this.f2479l = str;
    }

    /* renamed from: M */
    public boolean mo2136M() {
        return this.f2486s;
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m3633k(boolean z) {
        this.f2486s = z;
    }

    /* renamed from: N */
    public boolean mo2137N() {
        return this.f2468a;
    }

    /* renamed from: e */
    public void mo2169e(boolean z) {
        this.f2468a = z;
    }

    /* renamed from: O */
    public Location mo2138O() {
        return this.f2469b;
    }

    /* renamed from: a */
    public void mo2153a(Location location) {
        this.f2469b = location;
    }

    /* renamed from: P */
    public boolean mo2139P() {
        return this.f2470c;
    }

    /* renamed from: f */
    public void mo2170f(boolean z) {
        this.f2470c = z;
    }

    /* renamed from: Q */
    public boolean mo2140Q() {
        return this.f2471d;
    }

    /* renamed from: g */
    public void mo2171g(boolean z) {
        this.f2471d = z;
    }

    /* renamed from: R */
    public boolean mo2141R() {
        return this.f2472e;
    }

    /* renamed from: h */
    public void mo2172h(boolean z) {
        this.f2472e = z;
    }

    /* renamed from: S */
    public int mo2142S() {
        return this.f2473f;
    }

    /* renamed from: a */
    public void mo2151a(int i) {
        this.f2473f = i;
    }

    /* renamed from: T */
    public int mo2143T() {
        return this.f2474g;
    }

    /* renamed from: B */
    public void mo2159b(int i) {
        this.f2474g = i;
    }

    /* renamed from: i */
    public void mo2173i(boolean z) {
        this.f2475h = z;
    }

    /* renamed from: U */
    public int mo2144U() {
        return this.f2476i;
    }

    /* renamed from: a */
    public void mo2164c(int i) {
        this.f2476i = i;
    }

    /* renamed from: V */
    public int mo2145V() {
        return this.f2487t;
    }

    /* renamed from: d */
    public void mo2167d(int i) {
        this.f2487t = i;
    }

    /* renamed from: W */
    public long mo2146W() {
        return this.f2491x;
    }

    /* renamed from: a */
    public void mo2165c(long j) {
        this.f2491x = j;
    }

    /* renamed from: X */
    public boolean mo2147X() {
//        return true;// todo bypassed
        return this.f2478k.mo892a(this.f2477j);
    }

    @Nullable
    /* renamed from: Y */
    public List<String> mo2148Y() {
        return this.f2492y;
    }

    /* renamed from: B */
    public void mo2161b(@NonNull List<String> list) {
        this.f2492y = list;
    }

    /* renamed from: a */
    public void a(@Nullable Boolean bool, @NonNull C0987d dVar) {
        this.f2477j = bool;
        this.f2478k = dVar;
    }

    /* renamed from: Z */
    public boolean mo2149Z() {
        return this.f2490w;
    }

    /* renamed from: j */
    public void mo2174j(boolean z) {
        this.f2490w = z;
    }
}
