package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.xq */
abstract class xq<T> implements yb<T> {
    @NonNull

    /* renamed from: a */
    protected final vz f2984a;

    /* renamed from: B */
    private final int f2985b;

    /* renamed from: a */
    private final String f2986c;

    public xq(int i, @NonNull String str, @NonNull vz vzVar) {
        this.f2985b = i;
        this.f2986c = str;
        this.f2984a = vzVar;
    }

    @VisibleForTesting(otherwise = 3)
    /* renamed from: a */
    public int mo2635a() {
        return this.f2985b;
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    /* renamed from: B */
    public String mo2636b() {
        return this.f2986c;
    }
}
