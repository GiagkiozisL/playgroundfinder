package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.gh.C0386a;

import java.util.Collection;

/* renamed from: com.yandex.metrica.impl.ob.gg */
public class gg extends bs<sv> {
    @NonNull

    /* renamed from: j */
    private final gj f949j;
    @NonNull

    /* renamed from: k */
    private final w f950k;
    @NonNull

    /* renamed from: l */
    private final gk f951l;
    @NonNull

    /* renamed from: m */
    private final C0386a f952m;
    @NonNull

    /* renamed from: n */
    private final wh f953n;
    @NonNull

    /* renamed from: o */
    private vm f954o;
    @NonNull

    /* renamed from: p */
    private final String f955p;
    @NonNull

    /* renamed from: q */
    private final lw f956q;
    @Nullable

    /* renamed from: r */
    private gi f957r;

    public gg(@NonNull gj gjVar, @NonNull w wVar, @NonNull gk gkVar, @NonNull lw lwVar) {
        this(gjVar, wVar, gkVar, lwVar, new C0386a(), new wg(), new vm(), new sv());
    }

    public gg(@NonNull gj gjVar, @NonNull w wVar, @NonNull gk gkVar, @NonNull lw lwVar, @NonNull C0386a aVar, @NonNull wh whVar, @NonNull vm vmVar, @NonNull sv svVar) {
        super(new ab(), svVar);
        this.f949j = gjVar;
        this.f950k = wVar;
        this.f951l = gkVar;
        this.f956q = lwVar;
        this.f952m = aVar;
        this.f953n = whVar;
        this.f954o = vmVar;
        this.f955p = getClass().getName() + "@" + Integer.toHexString(hashCode());
    }

    /* renamed from: a */
    public boolean mo461a() {
        this.f957r = this.f949j.d();
        if (!(this.f957r.mo2088f() && !cx.a((Collection) this.f957r.mo936a()))) {
            return false;
        }
        mo457a(this.f957r.mo936a());
        byte[] btsBeforeGzip = this.f952m.mo935a(this.f950k, this.f957r, this.f951l, this.f956q).mo934a();
        byte[] bArr = null;
        try {
            bArr = this.f954o.mo2492a(btsBeforeGzip); //gzip bytes
        } catch (Throwable th) {
        }
        if (!cx.nullOrEmpty(bArr)) {
            mo455a("Content-Encoding", "gzip");
        } else {
            bArr = btsBeforeGzip;
        }
        setEncryptedBytes(bArr);
        return true;
    }

    /* renamed from: d */
    public void d() {
        super.d();
        mo450a(this.f953n.a());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo451a(@NonNull Builder builder) {
        ((sv) this.f388i).mo2207a(builder, this.f957r);
    }

    /* renamed from: t */
    public boolean mo482t() {
        return (400 != mo473k()) & super.mo482t();
    }

    @NonNull
    /* renamed from: n */
    public String mo476n() {
        return this.f955p;
    }
}
