package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.fz */
public class fz implements fq, ft<en> {
    @NonNull
    /* renamed from: a */
    public fp a(@NonNull Context context, @NonNull fu fuVar, @NonNull fn fnVar, @NonNull eg egVar) {
        return new fy(context, fuVar.mo924a(new ek(fnVar.mo908b(), fnVar.mo907a()), egVar, new fd(this)));
    }

    @NonNull
    /* renamed from: a */
    public en d(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        return new fi(context, ekVar, blVar, aVar, al.m324a().mo290h(), ulVar.mo2406e(), new uo(ulVar));
    }

    @NonNull
    /* renamed from: B */
    public ge c(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        return new ge(context, ekVar, blVar, aVar, ulVar.mo2406e(), new uo(ulVar), C0008a.MANUAL);
    }
}
