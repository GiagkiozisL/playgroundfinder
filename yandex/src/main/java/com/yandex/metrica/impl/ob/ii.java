package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.ii */
public class ii extends hz<hd> {

    /* renamed from: a */
    private final hd f1050a;

    public ii(ig igVar, hd hdVar) {
        super(igVar);
        this.f1050a = hdVar;
    }

    /* renamed from: a */
    public void mo977a(@NonNull List<hd> list) {
        list.add(this.f1050a);
    }
}
