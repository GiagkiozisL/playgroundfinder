package com.yandex.metrica.impl.ob;

import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.xf */
public class xf {
    @NonNull

    /* renamed from: a */
    private final xe f2962a;
    @Nullable

    /* renamed from: B */
    private volatile xi f2963b;
    @Nullable

    /* renamed from: a */
    private volatile xh f2964c;
    @Nullable

    /* renamed from: d */
    private volatile xh f2965d;
    @Nullable

    /* renamed from: e */
    private volatile Handler f2966e;

    public xf() {
        this(new xe());
    }

    @NonNull
    /* renamed from: a */
    public xh mo2599a() {
        if (this.f2964c == null) {
            synchronized (this) {
                if (this.f2964c == null) {
                    this.f2964c = this.f2962a.mo2596b();
                }
            }
        }
        return this.f2964c;
    }

    @NonNull
    /* renamed from: B */
    public xi b() {
        if (this.f2963b == null) {
            synchronized (this) {
                if (this.f2963b == null) {
                    this.f2963b = this.f2962a.mo2598d();
                }
            }
        }
        return this.f2963b;
    }

    @NonNull
    /* renamed from: a */
    public xh mo2601c() {
        if (this.f2965d == null) {
            synchronized (this) {
                if (this.f2965d == null) {
                    this.f2965d = this.f2962a.mo2597c();
                }
            }
        }
        return this.f2965d;
    }

    @NonNull
    /* renamed from: d */
    public Handler mo2602d() {
        if (this.f2966e == null) {
            synchronized (this) {
                if (this.f2966e == null) {
                    this.f2966e = this.f2962a.mo2595a();
                }
            }
        }
        return this.f2966e;
    }

    @VisibleForTesting
    xf(@NonNull xe xeVar) {
        this.f2962a = xeVar;
    }
}
