package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.ue */
public enum ue {
    UNKNOWN(0),
    NETWORK(1),
    PARSE(2);
    

    /* renamed from: d */
    private int f2722d;

    private ue(int i) {
        this.f2722d = i;
    }

    /* renamed from: a */
    public int mo2335a() {
        return this.f2722d;
    }

    /* renamed from: a */
    public Bundle mo2336a(Bundle bundle) {
        bundle.putInt("startup_error_key_code", mo2335a());
        return bundle;
    }

    @Nullable
    /* renamed from: B */
    public static ue m3903b(Bundle bundle) {
        if (bundle.containsKey("startup_error_key_code")) {
            return m3902a(bundle.getInt("startup_error_key_code"));
        }
        return null;
    }

    @NonNull
    /* renamed from: a */
    private static ue m3902a(int i) {
        ue ueVar = UNKNOWN;
        switch (i) {
            case 1:
                return NETWORK;
            case 2:
                return PARSE;
            default:
                return ueVar;
        }
    }
}
