package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rl.a.Aa;

/* renamed from: com.yandex.metrica.impl.ob.qx */
public class qx extends qr<Double> {
    public qx(@NonNull String str, double d, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(1, str, Double.valueOf(d), ykVar, qoVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1750a(@NonNull Aa aVar) {
        aVar.f2049e.f2053c = ((Double) mo1752b()).doubleValue();
    }
}
