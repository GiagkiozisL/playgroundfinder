package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import android.util.Log;

import com.yandex.metrica.j;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.sa */
public class sa {
    @WorkerThread
    /* renamed from: a */
    public db mo1987a(@NonNull Context context) {
        return db.m1275a(context);
    }

    @Nullable
    @WorkerThread
    /* renamed from: a */
    public db mo1986a() {
        return db.m1287e();
    }

    /* renamed from: a */
    public void mo1988a(@NonNull Context context, @NonNull j jVar) {
        db.m1277a(context, jVar);
    }

    /* renamed from: B */
    public void mo1993b(@NonNull Context context, @NonNull j jVar) {
        Log.d(Constant.RUS_TAG, "sa$mo1993b");
        db.m1282b(context, jVar);
    }

    /* renamed from: B */
    public void mo1992b() {
        db.m1281b();
    }

    /* renamed from: a */
    public boolean c() {
        return db.m1285c();
    }

    /* renamed from: d */
    public boolean mo1996d() {
        return db.m1287e() != null;
    }

    /* renamed from: e */
    public ax mo1997e() {
        return db.m1288f();
    }

    /* renamed from: a */
    public void mo1990a(@Nullable Location location) {
        db.m1278a(location);
    }

    /* renamed from: a */
    public void mo1991a(boolean z) {
        db.m1280a(z);
    }

    /* renamed from: a */
    public void mo1989a(@NonNull Context context, boolean z) {
        db.m1275a(context).mo685b(z);
    }

    /* renamed from: B */
    public void mo1994b(@NonNull Context context, boolean z) {
        db.m1275a(context).mo686c(z);
    }

    /* renamed from: f */
    public db mo1998f() {
        return db.m1286d();
    }
}
