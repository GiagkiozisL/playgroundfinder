package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.pr */
public class pr {
    @NonNull

    /* renamed from: a */
    private pp f1742a;

    public pr(@NonNull pp ppVar) {
        this.f1742a = ppVar;
    }

    /* renamed from: a */
    public boolean mo1651a(@NonNull Context context, String str) {
        if (!mo1648a().a(str)) {
            return false;
        }
        try {
            if (context.checkCallingOrSelfPermission(str) == 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    /* renamed from: a */
    public boolean mo1650a(@NonNull Context context) {
        return mo1651a(context, "android.permission.ACCESS_COARSE_LOCATION");
    }

    /* renamed from: B */
    public boolean mo1652b(@NonNull Context context) {
        return mo1651a(context, "android.permission.ACCESS_FINE_LOCATION");
    }

    /* renamed from: a */
    public boolean mo1653c(@NonNull Context context) {
        return mo1650a(context) || mo1652b(context);
    }

    /* renamed from: d */
    public boolean mo1654d(@NonNull Context context) {
        return mo1651a(context, "android.permission.READ_PHONE_STATE");
    }

    /* renamed from: e */
    public boolean mo1655e(@NonNull Context context) {
        return mo1651a(context, "android.permission.ACCESS_WIFI_STATE");
    }

    /* renamed from: f */
    public boolean mo1656f(@NonNull Context context) {
        return mo1651a(context, "android.permission.CHANGE_WIFI_STATE");
    }

    /* renamed from: g */
    public boolean mo1657g(@NonNull Context context) {
        return mo1651a(context, "android.permission.BLUETOOTH");
    }

    /* renamed from: h */
    public boolean mo1658h(@NonNull Context context) {
        return mo1651a(context, "android.permission.BLUETOOTH_ADMIN");
    }

    /* renamed from: i */
    public boolean mo1659i(@NonNull Context context) {
        return mo1657g(context) && mo1658h(context);
    }

    /* renamed from: a */
    public void mo1649a(@NonNull pp ppVar) {
        this.f1742a = ppVar;
    }

    @VisibleForTesting
    @NonNull
    /* renamed from: a */
    public pp mo1648a() {
        return this.f1742a;
    }
}
