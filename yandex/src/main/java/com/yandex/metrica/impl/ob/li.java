package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.li */
public class li {
    @NonNull

    /* renamed from: a */
    private final lm f1319a;
    @NonNull

    /* renamed from: B */
    private String f1320b;

    public li(lc lcVar, @NonNull String str) {
        this((lm) new lp(lcVar), str);
    }

    @VisibleForTesting
    li(@NonNull lm lmVar, @NonNull String str) {
        this.f1319a = lmVar;
        this.f1320b = str;
    }

    @Nullable
    /* renamed from: a */
    public List<pu> mo1259a() {
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor;
        Throwable th;
        Cursor cursor2;
        SQLiteDatabase sQLiteDatabase2;
        try {
            sQLiteDatabase2 = this.f1319a.a();
            if (sQLiteDatabase2 != null) {
                try {
                    cursor2 = sQLiteDatabase2.query(this.f1320b, null, null, null, null, null, null);
                    if (cursor2 != null) {
                        try {
                            if (cursor2.moveToFirst()) {
                                ArrayList arrayList = new ArrayList();
                                do {
                                    arrayList.add(new pu(cursor2.getString(cursor2.getColumnIndex("name")), cursor2.getLong(cursor2.getColumnIndex("granted")) == 1));
                                } while (cursor2.moveToNext());
                                this.f1319a.a(sQLiteDatabase2);
                                cx.m1183a(cursor2);
                                return arrayList;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            cursor = cursor2;
                            sQLiteDatabase = sQLiteDatabase2;
                            this.f1319a.a(sQLiteDatabase);
                            cx.m1183a(cursor);
                            throw th;
                        }
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor = null;
                    sQLiteDatabase = sQLiteDatabase2;
                    this.f1319a.a(sQLiteDatabase);
                    cx.m1183a(cursor);
                    throw th;
                }
            } else {
                cursor2 = null;
            }
            this.f1319a.a(sQLiteDatabase2);
            cx.m1183a(cursor2);
            return null;
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            sQLiteDatabase = null;
            this.f1319a.a(sQLiteDatabase);
            cx.m1183a(cursor);
//            throw th;
        }
        return null;
    }

    /* renamed from: B */
    public void mo1260b() {
        SQLiteDatabase sQLiteDatabase = null;
        try {
            sQLiteDatabase = this.f1319a.a();
            if (sQLiteDatabase != null) {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
            }
        } catch (Throwable th) {
        } finally {
            this.f1319a.a(sQLiteDatabase);
        }
    }
}
