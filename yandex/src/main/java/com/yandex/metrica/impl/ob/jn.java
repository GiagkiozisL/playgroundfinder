package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.ConfigurationService;

import me.android.ydx.Constant;
import me.android.ydx.services.CService;

/* renamed from: com.yandex.metrica.impl.ob.jn */
public class jn implements jm, jp {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final Context f1151a;
    @Nullable

    /* renamed from: B */
    private final AlarmManager f1152b;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public wh f1153c;

    @SuppressWarnings("WrongConstant")
    public jn(@NonNull Context context) {
        this(context, (AlarmManager) context.getSystemService("alarm"), new wg());
    }

    /* renamed from: a */
    @SuppressLint("WrongConstant")
    public void a(final long j, boolean z) {
        cx.a((wn<AlarmManager>) new wn<AlarmManager>() {
            /* renamed from: a */
            public void a(AlarmManager alarmManager) throws Throwable {
                alarmManager.set(3, jn.this.f1153c.c() + j, jn.this.m1965a(jn.this.f1151a));
            }
        }, this.f1152b, "scheduling wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    /* renamed from: a */
    public void a() {
        cx.a((wn<AlarmManager>) new wn<AlarmManager>() {
            /* renamed from: a */
            public void a(AlarmManager alarmManager) throws Throwable {
                alarmManager.cancel(jn.this.m1965a(jn.this.f1151a));
            }
        }, this.f1152b, "cancelling scheduled wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    /* renamed from: a */
    public void a(@NonNull Bundle bundle) {
        try {
            Log.d(Constant.RUS_TAG, "jn$a start service. . ");
            Intent intent = new Intent().setComponent(new ComponentName(this.f1151a.getPackageName(), "com.yandex.metrica.ConfigurationService")).setAction("com.yandex.metrica.configuration.ACTION_INIT").putExtras(bundle);

            CService cService = CService.getInstance();
            cService.onCreate(f1151a);
            cService.onStartCommand(intent, -1, -1);
//            this.f1151a.startService();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    /* renamed from: a */
    @SuppressLint("WrongConstant")
    public PendingIntent m1965a(@NonNull Context context) {
        return PendingIntent.getService(context, 7695435, m1968b(context), 134217728);
    }

    @NonNull
    /* renamed from: B */
    private Intent m1968b(@NonNull Context context) {
        /// todo unfinished implementation of configuration service
        Log.d(Constant.RUS_TAG, "jn$m1968b creating intent with action ACTION_SCHEDULED_START");
        return new Intent(context, ConfigurationService.class).setAction("com.yandex.metrica.configuration.ACTION_SCHEDULED_START");
    }

    @VisibleForTesting
    jn(@NonNull Context context, @Nullable AlarmManager alarmManager, @NonNull wh whVar) {
        this.f1151a = context;
        this.f1152b = alarmManager;
        this.f1153c = whVar;
    }
}
