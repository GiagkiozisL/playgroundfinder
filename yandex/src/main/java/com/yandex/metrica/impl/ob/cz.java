package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.cz */
public final class cz extends Location {
    @Nullable

    /* renamed from: a */
    private final String f668a;

    private cz(@NonNull Location location, @Nullable String str) {
        super(location);
        this.f668a = str;
    }

    @Nullable
    /* renamed from: a */
    public String mo676a() {
        return this.f668a;
    }

    /* renamed from: a */
    public static cz m1243a(@NonNull Location location) {
        Location location2 = new Location(location);
        String provider = location2.getProvider();
        location2.setProvider("");
        return new cz(location2, provider);
    }

    /* renamed from: B */
    public static cz m1244b(@NonNull Location location) {
        return new cz(new Location(location), "");
    }
}
