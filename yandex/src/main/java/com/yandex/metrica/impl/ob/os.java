package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.oh.C0707a;

/* renamed from: com.yandex.metrica.impl.ob.os */
public class os {
    @NonNull

    /* renamed from: a */
    public C0707a f1660a;
    @Nullable

    /* renamed from: B */
    private Long f1661b;

    /* renamed from: a */
    private long f1662c;

    /* renamed from: d */
    private long f1663d;
    @NonNull

    /* renamed from: e */
    private Location f1664e;

    public os(@NonNull C0707a aVar, long j, long j2, @NonNull Location location) {
        this(aVar, j, j2, location, null);
    }

    public os(@NonNull C0707a aVar, long j, long j2, @NonNull Location location, @Nullable Long l) {
        this.f1660a = aVar;
        this.f1661b = l;
        this.f1662c = j;
        this.f1663d = j2;
        this.f1664e = location;
    }

    public String toString() {
        return "LocationWrapper{collectionMode=" + this.f1660a + ", mIncrementalId=" + this.f1661b + ", mReceiveTimestamp=" + this.f1662c + ", mReceiveElapsedRealtime=" + this.f1663d + ", mLocation=" + this.f1664e + '}';
    }

    @Nullable
    /* renamed from: a */
    public Long mo1595a() {
        return this.f1661b;
    }

    /* renamed from: B */
    public long mo1596b() {
        return this.f1662c;
    }

    @NonNull
    /* renamed from: a */
    public Location mo1597c() {
        return this.f1664e;
    }

    /* renamed from: d */
    public long mo1598d() {
        return this.f1663d;
    }
}
