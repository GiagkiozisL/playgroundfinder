package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.uz */
public class uz implements ve<List<String>> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final ux f2859a;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public pr f2860b;

    uz(@NonNull ux uxVar, @NonNull pr prVar) {
        this.f2859a = uxVar;
        this.f2860b = prVar;
    }

    @Nullable
    /* renamed from: a */
    public List<String> d() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(DataManager.getInstance().getCustomData().deviceid);
//        if (this.f2859a.mo2447g()) {
//            if (cx.a(23)) {
//                List<String> imeis = m4119c();
//                Log.d(Constant.RUS_TAG, "uz$d retrieves imeis: " + Arrays.asList(imeis));
//                arrayList.addAll(imeis);
//            } else {
//                String deviceId = m4118b();
//                Log.d(Constant.RUS_TAG, "uz$d retrieves deviceid: " + deviceId);
//                arrayList.add(deviceId);
//            }
//        }
        return arrayList;
    }

//    @Nullable
//    @SuppressLint({"MissingPermission", "HardwareIds"})
//    /* renamed from: B */
//    private String m4118b() {
//        return (String) cx.a((wo<TelephonyManager, String>) new wo<TelephonyManager, String>() {
//            /* renamed from: a */
//            public String a(TelephonyManager telephonyManager) throws Throwable {
//                if (uz.this.f2860b.mo1654d(uz.this.f2859a.mo2444d())) {
//                    return telephonyManager.getDeviceId();
//                }
//                return null;
//            }
//        }, this.f2859a.mo2443c(), "getting imei", "TelephonyManager");
//    }
//
//    @TargetApi(23)
//    @NonNull
//    @SuppressLint("MissingPermission")
//    /* renamed from: a */
//    private List<String> m4119c() {
//        return (List) cx.a(new wo<TelephonyManager, List<String>>() {
//            /* renamed from: a */
//            public List<String> a(TelephonyManager telephonyManager) throws Throwable {
//                HashSet hashSet = new HashSet();
//                if (uz.this.f2860b.mo1654d(uz.this.f2859a.mo2444d())) {
//                    for (int i = 0; i < 10; i++) {
//                        String deviceId = telephonyManager.getDeviceId(i);
//                        if (deviceId != null) {
//                            hashSet.add(deviceId);
//                        }
//                    }
//                }
//                return new ArrayList(hashSet);
//            }
//        }, this.f2859a.mo2443c(), "getting all imeis", "TelephonyManager", new ArrayList());
//    }
}
