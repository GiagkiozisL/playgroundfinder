package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.yf */
public class yf<T> implements yk<T> {
    @NonNull

    /* renamed from: a */
    private final String f3006a;

    public yf(@NonNull String str) {
        this.f3006a = str;
    }

    /* renamed from: a */
    public yi a(@Nullable T t) {
        if (t == null) {
            return yi.a(this, this.f3006a + " is null.");
        }
        return yi.a(this);
    }
}
