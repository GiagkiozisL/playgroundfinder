package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.ReporterConfig;

import java.util.HashMap;

/* renamed from: com.yandex.metrica.impl.ob.ry */
public abstract class ry<T extends rx> {
    @NonNull

    /* renamed from: a */
    private HashMap<String, T> f2295a = new HashMap<>();
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public sa f2296b = new sa();
    @NonNull

    /* renamed from: a */
    private final xh f2297c;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract T mo1959a(@NonNull xh xhVar, @NonNull Context context, @NonNull String str);

    protected ry(@NonNull xh xhVar) {
        this.f2297c = xhVar;
    }

    /* renamed from: B */
    private T m3431b(@NonNull final Context context, @NonNull String str) {
        if (this.f2296b.mo1998f() == null) {
            this.f2297c.a((Runnable) new Runnable() {
                public void run() {
                    ry.this.f2296b.mo1987a(context);
                }
            });
        }
        T a = mo1959a(this.f2297c, context, str);
        this.f2295a.put(str, a);
        return a;
    }

    /* renamed from: a */
    public T mo1958a(@NonNull Context context, @NonNull String str) {
        T t = (T) this.f2295a.get(str);
        if (t == null) {
            synchronized (this.f2295a) {
                t = (T) this.f2295a.get(str);
                if (t == null) {
                    t = m3431b(context, str);
                    t.mo1941a(str);
                }
            }
        }
        return t;
    }

    /* renamed from: a */
    public T mo1957a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        T t = this.f2295a.get(reporterConfig.apiKey);
        if (t == null) {
            synchronized (this.f2295a) {
                t = this.f2295a.get(reporterConfig.apiKey);
                if (t == null) {
                    t = m3431b(context, reporterConfig.apiKey);
                    t.mo1940a(reporterConfig);
                }
            }
        }
        return t;
    }
}
