package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rn.C0850a;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.ir */
public class ir extends mb<C0850a> {
    @NonNull
    /* renamed from: a */
    public C0850a b(@NonNull byte[] bArr) throws IOException {
        return C0850a.m3226a(bArr);
    }

    @NonNull
    /* renamed from: a */
    public C0850a c() {
        return new C0850a();
    }
}
