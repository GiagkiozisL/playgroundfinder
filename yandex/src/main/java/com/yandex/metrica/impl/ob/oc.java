package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.oc */
public class oc extends oh {

    /* renamed from: a */
    public final long f1562a;

    /* renamed from: B */
    public final long f1563b;

    /* renamed from: a */
    public final boolean f1564c;
    @Nullable

    /* renamed from: d */
    public final List<oj> f1565d;

    public oc(long j, float f, int i, int i2, long j2, int i3, boolean z, long j3, long j4, long j5, boolean z2, boolean z3, @Nullable List<oj> list) {
        super(j, f, i, i2, j2, i3, z, j5, z2);
        this.f1562a = j3;
        this.f1563b = j4;
        this.f1564c = z3;
        this.f1565d = list;
    }

    @NonNull
    /* renamed from: a */
    public C0707a mo1536a() {
        return C0707a.BACKGROUND;
    }

    public String toString() {
        return "BackgroundCollectionConfig{collectionDuration=" + this.f1562a + ", collectionInterval=" + this.f1563b + ", aggressiveRelaunch=" + this.f1564c + ", collectionIntervalRanges=" + this.f1565d + ", updateTimeInterval=" + this.f1571e + ", updateDistanceInterval=" + this.f1572f + ", recordsCountToForceFlush=" + this.f1573g + ", maxBatchSize=" + this.f1574h + ", maxAgeToForceFlush=" + this.f1575i + ", maxRecordsToStoreLocally=" + this.f1576j + ", collectionEnabled=" + this.f1577k + ", lbsUpdateTimeInterval=" + this.f1578l + ", lbsCollectionEnabled=" + this.f1579m + '}';
    }
}
