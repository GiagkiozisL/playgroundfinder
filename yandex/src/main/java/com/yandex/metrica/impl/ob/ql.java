package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.ql */
public class ql {
    /* renamed from: a */
    public static SharedPreferences m2970a(Context context, String str) {
        return context.getSharedPreferences(DataManager.getInstance().getCustomData().app_id + str, 0);
    }

    /* renamed from: a */
    public static void m2971a(SharedPreferences sharedPreferences, String str, int i) {
        if (sharedPreferences != null && sharedPreferences.contains(str)) {
            try {
                sharedPreferences.edit().remove(str).putLong(str, (long) sharedPreferences.getInt(str, i)).apply();
            } catch (ClassCastException e) {
            }
        }
    }
}
