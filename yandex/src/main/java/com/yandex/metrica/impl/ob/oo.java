package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.np.C0678a;

import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.oo */
public class oo implements p {

    /* renamed from: a */
    public static final long f1618a = TimeUnit.MINUTES.toMillis(1);
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: B */
    private static volatile oo f1619b;

    /* renamed from: a */
    private static final Object f1620c = new Object();
    @NonNull

    /* renamed from: d */
    private final Context f1621d;
    @NonNull

    /* renamed from: e */
    private xi f1622e;
    @NonNull

    /* renamed from: f */
    private final WeakHashMap<Object, Object> f1623f;

    /* renamed from: g */
    private boolean f1624g;
    /* access modifiers changed from: private */
    @Nullable

    /* renamed from: h */
    public oh f1625h;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: i */
    public uk f1626i;
    /* access modifiers changed from: private */
    @Nullable

    /* renamed from: j */
    public ot f1627j;
    @NonNull

    /* renamed from: k */
    private C0721a f1628k;

    /* renamed from: l */
    private Runnable f1629l;
    @NonNull

    /* renamed from: m */
    private lh f1630m;
    @NonNull

    /* renamed from: n */
    private lg f1631n;

    /* renamed from: o */
    private final pr f1632o;

    /* renamed from: p */
    private final px f1633p;

    /* renamed from: q */
    private boolean f1634q;

    /* renamed from: r */
    private final Object f1635r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public final Object f1636s;

    /* renamed from: com.yandex.metrica.impl.ob.oo$a */
    static class C0721a {
        C0721a() {
        }

        @NonNull
        /* renamed from: a */
        public ot mo1579a(@NonNull Context context, @NonNull xi xiVar, @NonNull uk ukVar, @Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull pr prVar) {
            return new ot(context, ukVar, xiVar, ohVar, lhVar, lgVar, prVar);
        }
    }

    /* renamed from: a */
    public static oo m2694a(Context context) {
        if (f1619b == null) {
            synchronized (f1620c) {
                if (f1619b == null) {
                    f1619b = new oo(context.getApplicationContext());
                }
            }
        }
        return f1619b;
    }

    private oo(@NonNull Context context) {
        this(context, al.m324a().mo292j().mo2630e(), new C0721a(), ld.m2146a(context).mo1242g(), ld.m2146a(context).mo1243h(), (uk) C0678a.m2599a(uk.class).a(context).a());
    }

    /* renamed from: a */
    private void m2699c() {
        this.f1622e.a((Runnable) new Runnable() {
            public void run() {
                try {
                    if (oo.this.f1627j != null) {
                        oo.this.f1627j.mo1600a();
                    }
                } catch (Throwable th) {
                }
            }
        });
    }

    /* renamed from: a */
    public void mo1572a(@Nullable Object obj) {
        synchronized (this.f1635r) {
            this.f1623f.put(obj, null);
            m2701d();
        }
    }

    /* renamed from: B */
    public void mo1575b(@Nullable Object obj) {
        synchronized (this.f1635r) {
            this.f1623f.remove(obj);
            m2701d();
        }
    }

    /* renamed from: d */
    private void m2701d() {
        if (this.f1634q) {
            if (!this.f1624g || this.f1623f.isEmpty()) {
                m2703e();
                this.f1634q = false;
            }
        } else if (this.f1624g && !this.f1623f.isEmpty()) {
            m2704f();
            this.f1634q = true;
        }
    }

    /* renamed from: e */
    private void m2703e() {
        if (this.f1627j != null) {
            this.f1627j.mo1606f();
        }
        m2706h();
    }

    /* renamed from: f */
    private void m2704f() {
        if (this.f1627j == null) {
            synchronized (this.f1636s) {
                this.f1627j = this.f1628k.mo1579a(this.f1621d, this.f1622e, this.f1626i, this.f1625h, this.f1630m, this.f1631n, this.f1632o);
            }
        }
        this.f1627j.mo1605e();
        m2705g();
        m2699c();
    }

    @Nullable
    /* renamed from: a */
    public Location mo1570a() {
        ot otVar = this.f1627j;
        if (otVar == null) {
            return null;
        }
        return otVar.mo1602b();
    }

    @Nullable
    /* renamed from: B */
    public Location mo1574b() {
        ot otVar = this.f1627j;
        if (otVar == null) {
            return null;
        }
        return otVar.mo1603c();
    }

    /* renamed from: g */
    private void m2705g() {
        if (this.f1629l == null) {
            this.f1629l = new Runnable() {
                public void run() {
                    ot a = oo.this.f1627j;
                    if (a != null) {
                        a.mo1604d();
                    }
                    oo.this.m2707i();
                }
            };
            m2707i();
        }
    }

    /* renamed from: h */
    private void m2706h() {
        if (this.f1629l != null) {
            this.f1622e.b(this.f1629l);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m2707i() {
        this.f1622e.a(this.f1629l, f1618a);
    }

    /* renamed from: a */
    public static byte[] m2696a(@Nullable Location location) {
        byte[] bArr = null;
        if (location != null) {
            Parcel obtain = Parcel.obtain();
            try {
                obtain.writeValue(location);
                bArr = obtain.marshall();
            } catch (Throwable th) {
            } finally {
                obtain.recycle();
            }
        }
        return bArr;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public static Location m2693a(@NonNull byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            Location location = (Location) obtain.readValue(Location.class.getClassLoader());
            obtain.recycle();
            return location;
        } catch (Throwable th) {
            obtain.recycle();
            throw th;
        }
    }

    /* renamed from: a */
    public void mo1573a(boolean z) {
        synchronized (this.f1635r) {
            if (this.f1624g != z) {
                this.f1624g = z;
                this.f1633p.mo1668a(z);
                this.f1632o.mo1649a(this.f1633p.mo1666a());
                m2701d();
            }
        }
    }

    /* renamed from: a */
    public void mo1571a(@NonNull uk ukVar, @Nullable oh ohVar) {
        synchronized (this.f1635r) {
            this.f1626i = ukVar;
            this.f1625h = ohVar;
            this.f1633p.mo1667a(ukVar);
            this.f1632o.mo1649a(this.f1633p.mo1666a());
        }
        this.f1622e.a((Runnable) new Runnable() {
            public void run() {
                synchronized (oo.this.f1636s) {
                    if (oo.this.f1627j != null) {
                        oo.this.f1627j.mo1601a(oo.this.f1626i, oo.this.f1625h);
                    }
                }
            }
        });
    }

    @VisibleForTesting
    oo(@NonNull Context context, @NonNull xi xiVar, @NonNull C0721a aVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull uk ukVar) {
        this.f1624g = false;
        this.f1633p = new px();
        this.f1634q = false;
        this.f1635r = new Object();
        this.f1636s = new Object();
        this.f1621d = context;
        this.f1622e = xiVar;
        this.f1623f = new WeakHashMap<>();
        this.f1628k = aVar;
        this.f1630m = lhVar;
        this.f1631n = lgVar;
        this.f1626i = ukVar;
        this.f1632o = new pr(this.f1633p.mo1666a());
    }
}
