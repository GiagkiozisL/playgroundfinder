package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

@SuppressLint({"ParcelCreator"})
/* renamed from: com.yandex.metrica.impl.ob.x */
public class x extends ResultReceiver {
    @NonNull

    /* renamed from: a */
    private final C1181a f2958a;

    /* renamed from: com.yandex.metrica.impl.ob.x$a */
    public interface C1181a {
        /* renamed from: a */
        void mo677a(int i, @NonNull Bundle bundle);
    }

    public x(Handler handler, @NonNull C1181a aVar) {
        super(handler);
        this.f2958a = aVar;
    }

    /* access modifiers changed from: protected */
    public void onReceiveResult(int resultCode, Bundle resultData) {
        C1181a aVar = this.f2958a;
        if (resultData == null) {
            resultData = new Bundle();
        }
        aVar.mo677a(resultCode, resultData);
    }

    /* renamed from: a */
    public static void m4410a(@Nullable ResultReceiver resultReceiver, @NonNull uk ukVar) {
        if (resultReceiver != null) {
            Bundle bundle = new Bundle();
            m4408a(bundle, ukVar);
            resultReceiver.send(1, bundle);
        }
    }

    /* renamed from: a */
    public static void m4409a(ResultReceiver resultReceiver, ue ueVar, @Nullable uk ukVar) {
        if (resultReceiver != null) {
            Bundle bundle = new Bundle();
            ueVar.mo2336a(bundle);
            if (ukVar != null) {
                m4408a(bundle, ukVar);
            }
            resultReceiver.send(2, bundle);
        }
    }

    /* renamed from: a */
    private static void m4408a(@NonNull Bundle bundle, @NonNull uk ukVar) {
        bundle.putString("Uuid", ukVar.uuid);
        bundle.putString("DeviceId", ukVar.deviceId);
        bundle.putString("DeviceIdHash", ukVar.deviceIDHash);
        bundle.putString("AdUrlGet", ukVar.getAdUrl);
        bundle.putString("AdUrlReport", ukVar.reportAdUrl);
        bundle.putLong("ServerTimeOffset", wi.m4361c());
        Map a = we.m4340a(ukVar.encodedClidsFromResponse);
        if (a.isEmpty()) {
            a = new HashMap();
        }
        bundle.putString("Clids", vq.m4214a(a));
        bundle.putString("RequestClids", ukVar.lastStartupRequestClids);
    }
}
