package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.it */
public abstract class it implements iw<iy> {

    /* renamed from: a */
    private final en f1062a;
    @NonNull

    /* renamed from: B */
    private final jc f1063b;

    /* renamed from: a */
    private final jg f1064c;

    /* renamed from: d */
    private final jb f1065d;

    public it(@NonNull en enVar, @NonNull jc jcVar, @NonNull jg jgVar, @NonNull jb jbVar) {
        this.f1062a = enVar;
        this.f1063b = jcVar;
        this.f1064c = jgVar;
        this.f1065d = jbVar;
    }

    @Nullable
    /* renamed from: a */
    public final ix a() {
        if (this.f1064c.mo1101i()) {
            return new ix(this.f1062a, this.f1064c, mo1033b());
        }
        return null;
    }

    @NonNull
    /* renamed from: a */
    public final ix a(@NonNull iy iyVar) {
        if (this.f1064c.mo1101i()) {
            tl.a(this.f1062a.mo824k()).reportEvent("create session with non-empty storage");
        }
        return new ix(this.f1062a, this.f1064c, m1842b(iyVar));
    }

    @NonNull
    /* renamed from: B */
    private iz m1842b(@NonNull iy iyVar) {
        long a = this.f1063b.mo1071a();
        this.f1064c.mo1094d(a).b(TimeUnit.MILLISECONDS.toSeconds(iyVar.f1084a)).mo1096e(iyVar.f1084a).a(0).a(true).mo1100h();
        this.f1062a.mo823j().mo1200a(a, this.f1065d.mo1067a(), TimeUnit.MILLISECONDS.toSeconds(iyVar.f1085b));
        return mo1033b();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    /* renamed from: B */
    public iz mo1033b() {
        return iz.m1867a(this.f1065d).mo1058a(this.f1064c.mo1099g()).mo1062c(this.f1064c.mo1095d()).mo1061b(this.f1064c.mo1093c()).mo1059a(this.f1064c.mo1092b()).mo1063d(this.f1064c.mo1097e()).mo1064e(this.f1064c.mo1098f()).mo1060a();
    }
}
