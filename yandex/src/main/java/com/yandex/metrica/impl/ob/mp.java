package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.Arrays;

/* renamed from: com.yandex.metrica.impl.ob.mp */
public class mp {
    @NonNull

    /* renamed from: a */
    private final C0644a f1486a;
    @NonNull

    /* renamed from: B */
    private final vm f1487b;

    /* renamed from: com.yandex.metrica.impl.ob.mp$a */
    public static class C0644a {
        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public Crypto mo1436a(byte[] bArr, byte[] bArr2) {
            return new Crypto("AES/CBC/PKCS5Padding", bArr, bArr2);
        }
    }

    public mp() {
        this(new C0644a(), new vm());
    }

    @VisibleForTesting
    public mp(@NonNull C0644a aVar, @NonNull vm vmVar) {
        this.f1486a = aVar;
        this.f1487b = vmVar;
    }

    @Nullable
    /* renamed from: a */
    public byte[] mo1435a(@Nullable byte[] seed, @NonNull String str) {
        try {
            Crypto crypto = this.f1486a.mo1436a(str.getBytes(), Arrays.copyOfRange(seed, 0, 16));
            if (cx.nullOrEmpty(seed)) {
                return null;
            }
            byte[] decryptedMsg = crypto.decrypt(seed, 16, seed.length - 16);
            return this.f1487b.mo2493b(decryptedMsg);
        } catch (Throwable th) {
            return null;
        }
    }
}
