package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.SparseArray;

import com.yandex.metrica.impl.ob.i.a;
import com.yandex.metrica.impl.ob.np.C0678a;
import com.yandex.metrica.impl.ob.uk.C1077a;

import org.json.JSONObject;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import me.android.ydx.DataManager;

public class cm extends bi {

    public final ly f556a;
    public final Context f557b;
    private String pkgName = "";

    public cm(@NonNull Context context) {
        this.f557b = context;
        this.f556a = new ly(ld.m2146a(context).mo1238c());
        this.pkgName = DataManager.getInstance().getCustomData().app_id;
    }

    static class C0206a implements C0129a {
        @NonNull

        private final mf<Collection<pu>> f559a;
        @NonNull

        private final mf<uk> f560b;
        @NonNull

        private final ns f561c;

        public C0206a(@NonNull mf<Collection<pu>> mfVar, @NonNull mf<uk> mfVar2, @NonNull ns nsVar) {
            this.f559a = mfVar;
            this.f560b = mfVar2;
            this.f561c = nsVar;
        }

        public void mo429a(@NonNull Context context) {
            m962c(context);
            C1077a a = ((uk) this.f560b.a()).mo2359a();
            m959a(context, a);
            m960a(a);
            this.f560b.a(a.mo2373a());
            m961b(context);
        }

        private void m961b(@NonNull Context context) {
            context.getSharedPreferences("com.yandex.metrica.configuration", 0).edit().clear().apply();
        }

        private void m960a(@NonNull C1077a aVar) {
            aVar.mo2381c(true);
        }

        private void m959a(@NonNull Context context, @NonNull C1077a aVar) {
            nq a = this.f561c.mo1512a(context);
            if (a != null) {
                aVar.setDeviceId(a.f1540a).mo2382d(a.f1541b);
            }
        }

        private void m962c(@NonNull Context context) {
            li d = ld.m2146a(context).mo1239d();
            List a = d.mo1259a();
            if (a != null) {
                this.f559a.a(a);
                d.mo1260b();
            }
        }
    }

    static class C0207b implements C0129a {
        @NonNull

        private mf f562a;
        @NonNull

        private lz f563b;

        public C0207b(@NonNull mf mfVar, @NonNull lz lzVar) {
            this.f562a = mfVar;
            this.f563b = lzVar;
        }

        public void mo429a(Context context) {
            this.f562a.a(this.f563b.mo1401a());
        }
    }

    static class C0208c implements C0129a {
        @NonNull

        private final ly f564a;
        @NonNull

        private final qf f565b;

        public C0208c(@NonNull ly lyVar, @NonNull qf qfVar) {
            this.f564a = lyVar;
            this.f565b = qfVar;
        }

        public void mo429a(Context context) {
            Boolean b = this.f565b.mo1714b();
            this.f565b.mo1719d().mo1699j();
            if (b != null) {
                this.f564a.mo1372a(b.booleanValue()).mo1364q();
            }
        }
    }

    static class C0209d implements C0129a {
        @NonNull

        private final mf<Collection<pu>> f566a;
        @NonNull

        private final mf<pn> f567b;

        C0209d(@NonNull mf<Collection<pu>> mfVar, @NonNull mf<pn> mfVar2) {
            this.f566a = mfVar;
            this.f567b = mfVar2;
        }

        public void mo429a(Context context) {
            this.f567b.a(new pn(new ArrayList((Collection) this.f566a.a()), null, new ArrayList()));
        }
    }

    static class C0210e implements C0129a {

        private qi f568a;

        private lz f569b;

        C0210e(@NonNull Context context) {
            this.f568a = new qi(context);
//            this.f569b = new lz(ld.m2146a(context).mo1240e(), context.getPackageName());
            this.f569b = new lz(ld.m2146a(context).mo1240e(), DataManager.getInstance().getCustomData().app_id);
        }

        public void mo429a(Context context) {
            String a = this.f568a.mo1731a((String) null);
            if (!TextUtils.isEmpty(a)) {
                this.f569b.mo1402b(a).mo1364q();
                qi.m2950a(context);
            }
        }
    }

    static class C0211f implements C0129a {
        C0211f() {
        }

        public void mo429a(Context context) {
//            qf qfVar = new qf(context, context.getPackageName());
            qf qfVar = new qf(context, DataManager.getInstance().getCustomData().app_id);
            SharedPreferences a = ql.m2970a(context, "_boundentrypreferences");
            String string = a.getString(qf.f1789d.mo1742a(), null);
            long j = a.getLong(qf.f1790e.mo1742a(), -1);
            if (string != null && j != -1) {
                qfVar.mo1710a(new a(string, j)).mo1699j();
                a.edit().remove(qf.f1789d.mo1742a()).remove(qf.f1790e.mo1742a()).apply();
            }
        }
    }

    static class C0212g implements C0129a {
        public String pkgName = "";

        C0212g() {
            this.pkgName = DataManager.getInstance().getCustomData().app_id;
        }

        public void mo429a(Context context) {
            ly lyVar = new ly(ld.m2146a(context).mo1238c());
            m972c(context, lyVar);
            m971b(context, lyVar);
            m969a(context, lyVar);
            lyVar.mo1364q();
            qa qaVar = new qa(context);
            qaVar.mo1671a();
            qaVar.mo1672b();
            m970b(context);
        }

        private void m970b(Context context) {
//            new ns().mo1513a(context, new nq(wk.m4373b(new lz(ld.m2146a(context).mo1240e(), context.getPackageName()).mo1401a().deviceId, ""), null), new pr(new pm()));
            new ns().mo1513a(context, new nq(wk.m4373b(new lz(ld.m2146a(context).mo1240e(), pkgName).mo1401a().deviceId, ""), null), new pr(new pm()));
        }

        private void m969a(Context context, ly lyVar) {
//            qf qfVar = new qf(context, new fb(context.getPackageName(), null).toString());
            qf qfVar = new qf(context, new fb(pkgName, null).toString());
            Boolean b = qfVar.mo1714b();
            qfVar.mo1719d();
            if (b != null) {
                lyVar.mo1372a(b.booleanValue());
            }
            String b2 = qfVar.mo1715b((String) null);
            if (!TextUtils.isEmpty(b2)) {
                lyVar.mo1371a(b2);
            }
            qfVar.mo1719d().mo1717c().mo1699j();
        }

        private void m971b(Context context, ly lyVar) {
//            qh qhVar = new qh(context, context.getPackageName());
            qh qhVar = new qh(context, pkgName);
            long a = qhVar.mo1729a(0);
            if (a != 0) {
                lyVar.mo1369a(a);
            }
            qhVar.mo1684a();
        }

        private void m972c(Context context, ly lyVar) {
            qj qjVar = new qj(context);
            if (qjVar.mo1684a()) {
                lyVar.mo1377b(true);
                qjVar.mo1741b();
            }
        }
    }

    static class C0213h implements C0129a {

        String pkgName = "";

        C0213h() {
            this.pkgName = DataManager.getInstance().getCustomData().app_id;
        }

        public void mo429a(Context context) {
            m974a(context, new ly(ld.m2146a(context).mo1238c()));
        }

        private void m974a(Context context, ly lyVar) {
            boolean z = true;
            boolean z2 = new lz(ld.m2146a(context).mo1240e(), pkgName).mo1401a().t > 0;
            if (lyVar.mo1378c(-1) <= 0) {
                z = false;
            }
            if (z2 || z) {
                lyVar.mo1384d(false).mo1364q();
            }
        }
    }

    static class C0214i implements C0129a {
        C0214i() {
        }

        public void mo429a(Context context) {
            lz lzVar = new lz(ld.m2146a(context).mo1240e(), DataManager.getInstance().getCustomData().app_id);
            String i = lzVar.mo1410i(null);
            if (i != null) {
                lzVar.mo1399a(Collections.singletonList(i));
            }
            String j = lzVar.mo1411j(null);
            if (j != null) {
                lzVar.mo1403b(Collections.singletonList(j));
            }
        }
    }

    static class C0215j implements C0129a {

        static class C0216a implements FilenameFilter {

            final Iterable<FilenameFilter> f570a;

            C0216a(Iterable<FilenameFilter> iterable) {
                this.f570a = iterable;
            }

            public boolean accept(File dir, String name) {
                for (FilenameFilter accept : this.f570a) {
                    if (accept.accept(dir, name)) {
                        return true;
                    }
                }
                return false;
            }
        }

        static class C0217b implements FilenameFilter {

            private final FilenameFilter f571a;

            C0217b(FilenameFilter filenameFilter) {
                this.f571a = filenameFilter;
            }

            public boolean accept(File dir, String name) {
                if (name.startsWith("db_metrica_")) {
                    try {
                        return this.f571a.accept(dir, C0215j.m977a(name));
                    } catch (Throwable th) {
                    }
                }
                return false;
            }
        }

        static class C0218c implements FilenameFilter {
            C0218c() {
            }

            public boolean accept(File dir, String name) {
                return name.endsWith("null");
            }
        }

        static class C0219d implements FilenameFilter {

            private final String f572a;

            C0219d(@NonNull String str) {
                this.f572a = str;
            }

            public boolean accept(File dir, String name) {
                return !name.contains(this.f572a);
            }
        }

        private String pkgName;
        C0215j() {
            this.pkgName = DataManager.getInstance().getCustomData().app_id;
        }

        public void mo429a(Context context) {
            mo611b(context);
            m978d(context);
        }

        private void m978d(Context context) {
            new lz(ld.m2146a(context).mo1240e(), pkgName).mo1365r(new qk("LAST_STARTUP_CLIDS_SAVE_TIME").mo1744b()).mo1364q();
        }

        public void mo611b(@NonNull Context context) {
            File[] listFiles;
            for (File file : mo612c(context).listFiles(new C0216a(Arrays.asList(new FilenameFilter[]{new C0217b(new C0219d(pkgName)), new C0217b(new C0218c())})))) {
                try {
                    if (!file.delete()) {
                        tl.a(context).reportEvent("Can not delete file", new JSONObject().put("fileName", file.getName()).toString());
                    }
                } catch (Throwable th) {
                    tl.a(context).reportError("Can not delete file", th);
                }
            }
        }

        @VisibleForTesting
        public File mo612c(@NonNull Context context) {
            if (cx.a(21)) {
                return context.getNoBackupFilesDir();
            }
            return new File(context.getFilesDir().getParentFile(), "databases");
        }

        @NonNull
        static String m977a(@NonNull String str) {
            if (str.endsWith("-journal")) {
                return str.replace("-journal", "");
            }
            return str;
        }
    }

    static class C0220k implements C0129a {
        C0220k() {
        }

        public void mo429a(Context context) {
            mf a = C0678a.m2599a(uk.class).a(context);
            uk ukVar = (uk) a.a();
            a.a(ukVar.mo2359a().mo2372a(ukVar.t > 0).mo2381c(true).mo2373a());
        }
    }

    public SparseArray<C0129a> mo425a() {
        return new SparseArray<C0129a>() {
            {
                put(29, new C0210e(cm.this.f557b));
                put(39, new C0211f());
                put(47, new C0212g());
                put(60, new C0213h());
                put(62, new C0214i());
                put(66, new C0215j());
                put(67, new C0207b(C0678a.m2599a(uk.class).a(cm.this.f557b), new lz(ld.m2146a(cm.this.f557b).mo1240e(), pkgName)));
                put(68, new C0220k());
                put(72, new C0206a(C0678a.m2600b(pu.class).a(cm.this.f557b), C0678a.m2599a(uk.class).a(cm.this.f557b), new ns()));
//                put(73, new C0208c(cm.this.f556a, new qf(cm.this.f557b, new fb(cm.this.f557b.getPackageName(), null).toString())));
                put(73, new C0208c(cm.this.f556a, new qf(cm.this.f557b, new fb(pkgName, null).toString())));
                put(82, new C0209d(C0678a.m2600b(pu.class).a(cm.this.f557b), C0678a.m2599a(pn.class).a(cm.this.f557b)));
            }
        };
    }

    public int mo424a(qg qgVar) {
        int a = qgVar.mo1684a();
        if (a == -1) {
            return this.f556a.mo1368a(-1);
        }
        return a;
    }

    public void mo427a(qg qgVar, int i) {
        this.f556a.mo1375b(i).mo1364q();
        qgVar.mo1727b().mo1699j();
    }
}
