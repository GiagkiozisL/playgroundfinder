package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.hv */
public class hv extends hw<hd> {
    public hv(@NonNull ig igVar) {
        super(igVar);
    }

    /* renamed from: a */
    public void mo978a(@NonNull C0058a aVar, @NonNull List<hd> list) {
        if (m1772a(aVar)) {
            list.add(mo979a().mo998h());
        }
        if (m1773b(aVar)) {
            list.add(mo979a().mo994d());
        }
    }

    /* renamed from: a */
    private boolean m1772a(@NonNull C0058a aVar) {
        return af.m286b(aVar);
    }

    /* renamed from: B */
    private boolean m1773b(@NonNull C0058a aVar) {
        return af.m281a(aVar);
    }
}
