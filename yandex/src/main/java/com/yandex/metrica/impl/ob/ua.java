package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.ua */
public class ua {

    /* renamed from: a */
    public final long f2704a;

    /* renamed from: B */
    public final long f2705b;

    /* renamed from: a */
    public final long f2706c;

    /* renamed from: d */
    public final long f2707d;

    public ua(long j, long j2, long j3, long j4) {
        this.f2704a = j;
        this.f2705b = j2;
        this.f2706c = j3;
        this.f2707d = j4;
    }

    public String toString() {
        return "SdkFingerprintingConfig{minCollectingInterval=" + this.f2704a + ", minFirstCollectingDelay=" + this.f2705b + ", minCollectingDelayAfterLaunch=" + this.f2706c + ", minRequestRetryInterval=" + this.f2707d + '}';
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ua uaVar = (ua) o;
        if (this.f2704a != uaVar.f2704a || this.f2705b != uaVar.f2705b || this.f2706c != uaVar.f2706c) {
            return false;
        }
        if (this.f2707d != uaVar.f2707d) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((((((int) (this.f2704a ^ (this.f2704a >>> 32))) * 31) + ((int) (this.f2705b ^ (this.f2705b >>> 32)))) * 31) + ((int) (this.f2706c ^ (this.f2706c >>> 32)))) * 31) + ((int) (this.f2707d ^ (this.f2707d >>> 32)));
    }
}
