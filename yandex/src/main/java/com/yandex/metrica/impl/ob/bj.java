package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.bj */
public class bj {
    @NonNull

    /* renamed from: a */
    private final Context f360a;
    @NonNull

    /* renamed from: B */
    private final String f361b;
    @NonNull

    /* renamed from: a */
    private final bk f362c;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: d */
    public final lw f363d;
    @NonNull

    /* renamed from: e */
    private final ah f364e;

    public bj(@NonNull Context context, @NonNull bk bkVar, @NonNull lw lwVar) {
        this(context, new ah(), bkVar, lwVar);
    }

    @VisibleForTesting
    bj(@NonNull Context context, @NonNull ah ahVar, @NonNull bk bkVar, @NonNull lw lwVar) {
        this.f360a = context;
        this.f364e = ahVar;
        this.f361b = ahVar.mo267c(context).getAbsolutePath();
        this.f362c = bkVar;
        this.f363d = lwVar;
    }

    /* renamed from: a */
    public synchronized void mo430a() {
        if (am.m348a() && !this.f363d.mo1347m()) {
            m647a(this.f360a.getFilesDir().getAbsolutePath() + "/" + "YandexMetricaNativeCrashes", new wm<Boolean>() {
                /* renamed from: a */
                public void a(Boolean bool) {
                    bj.this.f363d.mo1348n();
                }
            });
        }
        m647a(this.f361b, new wm<Boolean>() {
            /* renamed from: a */
            public void a(Boolean bool) {
            }
        });
    }

    /* renamed from: a */
    private void m647a(@NonNull String str, @NonNull wm<Boolean> wmVar) {
        String[] a = m648a(str);
        int length = a.length;
        for (int i = 0; i < length; i++) {
            mo431a(str + "/" + a[i], wmVar, false);
        }
    }

    /* renamed from: a */
    private String[] m648a(String str) {
        File a = this.f364e.mo265a(str);
        if (!a.mkdir() && !a.exists()) {
            return new String[0];
        }
        String[] list = a.list(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".dmp");
            }
        });
        return list == null ? new String[0] : list;
    }

    /* renamed from: a */
    public void mo431a(@NonNull String fName, @NonNull wm<Boolean> wmVar, boolean z) {
        try {
            String contentFile = am.readFile(fName);
            Log.d(Constant.RUS_TAG, "bj$mo431a contentFile: " + contentFile);
            String b = am.compressNencodeB64(contentFile);
            if (b != null) {
                if (z) {
                    this.f362c.mo435a(b);
                } else {
                    this.f362c.mo436b(b);
                }
            }
            wmVar.a(true);
        } catch (Throwable th) {
            wmVar.a(false);
        } finally {
            this.f364e.mo265a(fName).delete();
        }
    }
}
