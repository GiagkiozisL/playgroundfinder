package com.yandex.metrica.impl.ob;

import android.text.TextUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.qb */
public class qb {

    /* renamed from: a */
    public static final Map<String, String> a = Collections.unmodifiableMap(new HashMap<String, String>() {
        {
            put("20799a27-fa80-4b36-b2db-0f8141f24180", "13");
            put("01528cc0-dd34-494d-9218-24af1317e1ee", "17233");
            put("4e610cd2-753f-4bfc-9b05-772ce8905c5e", "21952");
            put("67bb016b-be40-4c08-a190-96a3f3b503d3", "22675");
            put("e4250327-8d3c-4d35-b9e8-3c1720a64b91", "22678");
            put("6c5f504e-8928-47b5-bfb5-73af8d8bf4b4", "30404");
            put("7d962ba4-a392-449a-a02d-6c5be5613928", "30407");
        }
    });

    /* renamed from: B */
    private qc b;

    public qb(qc qcVar) {
        this.b = qcVar;
    }

    /* renamed from: a */
    public void mo1673a() {
        if (mo1680f()) {
            mo1681g();
            mo1682h();
        }
    }

    /* renamed from: B */
    public void mo1675b() {
        String d = mo1678d();
        if (m2873c(d)) {
            mo1674a(d);
        }
    }

    /* renamed from: a */
    private boolean m2873c(String str) {
        return !TextUtils.isEmpty(str) && "DONE".equals(this.b.g().get(qc.f(str)));
    }

    /* renamed from: a */
    public void mo1677c() {
        mo1674a(mo1679e());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1674a(String str) {
        if (str != null) {
            mo1676b(str);
            mo1681g();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public String mo1678d() {
        return (String) a.get(this.b.mo1698i());
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public String mo1679e() {
        Map var1 = this.b.g();
        Iterator var2 = a.values().iterator();

        while(var2.hasNext()) {
            String var3 = (String)var2.next();
            var1.remove(qc.f(var3));
        }

        LinkedList var7 = new LinkedList();
        Iterator var8 = var1.keySet().iterator();

        while(var8.hasNext()) {
            String var4 = (String)var8.next();

            try {
                var7.add(Integer.parseInt(qc.g(var4)));
            } catch (Throwable var6) {
            }
        }

        return var7.size() == 1 ? ((Integer)var7.getFirst()).toString() : null;
//        Map g = this.b.g();
//        for (String f : a.values()) {
//            g.remove(qc.f(f));
//        }
//        LinkedList linkedList = new LinkedList();
//        for (String g2 : g.keySet()) {
//            try {
//                linkedList.add(Integer.valueOf(Integer.parseInt(qc.g(g2))));
//            } catch (Throwable th) {
//            }
//        }
//        if (linkedList.size() == 1) {
//            return ((Integer) linkedList.getFirst()).toString();
//        }
//        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public boolean mo1680f() {
        return this.b.mo1683a((String) null) != null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public void mo1681g() {
        this.b.mo1684a();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo1676b(String str) {
        this.b.mo1690d(str);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: h */
    public void mo1682h() {
        this.b.mo1686b();
    }
}
