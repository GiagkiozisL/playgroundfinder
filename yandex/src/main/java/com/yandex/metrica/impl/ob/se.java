package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.IReporter;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.profile.UserProfile;

import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.se */
public class se implements IReporter {

    /* renamed from: a */
    static final yk<String> f2390a = new yg(new ye("Event name"));

    /* renamed from: B */
    static final yk<String> f2391b = new yg(new ye("Error message"));

    /* renamed from: a */
    static final yk<Throwable> f2392c = new yg(new yf("Unhandled exception"));

    /* renamed from: d */
    static final yk<UserProfile> f2393d = new yg(new yf("User profile"));

    /* renamed from: e */
    static final yk<Revenue> f2394e = new yg(new yf("Revenue"));

    public void reportEvent(@NonNull String eventName) throws yh {
        f2390a.a(eventName);
    }

    public void reportEvent(@NonNull String eventName, @Nullable String jsonValue) throws yh {
        f2390a.a(eventName);
    }

    public void reportEvent(@NonNull String eventName, @Nullable Map<String, Object> map) throws yh {
        f2390a.a(eventName);
    }

    public void reportError(@NonNull String message, @Nullable Throwable error) throws yh {
        f2391b.a(message);
    }

    public void reportUnhandledException(@NonNull Throwable exception) throws yh {
        f2392c.a(exception);
    }

    public void resumeSession() {
    }

    public void pauseSession() {
    }

    public void setUserProfileID(@Nullable String profileID) {
    }

    public void reportUserProfile(@NonNull UserProfile profile) throws yh {
        f2393d.a(profile);
    }

    public void reportRevenue(@NonNull Revenue revenue) throws yh {
        f2394e.a(revenue);
    }

    public void setStatisticsSending(boolean enabled) {
    }

    public void sendEventsBuffer() {
    }
}
