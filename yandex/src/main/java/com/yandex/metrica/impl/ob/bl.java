package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.yandex.metrica.impl.ob.bl */
public class bl extends xl {
    @NonNull

    /* renamed from: a */
    private final Executor a;

    /* renamed from: B */
    private Executor b;

    /* renamed from: a */
    private final BlockingQueue<bl.a> c = new LinkedBlockingQueue();

    /* renamed from: d */
    private final Object d = new Object();

    /* renamed from: e */
    private final Object e = new Object();

    /* renamed from: f */
    private volatile bl.a f;
    @NonNull

    /* renamed from: g */
    private pi g;

    /* renamed from: h */
    private String h;

    /* renamed from: com.yandex.metrica.impl.ob.bl$a */
    private static class a {
        @NonNull

        /* renamed from: a */
        final bo a;
        @NonNull

        /* renamed from: B */
        private final String b;

        private a(@NonNull bo boVar) {
            this.a = boVar;
            this.b = boVar.mo476n();
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            return this.b.equals(((bl.a) o).b);
        }

        public int hashCode() {
            return this.b.hashCode();
        }
    }

    public bl(@NonNull Context context, @NonNull ek ekVar, @NonNull Executor executor) {
        this.a = executor;
        this.b = new xd();
        this.h = String.format(Locale.US, "[%s:%s]", new Object[]{"NetworkTaskQueue", ekVar.toString()});
        this.g = new pi(context);
    }

    /* renamed from: a */
    public void a(bo boVar) {
        synchronized (this.d) {
            bl.a aVar = new a(boVar);
            if (!a(aVar)) {
                aVar.a.mo447C();
                this.c.offer(aVar);
            }
        }
    }

    /* renamed from: a */
    public void a() {
        synchronized (this.e) {
            bl.a aVar = this.f;
            if (aVar != null) {
                aVar.a.mo485w();
                aVar.a.D();
            }
            while (!this.c.isEmpty()) {
                try {
                    ((bl.a) this.c.take()).a.D();
                } catch (InterruptedException e) {
                }
            }
            mo2611b();
        }
    }

    public void run() {

        bo var1 = null;

        while(this.c()) {
            boolean var15 = false;

            label143: {
                try {
                    var15 = true;
                    synchronized(this.e) {
                        ;
                    }

                    this.f = (bl.a)this.c.take();
                    var1 = this.f.a;
                    this.c(var1).execute(this.b(var1));
                    var15 = false;
                    break label143;
                } catch (InterruptedException var20) {
                    var15 = false;
                } finally {
                    if (var15) {
                        synchronized(this.e) {
                            this.f = null;
                            if (var1 != null) {
                                var1.D();
                            }

                        }
                    }
                }

                synchronized(this.e) {
                    this.f = null;
                    if (var1 != null) {
                        var1.D();
                    }
                    continue;
                }
            }

            synchronized(this.e) {
                this.f = null;
                if (var1 != null) {
                    var1.D();
                }
            }
        }


    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    /* renamed from: B */
    public br b(@NonNull bo boVar) {
        return new br(this.g, boVar, this, this.h);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public Executor c(bo boVar) {
        if (boVar.mo477o()) {
            return this.a;
        }
        return this.b;
    }

    /* renamed from: a */
    private boolean a(bl.a aVar) {
        return this.c.contains(aVar) || aVar.equals(this.f);
    }
}
