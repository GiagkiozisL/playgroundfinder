package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.yg */
public class yg<T> implements yk<T> {
    @NonNull

    /* renamed from: a */
    private final yk<T> f3007a;

    public yg(@NonNull yk<T> ykVar) {
        this.f3007a = ykVar;
    }

    /* renamed from: a */
    public yi a(@Nullable T t) {
        yi a = this.f3007a.a(t);
        if (a.a()) {
            return a;
        }
        throw new yh(a.b());
    }
}
