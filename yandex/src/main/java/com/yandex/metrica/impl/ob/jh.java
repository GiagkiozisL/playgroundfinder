package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.jh */
public enum jh {
    FOREGROUND(0),
    BACKGROUND(1);
    

    /* renamed from: a */
    private final int f1133c;

    private jh(int i) {
        this.f1133c = i;
    }

    /* renamed from: a */
    public int a() {
        return this.f1133c;
    }

    /* renamed from: a */
    public static jh a(Integer num) {
        jh jhVar = FOREGROUND;
        if (num == null) {
            return jhVar;
        }
        switch (num.intValue()) {
            case 0:
                return FOREGROUND;
            case 1:
                return BACKGROUND;
            default:
                return jhVar;
        }
    }
}
