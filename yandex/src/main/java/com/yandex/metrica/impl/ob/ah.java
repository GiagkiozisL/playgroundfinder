package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.ah */
public class ah {
    @NonNull
    /* renamed from: a */
    public File mo263a(@NonNull Context context) {
        if (am.m348a()) {
            return context.getNoBackupFilesDir();
        }
        return context.getFilesDir();
    }

    @NonNull
    /* renamed from: B */
    public File mo266b(@NonNull Context context) {
        return new File(mo263a(context), "appmetrica_crashes");
    }

    @NonNull
    /* renamed from: a */
    public File mo267c(@NonNull Context context) {
        return new File(mo263a(context), "YandexMetricaNativeCrashes");
    }

    @NonNull
    /* renamed from: a */
    public File mo265a(@NonNull String str) {
        return new File(str);
    }

    @NonNull
    /* renamed from: a */
    public File mo264a(@NonNull File file, @NonNull String str) {
        return new File(file, str);
    }
}
