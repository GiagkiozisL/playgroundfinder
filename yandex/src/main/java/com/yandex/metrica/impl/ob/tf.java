package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.tf */
public class tf {
    @NonNull
    /* renamed from: a */
    public List<th> mo2235a(@NonNull List<th> list) {
        ArrayList arrayList = new ArrayList();
        for (th thVar : list) {
            ArrayList arrayList2 = new ArrayList(thVar.f2573b.size());
            for (String str : thVar.f2573b) {
                if (ci.m946b(str)) {
                    arrayList2.add(str);
                }
            }
            if (!arrayList2.isEmpty()) {
                arrayList.add(new th(thVar.f2572a, arrayList2));
            }
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: B */
    public JSONObject mo2236b(@NonNull List<th> list) {
        JSONObject jSONObject = new JSONObject();
        for (th thVar : list) {
            try {
                jSONObject.put(thVar.f2572a, new JSONObject().put("classes", new JSONArray(thVar.f2573b)));
            } catch (Throwable th) {
            }
        }
        return jSONObject;
    }
}
