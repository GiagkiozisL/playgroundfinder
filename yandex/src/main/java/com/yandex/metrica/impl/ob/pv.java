package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Build.VERSION;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.pv */
public class pv {
    @Nullable
    /* renamed from: a */
    public List<pu> mo1665a(Context context, @Nullable List<pu> list) {
        List<pu> a = mo1664a(context).a();
        if (vj.a(a, list)) {
            return null;
        }
        return a;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public pt mo1664a(Context context) {
        if (VERSION.SDK_INT >= 16) {
            return new pw(context);
        }
        return new pz(context);
    }
}
