package com.yandex.metrica.impl.ob;

import android.os.ParcelUuid;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rr.a.b.Aa.C0869c;
import com.yandex.metrica.impl.ob.tt.a.C1043c;

/* renamed from: com.yandex.metrica.impl.ob.nn */
public class nn implements mq<C1043c, C0869c> {
    @NonNull
    /* renamed from: a */
    public C0869c b(@NonNull C1043c cVar) {
        C0869c cVar2 = new C0869c();
        cVar2.f2149b = cVar.f2630a.toString();
        if (cVar.f2631b != null) {
            cVar2.f2150c = cVar.f2631b.toString();
        }
        return cVar2;
    }

    @NonNull
    /* renamed from: a */
    public C1043c a(@NonNull C0869c cVar) {
        return new C1043c(ParcelUuid.fromString(cVar.f2149b), TextUtils.isEmpty(cVar.f2150c) ? null : ParcelUuid.fromString(cVar.f2150c));
    }
}
