package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.fs */
public class fs implements fp, uh {
    @NonNull

    /* renamed from: a */
    private final Context f928a;
    @NonNull

    /* renamed from: B */
    private ei f929b;
    @Nullable

    /* renamed from: a */
    private final ResultReceiver f930c;

    public fs(@NonNull Context context, @NonNull ei eiVar, @NonNull eg egVar) {
        this.f928a = context;
        this.f929b = eiVar;
        this.f930c = egVar.f767c;
        this.f929b.mo778a(this);
    }

    /* renamed from: a */
    public void a(@NonNull w wVar, @NonNull eg egVar) {
        this.f929b.a(egVar.f766b);
        this.f929b.mo781a(wVar, this);
    }

    /* renamed from: a */
    public void a(@Nullable uk ukVar) {
        x.m4410a(this.f930c, ukVar);
    }

    /* renamed from: a */
    public void a(@NonNull ue ueVar, @Nullable uk ukVar) {
    }

    /* renamed from: a */
    public void a() {
        this.f929b.mo783b(this);
    }

    @NonNull
    /* renamed from: B */
    public ei mo922b() {
        return this.f929b;
    }
}
