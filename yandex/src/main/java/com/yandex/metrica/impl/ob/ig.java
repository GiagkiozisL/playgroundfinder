package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.ig */
public class ig {

    /* renamed from: a */
    private final hi f1032a;

    /* renamed from: B */
    private final hm f1033b;

    /* renamed from: a */
    private final ho f1034c;

    /* renamed from: d */
    private final hq f1035d;

    /* renamed from: e */
    private final in f1036e;

    /* renamed from: f */
    private final hr f1037f;

    /* renamed from: g */
    private final hp f1038g;

    /* renamed from: h */
    private final hs f1039h;

    /* renamed from: i */
    private final hb f1040i;

    /* renamed from: j */
    private final ha f1041j;

    /* renamed from: k */
    private final hg f1042k;

    /* renamed from: l */
    private final hl f1043l;

    /* renamed from: m */
    private final hk f1044m;

    /* renamed from: n */
    private final hf f1045n;

    /* renamed from: o */
    private final ht f1046o;

    /* renamed from: p */
    private final hc f1047p;

    /* renamed from: q */
    private final hh f1048q;

    /* renamed from: r */
    private final he f1049r;

    public ig(en enVar) {
        this.f1032a = new hi(enVar);
        this.f1033b = new hm(enVar);
        this.f1034c = new ho(enVar);
        this.f1035d = new hq(enVar);
        this.f1036e = new in(enVar);
        this.f1037f = new hr(enVar);
        this.f1038g = new hp(enVar);
        this.f1039h = new hs(enVar);
        this.f1040i = new hb(enVar);
        this.f1041j = new ha(enVar);
        this.f1042k = new hg(enVar);
        this.f1043l = new hl(enVar);
        this.f1044m = new hk(enVar, new pv());
        this.f1045n = new hf(enVar);
        this.f1046o = new ht(enVar);
        this.f1047p = new hc(enVar);
        this.f1048q = new hh(enVar);
        this.f1049r = new he(enVar, tl.a(enVar.mo824k()));
    }

    /* renamed from: a */
    public hi mo991a() {
        return this.f1032a;
    }

    /* renamed from: B */
    public hm mo992b() {
        return this.f1033b;
    }

    /* renamed from: a */
    public ho mo993c() {
        return this.f1034c;
    }

    /* renamed from: d */
    public hq mo994d() {
        return this.f1035d;
    }

    /* renamed from: e */
    public in mo995e() {
        return this.f1036e;
    }

    /* renamed from: f */
    public hr mo996f() {
        return this.f1037f;
    }

    /* renamed from: g */
    public hp mo997g() {
        return this.f1038g;
    }

    /* renamed from: h */
    public hs mo998h() {
        return this.f1039h;
    }

    /* renamed from: i */
    public hb mo999i() {
        return this.f1040i;
    }

    /* renamed from: j */
    public ha mo1000j() {
        return this.f1041j;
    }

    /* renamed from: k */
    public hg mo1001k() {
        return this.f1042k;
    }

    /* renamed from: l */
    public hl mo1002l() {
        return this.f1043l;
    }

    /* renamed from: m */
    public hk mo1003m() {
        return this.f1044m;
    }

    /* renamed from: n */
    public hf mo1004n() {
        return this.f1045n;
    }

    /* renamed from: o */
    public ht mo1005o() {
        return this.f1046o;
    }

    /* renamed from: p */
    public hc mo1006p() {
        return this.f1047p;
    }

    /* renamed from: q */
    public hh mo1007q() {
        return this.f1048q;
    }

    /* renamed from: r */
    public he mo1008r() {
        return this.f1049r;
    }
}
