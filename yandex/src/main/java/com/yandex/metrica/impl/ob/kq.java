package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rg.C0813d;

/* renamed from: com.yandex.metrica.impl.ob.kq */
public class kq implements mq<StackTraceElement, C0813d> {
    @NonNull
    /* renamed from: a */
    public C0813d b(@NonNull StackTraceElement stackTraceElement) {
        C0813d dVar = new C0813d();
        dVar.f1890b = stackTraceElement.getClassName();
        dVar.f1891c = wk.m4373b(stackTraceElement.getFileName(), "");
        dVar.f1892d = stackTraceElement.getLineNumber();
        dVar.f1893e = stackTraceElement.getMethodName();
        dVar.f1894f = stackTraceElement.isNativeMethod();
        return dVar;
    }

    @NonNull
    /* renamed from: a */
    public StackTraceElement a(@NonNull C0813d dVar) {
        throw new UnsupportedOperationException();
    }
}
