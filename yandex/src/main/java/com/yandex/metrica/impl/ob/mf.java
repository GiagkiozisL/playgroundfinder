package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.mf */
public interface mf<T> {
    @NonNull
    /* renamed from: a */
    T a();

    /* renamed from: a */
    void a(@NonNull T t);
}
