package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.mk */
public class mk<T> implements ml<T> {
    @NonNull

    /* renamed from: a */
    private final ml<T> f1480a;
    @NonNull

    /* renamed from: B */
    private final Crypto crypto;

    public mk(@NonNull ml<T> mlVar, @NonNull Crypto cryptoVar) {
        this.f1480a = mlVar;
        this.crypto = cryptoVar;
    }

    @NonNull
    /* renamed from: a */
    public byte[] a(@NonNull T t) {
        try {
            return this.crypto.encrypt(this.f1480a.a(t));
        } catch (Throwable th) {
            return new byte[0];
        }
    }

    @NonNull
    /* renamed from: B */
    public T b(@NonNull byte[] bArr) throws IOException {
        try {
            return this.f1480a.b(this.crypto.decrypt(bArr));
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    @NonNull
    /* renamed from: a */
    public T c() {
        return this.f1480a.c();
    }
}
