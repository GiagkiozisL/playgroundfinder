package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.k */
public class k {
    @Nullable

    /* renamed from: a */
    private final LocationManager f1189a;

    public k(@NonNull Context context) {
        this((LocationManager) context.getSystemService("location"));
    }

    @VisibleForTesting
    k(@Nullable LocationManager locationManager) {
        this.f1189a = locationManager;
    }

    @NonNull
    /* renamed from: a */
    public List<String> mo1139a() {
        ArrayList arrayList = new ArrayList();
        if (this.f1189a != null) {
            return this.f1189a.getProviders(true);
        }
        return arrayList;
    }
}
