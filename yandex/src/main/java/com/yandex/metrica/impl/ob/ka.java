package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.ka */
public class ka extends jy {
    @NonNull

    /* renamed from: a */
    private final jv f1190a;
    @Nullable

    /* renamed from: B */
    private final de f1191b;

    public ka(@NonNull Context context, @NonNull jv jvVar, @Nullable de deVar) {
        super(context);
        this.f1190a = jvVar;
        this.f1191b = deVar;
    }

    /* renamed from: a */
    public void mo1136a(@Nullable Bundle bundle, @Nullable jw jwVar) {
        this.f1190a.mo1133a();
        if (this.f1191b != null) {
            this.f1191b.mo695a(mo1138a());
        }
        if (jwVar != null) {
            jwVar.a();
        }
    }
}
