package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.te */
public class te {
    @NonNull

    /* renamed from: a */
    public final List<th> f2564a;
    @NonNull

    /* renamed from: B */
    public final String f2565b;

    /* renamed from: a */
    public final long f2566c;

    /* renamed from: d */
    public final boolean f2567d;

    /* renamed from: e */
    public final boolean f2568e;

    public te(@NonNull List<th> list, @NonNull String str, long j, boolean z, boolean z2) {
        this.f2564a = Collections.unmodifiableList(list);
        this.f2565b = str;
        this.f2566c = j;
        this.f2567d = z;
        this.f2568e = z2;
    }

    public String toString() {
        return "SdkFingerprintingState{sdkItemList=" + this.f2564a + ", etag='" + this.f2565b + '\'' + ", lastAttemptTime=" + this.f2566c + ", hasFirstCollectionOccurred=" + this.f2567d + ", shouldRetry=" + this.f2568e + '}';
    }
}
