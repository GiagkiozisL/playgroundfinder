package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.d;
import com.yandex.metrica.impl.ob.rs.a.aa;
import com.yandex.metrica.impl.ob.rs.a.b;

/* renamed from: com.yandex.metrica.impl.ob.tj */
public class tj {
    @NonNull

    /* renamed from: a */
    private final tk f2574a;
    @NonNull

    /* renamed from: B */
    private final d f2575b;

    public tj(@NonNull Context context) {
        this(new tk(), tl.a(context));
    }

    /* renamed from: a */
    public void mo2241a(@NonNull b bVar) {
        this.f2575b.a("provided_request_result", this.f2574a.mo2244a(bVar));
    }

    /* renamed from: a */
    public void mo2240a(@NonNull aa aVar) {
        this.f2575b.a("provided_request_schedule", this.f2574a.mo2243a(aVar));
    }

    /* renamed from: B */
    public void b(@NonNull rs.a.aa aVar) {
        this.f2575b.a("provided_request_send", this.f2574a.mo2243a(aVar));
    }

    @VisibleForTesting
    tj(@NonNull tk tkVar, @NonNull d dVar) {
        this.f2574a = tkVar;
        this.f2575b = dVar;
    }
}
