package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.wf */
public class wf {
    @NonNull

    /* renamed from: a */
    private final wg f2933a;

    public wf() {
        this(new wg());
    }

    @VisibleForTesting
    public wf(@NonNull wg wgVar) {
        this.f2933a = wgVar;
    }

    /* renamed from: a */
    public long mo2560a(long j, @NonNull TimeUnit timeUnit) {
        return TimeUnit.MILLISECONDS.toSeconds(mo2561b(j, timeUnit));
    }

    /* renamed from: B */
    public long mo2561b(long j, @NonNull TimeUnit timeUnit) {
        return this.f2933a.c() - timeUnit.toMillis(j);
    }

    /* renamed from: a */
    public long mo2562c(long j, @NonNull TimeUnit timeUnit) {
        return this.f2933a.mo2568d() - timeUnit.toNanos(j);
    }

    /* renamed from: d */
    public long mo2563d(long j, @NonNull TimeUnit timeUnit) {
        return TimeUnit.NANOSECONDS.toSeconds(mo2562c(j, timeUnit));
    }

    /* renamed from: e */
    public long mo2564e(long j, @NonNull TimeUnit timeUnit) {
        return this.f2933a.b() - timeUnit.toSeconds(j);
    }
}
