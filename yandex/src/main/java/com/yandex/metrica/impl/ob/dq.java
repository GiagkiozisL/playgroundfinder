package com.yandex.metrica.impl.ob;

import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/* renamed from: com.yandex.metrica.impl.ob.dq */
public class dq {

    /* renamed from: a */
    private final C0281a f731a;

    /* renamed from: B */
    private final wl<Thread, StackTraceElement[], kk> f732b;

    /* renamed from: com.yandex.metrica.impl.ob.dq$a */
    interface C0281a {
        /* renamed from: a */
        Thread mo719a();

        /* renamed from: B */
        Map<Thread, StackTraceElement[]> mo720b();
    }

    public dq() {
        this(new C0281a() {
            /* renamed from: a */
            public Thread mo719a() {
                return Looper.getMainLooper().getThread();
            }

            /* renamed from: B */
            public Map<Thread, StackTraceElement[]> mo720b() {
                return Thread.getAllStackTraces();
            }
        }, new dp());
    }

    @VisibleForTesting
    dq(@NonNull C0281a aVar, @NonNull wl<Thread, StackTraceElement[], kk> wlVar) {
        this.f731a = aVar;
        this.f732b = wlVar;
    }

    /* renamed from: a */
    public kg mo717a() {
        Thread a = this.f731a.mo719a();
        return new kg(m1366b(a), m1365a(a, null));
    }

    /* renamed from: a */
    public List<kk> mo718a(@Nullable Thread thread) {
        Thread a = this.f731a.mo719a();
        List<kk> a2 = m1365a(a, thread);
        if (thread != a) {
            a2.add(0, m1366b(a));
        }
        return a2;
    }

    /* renamed from: B */
    private kk m1366b(@NonNull Thread thread) {
        StackTraceElement[] stackTraceElementArr = null;
        try {
            stackTraceElementArr = thread.getStackTrace();
        } catch (SecurityException e) {
        }
        return (kk) this.f732b.a(thread, stackTraceElementArr);
    }

    /* renamed from: a */
    private List<kk> m1365a(@NonNull Thread thread, @Nullable Thread thread2) {

        ArrayList arrayList = new ArrayList();
        TreeMap treeMap = new TreeMap(new Comparator<Thread>() {
            /* renamed from: a */
            public int compare(Thread thread, Thread thread2) {
                if (thread == thread2) {
                    return 0;
                }
                return cu.m1161d(thread.getName(), thread2.getName());
            }
        });
        Map map = null;
        try {
            map = this.f731a.mo720b();
        } catch (SecurityException e) {
        }
        if (map != null) {
            treeMap.putAll(map);
        }
        if (thread2 != null) {
            treeMap.remove(thread2);
        }
        Iterator var7 = treeMap.entrySet().iterator();

        while(var7.hasNext()) {
            Entry var8 = (Entry)var7.next();
            Thread var9 = (Thread)var8.getKey();
            if (var9 != thread && var9 != thread2) {
                StackTraceElement[] var10 = (StackTraceElement[])var8.getValue();
                arrayList.add(this.f732b.a(var9, var10));
            }
        }
        return arrayList;
    }
}
