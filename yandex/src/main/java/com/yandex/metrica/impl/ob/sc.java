package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.f;
import com.yandex.metrica.j;

/* renamed from: com.yandex.metrica.impl.ob.sc */
public class sc {
    @NonNull
    /* renamed from: a */
    public j mo2048a(@NonNull j jVar) {
        if (cx.m1189a((Object) jVar.maxReportsInDatabaseCount)) {
            return j.m4544a(jVar).mo2703e(m3522a(jVar.maxReportsInDatabaseCount, jVar.apiKey)).mo2697b();
        }
        return jVar;
    }

    @NonNull
    /* renamed from: a */
    public f mo2047a(@NonNull f fVar) {
        if (cx.m1189a((Object) fVar.maxReportsInDatabaseCount)) {
            return f.m120a(fVar).mo155d(m3522a(fVar.maxReportsInDatabaseCount, fVar.apiKey)).mo153b();
        }
        return fVar;
    }

    /* renamed from: a */
    private int m3522a(@Nullable Integer num, @NonNull String str) {
        if (num.intValue() < 100) {
            vr.m4237a(str).mo2482b("Value passed as maxReportsInDatabaseCount is invalid. Should be greater than or equal to %d, but was: %d. Default value (%d) will be used", Integer.valueOf(100), num, Integer.valueOf(100));
            return 100;
        } else if (num.intValue() <= 10000) {
            return num.intValue();
        } else {
            vr.m4237a(str).mo2482b("Value passed as maxReportsInDatabaseCount is invalid. Should be less than or equal to %d, but was: %d. Default value (%d) will be used", Integer.valueOf(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT_UPPER_BOUND), num, Integer.valueOf(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT_UPPER_BOUND));
            return YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT_UPPER_BOUND;
        }
    }
}
