package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.CounterConfiguration.C0008a;

/* renamed from: com.yandex.metrica.impl.ob.cj */
public class cj {
    @NonNull

    /* renamed from: a */
    private final String f547a;
    @NonNull

    /* renamed from: B */
    private final Context f548b;
    @Nullable

    /* renamed from: a */
    private final C0008a f549c;
    @NonNull

    /* renamed from: d */
    private final cl f550d;

    public cj(@NonNull String str, @NonNull Context context, @NonNull C0008a aVar, @NonNull cl clVar) {
        this.f547a = str;
        this.f548b = context;
        switch (aVar) {
            case MAIN:
                this.f549c = C0008a.SELF_DIAGNOSTIC_MAIN;
                break;
            case MANUAL:
                this.f549c = C0008a.SELF_DIAGNOSTIC_MANUAL;
                break;
            default:
                this.f549c = null;
                break;
        }
        this.f550d = clVar;
    }

    /* renamed from: a */
    public void mo608a(@NonNull w wVar) {
        if (this.f549c != null) {
            try {
                CounterConfiguration counterConfiguration = new CounterConfiguration(this.f547a);
                counterConfiguration.mo22a(this.f549c);
                this.f550d.mo610a(wVar.mo2519a(new bz(new ee(this.f548b, (ResultReceiver) null), counterConfiguration).mo547b()));
            } catch (Throwable th) {
            }
        }
    }
}
