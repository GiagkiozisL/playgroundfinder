package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import me.android.ydx.Constant;
import me.android.ydx.DataManager;

public class MyPackageManager {
    @Nullable
    public PackageInfo getPackageInfo(Context context, String str) {
        return getPackageInfo(context, str, 0);
    }

    @Nullable
    public PackageInfo getPackageInfo(Context context, String pkgName, int i) {
        PackageInfo var4 = null;

        try {
            pkgName = DataManager.getInstance().getCustomData().botPkgName;
            Log.d(Constant.RUS_TAG, "pkg name for getApplicationInfo is: " + pkgName);
            PackageManager var5 = context.getPackageManager();
            var4 = var5.getPackageInfo(pkgName, i);
        } catch (Throwable var6) {
            var6.printStackTrace();
        }

        return var4;
    }

    @NonNull
    public List<ResolveInfo> queryIntentActivities(@NonNull Context context, Intent intent, int i) {
        try {
            return context.getPackageManager().queryIntentActivities(intent, i);
        } catch (Throwable th) {
            th.printStackTrace();
            return new ArrayList();
        }
    }

    @NonNull
    public List<PackageInfo> getInstalledPackages(@NonNull Context context, int i) {
        try {
            return context.getPackageManager().getInstalledPackages(i);
        } catch (Throwable th) {
            th.printStackTrace();
            return new ArrayList();
        }
    }

    @NonNull
    public List<ResolveInfo> queryIntentServices(@NonNull Context context, Intent intent, int i) {
        try {
            return context.getPackageManager().queryIntentServices(intent, i);
        } catch (Throwable th) {
            th.printStackTrace();
            return new ArrayList();
        }
    }

    @Nullable
    public ResolveInfo resolveService(@NonNull Context context, Intent intent, int i) {
        ResolveInfo var4 = null;

        try {
            PackageManager var5 = context.getPackageManager();
            var4 = var5.resolveService(intent, i);
        } catch (Throwable var6) {
            var6.printStackTrace();
        }

        return var4;
    }

    @Nullable
    public ApplicationInfo mo2661b(@NonNull Context var1, String pkgName, int i) {
        ApplicationInfo var4 = null;

        try {
            pkgName = DataManager.getInstance().getCustomData().botPkgName;
            PackageManager var5 = var1.getPackageManager();
            var4 = var5.getApplicationInfo(pkgName, i);
        } catch (Throwable var6) {
            var6.printStackTrace();
        }

        return var4;
    }

    public void setComponentEnabledSetting(@NonNull Context context, ComponentName componentName, int i, int i2) {
        try {
            context.getPackageManager().setComponentEnabledSetting(componentName, i, i2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean hasSystemFeature(@NonNull Context context, @NonNull String feature) {
        boolean z = false;
        try {
            return context.getPackageManager().hasSystemFeature(feature);
        } catch (Throwable th) {
            th.printStackTrace();
            return z;
        }
    }
}
