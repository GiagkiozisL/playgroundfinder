package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.r.C0802a;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.af */
public final class af {

    /* renamed from: a */
    public static final EnumSet<C0058a> f156a = EnumSet.of(C0058a.EVENT_TYPE_INIT, new C0058a[]{C0058a.EVENT_TYPE_FIRST_ACTIVATION, C0058a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, C0058a.EVENT_TYPE_SEND_REFERRER, C0058a.EVENT_TYPE_APP_UPDATE, C0058a.EVENT_TYPE_CLEANUP});

    /* renamed from: B */
    private static final EnumSet<C0058a> f157b = EnumSet.of(C0058a.EVENT_TYPE_UNDEFINED, new C0058a[]{C0058a.EVENT_TYPE_PURGE_BUFFER, C0058a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, C0058a.EVENT_TYPE_SEND_REFERRER, C0058a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, C0058a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, C0058a.EVENT_TYPE_ACTIVATION, C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH});

    /* renamed from: a */
    private static final EnumSet<C0058a> f158c = EnumSet.of(C0058a.EVENT_TYPE_SET_USER_INFO, new C0058a[]{C0058a.EVENT_TYPE_REPORT_USER_INFO, C0058a.EVENT_TYPE_IDENTITY, C0058a.EVENT_TYPE_UNDEFINED, C0058a.EVENT_TYPE_INIT, C0058a.EVENT_TYPE_APP_UPDATE, C0058a.EVENT_TYPE_SEND_REFERRER, C0058a.EVENT_TYPE_ALIVE, C0058a.EVENT_TYPE_STARTUP, C0058a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, C0058a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, C0058a.EVENT_TYPE_ACTIVATION, C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH});

    /* renamed from: d */
    private static final EnumSet<C0058a> f159d = EnumSet.of(C0058a.EVENT_TYPE_UPDATE_FOREGROUND_TIME, C0058a.EVENT_TYPE_SET_USER_INFO, C0058a.EVENT_TYPE_REPORT_USER_INFO, C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE);

    /* renamed from: e */
    private static final EnumSet<C0058a> f160e = EnumSet.of(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED, new C0058a[]{C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, C0058a.EVENT_TYPE_EXCEPTION_USER, C0058a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, C0058a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, C0058a.EVENT_TYPE_REGULAR});

    /* renamed from: f */
    private static final EnumSet<C0058a> f161f = EnumSet.of(C0058a.EVENT_TYPE_DIAGNOSTIC, C0058a.EVENT_TYPE_DIAGNOSTIC_STATBOX, C0058a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING);

    /* renamed from: g */
    private static final EnumSet<C0058a> f162g = EnumSet.of(C0058a.EVENT_TYPE_REGULAR);

    /* renamed from: h */
    private static final EnumSet<C0058a> f163h = EnumSet.of(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH);

    @SuppressLint({"UseSparseArrays"})
    /* renamed from: com.yandex.metrica.impl.ob.af$a */
    public enum C0058a {
        EVENT_TYPE_UNDEFINED(-1, "Unrecognized action"),
        EVENT_TYPE_INIT(0, "First initialization event"),
        EVENT_TYPE_REGULAR(1, "Regular event"),
        EVENT_TYPE_UPDATE_FOREGROUND_TIME(3, "Update foreground time"),
        EVENT_TYPE_EXCEPTION_USER(5, "Error from developer"),
        EVENT_TYPE_ALIVE(7, "App is still alive"),
        EVENT_TYPE_SET_USER_INFO(9, "User info"),
        EVENT_TYPE_REPORT_USER_INFO(10, "Report user info"),
        EVENT_TYPE_SEND_USER_PROFILE(40961, "Send user profile"),
        EVENT_TYPE_SET_USER_PROFILE_ID(40962, "Set user profile ID"),
        EVENT_TYPE_SEND_REVENUE_EVENT(40976, "Send revenue event"),
        EVENT_TYPE_PURGE_BUFFER(256, "Forcible buffer clearing"),
        EVENT_TYPE_PREV_SESSION_NATIVE_CRASH(768, "Native crash of app, read from file"),
        EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH(769, "Native crash of app, caught by FileObserver"),
        EVENT_TYPE_STARTUP(1536, "Sending the startup due to lack of data"),
        EVENT_TYPE_IDENTITY(1792, "System identification"),
        EVENT_TYPE_IDENTITY_LIGHT(1793, "Identity light"),
        EVENT_TYPE_DIAGNOSTIC(2048, "Diagnostic event"),
        EVENT_TYPE_DIAGNOSTIC_STATBOX(2049, "Diagnostic statbox event"),
        EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING(2050, "Disable stat send"),
        EVENT_TYPE_STATBOX(2304, "Event with statistical data"),
        EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST(4096, "Referrer received"),
        EVENT_TYPE_SEND_REFERRER(4097, "Send referrer"),
        EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES(4098, "Referrer obtained"),
        EVENT_TYPE_APP_ENVIRONMENT_UPDATED(5376, "App Environment Updated"),
        EVENT_TYPE_APP_ENVIRONMENT_CLEARED(5632, "App Environment Cleared"),
        EVENT_TYPE_EXCEPTION_UNHANDLED(5888, "Crash of App"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE(5889, "Crash of App, read from file"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT(5890, "Crash of App, passed to server via intent"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF(5891, "Crash of App"),
        EVENT_TYPE_EXCEPTION_USER_PROTOBUF(5892, "Error from developer"),
        EVENT_TYPE_ANR(5968, "ANR"),
        EVENT_TYPE_ACTIVATION(6144, "Activation of metrica"),
        EVENT_TYPE_FIRST_ACTIVATION(6145, "First activation of metrica"),
        EVENT_TYPE_START(6400, "Start of new session"),
        EVENT_TYPE_CUSTOM_EVENT(8192, "Custom event"),
        EVENT_TYPE_APP_OPEN(8208, "App open event"),
        EVENT_TYPE_APP_UPDATE(8224, "App update event"),
        EVENT_TYPE_PERMISSIONS(12288, "Permissions changed event"),
        EVENT_TYPE_APP_FEATURES(12289, "Features, required by application"),
        EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG(16384, "Update pre-activation config"),
        EVENT_TYPE_CLEANUP(12290, "Cleanup database");
        

        /* renamed from: Q */
        static HashMap<Integer, C0058a> f180Q = null;

        /* renamed from: R */
        private final int f208R;

        /* renamed from: S */
        private final String f209S;

        static {
            int i;
            C0058a[] values;
            f180Q = new HashMap<>();
            for (C0058a aVar : values()) {
                f180Q.put(Integer.valueOf(aVar.mo259a()), aVar);
            }
        }

        private C0058a(int i, String str) {
            this.f208R = i;
            this.f209S = str;
        }

        /* renamed from: a */
        public int mo259a() {
            return this.f208R;
        }

        /* renamed from: B */
        public String mo260b() {
            return this.f209S;
        }

        /* renamed from: a */
        public static C0058a m298a(int i) {
            C0058a aVar = (C0058a) f180Q.get(Integer.valueOf(i));
            return aVar == null ? EVENT_TYPE_UNDEFINED : aVar;
        }
    }

    /* renamed from: a */
    public static boolean m281a(C0058a aVar) {
        return !f157b.contains(aVar);
    }

    /* renamed from: B */
    public static boolean m286b(C0058a aVar) {
        return !f158c.contains(aVar);
    }

    /* renamed from: a */
    public static boolean m280a(int i) {
        return f159d.contains(C0058a.m298a(i));
    }

    /* renamed from: B */
    public static boolean m285b(int i) {
        return f160e.contains(C0058a.m298a(i));
    }

    /* renamed from: a */
    public static boolean m289c(int i) {
        return f162g.contains(C0058a.m298a(i));
    }

    /* renamed from: d */
    public static boolean m291d(int i) {
        return f161f.contains(C0058a.m298a(i));
    }

    /* renamed from: e */
    public static boolean m293e(int i) {
        return !f163h.contains(C0058a.m298a(i));
    }

    /* renamed from: a */
    public static w m275a(@NonNull String str, @NonNull vz vzVar) {
        return m272a(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, str, vzVar);
    }

    /* renamed from: B */
    public static w m283b(@NonNull String str, @NonNull vz vzVar) {
        return m272a(C0058a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, str, vzVar);
    }

    /* renamed from: a */
    static w m272a(C0058a aVar, String str, @NonNull vz vzVar) {
        return new r(str, "", aVar.mo259a(), vzVar);
    }

    /* renamed from: a */
    public static w m271a(C0058a aVar, @NonNull vz vzVar) {
        return new r("", aVar.mo259a(), vzVar);
    }

    /* renamed from: a */
    public static w m287c(String str, @NonNull vz vzVar) {
        return new r(str, C0058a.EVENT_TYPE_REGULAR.mo259a(), vzVar);
    }

    /* renamed from: a */
    static w m276a(String str, String str2, @NonNull vz vzVar) {
        return new r(str2, str, C0058a.EVENT_TYPE_REGULAR.mo259a(), vzVar);
    }

    /* renamed from: B */
    static w m284b(String str, String str2, @NonNull vz vzVar) {
        return new r(str2, str, C0058a.EVENT_TYPE_STATBOX.mo259a(), vzVar);
    }

    /* renamed from: a */
    public static w m269a() {
        w a = new w().mo2520a(C0058a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.mo259a());
        try {
            a.mo1767c(vq.m4219a().toString());
        } catch (Throwable th) {
        }
        return a;
    }

    /* renamed from: a */
    static w m279a(byte[] bArr, @NonNull vz vzVar) {
        return new r(bArr, "", C0058a.EVENT_TYPE_ANR.mo259a(), vzVar);
    }

    /* renamed from: a */
    static w m278a(String str, byte[] bArr, @NonNull vz vzVar) {
        return new r(bArr, str, C0058a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.mo259a(), vzVar);
    }

    /* renamed from: d */
    static w m290d(String str, @NonNull vz vzVar) {
        return new r(str, C0058a.EVENT_TYPE_START.mo259a(), vzVar);
    }

    /* renamed from: e */
    static w m292e(String str, @NonNull vz vzVar) {
        return new r(str, C0058a.EVENT_TYPE_UPDATE_FOREGROUND_TIME.mo259a(), vzVar);
    }

    /* renamed from: B */
    public static r m282b(String str, byte[] bArr, @NonNull vz vzVar) {
        return m268a(bArr, str, C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, vzVar);
    }

    /* renamed from: a */
    public static w m277a(String str, byte[] bArr, int i, @NonNull HashMap<C0802a, Integer> hashMap, @NonNull vz vzVar) {
        return m268a(bArr, str, C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, vzVar).mo1762a(hashMap).mo2527c(i);
    }

    /* renamed from: a */
    private static r m268a(byte[] bArr, String str, C0058a aVar, @NonNull vz vzVar) {
        return new r(bArr, str, aVar.mo259a(), vzVar);
    }

    /* renamed from: f */
    public static w m294f(String str, @NonNull vz vzVar) {
        return new r("", str, C0058a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST.mo259a(), vzVar);
    }

    /* renamed from: a */
    public static w m274a(@Nullable sj sjVar, @NonNull vz vzVar) {
        if (sjVar == null) {
            return new r("", C0058a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.mo259a(), vzVar);
        }
        return new r(sjVar.installReferrer, C0058a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.mo259a(), vzVar).mo1764a(sjVar.mo2068a());
    }

    /* renamed from: g */
    static w m295g(String str, @NonNull vz vzVar) {
        return m288c("open", str, vzVar);
    }

    @Deprecated
    /* renamed from: h */
    static w m296h(String str, @NonNull vz vzVar) {
        return m288c("referral", str, vzVar);
    }

    /* renamed from: a */
    static w m288c(String str, String str2, @NonNull vz vzVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", str);
        hashMap.put("link", str2);
        return new r(vq.m4225b((Map) hashMap), "", C0058a.EVENT_TYPE_APP_OPEN.mo259a(), vzVar);
    }

    /* renamed from: a */
    public static w m273a(bu buVar, @NonNull vz vzVar) {
        return new r(buVar == null ? "" : buVar.mo498a(), "", C0058a.EVENT_TYPE_ACTIVATION.mo259a(), vzVar);
    }

    /* renamed from: i */
    public static w m297i(@NonNull String str, @NonNull vz vzVar) {
        return new r(str, "", C0058a.EVENT_TYPE_CLEANUP.mo259a(), vzVar);
    }

    /* renamed from: a */
    static w m270a(int i, String str, String str2, Map<String, Object> map, @NonNull vz vzVar) {
        return new r(str2, str, C0058a.EVENT_TYPE_CUSTOM_EVENT.mo259a(), i, vzVar).mo2530e(vq.m4225b((Map) map));
    }
}
