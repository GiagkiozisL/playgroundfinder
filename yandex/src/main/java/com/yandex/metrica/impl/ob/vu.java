package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.vu */
public class vu {

    /* renamed from: a */
    private String f2907a;

    /* renamed from: a */
    public String mo2503a() {
        if (this.f2907a != null) {
            return this.f2907a;
        }
        this.f2907a = VERSION.SDK_INT >= 18 ? m4246c() : m4245b();
        return this.f2907a;
    }

    @SuppressLint({"StaticFieldLeak"})
    /* renamed from: B */
    private String m4245b() {
        try {
            FutureTask futureTask = new FutureTask(new Callable<String>() {
                /* renamed from: a */
                public String call() {
                    return vu.this.m4247d();
                }
            });
            db.k().mo2602d().post(futureTask);
            return (String) futureTask.get(5, TimeUnit.SECONDS);
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    private String m4246c() {
        return m4247d();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"PrivateApi"})
    /* renamed from: d */
    public String m4247d() {
        try {
            Class cls = Class.forName("android.app.ActivityThread");
            return (String) cls.getMethod("getProcessName", new Class[0]).invoke(cls.getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]), new Object[0]);
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    /* renamed from: a */
    public boolean mo2504a(@NonNull String str) {
        try {
            if (TextUtils.isEmpty(mo2503a()) || !mo2503a().endsWith(":" + str)) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            return false;
        }
    }
}
