package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.hn */
public class hn extends hd {

    /* renamed from: a */
    private lu f1018a;

    public hn(en enVar) {
        this(enVar, enVar.mo834u());
    }

    @VisibleForTesting
    hn(en enVar, lu luVar) {
        super(enVar);
        this.f1018a = luVar;
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        w d;
        en a = mo973a();
        if (!this.f1018a.mo1282c()) {
            if (a.mo822i().mo2139P()) {
                d = w.m4289f(wVar);
            } else {
                d = w.m4287d(wVar);
            }
            a.mo818e().mo889e(d.mo1767c(this.f1018a.mo1283d("")));
            this.f1018a.mo1276a();
            this.f1018a.mo1285e();
        }
        return false;
    }
}
