package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.ro */
public interface ro {

    /* renamed from: com.yandex.metrica.impl.ob.ro$a */
    public static final class C0852a extends e {

        /* renamed from: B */
        public C0853a[] f2068b;

        /* renamed from: com.yandex.metrica.impl.ob.ro$a$a */
        public static final class C0853a extends e {

            /* renamed from: d */
            private static volatile C0853a[] f2069d;

            /* renamed from: B */
            public String f2070b;

            /* renamed from: a */
            public boolean f2071c;

            /* renamed from: d */
            public static C0853a[] m3238d() {
                if (f2069d == null) {
                    synchronized (c.a) {
                        if (f2069d == null) {
                            f2069d = new C0853a[0];
                        }
                    }
                }
                return f2069d;
            }

            public C0853a() {
                mo1851e();
            }

            /* renamed from: e */
            public C0853a mo1851e() {
                this.f2070b = "";
                this.f2071c = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(b bVar) throws IOException {
                bVar.mo351a(1, this.f2070b);
                bVar.mo352a(2, this.f2071c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + b.m437b(1, this.f2070b) + b.m438b(2, this.f2071c);
            }

            /* renamed from: B */
            public C0853a mo738a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2070b = aVar.mo229i();
                            continue;
                        case 16:
                            this.f2071c = aVar.mo228h();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public C0852a() {
            mo1849d();
        }

        /* renamed from: d */
        public C0852a mo1849d() {
            this.f2068b = C0853a.m3238d();
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (this.f2068b != null && this.f2068b.length > 0) {
                for (C0853a aVar : this.f2068b) {
                    if (aVar != null) {
                        bVar.mo350a(1, (e) aVar);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f2068b != null && this.f2068b.length > 0) {
                for (C0853a aVar : this.f2068b) {
                    if (aVar != null) {
                        c += b.b(1, (e) aVar);
                    }
                }
            }
            return c;
        }

        /* renamed from: B */
        public C0852a mo738a(a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        int b = g.b(aVar, 10);
                        if (this.f2068b == null) {
                            length = 0;
                        } else {
                            length = this.f2068b.length;
                        }
                        C0853a[] aVarArr = new C0853a[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.f2068b, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0853a();
                            aVar.mo215a((e) aVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        aVarArr[length] = new C0853a();
                        aVar.mo215a((e) aVarArr[length]);
                        this.f2068b = aVarArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }

        /* renamed from: a */
        public static C0852a m3232a(byte[] bArr) throws d {
            return (C0852a) e.m1393a(new C0852a(), bArr);
        }
    }
}
