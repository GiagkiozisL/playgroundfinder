package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.qu */
public class qu implements yk<String> {
    /* renamed from: a */
    public yi a(@Nullable String str) {
        if (str == null) {
            return yi.a(this, "key is null");
        }
        if (str.startsWith("appmetrica")) {
            return yi.a(this, "key starts with appmetrica");
        }
        if (str.length() > 200) {
            return yi.a(this, "key length more then 200 characters");
        }
        return yi.a(this);
    }
}
