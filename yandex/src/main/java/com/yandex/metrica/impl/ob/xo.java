package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.Executor;

/* renamed from: com.yandex.metrica.impl.ob.xo */
public class xo {
    @NonNull

    /* renamed from: a */
    private final xn f2974a;
    @Nullable

    /* renamed from: B */
    private volatile xh f2975b;
    @Nullable

    /* renamed from: a */
    private volatile Executor f2976c;
    @Nullable

    /* renamed from: d */
    private volatile xh f2977d;
    @Nullable

    /* renamed from: e */
    private volatile xh f2978e;
    @Nullable

    /* renamed from: f */
    private volatile xi f2979f;
    @Nullable

    /* renamed from: g */
    private volatile xh f2980g;
    @Nullable

    /* renamed from: h */
    private volatile xh f2981h;
    @Nullable

    /* renamed from: i */
    private volatile xh f2982i;
    @Nullable

    /* renamed from: j */
    private volatile xh f2983j;

    public xo() {
        this(new xn());
    }

    @NonNull
    /* renamed from: a */
    public xh mo2625a() {
        if (this.f2975b == null) {
            synchronized (this) {
                if (this.f2975b == null) {
                    this.f2975b = this.f2974a.mo2615a();
                }
            }
        }
        return this.f2975b;
    }

    @NonNull
    /* renamed from: B */
    public Executor mo2627b() {
        if (this.f2976c == null) {
            synchronized (this) {
                if (this.f2976c == null) {
                    this.f2976c = this.f2974a.mo2617b();
                }
            }
        }
        return this.f2976c;
    }

    @NonNull
    /* renamed from: a */
    public xh mo2628c() {
        if (this.f2977d == null) {
            synchronized (this) {
                if (this.f2977d == null) {
                    this.f2977d = this.f2974a.mo2618c();
                }
            }
        }
        return this.f2977d;
    }

    @NonNull
    /* renamed from: d */
    public xh mo2629d() {
        if (this.f2978e == null) {
            synchronized (this) {
                if (this.f2978e == null) {
                    this.f2978e = this.f2974a.mo2619d();
                }
            }
        }
        return this.f2978e;
    }

    @NonNull
    /* renamed from: e */
    public xi mo2630e() {
        if (this.f2979f == null) {
            synchronized (this) {
                if (this.f2979f == null) {
                    this.f2979f = this.f2974a.mo2620e();
                }
            }
        }
        return this.f2979f;
    }

    @NonNull
    /* renamed from: f */
    public xh mo2631f() {
        if (this.f2980g == null) {
            synchronized (this) {
                if (this.f2980g == null) {
                    this.f2980g = this.f2974a.mo2621f();
                }
            }
        }
        return this.f2980g;
    }

    @NonNull
    /* renamed from: g */
    public xh mo2632g() {
        if (this.f2981h == null) {
            synchronized (this) {
                if (this.f2981h == null) {
                    this.f2981h = this.f2974a.mo2622g();
                }
            }
        }
        return this.f2981h;
    }

    @NonNull
    /* renamed from: h */
    public xh mo2633h() {
        if (this.f2982i == null) {
            synchronized (this) {
                if (this.f2982i == null) {
                    this.f2982i = this.f2974a.mo2623h();
                }
            }
        }
        return this.f2982i;
    }

    @NonNull
    /* renamed from: i */
    public xh mo2634i() {
        if (this.f2983j == null) {
            synchronized (this) {
                if (this.f2983j == null) {
                    this.f2983j = this.f2974a.mo2624i();
                }
            }
        }
        return this.f2983j;
    }

    @NonNull
    /* renamed from: a */
    public xl mo2626a(@NonNull Runnable runnable) {
        return this.f2974a.mo2616a(runnable);
    }

    @VisibleForTesting
    xo(@NonNull xn xnVar) {
        this.f2974a = xnVar;
    }
}
