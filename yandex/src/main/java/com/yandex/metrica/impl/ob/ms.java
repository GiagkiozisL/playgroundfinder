package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0873e;

/* renamed from: com.yandex.metrica.impl.ob.ms */
public class ms implements mq<oh, C0873e> {

    @NonNull
    /* renamed from: a */
    public C0873e b(@NonNull oh ohVar) {
        C0873e eVar = new C0873e();
        eVar.f2173b = ohVar.f1571e;
        eVar.f2174c = ohVar.f1572f;
        eVar.f2175d = ohVar.f1573g;
        eVar.f2176e = ohVar.f1574h;
        eVar.f2177f = ohVar.f1575i;
        eVar.f2178g = ohVar.f1576j;
        eVar.f2179h = ohVar.f1577k;
        eVar.f2181j = ohVar.f1578l;
        eVar.f2180i = ohVar.f1579m;
        return eVar;
    }

    @NonNull
    /* renamed from: a */
    public oh a(@NonNull C0873e eVar) {
        return new oh(eVar.f2173b, eVar.f2174c, eVar.f2175d, eVar.f2176e, eVar.f2177f, eVar.f2178g, eVar.f2179h, eVar.f2181j, eVar.f2180i);
    }
}
