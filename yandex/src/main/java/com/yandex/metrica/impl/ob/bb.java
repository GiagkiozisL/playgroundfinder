package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.IMetricaService;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import me.android.ydx.Constant;
import me.android.ydx.services.MService;
import me.android.ydx.ServiceConnection_;

public class bb {

    public static final long f285a = TimeUnit.SECONDS.toMillis(10);

    private final Context ctx;

    private final xh f287c;

    private boolean f288d;

    private final List<a> f289e = new CopyOnWriteArrayList();

    public volatile IMetricaService iMetricaService = null;

    private final Object f291g = new Object();

    private final Runnable f292h = new Runnable() {
        public void run() {
            bb.this.m502i();
        }
    };

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(Constant.RUS_TAG, "bb onServiceConnected");
            bb.this.iMetricaService = IMetricaService.C0009a.getMetricaServiceIF(service);
            bb.this.m503j();
        }

        public void onServiceDisconnected(ComponentName name) {
            bb.this.iMetricaService = null;
            bb.this.m504k();
        }
    };

    private final ServiceConnection_ serviceConnection_ = new ServiceConnection_() {
        @Override
        public void onServiceConnected(IBinder service) {
            Log.d(Constant.RUS_TAG, "bb onServiceConnected");
            bb.this.iMetricaService = IMetricaService.C0009a.getMetricaServiceIF(service);
            bb.this.m503j();
            // IMetricaService$getMetricaServiceIF
        }

        @Override
        public void onServiceDisconnected() {
            bb.this.iMetricaService = null;
            bb.this.m504k();
        }
    };

    interface a {
        void a();

        void b();
    }

    public Context mo376a() {
        return this.ctx;
    }

    public bb(Context context, xh xhVar) {
        this.ctx = context.getApplicationContext();
        this.f287c = xhVar;
        this.f288d = false;
    }

    public synchronized void mo379b() {
        if (this.iMetricaService == null) {
            try {
                Log.d(Constant.RUS_TAG, "bb$mo379b bind service. .");
                Intent intent = cn.m985b(this.ctx);
//                this.ctx.bindService(intent, this.serviceConnection, 1);

                MService mService = MService.getInstance();
                mService.onCreate(ctx);
                mService.setConnectionListener(serviceConnection_);
                mService.onBind(intent);

            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void mo380c() {
        mo378a(this.f287c);
    }

    @VisibleForTesting
    public void mo378a(@NonNull xh xhVar) {
        synchronized (this.f291g) {
            xhVar.b(this.f292h);
            if (!this.f288d) {
                xhVar.a(this.f292h, f285a);
            }
        }
    }

    public synchronized void mo381d() {
        this.f287c.b(this.f292h);
    }

    public boolean mo382e() {
        return this.iMetricaService != null;
    }

    public IMetricaService mo383f() {
        return this.iMetricaService;
    }

    public synchronized void m502i() {
        if (this.ctx != null && mo382e()) {
            try {
                Log.d(Constant.RUS_TAG, "bb$m502i unbind service . .");
//                this.ctx.unbindService(this.serviceConnection);
                this.iMetricaService = null;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        this.iMetricaService = null;
        m504k();
    }

    public void m503j() {
        for (bb.a a : this.f289e) {
            a.a();
        }
    }

    public void m504k() {
        for (a b : this.f289e) {
            b.b();
        }
    }

    public void mo377a(a aVar) {
        this.f289e.add(aVar);
    }

    public void mo384g() {
        synchronized (this.f291g) {
            this.f288d = true;
        }
        mo381d();
    }

    public void mo385h() {
        this.f288d = false;
        mo380c();
    }
}
