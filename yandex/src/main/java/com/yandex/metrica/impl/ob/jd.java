package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.af.C0058a;

/* renamed from: com.yandex.metrica.impl.ob.jd */
public class jd {
    @NonNull

    /* renamed from: a */
    private final en f1111a;
    @NonNull

    /* renamed from: B */
    private final jc f1112b;
    @NonNull

    /* renamed from: a */
    private final C0484a f1113c;
    @NonNull

    /* renamed from: d */
    private final iw<iy> f1114d;
    @NonNull

    /* renamed from: e */
    private final iw<iy> f1115e;
    @Nullable

    /* renamed from: f */
    private ix f1116f;
    @Nullable

    /* renamed from: g */
    private C0485b f1117g;

    /* renamed from: com.yandex.metrica.impl.ob.jd$a */
    public interface C0484a {
        /* renamed from: a */
        void mo840a(@NonNull w wVar, @NonNull je jeVar);
    }

    /* renamed from: com.yandex.metrica.impl.ob.jd$B */
    public enum C0485b {
        EMPTY,
        BACKGROUND,
        FOREGROUND
    }

    public jd(@NonNull en enVar, @NonNull jc jcVar, @NonNull C0484a aVar) {
        this(enVar, jcVar, aVar, new iv(enVar, jcVar), new iu(enVar, jcVar));
    }

    @VisibleForTesting
    public jd(@NonNull en enVar, @NonNull jc jcVar, @NonNull C0484a aVar, @NonNull iw<iy> iwVar, @NonNull iw<iy> iwVar2) {
        this.f1117g = null;
        this.f1111a = enVar;
        this.f1113c = aVar;
        this.f1114d = iwVar;
        this.f1115e = iwVar2;
        this.f1112b = jcVar;
    }

    /* renamed from: a */
    public synchronized void mo1074a(@NonNull w wVar) {
        m1907g(wVar);
        switch (this.f1117g) {
            case FOREGROUND:
                if (!m1903a(this.f1116f, wVar)) {
                    this.f1116f = m1906f(wVar);
                    break;
                } else {
                    this.f1116f.mo1039b(wVar.mo2544r());
                    break;
                }
            case BACKGROUND:
                m1905c(this.f1116f, wVar);
                this.f1116f = m1906f(wVar);
                break;
            case EMPTY:
                this.f1116f = m1906f(wVar);
                break;
        }
    }

    /* renamed from: B */
    public synchronized void mo1075b(@NonNull w wVar) {
        mo1076c(wVar).mo1035a(false);
        if (this.f1117g != C0485b.EMPTY) {
            m1905c(this.f1116f, wVar);
        }
        this.f1117g = C0485b.EMPTY;
    }

    @NonNull
    /* renamed from: a */
    public synchronized ix mo1076c(@NonNull w wVar) {
        ix ixVar;
        m1907g(wVar);
        if (this.f1117g != C0485b.EMPTY && !m1903a(this.f1116f, wVar)) {
            this.f1117g = C0485b.EMPTY;
            this.f1116f = null;
        }
        switch (this.f1117g) {
            case FOREGROUND:
                ixVar = this.f1116f;
                break;
            case BACKGROUND:
                this.f1116f.mo1039b(wVar.mo2544r());
                ixVar = this.f1116f;
                break;
            default:
                this.f1116f = m1909i(wVar);
                ixVar = this.f1116f;
                break;
        }
        return ixVar;
    }

    @NonNull
    /* renamed from: d */
    public je mo1077d(@NonNull w wVar) {
        return m1902a(mo1076c(wVar), wVar.mo2544r());
    }

    /* renamed from: a */
    public synchronized long mo1072a() {
        return this.f1116f == null ? 10000000000L : this.f1116f.mo1040c() - 1;
    }

    @NonNull
    /* renamed from: a */
    public je mo1073a(long j) {
        long a = this.f1112b.mo1071a();
        this.f1111a.mo823j().mo1200a(a, jh.BACKGROUND, j);
        return new je().mo1080a(a).mo1081a(jh.BACKGROUND).mo1082b(0).mo1085c(0);
    }

    @NonNull
    /* renamed from: f */
    private ix m1906f(@NonNull w wVar) {
        long r = wVar.mo2544r();
        ix a = this.f1114d.a(new iy(r, wVar.mo2545s()));
        this.f1117g = C0485b.FOREGROUND;
        this.f1111a.mo812C().mo1526a();
        this.f1113c.mo840a(w.m4284c(wVar), m1902a(a, r));
        return a;
    }

    /* renamed from: g */
    private void m1907g(@NonNull w wVar) {
        if (this.f1117g == null) {
            ix a = this.f1114d.a();
            if (m1903a(a, wVar)) {
                this.f1116f = a;
                this.f1117g = C0485b.FOREGROUND;
                return;
            }
            ix a2 = this.f1115e.a();
            if (m1903a(a2, wVar)) {
                this.f1116f = a2;
                this.f1117g = C0485b.BACKGROUND;
                return;
            }
            this.f1116f = null;
            this.f1117g = C0485b.EMPTY;
        }
    }

    @Nullable
    /* renamed from: h */
    private ix m1908h(@NonNull w wVar) {
        if (this.f1117g != null) {
            return this.f1116f;
        }
        ix a = this.f1114d.a();
        if (!m1904b(a, wVar)) {
            return a;
        }
        ix a2 = this.f1115e.a();
        if (m1904b(a2, wVar)) {
            return null;
        }
        return a2;
    }

    /* renamed from: a */
    private boolean m1903a(@Nullable ix ixVar, @NonNull w wVar) {
        if (ixVar == null) {
            return false;
        }
        if (ixVar.mo1036a(wVar.mo2544r())) {
            return true;
        }
        m1905c(ixVar, wVar);
        return false;
    }

    /* renamed from: B */
    private boolean m1904b(@Nullable ix ixVar, @NonNull w wVar) {
        if (ixVar == null) {
            return false;
        }
        return ixVar.mo1036a(wVar.mo2544r());
    }

    /* renamed from: a */
    private void m1905c(@NonNull ix ixVar, @Nullable w wVar) {
        if (ixVar.mo1046h()) {
            this.f1113c.mo840a(w.m4281b(wVar), m1901a(ixVar));
            ixVar.mo1035a(false);
        }
        ixVar.mo1043e();
    }

    @NonNull
    /* renamed from: i */
    private ix m1909i(@NonNull w wVar) {
        this.f1117g = C0485b.BACKGROUND;
        long r = wVar.mo2544r();
        ix a = this.f1115e.a(new iy(r, wVar.mo2545s()));
        if (this.f1111a.mo834u().mo1284d()) {
            this.f1113c.mo840a(w.m4284c(wVar), m1902a(a, wVar.mo2544r()));
        } else if (wVar.mo2533g() == C0058a.EVENT_TYPE_FIRST_ACTIVATION.mo259a()) {
            this.f1113c.mo840a(wVar, m1902a(a, r));
            this.f1113c.mo840a(w.m4284c(wVar), m1902a(a, r));
        }
        return a;
    }

    @NonNull
    /* renamed from: a */
    private je m1901a(@NonNull ix ixVar) {
        return new je().mo1080a(ixVar.mo1040c()).mo1081a(ixVar.mo1034a()).mo1082b(ixVar.mo1045g()).mo1085c(ixVar.mo1042d());
    }

    @NonNull
    /* renamed from: a */
    private je m1902a(@NonNull ix ixVar, long j) {
        return new je().mo1080a(ixVar.mo1040c()).mo1082b(ixVar.mo1045g()).mo1085c(ixVar.mo1041c(j)).mo1081a(ixVar.mo1034a());
    }

    @NonNull
    /* renamed from: e */
    public je mo1078e(@NonNull w wVar) {
        ix h = m1908h(wVar);
        if (h != null) {
            return new je().mo1080a(h.mo1040c()).mo1082b(h.mo1045g()).mo1085c(h.mo1044f()).mo1081a(h.mo1034a());
        }
        return mo1073a(wVar.mo2545s());
    }
}
