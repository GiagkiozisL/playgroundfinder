package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.b.Aa.C0867a;
import com.yandex.metrica.impl.ob.tt.a.C1041a;

/* renamed from: com.yandex.metrica.impl.ob.ni */
public class ni implements mq<C1041a, C0867a> {

    /* renamed from: a */
    @NonNull
    @Override
    public C0867a b(@NonNull C1041a aVar) {
        C0867a aVar2 = new C0867a();
        aVar2.f2143b = aVar.f2624a;
        if (aVar.f2625b != null) {
            aVar2.f2144c = aVar.f2625b;
        }
        if (aVar.f2626c != null) {
            aVar2.f2145d = aVar.f2626c;
        }
        return aVar2;
    }


    /* renamed from: a */
    @NonNull
    @Override
    public C1041a a(@NonNull C0867a aVar) {
        byte[] bArr = null;
        int i = aVar.f2143b;
        byte[] bArr2 = cx.nullOrEmpty(aVar.f2144c) ? null : aVar.f2144c;
        if (!cx.nullOrEmpty(aVar.f2145d)) {
            bArr = aVar.f2145d;
        }
        return new C1041a(i, bArr2, bArr);
    }
}
