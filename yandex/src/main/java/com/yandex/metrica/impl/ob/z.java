package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import androidx.annotation.NonNull;

@TargetApi(14)
/* renamed from: com.yandex.metrica.impl.ob.z */
public class z implements ActivityLifecycleCallbacks {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final ax f3013a;
    @NonNull

    /* renamed from: B */
    private final xh f3014b;

    public z(ax axVar, @NonNull xh xhVar) {
        this.f3013a = axVar;
        this.f3014b = xhVar;
    }

    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityResumed(final Activity activity) {
        this.f3014b.a((Runnable) new Runnable() {
            public void run() {
                z.this.f3013a.mo332b(activity);
            }
        });
    }

    public void onActivityPaused(final Activity activity) {
        this.f3014b.a((Runnable) new Runnable() {
            public void run() {
                z.this.f3013a.mo333c(activity);
            }
        });
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    public void onActivityDestroyed(Activity activity) {
    }
}
