package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rr.a.b.Aa;
import com.yandex.metrica.impl.ob.tt.a;
import com.yandex.metrica.impl.ob.tt.a.C1041a;
import com.yandex.metrica.impl.ob.tt.a.C1042b;
import com.yandex.metrica.impl.ob.tt.a.C1043c;

/* renamed from: com.yandex.metrica.impl.ob.nj */
public class nj implements mq<a, Aa> {
    @NonNull

    /* renamed from: a */
    private final ni f1515a;
    @NonNull

    /* renamed from: B */
    private final nm f1516b;
    @NonNull

    /* renamed from: a */
    private final nn f1517c;

    public nj() {
        this(new ni(), new nm(), new nn());
    }

    @NonNull
    /* renamed from: a */
    @Override
    public Aa b(@NonNull a aVar) {
        Aa aVar2 = new Aa();
        if (!TextUtils.isEmpty(aVar.f2619a)) {
            aVar2.f2138b = aVar.f2619a;
        }
        if (!TextUtils.isEmpty(aVar.f2620b)) {
            aVar2.f2139c = aVar.f2620b;
        }
        if (aVar.f2621c != null) {
            aVar2.f2140d = this.f1515a.b(aVar.f2621c);
        }
        if (aVar.f2622d != null) {
            aVar2.f2141e = this.f1516b.b(aVar.f2622d);
        }
        if (aVar.f2623e != null) {
            aVar2.f2142f = this.f1517c.b(aVar.f2623e);
        }
        return aVar2;
    }

    @NonNull
    /* renamed from: a */
    @Override
    public a a(@NonNull Aa aVar) {
        C1041a a;
        C1042b a2;
        C1043c cVar = null;
        String str = TextUtils.isEmpty(aVar.f2138b) ? null : aVar.f2138b;
        String str2 = TextUtils.isEmpty(aVar.f2139c) ? null : aVar.f2139c;
        if (aVar.f2140d == null) {
            a = null;
        } else {
            a = this.f1515a.a(aVar.f2140d);
        }
        if (aVar.f2141e == null) {
            a2 = null;
        } else {
            a2 = this.f1516b.a(aVar.f2141e);
        }
        if (aVar.f2142f != null) {
            cVar = this.f1517c.a(aVar.f2142f);
        }
        return new a(str, str2, a, a2, cVar);
    }

    @VisibleForTesting
    nj(@NonNull ni niVar, @NonNull nm nmVar, @NonNull nn nnVar) {
        this.f1515a = niVar;
        this.f1516b = nmVar;
        this.f1517c = nnVar;
    }
}
