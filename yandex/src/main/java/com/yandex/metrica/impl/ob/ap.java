package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.IParamsCallback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ap */
public class ap implements Parcelable {
    public static final Creator<ap> CREATOR = new Creator<ap>() {
        /* renamed from: a */
        public ap createFromParcel(Parcel parcel) {
            return new ap(parcel);
        }

        /* renamed from: a */
        public ap[] newArray(int i) {
            return new ap[i];
        }
    };
    @Nullable

    /* renamed from: a */
    private ResultReceiver f242a;
    @Nullable

    /* renamed from: B */
    private List<String> f243b;
    @NonNull

    /* renamed from: a */
    private Map<String, String> f244c;

    public ap(@Nullable List<String> list, @Nullable Map<String, String> map, @Nullable ResultReceiver resultReceiver) {
        this.f243b = list;
        this.f242a = resultReceiver;
        this.f244c = map == null ? new HashMap() : new HashMap(map);
    }

    /* renamed from: a */
    public boolean mo302a(@Nullable uk ukVar) {
        boolean z;
        boolean z2;
        if (cx.a((Collection) this.f243b)) {
            return true;
        }
        if (ukVar == null) {
            return false;
        }
        boolean z3 = true;
        for (String str : this.f243b) {
            if ("yandex_mobile_metrica_device_id".equals(str)) {
                if (!TextUtils.isEmpty(ukVar.deviceId)) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                z = z2 & z3;
            } else if ("yandex_mobile_metrica_uuid".equals(str)) {
                z = (!TextUtils.isEmpty(ukVar.uuid)) & z3;
            } else if ("appmetrica_device_id_hash".equals(str)) {
                z = (!TextUtils.isEmpty(ukVar.deviceIDHash)) & z3;
            } else if ("yandex_mobile_metrica_report_ad_url".equals(str)) {
                z = (!TextUtils.isEmpty(ukVar.reportAdUrl)) & z3;
            } else if ("yandex_mobile_metrica_get_ad_url".equals(str)) {
                z = (!TextUtils.isEmpty(ukVar.getAdUrl)) & z3;
            } else if (IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS.equals(str)) {
                z = this.f244c.equals(we.m4340a(ukVar.lastStartupRequestClids)) & z3;
            } else {
                z = z3;
            }
            z3 = z;
        }
        return z3;
    }

    @Nullable
    /* renamed from: a */
    public List<String> mo301a() {
        return this.f243b;
    }

    @NonNull
    /* renamed from: B */
    public Map<String, String> mo303b() {
        return this.f244c;
    }

    @Nullable
    /* renamed from: a */
    public ResultReceiver mo304c() {
        return this.f242a;
    }

    protected ap(Parcel parcel) {
        Bundle readBundle = parcel.readBundle(x.class.getClassLoader());
        if (readBundle != null) {
            this.f242a = (ResultReceiver) readBundle.getParcelable("com.yandex.metrica.CounterConfiguration.receiver");
            this.f243b = readBundle.getStringArrayList("com.yandex.metrica.CounterConfiguration.identifiersList");
            this.f244c = we.m4340a(readBundle.getString("com.yandex.metrica.CounterConfiguration.clidsForVerification"));
            return;
        }
        this.f244c = new HashMap();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.yandex.metrica.CounterConfiguration.receiver", this.f242a);
        if (this.f243b != null) {
            bundle.putStringArrayList("com.yandex.metrica.CounterConfiguration.identifiersList", new ArrayList(this.f243b));
        }
        if (this.f244c != null) {
            bundle.putString("com.yandex.metrica.CounterConfiguration.clidsForVerification", we.m4339a(this.f244c));
        }
        dest.writeBundle(bundle);
    }
}
