package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.lp */
public class lp implements lm {

    /* renamed from: a */
    private final lc f1339a;

    public lp(lc lcVar) {
        this.f1339a = lcVar;
    }

    @Nullable
    /* renamed from: a */
    public SQLiteDatabase a() {
        try {
            return this.f1339a.getWritableDatabase();
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    public void a(@Nullable SQLiteDatabase sQLiteDatabase) {
    }
}
