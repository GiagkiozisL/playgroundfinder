package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.ft */
public interface ft<C extends ew & et> {
    @NonNull
    /* renamed from: a */
    eu c(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar);

    @NonNull
    /* renamed from: d */
    C d(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar);
}
