package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.i.a;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.y */
public final class y {
    @NonNull

    /* renamed from: a */
    private Context f2998a;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public ContentValues cDataValues;

    /* renamed from: a */
    private st f3000c;

    public y(Context context) {
        this.f2998a = context;
    }

    /* renamed from: a */
    public y mo2650a(ContentValues contentValues) {
        this.cDataValues = contentValues;
        return this;
    }

    /* renamed from: a */
    public y mo2651a(@NonNull st stVar) {
        this.f3000c = stVar;
        return this;
    }

    /* renamed from: a */
    public void mo2652a() {
        storeDataInJson();
    }

    /* renamed from: a */
    private void storeDataInJson() {
        JSONObject jSONObject = new JSONObject();
        try {
            m4497a(jSONObject);
        } catch (Throwable th) {
            jSONObject = new JSONObject();
        }
        this.cDataValues.put("report_request_parameters", jSONObject.toString());
    }

    /* renamed from: a */
    private void m4497a(@NonNull JSONObject jSONObject) throws JSONException {
        jSONObject.putOpt("dId", this.f3000c.getDeviceId()).putOpt("uId", this.f3000c.getUUId()).putOpt("appVer", this.f3000c.getAppVersionName()).putOpt("appBuild", this.f3000c.getAppVersionCode()).putOpt("analyticsSdkVersionName", this.f3000c.getSDKVersionName()).putOpt("kitBuildNumber", this.f3000c.getSDKVersionNumber()).putOpt("kitBuildType", this.f3000c.getSDKBuildType()).putOpt("osVer", this.f3000c.getOsVersion()).putOpt("osApiLev", Integer.valueOf(this.f3000c.getApiLevel())).putOpt("lang", this.f3000c.A()).putOpt("root", this.f3000c.getIsRooted()).putOpt("app_debuggable", this.f3000c.mo2126E()).putOpt("app_framework", this.f3000c.getAppFramework()).putOpt("attribution_id", Integer.valueOf(this.f3000c.mo2145V()));
    }

    /* renamed from: a */
    private void m4498a(@NonNull JSONObject jSONObject, @NonNull cz czVar) throws JSONException {
        vq.m4221a(jSONObject, czVar);
    }

    /* renamed from: d */
    private void m4501d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("enabled", this.f3000c.mo2137N());
            cz b = mo2655b();
            if (b != null) {
                m4498a(jSONObject, b);
            }
            this.cDataValues.put("location_info", jSONObject.toString());
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: B */
    public cz mo2655b() {
        Location location;
        cz czVar;
        if (this.f3000c.mo2137N()) {
            location = this.f3000c.mo2138O();
            if (location == null) {
                location = oo.m2694a(this.f2998a).mo1570a();
                if (location == null) {
                    location = oo.m2694a(this.f2998a).mo1574b();
                    czVar = null;
                } else {
                    czVar = null;
                }
            } else {
                czVar = cz.m1243a(location);
            }
        } else {
            location = null;
            czVar = null;
        }
        if (czVar != null || location == null) {
            return czVar;
        }
        return cz.m1244b(location);
    }

    /* renamed from: B */
    private void m4499b(@NonNull cy cyVar) {
        this.cDataValues.put("wifi_network_info", cyVar.mo664a().toString());
    }

    /* renamed from: a */
    private void m4496a(ut utVar) {
        utVar.mo2413a((uv) new uv() {
            /* renamed from: a */
            public void a(uu[] uuVarArr) {
                y.this.cDataValues.put("cell_info", vq.m4218a(uuVarArr).toString());
            }
        });
    }

    /* renamed from: a */
    private void m4495a(a aVar) {
        this.cDataValues.put("app_environment", aVar.a);
        this.cDataValues.put("app_environment_revision", Long.valueOf(aVar.f1031b));
    }

    /* renamed from: e */
    private void m4502e() {
        vd k = al.m324a().mo293k();
        k.mo2414a((vg) new vg() {
            /* renamed from: a */
            public void a(vf vfVar) {
                uu b = vfVar.mo2472b();
                if (b != null) {
                    y.this.cDataValues.put("cellular_connection_type", b.mo2423g());
                }
            }
        });
        m4496a((ut) k);
    }

    /* renamed from: a */
    public void mo2654a(@NonNull ww wwVar, @NonNull a aVar) {
        w wVar = wwVar.f2949a;
        this.cDataValues.put("name", wVar.mo2528d());
        this.cDataValues.put("value", wVar.mo2531e());
        this.cDataValues.put("type", Integer.valueOf(wVar.mo2533g()));
        this.cDataValues.put("custom_type", Integer.valueOf(wVar.mo2534h()));
        this.cDataValues.put("error_environment", wVar.mo2536j());
        this.cDataValues.put("user_info", wVar.mo2538l());
        this.cDataValues.put("truncated", Integer.valueOf(wVar.mo2541o()));
        this.cDataValues.put("connection_type", Integer.valueOf(bt.m732e(this.f2998a)));
        this.cDataValues.put("profile_id", wVar.mo2542p());
        this.cDataValues.put("encrypting_mode", Integer.valueOf(wwVar.f2950b.mo2592a()));
        this.cDataValues.put("first_occurrence_status", Integer.valueOf(wwVar.f2949a.mo2543q().f225d));
        m4495a(aVar);
        m4501d();
        m4503f();
    }

    /* renamed from: f */
    private void m4503f() {
        m4502e();
        cy a = cy.m1204a(this.f2998a);
        m4499b(a);
        mo2653a(a);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2653a(cy cyVar) {
        String b = cyVar.mo667b(this.f2998a);
        if (!TextUtils.isEmpty(b)) {
            int c = cyVar.mo669c(this.f2998a);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ssid", b);
                jSONObject.put("state", c);
                this.cDataValues.put("wifi_access_point", jSONObject.toString());
            } catch (Throwable th) {
            }
        }
    }
}
