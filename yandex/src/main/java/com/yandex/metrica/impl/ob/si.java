package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import android.text.TextUtils;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.si */
public class si {

    /* renamed from: a */
    private static final EnumSet<C0965b> f2398a = EnumSet.of(C0965b.HAS_FROM_PLAY_SERVICES, C0965b.HAS_FROM_RECEIVER_ONLY, C0965b.RECEIVER);

    /* renamed from: B */
    private static final EnumSet<C0965b> f2399b = EnumSet.of(C0965b.HAS_FROM_PLAY_SERVICES, C0965b.HAS_FROM_RECEIVER_ONLY);

    /* renamed from: a */
    private final Set<C0964a> f2400c;
    @Nullable

    /* renamed from: d */
    private sj f2401d;
    @Nullable

    /* renamed from: e */
    private sj f2402e;

    /* renamed from: f */
    private boolean f2403f;
    @NonNull

    /* renamed from: g */
    private final ly f2404g;

    /* renamed from: h */
    private C0965b f2405h;

    /* renamed from: com.yandex.metrica.impl.ob.si$a */
    public interface C0964a {
        /* renamed from: a */
        boolean mo787a(@NonNull sj sjVar);
    }

    /* renamed from: com.yandex.metrica.impl.ob.si$B */
    private enum C0965b {
        EMPTY,
        RECEIVER,
        WAIT_FOR_RECEIVER_ONLY,
        HAS_FROM_PLAY_SERVICES,
        HAS_FROM_RECEIVER_ONLY
    }

    @WorkerThread
    public si(@NonNull Context context) {
        this(new ly(ld.m2146a(context).mo1238c()));
    }

    @VisibleForTesting
    si(@NonNull ly lyVar) {
        this.f2400c = new HashSet();
        this.f2405h = C0965b.EMPTY;
        this.f2404g = lyVar;
        this.f2403f = this.f2404g.mo1385d();
        if (!this.f2403f) {
            String b = this.f2404g.mo1376b();
            if (!TextUtils.isEmpty(b)) {
                this.f2401d = new sj(b, 0, 0);
            }
            this.f2402e = this.f2404g.mo1380c();
            this.f2405h = C0965b.values()[this.f2404g.mo1382d(0)];
        }
    }

    /* renamed from: a */
    public synchronized void mo2066a(@Nullable sj sjVar) {
        if (!f2399b.contains(this.f2405h)) {
            this.f2402e = sjVar;
            this.f2404g.mo1370a(sjVar).mo1364q();
            m3542a(m3543b(sjVar));
        }
    }

    /* renamed from: a */
    public synchronized void mo2067a(@Nullable String str) {
        if (!f2398a.contains(this.f2405h) && !TextUtils.isEmpty(str)) {
            this.f2401d = new sj(str, 0, 0);
            this.f2404g.mo1371a(str).mo1364q();
            m3542a(m3541a());
        }
    }

    /* renamed from: a */
    public synchronized void mo2065a(@NonNull C0964a aVar) {
        if (!this.f2403f) {
            this.f2400c.add(aVar);
            m3544b();
        }
    }

    /* renamed from: B */
    private C0965b m3543b(sj sjVar) {
        switch (this.f2405h) {
            case EMPTY:
                return sjVar == null ? C0965b.WAIT_FOR_RECEIVER_ONLY : C0965b.HAS_FROM_PLAY_SERVICES;
            case RECEIVER:
                return sjVar == null ? C0965b.HAS_FROM_RECEIVER_ONLY : C0965b.HAS_FROM_PLAY_SERVICES;
            default:
                return this.f2405h;
        }
    }

    /* renamed from: a */
    private C0965b m3541a() {
        switch (this.f2405h) {
            case EMPTY:
                return C0965b.RECEIVER;
            case WAIT_FOR_RECEIVER_ONLY:
                return C0965b.HAS_FROM_RECEIVER_ONLY;
            default:
                return this.f2405h;
        }
    }

    /* renamed from: a */
    private void m3542a(@NonNull C0965b bVar) {
        if (bVar != this.f2405h) {
            this.f2405h = bVar;
            this.f2404g.mo1387e(this.f2405h.ordinal()).mo1364q();
            m3544b();
        }
    }

    /* renamed from: B */
    private void m3544b() {
        switch (this.f2405h) {
            case HAS_FROM_PLAY_SERVICES:
                m3546c(this.f2402e);
                return;
            case HAS_FROM_RECEIVER_ONLY:
                m3546c(this.f2401d);
                return;
            default:
                return;
        }
    }

    /* renamed from: a */
    private synchronized void m3546c(@Nullable sj sjVar) {
        boolean z;
        if (sjVar != null) {
            if (!this.f2400c.isEmpty() && !this.f2403f) {
                boolean z2 = false;
                for (C0964a a : this.f2400c) {
                    if (a.mo787a(sjVar)) {
                        z = true;
                    } else {
                        z = z2;
                    }
                    z2 = z;
                }
                if (z2) {
                    m3545c();
                    this.f2400c.clear();
                }
            }
        }
    }

    /* renamed from: a */
    private void m3545c() {
        this.f2403f = true;
        this.f2404g.mo1386e().mo1391f().mo1364q();
    }
}
