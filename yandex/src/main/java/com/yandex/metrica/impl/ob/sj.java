package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class sj {
    @NonNull

    public final String installReferrer;
    public final long eferrerClickTimestampSeconds;
    public final long installBeginTimestampSeconds;

    @Nullable
    public static sj m3551a(@NonNull byte[] bArr) throws d {
        if (cx.nullOrEmpty(bArr)) {
            return null;
        }
        return new sj(bArr);
    }

    private sj(@NonNull byte[] bArr) throws d {
        rj a = rj.m3172a(bArr);
        this.installReferrer = a.installReferrer;
        this.eferrerClickTimestampSeconds = a.eferrerClickTimestampSeconds;
        this.installBeginTimestampSeconds = a.installBeginTimestampSeconds;
    }

    public sj(@NonNull String installReferrer, long eferrerClickTimestampSeconds, long installBeginTimestampSeconds) {
        this.installReferrer = installReferrer;
        this.eferrerClickTimestampSeconds = eferrerClickTimestampSeconds;
        this.installBeginTimestampSeconds = installBeginTimestampSeconds;
    }

    public byte[] mo2068a() {
        rj rjVar = new rj();
        rjVar.installReferrer = this.installReferrer;
        rjVar.eferrerClickTimestampSeconds = this.eferrerClickTimestampSeconds;
        rjVar.installBeginTimestampSeconds = this.installBeginTimestampSeconds;
        return e.m1395a((e) rjVar);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        sj sjVar = (sj) o;
        if (this.eferrerClickTimestampSeconds == sjVar.eferrerClickTimestampSeconds && this.installBeginTimestampSeconds == sjVar.installBeginTimestampSeconds) {
            return this.installReferrer.equals(sjVar.installReferrer);
        }
        return false;
    }

    public int hashCode() {
        return (((this.installReferrer.hashCode() * 31) + ((int) (this.eferrerClickTimestampSeconds ^ (this.eferrerClickTimestampSeconds >>> 32)))) * 31) + ((int) (this.installBeginTimestampSeconds ^ (this.installBeginTimestampSeconds >>> 32)));
    }

    public String toString() {
        return "ReferrerInfo{installReferrer='" + this.installReferrer + '\'' + ", referrerClickTimestampSeconds=" + this.eferrerClickTimestampSeconds + ", installBeginTimestampSeconds=" + this.installBeginTimestampSeconds + '}';
    }
}
