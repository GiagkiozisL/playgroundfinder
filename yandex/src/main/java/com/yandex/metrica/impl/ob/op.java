package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.op */
class op {
    @Nullable

    /* renamed from: a */
    private String f1640a;
    @Nullable

    /* renamed from: B */
    private oh f1641b;
    @NonNull

    /* renamed from: a */
    private oe f1642c;
    @Nullable

    /* renamed from: d */
    private Location f1643d;

    /* renamed from: e */
    private long f1644e;
    @NonNull

    /* renamed from: f */
    private cw f1645f;
    @NonNull

    /* renamed from: g */
    private ow f1646g;
    @NonNull

    /* renamed from: h */
    private od f1647h;

    public op(@Nullable String str, @Nullable oh ohVar, @NonNull oe oeVar, @NonNull ow owVar, @NonNull od odVar) {
        this(str, ohVar, oeVar, null, 0, new cw(), owVar, odVar);
    }

    op(@Nullable String str, @Nullable oh ohVar, @NonNull oe oeVar, @Nullable Location location, long j, @NonNull cw cwVar, @NonNull ow owVar, @NonNull od odVar) {
        this.f1640a = str;
        this.f1641b = ohVar;
        this.f1642c = oeVar;
        this.f1643d = location;
        this.f1644e = j;
        this.f1645f = cwVar;
        this.f1646g = owVar;
        this.f1647h = odVar;
    }

    /* renamed from: a */
    public void mo1580a(@Nullable Location location) {
        if (m2720d(location)) {
            m2717b(location);
            m2718c(location);
            m2716b();
            m2715a();
        }
    }

    /* renamed from: a */
    private void m2715a() {
        this.f1647h.mo1538a();
    }

    /* renamed from: B */
    private void m2716b() {
        this.f1646g.mo1613a();
    }

    /* renamed from: B */
    private void m2717b(@Nullable Location location) {
        this.f1643d = location;
        this.f1644e = System.currentTimeMillis();
    }

    /* renamed from: a */
    private void m2718c(@Nullable Location location) {
        this.f1642c.a(this.f1640a, location, this.f1641b);
    }

    /* renamed from: d */
    private boolean m2720d(@Nullable Location location) {
        if (location == null || this.f1641b == null) {
            return false;
        }
        if (this.f1643d == null) {
            return true;
        }
        boolean c = m2719c();
        boolean e = m2721e(location);
        boolean f = m2722f(location);
        if ((c || e) && f) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    private boolean m2719c() {
        return this.f1645f.mo663b(this.f1644e, this.f1641b.f1571e, "isSavedLocationOutdated");
    }

    /* renamed from: e */
    private boolean m2721e(Location location) {
        return m2723g(location) > this.f1641b.f1572f;
    }

    /* renamed from: f */
    private boolean m2722f(@NonNull Location location) {
        if (this.f1643d == null || location.getTime() - this.f1643d.getTime() >= 0) {
            return true;
        }
        return false;
    }

    /* renamed from: g */
    private float m2723g(Location location) {
        return location.distanceTo(this.f1643d);
    }

    /* renamed from: a */
    public void mo1581a(@Nullable oh ohVar) {
        this.f1641b = ohVar;
    }
}
