package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.yandex.metrica.CounterConfiguration;

/* renamed from: com.yandex.metrica.impl.ob.bz */
class bz extends ed {

    /* renamed from: a */
    protected ae f483a;

    /* renamed from: B */
    protected bu f484b;

    /* renamed from: a */
    private ak f485c = new ak();

    protected bz(@NonNull ee eeVar, @NonNull CounterConfiguration counterConfiguration) {
        super(eeVar, counterConfiguration);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo544a(xx xxVar) {
        this.f483a = new ae(xxVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public Bundle mo547b() {
        Bundle bundle = new Bundle();
        mo744h().mo21a(bundle);
        mo743g().mo752b(bundle);
        return bundle;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo543a(ug ugVar) {
        mo548b(ugVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo549c() {
        this.f485c.mo280b();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public boolean mo550d() {
        return this.f485c.mo279a();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo546a() {
        return this.f485c.mo281c();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo548b(ug ugVar) {
        if (ugVar != null) {
            mo744h().mo32c(ugVar.a());
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo545a(String str, String str2) {
        this.f483a.mo258a(str, str2);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public String mo551e() {
        return this.f483a.mo257a();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public bu mo552f() {
        return this.f484b;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo542a(bu buVar) {
        this.f484b = buVar;
    }
}
