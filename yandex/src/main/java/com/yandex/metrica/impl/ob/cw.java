package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.cw */
public class cw {
    @NonNull

    /* renamed from: a */
    private final wh f643a;

    public cw() {
        this(new wg());
    }

    @VisibleForTesting
    cw(@NonNull wh whVar) {
        this.f643a = whVar;
    }

    /* renamed from: a */
    public boolean mo662a(long j, long j2, @NonNull String str) {
        return m1169a(this.f643a.b(), j, j2, str);
    }

    /* renamed from: B */
    public boolean mo663b(long j, long j2, @NonNull String str) {
        return m1169a(this.f643a.a(), j, j2, str);
    }

    /* renamed from: a */
    private boolean m1169a(long j, long j2, long j3, @NonNull String str) {
        if (j >= j2 && j - j2 < j3) {
            return false;
        }
        return true;
    }
}
