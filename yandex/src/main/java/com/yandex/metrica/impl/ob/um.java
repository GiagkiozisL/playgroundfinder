package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.um */
public class um {

    /* renamed from: a */
    public final long f2813a;

    public um(long j) {
        this.f2813a = j;
    }

    public String toString() {
        return "StatSending{disabledReportingInterval=" + this.f2813a + '}';
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (this.f2813a != ((um) o).f2813a) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (int) (this.f2813a ^ (this.f2813a >>> 32));
    }
}
