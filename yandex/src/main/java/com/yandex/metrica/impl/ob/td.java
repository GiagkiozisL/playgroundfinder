package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.h.C0417b;
import com.yandex.metrica.impl.ob.np.C0678a;

/* renamed from: com.yandex.metrica.impl.ob.td */
public class td {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final tc f2545a;
    @NonNull

    /* renamed from: B */
    private final mf<te> f2546b;
    @NonNull

    /* renamed from: a */
    private final cw f2547c;
    @NonNull

    /* renamed from: d */
    private final xh f2548d;
    @NonNull

    /* renamed from: e */
    private final C0417b f2549e;
    @NonNull

    /* renamed from: f */
    private final h f2550f;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: g */
    public final tb f2551g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public boolean f2552h;
    @Nullable

    /* renamed from: i */
    private ua f2553i;

    /* renamed from: j */
    private boolean f2554j;

    /* renamed from: k */
    private long f2555k;

    /* renamed from: l */
    private long f2556l;

    /* renamed from: m */
    private long f2557m;

    /* renamed from: n */
    private boolean f2558n;

    /* renamed from: o */
    private boolean f2559o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public boolean f2560p;

    /* renamed from: q */
    private final Object f2561q;

    public td(@NonNull Context context, @NonNull xh xhVar) {
        this(context, new tc(context, null, xhVar), C0678a.m2599a(te.class).a(context), new cw(), xhVar, al.m324a().mo291i());
    }

    @VisibleForTesting
    td(@NonNull Context context, @NonNull tc tcVar, @NonNull mf<te> mfVar, @NonNull cw cwVar, @NonNull xh xhVar, @NonNull h hVar) {
        this.f2560p = false;
        this.f2561q = new Object();
        this.f2545a = tcVar;
        this.f2546b = mfVar;
        this.f2551g = new tb(context, mfVar, new tb.C1000a() {
            /* renamed from: a */
            public void mo2221a() {
                td.this.mo2233c();
                td.this.f2552h = false;
            }
        });
        this.f2547c = cwVar;
        this.f2548d = xhVar;
        this.f2549e = new C0417b() {
            /* renamed from: a */
            public void mo972a() {
                td.this.f2560p = true;
                td.this.f2545a.mo2223a(td.this.f2551g);
            }
        };
        this.f2550f = hVar;
    }

    /* renamed from: a */
    public void mo2230a(@Nullable uk ukVar) {
        mo2233c();
        mo2232b(ukVar);
    }

    /* renamed from: d */
    private void m3767d() {
        if (this.f2559o) {
            m3769f();
        } else {
            m3770g();
        }
    }

    /* renamed from: e */
    private void m3768e() {
        if (this.f2555k - this.f2556l >= this.f2553i.f2705b) {
            mo2231b();
        }
    }

    /* renamed from: f */
    private void m3769f() {
        if (this.f2547c.mo663b(this.f2557m, this.f2553i.f2707d, "should retry sdk collecting")) {
            mo2231b();
        }
    }

    /* renamed from: g */
    private void m3770g() {
        if (this.f2547c.mo663b(this.f2557m, this.f2553i.f2704a, "should collect sdk as usual")) {
            mo2231b();
        }
    }

    /* renamed from: a */
    public void mo2229a() {
        synchronized (this.f2561q) {
            if (this.f2554j && this.f2553i != null) {
                if (this.f2558n) {
                    m3767d();
                } else {
                    m3768e();
                }
            }
        }
    }

    /* renamed from: B */
    public void mo2232b(@Nullable uk ukVar) {
        boolean c = m3766c(ukVar);
        synchronized (this.f2561q) {
            if (ukVar != null) {
                this.f2554j = ukVar.collectingFlags.f2659e;
                this.f2553i = ukVar.sdkFingerprintingConfig;
                this.f2555k = ukVar.obtainServerTime;
                this.f2556l = ukVar.firstStartupServerTime;
            }
            this.f2545a.mo2224a(ukVar);
        }
        if (c) {
            mo2229a();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo2231b() {
        if (!this.f2552h) {
            this.f2552h = true;
            if (!this.f2560p) {
                this.f2550f.mo969a(this.f2553i.f2706c, this.f2548d, this.f2549e);
            } else {
                this.f2545a.mo2223a(this.f2551g);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2233c() {
        te teVar = (te) this.f2546b.a();
        this.f2557m = teVar.f2566c;
        this.f2558n = teVar.f2567d;
        this.f2559o = teVar.f2568e;
    }

    /* renamed from: a */
    private boolean m3766c(@Nullable uk ukVar) {
        if (ukVar == null) {
            return false;
        }
        if ((this.f2554j || !ukVar.collectingFlags.f2659e) && this.f2553i != null && this.f2553i.equals(ukVar.sdkFingerprintingConfig) && this.f2555k == ukVar.obtainServerTime && this.f2556l == ukVar.firstStartupServerTime && !this.f2545a.mo2225b(ukVar)) {
            return false;
        }
        return true;
    }
}
