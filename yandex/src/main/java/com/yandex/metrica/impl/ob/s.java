package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;

/* renamed from: com.yandex.metrica.impl.ob.s */
public class s extends bi {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final lv f2321a;

    /* renamed from: com.yandex.metrica.impl.ob.s$a */
    static class C0928a implements C0129a {

        /* renamed from: a */
        private lv f2323a;

        public C0928a(lv lvVar) {
            this.f2323a = lvVar;
        }

        /* renamed from: a */
        public void mo429a(Context context) {
            qe qeVar = new qe(context);
            if (cx.m1193a(qeVar.mo1706c())) {
                return;
            }
            if (this.f2323a.mo1289a((String) null) == null || this.f2323a.mo1291b((String) null) == null) {
                m3470a(qeVar);
                m3473b(qeVar);
                m3474c(qeVar);
                m3475d(qeVar);
                m3476e(qeVar);
                m3477f(qeVar);
                m3478g(qeVar);
                m3479h(qeVar);
                this.f2323a.mo1364q();
                qeVar.mo1703b().mo1699j();
            }
        }

        /* renamed from: a */
        private boolean m3471a(long j, long j2, long j3) {
            return j != j3 && j2 == j3;
        }

        /* renamed from: a */
        private boolean m3472a(String str, String str2) {
            return !TextUtils.isEmpty(str) && TextUtils.isEmpty(str2);
        }

        /* renamed from: a */
        private void m3470a(@NonNull qe qeVar) {
            String b = qeVar.mo1704b((String) null);
            if (m3472a(b, this.f2323a.mo1291b((String) null))) {
                this.f2323a.mo1307h(b);
            }
        }

        /* renamed from: B */
        private void m3473b(@NonNull qe qeVar) {
            String a = qeVar.mo1684a();
            if (m3472a(a, this.f2323a.mo1288a())) {
                this.f2323a.mo1312m(a);
            }
        }

        /* renamed from: a */
        private void m3474c(@NonNull qe qeVar) {
            String a = qeVar.mo1701a((String) null);
            if (m3472a(a, this.f2323a.mo1289a((String) null))) {
                this.f2323a.mo1306g(a);
            }
        }

        /* renamed from: d */
        private void m3475d(@NonNull qe qeVar) {
            String c = qeVar.mo1705c(null);
            if (m3472a(c, this.f2323a.mo1297d((String) null))) {
                this.f2323a.mo1309j(c);
            }
        }

        /* renamed from: e */
        private void m3476e(@NonNull qe qeVar) {
            String d = qeVar.mo1707d(null);
            if (m3472a(d, this.f2323a.mo1300e((String) null))) {
                this.f2323a.mo1310k(d);
            }
        }

        /* renamed from: f */
        private void m3477f(@NonNull qe qeVar) {
            String e = qeVar.mo1708e(null);
            if (m3472a(e, this.f2323a.mo1304f((String) null))) {
                this.f2323a.mo1311l(e);
            }
        }

        /* renamed from: g */
        private void m3478g(@NonNull qe qeVar) {
            long a = qeVar.mo1700a(-1);
            if (m3471a(a, this.f2323a.mo1286a(-1), -1)) {
                this.f2323a.mo1296d(a);
            }
        }

        /* renamed from: h */
        private void m3479h(@NonNull qe qeVar) {
            long b = qeVar.mo1702b(-1);
            if (m3471a(b, this.f2323a.mo1290b(-1), -1)) {
                this.f2323a.mo1299e(b);
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.s$B */
    private class C0929b implements C0129a {

        /* renamed from: B */
        private final lv f2325b;

        public C0929b(lv lvVar) {
            this.f2325b = lvVar;
        }

        /* renamed from: a */
        public void mo429a(Context context) {
            this.f2325b.mo1365r(new qk("COOKIE_BROWSERS").mo1744b());
            this.f2325b.mo1365r(new qk("BIND_ID_URL").mo1744b());
            am.m343a(context, "b_meta.dat");
            am.m343a(context, "browsers.dat");
        }
    }

    public s(lv lvVar) {
        this.f2321a = lvVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public SparseArray<C0129a> mo425a() {
        return new SparseArray<C0129a>() {
            {
                put(47, new C0928a(s.this.f2321a));
                put(66, new C0929b(s.this.f2321a));
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo424a(qg qgVar) {
        return (int) this.f2321a.mo1293c(-1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo427a(qg qgVar, int i) {
        this.f2321a.mo1303f((long) i);
        qgVar.mo1728c().mo1699j();
    }
}
