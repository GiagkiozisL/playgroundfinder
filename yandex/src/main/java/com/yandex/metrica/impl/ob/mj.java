package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a;
import com.yandex.metrica.impl.ob.rr.a.C0871c;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.mj */
public class mj extends mb<a> {
    @NonNull
    /* renamed from: a */
    public a b(@NonNull byte[] bArr) throws IOException {
        return a.m3274a(bArr);
    }

    @NonNull
    /* renamed from: a */
    public a c() {
        a aVar = new a();
        aVar.f2108j = new C0871c();
        return aVar;
    }
}
