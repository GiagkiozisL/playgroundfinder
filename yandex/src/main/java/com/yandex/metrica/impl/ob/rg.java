package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.rg */
public interface rg {

    /* renamed from: com.yandex.metrica.impl.ob.rg$a */
    final class C0810a extends e {

        /* renamed from: B */
        public C0814e f1880b;

        /* renamed from: a */
        public C0814e[] f1881c;

        public C0810a() {
            mo1775d();
        }

        /* renamed from: d */
        public C0810a mo1775d() {
            this.f1880b = null;
            this.f1881c = C0814e.m3050d();
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (this.f1880b != null) {
                bVar.mo350a(1, (e) this.f1880b);
            }
            if (this.f1881c != null && this.f1881c.length > 0) {
                for (C0814e eVar : this.f1881c) {
                    if (eVar != null) {
                        bVar.mo350a(2, (e) eVar);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f1880b != null) {
                c += b.b(1, (e) this.f1880b);
            }
            if (this.f1881c == null || this.f1881c.length <= 0) {
                return c;
            }
            int i = c;
            for (C0814e eVar : this.f1881c) {
                if (eVar != null) {
                    i += b.b(2, (e) eVar);
                }
            }
            return i;
        }

        /* renamed from: B */
        public C0810a mo738a(a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        if (this.f1880b == null) {
                            this.f1880b = new C0814e();
                        }
                        aVar.mo215a((e) this.f1880b);
                        continue;
                    case 18:
                        int b = g.b(aVar, 18);
                        if (this.f1881c == null) {
                            length = 0;
                        } else {
                            length = this.f1881c.length;
                        }
                        C0814e[] eVarArr = new C0814e[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.f1881c, 0, eVarArr, 0, length);
                        }
                        while (length < eVarArr.length - 1) {
                            eVarArr[length] = new C0814e();
                            aVar.mo215a((e) eVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        eVarArr[length] = new C0814e();
                        aVar.mo215a((e) eVarArr[length]);
                        this.f1881c = eVarArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rg$B */
    final class C0811b extends e {

        /* renamed from: B */
        public C0810a f1882b;

        /* renamed from: a */
        public String f1883c;

        /* renamed from: d */
        public int f1884d;

        public C0811b() {
            mo1777d();
        }

        /* renamed from: d */
        public C0811b mo1777d() {
            this.f1882b = null;
            this.f1883c = "";
            this.f1884d = -1;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (this.f1882b != null) {
                bVar.mo350a(1, (e) this.f1882b);
            }
            if (!this.f1883c.equals("")) {
                bVar.mo351a(2, this.f1883c);
            }
            if (this.f1884d != -1) {
                bVar.mo348a(3, this.f1884d);
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f1882b != null) {
                c += b.b(1, (e) this.f1882b);
            }
            if (!this.f1883c.equals("")) {
                c += b.m437b(2, this.f1883c);
            }
            if (this.f1884d != -1) {
                return c + b.m445d(3, this.f1884d);
            }
            return c;
        }

        /* renamed from: B */
        public C0811b mo738a(a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        if (this.f1882b == null) {
                            this.f1882b = new C0810a();
                        }
                        aVar.mo215a((e) this.f1882b);
                        continue;
                    case 18:
                        this.f1883c = aVar.mo229i();
                        continue;
                    case 24:
                        int g = aVar.mo225g();
                        switch (g) {
                            case -1:
                            case 0:
                            case 1:
                                this.f1884d = g;
                                break;
                            default:
                                continue;
                        }
                    default:
                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rg$a */
    final class C0812c extends e {

        /* renamed from: B */
        public C0815f f1885b;

        /* renamed from: a */
        public C0810a f1886c;

        /* renamed from: d */
        public String f1887d;

        /* renamed from: e */
        public int f1888e;

        public C0812c() {
            mo1779d();
        }

        /* renamed from: d */
        public C0812c mo1779d() {
            this.f1885b = null;
            this.f1886c = null;
            this.f1887d = "";
            this.f1888e = -1;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (this.f1885b != null) {
                bVar.mo350a(1, (e) this.f1885b);
            }
            if (this.f1886c != null) {
                bVar.mo350a(2, (e) this.f1886c);
            }
            if (!this.f1887d.equals("")) {
                bVar.mo351a(3, this.f1887d);
            }
            if (this.f1888e != -1) {
                bVar.mo348a(4, this.f1888e);
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f1885b != null) {
                c += b.b(1, (e) this.f1885b);
            }
            if (this.f1886c != null) {
                c += b.b(2, (e) this.f1886c);
            }
            if (!this.f1887d.equals("")) {
                c += b.m437b(3, this.f1887d);
            }
            if (this.f1888e != -1) {
                return c + b.m445d(4, this.f1888e);
            }
            return c;
        }

        /* renamed from: B */
        public C0812c mo738a(a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        if (this.f1885b == null) {
                            this.f1885b = new C0815f();
                        }
                        aVar.mo215a((e) this.f1885b);
                        continue;
                    case 18:
                        if (this.f1886c == null) {
                            this.f1886c = new C0810a();
                        }
                        aVar.mo215a((e) this.f1886c);
                        continue;
                    case 26:
                        this.f1887d = aVar.mo229i();
                        continue;
                    case 32:
                        int g = aVar.mo225g();
                        switch (g) {
                            case -1:
                            case 0:
                            case 1:
                                this.f1888e = g;
                                break;
                            default:
                                continue;
                        }
                    default:
                        if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rg$d */
    final class C0813d extends e {

        /* renamed from: g */
        private static volatile C0813d[] f1889g;

        /* renamed from: B */
        public String f1890b;

        /* renamed from: a */
        public String f1891c;

        /* renamed from: d */
        public int f1892d;

        /* renamed from: e */
        public String f1893e;

        /* renamed from: f */
        public boolean f1894f;

        /* renamed from: d */
        public static C0813d[] m3044d() {
            if (f1889g == null) {
                synchronized (c.a) {
                    if (f1889g == null) {
                        f1889g = new C0813d[0];
                    }
                }
            }
            return f1889g;
        }

        public C0813d() {
            mo1781e();
        }

        /* renamed from: e */
        public C0813d mo1781e() {
            this.f1890b = "";
            this.f1891c = "";
            this.f1892d = 0;
            this.f1893e = "";
            this.f1894f = false;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            bVar.mo351a(1, this.f1890b);
            if (!this.f1891c.equals("")) {
                bVar.mo351a(2, this.f1891c);
            }
            bVar.mo366c(3, this.f1892d);
            bVar.mo351a(4, this.f1893e);
            bVar.mo352a(5, this.f1894f);
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c() + b.m437b(1, this.f1890b);
            if (!this.f1891c.equals("")) {
                c += b.m437b(2, this.f1891c);
            }
            return c + b.m453f(3, this.f1892d) + b.m437b(4, this.f1893e) + b.m438b(5, this.f1894f);
        }

        /* renamed from: B */
        public C0813d mo738a(a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        this.f1890b = aVar.mo229i();
                        continue;
                    case 18:
                        this.f1891c = aVar.mo229i();
                        continue;
                    case 24:
                        this.f1892d = aVar.mo232l();
                        continue;
                    case 34:
                        this.f1893e = aVar.mo229i();
                        continue;
                    case 40:
                        this.f1894f = aVar.mo228h();
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rg$e */
    final class C0814e extends e {

        /* renamed from: h */
        private static volatile C0814e[] f1895h;

        /* renamed from: B */
        public String f1896b;

        /* renamed from: a */
        public int f1897c;

        /* renamed from: d */
        public long f1898d;

        /* renamed from: e */
        public String f1899e;

        /* renamed from: f */
        public int f1900f;

        /* renamed from: g */
        public C0813d[] f1901g;

        /* renamed from: d */
        public static C0814e[] m3050d() {
            if (f1895h == null) {
                synchronized (c.a) {
                    if (f1895h == null) {
                        f1895h = new C0814e[0];
                    }
                }
            }
            return f1895h;
        }

        public C0814e() {
            mo1783e();
        }

        /* renamed from: e */
        public C0814e mo1783e() {
            this.f1896b = "";
            this.f1897c = 0;
            this.f1898d = 0;
            this.f1899e = "";
            this.f1900f = 0;
            this.f1901g = C0813d.m3044d();
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            bVar.mo351a(1, this.f1896b);
            bVar.mo366c(2, this.f1897c);
            bVar.mo367c(3, this.f1898d);
            if (!this.f1899e.equals("")) {
                bVar.mo351a(4, this.f1899e);
            }
            if (this.f1900f != 0) {
                bVar.mo361b(5, this.f1900f);
            }
            if (this.f1901g != null && this.f1901g.length > 0) {
                for (C0813d dVar : this.f1901g) {
                    if (dVar != null) {
                        bVar.mo350a(6, (e) dVar);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c() + b.m437b(1, this.f1896b) + b.m453f(2, this.f1897c) + b.m454f(3, this.f1898d);
            if (!this.f1899e.equals("")) {
                c += b.m437b(4, this.f1899e);
            }
            if (this.f1900f != 0) {
                c += b.m449e(5, this.f1900f);
            }
            if (this.f1901g == null || this.f1901g.length <= 0) {
                return c;
            }
            int i = c;
            for (C0813d dVar : this.f1901g) {
                if (dVar != null) {
                    i += b.b(6, (e) dVar);
                }
            }
            return i;
        }

        /* renamed from: B */
        public C0814e mo738a(a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        this.f1896b = aVar.mo229i();
                        continue;
                    case 16:
                        this.f1897c = aVar.mo232l();
                        continue;
                    case 24:
                        this.f1898d = aVar.mo233m();
                        continue;
                    case 34:
                        this.f1899e = aVar.mo229i();
                        continue;
                    case 40:
                        this.f1900f = aVar.mo231k();
                        continue;
                    case 50:
                        int b = g.b(aVar, 50);
                        if (this.f1901g == null) {
                            length = 0;
                        } else {
                            length = this.f1901g.length;
                        }
                        C0813d[] dVarArr = new C0813d[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.f1901g, 0, dVarArr, 0, length);
                        }
                        while (length < dVarArr.length - 1) {
                            dVarArr[length] = new C0813d();
                            aVar.mo215a((e) dVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        dVarArr[length] = new C0813d();
                        aVar.mo215a((e) dVarArr[length]);
                        this.f1901g = dVarArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.rg$f */
    final class C0815f extends e {

        /* renamed from: g */
        private static volatile C0815f[] f1902g;

        /* renamed from: B */
        public String f1903b;

        /* renamed from: a */
        public String f1904c;

        /* renamed from: d */
        public C0813d[] f1905d;

        /* renamed from: e */
        public C0815f f1906e;

        /* renamed from: f */
        public C0815f[] f1907f;

        /* renamed from: d */
        public static C0815f[] m3056d() {
            if (f1902g == null) {
                synchronized (c.a) {
                    if (f1902g == null) {
                        f1902g = new C0815f[0];
                    }
                }
            }
            return f1902g;
        }

        public C0815f() {
            mo1785e();
        }

        /* renamed from: e */
        public C0815f mo1785e() {
            this.f1903b = "";
            this.f1904c = "";
            this.f1905d = C0813d.m3044d();
            this.f1906e = null;
            this.f1907f = m3056d();
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            bVar.mo351a(1, this.f1903b);
            if (!this.f1904c.equals("")) {
                bVar.mo351a(2, this.f1904c);
            }
            if (this.f1905d != null && this.f1905d.length > 0) {
                for (C0813d dVar : this.f1905d) {
                    if (dVar != null) {
                        bVar.mo350a(3, (e) dVar);
                    }
                }
            }
            if (this.f1906e != null) {
                bVar.mo350a(4, (e) this.f1906e);
            }
            if (this.f1907f != null && this.f1907f.length > 0) {
                for (C0815f fVar : this.f1907f) {
                    if (fVar != null) {
                        bVar.mo350a(5, (e) fVar);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c() + b.m437b(1, this.f1903b);
            if (!this.f1904c.equals("")) {
                c += b.m437b(2, this.f1904c);
            }
            if (this.f1905d != null && this.f1905d.length > 0) {
                int i = c;
                for (C0813d dVar : this.f1905d) {
                    if (dVar != null) {
                        i += b.b(3, (e) dVar);
                    }
                }
                c = i;
            }
            if (this.f1906e != null) {
                c += b.b(4, (e) this.f1906e);
            }
            if (this.f1907f != null && this.f1907f.length > 0) {
                for (C0815f fVar : this.f1907f) {
                    if (fVar != null) {
                        c += b.b(5, (e) fVar);
                    }
                }
            }
            return c;
        }

        /* renamed from: B */
        public C0815f mo738a(a aVar) throws IOException {
            int length;
            int length2;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        this.f1903b = aVar.mo229i();
                        continue;
                    case 18:
                        this.f1904c = aVar.mo229i();
                        continue;
                    case 26:
                        int b = g.b(aVar, 26);
                        if (this.f1905d == null) {
                            length2 = 0;
                        } else {
                            length2 = this.f1905d.length;
                        }
                        C0813d[] dVarArr = new C0813d[(b + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.f1905d, 0, dVarArr, 0, length2);
                        }
                        while (length2 < dVarArr.length - 1) {
                            dVarArr[length2] = new C0813d();
                            aVar.mo215a((e) dVarArr[length2]);
                            aVar.mo213a();
                            length2++;
                        }
                        dVarArr[length2] = new C0813d();
                        aVar.mo215a((e) dVarArr[length2]);
                        this.f1905d = dVarArr;
                        continue;
                    case 34:
                        if (this.f1906e == null) {
                            this.f1906e = new C0815f();
                        }
                        aVar.mo215a((e) this.f1906e);
                        continue;
                    case 42:
                        int b2 = g.b(aVar, 42);
                        if (this.f1907f == null) {
                            length = 0;
                        } else {
                            length = this.f1907f.length;
                        }
                        C0815f[] fVarArr = new C0815f[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.f1907f, 0, fVarArr, 0, length);
                        }
                        while (length < fVarArr.length - 1) {
                            fVarArr[length] = new C0815f();
                            aVar.mo215a((e) fVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        fVarArr[length] = new C0815f();
                        aVar.mo215a((e) fVarArr[length]);
                        this.f1907f = fVarArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }
}
