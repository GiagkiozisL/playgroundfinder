package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.py */
public class py implements pp {
    @NonNull

    /* renamed from: a */
    private Set<String> f1748a;

    public py(@Nullable List<pu> list) {
        if (list == null) {
            this.f1748a = new HashSet();
            return;
        }
        this.f1748a = new HashSet(list.size());
        for (pu puVar : list) {
            if (puVar.granted) {
                this.f1748a.add(puVar.name);
            }
        }
    }

    /* renamed from: a */
    public boolean a(@NonNull String str) {
        return this.f1748a.contains(str);
    }

    public String toString() {
        return "StartupBasedPermissionStrategy{mEnabledPermissions=" + this.f1748a + '}';
    }
}
