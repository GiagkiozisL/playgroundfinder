package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.aq */
public class aq {
    @NonNull

    /* renamed from: a */
    private final wh f245a;
    @NonNull

    /* renamed from: B */
    private final cw f246b;
    @NonNull

    /* renamed from: a */
    private final ly f247c;
    @NonNull

    /* renamed from: d */
    private final Context f248d;

    /* renamed from: e */
    private long f249e;
    @Nullable

    /* renamed from: f */
    private ty f250f;

    public aq(@NonNull ly lyVar, @NonNull Context context, @Nullable ty tyVar) {
        this(lyVar, context, tyVar, new wg(), new cw());
    }

    @VisibleForTesting
    aq(@NonNull ly lyVar, @NonNull Context context, @Nullable ty tyVar, @NonNull wh whVar, @NonNull cw cwVar) {
        this.f247c = lyVar;
        this.f248d = context;
        this.f250f = tyVar;
        this.f249e = this.f247c.mo1394h(0);
        this.f245a = whVar;
        this.f246b = cwVar;
    }

    /* renamed from: a */
    public void mo311a() {
        if (this.f250f != null && this.f246b.mo662a(this.f249e, this.f250f.f2701a, "should send EVENT_IDENTITY_LIGHT")) {
            m374b();
            this.f249e = this.f245a.b();
            this.f247c.mo1396i(this.f249e);
        }
    }

    /* renamed from: B */
    private void m374b() {
        tl.a(this.f248d).e();
    }

    /* renamed from: a */
    public void mo312a(@Nullable ty tyVar) {
        this.f250f = tyVar;
    }
}
