package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.rv */
public class rv extends rw implements an {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: d */
    public final an f2249d = ((an) mo1939a());

    public rv(@NonNull xh xhVar, @NonNull Context context, @NonNull String str) {
        super(xhVar, context, str);
    }

    /* renamed from: a */
    public void a(@NonNull final kl klVar) {
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rv.this.f2249d.a(klVar);
            }
        });
    }

    /* renamed from: a */
    public void a(@NonNull final kg kgVar) {
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rv.this.f2249d.a(kgVar);
            }
        });
    }

    /* renamed from: e */
    public void e() {
        this.f2262b.a((Runnable) new Runnable() {
            public void run() {
                rv.this.f2249d.e();
            }
        });
    }
}
