package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.Pair;

import com.yandex.metrica.impl.ob.cq.a;
import com.yandex.metrica.impl.ob.cq.a.a_enum;
import com.yandex.metrica.impl.ob.rr.a.C0876h;
import com.yandex.metrica.impl.ob.rr.a.C0876h.C0877a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.nb */
public class nb implements mq<a, C0876h> {

    /* renamed from: a */
    private static final Map<Integer, a_enum> f1500a = Collections.unmodifiableMap(new HashMap<Integer, a_enum>() {
        {
            put(Integer.valueOf(1), a_enum.WIFI);
            put(Integer.valueOf(2), a_enum.CELL);
        }
    });

    /* renamed from: B */
    private static final Map<a_enum, Integer> f1501b = Collections.unmodifiableMap(new HashMap<a_enum, Integer>() {
        {
            put(a_enum.WIFI, Integer.valueOf(1));
            put(a_enum.CELL, Integer.valueOf(2));
        }
    });

    @NonNull
    /* renamed from: a */
    public C0876h b(@NonNull a aVar) {
        C0876h hVar = new C0876h();
        hVar.f2188b = aVar.a;
        hVar.f2189c = aVar.b;
        hVar.f2190d = aVar.c;
        hVar.f2191e = m2535a(aVar.d);
        hVar.f2192f = aVar.e == null ? 0 : aVar.e.longValue();
        hVar.f2193g = m2536b(aVar.f);
        return hVar;
    }

    @NonNull
    /* renamed from: a */
    public a a(@NonNull C0876h hVar) {
        return new a(hVar.f2188b, hVar.f2189c, hVar.f2190d, m2534a(hVar.f2191e), Long.valueOf(hVar.f2192f), m2533a(hVar.f2193g));
    }

    @NonNull
    /* renamed from: a */
    private C0877a[] m2535a(@NonNull List<Pair<String, String>> list) {
        C0877a[] aVarArr = new C0877a[list.size()];
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return aVarArr;
            }
            Pair pair = (Pair) it.next();
            C0877a aVar = new C0877a();
            aVar.f2195b = (String) pair.first;
            aVar.f2196c = (String) pair.second;
            aVarArr[i2] = aVar;
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    private List<Pair<String, String>> m2534a(@NonNull C0877a[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (C0877a aVar : aVarArr) {
            arrayList.add(new Pair(aVar.f2195b, aVar.f2196c));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    private List<a_enum> m2533a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(f1500a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: B */
    private int[] m2536b(@NonNull List<a_enum> list) {
        int[] iArr = new int[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return iArr;
            }
            iArr[i2] = ((Integer) f1501b.get(list.get(i2))).intValue();
            i = i2 + 1;
        }
    }
}
