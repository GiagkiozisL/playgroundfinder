package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.rl.a.Aa;

/* renamed from: com.yandex.metrica.impl.ob.qr */
public abstract class qr<T> extends qw {
    @NonNull

    /* renamed from: a */
    private final T f1855a;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo1750a(@NonNull Aa aVar);

    qr(int i, @NonNull String str, @NonNull T t, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(i, str, ykVar, qoVar);
        this.f1855a = t;
    }

    @NonNull
    /* renamed from: B */
    public T mo1752b() {
        return this.f1855a;
    }

    /* renamed from: a */
    public void a(@NonNull re reVar) {
        if (mo1761f()) {
            Aa b = m2979b(reVar);
            if (b != null) {
                mo1750a(b);
            }
        }
    }

    @Nullable
    /* renamed from: B */
    private Aa m2979b(@NonNull re reVar) {
        return mo1760e().mo1748a(reVar, reVar.mo1770a(mo1759d(), mo1758c()), this);
    }
}
