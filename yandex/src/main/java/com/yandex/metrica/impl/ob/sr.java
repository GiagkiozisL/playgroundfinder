package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.vq.a;

/* renamed from: com.yandex.metrica.impl.ob.sr */
public class sr {
    @Nullable

    /* renamed from: a */
    public final String f2452a;
    @Nullable

    /* renamed from: B */
    public final String f2453b;
    @Nullable
    @Deprecated

    /* renamed from: a */
    public final String f2454c;
    @Nullable

    /* renamed from: d */
    public final String f2455d;
    @Nullable

    /* renamed from: e */
    public final String f2456e;
    @Nullable

    /* renamed from: f */
    public final String f2457f;
    @Nullable

    /* renamed from: g */
    public final String f2458g;
    @Nullable

    /* renamed from: h */
    public final String f2459h;
    @Nullable

    /* renamed from: i */
    public final String f2460i;
    @Nullable

    /* renamed from: j */
    public final String f2461j;
    @Nullable

    /* renamed from: k */
    public final String f2462k;
    @Nullable

    /* renamed from: l */
    public final String f2463l;
    @Nullable

    /* renamed from: m */
    public final String f2464m;
    @Nullable

    /* renamed from: n */
    public final String f2465n;
    @Nullable

    /* renamed from: o */
    public final String f2466o;

    public sr(@NonNull a aVar) {
        String str = null;
        this.f2452a = aVar.a_("dId");
        this.f2453b = aVar.a_("uId");
        this.f2454c = aVar.mo2498b("kitVer");
        this.f2455d = aVar.a_("analyticsSdkVersionName");
        this.f2456e = aVar.a_("kitBuildNumber");
        this.f2457f = aVar.a_("kitBuildType");
        this.f2458g = aVar.a_("appVer");
        this.f2459h = aVar.optString("app_debuggable", "0");
        this.f2460i = aVar.a_("appBuild");
        this.f2461j = aVar.a_("osVer");
        this.f2463l = aVar.a_("lang");
        this.f2464m = aVar.a_("root");
        this.f2465n = aVar.optString("app_framework", ci.m945b());
        int optInt = aVar.optInt("osApiLev", -1);
        this.f2462k = optInt == -1 ? null : String.valueOf(optInt);
        int optInt2 = aVar.optInt("attribution_id", 0);
        if (optInt2 > 0) {
            str = String.valueOf(optInt2);
        }
        this.f2466o = str;
    }

    public sr() {
        this.f2452a = null;
        this.f2453b = null;
        this.f2454c = null;
        this.f2455d = null;
        this.f2456e = null;
        this.f2457f = null;
        this.f2458g = null;
        this.f2459h = null;
        this.f2460i = null;
        this.f2461j = null;
        this.f2462k = null;
        this.f2463l = null;
        this.f2464m = null;
        this.f2465n = null;
        this.f2466o = null;
    }
}
