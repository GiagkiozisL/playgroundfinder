package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.impl.ob.ai.C0065c;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import me.android.ydx.Constant;
import me.android.ydx.services.MService;

public class ts implements tr, Runnable {

    public final ServiceConnection f2590a;

    private final Handler f2591b;

    private HashMap<String, tp> f2592c;

    public final Context ctx;

    private boolean f2594e;

    private ServerSocket f2595f;

    public final tn f2596g;

    public ub f2597h;

    private xl f2598i;

    private long f2599j;

    private long f2600k;
    @NonNull

    private final wh f2601l;
    @NonNull

    private final wf f2602m;
    @NonNull

    private final C0065c f2603n;

    public static class C1038a {
        public ts mo2280a(@NonNull Context context) {
            return new ts(context);
        }
    }

    public ts(Context context) {
        this(context, al.m324a().mo289g(), al.m324a().mo292j().mo2634i(), new wg(), new wf());
    }

    @VisibleForTesting
    ts(@NonNull Context context, @NonNull ai aiVar, @NonNull xh xhVar, @NonNull wh whVar, @NonNull wf wfVar) {
        this.f2590a = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(Constant.RUS_TAG, "ts onServiceConnected");
            }

            public void onServiceDisconnected(ComponentName name) {
            }
        };
        this.f2591b = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT_LOWER_BOUND /*100*/:
                        ts.this.mo2267e();
                        try {
                            Log.d(Constant.RUS_TAG, "ts unbind service . .");
                            ts.this.ctx.unbindService(ts.this.f2590a);
                            return;
                        } catch (Throwable th) {
                            tl.a(ts.this.ctx).reportEvent("socket_unbind_has_thrown_exception");
                            return;
                        }
                    default:
                        return;
                }
            }
        };
        this.f2592c = new HashMap<String, tp>() {
            {
                put("p", new tp() {
                    @NonNull
                    public to a(@NonNull Socket socket, @NonNull Uri uri) {
                        return new tm(socket, uri, ts.this, ts.this.f2597h, ts.this.f2596g);
                    }
                });
            }
        };
        this.f2596g = new tn();
        this.ctx = context;
        this.f2601l = whVar;
        this.f2602m = wfVar;
        this.f2603n = aiVar.mo269a(new Runnable() {
            public void run() {
                ts.this.m3823h();
            }
        }, xhVar);
        m3822g();
    }

    private void m3822g() {
        dr.a().a(this, ec.class, dv.m1388a((du<ec>) new du<ec>() {
            public void a(ec ecVar) {
                ts.this.f2596g.mo2249a(ecVar.f758a);
            }
        }).mo735a(new ds<ec>() {
            public boolean a(ec ecVar) {
                Log.d(Constant.RUS_TAG , "TODO -> m3822g:"  + !ts.this.ctx.getPackageName().equals(ecVar.pkgName));
                return false; //todo check?
//                return !ts.this.ctx.getPackageName().equals(ecVar.pkgName);
            }
        }).mo736a());
        dr.a().a(this, dy.class, dv.m1388a((du<dy>) new du<dy>() {
            public void a(dy dyVar) {
                ts.this.f2596g.mo2250b(dyVar.f752a);
            }
        }).mo736a());
        dr.a().a(this, dw.class, dv.m1388a((du<dw>) new du<dw>() {
            public void a(dw dwVar) {
                ts.this.f2596g.mo2251c(dwVar.f750a);
            }
        }).mo736a());
        dr.a().a(this, dx.class, dv.m1388a((du<dx>) new du<dx>() {
            public void a(dx dxVar) {
                ts.this.f2596g.mo2252d(dxVar.f751a);
            }
        }).mo736a());
        dr.a().a(this, ea.class, dv.m1388a((du<ea>) new du<ea>() {
            public void a(ea eaVar) {
                ts.this.mo2261a(eaVar.f755a);
                ts.this.mo2265c();
            }
        }).mo736a());
    }

    public void mo2260a() {
        if (this.f2594e) {
            mo2264b();
            this.f2591b.sendMessageDelayed(this.f2591b.obtainMessage(100), TimeUnit.SECONDS.toMillis(this.f2597h.f2708a));
            this.f2600k = this.f2601l.a();
        }
    }

    public void mo2264b() {
        this.f2591b.removeMessages(100);
        this.f2600k = 0;
    }

    public synchronized void mo2265c() {
        if (!(this.f2594e || this.f2597h == null || !this.f2603n.mo278a(this.f2597h.f2712e))) {
            this.f2594e = true;
        }
    }

    public void m3823h() {
        mo2266d();
        this.f2598i = al.m324a().mo292j().mo2626a(this);
        this.f2598i.start();
        this.f2599j = this.f2601l.a();
    }

    @VisibleForTesting
    public void mo2261a(ub ubVar) {
        this.f2597h = ubVar;
        if (this.f2597h != null) {
            this.f2603n.mo276a(this.f2597h.f2711d);
        }
    }

    @VisibleForTesting
    @SuppressLint("WrongConstant")
    public void mo2266d() {
        Log.d(Constant.RUS_TAG, "ts$mo2266d creating intent with action ACTION_BIND_TO_LOCAL_SERVER");
        Intent intent = new Intent();//this.ctx, MetricaService.class);
        intent.setAction("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER");
        try {
            Log.d(Constant.RUS_TAG, "ts$mo2266d bind service. . .");
            MService.getInstance().onBind(intent);
//            if (!this.ctx.bindService(intent, this.f2590a, 1)) {
//                tl.a(this.ctx).reportEvent("socket_bind_has_failed");
//            }
        } catch (Throwable th) {
//            th.printStackTrace();
//            tl.a(this.ctx).reportEvent("socket_bind_has_thrown_exception");
        }
    }

    public synchronized void mo2267e() {
        try {
            this.f2594e = false;
            if (this.f2598i != null) {
                this.f2598i.mo2611b();
                this.f2598i = null;
            }
            if (this.f2595f != null) {
                this.f2595f.close();
                this.f2595f = null;
            }
        } catch (IOException e) {
        }
    }

    public void run() {
        ServerSocket serverSocket;
        this.f2595f = mo2268f();
        if (cx.a(26)) {
            TrafficStats.setThreadStatsTag(40230);
        }
        if (this.f2595f != null) {
            while (this.f2594e) {
                synchronized (this) {
                    serverSocket = this.f2595f;
                }
                if (serverSocket != null) {
                    boolean z = false;
                    try {
                        Socket accept = serverSocket.accept();
                        z = cx.a(26);
                        if (z) {
                            TrafficStats.tagSocket(accept);
                        }
                        m3814a(accept);
                        if (accept != null) {
                            try {
                                accept.close();
                            } catch (IOException e) {
                            }
                        }
                    } catch (Throwable th) {
//                        if ( != null) {
//                            try {
//                                z.close();
//                            } catch (IOException e2) {
//                            }
//                        }
                    } finally {
//                        if (z != null) {
//                            try {
//                                z.close();
//                            } catch (IOException e3) {
//                            }
//                        }
                    }
                }
            }
        }
    }

    @VisibleForTesting
    public ServerSocket mo2268f() {
        Integer num;
        Iterator it = this.f2597h.f2710c.iterator();
        Integer num2 = null;
        ServerSocket serverSocket = null;
        while (serverSocket == null && it.hasNext()) {
            try {
                num = (Integer) it.next();
                if (num != null) {
                    try {
                        serverSocket = mo2263b(num.intValue());
                    } catch (SocketException e) {
                    } catch (IOException e2) {
                        num2 = num;
                    }
                }
                num2 = num;
            } catch (Exception e3) {
                num = num2;
                mo2262a("port_already_in_use", num.intValue());
                num2 = num;
//            } catch (IOException e4) {
//                num = num2;
//                num2 = num;
            }
        }
        return serverSocket;
    }

    public ServerSocket mo2263b(int i) throws IOException {
        return new ServerSocket(i);
    }

    private Map<String, Object> m3818c(int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("port", String.valueOf(i));
        return hashMap;
    }

    private Map<String, Object> m3820d(int i) {
        Map<String, Object> c = m3818c(i);
        c.put("idle_interval", Double.valueOf(m3812a(this.f2599j)));
        c.put("background_interval", Double.valueOf(m3812a(this.f2600k)));
        return c;
    }

    private double m3812a(long j) {
        long j2 = 0;
        if (j != 0) {
            j2 = this.f2602m.mo2564e(j, TimeUnit.MILLISECONDS);
        }
        return (double) j2;
    }

    private void m3814a(@NonNull Socket socket) {
        new tq(socket, this, this.f2592c).a();
    }

    public void a(@NonNull String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("uri", str2);
        tl.a(this.ctx).reportEvent("socket_" + str, (Map<String, Object>) hashMap);
    }

    public void a(@NonNull String str) {
        tl.a(this.ctx).reportEvent(m3816b(str));
    }

    public void a(@NonNull String str, Throwable th) {
        tl.a(this.ctx).reportError(m3816b(str), th);
    }

    public void mo2262a(@NonNull String str, int i) {
        tl.a(this.ctx).reportEvent(m3816b(str), m3818c(i));
    }

    public void a(int i) {
        tl.a(this.ctx).reportEvent(m3816b("sync_succeed"), m3820d(i));
    }

    private String m3816b(@NonNull String str) {
        return "socket_" + str;
    }
}
