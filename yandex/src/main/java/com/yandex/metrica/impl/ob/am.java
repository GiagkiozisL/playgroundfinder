package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.Base64;
import android.util.Log;

import com.yandex.metrica.impl.ob.rs.a.b;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import me.android.ydx.Constant;
import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.am */
public final class am {
    /* renamed from: a */
    public static String a(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        StringWriter stringWriter = new StringWriter();
        m339a((Reader) inputStreamReader, (Writer) stringWriter);
        return stringWriter.toString();
    }

    /* renamed from: a */
    public static String readFile(String fileName) throws IOException {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(fileName);
            try {
                String a = a(fileInputStream);
                cx.a(fileInputStream);
                return a;
            } catch (Throwable th) {
                cx.a(fileInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            fileInputStream = null;
            cx.a(fileInputStream);
        }
        return "";
    }

    /* renamed from: a */
    public static int m339a(Reader reader, Writer writer) throws IOException {
        char[] cArr = new char[4096];
        int i = 0;
        while (true) {
            int read = reader.read(cArr, 0, 4096);
            if (-1 == read) {
                return i;
            }
            writer.write(cArr, 0, read);
            i += read;
        }
    }

    /* renamed from: B */
    public static String compressNencodeB64(String seed) {
        boolean z = false;
        try {
            byte[] compressedSeed = compressSeed(seed.getBytes("UTF-8"));
            return Base64.encodeToString(compressedSeed, 0);
        } catch (Throwable th) {
            return "";
        }
    }

    /* renamed from: a */
    public static byte[] compressSeed(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPOutputStream gZIPOutputStream;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                try {
                    gZIPOutputStream.write(bArr);
                    gZIPOutputStream.finish();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    cx.a(gZIPOutputStream);
                    cx.a(byteArrayOutputStream);
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    cx.a(gZIPOutputStream);
                    cx.a(byteArrayOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
//                th = th2;
                gZIPOutputStream = null;
                cx.a(gZIPOutputStream);
                cx.a(byteArrayOutputStream);
//                throw th;
            }
        } catch (Throwable th3) {
//            th = th3;
            gZIPOutputStream = null;
            byteArrayOutputStream = null;
            cx.a(gZIPOutputStream);
            cx.a(byteArrayOutputStream);
//            throw th;
        }
        return null;
    }

    /* renamed from: B */
    public static byte[] compressSeed_(byte[] bArr) throws IOException {
        ByteArrayInputStream byteArrayInputStream;
        GZIPInputStream gZIPInputStream;
        try {
            byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
                try {
                    byte[] b = b(gZIPInputStream);
                    cx.a(gZIPInputStream);
                    cx.a(byteArrayInputStream);
                    return b;
                } catch (Throwable th) {
                    cx.a(gZIPInputStream);
                    cx.a(byteArrayInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                gZIPInputStream = null;
                cx.a(gZIPInputStream);
                cx.a(byteArrayInputStream);
            }
        } catch (Throwable th3) {
            gZIPInputStream = null;
            byteArrayInputStream = null;
            cx.a(gZIPInputStream);
            cx.a(byteArrayInputStream);
        }
        return null;
    }

    public static String m358c(String var0) {
        ByteArrayInputStream var1 = null;
        GZIPInputStream var2 = null;

        String var4;
        try {
            byte[] var3 = Base64.decode(var0, 0);
            var1 = new ByteArrayInputStream(var3);
            var2 = new GZIPInputStream(var1);
            var4 = a((InputStream)var2);
            return var4;
        } catch (Throwable var8) {
            var4 = null;
        } finally {
            cx.a(var2);
            cx.a(var1);
        }

        return var4;
    }

    /* renamed from: B */
    public static byte[] b(@Nullable InputStream inputStream) throws IOException {
        return a(inputStream, Integer.MAX_VALUE);
    }

    /* renamed from: a */
    public static byte[] a(@Nullable InputStream var0, int var1) throws IOException {
        if (null == var0) {
            return null;
        } else {
            byte[] var2 = new byte[8192];
            ByteArrayOutputStream var3 = new ByteArrayOutputStream();

            try {
                int var4 = 0;

                while(true) {
                    int var5 = var0.read(var2);
                    if (-1 == var5 || var4 > var1) {
                        byte[] var6 = var3.toByteArray();
                        return var6;
                    }

                    if (var5 > 0) {
                        var3.write(var2, 0, var5);
                        var4 += var5;
                    }
                }
            } finally {
                cx.a(var3);
            }
        }
    }

    @Nullable
    /* renamed from: a */
    public static String m340a(Context context, File file) {
        byte[] b = readAFile(context, file);

        try {
            String content = new String(b, "UTF-8");
            Log.d(Constant.RUS_TAG, "content of file is: " + content);
            return content;
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException unsupportedEncodingException = e;
            String str = new String(b);
            tl.a(context).reportError("read_share_file_with_unsupported_encoding", unsupportedEncodingException);
            return str;
        }
    }

    @Nullable
    public static byte[] readAFile(Context var0, File var1) {
        RandomAccessFile var2 = null;
        FileLock var5 = null;

        try {
            var2 = new RandomAccessFile(var1, "r");
            FileChannel var3 = var2.getChannel();
            var5 = var3.lock(0L, 9223372036854775807L, true);
            ByteBuffer var4 = ByteBuffer.allocate((int)var1.length());
            var3.read(var4);
            var4.flip();
            byte[] var6 = var4.array();
            return var6;
        } catch (IOException var12) {
        } catch (SecurityException var13) {
        } catch (Throwable var14) {
            tl.a(var0).reportError("error_during_file_reading", var14);
        } finally {
            a(var1.getAbsolutePath(), var5);
            cx.a(var2);
        }

        return null;
    }

    /* renamed from: a */
    public static void a(String str, FileLock fileLock) {
        if (fileLock != null && fileLock.isValid()) {
            try {
                fileLock.release();
            } catch (IOException e) {
            }
        }
    }

    @SuppressLint({"WorldReadableFiles", "WrongConstant"})
    /* renamed from: a */
    public static void a(Context context, String str, String str2) {
        try {
            if (m354b()) {
                m345a(str2, str, context.openFileOutput(str, 0));
                m359c(context, context.getFileStreamPath(str));
                return;
            }
            m345a(str2, str, context.openFileOutput(str, 1));
        } catch (FileNotFoundException e) {
        }
    }

    @SuppressLint({"WorldReadableFiles"})
    /* renamed from: a */
    public static void m343a(@NonNull Context context, @NonNull String str) {
        if (m354b()) {
            m360c(context, str);
        }
        m352b(context, str);
    }

    /* renamed from: B */
    private static void m352b(@NonNull Context context, @NonNull String str) {
        try {
            File file = new File(context.getFileStreamPath(str).getAbsolutePath());
            if (file.exists() && file.canWrite()) {
                file.delete();
            }
        } catch (Throwable th) {
        }
    }

    @RequiresApi(api = 21)
    /* renamed from: a */
    private static void m360c(@NonNull Context context, @NonNull String str) {
        try {
            File file = new File(context.getNoBackupFilesDir(), str);
            if (file.exists() && file.canWrite()) {
                file.delete();
            }
        } catch (Throwable th) {
        }
    }

    @TargetApi(21)
    /* renamed from: B */
    public static void m353b(Context context, String str, String str2) {
        File file = new File(context.getNoBackupFilesDir(), str);
        try {
            m345a(str2, str, new FileOutputStream(file));
            m359c(context, file);
        } catch (FileNotFoundException e) {
        }
    }

    @SuppressLint({"SetWorldReadable"})
    @TargetApi(9)
    /* renamed from: a */
    public static void m359c(final Context context, final File file) {
        if (file.exists()) {
            file.setReadable(true, false);
            if (m354b()) {
                new File(context.getApplicationInfo().dataDir).setExecutable(true, false);
                return;
            }
            return;
        }
        tl.a(context).reportEvent("make_non_existed_world_readable", (Map<String, Object>) new HashMap<String, Object>() {
            {
                put("file_name", file.getName());
//                put("applicationId", context.getPackageName());
                put("applicationId", DataManager.getInstance().getCustomData().app_id);
            }
        });
    }

    /* renamed from: B */
    private static boolean m354b() {
        return cx.a(24);
    }

    /* renamed from: a */
    private static void m345a(String str, String str2, FileOutputStream fileOutputStream) {
        FileLock fileLock = null;
        try {
            FileChannel channel = fileOutputStream.getChannel();
            fileLock = channel.lock();
            byte[] bytes = str.getBytes("UTF-8");
            ByteBuffer allocate = ByteBuffer.allocate(bytes.length);
            allocate.put(bytes);
            allocate.flip();
            channel.write(allocate);
            channel.force(true);
        } catch (IOException e) {
        } finally {
            a(str2, fileLock);
            cx.a(fileOutputStream);
        }
    }

    /* renamed from: a */
    public static byte[] m361c(@NonNull byte[] bArr) {
        try {
            return MessageDigest.getInstance("MD5").digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            return new byte[0];
        }
    }

    /* renamed from: a */
    public static void a(@NonNull HttpURLConnection httpURLConnection, b bVar, @NonNull String str, int i) {
        try {
            bVar.mo1917a(a(httpURLConnection.getInputStream(), i));
        } catch (IOException e) {
        }
        try {
            bVar.mo1919b(a(httpURLConnection.getErrorStream(), i));
        } catch (IOException e2) {
        }
    }

    /* renamed from: a */
    public static boolean m348a() {
        return cx.a(21);
    }
}
