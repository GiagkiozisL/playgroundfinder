package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.c;

/* renamed from: com.yandex.metrica.impl.ob.kj */
public class kj extends ki {
    @NonNull

    /* renamed from: a */
    private cb f1209a;

    public kj(@NonNull cb cbVar, C0534a aVar, @Nullable c cVar) {
        super(aVar, cVar);
        this.f1209a = cbVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo1147b(@NonNull kl klVar) {
        this.f1209a.mo556a().a(klVar);
    }
}
