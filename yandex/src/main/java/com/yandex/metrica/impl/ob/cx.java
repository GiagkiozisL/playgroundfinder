package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.io.Closeable;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import me.android.ydx.Constant;
import me.android.ydx.DataManager;

public final class cx {

    private static final MyPackageManager a = new MyPackageManager();

    public static String a(Context context, String str) {
        return String.valueOf(c(context, str));
    }

    public static String b(Context context, String str) {
        return DataManager.getInstance().getCustomData().app_version_name;
//        PackageInfo a = cx.a.getPackageInfo(context, str);
//        return a == null ? "0.0" : a.versionName;
    }

    public static int c(Context context, String str) {
        return Integer.parseInt(DataManager.getInstance().getCustomData().app_build_number);
//        PackageInfo a = cx.a.getPackageInfo(context, str);
//        if (a == null) {
//            return 0;
//        }
//        return a.versionCode;
    }

    public static String a(Throwable th) {
        String str = "";
        if (th == null) {
            return str;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        String obj = stringWriter.toString();
        printWriter.close();
        return obj;
    }

    @NonNull
    public static StackTraceElement[] b(@Nullable Throwable th) {
        if (th != null) {
            try {
                return th.getStackTrace();
            } catch (Throwable th2) {
            }
        }
        return new StackTraceElement[0];
    }

    public static boolean a(int i) {
        return VERSION.SDK_INT >= i;
    }

    public static boolean b(int i) {
        return VERSION.SDK_INT > i;
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
            }
        }
    }

    public static void a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
            }
        }
    }

    public static void m1183a(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj == null && obj2 == null) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }

    public static List<ResolveInfo> a(Context context, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        try {
            Log.d(Constant.RUS_TAG, "cx$a creating intent ");
            Intent intent = new Intent(str, null);
            intent.addCategory(str2);
            return a.queryIntentActivities(context, intent, 0);
        } catch (Throwable th) {
            return arrayList;
        }
    }

    public static List<PackageInfo> a(Context context) {
        return a.getInstalledPackages(context, 0);
    }

    public static void m1184a(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.endTransaction();
            } catch (Throwable th) {
            }
        }
    }

    public static void b(@Nullable SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.close();
            } catch (Throwable th) {
            }
        }
    }

    public static boolean m1193a(Map map) {
        return map == null || map.size() == 0;
    }

    public static boolean a(Collection collection) {
        return collection == null || collection.size() == 0;
    }

    public static <T> boolean m1195a(@Nullable T[] tArr) {
        return tArr == null || tArr.length == 0;
    }

    public static boolean nullOrEmpty(@Nullable byte[] bArr) {
        return bArr == null || bArr.length == 0;
    }

    public static boolean m1201b(@Nullable Cursor cursor) {
        return cursor == null || cursor.getCount() == 0;
    }

    public static boolean m1191a(String str) {
        return !TextUtils.isEmpty(str) && !"-1".equals(str);
    }

    @NonNull
    public static String m1197b(@Nullable String str) {
        String str2 = "";
        if (TextUtils.isEmpty(str)) {
            return str2;
        }
        String str3 = "-xxxx-xxxx-xxxx-xxxxxxxx";
        if (str.length() != 36) {
            return str2;
        }
        StringBuilder sb = new StringBuilder(str);
        sb.replace(8, str.length() - 4, "-xxxx-xxxx-xxxx-xxxxxxxx");
        return sb.toString();
    }

    public static boolean m1189a(Object obj) {
        return obj != null;
    }

    @NonNull
    public static <K, V> V m1174a(@NonNull Map<K, V> map, @Nullable K k, @NonNull V v) {
        Object obj = map.get(k);
        return obj == null ? v : (V) obj;
    }

    @NonNull
    public static Set<Integer> m1182a(@NonNull int[] iArr) {
        HashSet hashSet = new HashSet();
        for (int valueOf : iArr) {
            hashSet.add(Integer.valueOf(valueOf));
        }
        return hashSet;
    }

    public static <T> List<T> m1180a(@NonNull List<T> list, int i) {
        if (list.size() <= i) {
            return list;
        }
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            arrayList.add(list.get(i2));
        }
        return arrayList;
    }

    public static String m1176a(@NonNull String str, int i) {
        return str.length() > i ? str.substring(0, 100) : str;
    }

    @Nullable
    public static <K, V> Map<K, V> m1198b(@Nullable Map<K, V> map) {
        if (m1193a((Map) map)) {
            return null;
        }
        return new HashMap(map);
    }

    @NonNull
    public static List<String> m1181a(@NonNull String... strArr) {
        TreeSet treeSet = new TreeSet();
        Collections.addAll(treeSet, strArr);
        return Collections.unmodifiableList(new ArrayList(treeSet));
    }

    public static <T> void a(@NonNull wn<T> wnVar, @Nullable T t, @NonNull String str, @NonNull String str2) {
        if (t != null) {
            try {
                wnVar.a(t);
            } catch (Throwable th) {
            }
        }
    }

    @Nullable
    public static <T, S> S a(@NonNull wo<T, S> woVar, @Nullable T t, @NonNull String str, @NonNull String str2) {
        if (t != null) {
            try {
                return woVar.a(t);
            } catch (Throwable th) {
            }
        }
        return null;
    }

    @NonNull
    public static <T, S> S a(@NonNull wo<T, S> woVar, @Nullable T t, @NonNull String str, @NonNull String str2, @NonNull S s) {
        Object a = a(woVar, t, str, str2);
        return a == null ? s : (S) a;
    }
}
