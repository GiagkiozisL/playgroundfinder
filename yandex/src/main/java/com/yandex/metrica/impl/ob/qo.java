package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.qo */
public abstract class qo {
    @NonNull

    /* renamed from: a */
    private final qn f1854a;

    /* renamed from: a */
    public abstract rl.a.Aa mo1748a(@NonNull re reVar, @Nullable rl.a.Aa aVar, @NonNull qm qmVar);

    qo(@NonNull qn qnVar) {
        this.f1854a = qnVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public qn mo1747a() {
        return this.f1854a;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public final boolean mo1749a(@Nullable rl.a.Aa aVar) {
        return aVar == null || aVar.f2048d.f2051c;
    }
}
