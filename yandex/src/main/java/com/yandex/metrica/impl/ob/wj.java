package com.yandex.metrica.impl.ob;

import android.text.TextUtils;

import com.yandex.metrica.i;

import org.json.JSONObject;

import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.wj */
public class wj {

    /* renamed from: com.yandex.metrica.impl.ob.wj$a */
    public enum C1163a {
        LOGIN("login"),
        LOGOUT("logout"),
        SWITCH("switch"),
        UPDATE("update");
        

        /* renamed from: e */
        private String f2939e;

        private C1163a(String str) {
            this.f2939e = str;
        }

        public String toString() {
            return this.f2939e;
        }
    }

    /* renamed from: a */
    public static i m4363a(String str) {
        i iVar = new i();
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                iVar.mo180a(jSONObject.optString("UserInfo.UserId", null));
                iVar.mo183b(jSONObject.optString("UserInfo.Type", null));
                iVar.mo181a((Map<String, String>) vq.m4216a(jSONObject.optJSONObject("UserInfo.Options")));
            } catch (Throwable th) {
            }
        }
        return iVar;
    }

    /* renamed from: a */
    public static String m4364a(C1163a var0) {
        String var1 = null;

        try {
            JSONObject var2 = new JSONObject();
            var2.putOpt("action", var0.toString());
            var1 = var2.toString();
        } catch (Throwable var3) {
        }

        return var1;
    }
}
