package com.yandex.metrica.impl.ob;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.p */
public interface p {

    /* renamed from: com.yandex.metrica.impl.ob.p$a */
    class C0740a<T> {

        /* renamed from: a */
        public static final long f1700a = TimeUnit.SECONDS.toMillis(10);

        /* renamed from: B */
        private long f1701b;

        /* renamed from: a */
        private long f1702c;

        /* renamed from: d */
        private T f1703d;

        /* renamed from: e */
        private boolean f1704e;

        public C0740a() {
            this(f1700a);
        }

        public C0740a(long j) {
            this.f1702c = 0;
            this.f1703d = null;
            this.f1704e = true;
            this.f1701b = j;
        }

        /* renamed from: a */
        public T mo1620a() {
            return this.f1703d;
        }

        /* renamed from: a */
        public void mo1621a(T t) {
            this.f1703d = t;
            m2780e();
            this.f1704e = false;
        }

        /* renamed from: B */
        public final boolean mo1623b() {
            return this.f1703d == null;
        }

        /* renamed from: e */
        private void m2780e() {
            this.f1702c = System.currentTimeMillis();
        }

        /* renamed from: a */
        public final boolean mo1622a(long j) {
            long currentTimeMillis = System.currentTimeMillis() - this.f1702c;
            return currentTimeMillis > j || currentTimeMillis < 0;
        }

        /* renamed from: a */
        public final boolean mo1624c() {
            return mo1622a(this.f1701b);
        }

        /* renamed from: d */
        public T mo1625d() {
            if (mo1624c()) {
                return null;
            }
            return this.f1703d;
        }
    }
}
