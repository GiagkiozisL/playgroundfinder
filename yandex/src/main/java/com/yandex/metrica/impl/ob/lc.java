package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.io.Closeable;

/* renamed from: com.yandex.metrica.impl.ob.lc */
public class lc extends SQLiteOpenHelper implements Closeable {

    /* renamed from: a */
    protected final lj f1292a;
    @NonNull

    /* renamed from: B */
    private final String f1293b;

    /* renamed from: a */
    private final vz f1294c;
    @NonNull

    /* renamed from: d */
    private final Context f1295d;

    public lc(Context context, @NonNull String str, lj ljVar) {
        this(context, str, ljVar, vr.m4236a());
    }

    @VisibleForTesting
    lc(Context context, @NonNull String str, lj ljVar, @NonNull vz vzVar) {
        super(context, str, null, lq.f1341b);
        this.f1295d = context;
        this.f1292a = ljVar;
        this.f1293b = str;
        this.f1294c = vzVar;
    }

    public void onCreate(SQLiteDatabase database) {
        this.f1292a.mo1264b(database);
    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        this.f1292a.mo1262a(database, oldVersion, newVersion);
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        this.f1292a.mo1261a(db);
    }

    @Nullable
    public SQLiteDatabase getReadableDatabase() {
        try {
            return super.getReadableDatabase();
        } catch (Throwable th) {
            this.f1294c.mo2479a(th, "", new Object[0]);
            this.f1294c.mo2484c("Could not get readable database %s due to an exception. AppMetrica SDK may behave unexpectedly.", this.f1293b);
            tl.a(this.f1295d).reportError("db_read_error", th);
            return null;
        }
    }

    @Nullable
    public SQLiteDatabase getWritableDatabase() {
        try {
            return super.getWritableDatabase();
        } catch (Throwable th) {
            this.f1294c.mo2479a(th, "", new Object[0]);
            this.f1294c.mo2484c("Could not get writable database %s due to an exception. AppMetrica SDK may behave unexpectedly.", this.f1293b);
            tl.a(this.f1295d).reportError("db_write_error", th);
            return null;
        }
    }
}
