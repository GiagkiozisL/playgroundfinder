package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.xi */
public interface xi extends xh {
    @NonNull
    /* renamed from: a */
    Handler a();

    @NonNull
    /* renamed from: B */
    Looper b();
}
