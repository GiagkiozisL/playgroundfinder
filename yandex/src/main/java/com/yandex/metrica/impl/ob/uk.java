package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.cq.a;

import java.util.Collections;
import java.util.List;

public class uk {

    public final List<pu> permissions;
    public final ua sdkFingerprintingConfig;
    public final ty identityLightCollectingConfig;
    public final long obtainServerTime;
    public final long firstStartupServerTime;
    public final boolean outdated;
    public final tt bleCollectingConfig;
    public final String uuid;
    public final String deviceId;
    public final String deviceID2;
    public final String deviceIDHash;
    public final List<String> reportUrls;
    public final String getAdUrl;
    public final String reportAdUrl;
    public final String sdkListUrl;
    public final List<String> locationUrls;
    public final List<String> hostUrlsFromStartup;
    public final List<String> hostUrlsFromClient;
    public final List<String> diagnosticUrls;
    public final String encodedClidsFromResponse;
    public final String lastStartupRequestClids;
    public final tv collectingFlags;
    public final oh foregroundLocationCollectionConfig;
    public final oc backgroundLocationCollectionConfig;
    public final ub socketConfig;
    public final String distributionReferrer;
    public final long t;
    public final boolean hadFirstStartup;
    public final boolean startupResponseClidsMatchClientClids;
    public final List<a> w;
    public final String countryInit;
    public final um statSending;
    public final tz permissionsCollectingConfig;

    public static class C1077a {
        /* access modifiers changed from: private */
        @Nullable

        public List<a> f2774A;
        /* access modifiers changed from: private */
        @Nullable

        public String f2775B;
        /* access modifiers changed from: private */
        @Nullable

        public List<pu> f2776C;
        /* access modifiers changed from: private */
        @NonNull

        public ua f2777D;
        /* access modifiers changed from: private */

        public long f2778E;
        /* access modifiers changed from: private */

        public long f2779F;
        /* access modifiers changed from: private */
        @Nullable

        public ty f2780G;
        @Nullable

        String uuid;
        @Nullable

        String deviceId;
        @Nullable

        String f2783c;
        @Nullable

        String f2784d;
        @Nullable

        List<String> f2785e;
        @Nullable

        String f2786f;
        @Nullable

        String f2787g;
        @Nullable

        String f2788h;
        @Nullable

        List<String> f2789i;
        @Nullable

        List<String> f2790j;
        @Nullable

        List<String> f2791k;
        @Nullable

        List<String> f2792l;
        @Nullable

        String f2793m;
        @Nullable

        String f2794n;
        @NonNull

        final tv f2795o;
        @Nullable

        oh f2796p;
        @Nullable

        oc f2797q;
        @Nullable

        ub f2798r;
        @Nullable

        tz f2799s;

        long f2800t;

        boolean f2801u;
        @Nullable

        String f2802v;

        boolean f2803w;
        @Nullable

        um f2804x;

        boolean f2805y;
        @Nullable

        tt f2806z;

        public C1077a(@NonNull tv tvVar) {
            this.f2795o = tvVar;
        }

        public C1077a mo2370a(@Nullable String str) {
            this.uuid = str;
            return this;
        }

        public C1077a setDeviceId(@Nullable String str) {
            this.deviceId = str;
            return this;
        }

        public C1077a mo2379c(@Nullable String str) {
            this.f2783c = str;
            return this;
        }

        public C1077a mo2382d(@Nullable String str) {
            this.f2784d = str;
            return this;
        }

        public C1077a mo2371a(@Nullable List<String> list) {
            this.f2785e = list;
            return this;
        }

        public C1077a mo2384e(@Nullable String str) {
            this.f2786f = str;
            return this;
        }

        public C1077a mo2386f(@Nullable String str) {
            this.f2787g = str;
            return this;
        }

        public C1077a mo2388g(@Nullable String str) {
            this.f2788h = str;
            return this;
        }

        public C1077a mo2376b(@Nullable List<String> list) {
            this.f2789i = list;
            return this;
        }

        public C1077a mo2380c(@Nullable List<String> list) {
            this.f2790j = list;
            return this;
        }

        public C1077a mo2383d(@Nullable List<String> list) {
            this.f2791k = list;
            return this;
        }

        public C1077a mo2385e(@Nullable List<String> list) {
            this.f2792l = list;
            return this;
        }

        public C1077a mo2390h(@Nullable String str) {
            this.f2793m = str;
            return this;
        }

        public C1077a mo2391i(@Nullable String str) {
            this.f2794n = str;
            return this;
        }

        public C1077a mo2363a(@Nullable oh ohVar) {
            this.f2796p = ohVar;
            return this;
        }

        public C1077a mo2362a(@Nullable oc ocVar) {
            this.f2797q = ocVar;
            return this;
        }

        public C1077a mo2368a(@Nullable ub ubVar) {
            this.f2798r = ubVar;
            return this;
        }

        public C1077a mo2366a(@Nullable tz tzVar) {
            this.f2799s = tzVar;
            return this;
        }

        public C1077a mo2392j(@Nullable String str) {
            this.f2802v = str;
            return this;
        }

        public C1077a mo2361a(long j) {
            this.f2800t = j;
            return this;
        }

        public C1077a mo2372a(boolean z) {
            this.f2801u = z;
            return this;
        }

        public C1077a mo2377b(boolean z) {
            this.f2803w = z;
            return this;
        }

        public C1077a mo2387f(@Nullable List<a> list) {
            this.f2774A = list;
            return this;
        }

        public C1077a mo2393k(@Nullable String str) {
            this.f2775B = str;
            return this;
        }

        public C1077a mo2389g(@Nullable List<pu> list) {
            this.f2776C = list;
            return this;
        }

        public C1077a mo2367a(@NonNull ua uaVar) {
            this.f2777D = uaVar;
            return this;
        }

        public C1077a mo2374b(long j) {
            this.f2778E = j;
            return this;
        }

        public C1077a mo2378c(long j) {
            this.f2779F = j;
            return this;
        }

        public C1077a mo2369a(um umVar) {
            this.f2804x = umVar;
            return this;
        }

        public C1077a mo2381c(boolean z) {
            this.f2805y = z;
            return this;
        }

        public C1077a mo2365a(@Nullable ty tyVar) {
            this.f2780G = tyVar;
            return this;
        }

        public C1077a mo2364a(@Nullable tt ttVar) {
            this.f2806z = ttVar;
            return this;
        }

        @NonNull
        public uk mo2373a() {
            return new uk(this);
        }
    }

    private uk(@NonNull C1077a aVar) {
        List<String> unmodifiableList;
        List<String> unmodifiableList2;
        List<String> unmodifiableList3;
        List<a> list = null;
        this.uuid = aVar.uuid;
        this.deviceId = aVar.deviceId;
        this.deviceID2 = aVar.f2783c;
        this.deviceIDHash = aVar.f2784d;
        this.reportUrls = aVar.f2785e == null ? null : Collections.unmodifiableList(aVar.f2785e);
        this.getAdUrl = aVar.f2786f;
        this.reportAdUrl = aVar.f2787g;
        this.sdkListUrl = aVar.f2788h;
        this.locationUrls = aVar.f2789i == null ? null : Collections.unmodifiableList(aVar.f2789i);
        if (aVar.f2790j == null) {
            unmodifiableList = null;
        } else {
            unmodifiableList = Collections.unmodifiableList(aVar.f2790j);
        }
        this.hostUrlsFromStartup = unmodifiableList;
        if (aVar.f2791k == null) {
            unmodifiableList2 = null;
        } else {
            unmodifiableList2 = Collections.unmodifiableList(aVar.f2791k);
        }
        this.hostUrlsFromClient = unmodifiableList2;
        if (aVar.f2792l == null) {
            unmodifiableList3 = null;
        } else {
            unmodifiableList3 = Collections.unmodifiableList(aVar.f2792l);
        }
        this.diagnosticUrls = unmodifiableList3;
        this.encodedClidsFromResponse = aVar.f2793m;
        this.lastStartupRequestClids = aVar.f2794n;
        this.collectingFlags = aVar.f2795o;
        this.foregroundLocationCollectionConfig = aVar.f2796p;
        this.backgroundLocationCollectionConfig = aVar.f2797q;
        this.socketConfig = aVar.f2798r;
        this.permissionsCollectingConfig = aVar.f2799s;
        this.distributionReferrer = aVar.f2802v;
        this.t = aVar.f2800t;
        this.hadFirstStartup = aVar.f2801u;
        this.startupResponseClidsMatchClientClids = aVar.f2803w;
        if (aVar.f2774A != null) {
            list = Collections.unmodifiableList(aVar.f2774A);
        }
        this.w = list;
        this.countryInit = aVar.f2775B;
        this.permissions = aVar.f2776C;
        this.sdkFingerprintingConfig = aVar.f2777D;
        this.statSending = aVar.f2804x;
        this.obtainServerTime = aVar.f2778E;
        this.firstStartupServerTime = aVar.f2779F;
        this.outdated = aVar.f2805y;
        this.identityLightCollectingConfig = aVar.f2780G;
        this.bleCollectingConfig = aVar.f2806z;
    }

    public C1077a mo2359a() {
        return new C1077a(this.collectingFlags).mo2370a(this.uuid).setDeviceId(this.deviceId).mo2379c(this.deviceID2).mo2382d(this.deviceIDHash).mo2380c(this.hostUrlsFromStartup).mo2383d(this.hostUrlsFromClient).mo2390h(this.encodedClidsFromResponse).mo2371a(this.reportUrls).mo2376b(this.locationUrls).mo2384e(this.getAdUrl).mo2386f(this.reportAdUrl).mo2388g(this.sdkListUrl).mo2385e(this.diagnosticUrls).mo2392j(this.distributionReferrer).mo2363a(this.foregroundLocationCollectionConfig).mo2362a(this.backgroundLocationCollectionConfig).mo2368a(this.socketConfig).mo2391i(this.lastStartupRequestClids).mo2377b(this.startupResponseClidsMatchClientClids).mo2361a(this.t).mo2372a(this.hadFirstStartup).mo2387f(this.w).mo2393k(this.countryInit).mo2389g(this.permissions).mo2366a(this.permissionsCollectingConfig).mo2367a(this.sdkFingerprintingConfig).mo2374b(this.obtainServerTime).mo2378c(this.firstStartupServerTime).mo2369a(this.statSending).mo2381c(this.outdated).mo2365a(this.identityLightCollectingConfig).mo2364a(this.bleCollectingConfig);
    }

    public String toString() {
        return "StartupState{uuid='" + this.uuid + '\'' + ", deviceID='" + this.deviceId + '\'' + ", deviceID2='" + this.deviceID2 + '\'' + ", deviceIDHash='" + this.deviceIDHash + '\'' + ", reportUrls=" + this.reportUrls + ", getAdUrl='" + this.getAdUrl + '\'' + ", reportAdUrl='" + this.reportAdUrl + '\'' + ", sdkListUrl='" + this.sdkListUrl + '\'' + ", locationUrls=" + this.locationUrls + ", hostUrlsFromStartup=" + this.hostUrlsFromStartup + ", hostUrlsFromClient=" + this.hostUrlsFromClient + ", diagnosticUrls=" + this.diagnosticUrls + ", encodedClidsFromResponse='" + this.encodedClidsFromResponse + '\'' + ", lastStartupRequestClids='" + this.lastStartupRequestClids + '\'' + ", collectingFlags=" + this.collectingFlags + ", foregroundLocationCollectionConfig=" + this.foregroundLocationCollectionConfig + ", backgroundLocationCollectionConfig=" + this.backgroundLocationCollectionConfig + ", socketConfig=" + this.socketConfig + ", distributionReferrer='" + this.distributionReferrer + '\'' + ", obtainTime=" + this.t + ", hadFirstStartup=" + this.hadFirstStartup + ", startupResponseClidsMatchClientClids=" + this.startupResponseClidsMatchClientClids + ", requests=" + this.w + ", countryInit='" + this.countryInit + '\'' + ", statSending=" + this.statSending + ", permissionsCollectingConfig=" + this.permissionsCollectingConfig + ", permissions=" + this.permissions + ", sdkFingerprintingConfig=" + this.sdkFingerprintingConfig + ", identityLightCollectingConfig=" + this.identityLightCollectingConfig + ", obtainServerTime=" + this.obtainServerTime + ", firstStartupServerTime=" + this.firstStartupServerTime + ", outdated=" + this.outdated + ", bleCollectingConfig=" + this.bleCollectingConfig + '}';
    }
}
