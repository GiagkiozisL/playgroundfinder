package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ym */
public class ym implements yk<String> {

    /* renamed from: a */
    private final Map<String, ?> f3011a;

    public ym(@NonNull Map<String, ?> map) {
        this.f3011a = map;
    }

    /* renamed from: a */
    public yi a(@Nullable String str) {
        if (!this.f3011a.containsKey(str)) {
            return yi.a(this);
        }
        return yi.a(this, String.format("Failed to activate AppMetrica with provided apiKey ApiKey %s has already been used by another reporter.", new Object[]{str}));
    }
}
