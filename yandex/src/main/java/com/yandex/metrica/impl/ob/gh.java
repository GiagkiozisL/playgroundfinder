package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.gk.C0392a;
import com.yandex.metrica.impl.ob.rh.c.e;
import com.yandex.metrica.impl.ob.rh.c.e.a;
import com.yandex.metrica.impl.ob.rh.c.e.b;
import com.yandex.metrica.impl.ob.rh.c.g;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.gh */
public class gh {

    /* renamed from: a */
    public static final Map<Integer, Integer> f958a = Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
        {
            put(Integer.valueOf(C0058a.EVENT_TYPE_DIAGNOSTIC.mo259a()), Integer.valueOf(22));
            put(Integer.valueOf(C0058a.EVENT_TYPE_DIAGNOSTIC_STATBOX.mo259a()), Integer.valueOf(23));
            put(Integer.valueOf(C0058a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.mo259a()), Integer.valueOf(24));
        }
    });
    @NonNull

    /* renamed from: B */
    private final w f959b;
    @NonNull

    /* renamed from: a */
    private final gi f960c;
    @NonNull

    /* renamed from: d */
    private final gk f961d;
    @NonNull

    /* renamed from: e */
    private final xy f962e;
    @NonNull

    /* renamed from: f */
    private final xy f963f;
    @NonNull

    /* renamed from: g */
    private final wh f964g;
    @NonNull

    /* renamed from: h */
    private final er f965h;

    /* renamed from: com.yandex.metrica.impl.ob.gh$a */
    public static class C0386a {
        /* renamed from: a */
        public gh mo935a(@NonNull w wVar, @NonNull gi giVar, @NonNull gk gkVar, @NonNull lw lwVar) {
            return new gh(wVar, giVar, gkVar, lwVar);
        }
    }

    public gh(@NonNull w wVar, @NonNull gi giVar, @NonNull gk gkVar, @NonNull lw lwVar) {
        this(wVar, giVar, gkVar, new er(lwVar), new xy(1024, "diagnostic event name"), new xy(204800, "diagnostic event value"), new wg());
    }

    public gh(@NonNull w wVar, @NonNull gi giVar, @NonNull gk gkVar, @NonNull er erVar, @NonNull xy xyVar, @NonNull xy xyVar2, @NonNull wh whVar) {
        this.f959b = wVar;
        this.f960c = giVar;
        this.f961d = gkVar;
        this.f965h = erVar;
        this.f963f = xyVar;
        this.f962e = xyVar2;
        this.f964g = whVar;
    }

    /* renamed from: a */
    public byte[] mo934a() {
        rh.c cVar = new rh.c();
        e eVar = new e();
        cVar.f1940b = new e[]{eVar};
        C0392a a = this.f961d.mo951a();
        eVar.b = a.f974a;
        eVar.c = new b();
        eVar.c.d = 2;
        eVar.c.f2010b = new g();
        eVar.c.f2010b.f2019b = a.f975b;
        eVar.c.f2010b.f2020c = wi.m4358a(a.f975b);
        eVar.c.f2011c = this.f960c.A();
        e.a aVar = new a();
        eVar.d = new a[]{aVar};
        aVar.f1984b = (long) a.f976c;
        aVar.f1999q = (long) this.f965h.mo866a(this.f959b.mo2533g());
        aVar.f1985c = this.f964g.b() - a.f975b;
        aVar.f1986d = ((Integer) f958a.get(Integer.valueOf(this.f959b.mo2533g()))).intValue();
        if (!TextUtils.isEmpty(this.f959b.mo2528d())) {
            aVar.f1987e = this.f963f.a(this.f959b.mo2528d());
        }
        if (!TextUtils.isEmpty(this.f959b.mo2531e())) {
            String e = this.f959b.mo2531e();
            String a2 = this.f962e.a(e);
            if (!TextUtils.isEmpty(a2)) {
                aVar.f1988f = a2.getBytes();
            }
            aVar.f1993k = e.getBytes().length - (aVar.f1988f == null ? 0 : aVar.f1988f.length);
        }
        return com.yandex.metrica.impl.ob.e.m1395a((com.yandex.metrica.impl.ob.e) cVar);
    }
}
