package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.sn.c;

/* renamed from: com.yandex.metrica.impl.ob.sp */
public abstract class sp<T extends sn,
        IA,
        A extends sm<IA, A>,
        L extends sn.d<T, sn.c<A>>> {
    @Nullable

    /* renamed from: a */
    private T f2446a;
    @NonNull

    /* renamed from: B */
    private L f2447b;
    @NonNull

    /* renamed from: a */
    private sn.c<A> c;

    public sp(@NonNull L l, @NonNull uk ukVar, @NonNull A a) {
        this.f2447b = l;
        dr.a().a(this, dz.class, dv.m1388a((du<dz>) new du<dz>() {
            /* renamed from: a */
            public void a(dz dzVar) {
                sp.this.mo2119a();
            }
        }).mo736a());
        mo2120a(new c<>(ukVar, a));
    }

    /* renamed from: a */
    public synchronized void mo2119a() {
        this.f2446a = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized void mo2120a(@NonNull sn.c<A> cVar) {
        this.c = cVar;
    }

    /* renamed from: a */
    public synchronized void a(@NonNull IA ia) {
        if (!((sm) this.c.b).a(ia)) {
//            mo2120a(new a<>(mo2122b(), ((sm) this.a.b).b(ia)));
            mo2120a(new c(this.mo2122b(), ((sm)this.c.b).b(ia)));
            mo2119a();
        }
    }

    /* renamed from: a */
    public synchronized void mo2121a(@NonNull uk ukVar) {
        mo2120a(new c<>(ukVar, mo2123c()));
        mo2119a();
    }

    @NonNull
    /* renamed from: B */
    public synchronized uk mo2122b() {
        return this.c.f2444a;
    }

    @VisibleForTesting(otherwise = 4)
    @NonNull
    /* renamed from: a */
    public synchronized A mo2123c() {
//        return (sm) this.a.b;
        return (A) this.c.b;
    }

    @NonNull
    /* renamed from: d */
    public synchronized T mo2124d() {
        if (this.f2446a == null) {
            this.f2446a = this.f2447b.a(this.c);
//            this.f2446a = this.f2447b.a(this.resolveService);
        }
        return this.f2446a;
    }
}
