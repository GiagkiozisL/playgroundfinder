package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.px */
public class px {

    /* renamed from: a */
    private po f1747a = po.m2828b().mo1646a();

    /* renamed from: a */
    public synchronized void mo1668a(boolean z) {
        this.f1747a = this.f1747a.mo1642a().mo1645a(z).mo1646a();
    }

    /* renamed from: a */
    public synchronized void mo1667a(@NonNull uk ukVar) {
        this.f1747a = this.f1747a.mo1642a().mo1644a(ukVar).mo1646a();
    }

    /* renamed from: a */
    public synchronized pp mo1666a() {
        return this.f1747a;
    }
}
