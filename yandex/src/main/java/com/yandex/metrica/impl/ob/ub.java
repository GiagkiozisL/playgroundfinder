package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.ub */
public class ub {

    /* renamed from: a */
    public final long f2708a;

    /* renamed from: B */
    public final String f2709b;

    /* renamed from: a */
    public final List<Integer> f2710c;

    /* renamed from: d */
    public final long f2711d;

    /* renamed from: e */
    public final int f2712e;

    public ub(long j, @NonNull String str, @NonNull List<Integer> list, long j2, int i) {
        this.f2708a = j;
        this.f2709b = str;
        this.f2710c = Collections.unmodifiableList(list);
        this.f2711d = j2;
        this.f2712e = i;
    }

    @Nullable
    /* renamed from: a */
    public static ub m3889a(@Nullable String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            return new ub(jSONObject.getLong("seconds_to_live"), jSONObject.getString("token"), m3890a(jSONObject.getJSONArray("ports")), jSONObject.getLong("first_delay_seconds"), jSONObject.getInt("launch_delay_seconds"));
        } catch (Throwable th) {
            return null;
        }
    }

    /* renamed from: a */
    private static ArrayList<Integer> m3890a(JSONArray jSONArray) throws JSONException {
        ArrayList<Integer> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(Integer.valueOf(jSONArray.getInt(i)));
        }
        return arrayList;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ub ubVar = (ub) o;
        if (this.f2708a != ubVar.f2708a || this.f2711d != ubVar.f2711d || this.f2712e != ubVar.f2712e) {
            return false;
        }
        if (this.f2709b != null) {
            if (!this.f2709b.equals(ubVar.f2709b)) {
                return false;
            }
        } else if (ubVar.f2709b != null) {
            return false;
        }
        if (this.f2710c != null) {
            z = this.f2710c.equals(ubVar.f2710c);
        } else if (ubVar.f2710c != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int i3 = ((int) (this.f2708a ^ (this.f2708a >>> 32))) * 31;
        if (this.f2709b != null) {
            i = this.f2709b.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i + i3) * 31;
        if (this.f2710c != null) {
            i2 = this.f2710c.hashCode();
        }
        return ((((i4 + i2) * 31) + ((int) (this.f2711d ^ (this.f2711d >>> 32)))) * 31) + this.f2712e;
    }

    public String toString() {
        return "SocketConfig{secondsToLive=" + this.f2708a + ", token='" + this.f2709b + '\'' + ", ports=" + this.f2710c + ", firstDelaySeconds=" + this.f2711d + ", launchDelaySeconds=" + this.f2712e + '}';
    }
}
