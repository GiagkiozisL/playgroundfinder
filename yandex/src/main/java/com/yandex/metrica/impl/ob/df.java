package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.df */
public class df implements dh {

    /* renamed from: g */
    private static final long f703g = new rr.a.b().d;
    @NonNull

    /* renamed from: a */
    private final Context f704a;
    @NonNull

    /* renamed from: B */
    private final dd f705b;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final dk f706c;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: d */
    public final di f707d;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: e */
    public ScanCallback f708e;

    /* renamed from: f */
    private long f709f;

    public df(@NonNull Context context) {
        this(context, new dd(context), new dk(), new di(), new dl(context, f703g));
    }

    /* renamed from: a */
    @SuppressLint("MissingPermission")
    public synchronized void a(@NonNull final tt ttVar) {
        BluetoothLeScanner a = this.f705b.mo692a();
        if (a != null) {
            a();
            long j = ttVar.c;
            if (this.f709f != j) {
                this.f709f = j;
                this.f708e = new dl(this.f704a, this.f709f);
            }
            cx.a((wn<BluetoothLeScanner>) new wn<BluetoothLeScanner>() {
                /* renamed from: a */
                public void a(BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.startScan(df.this.f707d.mo703a(ttVar.b), df.this.f706c.mo706a(ttVar.a), df.this.f708e);
                }
            }, a, "startScan", "BluetoothLeScanner");
        }
    }

    /* renamed from: a */
    @SuppressLint("MissingPermission")
    public synchronized void a() {
        BluetoothLeScanner a = this.f705b.mo692a();
        if (a != null) {
            cx.a((wn<BluetoothLeScanner>) new wn<BluetoothLeScanner>() {
                /* renamed from: a */
                public void a(BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.stopScan(df.this.f708e);
                }
            }, a, "stopScan", "BluetoothLeScanner");
        }
    }

    @VisibleForTesting
    public df(@NonNull Context context, @NonNull dd ddVar, @NonNull dk dkVar, @NonNull di diVar, @NonNull ScanCallback scanCallback) {
        this.f709f = f703g;
        this.f704a = context;
        this.f705b = ddVar;
        this.f706c = dkVar;
        this.f707d = diVar;
        this.f708e = scanCallback;
    }
}
