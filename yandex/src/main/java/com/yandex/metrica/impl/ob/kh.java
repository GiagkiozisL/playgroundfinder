package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.kh */
public class kh {
    @NonNull

    /* renamed from: a */
    public final kg f1204a;
    @Nullable

    /* renamed from: B */
    public final String f1205b;
    @Nullable

    /* renamed from: a */
    public final Boolean f1206c;

    public kh(@NonNull kg kgVar, @Nullable String str, @Nullable Boolean bool) {
        this.f1204a = kgVar;
        this.f1205b = str;
        this.f1206c = bool;
    }
}
