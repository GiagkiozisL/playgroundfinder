package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.SparseArray;

/* renamed from: com.yandex.metrica.impl.ob.ch */
public final class ch {

    /* renamed from: a */
    public static final C0197a f541a = new C0197a("3.8.0");
    @VisibleForTesting

    /* renamed from: B */
    static final SparseArray<C0197a> f542b = new SparseArray<>();

    /* renamed from: com.yandex.metrica.impl.ob.ch$a */
    public static final class C0197a {

        /* renamed from: a */
        public final String f543a;

        C0197a(String str) {
            this.f543a = str;
        }
    }

    static {
        f542b.put(1, new C0197a("1.00"));
        f542b.put(2, new C0197a("1.10"));
        f542b.put(3, new C0197a("1.11"));
        f542b.put(4, new C0197a("1.20"));
        f542b.put(5, new C0197a("1.21"));
        f542b.put(6, new C0197a("1.22"));
        f542b.put(7, new C0197a("1.23"));
        f542b.put(8, new C0197a("1.24"));
        f542b.put(9, new C0197a("1.26"));
        f542b.put(10, new C0197a("1.27"));
        f542b.put(11, new C0197a("1.40"));
        f542b.put(12, new C0197a("1.41"));
        f542b.put(13, new C0197a("1.42"));
        f542b.put(14, new C0197a("1.50"));
        f542b.put(15, new C0197a("1.51"));
        f542b.put(16, new C0197a("1.60"));
        f542b.put(17, new C0197a("1.61"));
        f542b.put(18, new C0197a("1.62"));
        f542b.put(19, new C0197a("1.63"));
        f542b.put(20, new C0197a("1.64"));
        f542b.put(21, new C0197a("1.65"));
        f542b.put(22, new C0197a("1.66"));
        f542b.put(23, new C0197a("1.67"));
        f542b.put(24, new C0197a("1.68"));
        f542b.put(25, new C0197a("1.69"));
        f542b.put(26, new C0197a("1.70"));
        f542b.put(27, new C0197a("1.71"));
        f542b.put(28, new C0197a("1.72"));
        f542b.put(29, new C0197a("1.80"));
        f542b.put(30, new C0197a("1.81"));
        f542b.put(31, new C0197a("1.82"));
        f542b.put(32, new C0197a("2.00"));
        f542b.put(33, new C0197a("2.10"));
        f542b.put(34, new C0197a("2.11"));
        f542b.put(35, new C0197a("2.20"));
        f542b.put(36, new C0197a("2.21"));
        f542b.put(37, new C0197a("2.22"));
        f542b.put(38, new C0197a("2.23"));
        f542b.put(39, new C0197a("2.30"));
        f542b.put(40, new C0197a("2.31"));
        f542b.put(41, new C0197a("2.32"));
        f542b.put(42, new C0197a("2.33"));
        f542b.put(43, new C0197a("2.40"));
        f542b.put(44, new C0197a("2.41"));
        f542b.put(45, new C0197a("2.42"));
        f542b.put(46, new C0197a("2.43"));
        f542b.put(47, new C0197a("2.50"));
        f542b.put(48, new C0197a("2.51"));
        f542b.put(49, new C0197a("2.52"));
        f542b.put(50, new C0197a("2.60"));
        f542b.put(51, new C0197a("2.61"));
        f542b.put(52, new C0197a("2.62"));
        f542b.put(53, new C0197a("2.63"));
        f542b.put(54, new C0197a("2.64"));
        f542b.put(55, new C0197a("2.70"));
        f542b.put(56, new C0197a("2.71"));
        f542b.put(57, new C0197a("2.72"));
        f542b.put(58, new C0197a("2.73"));
        f542b.put(59, new C0197a("2.74"));
        f542b.put(60, new C0197a("2.75"));
        f542b.put(61, new C0197a("2.76"));
        f542b.put(62, new C0197a("2.77"));
        f542b.put(63, new C0197a("2.78"));
        f542b.put(64, new C0197a("2.80"));
        f542b.put(65, new C0197a("2.81-RC1"));
        f542b.put(66, new C0197a("3.0.0"));
        f542b.put(67, new C0197a("3.1.0"));
        f542b.put(68, new C0197a("3.2.0"));
        f542b.put(69, new C0197a("3.2.1"));
        f542b.put(70, new C0197a("3.2.2"));
        f542b.put(71, new C0197a("3.3.0"));
        f542b.put(72, new C0197a("3.4.0"));
        f542b.put(73, new C0197a("3.5.0"));
        f542b.put(74, new C0197a("3.5.1"));
        f542b.put(75, new C0197a("3.5.2"));
        f542b.put(76, new C0197a("3.5.3"));
        f542b.put(77, new C0197a("3.6.0"));
        f542b.put(78, new C0197a("3.6.1"));
        f542b.put(79, new C0197a("3.6.2"));
        f542b.put(80, new C0197a("3.6.3"));
        f542b.put(81, new C0197a("3.6.4"));
        f542b.put(82, new C0197a("3.7.0"));
        f542b.put(83, new C0197a("3.7.1"));
        f542b.put(84, new C0197a("3.7.2"));
        f542b.put(85, new C0197a("3.8.0"));
    }

    @Nullable
    /* renamed from: a */
    static C0197a m941a(int i) {
        return (C0197a) f542b.get(i);
    }
}
