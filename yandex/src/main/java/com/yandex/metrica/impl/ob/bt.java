package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.yandex.metrica.b;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Locale;

/* renamed from: com.yandex.metrica.impl.ob.bt */
public final class bt {

    /* renamed from: a */
    private static final MyPackageManager f403a = new MyPackageManager();

    /* renamed from: B */
    private static final vw<Integer, a> f404b = new vw<Integer, a>(a.UNDEFINED) {
        {
            mo2510a(Integer.valueOf(1), a.WIFI);
            mo2510a(Integer.valueOf(0), a.CELL);
            if (cx.a(13)) {
                mo2510a(Integer.valueOf(7), a.BLUETOOTH);
                mo2510a(Integer.valueOf(9), a.ETHERNET);
            }
            mo2510a(Integer.valueOf(4), a.MOBILE_DUN);
            mo2510a(Integer.valueOf(5), a.MOBILE_HIPRI);
            mo2510a(Integer.valueOf(2), a.MOBILE_MMS);
            mo2510a(Integer.valueOf(3), a.MOBILE_SUPL);
            mo2510a(Integer.valueOf(6), a.WIMAX);
            if (cx.a(21)) {
                mo2510a(Integer.valueOf(17), a.VPN);
            }
        }
    };
    @TargetApi(23)

    /* renamed from: a */
    private static final vw<Integer, a> f405c = new vw<Integer, a>(a.UNDEFINED) {
        {
            mo2510a(Integer.valueOf(1), a.WIFI);
            mo2510a(Integer.valueOf(0), a.CELL);
            mo2510a(Integer.valueOf(3), a.ETHERNET);
            mo2510a(Integer.valueOf(2), a.BLUETOOTH);
            mo2510a(Integer.valueOf(4), a.VPN);
            if (cx.a(27)) {
                mo2510a(Integer.valueOf(6), a.LOWPAN);
            }
            if (cx.a(26)) {
                mo2510a(Integer.valueOf(5), a.WIFI_AWARE);
            }
        }
    };

    /* renamed from: d */
    private static final vw<a, Integer> f406d = new vw<a, Integer>(Integer.valueOf(2)) {
        {
            mo2510a(a.CELL, Integer.valueOf(0));
            mo2510a(a.WIFI, Integer.valueOf(1));
            mo2510a(a.BLUETOOTH, Integer.valueOf(3));
            mo2510a(a.ETHERNET, Integer.valueOf(4));
            mo2510a(a.MOBILE_DUN, Integer.valueOf(5));
            mo2510a(a.MOBILE_HIPRI, Integer.valueOf(6));
            mo2510a(a.MOBILE_MMS, Integer.valueOf(7));
            mo2510a(a.MOBILE_SUPL, Integer.valueOf(8));
            mo2510a(a.VPN, Integer.valueOf(9));
            mo2510a(a.WIMAX, Integer.valueOf(10));
            mo2510a(a.LOWPAN, Integer.valueOf(11));
            mo2510a(a.WIFI_AWARE, Integer.valueOf(12));
        }
    };

    /* renamed from: com.yandex.metrica.impl.ob.bt$a */
    public enum a {
        WIFI,
        CELL,
        ETHERNET,
        BLUETOOTH,
        VPN,
        LOWPAN,
        WIFI_AWARE,
        MOBILE_DUN,
        MOBILE_HIPRI,
        MOBILE_MMS,
        MOBILE_SUPL,
        WIMAX,
        OFFLINE,
        UNDEFINED
    }

    /* renamed from: com.yandex.metrica.impl.ob.bt$B */
    public static final class C0153b {

        /* renamed from: a */
        private static final String[] f422a = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};

        /* renamed from: a */
        public static boolean m739a() {
            try {
                if (new File("/system/app/Superuser.apk").exists()) {
                    return true;
                }
            } catch (Throwable th) {
            }
            return false;
        }

        /* renamed from: B */
        public static boolean m740b() {
            String[] strArr = f422a;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                try {
                    if (new File(strArr[i] + "su").exists()) {
                        return true;
                    }
                    i++;
                } catch (Throwable th) {
                }
            }
            return false;
        }

        /* renamed from: a */
        public static int m741c() {
            return 0;//(m739a() || m740b()) ? 1 : 0;
        }
    }

    @NonNull
    /* renamed from: a */
    public static b m722a(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Point b = m726b(context);
        int i = b.x;
        int i2 = b.y;
        float f = displayMetrics.density;
        float min = Math.min(((float) i) / f, ((float) i2) / f);
        float f2 = f * 160.0f;
        float f3 = ((float) i) / f2;
        float f4 = ((float) i2) / f2;
        double sqrt = Math.sqrt((double) ((f4 * f4) + (f3 * f3)));
        if (m725a(context, sqrt)) {
            return com.yandex.metrica.b.TV;
        }
        if (sqrt >= 7.0d || min >= 600.0f) {
            return com.yandex.metrica.b.TABLET;
        }
        return com.yandex.metrica.b.PHONE;
    }

    /* renamed from: a */
    private static boolean m725a(Context context, double d) {
        return d >= 15.0d && !f403a.hasSystemFeature(context, "android.hardware.touchscreen");
    }

    @TargetApi(17)
    @NonNull
    @SuppressLint("WrongConstant")
    /* renamed from: B */
    public static Point m726b(Context var0) {
        WindowManager var1 = (WindowManager)var0.getSystemService("window");
        Display var2 = (Display)cx.a(new wo<WindowManager, Display>() {
            public Display a(WindowManager var1) throws Throwable {
                return var1.getDefaultDisplay();
            }
        }, var1, "getting display", "WindowManager");
        return (Point)cx.a(new wo<Display, Point>() {
            public Point a(Display var1) throws Throwable {
                int var2;
                int var3;
                if (VERSION.SDK_INT >= 17) {
                    DisplayMetrics var4 = new DisplayMetrics();
                    var1.getRealMetrics(var4);
                    var2 = var4.widthPixels;
                    var3 = var4.heightPixels;
                } else if (VERSION.SDK_INT >= 14) {
                    try {
                        Method var7 = Display.class.getMethod("getRawHeight");
                        Method var5 = Display.class.getMethod("getRawWidth");
                        var2 = (Integer)var5.invoke(var1);
                        var3 = (Integer)var7.invoke(var1);
                    } catch (Throwable var6) {
                        var2 = var1.getWidth();
                        var3 = var1.getHeight();
                    }
                } else {
                    var2 = var1.getWidth();
                    var3 = var1.getHeight();
                }

                return new Point(var2, var3);
            }
        }, var2, "getting display metrics", "Display", new Point(0, 0));
//        return (Point) cx.aaa(new wo<Display, Point>() {
//            /* renamed from: a */
//            public Point aaa(Display display) throws Throwable {
//                int width;
//                int height;
//                if (VERSION.SDK_INT >= 17) {
//                    DisplayMetrics displayMetrics = new DisplayMetrics();
//                    display.getRealMetrics(displayMetrics);
//                    width = displayMetrics.widthPixels;
//                    height = displayMetrics.heightPixels;
//                } else if (VERSION.SDK_INT >= 14) {
//                    try {
//                        Method method = Display.class.getMethod("getRawHeight", new Class[0]);
//                        width = ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(display, new Object[0])).intValue();
//                        height = ((Integer) method.invoke(display, new Object[0])).intValue();
//                    } catch (Throwable th) {
//                        width = display.getWidth();
//                        height = display.getHeight();
//                    }
//                } else {
//                    width = display.getWidth();
//                    height = display.getHeight();
//                }
//                return new Point(width, height);
//            }
//        }, (Display) cx.aaa((wo<D, S>) new wo<WindowManager, Display>() {
//            /* renamed from: a */
//            public Display aaa(WindowManager windowManager) throws Throwable {
//                return windowManager.getDefaultDisplay();
//            }
//        }, (WindowManager) context.getSystemService("window"), "getting display", "WindowManager"), "getting display metrics", "Display", new Point(0, 0));
    }

    /* renamed from: a */
    public static Integer m729c(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("level", -1);
        int intExtra2 = registerReceiver.getIntExtra("scale", -1);
        if (intExtra <= -1 || intExtra2 <= 0) {
            return null;
        }
        return Integer.valueOf(Math.round((((float) intExtra) / ((float) intExtra2)) * 100.0f));
    }

    @NonNull
    /* renamed from: a */
    public static String m724a(@NonNull Locale locale) {
        String language = locale.getLanguage();
        String country = locale.getCountry();
        StringBuilder sb = new StringBuilder(language);
        if (cx.a(21)) {
            String script = locale.getScript();
            if (!TextUtils.isEmpty(script)) {
                sb.append('-').append(script);
            }
        }
        if (!TextUtils.isEmpty(country)) {
            sb.append('_').append(country);
        }
        return sb.toString();
    }

    @NonNull
    /* renamed from: d */
    @SuppressLint("WrongConstant")
    public static a m730d(@NonNull Context context) {
        return (a) cx.a(new wo<ConnectivityManager, a>() {
            /* renamed from: a */
            public a a(ConnectivityManager connectivityManager) throws Throwable {
                if (cx.a(23)) {
                    return bt.m728c(connectivityManager);
                }
                return bt.m731d(connectivityManager);
            }
        }, (ConnectivityManager) context.getSystemService("connectivity"), "getting connection type", "ConnectivityManager", a.UNDEFINED);
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    @NonNull
    @SuppressLint("MissingPermission")
    /* renamed from: a */
    public static a m728c(@NonNull ConnectivityManager connectivityManager) {
        a aVar = a.UNDEFINED;
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            return a.OFFLINE;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(activeNetwork);
        if (networkInfo != null && !networkInfo.isConnected()) {
            return a.OFFLINE;
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
        if (networkCapabilities != null) {
            for (Integer num : f405c.mo2509a()) {
                if (networkCapabilities.hasTransport(num.intValue())) {
                    return (a) f405c.mo2508a(num);
                }
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    @NonNull
    @SuppressLint("MissingPermission")
    /* renamed from: d */
    public static a m731d(@NonNull ConnectivityManager connectivityManager) {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return a.OFFLINE;
        }
        return (a) f404b.mo2508a(Integer.valueOf(activeNetworkInfo.getType()));
    }

    /* renamed from: e */
    public static int m732e(@NonNull Context context) {
        return m721a(m730d(context));
    }

    @VisibleForTesting
    /* renamed from: a */
    static int m721a(@Nullable a aVar) {
        return ((Integer) f406d.mo2508a(aVar)).intValue();
    }
}
