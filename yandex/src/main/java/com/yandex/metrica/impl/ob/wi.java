package com.yandex.metrica.impl.ob;

import android.os.SystemClock;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.util.GregorianCalendar;

/* renamed from: com.yandex.metrica.impl.ob.wi */
public final class wi {
    /* renamed from: a */
    public static final long m4359a() {
        return System.currentTimeMillis() - SystemClock.elapsedRealtime();
    }

    /* renamed from: B */
    public static long m4360b() {
        return System.currentTimeMillis() / 1000;
    }

    /* renamed from: a */
    public static long m4361c() {
        return wd.m4332a().mo2557b();
    }

    /* renamed from: a */
    public static int m4358a(long j) {
        return ((GregorianCalendar) GregorianCalendar.getInstance()).getTimeZone().getOffset(1000 * j) / YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_IN_DATABASE_COUNT;
    }

    /* renamed from: d */
    public static long m4362d() {
        return SystemClock.elapsedRealtime() / 1000;
    }
}
