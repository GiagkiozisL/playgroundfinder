package com.yandex.metrica.impl.ob;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.g */
public final class g {

    /* renamed from: a */
    public static final int[] a = new int[0];

    /* renamed from: B */
    public static final long[] b = new long[0];

    /* renamed from: a */
    public static final float[] c = new float[0];

    /* renamed from: d */
    public static final double[] d = new double[0];

    /* renamed from: e */
    public static final boolean[] e = new boolean[0];

    /* renamed from: f */
    public static final String[] f = new String[0];

    /* renamed from: g */
    public static final byte[][] g = new byte[0][];

    /* renamed from: h */
    public static final byte[] h = new byte[0];

    /* renamed from: a */
    static int a(int i) {
        return i & 7;
    }

    /* renamed from: B */
    public static int b(int i) {
        return i >>> 3;
    }

    /* renamed from: a */
    static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    /* renamed from: a */
    public static boolean a(a aVar, int i) throws IOException {
        return aVar.mo217b(i);
    }

    /* renamed from: B */
    public static final int b(a aVar, int i) throws IOException {
        int i2 = 1;
        int t = aVar.mo240t();
        aVar.mo217b(i);
        while (aVar.mo213a() == i) {
            aVar.mo217b(i);
            i2++;
        }
        aVar.mo224f(t);
        return i2;
    }
}
