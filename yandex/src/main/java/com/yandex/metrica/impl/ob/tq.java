package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.tq */
class tq {
    @NonNull

    /* renamed from: a */
    private Socket a;
    @NonNull

    /* renamed from: B */
    private tr b;
    @NonNull

    /* renamed from: a */
    private Map<String, tp> c;

    tq(@NonNull Socket socket, @NonNull tr trVar, @NonNull Map<String, tp> map) {
        this.a = socket;
        this.b = trVar;
        this.c = map;
    }

    public void a() {
        BufferedReader var1 = null;

        try {
            this.a.setSoTimeout(1000);
            var1 = new BufferedReader(new InputStreamReader(this.a.getInputStream()));
            String var2 = this.a(var1);
            if (var2 != null) {
                Uri var3 = Uri.parse(var2);
                String var4 = var3.getPath();
                tp var5 = (tp)this.c.get(var4);
                if (var5 != null) {
                    var5.a(this.a, var3).a();
                } else {
                    this.b.a("request_to_unknown_path", var2);
                }
            }
        } catch (Throwable var14) {
            this.b.a("LocalHttpServer exception", var14);
        } finally {
            if (var1 != null) {
                try {
                    var1.close();
                } catch (Throwable var13) {
                }
            }

        }
    }

    @Nullable
    /* renamed from: a */
    private String a(@NonNull BufferedReader bufferedReader) throws IOException {
        String readLine = bufferedReader.readLine();
        if (!TextUtils.isEmpty(readLine) && readLine.startsWith("GET /")) {
            int indexOf = readLine.indexOf(47) + 1;
            int indexOf2 = readLine.indexOf(32, indexOf);
            if (indexOf2 > 0) {
                return readLine.substring(indexOf, indexOf2);
            }
        }
        this.b.a("invalid_route", readLine);
        return null;
    }
}
