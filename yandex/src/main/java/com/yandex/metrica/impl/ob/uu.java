package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrength;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.uu */
public class uu {
    @Nullable

    /* renamed from: a */
    private Integer f2816a;
    @Nullable

    /* renamed from: B */
    private final Integer f2817b;
    @Nullable

    /* renamed from: a */
    private final Integer f2818c;
    @Nullable

    /* renamed from: d */
    private final Integer f2819d;
    @Nullable

    /* renamed from: e */
    private final Integer f2820e;
    @Nullable

    /* renamed from: f */
    private final String f2821f;
    @Nullable

    /* renamed from: g */
    private final String f2822g;

    /* renamed from: h */
    private final boolean f2823h;

    /* renamed from: i */
    private final int f2824i;
    @Nullable

    /* renamed from: j */
    private final Integer f2825j;
    @Nullable

    /* renamed from: k */
    private final Long f2826k;

    @TargetApi(17)
    /* renamed from: com.yandex.metrica.impl.ob.uu$a */
    static class C1088a extends C1089b {
        C1088a() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public uu mo2429a(CellInfo cellInfo) {
            CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
            return mo2430a(null, null, cellInfoCdma.getCellSignalStrength(), null, null, cellInfoCdma.isRegistered(), 2, null, cellInfoCdma.getTimeStamp());
        }
    }

    @TargetApi(17)
    /* renamed from: com.yandex.metrica.impl.ob.uu$B */
    static abstract class C1089b {

        /* renamed from: a */
        static final Integer f2827a = Integer.valueOf(Integer.MAX_VALUE);

        /* renamed from: B */
        static final Integer f2828b = Integer.valueOf(Integer.MAX_VALUE);

        /* renamed from: a */
        static final Integer f2829c = Integer.valueOf(Integer.MAX_VALUE);

        /* renamed from: d */
        static final Integer f2830d = Integer.valueOf(Integer.MAX_VALUE);

        /* renamed from: e */
        static final Integer f2831e = Integer.valueOf(Integer.MAX_VALUE);
        @NonNull

        /* renamed from: f */
        private final wf f2832f;

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public abstract uu mo2429a(CellInfo cellInfo);

        public C1089b() {
            this(new wf());
        }

        /* access modifiers changed from: protected */
        @TargetApi(17)
        /* renamed from: a */
        public uu mo2430a(@Nullable Integer num, @Nullable Integer num2, @Nullable CellSignalStrength cellSignalStrength, @Nullable Integer num3, @Nullable Integer num4, boolean z, int i, @Nullable Integer num5, long j) {
            Integer num6 = null;
            if (num != null) {
                if (num == f2827a) {
                    num = null;
                }
                num6 = num;
            }
            Integer num7 = null;
            if (num2 != null) {
                if (num2 == f2828b) {
                    num2 = null;
                }
                num7 = num2;
            }
            Integer num8 = cellSignalStrength != null ? Integer.valueOf(cellSignalStrength.getDbm()) : null;
            Integer num9 = null;
            if (num4 != null) {
                if (num4 == f2829c) {
                    num4 = null;
                }
                num9 = num4;
            }
            Integer num10 = null;
            if (num3 != null) {
                if (num3 == f2830d) {
                    num3 = null;
                }
                num10 = num3;
            }
            return new uu(num9, num10, num7, num6, null, null, num8, z, i, (num5 == null || num5 == f2831e) ? null : num5, m4055a(j));
        }

        @Nullable
        /* renamed from: a */
        private Long m4055a(long j) {
            Long l = null;
            if (j <= 0) {
                return null;
            }
            long d = this.f2832f.mo2563d(j, TimeUnit.NANOSECONDS);
            if (d > 0 && d < TimeUnit.HOURS.toSeconds(1)) {
                l = Long.valueOf(d);
            }
            if (l != null) {
                return l;
            }
            long a = this.f2832f.mo2560a(j, TimeUnit.NANOSECONDS);
            if (a <= 0 || a >= TimeUnit.HOURS.toSeconds(1)) {
                return l;
            }
            return Long.valueOf(a);
        }

        @VisibleForTesting
        public C1089b(@NonNull wf wfVar) {
            this.f2832f = wfVar;
        }
    }

    @TargetApi(17)
    /* renamed from: com.yandex.metrica.impl.ob.uu$a */
    static class C1090c extends C1089b {
        C1090c() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public uu mo2429a(CellInfo cellInfo) {
            CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
            CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
            return mo2430a(Integer.valueOf(cellIdentity.getCid()), Integer.valueOf(cellIdentity.getLac()), cellInfoGsm.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoGsm.isRegistered(), 1, null, cellInfoGsm.getTimeStamp());
        }
    }

    @TargetApi(17)
    /* renamed from: com.yandex.metrica.impl.ob.uu$d */
    static class C1091d extends C1089b {
        C1091d() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public uu mo2429a(CellInfo cellInfo) {
            CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
            CellIdentityLte cellIdentity = cellInfoLte.getCellIdentity();
            return mo2430a(Integer.valueOf(cellIdentity.getCi()), Integer.valueOf(cellIdentity.getTac()), cellInfoLte.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoLte.isRegistered(), 4, Integer.valueOf(cellIdentity.getPci()), cellInfoLte.getTimeStamp());
        }
    }

    @TargetApi(18)
    /* renamed from: com.yandex.metrica.impl.ob.uu$e */
    static class C1092e extends C1089b {
        C1092e() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public uu mo2429a(CellInfo cellInfo) {
            CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
            CellIdentityWcdma cellIdentity = cellInfoWcdma.getCellIdentity();
            return mo2430a(Integer.valueOf(cellIdentity.getCid()), Integer.valueOf(cellIdentity.getLac()), cellInfoWcdma.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoWcdma.isRegistered(), 3, Integer.valueOf(cellIdentity.getPsc()), cellInfoWcdma.getTimeStamp());
        }
    }

    public uu(@Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Integer num4, @Nullable String str, @Nullable String str2, @Nullable Integer num5, boolean z, int i, @Nullable Integer num6, @Nullable Long l) {
        this.f2817b = num;
        this.f2818c = num2;
        this.f2819d = num3;
        this.f2820e = num4;
        this.f2821f = str;
        this.f2822g = str2;
        this.f2816a = num5;
        this.f2823h = z;
        this.f2824i = i;
        this.f2825j = num6;
        this.f2826k = l;
    }

    @TargetApi(17)
    /* renamed from: a */
    public static uu m4040a(CellInfo cellInfo) {
        C1089b b = m4041b(cellInfo);
        if (b == null) {
            return null;
        }
        return b.mo2429a(cellInfo);
    }

    @TargetApi(17)
    /* renamed from: B */
    public static C1089b m4041b(CellInfo cellInfo) {
        if (cellInfo instanceof CellInfoGsm) {
            return new C1090c();
        }
        if (cellInfo instanceof CellInfoCdma) {
            return new C1088a();
        }
        if (cellInfo instanceof CellInfoLte) {
            return new C1091d();
        }
        if (!cx.a(18) || !(cellInfo instanceof CellInfoWcdma)) {
            return null;
        }
        return new C1092e();
    }

    @Nullable
    /* renamed from: a */
    public Integer mo2416a() {
        return this.f2816a;
    }

    @Nullable
    /* renamed from: B */
    public Integer mo2418b() {
        return this.f2817b;
    }

    @Nullable
    /* renamed from: a */
    public Integer mo2419c() {
        return this.f2818c;
    }

    @Nullable
    /* renamed from: d */
    public Integer mo2420d() {
        return this.f2819d;
    }

    @Nullable
    /* renamed from: e */
    public Integer mo2421e() {
        return this.f2820e;
    }

    @Nullable
    /* renamed from: f */
    public String mo2422f() {
        return this.f2821f;
    }

    @Nullable
    /* renamed from: g */
    public String mo2423g() {
        return this.f2822g;
    }

    /* renamed from: h */
    public boolean mo2424h() {
        return this.f2823h;
    }

    /* renamed from: a */
    public void mo2417a(@Nullable Integer num) {
        this.f2816a = num;
    }

    /* renamed from: i */
    public int mo2425i() {
        return this.f2824i;
    }

    @Nullable
    /* renamed from: j */
    public Integer mo2426j() {
        return this.f2825j;
    }

    @Nullable
    /* renamed from: k */
    public Long mo2427k() {
        return this.f2826k;
    }

    public String toString() {
        return "CellDescription{mSignalStrength=" + this.f2816a + ", mMobileCountryCode=" + this.f2817b + ", mMobileNetworkCode=" + this.f2818c + ", mLocationAreaCode=" + this.f2819d + ", mCellId=" + this.f2820e + ", mOperatorName='" + this.f2821f + '\'' + ", mNetworkType='" + this.f2822g + '\'' + ", mConnected=" + this.f2823h + ", mCellType=" + this.f2824i + ", mPci=" + this.f2825j + ", mLastVisibleTimeOffset=" + this.f2826k + '}';
    }
}
