package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.i.a;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.lw */
public class lw extends lx {

    /* renamed from: a */
    public static final String f1382a = null;
    @Deprecated

    /* renamed from: B */
    public static final qk f1383b = new qk("COLLECT_INSTALLED_APPS");

    /* renamed from: a */
    static final qk f1384c = new qk("DEPRECATED_NATIVE_CRASHES_CHECKED");

    /* renamed from: d */
    private static final qk f1385d = new qk("IDENTITY_SEND_TIME");

    /* renamed from: e */
    private static final qk f1386e = new qk("PERMISSIONS_CHECK_TIME");

    /* renamed from: f */
    private static final qk f1387f = new qk("USER_INFO");

    /* renamed from: g */
    private static final qk f1388g = new qk("PROFILE_ID");

    /* renamed from: h */
    private static final qk f1389h = new qk("APP_ENVIRONMENT");

    /* renamed from: i */
    private static final qk f1390i = new qk("APP_ENVIRONMENT_REVISION");

    /* renamed from: j */
    private static final qk f1391j = new qk("LAST_MIGRATION_VERSION");

    /* renamed from: k */
    private static final qk f1392k = new qk("LAST_APP_VERSION_WITH_FEATURES");

    /* renamed from: l */
    private static final qk f1393l = new qk("APPLICATION_FEATURES");

    /* renamed from: m */
    private static final qk f1394m = new qk("CURRENT_SESSION_ID");

    /* renamed from: n */
    private static final qk f1395n = new qk("ATTRIBUTION_ID");

    /* renamed from: o */
    private static final qk f1396o = new qk("LAST_STAT_SENDING_DISABLED_REPORTING_TIMESTAMP");

    /* renamed from: p */
    private static final qk f1397p = new qk("NEXT_EVENT_GLOBAL_NUMBER");

    /* renamed from: r */
    private static final qk f1398r = new qk("LAST_REQUEST_ID");

    /* renamed from: s */
    private static final qk f1399s = new qk("CERTIFICATES_SHA1_FINGERPRINTS");

    /* renamed from: t */
    private static final lt f1400t = new lt();

    public lw(lf lfVar) {
        super(lfVar);
    }

    /* renamed from: a */
    public int mo1316a() {
        return mo1355b(f1397p.mo1744b(), 0);
    }

    /* renamed from: a */
    public int mo1317a(int i) {
        return mo1355b(f1400t.mo1274a(i), 0);
    }

    /* renamed from: a */
    public long mo1318a(long j) {
        return mo1356b(f1385d.mo1744b(), j);
    }

    /* renamed from: B */
    public long mo1324b() {
        return mo1356b(f1386e.mo1744b(), 0);
    }

    /* renamed from: a */
    public int mo1328c() {
        return mo1355b(f1392k.mo1744b(), -1);
    }

    /* renamed from: d */
    public a mo1332d() {
        a aVar;
        synchronized (this) {
            aVar = new a(mo1361c(f1389h.mo1744b(), "{}"), mo1356b(f1390i.mo1744b(), 0));
        }
        return aVar;
    }

    /* renamed from: e */
    public String mo1338e() {
        return mo1361c(f1393l.mo1744b(), "");
    }

    /* renamed from: f */
    public String mo1340f() {
        return mo1361c(f1387f.mo1744b(), f1382a);
    }

    /* renamed from: B */
    public lw mo1325b(int i) {
        return (lw) mo1350a(f1397p.mo1744b(), i);
    }

    /* renamed from: a */
    public lw mo1319a(int i, int i2) {
        return (lw) mo1350a(f1400t.mo1274a(i), i2);
    }

    /* renamed from: a */
    public lw mo1320a(a aVar) {
        synchronized (this) {
            mo1357b(f1389h.mo1744b(), aVar.a);
            mo1351a(f1390i.mo1744b(), aVar.f1031b);
        }
        return this;
    }

    /* renamed from: B */
    public lw mo1326b(long j) {
        return (lw) mo1351a(f1385d.mo1744b(), j);
    }

    /* renamed from: a */
    public lw mo1330c(long j) {
        return (lw) mo1351a(f1386e.mo1744b(), j);
    }

    /* renamed from: a */
    public lw mo1321a(String str) {
        return (lw) mo1357b(f1387f.mo1744b(), str);
    }

    /* renamed from: g */
    public long mo1341g() {
        return mo1356b(f1391j.mo1744b(), 0);
    }

    /* renamed from: d */
    public lw mo1334d(long j) {
        return (lw) mo1351a(f1391j.mo1744b(), j);
    }

    /* renamed from: a */
    public lw mo1329c(int i) {
        return (lw) mo1350a(f1392k.mo1744b(), i);
    }

    /* renamed from: B */
    public lw mo1327b(String str) {
        return (lw) mo1357b(f1393l.mo1744b(), str);
    }

    /* renamed from: a */
    public lw mo1322a(String str, String str2) {
        return (lw) mo1357b(new qk("SESSION_", str).mo1744b(), str2);
    }

    /* renamed from: a */
    public String mo1331c(String str) {
        return mo1361c(new qk("SESSION_", str).mo1744b(), "");
    }

    @Nullable
    /* renamed from: h */
    public String mo1342h() {
        return mo1366s(f1388g.mo1744b());
    }

    /* renamed from: d */
    public lw mo1335d(@Nullable String str) {
        return (lw) mo1357b(f1388g.mo1744b(), str);
    }

    @NonNull
    /* renamed from: d */
    public lw mo1333d(int i) {
        return (lw) mo1350a(f1395n.mo1744b(), i);
    }

    /* renamed from: i */
    public int mo1343i() {
        return mo1355b(f1395n.mo1744b(), 1);
    }

    /* renamed from: j */
    public long mo1344j() {
        return mo1356b(f1394m.mo1744b(), -1);
    }

    @NonNull
    /* renamed from: e */
    public lw mo1337e(long j) {
        return (lw) mo1351a(f1394m.mo1744b(), j);
    }

    /* renamed from: k */
    public long mo1345k() {
        return mo1356b(f1396o.mo1744b(), 0);
    }

    /* renamed from: f */
    public lw mo1339f(long j) {
        return (lw) mo1351a(f1396o.mo1744b(), j);
    }

    /* renamed from: l */
    public int mo1346l() {
        return mo1355b(f1398r.mo1744b(), -1);
    }

    /* renamed from: e */
    public lw mo1336e(int i) {
        return (lw) mo1350a(f1398r.mo1744b(), i);
    }

    /* renamed from: m */
    public boolean mo1347m() {
        return mo1359b(f1384c.mo1744b(), false);
    }

    /* renamed from: n */
    public lw mo1348n() {
        return (lw) mo1353a(f1384c.mo1744b(), true);
    }

    @NonNull
    /* renamed from: o */
    public List<String> mo1349o() {
        return mo1358b(f1399s.mo1744b(), new ArrayList<String>());// Collections.emptyList());
    }

    /* renamed from: a */
    public lw mo1323a(List<String> list) {
        return (lw) mo1352a(f1399s.mo1744b(), list);
    }
}
