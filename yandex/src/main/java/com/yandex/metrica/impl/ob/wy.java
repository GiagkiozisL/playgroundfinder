package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.af.C0058a;

/* renamed from: com.yandex.metrica.impl.ob.wy */
public class wy {
    @NonNull

    /* renamed from: a */
    private final vw<wz, wx> f2951a;
    @NonNull

    /* renamed from: B */
    private final vw<C0058a, wx> f2952b;

    public wy(@NonNull Context context) {
        this(new wv(), new xa(), new ws(context));
    }

    public wy(@NonNull wx wxVar, @NonNull wx wxVar2, @NonNull wx wxVar3) {
        this.f2951a = new vw<>(wxVar);
        this.f2951a.mo2510a(wz.NONE, wxVar);
        this.f2951a.mo2510a(wz.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER, wxVar2);
        this.f2951a.mo2510a(wz.AES_VALUE_ENCRYPTION, wxVar3);
        this.f2952b = new vw<>(wxVar);
        this.f2952b.mo2510a(C0058a.EVENT_TYPE_IDENTITY, wxVar3);
    }

    @NonNull
    /* renamed from: a */
    public wx mo2591a(wz wzVar) {
        return (wx) this.f2951a.mo2508a(wzVar);
    }

    @NonNull
    /* renamed from: a */
    public wx mo2590a(@NonNull w wVar) {
        return m4403a(C0058a.m298a(wVar.mo2533g()));
    }

    @NonNull
    /* renamed from: a */
    private wx m4403a(@NonNull C0058a aVar) {
        return (wx) this.f2952b.mo2508a(aVar);
    }
}
