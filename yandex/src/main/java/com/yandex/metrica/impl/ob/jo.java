package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.jo */
public class jo {
    @NonNull

    /* renamed from: a */
    private final C0505c f1157a;

    @TargetApi(26)
    /* renamed from: com.yandex.metrica.impl.ob.jo$a */
    static class C0503a implements C0505c {
        @NonNull

        /* renamed from: a */
        private final jl f1158a;

        public C0503a(@NonNull Context context) {
            this.f1158a = new jl(context);
        }

        @NonNull
        /* renamed from: a */
        public jm mo1117a() {
            return this.f1158a;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.jo$B */
    static class C0504b implements C0505c {
        @NonNull

        /* renamed from: a */
        private final jn f1159a;

        public C0504b(@NonNull Context context) {
            this.f1159a = new jn(context);
        }

        @NonNull
        /* renamed from: a */
        public jm mo1117a() {
            return this.f1159a;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.jo$a */
    interface C0505c {
        @NonNull
        /* renamed from: a */
        jm mo1117a();
    }

    public jo(@NonNull Context context) {
        this(VERSION.SDK_INT >= 26 ? new C0503a(context) : new C0504b(context));
    }

    jo(@NonNull C0505c cVar) {
        this.f1157a = cVar;
    }

    /* renamed from: a */
    public jm mo1116a() {
        return this.f1157a.mo1117a();
    }
}
