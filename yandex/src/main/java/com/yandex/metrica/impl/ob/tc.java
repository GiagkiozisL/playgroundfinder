package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.np.C0678a;
import com.yandex.metrica.impl.ob.pj.a;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.tc */
public class tc {
    @NonNull

    /* renamed from: a */
    private xh f2534a;
    @NonNull

    /* renamed from: B */
    private final mf<te> f2535b;
    @NonNull

    /* renamed from: a */
    private C1003a f2536c;
    @NonNull

    /* renamed from: d */
    private mp f2537d;
    @NonNull

    /* renamed from: e */
    private final C1004b f2538e;
    @NonNull

    /* renamed from: f */
    private final wg f2539f;
    @NonNull

    /* renamed from: g */
    private final pi f2540g;
    /* access modifiers changed from: private */
    @Nullable

    /* renamed from: h */
    public String f2541h;

    /* renamed from: com.yandex.metrica.impl.ob.tc$a */
    public static class C1003a {
        @NonNull

        /* renamed from: a */
        private final ti f2544a;

        public C1003a() {
            this(new ti());
        }

        @VisibleForTesting
        C1003a(@NonNull ti tiVar) {
            this.f2544a = tiVar;
        }

        @NonNull
        /* renamed from: a */
        public List<th> mo2227a(@Nullable byte[] bArr) {
            ArrayList arrayList = new ArrayList();
            if (cx.nullOrEmpty(bArr)) {
                return arrayList;
            }
            try {
                return this.f2544a.mo2239a(new String(bArr, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                return arrayList;
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.tc$B */
    static class C1004b {
        C1004b() {
        }

        @Nullable
        /* renamed from: a */
        public HttpURLConnection mo2228a(@NonNull String str, @NonNull String str2) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                if (!TextUtils.isEmpty(str)) {
                    httpURLConnection.setRequestProperty("If-None-Match", str);
                }
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setConnectTimeout(a.a);
                httpURLConnection.setReadTimeout(a.a);
                httpURLConnection.connect();
                return httpURLConnection;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    public tc(@NonNull Context context, @Nullable String str, @NonNull xh xhVar) {
        this(str, C0678a.m2599a(te.class).a(context), new C1003a(), new C1004b(), xhVar, new mp(), new wg(), new pi(context));
    }

    @VisibleForTesting
    tc(@Nullable String str, @NonNull mf mfVar, @NonNull C1003a aVar, @NonNull C1004b bVar, @NonNull xh xhVar, @NonNull mp mpVar, @NonNull wg wgVar, @NonNull pi piVar) {
        this.f2541h = str;
        this.f2535b = mfVar;
        this.f2536c = aVar;
        this.f2538e = bVar;
        this.f2534a = xhVar;
        this.f2537d = mpVar;
        this.f2539f = wgVar;
        this.f2540g = piVar;
    }

    /* renamed from: a */
    public void mo2223a(@NonNull final tb tbVar) {
        this.f2534a.a((Runnable) new Runnable() {
            public void run() {
                tc.this.m3754a(tbVar, tc.this.f2541h);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3754a(@NonNull tb tbVar, String str) {
        te teVar;
        te teVar2 = null;
        te teVar3 = (te) this.f2535b.a();
        if (this.f2540g.mo1636a() && str != null) {
            try {
                HttpURLConnection a = this.f2538e.mo2228a(teVar3.f2565b, str);
                if (a != null) {
                    teVar = mo2222a(a, teVar3);
                } else {
                    teVar = null;
                }
                teVar2 = teVar;
            } catch (Throwable th) {
            }
        }
        if (teVar2 != null) {
            tbVar.mo2220a(teVar2);
        } else {
            tbVar.mo2219a();
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: a */
    public te mo2222a(@NonNull HttpURLConnection httpURLConnection, @NonNull te teVar) throws IOException {
        switch (httpURLConnection.getResponseCode()) {
            case 200:
                try {
                    return new te(this.f2536c.mo2227a(this.f2537d.mo1435a(am.b(httpURLConnection.getInputStream()), "af9202nao18gswqp")), cu.m1151a(httpURLConnection.getHeaderField("ETag")), this.f2539f.a(), true, false);
                } catch (IOException e) {
                    return null;
                }
            case 304:
                return new te(teVar.f2564a, teVar.f2565b, this.f2539f.a(), true, false);
            default:
                return null;
        }
    }

    /* renamed from: a */
    public void mo2224a(@Nullable uk ukVar) {
        if (ukVar != null) {
            this.f2541h = ukVar.sdkListUrl;
        }
    }

    /* renamed from: B */
    public boolean mo2225b(@NonNull uk ukVar) {
        if (this.f2541h == null) {
            if (ukVar.sdkListUrl != null) {
                return true;
            }
            return false;
        } else if (this.f2541h.equals(ukVar.sdkListUrl)) {
            return false;
        } else {
            return true;
        }
    }
}
