package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.fr */
public class fr {
    @NonNull
    /* renamed from: a */
    public fq mo921a(@NonNull fn fnVar) {
        switch (fnVar.mo911e()) {
            case COMMUTATION:
                return new fx();
            case MAIN:
                return new fv();
            case SELF_DIAGNOSTIC_MAIN:
                return new gc();
            case SELF_DIAGNOSTIC_MANUAL:
                return new gd();
            case MANUAL:
                return new fz();
            case APPMETRICA:
                return new fm();
            default:
                return new fv();
        }
    }
}
