package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.vd */
public class vd extends ut {

    /* renamed from: a */
    private int f2895a;

    /* renamed from: B */
    private ut f2896b;

    public vd(@NonNull Context context, @NonNull xh xhVar) {
        this(context.getApplicationContext(), new MyPackageManager(), xhVar);
    }

    @VisibleForTesting
    vd(Context context, @NonNull MyPackageManager myPackageManagerVar, @NonNull xh xhVar) {
        if (myPackageManagerVar.hasSystemFeature(context, "android.hardware.telephony")) {
            this.f2896b = new ux(context, xhVar);
        } else {
            this.f2896b = new uy();
        }
    }

    /* renamed from: a */
    public synchronized void a() {
        this.f2895a++;
        if (this.f2895a == 1) {
            this.f2896b.a();
        }
    }

    /* renamed from: B */
    public synchronized void b() {
        this.f2895a--;
        if (this.f2895a == 0) {
            this.f2896b.b();
        }
    }

    /* renamed from: a */
    public synchronized void mo2414a(vg vgVar) {
        this.f2896b.mo2414a(vgVar);
    }

    /* renamed from: a */
    public synchronized void mo2413a(uv uvVar) {
        this.f2896b.mo2413a(uvVar);
    }

    /* renamed from: a */
    public void mo2415a(boolean z) {
        this.f2896b.mo2415a(z);
    }

    /* renamed from: a */
    public void mo2412a(@NonNull uk ukVar) {
        this.f2896b.mo2412a(ukVar);
    }
}
