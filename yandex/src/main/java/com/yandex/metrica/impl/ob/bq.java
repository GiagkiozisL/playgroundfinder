package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.bq */
public class bq {

    public void mo492a(@NonNull bo var1) {
        BufferedInputStream var2 = null;
        BufferedOutputStream var3 = null;
        HttpURLConnection var4 = null;
        OutputStream var5 = null;
        InputStream var6 = null;

        try {
            var1.d();
            var4 = var1.c().a();
            this.a(var4, var1.B());
            if (2 == var1.i()) {
                byte[] var7 = var1.j();
                if (null != var7 && var7.length > 0) {
                    this.a(var4, var1.z(), var1.A());
                    var5 = var4.getOutputStream();
                    var3 = new BufferedOutputStream(var5, var7.length);
                    var3.write(var1.j());
                    var3.flush();
                    cx.a(var5);
                }
            }

            int var13 = var4.getResponseCode();
            var1.a(var13);
            var1.a(var4.getHeaderFields());
            if (var1.b(var13)) {
                var6 = var4.getInputStream();
                var2 = new BufferedInputStream(var6, 8000);
                var1.b(am.b(var2));
                cx.a(var6);
            } else {
                var2 = new BufferedInputStream(var4.getErrorStream(), 8000);
            }
        } catch (Throwable var11) {
            var1.a(var11);
        } finally {
            cx.a(var3);
            cx.a(var2);
            cx.a(var5);
            cx.a(var6);
            cx.a(var4);
        }
    }

    /* renamed from: a */
    private void a(@NonNull HttpURLConnection httpURLConnection, @NonNull Map<String, List<String>> map) {
        for (Entry entry : map.entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), TextUtils.join(",", (Iterable) entry.getValue()));
        }
    }

    @NonNull
    /* renamed from: a */
    private HttpURLConnection a(@NonNull HttpURLConnection httpURLConnection, @Nullable Long l, @Nullable Integer num) {
        httpURLConnection.setDoOutput(true);
        if (l != null) {
            httpURLConnection.setRequestProperty("Send-Timestamp", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(l.longValue())));
        }
        if (num != null) {
            httpURLConnection.setRequestProperty("Send-Timezone", String.valueOf(num));
        }
        return httpURLConnection;
    }
}
