package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;

import com.yandex.metrica.impl.ob.rr.a.C0863a;
import com.yandex.metrica.impl.ob.rr.a.C0863a.C0864a;
import com.yandex.metrica.impl.ob.rr.a.C0871c;
import com.yandex.metrica.impl.ob.rr.a.C0872d;
import com.yandex.metrica.impl.ob.rr.a.C0873e;
import com.yandex.metrica.impl.ob.rr.a.C0875g;
import com.yandex.metrica.impl.ob.rr.a.C0878i;
import com.yandex.metrica.impl.ob.rr.a.C0879j;
import com.yandex.metrica.impl.ob.rr.a.C0880k;
import com.yandex.metrica.impl.ob.tv.C1053a;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.cq */
public class cq {

    /* renamed from: a */
    private static final Map<String, a.a_enum> f583a = Collections.unmodifiableMap(new HashMap<String, a.a_enum>() {
        {
            put("wifi", cq.a.a_enum.WIFI);
            put("cell", cq.a.a_enum.CELL);
        }
    });
    @NonNull

    /* renamed from: B */
    private final co f584b;

    /* renamed from: com.yandex.metrica.impl.ob.cq$a */
    public static class a {
        @Nullable

        /* renamed from: a */
        public final String a;
        @Nullable

        /* renamed from: B */
        public final String b;
        @Nullable

        /* renamed from: a */
        public final String c;
        @NonNull

        /* renamed from: d */
        public final List<Pair<String, String>> d;
        @Nullable

        /* renamed from: e */
        public final Long e;
        @NonNull

        /* renamed from: f */
        public final List<a_enum> f;

        /* renamed from: com.yandex.metrica.impl.ob.cq$a$a */
        public enum a_enum {
            WIFI,
            CELL
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull List<Pair<String, String>> list, @Nullable Long l, @NonNull List<a_enum> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = l;
            this.f = list2;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.cq$B */
    public static class b {
        @NonNull

        /* renamed from: A */
        private tt f594A;

        /* renamed from: B */
        private List<pu> f595B = new ArrayList();

        /* renamed from: C */
        private boolean f596C;
        @Nullable

        /* renamed from: D */
        private ty f597D;

        /* renamed from: a */
        private C1053a f598a = new C1053a();

        /* renamed from: B */
        private C0229a f599b;

        /* renamed from: a */
        private boolean f600c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public boolean f601d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public boolean f602e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public boolean f603f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public boolean f604g;

        /* renamed from: h */
        private List<String> f605h;

        /* renamed from: i */
        private String f606i = "";

        /* renamed from: j */
        private List<String> f607j;

        /* renamed from: k */
        private String f608k = "";

        /* renamed from: l */
        private List<String> f609l;

        /* renamed from: m */
        private String f610m;

        /* renamed from: n */
        private String f611n;

        /* renamed from: o */
        private String f612o;

        /* renamed from: p */
        private String f613p;

        /* renamed from: q */
        private ub f614q = null;
        @Nullable

        /* renamed from: r */
        private tz f615r = null;

        /* renamed from: s */
        private oh f616s;

        /* renamed from: t */
        private oc f617t;

        /* renamed from: u */
        private Long f618u;

        /* renamed from: v */
        private List<a> f619v;

        /* renamed from: w */
        private String f620w;

        /* renamed from: x */
        private List<String> f621x;

        /* renamed from: y */
        private um f622y;
        @Nullable

        /* renamed from: z */
        private ua f623z;

        /* renamed from: com.yandex.metrica.impl.ob.cq$B$a */
        public enum C0229a {
            BAD,
            OK
        }

        /* renamed from: a */
        public tv mo620a() {
            return this.f598a.mo2305a();
        }

        /* renamed from: B */
        public C1053a mo623b() {
            return this.f598a;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1052a(boolean z) {
            this.f598a.mo2304a(z);
        }

        /* access modifiers changed from: private */
        /* renamed from: B */
        public void m1059b(boolean z) {
            this.f598a.mo2306b(z);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1066c(boolean z) {
            this.f601d = z;
        }

        /* access modifiers changed from: private */
        /* renamed from: d */
        public void m1073d(boolean z) {
            this.f602e = z;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo622a(@NonNull String str, boolean z) {
            this.f595B.add(new pu(str, z));
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1051a(List<String> list) {
            this.f605h = list;
        }

        /* renamed from: a */
        public List<String> mo624c() {
            return this.f605h;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1050a(@Nullable String str) {
            this.f610m = str;
        }

        @Nullable
        /* renamed from: d */
        public String mo625d() {
            return this.f610m;
        }

        /* access modifiers changed from: private */
        /* renamed from: B */
        public void m1057b(String str) {
            this.f606i = str;
        }

        /* renamed from: e */
        public String mo626e() {
            return this.f606i;
        }

        /* access modifiers changed from: private */
        /* renamed from: B */
        public void m1058b(List<String> list) {
            this.f607j = list;
        }

        /* renamed from: f */
        public List<String> mo627f() {
            return this.f607j;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1064c(String str) {
            this.f608k = str;
        }

        /* renamed from: g */
        public String mo628g() {
            return this.f608k;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1065c(List<String> list) {
            this.f609l = list;
        }

        /* renamed from: h */
        public List<String> mo629h() {
            return this.f609l;
        }

        /* access modifiers changed from: private */
        /* renamed from: d */
        public void m1071d(String str) {
            this.f611n = str;
        }

        /* renamed from: i */
        public String mo630i() {
            return this.f611n;
        }

        /* access modifiers changed from: private */
        /* renamed from: e */
        public void m1078e(String str) {
            this.f612o = str;
        }

        /* renamed from: j */
        public String mo631j() {
            return this.f612o;
        }

        /* access modifiers changed from: private */
        /* renamed from: f */
        public void m1083f(String str) {
            this.f613p = str;
        }

        /* renamed from: k */
        public String mo632k() {
            return this.f613p;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1029a(C0229a aVar) {
            this.f599b = aVar;
        }

        /* renamed from: l */
        public C0229a mo633l() {
            return this.f599b;
        }

        /* renamed from: m */
        public boolean mo634m() {
            return this.f600c;
        }

        /* access modifiers changed from: private */
        /* renamed from: e */
        public void m1080e(boolean z) {
            this.f600c = z;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1047a(ub ubVar) {
            this.f614q = ubVar;
        }

        /* renamed from: n */
        public ub mo635n() {
            return this.f614q;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1045a(@NonNull tz tzVar) {
            this.f615r = tzVar;
        }

        @Nullable
        /* renamed from: o */
        public tz mo636o() {
            return this.f615r;
        }

        @NonNull
        /* renamed from: p */
        public ua mo637p() {
            return this.f623z;
        }

        /* renamed from: q */
        public List<pu> mo638q() {
            return this.f595B;
        }

        /* renamed from: r */
        public oh mo639r() {
            return this.f616s;
        }

        /* renamed from: s */
        public oc mo640s() {
            return this.f617t;
        }

        /* access modifiers changed from: private */
        /* renamed from: f */
        public void m1084f(boolean z) {
            this.f598a.mo2307c(z);
        }

        /* access modifiers changed from: private */
        /* renamed from: g */
        public void m1088g(boolean z) {
            this.f598a.mo2308d(z);
        }

        /* access modifiers changed from: private */
        /* renamed from: h */
        public void m1090h(boolean z) {
            this.f598a.mo2309e(z);
        }

        /* access modifiers changed from: private */
        /* renamed from: i */
        public void m1092i(boolean z) {
            this.f598a.mo2310f(z);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1049a(Long l) {
            this.f618u = l;
        }

        /* renamed from: t */
        public Long mo641t() {
            return this.f618u;
        }

        /* access modifiers changed from: private */
        /* renamed from: d */
        public void m1072d(List<a> list) {
            this.f619v = list;
        }

        /* renamed from: u */
        public List<a> mo642u() {
            return this.f619v;
        }

        /* renamed from: v */
        public String mo643v() {
            return this.f620w;
        }

        /* access modifiers changed from: private */
        /* renamed from: g */
        public void m1087g(String str) {
            this.f620w = str;
        }

        /* renamed from: w */
        public List<String> mo644w() {
            return this.f621x;
        }

        /* access modifiers changed from: private */
        /* renamed from: e */
        public void m1079e(List<String> list) {
            this.f621x = list;
        }

        /* renamed from: x */
        public um mo645x() {
            return this.f622y;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1048a(um umVar) {
            this.f622y = umVar;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1046a(@NonNull ua uaVar) {
            this.f623z = uaVar;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1043a(@NonNull oh ohVar) {
            this.f616s = ohVar;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1042a(@NonNull oc ocVar) {
            this.f617t = ocVar;
        }

        /* access modifiers changed from: private */
        /* renamed from: j */
        public void m1094j(boolean z) {
            this.f603f = z;
        }

        /* access modifiers changed from: private */
        /* renamed from: k */
        public void m1096k(boolean z) {
            this.f604g = z;
        }

        /* access modifiers changed from: private */
        /* renamed from: l */
        public void m1098l(boolean z) {
            this.f596C = z;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1044a(@Nullable ty tyVar) {
            this.f597D = tyVar;
        }

        @Nullable
        /* renamed from: y */
        public ty mo646y() {
            return this.f597D;
        }

        /* renamed from: z */
        public boolean mo647z() {
            return this.f596C;
        }

        @NonNull
        /* renamed from: A */
        public tt mo619A() {
            return this.f594A;
        }

        /* renamed from: a */
        public void mo621a(@NonNull tt ttVar) {
            this.f594A = ttVar;
        }
    }

    public cq() {
        this(new co());
    }

    /* renamed from: a */
    private String m1002a(JSONObject jSONObject, String str) {
        try {
            return jSONObject.getJSONObject(str).getJSONArray("urls").getString(0);
        } catch (Throwable th) {
            return "";
        }
    }

    /* renamed from: B */
    private List<String> m1012b(JSONObject jSONObject, String str) {
        boolean z = false;
        try {
            return vq.m4217a(jSONObject.getJSONObject(str).getJSONArray("urls"));
        } catch (Throwable th) {
            return new ArrayList<>();
        }
    }

    /* renamed from: a */
    public b mo618a(byte[] bArr) {
        b bVar = new b();
        try {
//            com.yandex.metrica.impl.ob.vq.a aVar = new com.yandex.metrica.impl.ob.vq.a(new String(bArr, "UTF-8"));
            com.yandex.metrica.impl.ob.vq.a aVar = new com.yandex.metrica.impl.ob.vq.a(new String(bArr, "UTF-8"));
            m1019d(bVar, aVar);
            m1022g(bVar, aVar);
            m1026k(bVar, aVar);
            m1027l(bVar, aVar);
            m1021f(bVar, aVar);
            m1003a(bVar, aVar);
            m1013b(bVar, aVar);
            m1017c(bVar, aVar);
            if (bVar.mo620a().f2657c) {
                m1024i(bVar, aVar);
            }
            if (bVar.mo634m()) {
                m1025j(bVar, aVar);
            }
            if (bVar.mo647z()) {
                m1018c(bVar, (JSONObject) aVar);
            }
            bVar.m1043a(m999a(aVar.optJSONObject("foreground_location_collection"), bVar.f601d, bVar.f603f));
            bVar.m1042a(m1015c(aVar.optJSONObject("background_location_collection"), bVar.f602e, bVar.f604g));
            m1014b(bVar, (JSONObject) aVar);
            m1020e(bVar, aVar);
            m1023h(bVar, aVar);
            bVar.mo621a(this.f584b.mo617a((JSONObject) aVar));
            bVar.m1029a(b.C0229a.OK);
            return bVar;
        } catch (Throwable th) {
            b bVar2 = new b();
            bVar2.m1029a(b.C0229a.BAD);
            return bVar2;
        }
    }

    /* renamed from: d */
    private void m1019d(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        String str = "";
        String str2 = "";
        JSONObject optJSONObject = aVar.optJSONObject("device_id");
        if (optJSONObject != null) {
            str2 = optJSONObject.optString("hash");
            str = optJSONObject.optString("value");
        }
        bVar.m1071d(str);
        bVar.m1078e(str2);
    }

    /* renamed from: a */
    private static oh m999a(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        return new ms().a(m1010b(jSONObject, z, z2));
    }

    /* renamed from: B */
    private static C0873e m1010b(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        C0873e eVar = new C0873e();
        eVar.f2173b = wk.m4368a(vq.m4211a(jSONObject, "min_update_interval_seconds"), TimeUnit.SECONDS, eVar.f2173b);
        eVar.f2174c = wk.m4365a(vq.m4229d(jSONObject, "min_update_distance_meters"), eVar.f2174c);
        eVar.f2175d = wk.m4366a(vq.m4224b(jSONObject, "records_count_to_force_flush"), eVar.f2175d);
        eVar.f2176e = wk.m4366a(vq.m4224b(jSONObject, "max_records_count_in_batch"), eVar.f2176e);
        eVar.f2177f = wk.m4368a(vq.m4211a(jSONObject, "max_age_seconds_to_force_flush"), TimeUnit.SECONDS, eVar.f2177f);
        eVar.f2178g = wk.m4366a(vq.m4224b(jSONObject, "max_records_to_store_locally"), eVar.f2178g);
        eVar.f2179h = z;
        eVar.f2181j = wk.m4368a(vq.m4211a(jSONObject, "lbs_min_update_interval_seconds"), TimeUnit.SECONDS, eVar.f2181j);
        eVar.f2180i = z2;
        return eVar;
    }

    /* renamed from: a */
    private static oc m1015c(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        JSONArray optJSONArray;
        C0863a aVar = new C0863a();
        aVar.f2125b = m1010b(jSONObject, z, z2);
        aVar.f2126c = wk.m4368a(vq.m4211a(jSONObject, "collection_duration_seconds"), TimeUnit.SECONDS, aVar.f2126c);
        aVar.f2127d = wk.m4368a(vq.m4211a(jSONObject, "collection_interval_seconds"), TimeUnit.SECONDS, aVar.f2127d);
        aVar.f2128e = vq.m4222a(jSONObject, "aggressive_relaunch", aVar.f2128e);
        if (jSONObject == null) {
            optJSONArray = null;
        } else {
            optJSONArray = jSONObject.optJSONArray("collection_interval_ranges_seconds");
        }
        aVar.f2129f = m1009a(optJSONArray, aVar.f2129f);
        return new mn().a(aVar);
    }

    /* renamed from: a */
    private static C0864a[] m1009a(@Nullable JSONArray jSONArray, C0864a[] aVarArr) {
        if (jSONArray != null && jSONArray.length() > 0) {
            try {
                aVarArr = new C0864a[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    aVarArr[i] = new C0864a();
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    aVarArr[i].f2131b = TimeUnit.SECONDS.toMillis(jSONObject.getLong("min"));
                    aVarArr[i].f2132c = TimeUnit.SECONDS.toMillis(jSONObject.getLong("max"));
                }
            } catch (Throwable th) {
            }
        }
        return aVarArr;
    }

    /* renamed from: e */
    private static void m1020e(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        if (aVar.has("requests")) {
            JSONObject jSONObject = aVar.getJSONObject("requests");
            if (jSONObject.has("list")) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                ArrayList arrayList = new ArrayList(jSONArray.length());
                for (int i = 0; i < jSONArray.length(); i++) {
                    try {
                        arrayList.add(m998a(jSONArray.getJSONObject(i)));
                    } catch (Throwable th) {
                    }
                }
                if (!arrayList.isEmpty()) {
                    bVar.m1072d((List<a>) arrayList);
                }
            }
        }
    }

    @NonNull
    /* renamed from: a */
    private static a m998a(@NonNull JSONObject var0) throws JSONException {
        JSONObject jSONObject2 = var0.getJSONObject("headers");
        ArrayList arrayList = new ArrayList(jSONObject2.length());
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            JSONArray jSONArray = jSONObject2.getJSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(new Pair(str, jSONArray.getString(i)));
            }
        }

//        return new a(var0.optString("id", null), var0.optString("url", null), var0.optString("method", null), arrayList, Long.valueOf(var0.getLong("delay_seconds")), B(var0));
        return new cq.a(var0.optString("id", (String)null), var0.optString("url", (String)null), var0.optString("method", (String)null), arrayList, var0.getLong("delay_seconds"), b(var0));
    }

    @NonNull
    /* renamed from: B */
    private static List<a.a_enum> b(@NonNull JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has("accept_network_types")) {
            JSONArray jSONArray = jSONObject.getJSONArray("accept_network_types");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(f583a.get(jSONArray.getString(i)));
            }
        }
        return arrayList;
    }

    /* renamed from: f */
    private static void m1021f(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        C0871c cVar = new C0871c();
        C0873e eVar = new C0873e();
        JSONObject jSONObject = (JSONObject) aVar.mo2496a("features", new JSONObject());
        if (jSONObject.has("list")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("list");
            bVar.m1052a(m1008a(jSONObject2, "easy_collecting", cVar.f2156b));
            bVar.m1059b(m1008a(jSONObject2, "package_info", cVar.f2157c));
            bVar.m1080e(m1008a(jSONObject2, "socket", false));
            bVar.m1084f(m1008a(jSONObject2, "permissions_collecting", cVar.f2158d));
            bVar.m1088g(m1008a(jSONObject2, "features_collecting", cVar.f2159e));
            bVar.m1090h(m1008a(jSONObject2, "sdk_list", cVar.f2169o));
            bVar.m1066c(m1008a(jSONObject2, "foreground_location_collection", eVar.f2179h));
            bVar.m1094j(m1008a(jSONObject2, "foreground_lbs_collection", eVar.f2180i));
            bVar.m1073d(m1008a(jSONObject2, "background_location_collection", eVar.f2179h));
            bVar.m1096k(m1008a(jSONObject2, "background_lbs_collection", eVar.f2180i));
            bVar.m1098l(m1008a(jSONObject2, "identity_light_collecting", cVar.f2170p));
            bVar.m1092i(m1008a(jSONObject2, "ble_collecting", cVar.f2171q));
            bVar.mo623b().mo2311g(m1008a(jSONObject2, "android_id", cVar.f2160f)).mo2312h(m1008a(jSONObject2, "google_aid", cVar.f2161g)).mo2313i(m1008a(jSONObject2, "wifi_around", cVar.f2162h)).mo2314j(m1008a(jSONObject2, "wifi_connected", cVar.f2163i)).mo2315k(m1008a(jSONObject2, "own_macs", cVar.f2164j)).mo2316l(m1008a(jSONObject2, "cells_around", cVar.f2165k)).mo2317m(m1008a(jSONObject2, "sim_info", cVar.f2166l)).mo2318n(m1008a(jSONObject2, "sim_imei", cVar.f2167m)).mo2319o(m1008a(jSONObject2, "access_point", cVar.f2168n));
        }
    }

    @VisibleForTesting
    /* renamed from: a */
    static void m1003a(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("locale");
        String str = "";
        if (optJSONObject != null) {
            JSONObject optJSONObject2 = optJSONObject.optJSONObject("country");
            if (optJSONObject2 != null && optJSONObject2.optBoolean("reliable", false)) {
                str = optJSONObject2.optString("value", "");
            }
        }
        bVar.m1087g(str);
    }

    @VisibleForTesting
    /* renamed from: B */
    static void m1013b(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("permissions");
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        String optString = optJSONObject2.optString("name");
                        boolean optBoolean = optJSONObject2.optBoolean("enabled");
                        if (TextUtils.isEmpty(optString)) {
                            bVar.mo622a("", false);
                        } else {
                            bVar.mo622a(optString, optBoolean);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a */
    static void m1017c(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        m1004a(bVar, aVar, new nc());
    }

    @VisibleForTesting
    /* renamed from: a */
    static void m1004a(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar, @NonNull nc ncVar) {
        bVar.m1046a(ncVar.a(m1000a(aVar)));
    }

    /* renamed from: a */
    static C0878i m1000a(@NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        C0878i iVar = new C0878i();
        JSONObject optJSONObject = aVar.optJSONObject("sdk_list");
        if (optJSONObject != null) {
            iVar.f2197b = wk.m4368a(vq.m4211a(optJSONObject, "min_collecting_interval_seconds"), TimeUnit.SECONDS, iVar.f2197b);
            iVar.f2198c = wk.m4368a(vq.m4211a(optJSONObject, "min_first_collecting_delay_seconds"), TimeUnit.SECONDS, iVar.f2198c);
            iVar.f2199d = wk.m4368a(vq.m4211a(optJSONObject, "min_collecting_delay_after_launch_seconds"), TimeUnit.SECONDS, iVar.f2199d);
            iVar.f2200e = wk.m4368a(vq.m4211a(optJSONObject, "min_request_retry_interval_seconds"), TimeUnit.SECONDS, iVar.f2200e);
        }
        return iVar;
    }

    /* renamed from: g */
    private static void m1022g(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("queries");
        if (optJSONObject != null) {
            JSONObject optJSONObject2 = optJSONObject.optJSONObject("list");
            if (optJSONObject2 != null) {
                JSONObject optJSONObject3 = optJSONObject2.optJSONObject("sdk_list");
                if (optJSONObject3 != null) {
                    bVar.m1050a(optJSONObject3.optString("url", null));
                }
            }
        }
    }

    /* renamed from: h */
    private static void m1023h(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        C0880k kVar = new C0880k();
        JSONObject optJSONObject = aVar.optJSONObject("stat_sending");
        if (optJSONObject != null) {
            kVar.f2206b = wk.m4368a(vq.m4211a(optJSONObject, "disabled_reporting_interval_seconds"), TimeUnit.SECONDS, kVar.f2206b);
        }
        bVar.m1048a(new ng().a(kVar));
    }

    /* renamed from: a */
    private static boolean m1008a(JSONObject jSONObject, String str, boolean z) throws JSONException {
        if (jSONObject.has(str)) {
            return jSONObject.getJSONObject(str).optBoolean("enabled", z);
        }
        return z;
    }

    /* renamed from: i */
    private void m1024i(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) {
        long j;
        long j2;
        JSONObject optJSONObject = aVar.optJSONObject("permissions_collecting");
        C0875g gVar = new C0875g();
        if (optJSONObject != null) {
            j = optJSONObject.optLong("check_interval_seconds", gVar.f2185b);
            j2 = optJSONObject.optLong("force_send_interval_seconds", gVar.f2186c);
        } else {
            j = gVar.f2185b;
            j2 = gVar.f2186c;
        }
        bVar.m1045a(new tz(j, j2));
    }

    /* renamed from: j */
    private void m1025j(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("socket");
        if (optJSONObject != null) {
            long optLong = optJSONObject.optLong("seconds_to_live");
            long optLong2 = optJSONObject.optLong("first_delay_seconds", new C0879j().f2204e);
            int optInt = optJSONObject.optInt("launch_delay_seconds", new C0879j().f2205f);
            String optString = optJSONObject.optString("token");
            JSONArray optJSONArray = optJSONObject.optJSONArray("ports");
            if (optLong > 0 && m1006a(optString) && optJSONArray != null && optJSONArray.length() > 0) {
                ArrayList arrayList = new ArrayList(optJSONArray.length());
                for (int i = 0; i < optJSONArray.length(); i++) {
                    int optInt2 = optJSONArray.optInt(i);
                    if (optInt2 != 0) {
                        arrayList.add(Integer.valueOf(optInt2));
                    }
                }
                if (!arrayList.isEmpty()) {
                    bVar.m1047a(new ub(optLong, optString, arrayList, optLong2, optInt));
                }
            }
        }
    }

    /* renamed from: k */
    private void m1026k(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        JSONObject jSONObject = (JSONObject) aVar.mo2496a("query_hosts", new JSONObject());
        if (jSONObject.has("list")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("list");
            String a = m1002a(jSONObject2, "get_ad");
            if (m1006a(a)) {
                bVar.m1057b(a);
            }
            List b = m1012b(jSONObject2, "report");
            if (m1007a(b)) {
                bVar.m1058b(b);
            }
            String a2 = m1002a(jSONObject2, "report_ad");
            if (m1006a(a2)) {
                bVar.m1064c(a2);
            }
            List b2 = m1012b(jSONObject2, "location");
            if (m1007a(b2)) {
                bVar.m1065c(b2);
            }
            List b3 = m1012b(jSONObject2, "startup");
            if (m1007a(b3)) {
                bVar.m1051a(b3);
            }
            List b4 = m1012b(jSONObject2, "diagnostic");
            if (m1007a(b4)) {
                bVar.m1079e(b4);
            }
        }
    }

    /* renamed from: a */
    private boolean m1006a(String str) {
        return !TextUtils.isEmpty(str);
    }

    /* renamed from: a */
    private boolean m1007a(List<String> list) {
        return !cx.a((Collection) list);
    }

    /* renamed from: l */
    private void m1027l(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        JSONObject optJSONObject = ((JSONObject) aVar.mo2496a("distribution_customization", new JSONObject())).optJSONObject("clids");
        if (optJSONObject != null) {
            m1005a(bVar, optJSONObject);
        }
    }

    /* renamed from: a */
    private void m1005a(b bVar, JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            JSONObject optJSONObject = jSONObject.optJSONObject(str);
            if (optJSONObject != null && optJSONObject.has("value")) {
                hashMap.put(str, optJSONObject.getString("value"));
            }
        }
        bVar.m1083f(we.m4339a((Map<String, String>) hashMap));
    }

    /* renamed from: B */
    private void m1014b(b bVar, JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("time");
        if (optJSONObject != null) {
            try {
                bVar.m1049a(Long.valueOf(optJSONObject.getLong("max_valid_difference_seconds")));
            } catch (Throwable th) {
            }
        }
    }

    /* renamed from: a */
    private void m1018c(b bVar, JSONObject jSONObject) {
        bVar.m1044a(new mt().a(m1016c(jSONObject.optJSONObject("identity_light_collecting"))));
    }

    /* renamed from: a */
    private C0872d m1016c(@Nullable JSONObject jSONObject) {
        C0872d dVar = new C0872d();
        if (jSONObject != null) {
            dVar.f2172b = jSONObject.optLong("min_interval_seconds", dVar.f2172b);
        }
        return dVar;
    }

    /* renamed from: a */
    public static Long m1001a(@Nullable Map<String, List<String>> map) {
        if (!cx.m1193a((Map) map)) {
            List list = (List) map.get("Date");
            if (!cx.a((Collection) list)) {
                try {
                    return Long.valueOf(new SimpleDateFormat("E, d MMM yyyy HH:mm:ss z", Locale.US).parse((String) list.get(0)).getTime());
                } catch (Throwable th) {
                    return null;
                }
            }
        }
        return null;
    }

    @VisibleForTesting
    public cq(@NonNull co coVar) {
        this.f584b = coVar;
    }
}
