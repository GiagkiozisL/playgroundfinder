package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.st.C0987d;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.fa */
public class fa extends eo {
    public fa(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull un unVar, @NonNull uk ukVar, @NonNull C0987d dVar, @NonNull bl blVar, @NonNull xh xhVar, int i) {
        super(context, ekVar, aVar, unVar, ukVar, dVar, blVar, xhVar, i);
    }

    @NonNull
    /* renamed from: B */
    public bk mo873b(@NonNull ez ezVar) {
        return new bk(ezVar);
    }

    @NonNull
    /* renamed from: a */
    public bj mo870a(@NonNull bk bkVar, @NonNull lw lwVar) {
        return new bj(this.f851a, bkVar, lwVar);
    }

    @NonNull
    /* renamed from: a */
    public ku mo872a(@NonNull ah ahVar, @NonNull wm<File> wmVar) {
        return new ku(ahVar.mo267c(this.f851a), wmVar);
    }
}
