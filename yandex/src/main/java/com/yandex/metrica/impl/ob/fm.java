package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.fm */
class fm extends fz {
    fm() {
    }

    @NonNull
    /* renamed from: a */
    public en d(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        return new ef(context, ulVar.mo2406e(), blVar, ekVar, aVar, new fj(al.m324a().mo290h()), new ur());
    }

    @NonNull
    /* renamed from: B */
    public ge c(@NonNull Context context, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        return new ge(context, ekVar, blVar, aVar, ulVar.mo2406e(), new ur(), C0008a.APPMETRICA);
    }
}
