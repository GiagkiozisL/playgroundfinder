package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.SparseArray;

/* renamed from: com.yandex.metrica.impl.ob.lt */
class lt {

    /* renamed from: a */
    private static volatile SparseArray<qk> f1361a = new SparseArray<>();

    lt() {
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public synchronized String mo1274a(int i) {
        if (f1361a.get(i) == null) {
            f1361a.put(i, new qk("EVENT_NUMBER_OF_TYPE_" + i));
        }
        return ((qk) f1361a.get(i)).mo1744b();
    }
}
