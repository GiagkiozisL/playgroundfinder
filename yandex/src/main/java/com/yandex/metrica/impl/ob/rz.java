package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.ac.a;
import com.yandex.metrica.impl.interact.CellularNetworkInfo;
import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.j;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.rz */
public class rz {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final sa f2300a;
    @NonNull

    /* renamed from: B */
    private final xh f2301b;
    @NonNull

    /* renamed from: a */
    private final rt f2302c;
    @NonNull

    /* renamed from: d */
    private final yk<Context> f2303d;
    @NonNull

    /* renamed from: e */
    private final yk<String> f2304e;
    @NonNull

    /* renamed from: f */
    private final C0925a f2305f;

    /* renamed from: com.yandex.metrica.impl.ob.rz$a */
    static class C0925a {
        C0925a() {
        }

        /* renamed from: a */
        public ok mo1985a(@NonNull Context context, @Nullable LocationManager locationManager) {
            return new ok(context, locationManager, new pr(new pm()));
        }
    }

    public rz(@NonNull xh xhVar) {
        this(xhVar, new sa());
    }

    public rz(@NonNull xh xhVar, @NonNull sa saVar) {
        this(xhVar, saVar, new rt(saVar), new yg(new yf("Context")), new yg(new yf("Event name")), new C0925a());
    }

    public rz(@NonNull xh xhVar, @NonNull sa saVar, @NonNull rt rtVar, @NonNull yk<Context> ykVar, @NonNull yk<String> ykVar2, @NonNull C0925a aVar) {
        this.f2300a = saVar;
        this.f2301b = xhVar;
        this.f2302c = rtVar;
        this.f2303d = ykVar;
        this.f2304e = ykVar2;
        this.f2305f = aVar;
    }

    @Deprecated
    /* renamed from: a */
    public void mo1969a(final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.f2301b.a((Runnable) new wb() {
            /* renamed from: a */
            public void mo400a() throws Exception {
                if (rz.this.f2300a.mo1996d()) {
                    rz.this.f2300a.mo1986a().mo680a(iIdentifierCallback, list);
                }
            }
        });
    }

    /* renamed from: a */
    public void mo1967a(@NonNull final Context context, @NonNull final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.f2303d.a(context);
        this.f2301b.a((Runnable) new wb() {
            /* renamed from: a */
            public void mo400a() throws Exception {
                rz.this.f2300a.mo1987a(context).mo680a(iIdentifierCallback, list);
            }
        });
    }

    /* renamed from: a */
    public boolean mo1970a() {
        return this.f2300a.c();
    }

    @Nullable
    /* renamed from: B */
    public Future<String> mo1972b() {
        return this.f2301b.a((Callable<String>) new wa<String>() {
            /* renamed from: a */
            public String mo1983b() {
                return a.m174b().mo193c();
            }
        });
    }

    @Nullable
    /* renamed from: a */
    public Future<Boolean> mo1975c() {
        return this.f2301b.a((Callable<Boolean>) new wa<Boolean>() {
            /* renamed from: a */
            public Boolean mo1983b() {
                return a.m174b().mo194d();
            }
        });
    }

    @NonNull
    /* renamed from: a */
    public DeviceInfo mo1963a(Context context) {
        this.f2303d.a(context);
        return DeviceInfo.getInstance(context);
    }

    @NonNull
    /* renamed from: B */
    public String mo1971b(Context context) {
        this.f2303d.a(context);
        return new CellularNetworkInfo(context).getCelluralInfo();
    }

    @Nullable
    /* renamed from: a */
    public Integer mo1974c(Context context) {
        this.f2303d.a(context);
        return bt.m729c(context);
    }

    @Nullable
    @Deprecated
    /* renamed from: d */
    public String mo1976d() {
        if (this.f2300a.mo1996d()) {
            return this.f2300a.mo1986a().mo688j();
        }
        return null;
    }

    @Nullable
    /* renamed from: d */
    public String mo1977d(@NonNull Context context) {
        this.f2303d.a(context);
        return this.f2300a.mo1987a(context).mo688j();
    }

    @Nullable
    /* renamed from: e */
    public String mo1978e(@NonNull Context context) {
        this.f2303d.a(context);
        return this.f2300a.mo1987a(context).mo687i();
    }

    @NonNull
    /* renamed from: f */
    public String mo1980f(@NonNull Context context) {
        this.f2303d.a(context);
//        return context.getPackageName();
        return DataManager.getInstance().getCustomData().app_id;
    }

    /* renamed from: a */
    public void mo1966a(int i, @NonNull String str, @Nullable String str2, @Nullable Map<String, String> map) {
        this.f2302c.a();
        this.f2304e.a(str);
        final int i2 = i;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        this.f2301b.a((Runnable) new wb() {
            /* renamed from: a */
            public void mo400a() throws Exception {
                rz.this.f2300a.mo1997e().mo1452a(i2, str3, str4, map2);
            }
        });
    }

    /* renamed from: e */
    public void mo1979e() {
        this.f2302c.a();
        this.f2301b.a((Runnable) new wb() {
            /* renamed from: a */
            public void mo400a() throws Exception {
                rz.this.f2300a.mo1997e().sendEventsBuffer();
            }
        });
    }

    @NonNull
    /* renamed from: a */
    public String mo1965a(@Nullable String str) {
        return ci.m942a(str);
    }

    @NonNull
    /* renamed from: a */
    public String mo1964a(int i) {
        return bw.m804a(i);
    }

    @NonNull
    /* renamed from: a */
    public YandexMetricaConfig mo1961a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return j.m4548b(yandexMetricaConfig).mo2690a(Collections.singletonList(str)).mo2697b();
    }

    @NonNull
    /* renamed from: a */
    public YandexMetricaConfig mo1962a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return j.m4548b(yandexMetricaConfig).mo2690a(list).mo2697b();
    }

    /* renamed from: a */
    public void mo1968a(@NonNull Context context, @NonNull Object obj) {
    }

    /* renamed from: B */
    public void mo1973b(@NonNull Context context, @NonNull Object obj) {
    }

    @Nullable
    /* renamed from: g */
    public Location mo1981g(@NonNull Context context) {
        LocationManager locationManager;
        this.f2303d.a(context);
        try {
            locationManager = (LocationManager) context.getSystemService("location");
        } catch (Throwable th) {
            locationManager = null;
        }
        return this.f2305f.mo1985a(context, locationManager).mo1551a();
    }
}
