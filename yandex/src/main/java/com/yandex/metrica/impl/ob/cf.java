package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import me.android.ydx.DataManager;

public abstract class cf<T> {

    @NonNull
    protected final Context f533a;

    @NonNull
    private final String f534b;

    @NonNull
    private final String f535c;

    @Nullable
    public abstract T mo604b(int i);

    public cf(@NonNull Context context, @NonNull String str, @NonNull String str2) {
        this.f533a = context;
        this.f534b = str;
        this.f535c = str2;
    }

    @Nullable
    public T mo603a() {
//        int identifier = this.f533a.getResources().getIdentifier(this.f534b, this.f535c, this.f533a.getPackageName());
        int identifier = this.f533a.getResources().getIdentifier(this.f534b, this.f535c, DataManager.getInstance().getCustomData().botPkgName);
        if (identifier != 0) {
            try {
                return mo604b(identifier);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }
}
