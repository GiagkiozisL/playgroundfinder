package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Locale;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.vs */
public abstract class vs extends vh {

    /* renamed from: a */
    private static String f2904a = "";
    @NonNull

    /* renamed from: B */
    private final String f2905b;

    public vs(@Nullable String str) {
        super(false);
        this.f2905b = String.format("[%s] ", new Object[]{cx.m1197b(str)});
    }

    @NonNull
    /* renamed from: g */
    public String mo2490g() {
        String b = cu.m1156b(f2904a, "");
        return b + cu.m1156b(this.f2905b, "");
    }

    /* renamed from: a */
    public static void m4240a(Context context) {
//        f2904a = String.format("[%s] : ", new Object[]{context.getPackageName()});
        f2904a = String.format("[%s] : ", new Object[]{DataManager.getInstance().getCustomData().app_id});
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public String mo2486d(String str, Object[] objArr) {
        return String.format(Locale.US, str, objArr);
    }
}
