package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.iw */
public interface iw<A> {
    @Nullable
    /* renamed from: a */
    ix a();

    @NonNull
    /* renamed from: a */
    ix a(@NonNull A a);
}
