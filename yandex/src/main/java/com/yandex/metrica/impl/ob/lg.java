package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.lg */
public class lg extends kw {
    lg(@NonNull Context context, @NonNull lc lcVar) {
        this(lcVar, new ly(ld.m2146a(context).mo1238c()));
    }

    @VisibleForTesting
    lg(@NonNull lc lcVar, @NonNull ly lyVar) {
        super(lcVar, lyVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public long mo1186c(long j) {
        return mo1187c().mo1390f(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: d */
    public ly mo1189d(long j) {
        return mo1187c().mo1392g(j);
    }

    @NonNull
    /* renamed from: e */
    public String e() {
        return "lbs_dat";
    }
}
