package com.yandex.metrica.impl.ob;

import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.yandex.metrica.impl.ob.ix */
public class ix {

    /* renamed from: a */
    private final en f1066a;

    /* renamed from: B */
    private final jf f1067b;

    /* renamed from: a */
    private final iz f1068c;

    /* renamed from: d */
    private long f1069d;

    /* renamed from: e */
    private long f1070e;

    /* renamed from: f */
    private AtomicLong f1071f;

    /* renamed from: g */
    private boolean f1072g;

    /* renamed from: h */
    private volatile C0470a f1073h;

    /* renamed from: i */
    private long f1074i;

    /* renamed from: j */
    private long f1075j;

    /* renamed from: k */
    private wg f1076k;

    /* renamed from: com.yandex.metrica.impl.ob.ix$a */
    static class C0470a {

        /* renamed from: a */
        private final String f1077a;

        /* renamed from: B */
        private final String f1078b;

        /* renamed from: a */
        private final String f1079c;

        /* renamed from: d */
        private final String f1080d;

        /* renamed from: e */
        private final String f1081e;

        /* renamed from: f */
        private final int f1082f;

        /* renamed from: g */
        private final int f1083g;

        C0470a(JSONObject jSONObject) {
            this.f1077a = jSONObject.optString("analyticsSdkVersionName", null);
            this.f1078b = jSONObject.optString("kitBuildNumber", null);
            this.f1079c = jSONObject.optString("appVer", null);
            this.f1080d = jSONObject.optString("appBuild", null);
            this.f1081e = jSONObject.optString("osVer", null);
            this.f1082f = jSONObject.optInt("osApiLev", -1);
            this.f1083g = jSONObject.optInt("attribution_id", 0);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo1048a(st stVar) {
            boolean z = TextUtils.equals(stVar.getSDKVersionName(), this.f1077a) && TextUtils.equals(stVar.getSDKVersionNumber(), this.f1078b) && TextUtils.equals(stVar.getAppVersionName(), this.f1079c) && TextUtils.equals(stVar.getAppVersionCode(), this.f1080d) && TextUtils.equals(stVar.getOsVersion(), this.f1081e) && this.f1082f == stVar.getApiLevel() && this.f1083g == stVar.mo2145V();
            if (!z) {
            }
            return z;
        }

        public String toString() {
            return "SessionRequestParams{mKitVersionName='" + this.f1077a + '\'' + ", mKitBuildNumber='" + this.f1078b + '\'' + ", mAppVersion='" + this.f1079c + '\'' + ", mAppBuild='" + this.f1080d + '\'' + ", mOsVersion='" + this.f1081e + '\'' + ", mApiLevel=" + this.f1082f + '}';
        }
    }

    ix(en enVar, jf jfVar, iz izVar) {
        this(enVar, jfVar, izVar, new wg());
    }

    ix(en enVar, jf jfVar, iz izVar, wg wgVar) {
        this.f1066a = enVar;
        this.f1067b = jfVar;
        this.f1068c = izVar;
        this.f1076k = wgVar;
        m1850i();
    }

    /* renamed from: i */
    private void m1850i() {
        this.f1070e = this.f1068c.mo1054b(this.f1076k.c());
        this.f1069d = this.f1068c.mo1051a(-1);
        this.f1071f = new AtomicLong(this.f1068c.mo1055c(0));
        this.f1072g = this.f1068c.mo1053a(true);
        this.f1074i = this.f1068c.mo1056d(0);
        this.f1075j = this.f1068c.mo1057e(this.f1074i - this.f1070e);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public jh mo1034a() {
        return this.f1068c.mo1052a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public int mo1038b() {
        return this.f1068c.mo1050a(this.f1066a.mo822i().mo2142S());
    }

    /* renamed from: a */
    public long mo1040c() {
        return this.f1069d;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public long mo1042d() {
        return Math.max(this.f1074i - TimeUnit.MILLISECONDS.toSeconds(this.f1070e), this.f1075j);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo1036a(long j) {
        boolean z;
        boolean z2 = this.f1069d >= 0;
        boolean j2 = m1851j();
        if (!mo1037a(j, this.f1076k.c())) {
            z = true;
        } else {
            z = false;
        }
        if (!z2 || !j2 || !z) {
            return false;
        }
        return true;
    }

    /* renamed from: j */
    private boolean m1851j() {
        C0470a k = m1852k();
        if (k != null) {
            return k.mo1048a(this.f1066a.mo822i());
        }
        return false;
    }

    /* renamed from: d */
    private long m1849d(long j) {
        return TimeUnit.MILLISECONDS.toSeconds(j - this.f1070e);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public boolean mo1037a(long j, long j2) {
        long j3 = this.f1074i;
        boolean z = TimeUnit.MILLISECONDS.toSeconds(j2) < j3;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j) - j3;
        long d = m1849d(j);
        if (z || seconds >= ((long) mo1038b()) || d >= ja.f1105c) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public synchronized void mo1043e() {
        this.f1067b.a();
        this.f1073h = null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo1039b(long j) {
        jf jfVar = this.f1067b;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j);
        this.f1074i = seconds;
        jfVar.b(seconds).mo1100h();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public long mo1041c(long j) {
        jf jfVar = this.f1067b;
        long d = m1849d(j);
        this.f1075j = d;
        jfVar.c(d);
        return this.f1075j;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public long mo1044f() {
        return this.f1075j;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public long mo1045g() {
        long andIncrement = this.f1071f.getAndIncrement();
        this.f1067b.a(this.f1071f.get()).mo1100h();
        return andIncrement;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: h */
    public boolean mo1046h() {
        return this.f1072g && mo1040c() > 0;
    }

    /* renamed from: a */
    public void mo1035a(boolean z) {
        if (this.f1072g != z) {
            this.f1072g = z;
            this.f1067b.a(this.f1072g).mo1100h();
        }
    }

    /* renamed from: k */
    private C0470a m1852k() {
        if (this.f1073h == null) {
            synchronized (this) {
                if (this.f1073h == null) {
                    try {
                        String asString = this.f1066a.mo823j().mo1206b(mo1040c(), mo1034a()).getAsString("report_request_parameters");
                        if (!TextUtils.isEmpty(asString)) {
                            this.f1073h = new C0470a(new JSONObject(asString));
                        }
                    } catch (Throwable th) {
                    }
                }
            }
        }
        return this.f1073h;
    }

    public String toString() {
        return "Session{mId=" + this.f1069d + ", mInitTime=" + this.f1070e + ", mCurrentReportId=" + this.f1071f + ", mSessionRequestParams=" + this.f1073h + ", mSleepStartSeconds=" + this.f1074i + '}';
    }
}
