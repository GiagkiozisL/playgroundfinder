package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.rq */
public interface rq {

    /* renamed from: com.yandex.metrica.impl.ob.rq$a */
    public static final class C0859a extends e {

        /* renamed from: B */
        public C0860a[] f2084b;

        /* renamed from: a */
        public String f2085c;

        /* renamed from: d */
        public long f2086d;

        /* renamed from: e */
        public boolean f2087e;

        /* renamed from: f */
        public boolean f2088f;

        /* renamed from: com.yandex.metrica.impl.ob.rq$a$a */
        public static final class C0860a extends e {

            /* renamed from: d */
            private static volatile C0860a[] f2089d;

            /* renamed from: B */
            public String f2090b;

            /* renamed from: a */
            public String[] f2091c;

            /* renamed from: d */
            public static C0860a[] m3268d() {
                if (f2089d == null) {
                    synchronized (c.a) {
                        if (f2089d == null) {
                            f2089d = new C0860a[0];
                        }
                    }
                }
                return f2089d;
            }

            public C0860a() {
                mo1861e();
            }

            /* renamed from: e */
            public C0860a mo1861e() {
                this.f2090b = "";
                this.f2091c = g.f;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(b bVar) throws IOException {
                bVar.mo351a(1, this.f2090b);
                if (this.f2091c != null && this.f2091c.length > 0) {
                    for (String str : this.f2091c) {
                        if (str != null) {
                            bVar.mo351a(2, str);
                        }
                    }
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int i;
                int c = super.mo741c() + b.m437b(1, this.f2090b);
                if (this.f2091c == null || this.f2091c.length <= 0) {
                    return c;
                }
                int i2 = 0;
                int i3 = 0;
                int i4 = 0;
                while (i2 < this.f2091c.length) {
                    String str = this.f2091c[i2];
                    if (str != null) {
                        i4++;
                        i = b.m441b(str) + i3;
                    } else {
                        i = i3;
                    }
                    i2++;
                    i3 = i;
                }
                return c + i3 + (i4 * 1);
            }

            /* renamed from: B */
            public C0860a mo738a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2090b = aVar.mo229i();
                            continue;
                        case 18:
                            int b = g.b(aVar, 18);
                            int length = this.f2091c == null ? 0 : this.f2091c.length;
                            String[] strArr = new String[(b + length)];
                            if (length != 0) {
                                System.arraycopy(this.f2091c, 0, strArr, 0, length);
                            }
                            while (length < strArr.length - 1) {
                                strArr[length] = aVar.mo229i();
                                aVar.mo213a();
                                length++;
                            }
                            strArr[length] = aVar.mo229i();
                            this.f2091c = strArr;
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public C0859a() {
            mo1859d();
        }

        /* renamed from: d */
        public C0859a mo1859d() {
            this.f2084b = C0860a.m3268d();
            this.f2085c = "";
            this.f2086d = 0;
            this.f2087e = false;
            this.f2088f = false;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (this.f2084b != null && this.f2084b.length > 0) {
                for (C0860a aVar : this.f2084b) {
                    if (aVar != null) {
                        bVar.mo350a(1, (e) aVar);
                    }
                }
            }
            bVar.mo351a(2, this.f2085c);
            bVar.b(3, this.f2086d);
            bVar.mo352a(4, this.f2087e);
            bVar.mo352a(5, this.f2088f);
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f2084b != null && this.f2084b.length > 0) {
                for (C0860a aVar : this.f2084b) {
                    if (aVar != null) {
                        c += b.b(1, (e) aVar);
                    }
                }
            }
            return b.m437b(2, this.f2085c) + c + b.m450e(3, this.f2086d) + b.m438b(4, this.f2087e) + b.m438b(5, this.f2088f);
        }

        /* renamed from: B */
        public C0859a mo738a(a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        int b = g.b(aVar, 10);
                        if (this.f2084b == null) {
                            length = 0;
                        } else {
                            length = this.f2084b.length;
                        }
                        C0860a[] aVarArr = new C0860a[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.f2084b, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0860a();
                            aVar.mo215a((e) aVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        aVarArr[length] = new C0860a();
                        aVar.mo215a((e) aVarArr[length]);
                        this.f2084b = aVarArr;
                        continue;
                    case 18:
                        this.f2085c = aVar.mo229i();
                        continue;
                    case 24:
                        this.f2086d = aVar.mo223f();
                        continue;
                    case 32:
                        this.f2087e = aVar.mo228h();
                        continue;
                    case 40:
                        this.f2088f = aVar.mo228h();
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }

        /* renamed from: a */
        public static C0859a m3262a(byte[] bArr) throws d {
            return (C0859a) e.m1393a(new C0859a(), bArr);
        }
    }
}
