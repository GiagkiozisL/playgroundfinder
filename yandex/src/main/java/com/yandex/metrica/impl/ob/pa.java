package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rh.b.C0819a;

/* renamed from: com.yandex.metrica.impl.ob.pa */
public class pa {
    @NonNull

    /* renamed from: a */
    private final og f1705a;
    @NonNull

    /* renamed from: B */
    private final oz f1706b;

    public pa(@NonNull og ogVar, @NonNull oz ozVar) {
        this.f1705a = ogVar;
        this.f1706b = ozVar;
    }

    @Nullable
    /* renamed from: a */
    public C0819a mo1626a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            on b = this.f1705a.mo1546b(j, str);
            if (b != null) {
                return this.f1706b.mo1619a(b);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
