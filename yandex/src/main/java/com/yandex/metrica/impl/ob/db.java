package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.d;
import com.yandex.metrica.f;
import com.yandex.metrica.impl.ac.a;
import com.yandex.metrica.impl.ob.ki.C0534a;
import com.yandex.metrica.impl.ob.vv.C1144a;
import com.yandex.metrica.impl.ob.x.C1181a;
import com.yandex.metrica.j;

import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.db */
public class db implements C1181a {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a */
    private static db f670a;

    /* renamed from: B */
    private static aa f671b = new aa();

    /* renamed from: a */
    private static volatile boolean f672c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static final EnumMap<Reason, AppMetricaDeviceIDListener.Reason> f673d = new EnumMap<>(Reason.class);

    /* renamed from: e */
    private static xf f674e = new xf();

    /* renamed from: f */
    private final Context f675f;

    /* renamed from: g */
    private final ca f676g;

    /* renamed from: h */
    private ax f677h;

    /* renamed from: i */
    private bh f678i;

    /* renamed from: j */
    private final uf f679j;

    /* renamed from: k */
    private final bx f680k;

    /* renamed from: l */
    private ki f681l;

    /* renamed from: m */
    private final ee f682m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public IIdentifierCallback f683n;

    /* renamed from: o */
    private cd f684o;// = new cd(this.f682m, this.f675f, k().b());
    @NonNull

    /* renamed from: p */
    private t f685p;

    static {
        f673d.put(Reason.UNKNOWN, AppMetricaDeviceIDListener.Reason.UNKNOWN);
        f673d.put(Reason.INVALID_RESPONSE, AppMetricaDeviceIDListener.Reason.INVALID_RESPONSE);
        f673d.put(Reason.NETWORK, AppMetricaDeviceIDListener.Reason.NETWORK);
    }

    private db(Context context) {
        this.f675f = context.getApplicationContext();
        lf f = ld.m2146a(this.f675f).mo1241f();
        vz.m4240a(context.getApplicationContext());
        ci.m943a();
        a.m174b().mo189a(this.f675f);
        Handler a = k().b().a();
        this.f682m = new ee(this.f675f, (ResultReceiver) new x(a, this));
        this.f684o = new cd(this.f682m, this.f675f, k().b());
        lv lvVar = new lv(f);
        f671b.mo245a(this.f684o);
        new s(lvVar).mo426a(this.f675f);
        this.f679j = new uf(this.f684o, lvVar, a);
        this.f684o.mo571a((ug) this.f679j);
        this.f680k = new bx(this.f684o, lvVar, k().b());
        this.f676g = new ca(this.f675f, this.f682m, this.f684o, a, this.f679j);
        this.f685p = new t();
        this.f685p.mo2216a();
    }

    /* renamed from: a */
    public static xh m1276a() {
        return k().mo2599a();
    }

    /* renamed from: a */
    public static synchronized void m1277a(@NonNull Context context, @NonNull j jVar) {
        synchronized (db.class) {
            m1284c(context, jVar);
            if (wk.m4371a(jVar.crashReporting, true) && m1286d().f678i == null) {
                m1286d().f678i = new bh(Thread.getDefaultUncaughtExceptionHandler(), context);
                Thread.setDefaultUncaughtExceptionHandler(m1286d().f678i);
            }
        }
    }

    /* renamed from: B */
    public static synchronized void m1282b(@NonNull Context context, @NonNull j jVar) {
        Log.d(Constant.RUS_TAG, "db$m1282b");
        synchronized (db.class) {
            vz a = vr.m4237a(jVar.apiKey);
            vp b = vr.m4239b(jVar.apiKey);
            boolean d = f671b.mo249d();
            j a2 = f671b.mo243a(jVar);
            m1284c(context, a2);
            if (f670a.f677h == null) {
                f670a.m1283b(jVar);
                f670a.f679j.mo2341a(a);
                f670a.m1279a(jVar);
                f670a.f682m.mo746a(jVar);
                f670a.mo682a(a2, d);
                Log.i("AppMetrica", "Activate AppMetrica with APIKey " + cx.m1197b(a2.apiKey));
                if (vi.m4192a(a2.logs)) {
                    a.mo2474a();
                    b.mo2474a();
                    vr.m4236a().mo2474a();
                    vr.m4238b().mo2474a();
                } else {
                    a.mo2480b();
                    b.mo2480b();
                    vr.m4236a().mo2480b();
                    vr.m4238b().mo2480b();
                }
            } else if (a.mo2485c()) {
                a.mo2481b("Appmetrica already has been activated!");
            }
        }
    }

    /* renamed from: a */
    public static synchronized void m1284c(Context context, @Nullable j jVar) {
        synchronized (db.class) {
            if (f670a == null) {
                f670a = new db(context.getApplicationContext());
                f670a.m1293m();
            }
        }
    }

    /* renamed from: B */
    public static void m1281b() {
        f672c = true;
    }

    /* renamed from: a */
    public static boolean m1285c() {
        return f672c;
    }

    /* renamed from: d */
    public static synchronized db m1286d() {
        db dbVar;
        synchronized (db.class) {
            dbVar = f670a;
        }
        return dbVar;
    }

    /* renamed from: a */
    public static db m1275a(Context context) {
        m1284c(context.getApplicationContext(), null);
        return m1286d();
    }

    @Nullable
    /* renamed from: e */
    public static db m1287e() {
        return m1286d();
    }

    /* renamed from: f */
    public static synchronized ax m1288f() {
        ax axVar;
        synchronized (db.class) {
            axVar = m1286d().f677h;
        }
        return axVar;
    }

    /* renamed from: g */
    static synchronized boolean m1289g() {
        boolean z;
        synchronized (db.class) {
            z = (f670a == null || f670a.f677h == null) ? false : true;
        }
        return z;
    }

    /* renamed from: m */
    private void m1293m() {
        as.m390a();
        k().b().a((Runnable) new C1144a(this.f675f));
    }

    /* renamed from: a */
    private void m1279a(j jVar) {
        if (jVar != null) {
            this.f679j.mo2343a(jVar.d);
            this.f679j.mo2344a(jVar.b);
            this.f679j.mo2342a(jVar.c);
        }
    }

    /* renamed from: a */
    public void mo681a(@NonNull f fVar) {
        this.f676g.mo554a(fVar);
    }

    /* renamed from: B */
    public d mo684b(@NonNull f fVar) {
        return this.f676g.mo555b(fVar);
    }

    /* renamed from: a */
    public void mo683a(String str) {
        this.f680k.mo527a(str);
    }

    @VisibleForTesting
    /* renamed from: h */
    static ba m1290h() {
        return m1289g() ? m1286d().f677h : f671b;
    }

    /* renamed from: a */
    public static void m1278a(Location location) {
        m1290h().a(location);
    }

    /* renamed from: a */
    public static void m1280a(boolean z) {
        m1290h().a(z);
    }

    /* renamed from: B */
    public void mo685b(boolean z) {
        m1290h().a(z);
    }

    /* renamed from: a */
    public void mo686c(boolean z) {
        m1290h().setStatisticsSending(z);
    }

    /* renamed from: i */
    public String mo687i() {
        return this.f679j.mo2345b();
    }

    /* renamed from: j */
    public String mo688j() {
        return this.f679j.a();
    }

    /* renamed from: a */
    public void mo680a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        this.f679j.mo2340a(iIdentifierCallback, list, this.f682m.mo753c());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo682a(j jVar, boolean z) {
        Log.d(Constant.RUS_TAG, "db$mo682a");
        this.f684o.mo573a(jVar.locationTracking, jVar.statisticsSending);
        this.f677h = this.f676g.mo553a(jVar, z);
        this.f679j.mo2346c();
    }

    /* renamed from: a */
    public void mo677a(int i, @NonNull Bundle bundle) {
        this.f679j.mo2338a(i, bundle);
    }

    /* renamed from: a */
    public void mo679a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        this.f680k.mo526a(deferredDeeplinkParametersListener);
    }

    /* renamed from: a */
    public void mo678a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        this.f683n = new IIdentifierCallback() {
            public void onReceive(Map<String, String> params) {
                db.this.f683n = null;
                appMetricaDeviceIDListener.onLoaded((String) params.get("appmetrica_device_id_hash"));
            }

            public void onRequestError(Reason reason) {
                db.this.f683n = null;
                appMetricaDeviceIDListener.onError((AppMetricaDeviceIDListener.Reason) db.f673d.get(reason));
            }
        };
        this.f679j.mo2340a(this.f683n, Collections.singletonList("appmetrica_device_id_hash"), this.f682m.mo753c());
    }

    /* renamed from: B */
    private void m1283b(@NonNull j jVar) {
        if (this.f678i != null) {
            this.f678i.mo421a((ki) new kj(new cb(this.f676g, "20799a27-fa80-4b36-b2db-0f8141f24180"), new C0534a() {
                /* renamed from: a */
                public boolean mo689a(Throwable th) {
                    return ci.m944a(th);
                }
            }, null));
            this.f678i.mo421a((ki) new kj(new cb(this.f676g, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"), new C0534a() {
                /* renamed from: a */
                public boolean mo689a(Throwable th) {
                    return ci.m947b(th);
                }
            }, null));
            if (this.f681l == null) {
                this.f681l = new kj(new cb(this.f676g, jVar.apiKey), new C0534a() {
                    /* renamed from: a */
                    public boolean mo689a(Throwable th) {
                        return true;
                    }
                }, jVar.n);
            }
            this.f678i.mo421a(this.f681l);
        }
    }

    /* renamed from: k */
    public static xf k() {
        return f674e;
    }
}
