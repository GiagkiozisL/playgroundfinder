package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.oq */
public class oq {
    @NonNull

    /* renamed from: a */
    private or f1648a;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public ou f1649b;
    @NonNull

    /* renamed from: a */
    private ok f1650c;
    @NonNull

    /* renamed from: d */
    private LocationListener f1651d;

    /* renamed from: e */
    private boolean f1652e;

    /* renamed from: com.yandex.metrica.impl.ob.oq$a */
    static class C0725a {
        C0725a() {
        }

        @NonNull
        /* renamed from: a */
        public or mo1592a(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull LocationListener locationListener, @NonNull pr prVar) {
            return new or(context, looper, locationManager, locationListener, prVar);
        }
    }

    public oq(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @Nullable oh ohVar, @NonNull ow owVar, @NonNull od odVar, @NonNull pr prVar) {
        this(context, looper, locationManager, new C0725a(), new ou(context, ohVar, owVar, odVar), new ok(context, locationManager, prVar), prVar);
    }

    /* renamed from: a */
    public void mo1582a() {
        Location a = this.f1650c.mo1551a();
        if (a != null) {
            this.f1649b.mo1608a(a);
        }
    }

    @Nullable
    /* renamed from: B */
    public Location mo1584b() {
        return this.f1649b.mo1607a();
    }

    @Nullable
    /* renamed from: a */
    public Location mo1585c() {
        return this.f1650c.mo1551a();
    }

    /* renamed from: d */
    public void mo1586d() {
        this.f1652e = true;
        this.f1648a.mo1593a();
    }

    /* renamed from: e */
    public void mo1587e() {
        this.f1652e = false;
        this.f1648a.mo1594b();
    }

    /* renamed from: a */
    public void mo1583a(@Nullable oh ohVar) {
        this.f1649b.mo1609a(ohVar);
        m2727f();
    }

    /* renamed from: f */
    private void m2727f() {
        if (this.f1652e) {
            mo1587e();
            mo1586d();
            mo1582a();
        }
    }

    @VisibleForTesting
    oq(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull C0725a aVar, @NonNull ou ouVar, @NonNull ok okVar, @NonNull pr prVar) {
        this.f1652e = false;
        this.f1651d = new LocationListener() {
            public void onLocationChanged(@Nullable Location location) {
                if (location != null) {
                    oq.this.f1649b.mo1608a(location);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        this.f1650c = okVar;
        this.f1648a = aVar.mo1592a(context, looper, locationManager, this.f1651d, prVar);
        this.f1649b = ouVar;
    }
}
