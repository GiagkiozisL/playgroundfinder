package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.net.Uri;
import android.os.Process;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.bf */
public class bf implements bg {

    /* renamed from: a */
    private final wp<String, Integer> f348a = new wp<>();

    /* renamed from: B */
    private final Map<C0125b, C0124a> f349b = new LinkedHashMap();

    /* renamed from: a */
    private final Map<C0125b, C0124a> f350c = new LinkedHashMap();

    /* renamed from: com.yandex.metrica.impl.ob.bf$a */
    interface C0124a {
        /* renamed from: a */
        boolean mo420a(@NonNull Intent intent, @NonNull bf bfVar);
    }

    /* renamed from: com.yandex.metrica.impl.ob.bf$B */
    interface C0125b {
        /* renamed from: a */
        void mo412a();
    }

    /* renamed from: a */
    public void a() {
    }

    /* renamed from: a */
    public void a(Intent intent, int i) {
    }

    /* renamed from: a */
    public void a(Intent intent, int i, int i2) {
    }

    /* renamed from: a */
    public void a(Intent intent) {
        if (intent != null) {
            m602d(intent);
        }
    }

    /* renamed from: B */
    public void b(Intent intent) {
        if (intent != null) {
            m602d(intent);
        }
    }

    /* renamed from: d */
    private void m602d(@NonNull Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            this.f348a.mo2572a(action, Integer.valueOf(m609h(intent)));
        }
        m593a(intent, this.f349b);
    }

    /* renamed from: a */
    private void m593a(@NonNull Intent intent, @NonNull Map<C0125b, C0124a> map) {
        for (Entry entry : map.entrySet()) {
            if (((C0124a) entry.getValue()).mo420a(intent, this)) {
                ((C0125b) entry.getKey()).mo412a();
            }
        }
    }

    /* renamed from: a */
    public void c(Intent intent) {
        if (intent != null) {
            m603e(intent);
        }
    }

    /* renamed from: e */
    private void m603e(@NonNull Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            this.f348a.b(action, Integer.valueOf(m609h(intent)));
        }
        m593a(intent, this.f350c);
    }

    /* renamed from: B */
    public void b() {
    }

    /* renamed from: a */
    public void mo414a(@NonNull C0125b bVar) {
        this.f349b.put(bVar, m601d());
    }

    /* renamed from: B */
    public void mo415b(@NonNull C0125b bVar) {
        this.f350c.put(bVar, m601d());
    }

    /* renamed from: a */
    public void mo416c(@NonNull C0125b bVar) {
        this.f349b.put(bVar, new C0124a() {
            /* renamed from: a */
            public boolean mo420a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.m606f(intent);
            }
        });
    }

    /* renamed from: d */
    public void mo418d(@NonNull C0125b bVar) {
        this.f349b.put(bVar, new C0124a() {
            /* renamed from: a */
            public boolean mo420a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.m608g(intent);
            }
        });
    }

    /* renamed from: e */
    public void mo419e(@NonNull C0125b bVar) {
        this.f350c.put(bVar, new C0124a() {
            /* renamed from: a */
            public boolean mo420a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.m608g(intent) && bf.this.m607g();
            }
        });
    }

    @NonNull
    /* renamed from: d */
    private C0124a m601d() {
        return new C0124a() {
            /* renamed from: a */
            public boolean mo420a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.m598a(intent.getAction());
            }
        };
    }

    /* renamed from: a */
    public boolean mo417c() {
        return !cx.a(this.f348a.mo2571a("com.yandex.metrica.ACTION_C_BG_L"));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m598a(@Nullable String str) {
        return "com.yandex.metrica.ACTION_C_BG_L".equals(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public boolean m606f(@NonNull Intent intent) {
        return m608g(intent) && m604e();
    }

    /* renamed from: B */
    private boolean m600b(@Nullable String str) {
        Log.d(Constant.RUS_TAG, "bf$m600b");
        return "com.yandex.metrica.IMetricaService".equals(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public boolean m608g(@NonNull Intent intent) {
        if (!m600b(intent.getAction()) || m594a(m609h(intent))) {
            return false;
        }
        return true;
    }

    /* renamed from: e */
    private boolean m604e() {
        return m605f() == 1;
    }

    /* renamed from: f */
    private int m605f() {
        int i = 0;
        Collection h = m610h();
        if (cx.a(h)) {
            return 0;
        }
        Iterator it = h.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            if (!m594a(((Integer) it.next()).intValue())) {
                i = i2 + 1;
            } else {
                i = i2;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public boolean m607g() {
        return m605f() == 0;
    }

    /* renamed from: h */
    private Collection<Integer> m610h() {
        Log.d(Constant.RUS_TAG, "bf$m610h");
        return this.f348a.mo2571a("com.yandex.metrica.IMetricaService");
    }

    /* renamed from: a */
    private boolean m594a(int i) {
        return i == Process.myPid();
    }

    /* renamed from: h */
    private int m609h(@NonNull Intent intent) {
        int i = -1;
        Uri data = intent.getData();
        if (data == null || !data.getPath().equals("/client")) {
            return i;
        }
        try {
            return Integer.parseInt(data.getQueryParameter("pid"));
        } catch (Throwable th) {
            return i;
        }
    }
}
