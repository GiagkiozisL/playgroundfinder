package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.kx */
public class kx {
    @NonNull

    /* renamed from: a */
    private final lm f1249a;
    @NonNull

    /* renamed from: B */
    private final String f1250b;

    public kx(@NonNull lm lmVar, @NonNull String str) {
        this.f1249a = lmVar;
        this.f1250b = str;
    }

    /* renamed from: a */
    public void mo1191a(@NonNull String str, @NonNull byte[] bArr) {
        SQLiteDatabase sQLiteDatabase = null;
        try {
            sQLiteDatabase = this.f1249a.a();
            if (sQLiteDatabase != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("data_key", str);
                contentValues.put("value", bArr);
                sQLiteDatabase.insertWithOnConflict(this.f1250b, null, contentValues, 5);
            }
        } catch (Throwable th) {
        } finally {
            this.f1249a.a(sQLiteDatabase);
        }
    }

    /* renamed from: a */
    public byte[] mo1192a(@NonNull String str) {
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor = null;
        Throwable th;
        Cursor cursor2 = null;
        SQLiteDatabase sQLiteDatabase2 = null;
        byte[] bArr = null;
        try {
            sQLiteDatabase2 = this.f1249a.a();
            if (sQLiteDatabase2 != null) {
                try {
                    cursor2 = sQLiteDatabase2.query(this.f1250b, null, "data_key = ?", new String[]{str}, null, null, null);
                    if (cursor2 != null) {
                        try {
                            if (cursor2.getCount() == 1 && cursor2.moveToFirst()) {
                                bArr = cursor2.getBlob(cursor2.getColumnIndex("value"));
                                cx.m1183a(cursor2);
                                this.f1249a.a(sQLiteDatabase2);
                                return bArr;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            cursor = cursor2;
                            sQLiteDatabase = sQLiteDatabase2;
                            cx.m1183a(cursor);
                            this.f1249a.a(sQLiteDatabase);
                            throw th;
                        }
                    }
                    if (!cx.m1201b(cursor2)) {
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor = null;
                    sQLiteDatabase = sQLiteDatabase2;
                    cx.m1183a(cursor);
                    this.f1249a.a(sQLiteDatabase);
                    throw th;
                }
            } else {
                cursor2 = null;
            }
            cx.m1183a(cursor2);
            this.f1249a.a(sQLiteDatabase2);
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            sQLiteDatabase = null;
            cx.m1183a(cursor);
            this.f1249a.a(sQLiteDatabase);
//            throw th;
        }
//        return bArr;
        cx.m1183a(cursor2);
        this.f1249a.a(sQLiteDatabase2);
        return bArr;
    }
}
