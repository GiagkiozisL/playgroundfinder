package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.mq */
public interface mq<S, P> {
    @NonNull
    /* renamed from: a */
    S a(@NonNull P p);
//    Object a(@NonNull Object p);

    @NonNull
    /* renamed from: B */
    P b(@NonNull S s);
//    Object aaa(@NonNull Object s);
}
