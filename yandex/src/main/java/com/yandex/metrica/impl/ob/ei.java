package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.si.C0964a;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ei */
public class ei implements et, ev, uh {
    @NonNull

    /* renamed from: a */
    private final Context f785a;
    @NonNull

    /* renamed from: B */
    private final ek f786b;
    @NonNull

    /* renamed from: a */
    private uc f787c;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: d */
    public ul f788d;
    @NonNull

    /* renamed from: e */
    private final fh f789e;
    @NonNull

    /* renamed from: f */
    private bl f790f;
    @NonNull

    /* renamed from: g */
    private gu<gt, ei> f791g;
    @NonNull

    /* renamed from: h */
    private cv<ei> f792h;
    @NonNull

    /* renamed from: i */
    private List<ap> f793i;

    /* renamed from: j */
    private final el<fs> f794j;

    /* renamed from: k */
    private si f795k;

    /* renamed from: l */
    private final C0964a f796l;
    @Nullable

    /* renamed from: m */
    private rs f797m;

    /* renamed from: n */
    private final Object f798n;

    public ei(@NonNull Context context, @NonNull uc ucVar, @NonNull ek ekVar, @NonNull eg egVar, @NonNull si siVar, @NonNull bl blVar) {
        this(context, ucVar, ekVar, egVar, new fh(egVar.f766b), siVar, blVar);
    }

    @VisibleForTesting
    ei(@NonNull Context context, @NonNull uc ucVar, @NonNull ek ekVar, @NonNull eg egVar, @NonNull fh fhVar, @NonNull si siVar, @NonNull bl blVar) {
        this.f794j = new el<>();
        this.f798n = new Object();
        this.f785a = context.getApplicationContext();
        this.f786b = ekVar;
        this.f787c = ucVar;
        this.f789e = fhVar;
        this.f790f = blVar;
        this.f793i = new ArrayList();
        this.f791g = new gu<>(new gm(this), this);
        this.f788d = this.f787c.mo2329a(this.f785a, this.f786b, this, egVar.f765a);
        this.f792h = new cv<>(this, new up(this.f788d), this.f790f);
        this.f795k = siVar;
        this.f796l = new C0964a() {
            /* renamed from: a */
            public boolean mo787a(@NonNull sj sjVar) {
                if (!TextUtils.isEmpty(sjVar.installReferrer)) {
                    ei.this.f788d.mo2400a(sjVar.installReferrer);
                }
                return false;
            }
        };
        this.f795k.mo2065a(this.f796l);
    }

    /* renamed from: a */
    public void a(@NonNull C0306a aVar) {
        this.f789e.mo894a(aVar);
    }

    /* renamed from: a */
    public synchronized void mo778a(@NonNull fs fsVar) {
        this.f794j.mo797a(fsVar);
    }

    /* renamed from: B */
    public synchronized void mo783b(@NonNull fs fsVar) {
        this.f794j.mo798b(fsVar);
    }

    /* renamed from: a */
    public void mo781a(@NonNull w wVar, @NonNull fs fsVar) {
        this.f791g.mo963a(wVar, fsVar);
    }

    @NonNull
    /* renamed from: a */
    public C0306a mo774a() {
        return this.f789e.mo893a();
    }

    @NonNull
    /* renamed from: B */
    public ek b() {
        return this.f786b;
    }

    /* renamed from: a */
    public void a(@Nullable uk ukVar) {
        m1430b(ukVar);
        if (ukVar != null) {
            if (this.f797m == null) {
                this.f797m = al.m324a().mo287e();
            }
            this.f797m.a(ukVar);
        }
    }

    /* renamed from: a */
    public void a(@NonNull ue ueVar, @Nullable uk ukVar) {
        synchronized (this.f798n) {
            for (ap c : this.f793i) {
                x.m4409a(c.mo304c(), ueVar, ukVar);
            }
            this.f793i.clear();
        }
    }

    /* renamed from: B */
    private void m1430b(uk ukVar) {
        synchronized (this.f798n) {
            for (uh a : this.f794j.mo796a()) {
                a.a(ukVar);
            }
            ArrayList arrayList = new ArrayList();
            for (ap apVar : this.f793i) {
                if (apVar.mo302a(ukVar)) {
                    x.m4410a(apVar.mo304c(), ukVar);
                } else {
                    arrayList.add(apVar);
                }
            }
            this.f793i = new ArrayList(arrayList);
            if (!arrayList.isEmpty()) {
                this.f792h.mo659e();
            }
        }
    }

    /* renamed from: a */
    public void c() {
        cx.a((Closeable) this.f792h);
        this.f790f.mo2611b();
    }

    @NonNull
    /* renamed from: d */
    public Context mo785d() {
        return this.f785a;
    }

    /* renamed from: a */
    public void mo775a(@Nullable ap apVar) {
        ResultReceiver resultReceiver;
        List list;
        Map hashMap = new HashMap();
        if (apVar != null) {
            List a = apVar.mo301a();
            resultReceiver = apVar.mo304c();
            hashMap = apVar.mo303b();
            list = a;
        } else {
            resultReceiver = null;
            list = null;
        }
        boolean a2 = this.f788d.mo2402a(list, hashMap);
        if (!a2) {
            x.m4410a(resultReceiver, this.f788d.mo2406e());
        }
        if (this.f788d.mo2404c()) {
            synchronized (this.f798n) {
                if (a2 && apVar != null) {
                    this.f793i.add(apVar);
                }
            }
            this.f792h.mo659e();
            return;
        }
        x.m4410a(resultReceiver, this.f788d.mo2406e());
    }

    /* renamed from: e */
    public si mo786e() {
        return this.f795k;
    }

    /* renamed from: a */
    public void a(@NonNull eg egVar) {
        this.f788d.mo2397a(egVar.f765a);
        a(egVar.f766b);
    }
}
