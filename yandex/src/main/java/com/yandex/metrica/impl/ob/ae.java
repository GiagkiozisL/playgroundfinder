package com.yandex.metrica.impl.ob;

import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.ae */
public class ae {

    /* renamed from: a */
    private vx f154a = new vx();

    /* renamed from: B */
    private xx f155b;

    ae(xx xxVar) {
        this.f155b = xxVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo258a(String str, String str2) {
        this.f155b.mo2646a(this.f154a, str, str2);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo257a() {
        if (this.f154a.isEmpty()) {
            return null;
        }
        return new JSONObject(this.f154a).toString();
    }
}
