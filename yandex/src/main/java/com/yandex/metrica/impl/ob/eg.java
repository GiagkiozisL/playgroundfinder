package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.su.C0990a;

import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.eg */
public class eg {

    /* renamed from: a */
    public final C0990a f765a;

    /* renamed from: B */
    public final C0306a f766b;
    @Nullable

    /* renamed from: a */
    public final ResultReceiver f767c;

    /* renamed from: com.yandex.metrica.impl.ob.eg$a */
    public static class C0306a implements sm<C0306a, C0306a> {
        @Nullable

        /* renamed from: a */
        public final String f768a;
        @Nullable

        /* renamed from: B */
        public final String f769b;
        @Nullable

        /* renamed from: a */
        public final String f770c;
        @Nullable

        /* renamed from: d */
        public final String f771d;
        @Nullable

        /* renamed from: e */
        public final Boolean f772e;
        @Nullable

        /* renamed from: f */
        public final Location f773f;
        @Nullable

        /* renamed from: g */
        public final Boolean f774g;
        @Nullable

        /* renamed from: h */
        public final Boolean f775h;
        @Nullable

        /* renamed from: i */
        public final Integer f776i;
        @Nullable

        /* renamed from: j */
        public final Integer f777j;
        @Nullable

        /* renamed from: k */
        public final Integer f778k;
        @Nullable

        /* renamed from: l */
        public final Boolean f779l;
        @Nullable

        /* renamed from: m */
        public final Boolean f780m;
        @Nullable

        /* renamed from: n */
        public final Boolean f781n;
        @Nullable

        /* renamed from: o */
        public final Map<String, String> f782o;
        @Nullable

        /* renamed from: p */
        public final Integer f783p;
        @Nullable

        /* renamed from: q */
        public final Boolean f784q;

        C0306a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map, @Nullable Integer num4, @Nullable Boolean bool7) {
            this.f768a = str;
            this.f769b = str2;
            this.f770c = str3;
            this.f771d = str4;
            this.f772e = bool;
            this.f773f = location;
            this.f774g = bool2;
            this.f775h = bool3;
            this.f781n = bool4;
            this.f776i = num;
            this.f777j = num2;
            this.f778k = num3;
            this.f779l = bool5;
            this.f780m = bool6;
            this.f782o = map;
            this.f783p = num4;
            this.f784q = bool7;
        }

        public C0306a(@NonNull CounterConfiguration counterConfiguration, @Nullable Map<String, String> map) {
            this(counterConfiguration.mo34d(), counterConfiguration.mo43g(), counterConfiguration.mo45h(), counterConfiguration.mo39e(), counterConfiguration.mo41f(), counterConfiguration.mo48k(), counterConfiguration.mo51n(), counterConfiguration.mo49l(), counterConfiguration.mo46i(), counterConfiguration.mo30c(), counterConfiguration.mo25b(), counterConfiguration.mo18a(), counterConfiguration.mo47j(), counterConfiguration.mo53p(), map, counterConfiguration.mo52o(), counterConfiguration.mo50m());
        }

        public C0306a() {
            this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }

        @NonNull
        /* renamed from: a */
        public C0306a b(@NonNull C0306a aVar) {
            return new C0306a((String) wk.m4369a(this.f768a, aVar.f768a), (String) wk.m4369a(this.f769b, aVar.f769b), (String) wk.m4369a(this.f770c, aVar.f770c), (String) wk.m4369a(this.f771d, aVar.f771d), (Boolean) wk.m4369a(this.f772e, aVar.f772e), (Location) wk.m4369a(this.f773f, aVar.f773f), (Boolean) wk.m4369a(this.f774g, aVar.f774g), (Boolean) wk.m4369a(this.f775h, aVar.f775h), (Boolean) wk.m4369a(this.f781n, aVar.f781n), (Integer) wk.m4369a(this.f776i, aVar.f776i), (Integer) wk.m4369a(this.f777j, aVar.f777j), (Integer) wk.m4369a(this.f778k, aVar.f778k), (Boolean) wk.m4369a(this.f779l, aVar.f779l), (Boolean) wk.m4369a(this.f780m, aVar.f780m), (Map) wk.m4369a(this.f782o, aVar.f782o), (Integer) wk.m4369a(this.f783p, aVar.f783p), (Boolean) wk.m4369a(this.f784q, aVar.f784q));
        }

        /* renamed from: B */
        public boolean a(@NonNull C0306a aVar) {
            return equals(aVar);
        }

        public boolean equals(Object o) {
            boolean z = true;
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            C0306a aVar = (C0306a) o;
            if (this.f768a != null) {
                if (!this.f768a.equals(aVar.f768a)) {
                    return false;
                }
            } else if (aVar.f768a != null) {
                return false;
            }
            if (this.f769b != null) {
                if (!this.f769b.equals(aVar.f769b)) {
                    return false;
                }
            } else if (aVar.f769b != null) {
                return false;
            }
            if (this.f770c != null) {
                if (!this.f770c.equals(aVar.f770c)) {
                    return false;
                }
            } else if (aVar.f770c != null) {
                return false;
            }
            if (this.f771d != null) {
                if (!this.f771d.equals(aVar.f771d)) {
                    return false;
                }
            } else if (aVar.f771d != null) {
                return false;
            }
            if (this.f772e != null) {
                if (!this.f772e.equals(aVar.f772e)) {
                    return false;
                }
            } else if (aVar.f772e != null) {
                return false;
            }
            if (this.f773f != null) {
                if (!this.f773f.equals(aVar.f773f)) {
                    return false;
                }
            } else if (aVar.f773f != null) {
                return false;
            }
            if (this.f774g != null) {
                if (!this.f774g.equals(aVar.f774g)) {
                    return false;
                }
            } else if (aVar.f774g != null) {
                return false;
            }
            if (this.f775h != null) {
                if (!this.f775h.equals(aVar.f775h)) {
                    return false;
                }
            } else if (aVar.f775h != null) {
                return false;
            }
            if (this.f776i != null) {
                if (!this.f776i.equals(aVar.f776i)) {
                    return false;
                }
            } else if (aVar.f776i != null) {
                return false;
            }
            if (this.f777j != null) {
                if (!this.f777j.equals(aVar.f777j)) {
                    return false;
                }
            } else if (aVar.f777j != null) {
                return false;
            }
            if (this.f778k != null) {
                if (!this.f778k.equals(aVar.f778k)) {
                    return false;
                }
            } else if (aVar.f778k != null) {
                return false;
            }
            if (this.f779l != null) {
                if (!this.f779l.equals(aVar.f779l)) {
                    return false;
                }
            } else if (aVar.f779l != null) {
                return false;
            }
            if (this.f780m != null) {
                if (!this.f780m.equals(aVar.f780m)) {
                    return false;
                }
            } else if (aVar.f780m != null) {
                return false;
            }
            if (this.f781n != null) {
                if (!this.f781n.equals(aVar.f781n)) {
                    return false;
                }
            } else if (aVar.f781n != null) {
                return false;
            }
            if (this.f782o != null) {
                if (!this.f782o.equals(aVar.f782o)) {
                    return false;
                }
            } else if (aVar.f782o != null) {
                return false;
            }
            if (this.f783p != null) {
                if (!this.f783p.equals(aVar.f783p)) {
                    return false;
                }
            } else if (aVar.f783p != null) {
                return false;
            }
            if (this.f784q != null) {
                z = this.f784q.equals(aVar.f784q);
            } else if (aVar.f784q != null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i;
            int i2;
            int i3;
            int i4;
            int i5;
            int i6;
            int i7;
            int i8;
            int i9;
            int i10;
            int i11;
            int i12;
            int i13;
            int i14;
            int i15;
            int i16 = 0;
            int hashCode = (this.f768a != null ? this.f768a.hashCode() : 0) * 31;
            if (this.f769b != null) {
                i = this.f769b.hashCode();
            } else {
                i = 0;
            }
            int i17 = (i + hashCode) * 31;
            if (this.f770c != null) {
                i2 = this.f770c.hashCode();
            } else {
                i2 = 0;
            }
            int i18 = (i2 + i17) * 31;
            if (this.f771d != null) {
                i3 = this.f771d.hashCode();
            } else {
                i3 = 0;
            }
            int i19 = (i3 + i18) * 31;
            if (this.f772e != null) {
                i4 = this.f772e.hashCode();
            } else {
                i4 = 0;
            }
            int i20 = (i4 + i19) * 31;
            if (this.f773f != null) {
                i5 = this.f773f.hashCode();
            } else {
                i5 = 0;
            }
            int i21 = (i5 + i20) * 31;
            if (this.f774g != null) {
                i6 = this.f774g.hashCode();
            } else {
                i6 = 0;
            }
            int i22 = (i6 + i21) * 31;
            if (this.f775h != null) {
                i7 = this.f775h.hashCode();
            } else {
                i7 = 0;
            }
            int i23 = (i7 + i22) * 31;
            if (this.f776i != null) {
                i8 = this.f776i.hashCode();
            } else {
                i8 = 0;
            }
            int i24 = (i8 + i23) * 31;
            if (this.f777j != null) {
                i9 = this.f777j.hashCode();
            } else {
                i9 = 0;
            }
            int i25 = (i9 + i24) * 31;
            if (this.f778k != null) {
                i10 = this.f778k.hashCode();
            } else {
                i10 = 0;
            }
            int i26 = (i10 + i25) * 31;
            if (this.f779l != null) {
                i11 = this.f779l.hashCode();
            } else {
                i11 = 0;
            }
            int i27 = (i11 + i26) * 31;
            if (this.f780m != null) {
                i12 = this.f780m.hashCode();
            } else {
                i12 = 0;
            }
            int i28 = (i12 + i27) * 31;
            if (this.f781n != null) {
                i13 = this.f781n.hashCode();
            } else {
                i13 = 0;
            }
            int i29 = (i13 + i28) * 31;
            if (this.f782o != null) {
                i14 = this.f782o.hashCode();
            } else {
                i14 = 0;
            }
            int i30 = (i14 + i29) * 31;
            if (this.f783p != null) {
                i15 = this.f783p.hashCode();
            } else {
                i15 = 0;
            }
            int i31 = (i15 + i30) * 31;
            if (this.f784q != null) {
                i16 = this.f784q.hashCode();
            }
            return i31 + i16;
        }
    }

    public eg(@NonNull ed edVar) {
        this(new C0990a(edVar), new C0306a(edVar.mo744h(), edVar.mo743g().mo753c()), edVar.mo743g().mo760i());
    }

    public eg(@NonNull C0990a aVar, @NonNull C0306a aVar2, @Nullable ResultReceiver resultReceiver) {
        this.f765a = aVar;
        this.f766b = aVar2;
        this.f767c = resultReceiver;
    }
}
