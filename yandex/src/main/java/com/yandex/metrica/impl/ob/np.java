package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rs.a;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.np */
public interface np<T> {

    /* renamed from: com.yandex.metrica.impl.ob.np$a */
    public static class C0678a {

        /* renamed from: a */
        private final HashMap<Class<?>, np<?>> f1528a;

        /* renamed from: B */
        private final np<uk> f1529b;

        /* renamed from: a */
        private final np<a> f1530c;

        /* renamed from: d */
        private final np<List<pu>> f1531d;

        /* renamed from: e */
        private final np<pn> f1532e;

        /* renamed from: f */
        private final np<te> f1533f;

        /* renamed from: com.yandex.metrica.impl.ob.np$a$a */
        private static final class C0684a {

            /* renamed from: a */
            static final C0678a f1539a = new C0678a();
        }

        /* renamed from: a */
        public static <T> np<T> m2599a(Class<T> cls) {
            return C0684a.f1539a.mo1503c(cls);
        }

        /* renamed from: B */
        public static <T> np<Collection<T>> m2600b(Class<T> cls) {
            return C0684a.f1539a.mo1504d(cls);
        }

        private C0678a() {
            this.f1528a = new HashMap<>();
            this.f1529b = new np<uk>() {
                /* renamed from: a */
                public mf<uk> a(@NonNull Context context) {
                    return new mg("startup_state", ld.m2146a(context).mo1235b(), new no(context).mo1497a(), new nf());
                }
            };
            this.f1530c = new np<a>() {
                /* renamed from: a */
                public mf<a> a(@NonNull Context context) {
                    return new mg("provided_request_state", ld.m2146a(context).mo1235b(), new no(context).mo1501e(), new na());
                }
            };
            this.f1531d = new np<List<pu>>() {
                /* renamed from: a */
                public mf<List<pu>> a(@NonNull Context context) {
                    return new mg("permission_list", ld.m2146a(context).mo1235b(), new no(context).mo1498b(), new my());
                }
            };
            this.f1532e = new np<pn>() {
                /* renamed from: a */
                public mf<pn> a(@NonNull Context context) {
                    return new mg("app_permissions_state", ld.m2146a(context).mo1235b(), new no(context).mo1499c(), new mm());
                }
            };
            this.f1533f = new np<te>() {
                /* renamed from: a */
                public mf<te> a(@NonNull Context context) {
                    return new mg("sdk_fingerprinting", ld.m2146a(context).mo1235b(), new no(context).mo1500d(), new nd());
                }
            };
            this.f1528a.put(uk.class, this.f1529b);
            this.f1528a.put(a.class, this.f1530c);
            this.f1528a.put(pu.class, this.f1531d);
            this.f1528a.put(pn.class, this.f1532e);
            this.f1528a.put(te.class, this.f1533f);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public <T> np<T> mo1503c(Class<T> cls) {
            return (np) this.f1528a.get(cls);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: d */
        public <T> np<Collection<T>> mo1504d(Class<T> cls) {
            return (np) this.f1528a.get(cls);
        }
    }

    /* renamed from: a */
    mf<T> a(@NonNull Context context);
}
