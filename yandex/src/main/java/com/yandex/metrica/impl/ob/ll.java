package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.ll */
public class ll implements lk {

    /* renamed from: a */
    private final String f1326a;

    /* renamed from: B */
    private final HashMap<String, List<String>> f1327b;

    public ll(@NonNull String str, @NonNull HashMap<String, List<String>> hashMap) {
        this.f1326a = str;
        this.f1327b = hashMap;
    }

    /* renamed from: a */
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        boolean z = true;
        try {
            for (Entry entry : this.f1327b.entrySet()) {
                Cursor query = sQLiteDatabase.query((String) entry.getKey(), null, null, null, null, null, null);
                if (query == null) {
                    cx.m1183a(query);
                    return false;
                }
                try {
                    z &= mo1268a(query, (String) entry.getKey(), (List) entry.getValue());
                    cx.m1183a(query);
                } catch (Throwable th) {
                    th = th;
                    cursor = query;
                    cx.m1183a(cursor);
                    throw th;
                }
            }
            return z;
        } catch (Throwable th2) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public boolean mo1268a(@NonNull Cursor cursor, @NonNull String str, @NonNull List<String> list) {
        List asList = Arrays.asList(cursor.getColumnNames());
        Collections.sort(asList);
        boolean equals = list.equals(asList);
        if (!equals) {
        }
        return equals;
    }
}
