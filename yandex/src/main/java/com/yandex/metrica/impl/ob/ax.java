package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.browser.crashreports.a.C0002a;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.a;
import com.yandex.metrica.j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.yandex.metrica.impl.ob.ax */
public class ax extends n implements ba {

    /* renamed from: f */
    private static final yk<String> f266f = new yg(new ye("Deeplink"));
    @Deprecated

    /* renamed from: g */
    private static final yk<String> f267g = new yg(new ye("Referral url"));
    @NonNull

    /* renamed from: h */
    private final a f268h;
    @NonNull

    /* renamed from: i */
    private final sg f269i;
    @NonNull

    /* renamed from: j */
    private final j f270j;
    @NonNull

    /* renamed from: k */
    private final uf f271k;
    @NonNull

    /* renamed from: l */
    private com.yandex.browser.crashreports.a f272l;

    /* renamed from: m */
    private final AtomicBoolean f273m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public final dq f274n;

//    public ax(@NonNull Context context, @NonNull ee eeVar, @NonNull j jVar, @NonNull cd cdVar, @NonNull uf ufVar, @NonNull cb cbVar, @NonNull cb cbVar2) {
//        long intValue;
//        bz bzVar = new bz(eeVar, new CounterConfiguration(jVar));
//        if (jVar.sessionTimeout == null) {
//            intValue = TimeUnit.SECONDS.toMillis(10);
//        } else {
//            intValue = (long) jVar.sessionTimeout.intValue();
//        }
//        this(context, jVar, cdVar, bzVar, new a(intValue), new sg(context), ufVar, new av(), cbVar, cbVar2, db.m1276a(), new ag(context));
//    }

    public ax(@NonNull Context var1, @NonNull ee var2, @NonNull j var3, @NonNull cd var4, @NonNull uf var5, @NonNull cb var6, @NonNull cb var7) {
        this(var1, var3, var4, new bz(var2, new CounterConfiguration(var3)), new a(var3.sessionTimeout == null ? TimeUnit.SECONDS.toMillis(10L) : (long)var3.sessionTimeout), new sg(var1), var5, new av(), var6, var7, db.m1276a(), new ag(var1));
    }

    @VisibleForTesting
    ax(@NonNull Context context, @NonNull j jVar, @NonNull cd cdVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull sg sgVar, @NonNull uf ufVar, @NonNull av avVar, @NonNull cb cbVar, @NonNull cb cbVar2, @NonNull xh xhVar, @NonNull ag agVar) {
        super(context, cdVar, bzVar, agVar);
        this.f273m = new AtomicBoolean(false);
        this.f274n = new dq();
        this.f1492b.mo542a(new bu(jVar.preloadInfo, this.f1493c));
        this.f268h = aVar;
        this.f269i = sgVar;
        this.f270j = jVar;
        this.f271k = ufVar;
        boolean a = wk.m4371a(jVar.nativeCrashReporting, true);
        this.f1495e.mo581a(a, this.f1492b);
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2478a("Set report native crashes enabled: %B", Boolean.valueOf(a));
        }
        this.f269i.mo2063a(aVar, this.f270j, this.f270j.m, ufVar.mo2345b(), this.f1493c);
        this.f272l = m408a(xhVar, avVar, cbVar, cbVar2);
        if (vi.m4192a(jVar.l)) {
            mo337g();
        }
    }

    public void reportError(String message, Throwable error) {
        super.reportError(message, error);
    }

    /* renamed from: g */
    public final void mo337g() {
        if (this.f273m.compareAndSet(false, true)) {
            this.f272l.mo1a();
        }
    }

    /* renamed from: a */
    public void mo329a(Activity activity) {
        if (activity != null) {
            if (activity.getIntent() != null) {
                String dataString = activity.getIntent().getDataString();
                if (!TextUtils.isEmpty(dataString)) {
                    this.f1495e.mo572a(af.m295g(dataString, this.f1493c), this.f1492b);
                }
                m411g(dataString);
            }
        } else if (this.f1493c.mo2485c()) {
            this.f1493c.mo2481b("Null activity parameter for reportAppOpen(Activity)");
        }
    }

    /* renamed from: g */
    private void m411g(@Nullable String str) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("App opened ");
            sb.append(" via deeplink: ");
            sb.append(mo1465d(str));
            this.f1493c.mo2477a(sb.toString());
        }
    }

    /* renamed from: e */
    public void mo335e(String str) {
        f266f.a(str);
        this.f1495e.mo572a(af.m295g(str, this.f1493c), this.f1492b);
        m411g(str);
    }

    @Deprecated
    /* renamed from: f */
    public void mo336f(String str) {
        f267g.a(str);
        this.f1495e.mo572a(af.m296h(str, this.f1493c), this.f1492b);
        m412h(str);
    }

    @Deprecated
    /* renamed from: h */
    private void m412h(@Nullable String str) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Referral URL received: ");
            sb.append(mo1465d(str));
            this.f1493c.mo2477a(sb.toString());
        }
    }

    /* renamed from: a */
    public void mo330a(Application application, @NonNull xh xhVar) {
        if (VERSION.SDK_INT >= 14) {
            if (this.f1493c.mo2485c()) {
                this.f1493c.mo2477a("Enable activity auto tracking");
            }
            m410b(application, xhVar);
        } else if (this.f1493c.mo2485c()) {
            this.f1493c.mo2481b("Could not enable activity auto tracking. API level should be more than 14 (ICE_CREAM_SANDWICH)");
        }
    }

    @TargetApi(14)
    /* renamed from: B */
    private void m410b(Application application, @NonNull xh xhVar) {
        application.registerActivityLifecycleCallbacks(new z(this, xhVar));
    }

    /* renamed from: B */
    public void mo332b(Activity activity) {
        mo1455a(mo334d(activity));
        this.f268h.mo141a();
    }

    /* renamed from: a */
    public void mo333c(Activity activity) {
        mo1458b(mo334d(activity));
        this.f268h.mo142b();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public String mo334d(Activity activity) {
        if (activity != null) {
            return activity.getClass().getSimpleName();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo331a(j jVar, boolean z) {
        if (z) {
            mo1457b();
        }
        mo1460b(jVar.i);
        mo1456a(jVar.h);
    }

    /* renamed from: a */
    public void a(Location location) {
        this.f1492b.mo744h().mo20a(location);
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2478a("Set location: %s" + location.toString(), new Object[0]);
        }
    }

    /* renamed from: a */
    public void a(boolean z) {
        this.f1492b.mo744h().mo24a(z);
    }

    @NonNull
    /* renamed from: a */
    private com.yandex.browser.crashreports.a m408a(@NonNull xh xhVar, @NonNull av avVar, @NonNull cb cbVar, @NonNull cb cbVar2) {
        final xh xhVar2 = xhVar;
        final av avVar2 = avVar;
        final cb cbVar3 = cbVar;
        final cb cbVar4 = cbVar2;
        return new com.yandex.browser.crashreports.a(new C0002a() {
            /* renamed from: a */
            public void mo4a() {
                final kg a = ax.this.f274n.mo717a();
                xhVar2.a((Runnable) new Runnable() {
                    public void run() {
                        ax.this.a(a);
                        if (avVar2.mo325a(a.f1202a.f1215f)) {
                            cbVar3.mo556a().a(a);
                        }
                        if (avVar2.mo326b(a.f1202a.f1215f)) {
                            cbVar4.mo556a().a(a);
                        }
                    }
                });
            }
        });
    }
}
