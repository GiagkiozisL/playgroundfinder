package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.ro.C0852a;
import com.yandex.metrica.impl.ob.ro.C0852a.C0853a;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.my */
public class my implements mq<List<pu>, C0852a> {
    @NonNull
    /* renamed from: a */
    public C0852a b(@NonNull List<pu> list) {
        C0852a aVar = new C0852a();
        aVar.f2068b = new C0853a[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return aVar;
            }
            aVar.f2068b[i2] = m2478a((pu) list.get(i2));
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    public List<pu> a(@NonNull C0852a aVar) {
        ArrayList arrayList = new ArrayList(aVar.f2068b.length);
        for (C0853a a : aVar.f2068b) {
            arrayList.add(m2477a(a));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    private C0853a m2478a(@NonNull pu puVar) {
        C0853a aVar = new C0853a();
        aVar.f2070b = puVar.name;
        aVar.f2071c = puVar.granted;
        return aVar;
    }

    @NonNull
    /* renamed from: a */
    private pu m2477a(@NonNull C0853a aVar) {
        return new pu(aVar.f2070b, aVar.f2071c);
    }
}
