package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.su.C0990a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.uc */
public class uc {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final wp<String, uh> f2713a = new wp<>();
    /* access modifiers changed from: private */

    /* renamed from: B */
    public final HashMap<String, ul> f2714b = new HashMap<>();

    /* renamed from: a */
    private final uj f2715c = new uj() {
        /* renamed from: a */
        public void a(@NonNull String str, @NonNull uk ukVar) {
            for (uh a : mo2331a(str)) {
                a.a(ukVar);
            }
        }

        /* renamed from: a */
        public void a(@NonNull String str, @NonNull ue ueVar, @Nullable uk ukVar) {
            for (uh a : mo2331a(str)) {
                a.a(ueVar, ukVar);
            }
        }

        @NonNull
        /* renamed from: a */
        public List<uh> mo2331a(@NonNull String str) {
            ArrayList arrayList;
            synchronized (uc.this.f2714b) {
                Collection a = uc.this.f2713a.mo2571a(str);
                if (a == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = new ArrayList(a);
                }
            }
            return arrayList;
        }
    };

    /* renamed from: com.yandex.metrica.impl.ob.uc$a */
    private static final class C1064a {

        /* renamed from: a */
        static final uc f2717a = new uc();
    }

    /* renamed from: a */
    public static final uc m3891a() {
        return C1064a.f2717a;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public ul mo2328a(@NonNull Context context, @NonNull ek ekVar, @NonNull C0990a aVar) {
        boolean z = true;
        ul ulVar = (ul) this.f2714b.get(ekVar.mo791b());
        if (ulVar == null) {
            synchronized (this.f2714b) {
                ulVar = (ul) this.f2714b.get(ekVar.mo791b());
                if (ulVar == null) {
                    z = false;
                    ulVar = mo2330b(context, ekVar, aVar);
                    this.f2714b.put(ekVar.mo791b(), ulVar);
                }
            }
        }
        if (z) {
            ulVar.mo2397a(aVar);
        }
        return ulVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: B */
    public ul mo2330b(@NonNull Context context, @NonNull ek ekVar, @NonNull C0990a aVar) {
        return new ul(context, ekVar.mo791b(), aVar, this.f2715c);
    }

    @NonNull
    /* renamed from: a */
    public ul mo2329a(@NonNull Context context, @NonNull ek ekVar, @NonNull uh uhVar, @NonNull C0990a aVar) {
        ul a;
        synchronized (this.f2714b) {
            this.f2713a.mo2572a(ekVar.mo791b(), uhVar);
            a = mo2328a(context, ekVar, aVar);
        }
        return a;
    }
}
