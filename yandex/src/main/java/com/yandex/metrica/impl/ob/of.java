package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.of */
abstract class of implements oe {
    @Nullable

    /* renamed from: a */
    private oe f1569a;

    /* renamed from: B */
    public abstract void mo1541b(@Nullable String str, @Nullable Location location, @Nullable oh ohVar);

    public of(@Nullable oe oeVar) {
        this.f1569a = oeVar;
    }

    /* renamed from: a */
    public void a(@Nullable String str, @Nullable Location location, @Nullable oh ohVar) {
        mo1541b(str, location, ohVar);
        if (this.f1569a != null) {
            this.f1569a.a(str, location, ohVar);
        }
    }
}
