package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.cr */
public class cr extends bs<ta> {

    /* renamed from: j */
    private final ul f627j;

    /* renamed from: k */
    private boolean f628k;

    /* renamed from: l */
    private ue f629l;
    @NonNull

    /* renamed from: m */
    private final su f630m;

    public cr(ul ulVar, su suVar) {
        this(ulVar, suVar, new ta(new ss()));
    }

    public cr(ul ulVar, su suVar, @NonNull ta taVar) {
        super(new cp(ulVar, suVar), taVar);
        this.f628k = false;
        this.f627j = ulVar;
        this.f630m = suVar;
        mo457a(this.f630m.mo2189a());
    }

    /* renamed from: a */
    public boolean mo461a() {
        if (this.f387h >= 0) {
            return false;
        }
        mo649b(false);
        mo455a("Accept-Encoding", "encrypted");
        return this.f627j.mo2404c();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo451a(@NonNull Builder builder) {
        ((ta) this.f388i).mo2218a(builder, this.f630m);
    }

    /* renamed from: B */
    public boolean mo463b() {
        if (mo648E()) {
            return true;
        }
        if (200 != this.f384e) {
            return false;
        }
        boolean b = super.mo463b();
        if (b) {
            return b;
        }
        this.f629l = ue.PARSE;
        return b;
    }

    /* renamed from: a */
    public void a(Throwable th) {
        this.f629l = ue.NETWORK;
    }

    /* renamed from: g */
    public void g() {
        super.g();
        this.f629l = ue.NETWORK;
    }

    /* renamed from: f */
    public void f() {
        if (!mo486x() && mo487y()) {
            if (this.f629l == null) {
                this.f629l = ue.UNKNOWN;
            }
            this.f627j.mo2398a(this.f629l);
        }
    }

    /* renamed from: B */
    public synchronized void mo649b(boolean z) {
        this.f628k = z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: E */
    public synchronized boolean mo648E() {
        return this.f628k;
    }

    /* renamed from: o */
    public boolean mo477o() {
        return true;
    }

    @NonNull
    /* renamed from: n */
    public String mo476n() {
        return "Startup task for component: " + this.f627j.b().toString();
    }
}
