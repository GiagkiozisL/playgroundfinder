package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.si.C0964a;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.ez */
public class ez extends en {
    @NonNull

    /* renamed from: B */
    private final si f869b;
    @NonNull

    /* renamed from: a */
    private final C0343a f870c;
    @NonNull

    /* renamed from: d */
    private final cs f871d;
    @NonNull

    /* renamed from: e */
    private final bj f872e;
    @NonNull

    /* renamed from: f */
    private final ku f873f;
    @NonNull

    /* renamed from: g */
    private final bk f874g;

    /* renamed from: com.yandex.metrica.impl.ob.ez$a */
    public class C0343a implements C0964a {
        public C0343a() {
        }

        /* renamed from: a */
        public boolean mo787a(@NonNull sj sjVar) {
            ez.this.a(new w().mo1764a(sjVar.mo2068a()).mo2520a(C0058a.EVENT_TYPE_SEND_REFERRER.mo259a()));
            return true;
        }
    }

//    public ez(@NonNull Context context, @NonNull uk ukVar, @NonNull bl blVar, @NonNull ek ekVar, @NonNull C0306a aVar, @NonNull si siVar, @NonNull cs csVar, @NonNull un unVar) {
//        C0325a aVar2 = new C0325a();
//        cw cwVar = new cw();
//        ah ahVar = new ah();
//        fa faVar = new fa(context, ekVar, aVar, unVar, ukVar, new ey(csVar), blVar, al.m324a().mo292j().mo2632g(), cx.a(context, ekVar.mo791b()));
//        this(context, ekVar, aVar2, cwVar, ahVar, faVar, siVar, csVar, aVar.f784q);
//    }

    public ez(@NonNull Context var1, @NonNull uk var2, @NonNull bl var3, @NonNull ek var4, @NonNull com.yandex.metrica.impl.ob.eg.C0306a var5, @NonNull si var6, @NonNull cs var7, @NonNull un var8) {
        this(var1, var4, new com.yandex.metrica.impl.ob.en.C0325a(), new cw(), new ah(), new fa(var1, var4, var5, var8, var2, new ey(var7), var3, al.m324a().mo292j().mo2632g(), cx.c(var1, var4.mo791b())), var6, var7, var5.f784q);
    }

    @VisibleForTesting
    ez(@NonNull Context context, @NonNull ek ekVar, @NonNull C0325a aVar, @NonNull cw cwVar, @NonNull ah ahVar, @NonNull fa faVar, @NonNull si siVar, @NonNull cs csVar, @Nullable Boolean bool) {
        super(context, ekVar, aVar, cwVar, faVar);
        this.f869b = siVar;
        gp f = mo819f();
        f.mo958a(C0058a.EVENT_TYPE_REGULAR, new ie(f.mo957a()));
        this.f870c = new C0343a();// faVar.a(this);
        this.f869b.mo2065a((C0964a) this.f870c);
        this.f871d = csVar;
        this.f874g = faVar.mo873b(this);
        this.f872e = faVar.mo870a(this.f874g, mo838y());
        this.f873f = faVar.mo872a(ahVar, (wm<File>) new wm<File>() {
            /* renamed from: a */
            public void a(File file) {
                ez.this.m1554a(file);
            }
        });
        if (Boolean.TRUE.equals(bool)) {
            this.f873f.mo1165a();
            this.f872e.mo430a();
        }
    }

    /* renamed from: a */
    public synchronized void a(@NonNull C0306a aVar) {
        super.a(aVar);
        m1552D();
        this.f871d.mo650a(aVar.f780m);
    }

    /* renamed from: D */
    private void m1552D() {
        this.f826a.mo1372a(mo822i().mo2141R()).mo1364q();
    }

    /* renamed from: a */
    public void c() {
        this.f873f.mo1166b();
        super.c();
    }

    @NonNull
    /* renamed from: a */
    public C0008a mo767a() {
        return C0008a.MAIN;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1554a(File file) {
        this.f872e.mo431a(file.getAbsolutePath(), new wm<Boolean>() {
            /* renamed from: a */
            public void a(Boolean bool) {
            }
        }, true);
    }
}
