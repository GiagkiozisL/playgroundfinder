package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.cc */
public class cc<C extends en> extends cv<C> {

    /* renamed from: d */
    private Runnable f497d = new Runnable() {
        public void run() {
            cc.this.mo659e();
        }
    };

    /* renamed from: e */
    private final xh f498e;

    public cc(@NonNull C c, @NonNull uq uqVar, @NonNull bl blVar, @NonNull xh xhVar) {
        super(c, uqVar, blVar);
        this.f498e = xhVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo557a() {
        this.f498e.b(this.f497d);
    }

    /* renamed from: B */
    public void mo558b() {
        synchronized (this.f638a) {
            if (!this.f640c) {
                mo557a();
                mo560d();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo559c() {
        super.mo559c();
        st i = ((en) mo661g()).mo822i();
        if (i.mo2147X() && cx.m1191a(i.getApiKey128())) { //data to be ovveride
            try {
                this.f639b.a((bo) by.m822J().mo541a((en) mo661g()));
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: d */
    public void mo560d() {
        if (((en) mo661g()).mo822i().mo2143T() > 0) {
            this.f498e.a(this.f497d, TimeUnit.SECONDS.toMillis((long) ((en) mo661g()).mo822i().mo2143T()));
        }
    }
}
