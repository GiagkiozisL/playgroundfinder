package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.kc */
public class kc {
    /* renamed from: a */
    public boolean mo1141a(@NonNull File file) {
        if (!file.exists()) {
            return mo1142b(file);
        }
        if (file.isDirectory()) {
            return true;
        }
        if (file.delete()) {
            return mo1142b(file);
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public boolean mo1142b(@NonNull File file) {
        if (file.mkdir()) {
            return true;
        }
        return false;
    }
}
