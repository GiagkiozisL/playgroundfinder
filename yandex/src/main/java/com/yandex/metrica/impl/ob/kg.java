package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.kg */
public class kg {
    @NonNull

    /* renamed from: a */
    public final kk f1202a;
    @NonNull

    /* renamed from: B */
    public final List<kk> f1203b;

    public kg(@NonNull kk kkVar, @Nullable List<kk> list) {
        this.f1202a = kkVar;
        this.f1203b = list == null ? new ArrayList<kk>() : Collections.unmodifiableList(list);
    }
}
