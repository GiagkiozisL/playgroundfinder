package com.yandex.metrica.impl.ob;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.rn */
public interface rn {

    /* renamed from: com.yandex.metrica.impl.ob.rn$a */
    public static final class C0850a extends e {

        /* renamed from: B */
        public boolean f2064b;

        /* renamed from: a */
        public int f2065c;

        /* renamed from: d */
        public int f2066d;

        /* renamed from: e */
        public int[] f2067e;

        public C0850a() {
            mo1847d();
        }

        /* renamed from: d */
        public C0850a mo1847d() {
            this.f2064b = false;
            this.f2065c = 0;
            this.f2066d = 0;
            this.f2067e = g.a;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            bVar.mo352a(1, this.f2064b);
            bVar.mo361b(2, this.f2065c);
            bVar.mo361b(3, this.f2066d);
            if (this.f2067e != null && this.f2067e.length > 0) {
                for (int a : this.f2067e) {
                    bVar.mo348a(4, a);
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int i = 0;
            int c = super.mo741c() + b.m438b(1, this.f2064b) + b.m449e(2, this.f2065c) + b.m449e(3, this.f2066d);
            if (this.f2067e == null || this.f2067e.length <= 0) {
                return c;
            }
            int i2 = 0;
            while (true) {
                int i3 = i;
                if (i2 >= this.f2067e.length) {
                    return c + i3 + (this.f2067e.length * 1);
                }
                i = b.m444d(this.f2067e[i2]) + i3;
                i2++;
            }
        }

        /* renamed from: B */
        public C0850a mo738a(a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case 8:
                        this.f2064b = aVar.mo228h();
                        continue;
                    case 16:
                        this.f2065c = aVar.mo231k();
                        continue;
                    case 24:
                        this.f2066d = aVar.mo231k();
                        continue;
                    case 32:
                        int b = g.b(aVar, 32);
                        int length = this.f2067e == null ? 0 : this.f2067e.length;
                        int[] iArr = new int[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.f2067e, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = aVar.mo225g();
                            aVar.mo213a();
                            length++;
                        }
                        iArr[length] = aVar.mo225g();
                        this.f2067e = iArr;
                        continue;
                    case 34:
                        int d = aVar.d(aVar.mo234n());
                        int t = aVar.mo240t();
                        int i = 0;
                        while (aVar.mo238r() > 0) {
                            aVar.mo225g();
                            i++;
                        }
                        aVar.mo224f(t);
                        int length2 = this.f2067e == null ? 0 : this.f2067e.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.f2067e, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = aVar.mo225g();
                            length2++;
                        }
                        this.f2067e = iArr2;
                        aVar.mo222e(d);
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }

        /* renamed from: a */
        public static C0850a m3226a(byte[] bArr) throws d {
            return (C0850a) e.m1393a(new C0850a(), bArr);
        }
    }
}
