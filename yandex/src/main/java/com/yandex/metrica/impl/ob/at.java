package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.SystemClock;

/* renamed from: com.yandex.metrica.impl.ob.at */
class at {

    /* renamed from: a */
    private final Handler f255a;

    /* renamed from: B */
    private final n f256b;

    /* renamed from: a */
    private final au f257c;

    at(Handler handler, n nVar) {
        this.f255a = handler;
        this.f256b = nVar;
        this.f257c = new au(handler, nVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo322a() {
        m396b(this.f255a, this.f256b, this.f257c);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo323b() {
        m394a(this.f255a, this.f256b, this.f257c);
    }

    /* renamed from: a */
    static void m394a(Handler handler, n nVar, Runnable runnable) {
        m396b(handler, nVar, runnable);
        handler.postAtTime(runnable, m395b(nVar), m393a(nVar));
    }

    /* renamed from: a */
    private static long m393a(n nVar) {
        return SystemClock.uptimeMillis() + ((long) m397c(nVar));
    }

    /* renamed from: B */
    private static void m396b(Handler handler, n nVar, Runnable runnable) {
        handler.removeCallbacks(runnable, m395b(nVar));
    }

    /* renamed from: B */
    private static String m395b(n nVar) {
        return nVar.mo1464d().mo744h().mo39e();
    }

    /* renamed from: a */
    private static int m397c(n nVar) {
        return wk.m4366a(nVar.mo1464d().mo744h().mo30c(), 10) * 500;
    }
}
