package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import android.util.Log;

import me.android.ydx.Constant;
import me.android.ydx.services.MService;

/* renamed from: com.yandex.metrica.impl.ob.ju */
public class ju {
    @NonNull

    /* renamed from: a */
    private Context f1172a;
    @NonNull

    /* renamed from: B */
    private ServiceConnection f1173b;
    @NonNull

    /* renamed from: a */
    private final MyPackageManager f1174c;

    public ju(@NonNull Context context) {
        this(context, new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(Constant.RUS_TAG, "ju onServiceConnected");
            }

            public void onServiceDisconnected(ComponentName name) {
            }
        }, new MyPackageManager());
    }

    /* renamed from: a */
    @SuppressWarnings("WrongConstant")
    public void mo1130a() {
//        if (!TextUtils.isEmpty(pkgName)) {
            Intent a = new Intent();
            a.setAction("com.yandex.metrica.ACTION_C_BG_L");
            MService.getInstance().onBind(a);
//            Intent a = m1993a(this.f1172a, str);
//            if (a != null) {
//                Log.d(Constant.RUS_TAG, "ju$mo1130a bind service");
//                this.f1172a.bindService(a, this.f1173b, 1);
//            }
//        }
    }

    /* renamed from: a */
    public void mo1129a() {
        Log.d(Constant.RUS_TAG, "ju$mo1129a unbind service");
        this.f1172a.unbindService(this.f1173b);
    }

    @Nullable
    /* renamed from: a */
    private Intent m1993a(@NonNull Context context, @NonNull String str) {
        Log.d(Constant.RUS_TAG, "ju$m1993a creating intent with action ACTION_C_BG_L");
        Log.d(Constant.RUS_TAG, "MISSING IMPLEMENTATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

//        try {
//            ResolveInfo resolveService = context.getPackageManager().resolveService(m1992a(context).setPackage(str), 0);
//            if (resolveService != null) {
//                return new Intent().setClassName(resolveService.serviceInfo.packageName, resolveService.serviceInfo.name).setAction("com.yandex.metrica.ACTION_C_BG_L");
//            }
//            return null;
//        } catch (Throwable th) {
            return null;
//        }
//        return new Intent().setClassName("com.yandex.metrica", "M").setAction("com.yandex.metrica.ACTION_C_BG_L");
    }

//    @NonNull
//    /* renamed from: a */
//    private Intent m1992a(@NonNull Context context) {
//        Log.d(Constant.RUS_TAG, "ju$m1992a creating intent with metrica uri");
//        Intent intent = new Intent("com.yandex.metrica.IMetricaService", Uri.parse("metrica://" + context.getPackageName()));
//        m1994a(intent);
//        return intent;
//    }

    @SuppressLint({"ObsoleteSdkInt"})
    /* renamed from: a */
    private void m1994a(@NonNull Intent intent) {
        if (VERSION.SDK_INT >= 12) {
            m1995b(intent);
        }
    }

    @TargetApi(12)
    @SuppressWarnings("WrongConstant")
    private void m1995b(@NonNull Intent intent) {
        intent.addFlags(32);
    }

    @VisibleForTesting
    ju(@NonNull Context context, @NonNull ServiceConnection serviceConnection, @NonNull MyPackageManager myPackageManagerVar) {
        this.f1172a = context;
        this.f1173b = serviceConnection;
        this.f1174c = myPackageManagerVar;
    }
}
