package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.iv */
public class iv extends it {
    iv(@NonNull en enVar, @NonNull jc jcVar) {
        this(enVar, jcVar, new jg(enVar.mo838y(), "foreground"));
    }

    @VisibleForTesting
    iv(@NonNull en enVar, @NonNull jc jcVar, @NonNull jg jgVar) {
        super(enVar, jcVar, jgVar, jb.m1893a(jh.FOREGROUND).mo1070a());
    }
}
