package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.SparseArray;

import com.yandex.metrica.impl.ob.ls.C0589a;
import com.yandex.metrica.impl.ob.ls.C0590aa;
import com.yandex.metrica.impl.ob.ls.C0591ab;
import com.yandex.metrica.impl.ob.ls.C0592b;
import com.yandex.metrica.impl.ob.ls.C0593c;
import com.yandex.metrica.impl.ob.ls.C0594d;
import com.yandex.metrica.impl.ob.ls.C0595e;
import com.yandex.metrica.impl.ob.ls.C0596f;
import com.yandex.metrica.impl.ob.ls.C0597g;
import com.yandex.metrica.impl.ob.ls.C0598h;
import com.yandex.metrica.impl.ob.ls.C0599i;
import com.yandex.metrica.impl.ob.ls.C0600j;
import com.yandex.metrica.impl.ob.ls.C0601k;
import com.yandex.metrica.impl.ob.ls.C0602l;
import com.yandex.metrica.impl.ob.ls.C0603m;
import com.yandex.metrica.impl.ob.ls.C0604n;
import com.yandex.metrica.impl.ob.ls.C0605o;
import com.yandex.metrica.impl.ob.ls.C0606p;
import com.yandex.metrica.impl.ob.ls.C0607q;
import com.yandex.metrica.impl.ob.ls.C0608r;
import com.yandex.metrica.impl.ob.ls.C0609s;
import com.yandex.metrica.impl.ob.ls.C0610t;
import com.yandex.metrica.impl.ob.ls.C0611u;
import com.yandex.metrica.impl.ob.ls.C0612v;
import com.yandex.metrica.impl.ob.ls.C0613w;
import com.yandex.metrica.impl.ob.ls.C0614x;
import com.yandex.metrica.impl.ob.ls.C0615y;
import com.yandex.metrica.impl.ob.ls.C0616z;

/* renamed from: com.yandex.metrica.impl.ob.lb */
public class lb {
    @NonNull

    /* renamed from: a */
    private final SparseArray<lr> f1284a = new SparseArray<>();
    @NonNull

    /* renamed from: B */
    private final SparseArray<lr> f1285b;
    @NonNull

    /* renamed from: a */
    private final lr f1286c;
    @NonNull

    /* renamed from: d */
    private final lr f1287d;
    @NonNull

    /* renamed from: e */
    private final lr f1288e;
    @NonNull

    /* renamed from: f */
    private final lr f1289f;
    @NonNull

    /* renamed from: g */
    private final lr f1290g;
    @NonNull

    /* renamed from: h */
    private final lr f1291h;

    public lb() {
        this.f1284a.put(6, new C0612v());
        this.f1284a.put(7, new C0616z());
        this.f1284a.put(14, new C0605o());
        this.f1284a.put(29, new C0606p());
        this.f1284a.put(37, new C0607q());
        this.f1284a.put(39, new C0608r());
        this.f1284a.put(45, new C0609s());
        this.f1284a.put(47, new C0610t());
        this.f1284a.put(50, new C0611u());
        this.f1284a.put(60, new C0613w());
        this.f1284a.put(66, new C0614x());
        this.f1284a.put(67, new C0615y());
        this.f1284a.put(73, new C0590aa());
        this.f1284a.put(77, new C0591ab());
        this.f1285b = new SparseArray<>();
        this.f1285b.put(12, new C0597g());
        this.f1285b.put(29, new C0598h());
        this.f1285b.put(47, new C0599i());
        this.f1285b.put(50, new C0600j());
        this.f1285b.put(55, new C0601k());
        this.f1285b.put(60, new C0602l());
        this.f1285b.put(63, new C0603m());
        this.f1285b.put(67, new C0604n());
        this.f1286c = new C0593c();
        this.f1287d = new C0594d();
        this.f1288e = new C0589a();
        this.f1289f = new C0592b();
        this.f1290g = new C0595e();
        this.f1291h = new C0596f();
    }

    @NonNull
    /* renamed from: a */
    public SparseArray<lr> mo1219a() {
        return this.f1284a;
    }

    @NonNull
    /* renamed from: B */
    public SparseArray<lr> mo1220b() {
        return this.f1285b;
    }

    @NonNull
    /* renamed from: a */
    public lr mo1221c() {
        return this.f1286c;
    }

    @NonNull
    /* renamed from: d */
    public lr mo1222d() {
        return this.f1287d;
    }

    @NonNull
    /* renamed from: e */
    public lr mo1223e() {
        return this.f1288e;
    }

    @NonNull
    /* renamed from: f */
    public lr mo1224f() {
        return this.f1289f;
    }

    @NonNull
    /* renamed from: g */
    public lr mo1225g() {
        return this.f1290g;
    }

    @NonNull
    /* renamed from: h */
    public lr mo1226h() {
        return this.f1291h;
    }
}
