package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0872d;

/* renamed from: com.yandex.metrica.impl.ob.mt */
public class mt implements mq<ty, C0872d> {
    @NonNull
    /* renamed from: a */
    public C0872d b(@NonNull ty tyVar) {
        C0872d dVar = new C0872d();
        dVar.f2172b = tyVar.f2701a;
        return dVar;
    }

    @NonNull
    /* renamed from: a */
    public ty a(@NonNull C0872d dVar) {
        return new ty(dVar.f2172b);
    }
}
