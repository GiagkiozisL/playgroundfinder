package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import java.io.UnsupportedEncodingException;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.og */
public class og {
    @NonNull

    /* renamed from: a */
    private Context ctx;

    public og(@NonNull Context context) {
        this.ctx = context;
    }

    @Nullable
    /* renamed from: a */
    public String mo1544a(@NonNull os osVar) {
        String seed = oy.getLocationObjextStr(osVar);
        Log.d(Constant.RUS_TAG, "og$mo1544a seed is: " + seed);
        if (seed != null) {
            return mo1545a(seed);
        }
        return null;
    }

    @Nullable
    /* renamed from: a */
    public os mo1542a(long j, @NonNull String str) {
        String b = mo1547b(str);
        if (b == null) {
            return null;
        }
        return oy.m2775a(j, b);
    }

    @Nullable
    /* renamed from: a */
    public String mo1543a(@NonNull on onVar) {
        String seed = oy.getWifiCellInfo(onVar);
        Log.d(Constant.RUS_TAG, "og$mo1543a seed is " + seed);
        if (seed == null) {
            return null;
        }
        return mo1545a(seed);
    }

    @Nullable
    /* renamed from: B */
    public on mo1546b(long j, @NonNull String str) {
        String b = mo1547b(str);
        if (b == null) {
            return null;
        }
        return oy.m2778b(j, b);
    }

    @Nullable
    /* renamed from: a */
    public String mo1545a(@NonNull String seed) {
        String var2 = null;

        try {
            var2 = wc.a(this.ctx, seed);
        } catch (UnsupportedEncodingException var4) {
        }

        return var2;
    }

    @Nullable
    /* renamed from: B */
    public String mo1547b(@NonNull String str) {
        String var2 = null;

        try {
            var2 = wc.m4330b(this.ctx, str);
        } catch (UnsupportedEncodingException var4) {
        }

        return var2;
    }
}
