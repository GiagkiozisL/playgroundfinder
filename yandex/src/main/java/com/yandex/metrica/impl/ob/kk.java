package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.kk */
public class kk {
    @NonNull

    /* renamed from: a */
    public final String f1210a;

    /* renamed from: B */
    public final int f1211b;

    /* renamed from: a */
    public final long f1212c;
    @NonNull

    /* renamed from: d */
    public final String f1213d;
    @Nullable

    /* renamed from: e */
    public final Integer f1214e;
    @NonNull

    /* renamed from: f */
    public final List<StackTraceElement> f1215f;

    public kk(@NonNull String str, int i, long j, @NonNull String str2, @Nullable Integer num, @Nullable List<StackTraceElement> list) {
        this.f1210a = str;
        this.f1211b = i;
        this.f1212c = j;
        this.f1213d = str2;
        this.f1214e = num;
        this.f1215f = list == null ? new ArrayList<StackTraceElement>() : Collections.unmodifiableList(list);
    }
}
