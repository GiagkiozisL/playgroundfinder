package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.lv */
public class lv extends lx {

    /* renamed from: a */
    static final qk f1366a = new qk("UUID");

    /* renamed from: B */
    static final qk f1367b = new qk("DEVICE_ID_POSSIBLE");

    /* renamed from: a */
    static final qk f1368c = new qk("DEVICE_ID");

    /* renamed from: d */
    static final qk f1369d = new qk("DEVICE_ID_HASH");

    /* renamed from: e */
    static final qk f1370e = new qk("AD_URL_GET");

    /* renamed from: f */
    static final qk f1371f = new qk("AD_URL_REPORT");

    /* renamed from: g */
    static final qk f1372g = new qk("CUSTOM_HOSTS");

    /* renamed from: h */
    static final qk f1373h = new qk("SERVER_TIME_OFFSET");

    /* renamed from: i */
    static final qk f1374i = new qk("STARTUP_REQUEST_TIME");

    /* renamed from: j */
    static final qk f1375j = new qk("CLIDS");

    /* renamed from: k */
    static final qk f1376k = new qk("CLIENT_CLIDS");

    /* renamed from: l */
    static final qk f1377l = new qk("REFERRER");

    /* renamed from: m */
    static final qk f1378m = new qk("DEFERRED_DEEP_LINK_WAS_CHECKED");

    /* renamed from: n */
    static final qk f1379n = new qk("REFERRER_FROM_PLAY_SERVICES_WAS_CHECKED");

    /* renamed from: o */
    static final qk f1380o = new qk("DEPRECATED_NATIVE_CRASHES_CHECKED");

    /* renamed from: p */
    static final qk f1381p = new qk("API_LEVEL");

    public lv(lf lfVar) {
        super(lfVar);
    }

    /* renamed from: a */
    public String mo1289a(String str) {
        return mo1361c(f1366a.mo1744b(), str);
    }

    /* renamed from: B */
    public String mo1291b(String str) {
        return mo1361c(f1368c.mo1744b(), str);
    }

    /* renamed from: a */
    public String mo1295c(String str) {
        return mo1361c(f1369d.mo1744b(), str);
    }

    /* renamed from: a */
    public String mo1288a() {
        return mo1361c(f1367b.mo1744b(), "");
    }

    /* renamed from: d */
    public String mo1297d(String str) {
        return mo1361c(f1370e.mo1744b(), str);
    }

    /* renamed from: e */
    public String mo1300e(String str) {
        return mo1361c(f1371f.mo1744b(), str);
    }

    /* renamed from: B */
    public List<String> mo1292b() {
        String c = mo1361c(f1372g.mo1744b(), null);
        if (TextUtils.isEmpty(c)) {
            return null;
        }
        return vq.m4228c(c);
    }

    /* renamed from: a */
    public long mo1286a(long j) {
        return mo1356b(f1373h.mo1742a(), j);
    }

    /* renamed from: B */
    public long mo1290b(long j) {
        return mo1356b(f1374i.mo1744b(), j);
    }

    /* renamed from: f */
    public String mo1304f(String str) {
        return mo1361c(f1375j.mo1744b(), str);
    }

    /* renamed from: a */
    public long mo1293c(long j) {
        return mo1356b(f1381p.mo1744b(), j);
    }

    /* renamed from: a */
    public String mo1294c() {
        return mo1361c(f1377l.mo1744b(), null);
    }

    /* renamed from: d */
    public boolean mo1298d() {
        return mo1359b(f1378m.mo1744b(), false);
    }

    /* renamed from: e */
    public boolean mo1301e() {
        return mo1359b(f1379n.mo1744b(), false);
    }

    /* renamed from: g */
    public lv mo1306g(String str) {
        return (lv) mo1357b(f1366a.mo1744b(), str);
    }

    /* renamed from: h */
    public lv mo1307h(String str) {
        return (lv) mo1357b(f1368c.mo1744b(), str);
    }

    /* renamed from: i */
    public lv mo1308i(String str) {
        return (lv) mo1357b(f1369d.mo1744b(), str);
    }

    /* renamed from: j */
    public lv mo1309j(String str) {
        return (lv) mo1357b(f1370e.mo1744b(), str);
    }

    /* renamed from: a */
    public lv mo1287a(List<String> list) {
        return (lv) mo1357b(f1372g.mo1744b(), vq.m4213a(list));
    }

    /* renamed from: k */
    public lv mo1310k(String str) {
        return (lv) mo1357b(f1371f.mo1744b(), str);
    }

    /* renamed from: d */
    public lv mo1296d(long j) {
        return (lv) mo1351a(f1373h.mo1744b(), j);
    }

    /* renamed from: e */
    public lv mo1299e(long j) {
        return (lv) mo1351a(f1374i.mo1744b(), j);
    }

    /* renamed from: l */
    public lv mo1311l(String str) {
        return (lv) mo1357b(f1375j.mo1744b(), str);
    }

    /* renamed from: f */
    public lv mo1303f(long j) {
        return (lv) mo1351a(f1381p.mo1744b(), j);
    }

    /* renamed from: m */
    public lv mo1312m(String str) {
        return (lv) mo1357b(f1367b.mo1744b(), str);
    }

    /* renamed from: n */
    public lv mo1313n(String str) {
        return (lv) mo1357b(f1377l.mo1744b(), str);
    }

    /* renamed from: f */
    public lv mo1302f() {
        return (lv) mo1353a(f1378m.mo1744b(), true);
    }

    /* renamed from: g */
    public lv mo1305g() {
        return (lv) mo1353a(f1379n.mo1744b(), true);
    }

    /* renamed from: o */
    public lv mo1314o(@Nullable String str) {
        return (lv) mo1357b(f1376k.mo1744b(), str);
    }

    @Nullable
    /* renamed from: p */
    public String mo1315p(@Nullable String str) {
        return mo1361c(f1376k.mo1744b(), str);
    }
}
