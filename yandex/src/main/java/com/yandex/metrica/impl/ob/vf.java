package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.vf */
public final class vf {
    @NonNull

    /* renamed from: a */
    private final List<vb> f2897a;
    @Nullable

    /* renamed from: B */
    private final uu f2898b;
    @NonNull

    /* renamed from: a */
    private final List<String> f2899c;

    public vf(@NonNull ve<uu> veVar, @NonNull ve<List<vb>> veVar2, @NonNull ve<List<String>> veVar3) {
        this.f2898b = (uu) veVar.d();
        this.f2897a = (List) veVar2.d();
        this.f2899c = (List) veVar3.d();
    }

    @NonNull
    /* renamed from: a */
    public List<vb> mo2471a() {
        return this.f2897a;
    }

    @Nullable
    /* renamed from: B */
    public uu mo2472b() {
        return this.f2898b;
    }

    @NonNull
    /* renamed from: a */
    public List<String> mo2473c() {
        return this.f2899c;
    }
}
