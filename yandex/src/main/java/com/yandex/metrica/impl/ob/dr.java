package com.yandex.metrica.impl.ob;

import androidx.annotation.VisibleForTesting;

import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.yandex.metrica.impl.ob.dr */
public class dr {

    /* renamed from: a */
    private final xl f734a = xm.m4443a("YMM-BD", new Runnable() {
        public void run() {
            while (dr.this.f735b) {
                try {
                    ((C0284a) dr.this.f736c.take()).mo730a();
                } catch (InterruptedException e) {
                }
            }
        }
    });
    /* access modifiers changed from: private */

    /* renamed from: B */
    public volatile boolean f735b = true;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final BlockingQueue<C0284a> f736c = new LinkedBlockingQueue();

    /* renamed from: d */
    private ConcurrentHashMap<Class, CopyOnWriteArrayList<dv<? extends dt>>> f737d = new ConcurrentHashMap<>();

    /* renamed from: e */
    private WeakHashMap<Object, CopyOnWriteArrayList<C0286c>> f738e = new WeakHashMap<>();

    /* renamed from: f */
    private ConcurrentHashMap<Class, dt> f739f = new ConcurrentHashMap<>();

    /* renamed from: com.yandex.metrica.impl.ob.dr$a */
    private static class C0284a {

        /* renamed from: a */
        private final dt f741a;

        /* renamed from: B */
        private final dv<? extends dt> f742b;

        private C0284a(dt dtVar, dv<? extends dt> dvVar) {
            this.f741a = dtVar;
            this.f742b = dvVar;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo730a() {
            try {
                if (!this.f742b.mo734b(this.f741a)) {
                    this.f742b.mo733a(this.f741a);
                }
            } catch (Throwable th) {
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.dr$B */
    private static final class C0285b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final dr f743a = new dr();
    }

    /* renamed from: com.yandex.metrica.impl.ob.dr$a */
    private static class C0286c {

        /* renamed from: a */
        final CopyOnWriteArrayList<dv<? extends dt>> f744a;

        /* renamed from: B */
        final dv<? extends dt> f745b;

        private C0286c(CopyOnWriteArrayList<dv<? extends dt>> copyOnWriteArrayList, dv<? extends dt> dvVar) {
            this.f744a = copyOnWriteArrayList;
            this.f745b = dvVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo731a() {
            this.f744a.remove(this.f745b);
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            super.finalize();
            mo731a();
        }
    }

    /* renamed from: a */
    public static final dr a() {
        return C0285b.f743a;
    }

    @VisibleForTesting
    dr() {
        this.f734a.start();
    }

    /* renamed from: a */
    public synchronized void mo723a(dt dtVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.f737d.get(dtVar.getClass());
        if (copyOnWriteArrayList != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                mo724a(dtVar, (dv) it.next());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo724a(dt dtVar, dv<? extends dt> dvVar) {
        this.f736c.add(new C0284a(dtVar, dvVar));
    }

    /* renamed from: B */
    public synchronized void b(dt dtVar) {
        mo723a(dtVar);
        this.f739f.put(dtVar.getClass(), dtVar);
    }

    /* renamed from: a */
    public synchronized void mo725a(Class<? extends dt> cls) {
        this.f739f.remove(cls);
    }

    /* renamed from: a */
    public synchronized void a(Object obj, Class cls, dv<? extends dt> dvVar) {
        CopyOnWriteArrayList copyOnWriteArrayList;
        CopyOnWriteArrayList copyOnWriteArrayList2 = (CopyOnWriteArrayList) this.f737d.get(cls);
        if (copyOnWriteArrayList2 == null) {
            CopyOnWriteArrayList copyOnWriteArrayList3 = new CopyOnWriteArrayList();
            this.f737d.put(cls, copyOnWriteArrayList3);
            copyOnWriteArrayList = copyOnWriteArrayList3;
        } else {
            copyOnWriteArrayList = copyOnWriteArrayList2;
        }
        copyOnWriteArrayList.add(dvVar);
        CopyOnWriteArrayList copyOnWriteArrayList4 = (CopyOnWriteArrayList) this.f738e.get(obj);
        if (copyOnWriteArrayList4 == null) {
            copyOnWriteArrayList4 = new CopyOnWriteArrayList();
            this.f738e.put(obj, copyOnWriteArrayList4);
        }
        copyOnWriteArrayList4.add(new C0286c(copyOnWriteArrayList, dvVar));
        dt dtVar = (dt) this.f739f.get(cls);
        if (dtVar != null) {
            mo724a(dtVar, dvVar);
        }
    }

    /* renamed from: a */
    public synchronized void mo726a(Object obj) {
        List<C0286c> list = (List) this.f738e.remove(obj);
        if (list != null) {
            for (C0286c a : list) {
                a.mo731a();
            }
        }
    }
}
