package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.f;

/* renamed from: com.yandex.metrica.impl.ob.ay */
class ay extends n {
    ay(Context context, ee eeVar, @NonNull f fVar, cd cdVar) {
        this(context, cdVar, new bz(eeVar, new CounterConfiguration(fVar)), new ag(context));
    }

    @VisibleForTesting
    ay(Context context, cd cdVar, bz bzVar, @NonNull ag agVar) {
        super(context, cdVar, bzVar, agVar);
    }
}
