package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.po */
public class po implements pp {
    @NonNull

    /* renamed from: a */
    private final pp f1737a;
    @NonNull

    /* renamed from: B */
    private final pp f1738b;

    /* renamed from: com.yandex.metrica.impl.ob.po$a */
    public static class C0761a {
        @NonNull

        /* renamed from: a */
        private pp f1739a;
        @NonNull

        /* renamed from: B */
        private pp f1740b;

        private C0761a() {
        }

        public C0761a(@NonNull pp ppVar, @NonNull pp ppVar2) {
            this.f1739a = ppVar;
            this.f1740b = ppVar2;
        }

        /* renamed from: a */
        public C0761a mo1644a(@NonNull uk ukVar) {
            this.f1740b = new py(ukVar.permissions);
            return this;
        }

        /* renamed from: a */
        public C0761a mo1645a(boolean z) {
            this.f1739a = new pq(z);
            return this;
        }

        /* renamed from: a */
        public po mo1646a() {
            return new po(this.f1739a, this.f1740b);
        }
    }

    @VisibleForTesting
    po(@NonNull pp ppVar, @NonNull pp ppVar2) {
        this.f1737a = ppVar;
        this.f1738b = ppVar2;
    }

    /* renamed from: a */
    public boolean a(@NonNull String str) {
        return this.f1738b.a(str) && this.f1737a.a(str);
    }

    /* renamed from: a */
    public C0761a mo1642a() {
        return new C0761a(this.f1737a, this.f1738b);
    }

    /* renamed from: B */
    public static C0761a m2828b() {
        return new C0761a(new pq(false), new py(null));
    }

    public String toString() {
        return "AskForPermissionsStrategy{mLocationFlagStrategy=" + this.f1737a + ", mStartupStateStrategy=" + this.f1738b + '}';
    }
}
