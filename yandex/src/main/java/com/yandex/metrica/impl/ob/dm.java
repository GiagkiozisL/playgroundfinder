package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.LruCache;

import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.d;

import java.util.List;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.dm */
public class dm {
    @NonNull

    /* renamed from: a */
    private final Context f726a;
    @NonNull

    /* renamed from: B */
    private final dj f727b;
    @NonNull

    /* renamed from: a */
    private final LruCache<String, Long> f728c;
    @NonNull

    /* renamed from: d */
    private final wh f729d;

    /* renamed from: e */
    private final long f730e;

    public dm(@NonNull Context context, long j) {
        this(context, j, new dj(), new wg());
    }

    /* renamed from: a */
    public void mo710a(int i) {
        try {
            tl.a(this.f726a).reportEvent("beacon_scan_error", this.f727b.mo704a(i).toString());
        } catch (Throwable th) {
        }
    }

    /* renamed from: a */
    public void mo712a(@NonNull List<ScanResult> list) {
        boolean z;
        boolean z2 = false;
        for (ScanResult b : list) {
            if (m1356b(b, null) || z2) {
                z = true;
            } else {
                z = false;
            }
            z2 = z;
        }
        if (z2) {
            m1354a().sendEventsBuffer();
        }
    }

    /* renamed from: a */
    public void mo711a(@NonNull ScanResult scanResult, @Nullable Integer num) {
        if (m1356b(scanResult, num)) {
            m1354a().sendEventsBuffer();
        }
    }

    /* renamed from: B */
    private boolean m1356b(@NonNull ScanResult scanResult, @Nullable Integer num) {
        try {
            if (!m1355a(scanResult)) {
                return false;
            }
            m1354a().reportEvent("beacon_scan_result", this.f727b.mo705a(scanResult, num).toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* renamed from: a */
    private d m1354a() {
        return tl.a(this.f726a);
    }

    /* renamed from: a */
    private boolean m1355a(@NonNull ScanResult scanResult) {
        if (scanResult.getDevice() != null) {
            String address = scanResult.getDevice().getAddress();
            long a = this.f729d.a();
            if (!TextUtils.isEmpty(address)) {
                Long l = (Long) this.f728c.get(address);
                boolean z = l == null || a - l.longValue() > this.f730e;
                if (!z) {
                    return z;
                }
                this.f728c.put(address, Long.valueOf(a));
                return z;
            }
        }
        return false;
    }

    @VisibleForTesting
    dm(@NonNull Context context, long j, @NonNull dj djVar, @NonNull wh whVar) {
        this.f726a = context;
        this.f730e = j;
        this.f727b = djVar;
        this.f729d = whVar;
        this.f728c = new LruCache<>(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_IN_DATABASE_COUNT);
    }
}
