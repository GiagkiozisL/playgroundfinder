package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.rp */
public interface rp {

    /* renamed from: com.yandex.metrica.impl.ob.rp$a */
    public static final class a extends e {

        /* renamed from: B */
        public aa[] b;

        /* renamed from: a */
        public String[] c;

        /* renamed from: com.yandex.metrica.impl.ob.rp$a$a */
        public static final class aa extends com.yandex.metrica.impl.ob.e {

            /* renamed from: h */
            private static volatile aa[] h;

            /* renamed from: B */
            public String b;

            /* renamed from: a */
            public String c;

            /* renamed from: d */
            public String d;

            /* renamed from: e */
            public C0857a[] e;

            /* renamed from: f */
            public long f;

            /* renamed from: g */
            public int[] g;

            /* renamed from: com.yandex.metrica.impl.ob.rp$a$a$a */
            public static final class C0857a extends com.yandex.metrica.impl.ob.e {

                /* renamed from: d */
                private static volatile C0857a[] d;

                /* renamed from: B */
                public String b;

                /* renamed from: a */
                public String c;

                /* renamed from: d */
                public static C0857a[] d() {
                    if (d == null) {
//                        synchronized (a.this.a.a) {
                            if (d == null) {
                                d = new C0857a[0];
                            }
//                        }
                    }
                    return d;
                }

                public C0857a() {
                    mo1857e();
                }

                /* renamed from: e */
                public C0857a mo1857e() {
                    this.b = "";
                    this.c = "";
                    this.f754a = -1;
                    return this;
                }

                /* renamed from: a */
                public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                    bVar.mo351a(1, this.b);
                    bVar.mo351a(2, this.c);
                    super.mo739a(bVar);
                }

                /* access modifiers changed from: protected */
                /* renamed from: a */
                public int mo741c() {
//                    return super.mo741c() + C0089b.m437b(1, this.f2082b) + C0089b.m437b(2, this.f2083c);
                    return super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.b) + com.yandex.metrica.impl.ob.b.m437b(2, this.c);
                }

                /* renamed from: B */
                public C0857a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                    while (true) {
                        int a = aVar.mo213a();
                        switch (a) {
                            case 0:
                                break;
                            case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                                this.b = aVar.mo229i();
                                continue;
                            case 18:
                                this.c = aVar.mo229i();
                                continue;
                            default:
                                if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
//                    return this;
                }
            }

            /* renamed from: d */
            public static aa[] m3250d() {
                if (h == null) {
//                    synchronized (a.a) {
                        if (h == null) {
                            h = new aa[0];
                        }
//                    }
                }
                return h;
            }

            public aa() {
                e();
            }

            /* renamed from: e */
            public aa e() {
                this.b = "";
                this.c = "";
                this.d = "";
                this.e = C0857a.d();
                this.f = 0;
                this.g = com.yandex.metrica.impl.ob.g.a;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo351a(1, this.b);
                bVar.mo351a(2, this.c);
                bVar.mo351a(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    for (C0857a aVar : this.e) {
                        if (aVar != null) {
                            bVar.mo350a(4, (com.yandex.metrica.impl.ob.e) aVar);
                        }
                    }
                }
                bVar.mo349a(5, this.f);
                if (this.g != null && this.g.length > 0) {
                    for (int a : this.g) {
                        bVar.mo348a(6, a);
                    }
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int b = com.yandex.metrica.impl.ob.b.m437b(3, this.d) + super.mo741c() + com.yandex.metrica.impl.ob.b.m437b(1, this.b) + com.yandex.metrica.impl.ob.b.m437b(2, this.c);
                if (this.e != null && this.e.length > 0) {
                    for (C0857a aVar : this.e) {
                        if (aVar != null) {
                            b += com.yandex.metrica.impl.ob.b.b(4, (com.yandex.metrica.impl.ob.e) aVar);
                        }
                    }
                }
                int d = b + com.yandex.metrica.impl.ob.b.d(5, this.f);
                if (this.g == null || this.g.length <= 0) {
                    return d;
                }
                int i = 0;
                for (int d2 : this.g) {
                    i += com.yandex.metrica.impl.ob.b.m444d(d2);
                }
                return d + i + (this.g.length * 1);
            }

            /* renamed from: B */
            public aa mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                int length;
                int i;
                int length2;
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.b = aVar.mo229i();
                            continue;
                        case 18:
                            this.c = aVar.mo229i();
                            continue;
                        case 26:
                            this.d = aVar.mo229i();
                            continue;
                        case 34:
                            int b = com.yandex.metrica.impl.ob.g.b(aVar, 34);
                            if (this.e == null) {
                                length2 = 0;
                            } else {
                                length2 = this.e.length;
                            }
                            C0857a[] aVarArr = new C0857a[(b + length2)];
                            if (length2 != 0) {
                                System.arraycopy(this.e, 0, aVarArr, 0, length2);
                            }
                            while (length2 < aVarArr.length - 1) {
                                aVarArr[length2] = new C0857a();
                                aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length2]);
                                aVar.mo213a();
                                length2++;
                            }
                            aVarArr[length2] = new C0857a();
                            aVar.mo215a((com.yandex.metrica.impl.ob.e) aVarArr[length2]);
                            this.e = aVarArr;
                            continue;
                        case 40:
                            this.f = aVar.mo221e();
                            continue;
                        case 48:
                            int b2 = com.yandex.metrica.impl.ob.g.b(aVar, 48);
                            int[] iArr = new int[b2];
                            int i2 = 0;
                            int i3 = 0;
                            while (i2 < b2) {
                                if (i2 != 0) {
                                    aVar.mo213a();
                                }
                                int g = aVar.mo225g();
                                switch (g) {
                                    case 1:
                                    case 2:
                                        i = i3 + 1;
                                        iArr[i3] = g;
                                        break;
                                    default:
                                        i = i3;
                                        break;
                                }
                                i2++;
                                i3 = i;
                            }
                            if (i3 != 0) {
                                int length3 = this.g == null ? 0 : this.g.length;
                                if (length3 != 0 || i3 != iArr.length) {
                                    int[] iArr2 = new int[(length3 + i3)];
                                    if (length3 != 0) {
                                        System.arraycopy(this.g, 0, iArr2, 0, length3);
                                    }
                                    System.arraycopy(iArr, 0, iArr2, length3, i3);
                                    this.g = iArr2;
                                    break;
                                } else {
                                    this.g = iArr;
                                    break;
                                }
                            } else {
                                continue;
                            }
                        case 50:
                            int d = aVar.d(aVar.mo234n());
                            int t = aVar.mo240t();
                            int i4 = 0;
                            while (aVar.mo238r() > 0) {
                                switch (aVar.mo225g()) {
                                    case 1:
                                    case 2:
                                        i4++;
                                        break;
                                }
                            }
                            if (i4 != 0) {
                                aVar.mo224f(t);
                                if (this.g == null) {
                                    length = 0;
                                } else {
                                    length = this.g.length;
                                }
                                int[] iArr3 = new int[(i4 + length)];
                                if (length != 0) {
                                    System.arraycopy(this.g, 0, iArr3, 0, length);
                                }
                                while (aVar.mo238r() > 0) {
                                    int g2 = aVar.mo225g();
                                    switch (g2) {
                                        case 1:
                                        case 2:
                                            int i5 = length + 1;
                                            iArr3[length] = g2;
                                            length = i5;
                                            break;
                                    }
                                }
                                this.g = iArr3;
                            }
                            aVar.mo222e(d);
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public a() {
            mo1853d();
        }

        /* renamed from: d */
        public a mo1853d() {
            this.b = aa.m3250d();
            this.c = g.f;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (aa aVar : this.b) {
                    if (aVar != null) {
                        bVar.mo350a(1, (e) aVar);
                    }
                }
            }
            if (this.c != null && this.c.length > 0) {
                for (String str : this.c) {
                    if (str != null) {
                        bVar.mo351a(2, str);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.b != null && this.b.length > 0) {
                for (aa aVar : this.b) {
                    if (aVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(1, (e) aVar);
                    }
                }
            }
            if (this.c == null || this.c.length <= 0) {
                return c;
            }
            int i = 0;
            int i2 = 0;
            for (String str : this.c) {
                if (str != null) {
                    i2++;
                    i += com.yandex.metrica.impl.ob.b.m441b(str);
                }
            }
            return c + i + (i2 * 1);
        }

        /* renamed from: B */
        public a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        int b = g.b(aVar, 10);
                        if (this.b == null) {
                            length = 0;
                        } else {
                            length = this.b.length;
                        }
                        aa[] aVarArr = new aa[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.b, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new aa();
                            aVar.mo215a((e) aVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        aVarArr[length] = new aa();
                        aVar.mo215a((e) aVarArr[length]);
                        this.b = aVarArr;
                        continue;
                    case 18:
                        int b2 = g.b(aVar, 18);
                        int length2 = this.c == null ? 0 : this.c.length;
                        String[] strArr = new String[(b2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.c, 0, strArr, 0, length2);
                        }
                        while (length2 < strArr.length - 1) {
                            strArr[length2] = aVar.mo229i();
                            aVar.mo213a();
                            length2++;
                        }
                        strArr[length2] = aVar.mo229i();
                        this.c = strArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }

        /* renamed from: a */
        public static a m3244a(byte[] bArr) throws d {
            return (a) e.m1393a(new a(), bArr);
        }
    }
}
