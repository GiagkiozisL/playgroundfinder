package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.CounterConfiguration.C0008a;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ck */
public class ck {
    @NonNull

    /* renamed from: a */
    private final cl f552a;
    @NonNull

    /* renamed from: B */
    private final Context f553b;
    @NonNull

    /* renamed from: a */
    private final Map<String, cj> f554c = new HashMap();

    public ck(@NonNull Context context, @NonNull cl clVar) {
        this.f553b = context;
        this.f552a = clVar;
    }

    @Nullable
    /* renamed from: a */
    public synchronized cj mo609a(@NonNull String str, @NonNull C0008a aVar) {
        cj cjVar;
        cjVar = (cj) this.f554c.get(str);
        if (cjVar == null) {
            cjVar = new cj(str, this.f553b, aVar, this.f552a);
            this.f554c.put(str, cjVar);
        }
        return cjVar;
    }
}
