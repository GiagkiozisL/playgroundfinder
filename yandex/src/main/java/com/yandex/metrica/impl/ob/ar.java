package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.util.Base64;

import com.yandex.metrica.impl.ob.ad.C0054a;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;

/* renamed from: com.yandex.metrica.impl.ob.ar */
public final class ar {

    /* renamed from: a */
    private final JSONObject f251a;
    @NonNull

    /* renamed from: B */
    private final MyPackageManager f252b;
    @NonNull

    /* renamed from: a */
    private final Context f253c;
    @NonNull

    /* renamed from: d */
    private final ad f254d;

    public ar(@NonNull Context context) {
        this(context, new ad(context), new MyPackageManager());
    }

    @VisibleForTesting
    public ar(@NonNull Context context, @NonNull ad adVar, @NonNull MyPackageManager myPackageManagerVar) {
        this.f251a = new JSONObject();
        this.f253c = context;
        this.f254d = adVar;
        this.f252b = myPackageManagerVar;
    }

    /* renamed from: a */
    public ar mo313a() {
        try {
            mo319f();
            mo317d();
        } catch (Throwable th) {
        }
        mo315b();
        return this;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public ar mo315b() {
        try {
            Object obj = Class.forName("kotlin.KotlinVersion").getDeclaredField("CURRENT").get(null);
            int intValue = ((Integer) obj.getClass().getDeclaredMethod("getMajor", new Class[0]).invoke(obj, new Object[0])).intValue();
            int intValue2 = ((Integer) obj.getClass().getDeclaredMethod("getMinor", new Class[0]).invoke(obj, new Object[0])).intValue();
            ((JSONObject) m377a(this.f251a, "dfid", new JSONObject())).put("kotlin_runtime", new JSONObject().put("major", intValue).put("minor", intValue2).put("patch", ((Integer) obj.getClass().getDeclaredMethod("getPatch", new Class[0]).invoke(obj, new Object[0])).intValue()));
        } catch (Throwable th) {
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public ar mo316c() throws JSONException {
        JSONArray i;
        JSONObject jSONObject = (JSONObject) m377a(this.f251a, "dfid", new JSONObject());
        if (cx.a(21)) {
            i = m380h();
        } else {
            i = m381i();
        }
        jSONObject.put("cpu_abis", i);
        return this;
    }

    @TargetApi(21)
    @NonNull
    /* renamed from: h */
    private JSONArray m380h() throws JSONException {
        return new JSONArray(Build.SUPPORTED_ABIS);
    }

    @NonNull
    /* renamed from: i */
    private JSONArray m381i() {
        ArrayList arrayList = new ArrayList();
        if (!"unknown".equals(Build.CPU_ABI)) {
            arrayList.add(Build.CPU_ABI);
        }
        if (!"unknown".equals(Build.CPU_ABI2)) {
            arrayList.add(Build.CPU_ABI2);
        }
        return new JSONArray(arrayList);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public ar mo317d() throws JSONException {
        ((JSONObject) m377a(this.f251a, "dfid", new JSONObject())).put("boot_time", wi.m4359a() / 1000);
        return this;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public ar mo314a(boolean z) throws JSONException, UnsupportedEncodingException {
        JSONObject jSONObject = (JSONObject) m377a((JSONObject) m377a(this.f251a, "dfid", new JSONObject()), "au", new JSONObject());
        JSONArray jSONArray = (JSONArray) m377a(jSONObject, "aun", new JSONArray());
        JSONArray jSONArray2 = (JSONArray) m377a(jSONObject, "ausf", new JSONArray());
        JSONArray jSONArray3 = (JSONArray) m377a(jSONObject, "audf", new JSONArray());
        JSONArray jSONArray4 = (JSONArray) m377a(jSONObject, "aulu", new JSONArray());
        JSONArray jSONArray5 = new JSONArray();
        if (z) {
            m377a(jSONObject, "aufi", jSONArray5);
        }
        HashSet hashSet = new HashSet();
        for (ResolveInfo resolveInfo : cx.a(this.f253c, new String(Base64.decode("YW5kcm9pZC5pbnRlbnQuYWN0aW9uLk1BSU4=", 0), "UTF-8"), new String(Base64.decode("YW5kcm9pZC5pbnRlbnQuY2F0ZWdvcnkuTEFVTkNIRVI=", 0), "UTF-8"))) {
            ApplicationInfo applicationInfo = resolveInfo.activityInfo.applicationInfo;
            if (hashSet.add(applicationInfo.packageName)) {
                jSONArray.put(applicationInfo.packageName);
                boolean z2 = (applicationInfo.flags & 1) == 1;
                jSONArray2.put(z2);
                jSONArray4.put(new File(applicationInfo.sourceDir).lastModified());
                jSONArray3.put(!applicationInfo.enabled);
                if (z) {
                    if (z2) {
                        jSONArray5.put(0);
                    } else {
                        PackageInfo a = this.f252b.getPackageInfo(this.f253c, applicationInfo.packageName);
                        if (a == null) {
                            jSONArray5.put(0);
                        } else {
                            jSONArray5.put(a.firstInstallTime / 1000);
                        }
                    }
                }
            }
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public ar mo318e() throws JSONException {
        boolean z;
        boolean z2;
        JSONObject jSONObject = (JSONObject) m377a((JSONObject) m377a(this.f251a, "dfid", new JSONObject()), "apps", new JSONObject());
        JSONArray jSONArray = (JSONArray) m377a(jSONObject, "names", new JSONArray());
        JSONArray jSONArray2 = (JSONArray) m377a(jSONObject, "system_flags", new JSONArray());
        JSONArray jSONArray3 = (JSONArray) m377a(jSONObject, "disabled_flags", new JSONArray());
        JSONArray jSONArray4 = (JSONArray) m377a(jSONObject, "first_install_time", new JSONArray());
        JSONArray jSONArray5 = (JSONArray) m377a(jSONObject, "last_update_time", new JSONArray());
        jSONObject.put("version", 0);
        for (PackageInfo packageInfo : cx.a(this.f253c)) {
            jSONArray.put(packageInfo.packageName);
            if ((packageInfo.applicationInfo.flags & 1) == 1) {
                z = true;
            } else {
                z = false;
            }
            jSONArray2.put(z);
            if (!packageInfo.applicationInfo.enabled) {
                z2 = true;
            } else {
                z2 = false;
            }
            jSONArray3.put(z2);
            m378a(jSONArray4, packageInfo);
            m379b(jSONArray5, packageInfo);
        }
        return this;
    }

    /* renamed from: a */
    private void m378a(JSONArray jSONArray, PackageInfo packageInfo) {
        jSONArray.put(packageInfo.firstInstallTime / 1000);
    }

    /* renamed from: B */
    private void m379b(JSONArray jSONArray, PackageInfo packageInfo) {
        jSONArray.put(packageInfo.lastUpdateTime / 1000);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public ar mo319f() throws JSONException {
        JSONObject jSONObject = (JSONObject) m377a(this.f251a, "dfid", new JSONObject());
        C0054a a = this.f254d.mo255a();
        jSONObject.put("tds", a.f152a);
        jSONObject.put("fds", a.f153b);
        return this;
    }

    /* renamed from: a */
    static <T> T m377a(JSONObject var0, String var1, T var2) throws JSONException {
        if (!var0.has(var1)) {
            var0.put(var1, var2);
        }

        return (T)var0.get(var1);
//        if (!jSONObject.has(str)) {
//            jSONObject.put(str, t);
//        }
//        return jSONObject.get(str);
    }

    public String toString() {
        return this.f251a.toString();
    }

    /* renamed from: g */
    public String mo320g() {
        return this.f251a.toString();
    }
}
