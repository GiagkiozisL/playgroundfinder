package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.SparseArray;

import com.yandex.metrica.impl.ob.af.C0058a;

/* renamed from: com.yandex.metrica.impl.ob.kd */
public class kd {

    /* renamed from: a */
    private static SparseArray<kd> f1194c = new SparseArray<>();

    /* renamed from: a */
    public final String f1195a;

    /* renamed from: B */
    public final String f1196b;

    static {
        f1194c.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED.mo259a(), new kd("jvm", "binder"));
        f1194c.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF.mo259a(), new kd("jvm", "binder"));
        f1194c.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.mo259a(), new kd("jvm", "intent"));
        f1194c.put(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE.mo259a(), new kd("jvm", "file"));
        f1194c.put(C0058a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH.mo259a(), new kd("jni_native", "file"));
        f1194c.put(C0058a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH.mo259a(), new kd("jni_native", "file"));
    }

    private kd(@NonNull String str, @NonNull String str2) {
        this.f1195a = str;
        this.f1196b = str2;
    }

    /* renamed from: a */
    public static kd m2021a(int i) {
        return (kd) f1194c.get(i);
    }
}
