package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanSettings;
import android.bluetooth.le.ScanSettings.Builder;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.tt.b;
import com.yandex.metrica.impl.ob.tt.b.a_enum;
import com.yandex.metrica.impl.ob.tt.b.b_enum;
import com.yandex.metrica.impl.ob.tt.b.c_enum;
import com.yandex.metrica.impl.ob.tt.b.d_enum;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.dk */
public class dk {

    /* renamed from: a */
    private static final Map<d_enum, Integer> f722a;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(d_enum.LOW_POWER, Integer.valueOf(0));
        hashMap.put(d_enum.BALANCED, Integer.valueOf(1));
        hashMap.put(d_enum.LOW_LATENCY, Integer.valueOf(2));
        f722a = Collections.unmodifiableMap(hashMap);
    }

    @NonNull
    /* renamed from: a */
    public ScanSettings mo706a(@NonNull b bVar) {
        Builder builder = new Builder();
        builder.setScanMode(m1352a(bVar.f2635d));
        builder.setReportDelay(bVar.f2636e);
        if (cx.a(23)) {
            builder.setCallbackType(m1349a(bVar.f2632a));
            builder.setMatchMode(m1350a(bVar.f2633b));
            builder.setNumOfMatches(m1351a(bVar.f2634c));
        }
        return builder.build();
    }

    /* renamed from: a */
    private int m1352a(@NonNull d_enum dVar) {
        Integer num = (Integer) f722a.get(dVar);
        if (num == null) {
            return 2;
        }
        return num.intValue();
    }

    @TargetApi(23)
    /* renamed from: a */
    private int m1349a(@NonNull a_enum aVar) {
        switch (aVar) {
            case MATCH_LOST:
                return 4;
            case FIRST_MATCH:
                return 2;
            default:
                return 1;
        }
    }

    @TargetApi(23)
    /* renamed from: a */
    private int m1350a(@NonNull b_enum bVar) {
        if (b_enum.AGGRESSIVE.equals(bVar)) {
            return 1;
        }
        return 2;
    }

    @TargetApi(23)
    /* renamed from: a */
    private int m1351a(@NonNull c_enum cVar) {
        switch (cVar) {
            case ONE_AD:
                return 1;
            case FEW_AD:
                return 2;
            default:
                return 3;
        }
    }
}
