package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.vc */
public class vc implements ve<List<vb>> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public final ux f2889a;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public pr f2890b;

    vc(@NonNull ux uxVar, @NonNull pr prVar) {
        this.f2889a = uxVar;
        this.f2890b = prVar;
    }

    @Nullable
    /* renamed from: a */
    public List<vb> d() {
        ArrayList arrayList = new ArrayList();
        if (this.f2889a.mo2448h()) {
            if (cx.a(23)) {
                arrayList.addAll(m4149g());
                if (arrayList.size() == 0) {
                    arrayList.add(m4145b());
                }
            } else {
                arrayList.add(m4145b());
            }
        }
        return arrayList;
    }

    /* renamed from: B */
    private vb m4145b() {
        return new vb(m4146c(), m4147e(), m4150h(), m4148f(), null);
    }

    @Nullable
    /* renamed from: a */
    private Integer m4146c() {
        return (Integer) cx.a((wo<TelephonyManager, Integer>) new wo<TelephonyManager, Integer>() {
            /* renamed from: a */
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String str;
                String simOperator = telephonyManager.getSimOperator();
                if (!TextUtils.isEmpty(simOperator)) {
                    str = simOperator.substring(0, 3);
                } else {
                    str = null;
                }
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(str));
            }
        }, this.f2889a.mo2443c(), "getting SimMcc", "TelephonyManager");
    }

    @Nullable
    /* renamed from: e */
    private Integer m4147e() {
        return (Integer) cx.a((wo<TelephonyManager, Integer>) new wo<TelephonyManager, Integer>() {
            /* renamed from: a */
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String str;
                String simOperator = telephonyManager.getSimOperator();
                if (!TextUtils.isEmpty(simOperator)) {
                    str = simOperator.substring(3);
                } else {
                    str = null;
                }
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(str));
            }
        }, this.f2889a.mo2443c(), "getting SimMnc", "TelephonyManager");
    }

    @Nullable
    /* renamed from: f */
    private String m4148f() {
        return (String) cx.a((wo<TelephonyManager, String>) new wo<TelephonyManager, String>() {
            /* renamed from: a */
            public String a(TelephonyManager telephonyManager) throws Throwable {
                return telephonyManager.getSimOperatorName();
            }
        }, this.f2889a.mo2443c(), "getting SimOperatorName", "TelephonyManager");
    }

    @TargetApi(23)
    @NonNull
    @SuppressLint("MissingPermission")
    /* renamed from: g */
    private List<vb> m4149g() {
        ArrayList arrayList = new ArrayList();
        if (this.f2890b.mo1654d(this.f2889a.mo2444d())) {
            try {
                List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(this.f2889a.mo2444d()).getActiveSubscriptionInfoList();
                if (activeSubscriptionInfoList != null) {
                    for (SubscriptionInfo vbVar : activeSubscriptionInfoList) {
                        arrayList.add(new vb(vbVar));
                    }
                }
            } catch (Throwable th) {
            }
        }
        return arrayList;
    }

    /* renamed from: h */
    private boolean m4150h() {
        return ((Boolean) cx.a(new wo<TelephonyManager, Boolean>() {
            /* renamed from: a */
            public Boolean a(TelephonyManager telephonyManager) throws Throwable {
                if (vc.this.f2890b.mo1654d(vc.this.f2889a.mo2444d())) {
                    return Boolean.valueOf(telephonyManager.isNetworkRoaming());
                }
                return null;
            }
        }, this.f2889a.mo2443c(), "getting NetworkRoaming", "TelephonyManager", Boolean.valueOf(false))).booleanValue();
    }
}
