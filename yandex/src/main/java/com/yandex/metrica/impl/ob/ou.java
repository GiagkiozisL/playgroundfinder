package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ou */
class ou {
    @NonNull

    /* renamed from: a */
    private C0732c f1669a;
    @NonNull

    /* renamed from: B */
    private C0730a f1670b;
    @NonNull

    /* renamed from: a */
    private C0731b f1671c;
    @NonNull

    /* renamed from: d */
    private Context f1672d;
    @Nullable

    /* renamed from: e */
    private oh f1673e;
    @Nullable

    /* renamed from: f */
    private ov f1674f;
    @NonNull

    /* renamed from: g */
    private ow f1675g;
    @NonNull

    /* renamed from: h */
    private od f1676h;
    @Nullable

    /* renamed from: i */
    private oi f1677i;
    @NonNull

    /* renamed from: j */
    private Map<String, op> f1678j;

    /* renamed from: com.yandex.metrica.impl.ob.ou$a */
    public static class C0730a {
        @NonNull
        /* renamed from: a */
        public oi mo1610a(oe oeVar) {
            return new oi(oeVar);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ou$B */
    public static class C0731b {
        @NonNull
        /* renamed from: a */
        public op mo1611a(@Nullable String str, @Nullable oh ohVar, @NonNull ov ovVar, @NonNull ow owVar, @NonNull od odVar) {
            return new op(str, ohVar, ovVar, owVar, odVar);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ou$a */
    public static class C0732c {
        @NonNull
        /* renamed from: a */
        public ov mo1612a(@NonNull Context context, @Nullable oe oeVar) {
            return new ov(context, oeVar);
        }
    }

    public ou(@NonNull Context context, @Nullable oh ohVar, @NonNull ow owVar, @NonNull od odVar) {
        this(context, ohVar, new C0732c(), new C0730a(), new C0731b(), owVar, odVar);
    }

    /* renamed from: a */
    public void mo1608a(@NonNull Location location) {
        String provider = location.getProvider();
        op opVar = (op) this.f1678j.get(provider);
        if (opVar == null) {
            opVar = m2749a(provider);
            this.f1678j.put(provider, opVar);
        } else {
            opVar.mo1581a(this.f1673e);
        }
        opVar.mo1580a(location);
    }

    @Nullable
    /* renamed from: a */
    public Location mo1607a() {
        if (this.f1677i == null) {
            return null;
        }
        return this.f1677i.mo1549a();
    }

    @NonNull
    /* renamed from: a */
    private op m2749a(String str) {
        if (this.f1674f == null) {
            this.f1674f = this.f1669a.mo1612a(this.f1672d, null);
        }
        if (this.f1677i == null) {
            this.f1677i = this.f1670b.mo1610a(this.f1674f);
        }
        return this.f1671c.mo1611a(str, this.f1673e, this.f1674f, this.f1675g, this.f1676h);
    }

    /* renamed from: a */
    public void mo1609a(@Nullable oh ohVar) {
        this.f1673e = ohVar;
    }

    @VisibleForTesting
    ou(@NonNull Context context, @Nullable oh ohVar, @NonNull C0732c cVar, @NonNull C0730a aVar, @NonNull C0731b bVar, @NonNull ow owVar, @NonNull od odVar) {
        this.f1678j = new HashMap();
        this.f1672d = context;
        this.f1673e = ohVar;
        this.f1669a = cVar;
        this.f1670b = aVar;
        this.f1671c = bVar;
        this.f1675g = owVar;
        this.f1676h = odVar;
    }
}
