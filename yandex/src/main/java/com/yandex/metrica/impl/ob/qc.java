package com.yandex.metrica.impl.ob;

import android.content.Context;

import java.util.Map;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qc */
public class qc extends qd {

    /* renamed from: d */
    private final qk f1759d = new qk("init_event_pref_key", mo1698i());

    /* renamed from: e */
    private final qk f1760e = new qk("init_event_pref_key");

    /* renamed from: f */
    private final qk f1761f = new qk("first_event_pref_key", mo1698i());

    /* renamed from: g */
    private final qk f1762g = new qk("fitst_event_description_key", mo1698i());

    public qc(Context context, String str) {
        super(context, str);
    }

    /* renamed from: a */
    public void mo1684a() {
        mo1695a(this.f1759d.mo1744b(), "DONE").mo1699j();
    }

    @Deprecated
    /* renamed from: a */
    public String mo1683a(String str) {
        return this.f1766c.getString(this.f1760e.mo1744b(), str);
    }

    /* renamed from: B */
    public String mo1685b(String str) {
        return this.f1766c.getString(this.f1759d.mo1744b(), str);
    }

    /* renamed from: a */
    public String mo1687c(String str) {
        return this.f1766c.getString(this.f1761f.mo1744b(), str);
    }

    @Deprecated
    /* renamed from: B */
    public void mo1686b() {
        m2884a(this.f1760e);
    }

    @Deprecated
    /* renamed from: d */
    public void mo1690d(String str) {
        m2884a(new qk("init_event_pref_key", str));
    }

    /* renamed from: a */
    public void mo1688c() {
        m2884a(this.f1759d);
    }

    /* renamed from: d */
    public void mo1689d() {
        m2884a(this.f1761f);
    }

    /* renamed from: e */
    public String mo1691e(String str) {
        return this.f1766c.getString(this.f1762g.mo1744b(), str);
    }

    /* renamed from: e */
    public void mo1692e() {
        m2884a(this.f1762g);
    }

    /* renamed from: a */
    private void m2884a(qk qkVar) {
        this.f1766c.edit().remove(qkVar.mo1744b()).apply();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_initpreferences";
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: g */
    public Map<String, ?> g() {
        return this.f1766c.getAll();
    }

    /* renamed from: f */
    static String f(String str) {
        return new qk("init_event_pref_key", str).mo1744b();
    }

    /* renamed from: g */
    static String g(String str) {
        return str.replace("init_event_pref_key", "");
    }
}
