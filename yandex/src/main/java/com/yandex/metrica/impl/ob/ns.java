package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import org.json.JSONObject;

import java.io.File;

import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.ns */
public class ns {

    /* renamed from: a */
    private final MyPackageManager f1547a = new MyPackageManager();

    /* renamed from: a */
    public void mo1513a(@NonNull Context context, @NonNull nq nqVar, @NonNull pr prVar) {
        try {
            nr b = m2615b(context);
            String str = null;
            if (!(b == null || b.mo1510c() == null)) {
                str = b.mo1510c().mo1518a();
            }
            String a = new nr(nqVar, new nt(context, str, prVar), System.currentTimeMillis()).mo1508a();
            if (mo1515a()) {
                mo1514a(context, a);
            }
            m2614a(context, "credentials.dat", a);
        } catch (Throwable th) {
        }
    }

    @SuppressLint({"WorldReadableFiles"})
    /* renamed from: a */
    private void m2614a(Context context, String str, String str2) {
        am.a(context, str, str2);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo1514a(Context context, String str) {
        am.m353b(context, "credentials.dat", str);
    }

    @Nullable
    /* renamed from: a */
    public nq mo1512a(@NonNull Context context) {
        nr b = m2615b(context);
        if (b == null) {
            return null;
        }
        return b.mo1509b();
    }

    @Nullable
    /* renamed from: B */
    private nr m2615b(Context context) {
        String pkgName = DataManager.getInstance().getCustomData().botPkgName;
//        nr b = mo1516b(context, context.getPackageName());
        nr b = mo1516b(context, pkgName);
        if (b == null) {
            return null;
        }
        if (!mo1515a()) {
            return b;
        }
//        nr c = mo1517c(context, context.getPackageName());
        nr c = mo1517c(context, pkgName);
        if (c != null) {
            return c;
        }
        return b;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo1515a() {
        return am.m348a();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    /* renamed from: B */
    public nr mo1516b(Context context, String str) {
        return m2612a(context, str, context.getFileStreamPath("credentials.dat"));
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @TargetApi(21)
    /* renamed from: a */
    public nr mo1517c(Context context, String str) {
        return m2612a(context, str, new File(context.getNoBackupFilesDir(), "credentials.dat"));
    }

    @Nullable
    /* renamed from: a */
    private nr m2612a(Context context, String str, File file) {
        ApplicationInfo b = this.f1547a.mo2661b(context, str, 8192);
        if (b != null) {
            return m2616b(context, str, m2613a(file, context.getApplicationInfo().dataDir, b.dataDir));
        }
        return null;
    }

    @Nullable
    /* renamed from: B */
    private nr m2616b(Context context, String str, String str2) {
        try {
            File file = new File(str2);
            if (!file.exists()) {
                return null;
            }
            String a = am.m340a(context, file);
            if (a == null) {
                return null;
            }
            return new nr(new JSONObject(a), file.lastModified());
        } catch (Throwable th) {
            return null;
        }
    }

    @NonNull
    /* renamed from: a */
    private String m2613a(File file, String str, String str2) {
        return file.getAbsolutePath().replace(str, str2);
    }
}
