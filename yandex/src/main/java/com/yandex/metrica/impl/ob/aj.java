package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.aj */
public enum aj {
    UNKNOWN(0),
    FIRST_OCCURRENCE(1),
    NON_FIRST_OCCURENCE(2);
    

    /* renamed from: d */
    public final int f225d;

    private aj(int i) {
        this.f225d = i;
    }

    @NonNull
    /* renamed from: a */
    public static aj m319a(@Nullable Integer num) {
        aj[] values;
        if (num != null) {
            for (aj ajVar : values()) {
                if (ajVar.f225d == num.intValue()) {
                    return ajVar;
                }
            }
        }
        return UNKNOWN;
    }
}
