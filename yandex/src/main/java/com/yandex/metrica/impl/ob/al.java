package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.cs.C0233b;
import com.yandex.metrica.impl.ob.np.C0678a;
import com.yandex.metrica.impl.ob.rs.a;

/* renamed from: com.yandex.metrica.impl.ob.al */
public final class al {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a */
    private static volatile al f228a;
    @NonNull

    /* renamed from: B */
    private final Context f229b;

    /* renamed from: a */
    private volatile si f230c;

    /* renamed from: d */
    private volatile tj f231d;

    /* renamed from: e */
    private volatile rs f232e;

    /* renamed from: f */
    private volatile cs f233f;

    /* renamed from: g */
    private volatile h f234g;
    @Nullable

    /* renamed from: h */
    private volatile td f235h;
    @Nullable

    /* renamed from: i */
    private volatile ai f236i;
    @NonNull

    /* renamed from: j */
    private volatile xo f237j = new xo();
    @Nullable

    /* renamed from: k */
    private volatile vd f238k;
    @Nullable

    /* renamed from: l */
    private volatile ck f239l;

    /* renamed from: a */
    public static void m325a(@NonNull Context context) {
        if (f228a == null) {
            synchronized (al.class) {
                if (f228a == null) {
                    f228a = new al(context.getApplicationContext());
                }
            }
        }
    }

    /* renamed from: a */
    public static al m324a() {
        return f228a;
    }

    private al(@NonNull Context context) {
        this.f229b = context;
    }

    @NonNull
    /* renamed from: B */
    public Context mo284b() {
        return this.f229b;
    }

    @NonNull
    /* renamed from: a */
    public si mo285c() {
        if (this.f230c == null) {
            synchronized (this) {
                if (this.f230c == null) {
                    this.f230c = new si(this.f229b);
                }
            }
        }
        return this.f230c;
    }

    @NonNull
    /* renamed from: d */
    public tj mo286d() {
        if (this.f231d == null) {
            synchronized (this) {
                if (this.f231d == null) {
                    this.f231d = new tj(this.f229b);
                }
            }
        }
        return this.f231d;
    }

    @NonNull
    /* renamed from: e */
    public rs mo287e() {
        if (this.f232e == null) {
            synchronized (this) {
                if (this.f232e == null) {
                    this.f232e = new rs(this.f229b, C0678a.m2599a(a.class).a(this.f229b), m324a().mo290h(), mo286d(), this.f237j.mo2633h());
                }
            }
        }
        return this.f232e;
    }

    @NonNull
    /* renamed from: f */
    public td mo288f() {
        if (this.f235h == null) {
            synchronized (this) {
                if (this.f235h == null) {
                    this.f235h = new td(this.f229b, this.f237j.mo2633h());
                }
            }
        }
        return this.f235h;
    }

    @NonNull
    /* renamed from: g */
    public ai mo289g() {
        if (this.f236i == null) {
            synchronized (this) {
                if (this.f236i == null) {
                    this.f236i = new ai();
                }
            }
        }
        return this.f236i;
    }

    @NonNull
    /* renamed from: h */
    public cs mo290h() {
        if (this.f233f == null) {
            synchronized (this) {
                if (this.f233f == null) {
                    this.f233f = new cs(new C0233b(new ly(ld.m2146a(this.f229b).mo1238c())));
                }
            }
        }
        return this.f233f;
    }

    @NonNull
    /* renamed from: i */
    public h mo291i() {
        if (this.f234g == null) {
            synchronized (this) {
                if (this.f234g == null) {
                    this.f234g = new h();
                }
            }
        }
        return this.f234g;
    }

    /* renamed from: a */
    public void mo283a(@Nullable uk ukVar) {
        if (this.f235h != null) {
            this.f235h.mo2232b(ukVar);
        }
        if (this.f236i != null) {
            this.f236i.mo270a(ukVar);
        }
    }

    @NonNull
    /* renamed from: j */
    public synchronized xo mo292j() {
        return this.f237j;
    }

    @NonNull
    /* renamed from: k */
    public vd mo293k() {
        if (this.f238k == null) {
            synchronized (this) {
                if (this.f238k == null) {
                    this.f238k = new vd(this.f229b, mo292j().mo2629d());
                }
            }
        }
        return this.f238k;
    }

    @Nullable
    /* renamed from: l */
    public synchronized ck mo294l() {
        return this.f239l;
    }

    /* renamed from: a */
    public synchronized void mo282a(@NonNull cl clVar) {
        this.f239l = new ck(this.f229b, clVar);
    }
}
