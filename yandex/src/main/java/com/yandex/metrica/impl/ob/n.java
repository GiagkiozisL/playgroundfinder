package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.rl.a;
import com.yandex.metrica.profile.UserProfile;
import com.yandex.metrica.profile.UserProfileUpdate;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.n */
public abstract class n implements an {

    /* renamed from: f */
    private static final Collection<Integer> f1488f = new HashSet(Arrays.asList(new Integer[]{Integer.valueOf(14), Integer.valueOf(15)}));

    /* renamed from: g */
    private static final yk<a> f1489g = new yk<a>() {
        /* renamed from: a */
        public yi a(@NonNull rl.a aVar) {
            if (cx.m1195a( aVar.b)) {
                return yi.a(this, "attributes list is empty");
            }
            return yi.a(this);
        }
    };

    /* renamed from: h */
    private static final yk<Revenue> f1490h = new yo();

    /* renamed from: a */
    protected final Context f1491a;

    /* renamed from: B */
    protected bz f1492b;
    @NonNull

    /* renamed from: a */
    protected vz f1493c;// = vr.m4237a(this.f1492b.mo744h().mo39e());
    @NonNull

    /* renamed from: d */
    protected vp f1494d;

    /* renamed from: e */
    protected final cd f1495e;

    /* renamed from: i */
    private at f1496i;
    @NonNull

    /* renamed from: j */
    private final ag f1497j;

    n(Context context, cd cdVar, @NonNull bz bzVar, @NonNull ag agVar) {
        this.f1491a = context.getApplicationContext();
        this.f1495e = cdVar;
        this.f1492b = bzVar;
        this.f1497j = agVar;
        this.f1492b.mo544a(new xx(this.f1493c, "Crash Environment"));
        this.f1493c = vr.m4237a(this.f1492b.mo744h().mo39e());
        this.f1494d = vr.m4239b(this.f1492b.mo744h().mo39e());
        if (vi.m4192a(this.f1492b.mo744h().mo47j())) {
            this.f1493c.mo2474a();
            this.f1494d.mo2474a();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1451a() {
        this.f1495e.mo564a(this.f1492b);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1454a(ug ugVar) {
        this.f1492b.mo548b(ugVar);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1453a(at atVar) {
        this.f1496i = atVar;
    }

    /* renamed from: B */
    public void mo1459b(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.f1492b.mo545a(str, str2);
        } else if (this.f1493c.mo2485c()) {
            this.f1493c.mo2482b("Invalid Error Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    /* renamed from: a */
    public void mo1456a(Map<String, String> map) {
        if (!cx.m1193a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                mo1459b((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    /* renamed from: B */
    public void mo1460b(Map<String, String> map) {
        if (!cx.m1193a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                mo1462c((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    /* renamed from: a */
    public void mo1462c(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.f1495e.mo577a(str, str2, this.f1492b);
        } else if (this.f1493c.mo2485c()) {
            this.f1493c.mo2482b("Invalid App Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    /* renamed from: B */
    public void mo1457b() {
        this.f1495e.mo582b(this.f1492b);
    }

    public void resumeSession() {
        mo1455a((String) null);
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2477a("Resume session");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo1455a(String str) {
        this.f1495e.mo568a(this);
        this.f1496i.mo323b();
        this.f1495e.mo572a(af.m290d(str, this.f1493c), this.f1492b);
        mo246a(this.f1492b.mo550d());
    }

    /* renamed from: a */
    private void mo246a(boolean z) {
        if (z) {
            this.f1495e.mo572a(af.m271a(C0058a.EVENT_TYPE_PURGE_BUFFER, this.f1493c), this.f1492b);
        }
    }

    public void pauseSession() {
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2477a("Pause session");
        }
        mo1458b((String) null);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo1458b(String str) {
        if (!this.f1492b.mo546a()) {
            this.f1495e.mo583b(this);
            this.f1496i.mo322a();
            this.f1492b.mo549c();
            this.f1495e.mo572a(af.m292e(str, this.f1493c), this.f1492b);
        }
    }

    public void reportEvent(@NonNull String eventName) {
        if (this.f1493c.mo2485c()) {
            mo335e(eventName);
        }
        m2488a(af.m287c(eventName, this.f1493c));
    }

    public void reportEvent(@NonNull String eventName, String jsonValue) {
        if (this.f1493c.mo2485c()) {
            m2495d(eventName, jsonValue);
        }
        m2488a(af.m276a(eventName, jsonValue, this.f1493c));
    }

    public void reportEvent(@NonNull String eventName, @Nullable Map<String, Object> attributes) {
        Log.d(Constant.RUS_TAG, "n$reportEvent");
        Map b = cx.m1198b(attributes);
        this.f1495e.mo562a(af.m287c(eventName, this.f1493c), mo1464d(), b);
        if (this.f1493c.mo2485c()) {
            m2495d(eventName, b == null ? null : b.toString());
        }
    }

    /* renamed from: e */
    private void mo335e(String str) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Event received: ");
            sb.append(mo1465d(str));
            this.f1493c.mo2477a(sb.toString());
        }
    }

    /* renamed from: d */
    private void m2495d(String str, String str2) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Event received: ");
            sb.append(mo1465d(str));
            sb.append(". With value: ");
            sb.append(mo1465d(str2));
            this.f1493c.mo2477a(sb.toString());
        }
    }

    /* renamed from: e */
    private void m2497e(@Nullable String str, @Nullable String str2) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Statbox event received ");
            sb.append(" with name: ");
            sb.append(mo1465d(str));
            sb.append(" with value: ");
            String d = mo1465d(str2);
            if (d.length() > 100) {
                sb.append(d.substring(0, 100));
                sb.append("...");
            } else {
                sb.append(d);
            }
            this.f1493c.mo2477a(sb.toString());
        }
    }

    /* renamed from: a */
    public void mo1452a(int i, String str, String str2, Map<String, String> map) {
        if (!m2491a(i)) {
            m2488a(af.m270a(i, str, str2, map == null ? null : new HashMap<String, Object>(map), this.f1493c));
        }
    }

    /* renamed from: a */
    private boolean m2491a(int i) {
        return !f1488f.contains(Integer.valueOf(i)) && i >= 1 && i <= 99;
    }

    public void reportError(@NonNull String message, Throwable error) {
        this.f1495e.mo576a(message, new kl(error, null, this.f1497j.mo261a(), this.f1497j.mo262b()), this.f1492b);
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2478a("Error received: %s", mo1465d(message));
        }
    }

    public void sendEventsBuffer() {
        this.f1495e.mo572a(af.m271a(C0058a.EVENT_TYPE_PURGE_BUFFER, this.f1493c), this.f1492b);
    }

    /* renamed from: a */
    public void a(@Nullable String str, @Nullable String str2) {
        m2488a(af.m284b(str, str2, this.f1493c));
        m2497e(str, str2);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo1463c() {
        boolean z = !mo1466f();
        if (z) {
            this.f1495e.mo572a(af.m292e("", this.f1493c), this.f1492b);
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public bz mo1464d() {
        return this.f1492b;
    }

    /* renamed from: a */
    private void m2488a(w wVar) {
        this.f1495e.mo572a(wVar, this.f1492b);
    }

    /* renamed from: a */
    public void mo1461c(@Nullable String seed) {
        mo336f(seed);
    }

    /* renamed from: f */
    private void mo336f(String seed) {
        this.f1495e.mo575a(am.compressNencodeB64(seed), this.f1492b);
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2477a("Error received: native");
        }
    }

    public void reportUnhandledException(@NonNull Throwable exception) {
        m2493b(new kl(exception, null, this.f1497j.mo261a(), this.f1497j.mo262b()));
    }

    /* renamed from: a */
    public void a(@NonNull kl klVar) {
        m2493b(klVar);
    }

    /* renamed from: B */
    private void m2493b(@NonNull kl klVar) {
        this.f1495e.mo567a(klVar, this.f1492b);
        m2494c(klVar);
    }

    /* renamed from: a */
    private void m2494c(@NonNull kl klVar) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Unhandled exception received: ");
            sb.append(klVar.toString());
            this.f1493c.mo2477a(sb.toString());
        }
    }

    public void reportRevenue(@NonNull Revenue revenue) {
        m2487a(revenue);
    }

    /* renamed from: a */
    private void m2487a(@NonNull Revenue revenue) {
        yi a = f1490h.a(revenue);
        if (a.a()) {
            this.f1495e.mo565a(new cg(revenue, this.f1493c), this.f1492b);
            m2492b(revenue);
        } else if (this.f1493c.mo2485c()) {
            this.f1493c.mo2481b("Passed revenue is not valid. Reason: " + a.b());
        }
    }

    /* renamed from: B */
    private void m2492b(@NonNull Revenue revenue) {
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Revenue received ");
            sb.append("for productID: ");
            sb.append(mo1465d(revenue.productID));
            sb.append(" of quantity: ");
            if (revenue.quantity != null) {
                sb.append(revenue.quantity);
            } else {
                sb.append("<null>");
            }
            sb.append(" with price: ");
            sb.append(revenue.price);
            sb.append(" ");
            sb.append(revenue.currency);
            this.f1493c.mo2477a(sb.toString());
        }
    }

    public void reportUserProfile(@NonNull UserProfile profile) {
        m2489a(profile);
    }

    /* renamed from: a */
    private void m2489a(@NonNull UserProfile userProfile) {
        re reVar = new re();
        for (UserProfileUpdate userProfileUpdatePatcher : userProfile.getUserProfileUpdates()) {
            rf userProfileUpdatePatcher2 = userProfileUpdatePatcher.getUserProfileUpdatePatcher();
            userProfileUpdatePatcher2.a(this.f1493c);
            userProfileUpdatePatcher2.a(reVar);
        }
        rl.a c = reVar.mo1773c();
        yi a = f1489g.a(c);
        if (a.a()) {
            this.f1495e.mo569a(c, this.f1492b);
            mo337g();
        } else if (this.f1493c.mo2485c()) {
            this.f1493c.mo2481b("UserInfo wasn't sent because " + a.b());
        }
    }

    /* renamed from: g */
    private void mo337g() {
        if (this.f1493c.mo2485c()) {
            this.f1493c.mo2477a(new StringBuilder("User profile received").toString());
        }
    }

    public void setUserProfileID(@Nullable String userProfileID) {
        this.f1495e.mo585b(userProfileID, this.f1492b);
        if (this.f1493c.mo2485c()) {
            StringBuilder sb = new StringBuilder("Set user profile ID: ");
            sb.append(mo1465d(userProfileID));
            this.f1493c.mo2477a(sb.toString());
        }
    }

    public void setStatisticsSending(boolean value) {
        this.f1492b.mo744h().mo44g(value);
    }

    /* renamed from: a */
    public void a(@NonNull kg kgVar) {
        this.f1495e.mo566a(new kh(kgVar, this.f1497j.mo261a(), this.f1497j.mo262b()), this.f1492b);
    }

    /* renamed from: e */
    public void e() {
        this.f1495e.mo572a(w.m4271a(this.f1491a), this.f1492b);
    }

    /* renamed from: f */
    public boolean mo1466f() {
        return this.f1492b.mo546a();
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: d */
    public String mo1465d(@Nullable String str) {
        if (str == null) {
            return "<null>";
        }
        if (str.isEmpty()) {
            return "<empty>";
        }
        return str;
    }
}
