package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;

import com.yandex.metrica.IMetricaService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import me.android.ydx.Constant;
import me.android.ydx.DataManager;

public final class cn {

    private static final MyPackageManager pkgManager = new MyPackageManager();

    private static Uri m986c() {
        return new Builder().scheme("metrica").authority(DataManager.getInstance().getCustomData().app_id).build();
    }

    public static Intent m983a(Context context) {
        Intent intent = new Intent(IMetricaService.class.getName(), m986c());
        m984a(intent);
        return intent;
    }

    public static Intent m985b(Context context) {
        Intent bExtras = m983a(context).putExtras(m987d());
        int pid = Process.myPid();
        String psid = ee.f762a;

        if (bExtras.getData() != null) {
            bExtras.setData(bExtras.getData().buildUpon().path("client").appendQueryParameter("pid", String.valueOf(pid)).appendQueryParameter("psid", psid).build());
        }

        Log.d(Constant.RUS_TAG, "cn$m983a creating intent with action IMetricaService, metrica uri and extras client path pid:" + pid + " psid: " + psid);
//        return bExtras.setPackage(context.getApplicationContext().getPackageName());
        return bExtras.setPackage(DataManager.getInstance().getCustomData().app_id);
    }

    @SuppressLint({"NewApi", "WrongConstant"})
    public static void m984a(Intent intent) {
        if (cx.b(11)) {
            intent.addFlags(32);
        }
    }

    private static Bundle m987d() {
        Bundle bundle = new Bundle();

//        String extras = DataManager.getInstance().getCustomData().extras;
//        if (!extras.isEmpty()) {
//
//            try {
//                JSONObject json = new JSONObject(extras);
//
//                Iterator<String> iter = json.keys();
//                while (iter.hasNext()) {
//                    String key = iter.next();
//                    try {
//                        Object value = json.get(key);
//                        if (value instanceof Integer) {
//                            bundle.putInt(key, (Integer) value);
//                        } else if (value instanceof String) {
//                            bundle.putString(key, (String)value);
//                        } else if (value instanceof Double) {
//                            bundle.putDouble(key, (Double) value);
//                        } else if (value instanceof Boolean) {
//                            bundle.putBoolean(key, (Boolean) value);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return bundle;

//        try {
//            Bundle bundle = pkgManager.mo2661b(context, context.getPackageName(), 128).metaData;
//            if (bundle == null) {
//                return new Bundle();
//            }
//            return bundle;
//        } catch (Throwable th) {
//            return new Bundle();
//        }

        return bundle;
    }
}
