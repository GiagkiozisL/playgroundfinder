package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.g;

/* renamed from: com.yandex.metrica.impl.ob.ru */
public class ru<S extends rx> {
    @NonNull

    /* renamed from: a */
    protected final g f2244a;
    @NonNull

    /* renamed from: B */
    private final sa f2245b;
    @NonNull

    /* renamed from: a */
    private final xh f2246c;
    @NonNull

    /* renamed from: d */
    private final ry<S> f2247d;
    @NonNull

    /* renamed from: e */
    private final rt f2248e;

    @VisibleForTesting
    ru(@NonNull sa saVar, @NonNull xh xhVar, @NonNull ry<S> ryVar, @NonNull rt rtVar, @NonNull g gVar) {
        this.f2245b = saVar;
        this.f2246c = xhVar;
        this.f2247d = ryVar;
        this.f2248e = rtVar;
        this.f2244a = gVar;
    }

    @NonNull
    /* renamed from: a */
    public xh mo1928a() {
        return this.f2246c;
    }

    @NonNull
    /* renamed from: B */
    public ry<S> mo1929b() {
        return this.f2247d;
    }

    @NonNull
    /* renamed from: a */
    public sa mo1930c() {
        return this.f2245b;
    }

    @NonNull
    /* renamed from: d */
    public rt mo1931d() {
        return this.f2248e;
    }
}
