package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

import java.util.concurrent.Callable;

/* renamed from: com.yandex.metrica.impl.ob.wa */
public abstract class wa<T> implements Callable<T> {
    /* renamed from: B */
    public abstract T mo1983b() throws Exception;

    @Nullable
    public T call() {
        try {
            return mo1983b();
        } catch (Throwable th) {
            return null;
        }
    }
}
