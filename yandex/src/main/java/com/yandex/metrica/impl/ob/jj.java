package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import me.android.ydx.DataManager;

class jj {

    @NonNull
    ly f1134a;

    @NonNull
    private final eh f1135b;

    @NonNull
    private String pkgName;

    @NonNull
    private ji f1137d;

    public jj(@NonNull Context context) {
        this(DataManager.getInstance().getCustomData().app_id, new ly(ld.m2146a(context).mo1238c()), new ji());
    }

    @VisibleForTesting
    jj(@NonNull String pkgName, @NonNull ly lyVar, @NonNull ji jiVar) {
        this.pkgName = pkgName;
        this.f1134a = lyVar;
        this.f1137d = jiVar;
        this.f1135b = new eh(this.pkgName);
    }

    @NonNull
    public Bundle mo1104a() {
        Bundle bundle = new Bundle();
        this.f1137d.mo1103a(bundle, this.pkgName, this.f1134a.mo1393g());
        return bundle;
    }
}
