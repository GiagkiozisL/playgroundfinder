package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.h.C0417b;

/* renamed from: com.yandex.metrica.impl.ob.ow */
class ow {
    @NonNull

    /* renamed from: a */
    private final Context f1682a;
    @NonNull

    /* renamed from: B */
    private uk f1683b;
    @Nullable

    /* renamed from: a */
    private oh f1684c;
    @NonNull

    /* renamed from: d */
    private final bq f1685d;
    @NonNull

    /* renamed from: e */
    private final lh f1686e;
    @NonNull

    /* renamed from: f */
    private final lg f1687f;
    @NonNull

    /* renamed from: g */
    private final wh f1688g;
    @NonNull

    /* renamed from: h */
    private final pg f1689h;
    @NonNull

    /* renamed from: i */
    private final h f1690i;
    @NonNull

    /* renamed from: j */
    private final C0417b f1691j;
    @NonNull

    /* renamed from: k */
    private final pi f1692k;
    @NonNull

    /* renamed from: l */
    private final xh f1693l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public boolean f1694m;

    public ow(@NonNull Context context, @NonNull uk ukVar, @Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull xh xhVar) {
        this(context, ukVar, ohVar, new bq(), lhVar, lgVar, xhVar, new wg(), new pg(), al.m324a().mo291i(), new pi(context));
    }

    /* renamed from: a */
    public void mo1613a() {
        if (m2766d()) {
            m2761b();
        }
    }

    /* renamed from: B */
    private void m2761b() {
        if (!this.f1694m) {
            this.f1690i.mo969a(h.a, this.f1693l, this.f1691j);
        } else {
            m2764c();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2764c() {
        if (this.f1684c != null) {
            pd a = this.f1689h.mo1632a(this.f1682a, this.f1683b, this.f1684c);
            boolean a2 = this.f1692k.mo1636a();
            do {
                if (this.f1692k.mo1636a()) {
                    boolean a3 = a.mo461a();
                    if (!a3 || !a.c().mo1638b()) {
                        a2 = a3;
                        continue;
                    } else {
                        int i = 0;
                        while (this.f1693l.c() && a3) {
                            i++;
                            this.f1685d.mo492a(a);
                            a3 = !a.mo463b() && a.mo482t();
                        }
                        a2 = a3;
                        continue;
                    }
                }
            } while (a2);
        }
    }

    /* renamed from: d */
    private boolean m2766d() {
        return m2758a((kw) this.f1686e) || m2758a((kw) this.f1687f);
    }

    /* renamed from: a */
    private boolean m2758a(kw kwVar) {
        if (this.f1684c == null || (!m2762b(kwVar) && !m2765c(kwVar))) {
            return false;
        }
        return true;
    }

    /* renamed from: B */
    private boolean m2762b(kw kwVar) {
        return this.f1684c != null && m2759a(kwVar, (long) this.f1684c.f1573g);
    }

    /* renamed from: a */
    private boolean m2759a(kw kwVar, long j) {
        return kwVar.mo1177a() >= j;
    }

    /* renamed from: a */
    private boolean m2765c(kw kwVar) {
        return this.f1684c != null && m2763b(kwVar, this.f1684c.f1575i);
    }

    /* renamed from: B */
    private boolean m2763b(kw kwVar, long j) {
        return this.f1688g.a() - kwVar.mo1182b() > j;
    }

    /* renamed from: a */
    public void mo1614a(@NonNull uk ukVar, @Nullable oh ohVar) {
        this.f1683b = ukVar;
        this.f1684c = ohVar;
    }

    @VisibleForTesting
    ow(@NonNull Context context, @NonNull uk ukVar, @Nullable oh ohVar, @NonNull bq bqVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull xh xhVar, @NonNull wh whVar, @NonNull pg pgVar, @NonNull h hVar, @NonNull pi piVar) {
        this.f1694m = false;
        this.f1682a = context;
        this.f1684c = ohVar;
        this.f1683b = ukVar;
        this.f1685d = bqVar;
        this.f1686e = lhVar;
        this.f1687f = lgVar;
        this.f1693l = xhVar;
        this.f1688g = whVar;
        this.f1689h = pgVar;
        this.f1690i = hVar;
        this.f1691j = new C0417b() {
            /* renamed from: a */
            public void mo972a() {
                ow.this.f1694m = true;
                ow.this.m2764c();
            }
        };
        this.f1692k = piVar;
    }
}
