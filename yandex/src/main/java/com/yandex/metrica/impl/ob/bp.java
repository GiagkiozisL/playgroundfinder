package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.bp */
public abstract class bp<T extends sy> extends bs<T> {
    @NonNull

    /* renamed from: j */
    private final xb f394j;
    @NonNull

    /* renamed from: k */
    private final vm f395k;
    @NonNull

    /* renamed from: l */
    private final wh f396l;

    /* access modifiers changed from: protected */
    /* renamed from: E */
    public abstract void mo489E();

    /* access modifiers changed from: protected */
    /* renamed from: F */
    public abstract void mo490F();

    public bp(@NonNull T t) {
        this(new ab(), new wu(), new vm(), new wg(), t);
    }

    public bp(@NonNull bn bnVar, @NonNull xb xbVar, @NonNull vm vmVar, @NonNull wh whVar, @NonNull T t) {
        super(bnVar, t);
        this.f394j = xbVar;
        this.f395k = vmVar;
        this.f396l = whVar;
        t.mo2212a(this.f394j);
    }

    /* renamed from: d */
    public void d() {
        super.d();
        mo450a(this.f396l.a());
    }

    /* renamed from: B */
    public boolean mo463b() {
        boolean b = super.mo463b();
        if (b) {
            mo489E();
        } else if (mo478p()) {
            mo490F();
        }
        return b;
    }

    /* renamed from: a */
    public boolean mo491c(@NonNull byte[] bArr) {
        try {
            byte[] seedBts = this.f395k.mo2492a(bArr);
            if (seedBts == null) {
                return false;
            }
            byte[] a2 = this.f394j.a(seedBts);
            if (a2 == null) {
                return false;
            }
            setEncryptedBytes(a2);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* renamed from: a */
    public void setEncryptedBytes(byte[] bArr) {
        super.setEncryptedBytes(bArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public boolean mo478p() {
        return mo473k() == 400;
    }
}
