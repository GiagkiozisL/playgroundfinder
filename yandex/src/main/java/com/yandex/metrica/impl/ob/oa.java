package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.pm.FeatureInfo;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.oa */
public abstract class oa {

    /* renamed from: com.yandex.metrica.impl.ob.oa$a */
    public static class C0697a {
        /* renamed from: a */
        public static oa m2646a() {
            if (cx.a(24)) {
                return new C0698b();
            }
            return new C0699c();
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.oa$B */
    public static class C0698b extends oa {
        @TargetApi(24)
        /* renamed from: a */
        public ob mo1530a(@NonNull FeatureInfo featureInfo) {
            return new ob(featureInfo.name, featureInfo.version, mo1532c(featureInfo));
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.oa$a */
    public static class C0699c extends oa {
        /* renamed from: a */
        public ob mo1530a(@NonNull FeatureInfo featureInfo) {
            return new ob(featureInfo.name, mo1532c(featureInfo));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract ob mo1530a(FeatureInfo featureInfo);

    /* renamed from: B */
    public ob mo1531b(@NonNull FeatureInfo featureInfo) {
        if (featureInfo.name != null) {
            return mo1530a(featureInfo);
        }
        if (featureInfo.reqGlEsVersion == 0) {
            return mo1530a(featureInfo);
        }
        return new ob("openGlFeature", featureInfo.reqGlEsVersion, mo1532c(featureInfo));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo1532c(FeatureInfo featureInfo) {
        return (featureInfo.flags & 1) != 0;
    }
}
