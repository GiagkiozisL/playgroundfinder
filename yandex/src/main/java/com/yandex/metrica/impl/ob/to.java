package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.to */
public abstract class to {
    @NonNull

    /* renamed from: a */
            tr a;
    @NonNull

    /* renamed from: B */
            Uri b;
    @NonNull

    /* renamed from: a */
    private Socket c;

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public abstract void a();

    to(@NonNull Socket socket, @NonNull Uri uri, @NonNull tr trVar) {
        this.c = socket;
        this.b = uri;
        this.a = trVar;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void a(@NonNull String str, @NonNull Map<String, String> map, @NonNull byte[] bArr) {
        BufferedOutputStream bufferedOutputStream;
        BufferedOutputStream bufferedOutputStream2 = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(this.c.getOutputStream());
            try {
                bufferedOutputStream.write(str.getBytes());
                a(bufferedOutputStream);
                for (Entry entry : map.entrySet()) {
                    a((OutputStream) bufferedOutputStream, (String) entry.getKey(), (String) entry.getValue());
                }
                a(bufferedOutputStream);
                bufferedOutputStream.write(bArr);
                bufferedOutputStream.flush();
                this.a.a(this.c.getLocalPort());
                cx.a((Closeable) bufferedOutputStream);
            } catch (IOException e) {
                e = e;
                bufferedOutputStream2 = bufferedOutputStream;
                try {
                    this.a.a("io_exception_during_sync", (Throwable) e);
                    cx.a((Closeable) bufferedOutputStream2);
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream = bufferedOutputStream2;
                    cx.a((Closeable) bufferedOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
//                th = th2;
                cx.a((Closeable) bufferedOutputStream);
//                throw th;
            }
        } catch (IOException e2) {
//            e = e2;
            this.a.a("io_exception_during_sync", (Throwable) e2);
            cx.a((Closeable) bufferedOutputStream2);
        } catch (Throwable th3) {
//            th = th3;
            bufferedOutputStream = null;
            cx.a((Closeable) bufferedOutputStream);
//            throw th;
        }
    }

    /* renamed from: a */
    private void a(@NonNull OutputStream outputStream, @NonNull String str, @NonNull String str2) throws IOException {
        outputStream.write((str + ": " + str2).getBytes());
        a(outputStream);
    }

    /* renamed from: a */
    private void a(@NonNull OutputStream outputStream) throws IOException {
        outputStream.write("\r\n".getBytes());
    }
}
