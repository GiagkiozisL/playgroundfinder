package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.nq */
public class nq {
    @Nullable

    /* renamed from: a */
    public final String f1540a;
    @Nullable

    /* renamed from: B */
    public final String f1541b;

    public nq(@Nullable String str, @Nullable String str2) {
        this.f1540a = str;
        this.f1541b = str2;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        nq nqVar = (nq) o;
        if (this.f1540a == null ? nqVar.f1540a != null : !this.f1540a.equals(nqVar.f1540a)) {
            return false;
        }
        if (this.f1541b != null) {
            return this.f1541b.equals(nqVar.f1541b);
        }
        if (nqVar.f1541b != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.f1540a != null) {
            i = this.f1540a.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        if (this.f1541b != null) {
            i2 = this.f1541b.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        return "AppMetricaDeviceIdentifiers{deviceID='" + this.f1540a + '\'' + ", deviceIDHash='" + this.f1541b + '\'' + '}';
    }
}
