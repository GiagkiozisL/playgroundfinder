package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.oh.C0707a;
import com.yandex.metrica.impl.ob.rh.b.C0820b;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.pb */
public class pb {
    @NonNull
    /* renamed from: a */
    public C0820b mo1627a(@NonNull os osVar) {
        C0820b bVar = new C0820b();
        Location c = osVar.mo1597c();
        bVar.f1928b = osVar.mo1595a() == null ? bVar.f1928b : osVar.mo1595a().longValue();
        bVar.f1930d = TimeUnit.MILLISECONDS.toSeconds(c.getTime());
        bVar.f1938l = m2788a(osVar.f1660a);
        bVar.f1929c = TimeUnit.MILLISECONDS.toSeconds(osVar.mo1596b());
        bVar.f1939m = TimeUnit.MILLISECONDS.toSeconds(osVar.mo1598d());
        bVar.f1931e = c.getLatitude();
        bVar.f1932f = c.getLongitude();
        bVar.f1933g = Math.round(c.getAccuracy());
        bVar.f1934h = Math.round(c.getBearing());
        bVar.f1935i = Math.round(c.getSpeed());
        bVar.f1936j = (int) Math.round(c.getAltitude());
        bVar.f1937k = m2789a(c.getProvider());
        return bVar;
    }

    /* renamed from: a */
    private static int m2788a(@NonNull C0707a aVar) {
        switch (aVar) {
            case BACKGROUND:
                return 1;
            default:
                return 0;
        }
    }

    /* renamed from: a */
    private static int m2789a(@NonNull String str) {
        if ("gps".equals(str)) {
            return 1;
        }
        if ("network".equals(str)) {
            return 2;
        }
        return 0;
    }
}
