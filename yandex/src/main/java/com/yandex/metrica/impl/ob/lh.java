package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.lh */
public class lh extends kw {
    lh(@NonNull Context context, @NonNull lc lcVar) {
        this(lcVar, new ly(ld.m2146a(context).mo1238c()));
    }

    @VisibleForTesting
    lh(@NonNull lc lcVar, @NonNull ly lyVar) {
        super(lcVar, lyVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public long mo1186c(long j) {
        return mo1187c().mo1383d(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: d */
    public ly mo1189d(long j) {
        return mo1187c().mo1388e(j);
    }

    @NonNull
    /* renamed from: e */
    public String e() {
        return "l_dat";
    }
}
