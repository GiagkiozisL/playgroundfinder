package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.vq.a;

/* renamed from: com.yandex.metrica.impl.ob.jg */
public class jg implements jf {

    /* renamed from: a */
    protected lw f1127a;

    /* renamed from: B */
    private final String f1128b;

    /* renamed from: a */
    private a f1129c;

    public jg(lw lwVar, String str) {
        a aVar;
        this.f1127a = lwVar;
        this.f1128b = str;
        a aVar2 = new a();
        try {
            String c = this.f1127a.mo1331c(this.f1128b);
            aVar = !TextUtils.isEmpty(c) ? new a(c) : aVar2;
        } catch (Throwable th) {
            aVar = aVar2;
        }
        this.f1129c = aVar;
    }

    @Nullable
    /* renamed from: B */
    public Long mo1092b() {
        return this.f1129c.mo2500d("SESSION_ID");
    }

    /* renamed from: d */
    public jg mo1094d(long j) {
        m1931a("SESSION_ID", Long.valueOf(j));
        return this;
    }

    @Nullable
    /* renamed from: a */
    public Long mo1093c() {
        return this.f1129c.mo2500d("SESSION_INIT_TIME");
    }

    /* renamed from: e */
    public jg mo1096e(long j) {
        m1931a("SESSION_INIT_TIME", Long.valueOf(j));
        return this;
    }

    @Nullable
    /* renamed from: d */
    public Long mo1095d() {
        return this.f1129c.mo2500d("SESSION_COUNTER_ID");
    }

    /* renamed from: a */
    public jg a(long j) {
        m1931a("SESSION_COUNTER_ID", Long.valueOf(j));
        return this;
    }

    @Nullable
    /* renamed from: e */
    public Long mo1097e() {
        return this.f1129c.mo2500d("SESSION_SLEEP_START");
    }

    /* renamed from: B */
    public jg b(long j) {
        m1931a("SESSION_SLEEP_START", Long.valueOf(j));
        return this;
    }

    @Nullable
    /* renamed from: f */
    public Long mo1098f() {
        return this.f1129c.mo2500d("SESSION_LAST_EVENT_OFFSET");
    }

    /* renamed from: a */
    public jg c(long j) {
        m1931a("SESSION_LAST_EVENT_OFFSET", Long.valueOf(j));
        return this;
    }

    @Nullable
    /* renamed from: g */
    public Boolean mo1099g() {
        return this.f1129c.mo2501e("SESSION_IS_ALIVE_REPORT_NEEDED");
    }

    /* renamed from: a */
    public jg a(boolean z) {
        m1931a("SESSION_IS_ALIVE_REPORT_NEEDED", Boolean.valueOf(z));
        return this;
    }

    /* renamed from: h */
    public void mo1100h() {
        this.f1127a.mo1322a(this.f1128b, this.f1129c.toString());
        this.f1127a.mo1364q();
    }

    /* renamed from: i */
    public boolean mo1101i() {
        return this.f1129c.length() > 0;
    }

    /* renamed from: a */
    private void m1931a(String str, Object obj) {
        try {
            this.f1129c.put(str, obj);
        } catch (Throwable th) {
        }
    }

    /* renamed from: a */
    public void a() {
        this.f1129c = new a();
        mo1100h();
    }
}
