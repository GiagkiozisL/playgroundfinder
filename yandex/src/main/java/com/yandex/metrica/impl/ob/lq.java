package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.util.SparseArray;

import com.yandex.metrica.YandexMetrica;

import java.util.List;
import java.util.Locale;

/* renamed from: com.yandex.metrica.impl.ob.lq */
public final class lq {

    /* renamed from: a */
    public static final Boolean f1340a = Boolean.valueOf(false);

    /* renamed from: B */
    public static final int f1341b = YandexMetrica.getLibraryApiLevel();

    /* renamed from: e */
    private static final lb f1344e = new lb();

    /* renamed from: a */
    public static final SparseArray<lr> f1342c = f1344e.mo1219a();

    /* renamed from: d */
    public static final SparseArray<lr> f1343d = f1344e.mo1220b();

    /* renamed from: f */
    private static final le f1345f = new le();

    /* renamed from: g */
    private static final la f1346g = new la(f1344e, f1345f);

    /* renamed from: com.yandex.metrica.impl.ob.lq$a */
    public interface C0577a {

        /* renamed from: a */
        public static final List<String> f1347a = cx.m1181a("incremental_id", "timestamp", "data");

        /* renamed from: com.yandex.metrica.impl.ob.lq$a$a */
        public interface C0578a {

            /* renamed from: a */
            public static final String f1348a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", new Object[]{"lbs_dat"});

            /* renamed from: B */
            public static final String f1349b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", new Object[]{"lbs_dat"});
        }

        /* renamed from: com.yandex.metrica.impl.ob.lq$a$B */
        public interface C0579b {

            /* renamed from: a */
            public static final String f1350a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", new Object[]{"l_dat"});

            /* renamed from: B */
            public static final String f1351b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", new Object[]{"l_dat"});
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$B */
    public static final class C0580b {

        /* renamed from: a */
        public static final List<String> f1352a = cx.m1181a("data_key", "value");
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$a */
    public interface C0581c {

        /* renamed from: a */
        public static final List<String> f1353a = cx.m1181a("key", "value", "type");
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$d */
    public static final class C0582d implements C0581c {
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$e */
    public static final class C0583e {
        /* renamed from: a */
        public static ContentValues m2214a() {
            ContentValues contentValues = new ContentValues();
            contentValues.put("API_LEVEL", Integer.valueOf(YandexMetrica.getLibraryApiLevel()));
            return contentValues;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$f */
    public static final class C0584f {

        /* renamed from: a */
        public static final List<String> f1354a = cx.m1181a("id", "number", "global_number", "number_of_type", "name", "value", "type", "time", "session_id", "wifi_network_info", "cell_info", "location_info", "error_environment", "user_info", "session_type", "app_environment", "app_environment_revision", "truncated", "connection_type", "cellular_connection_type", "custom_type", "wifi_access_point", "encrypting_mode", "profile_id", "first_occurrence_status");

        /* renamed from: B */
        public static final String f1355b = ("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,global_number INTEGER,number_of_type INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT " + jh.FOREGROUND.a() + "," + "app_environment" + " TEXT DEFAULT '" + "{}" + "'," + "app_environment_revision" + " INTEGER DEFAULT " + 0 + "," + "truncated" + " INTEGER DEFAULT 0," + "connection_type" + " INTEGER DEFAULT " + 2 + "," + "cellular_connection_type" + " TEXT," + "custom_type" + " INTEGER DEFAULT 0, " + "wifi_access_point" + " TEXT, " + "encrypting_mode" + " INTEGER DEFAULT " + wz.NONE.mo2592a() + ", " + "profile_id" + " TEXT, " + "first_occurrence_status" + " INTEGER DEFAULT " + aj.UNKNOWN.f225d + " )");
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$g */
    public static final class C0585g {

        /* renamed from: a */
        public static final List<String> f1356a = cx.m1181a("id", "start_time", "network_info", "report_request_parameters", "server_time_offset", "type", "obtained_before_first_sync");

        /* renamed from: B */
        public static final String f1357b = ("CREATE TABLE IF NOT EXISTS sessions (id INTEGER,start_time INTEGER,network_info TEXT,report_request_parameters TEXT,server_time_offset INTEGER,type INTEGER DEFAULT " + jh.FOREGROUND.a() + "," + "obtained_before_first_sync" + " INTEGER DEFAULT 0 )");

        /* renamed from: a */
        public static final String f1358c = String.format(Locale.US, "SELECT DISTINCT %s  FROM %s WHERE %s >=0 AND (SELECT count() FROM %5$s WHERE %5$s.%6$s = %2$s.%3$s AND %5$s.%7$s = %2$s.%4$s) > 0 ORDER BY %3$s LIMIT 1", new Object[]{"report_request_parameters", "sessions", "id", "type", "reports", "session_id", "session_type"});

        /* renamed from: d */
        public static final String f1359d = String.format(Locale.US, "(select count(%s.%s) from %s where %s.%s = %s.%s) = 0 and cast(%s as integer) < ?", new Object[]{"reports", "id", "reports", "reports", "session_id", "sessions", "id", "id"});
    }

    /* renamed from: com.yandex.metrica.impl.ob.lq$h */
    public static final class C0586h implements C0581c {
    }

    /* renamed from: a */
    public static la m2213a() {
        return f1346g;
    }
}
