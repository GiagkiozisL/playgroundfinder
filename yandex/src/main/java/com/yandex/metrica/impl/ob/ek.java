package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.ek */
public class ek {
    @NonNull

    /* renamed from: a */
    private final String pkgName;
    @Nullable

    /* renamed from: B */
    private final String f802b;

    public ek(@NonNull String pkgName, @Nullable String str2) {
        this.pkgName = pkgName;
        this.f802b = str2;
    }

    /* renamed from: a */
    public String mo790a() {
        return this.f802b;
    }

    /* renamed from: B */
    public String mo791b() {
        return this.pkgName;
    }

    public String toString() {
        return this.pkgName + "_" + this.f802b;
    }

    /* renamed from: a */
    public String mo792c() {
        return this.pkgName + "_" + cx.m1197b(this.f802b);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ek ekVar = (ek) o;
        if (this.pkgName == null ? ekVar.pkgName != null : !this.pkgName.equals(ekVar.pkgName)) {
            return false;
        }
        if (this.f802b != null) {
            return this.f802b.equals(ekVar.f802b);
        }
        if (ekVar.f802b != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.pkgName != null) {
            i = this.pkgName.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        if (this.f802b != null) {
            i2 = this.f802b.hashCode();
        }
        return i3 + i2;
    }
}
