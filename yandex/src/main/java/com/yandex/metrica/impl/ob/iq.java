package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rn.C0850a;

import java.util.Iterator;

/* renamed from: com.yandex.metrica.impl.ob.iq */
class iq implements mq<ip, C0850a> {
    iq() {
    }

    @NonNull
    /* renamed from: a */
    public C0850a b(@NonNull ip ipVar) {
        C0850a aVar = new C0850a();
        aVar.f2067e = new int[ipVar.mo1016c().size()];
        int i = 0;
        Iterator it = ipVar.mo1016c().iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                aVar.f2067e[i2] = ((Integer) it.next()).intValue();
                i = i2 + 1;
            } else {
                aVar.f2066d = ipVar.mo1017d();
                aVar.f2065c = ipVar.mo1018e();
                aVar.f2064b = ipVar.mo1015b();
                return aVar;
            }
        }
    }

    @NonNull
    /* renamed from: a */
    public ip a(@NonNull C0850a aVar) {
        return new ip(aVar.f2064b, aVar.f2065c, aVar.f2066d, aVar.f2067e);
    }
}
