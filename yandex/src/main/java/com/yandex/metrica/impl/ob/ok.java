package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.ok */
public class ok {

    /* renamed from: a */
    static final Set<String> f1594a = new HashSet(Arrays.asList(new String[]{"gps"}));
    @NonNull

    /* renamed from: B */
    private Context f1595b;
    @Nullable

    /* renamed from: a */
    private LocationManager f1596c;
    @NonNull

    /* renamed from: d */
    private pr f1597d;

    public ok(@NonNull Context context, @Nullable LocationManager locationManager, @NonNull pr prVar) {
        this.f1595b = context;
        this.f1596c = locationManager;
        this.f1597d = prVar;
    }

    @Nullable
    /* renamed from: a */
    public Location mo1551a() {
        List<String> list;
        Location location;
        if (this.f1596c == null) {
            return null;
        }
        boolean a = this.f1597d.mo1650a(this.f1595b);
        boolean b = this.f1597d.mo1652b(this.f1595b);
        try {
            list = this.f1596c.getAllProviders();
        } catch (Throwable th) {
            list = null;
        }
        if (list == null) {
            return null;
        }
        Location location2 = null;
        for (String str : list) {
            if (!f1594a.contains(str)) {
                if (a) {
                    try {
                        if (!"passive".equals(str) || b) {
                            location = this.f1596c.getLastKnownLocation(str);
                            if (location != null && oi.m2668a(location, location2, oi.f1584a, 200)) {
                                location2 = location;
                            }
                        }
                    } catch (Throwable th2) {
                        location = null;
                    }
                }
                location = null;
                location2 = location;
            }
        }
        return location2;
    }
}
