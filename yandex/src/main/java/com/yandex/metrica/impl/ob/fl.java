package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.fl */
public abstract class fl implements fp {
    @NonNull

    /* renamed from: a */
    private final Context f909a;
    @NonNull

    /* renamed from: B */
    private final fc f910b;
    @NonNull

    /* renamed from: a */
    private final ox f911c;

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public abstract void mo901b(@NonNull w wVar, @NonNull eg egVar);

    public fl(@NonNull Context context, @NonNull fc fcVar) {
        this(context, fcVar, new ox(oo.m2694a(context), al.m324a().mo293k(), cy.m1204a(context), new ly(ld.m2146a(context).mo1238c())));
    }

    /* renamed from: a */
    public void a(@NonNull w wVar, @NonNull eg egVar) {
        mo901b(wVar, egVar);
    }

    /* renamed from: a */
    public void a() {
        this.f910b.mo878b(this);
        this.f911c.mo1616a((Object) this);
    }

    @NonNull
    /* renamed from: B */
    public fc mo900b() {
        return this.f910b;
    }

    @VisibleForTesting
    fl(@NonNull Context context, @NonNull fc fcVar, @NonNull ox oxVar) {
        this.f909a = context.getApplicationContext();
        this.f910b = fcVar;
        this.f911c = oxVar;
        this.f910b.mo876a((fp) this);
        this.f911c.mo1618b(this);
    }

    @NonNull
    /* renamed from: a */
    public ox mo902c() {
        return this.f911c;
    }
}
