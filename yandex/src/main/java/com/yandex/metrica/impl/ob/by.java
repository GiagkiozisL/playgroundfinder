package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rh.c.C0822a;
import com.yandex.metrica.impl.ob.rh.c.C0824c;
import com.yandex.metrica.impl.ob.rh.c.C0825d;
import com.yandex.metrica.impl.ob.rh.c.C0833f;
import com.yandex.metrica.impl.ob.rh.c.g;
import com.yandex.metrica.impl.ob.vq.a;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.by */
class by extends bp<sz> {

    /* renamed from: j */
    rh.c f461j;

    /* renamed from: k */
    kz f462k;

    /* renamed from: l */
    List<Long> f463l;

    /* renamed from: m */
    int m;

    /* renamed from: n */
    int n;
    @NonNull

    /* renamed from: o */
    private final en f466o;

    /* renamed from: p */
    private final Map<String, String> f467p;

    /* renamed from: q */
    private sr f468q;

    /* renamed from: r */
    private c f469r;

    /* renamed from: s */
    private final yb<byte[]> f470s;

    /* renamed from: t */
    private final vz f471t;
    @Nullable

    /* renamed from: u */
    private st u;
    @NonNull

    /* renamed from: v */
    private final lw f473v;

    /* renamed from: w */
    private int f474w;

    /* renamed from: com.yandex.metrica.impl.ob.by$a */
    static class C0174a {
        C0174a() {
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public by mo541a(en enVar) {
            return new by(enVar);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.by$B */
    static final class b {

        /* renamed from: a */
        final rh.c.e a;

        /* renamed from: B */
        final i.a b;

        /* renamed from: a */
        final boolean c;

        b(rh.c.e eVar, i.a aVar, boolean z) {
            this.a = eVar;
            this.b = aVar;
            this.c = z;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.by$a */
    static final class c {

        /* renamed from: a */
        final List<rh.c.e> f480a;

        /* renamed from: B */
        final List<Long> f481b;

        /* renamed from: a */
        final JSONObject f482c;

        c(List<rh.c.e> list, List<Long> list2, JSONObject jSONObject) {
            this.f480a = list;
            this.f481b = list2;
            this.f482c = jSONObject;
        }
    }

    public by(en enVar) {
        this(enVar, enVar.mo823j(), new lw(ld.m2146a(enVar.mo824k()).mo1236b(enVar.b())));
    }

    by(@NonNull en enVar, @NonNull kz kzVar, @NonNull lw lwVar) {
        this(enVar, kzVar, new sz(), lwVar);
    }

    @VisibleForTesting
    by(@NonNull en enVar, @NonNull kz kzVar, @NonNull sz szVar, @NonNull lw lwVar) {
        super(szVar);
        this.f467p = new LinkedHashMap();
        this.m = 0;
        this.n = -1;
        this.f466o = enVar;
        this.f462k = kzVar;
        this.f471t = enVar.mo825l();
        this.f470s = new xr(245760, "event value in ReportTask", this.f471t);
        this.f473v = lwVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo451a(@NonNull Builder builder) {
        ((sz) this.f388i).mo2210a(builder, this.u);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo539a(@NonNull ContentValues contentValues) {
        this.f467p.clear();
        for (Entry entry : contentValues.valueSet()) {
            this.f467p.put((String)entry.getKey(), entry.getValue().toString());
        }
        String asString = contentValues.getAsString("report_request_parameters");
        if (!TextUtils.isEmpty(asString)) {
            try {
                this.f468q = new sr(new a(asString));
                ((sz) this.f388i).mo2215a(this.f468q);
            } catch (Throwable th) {
                m823K();
            }
        } else {
            m823K();
        }
    }

    /* renamed from: K */
    private void m823K() {
        this.f468q = new sr();
        ((sz) this.f388i).mo2215a(this.f468q);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public rh.c mo538a(c cVar, C0824c[] cVarArr, @NonNull List<String> list) {
        rh.c cVar2 = new rh.c();
        C0825d dVar = new C0825d();
        dVar.f1962b = wk.m4370a(this.f468q.f2453b, this.u.getUUId());
        dVar.f1963c = wk.m4370a(this.f468q.f2452a, this.u.getDeviceId());
        this.m += com.yandex.metrica.impl.ob.b.b(4, (com.yandex.metrica.impl.ob.e) dVar);
        cVar2.f1941c = dVar;
        mo540a(cVar2);
        cVar2.f1940b = (rh.c.e[]) cVar.f480a.toArray(new rh.c.e[cVar.f480a.size()]);
        cVar2.f1942d = m828a(cVar.f482c);
        cVar2.f1943e = cVarArr;
        cVar2.f1946h = (String[]) list.toArray(new String[list.size()]);
        this.m += com.yandex.metrica.impl.ob.b.m456h(8);
        return cVar2;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo540a(final rh.c cVar) {
        al.m324a().mo293k().mo2414a((vg) new vg() {
            /* renamed from: a */
            public void a(vf vfVar) {
                m850b(vfVar, cVar);
                m849a(vfVar, cVar);
            }

            /* renamed from: a */
            private void m849a(vf vfVar, rh.c cVar) {
                List a = vfVar.mo2471a();
                if (!cx.a((Collection) a)) {
                    cVar.f1945g = new C0833f[a.size()];
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < a.size()) {
                            cVar.f1945g[i2] = bv.m748a((vb) a.get(i2));
                            by.this.m += com.yandex.metrica.impl.ob.b.m440b((com.yandex.metrica.impl.ob.e) cVar.f1945g[i2]);
                            by.this.m += com.yandex.metrica.impl.ob.b.m456h(10);
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    }
                }
            }

            /* renamed from: B */
            private void m850b(vf vfVar, rh.c cVar) {
                List c = vfVar.mo2473c();
                if (!cx.a((Collection) c)) {
                    cVar.f1944f = new String[c.size()];
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < c.size()) {
                            String str = (String) c.get(i2);
                            if (!TextUtils.isEmpty(str)) {
                                cVar.f1944f[i2] = str;
                                by.this.m += com.yandex.metrica.impl.ob.b.m441b(cVar.f1944f[i2]);
                                by.this.m += com.yandex.metrica.impl.ob.b.m456h(9);
                            }
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    }
                }
            }
        });
    }

    /* renamed from: a */
    public boolean mo461a() {
        List d = this.f466o.mo823j().mo1210d();
        if (d.isEmpty()) {
            return false;
        }
        mo539a((ContentValues) d.get(0));
        this.u = this.f466o.mo822i();
        List Y = this.u.mo2148Y();
        if (Y.isEmpty()) {
            return false;
        }
        mo457a(this.u.mo2158b());
        if (!this.u.mo2135L() || cx.a((Collection) mo481s())) {
            return false;
        }
        this.f463l = null;
        C0824c[] G = mo533G();
        this.f469r = mo534H();
        if (this.f469r.f480a.isEmpty()) {
            return false;
        }
        this.f474w = this.f473v.mo1346l() + 1;
        ((sz) this.f388i).mo2213a(this.f474w);
        this.f461j = mo538a(this.f469r, G, Y);
        this.f463l = this.f469r.f481b;
        mo491c(com.yandex.metrica.impl.ob.e.m1395a((com.yandex.metrica.impl.ob.e) this.f461j));
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: G */
    public C0824c[] mo533G() {
        C0824c[] a = bv.m755a(this.f466o.mo824k());
        if (a != null) {
            for (C0824c b : a) {
                this.m = com.yandex.metrica.impl.ob.b.m440b((com.yandex.metrica.impl.ob.e) b) + this.m;
            }
        }
        return a;
    }

    /* renamed from: a */
    private C0822a[] m828a(JSONObject jSONObject) {
        int length = jSONObject.length();
        if (length <= 0) {
            return null;
        }
        C0822a[] aVarArr = new C0822a[length];
        Iterator keys = jSONObject.keys();
        int i = 0;
        while (true) {
            int i2 = i;
            if (!keys.hasNext()) {
                return aVarArr;
            }
            String str = (String) keys.next();
            try {
                C0822a aVar = new C0822a();
                aVar.f1948b = str;
                aVar.f1949c = jSONObject.getString(str);
                aVarArr[i2] = aVar;
            } catch (Throwable th) {
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: F */
    public void mo490F() {
        m830b(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: E */
    public void mo489E() {
        m830b(false);
    }

    /* renamed from: B */
    private void m830b(boolean z) {
        m824L();
        rh.c.e[] eVarArr = this.f461j.f1940b;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < eVarArr.length) {
                rh.c.e eVar = eVarArr[i2];
                this.f462k.mo1199a(((Long) this.f463l.get(i2)).longValue(), bv.a(eVar.c.d).a(), eVar.d.length, z);
                bv.m754a(eVar);
                i = i2 + 1;
            } else {
                this.f462k.mo1194a(this.f466o.mo817d().mo1072a());
                return;
            }
        }
    }

    /* renamed from: L */
    private void m824L() {
        this.f473v.mo1336e(this.f474w);
    }

    /* renamed from: t */
    public boolean mo482t() {
        return (400 != mo473k()) & super.mo482t();
    }

    /* renamed from: f */
    public void f() {
        if (mo486x()) {
            m825M();
        }
        this.f469r = null;
    }

    /* renamed from: M */
    private void m825M() {
        if (this.f471t.mo2485c()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f469r.f480a.size()) {
                    this.f471t.mo2517a((rh.c.e) this.f469r.f480a.get(i2), "Event sent");
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public c mo534H() {

        ArrayList var1 = new ArrayList();
        ArrayList var2 = new ArrayList();
        i.a var3 = null;
        JSONObject var4 = new JSONObject();
        Cursor var5 = null;

        try {
            var5 = this.I();

            while(var5.moveToNext()) {
                ContentValues var6 = new ContentValues();
                vl.a(var5, var6);
                long var7 = var6.getAsLong("id");
                jh var9 = jh.a(var6.getAsInteger("type"));
                g var10 = bv.a(var6);
                int var11 = bv.a(var9);
                com.yandex.metrica.impl.ob.rh.c.e.b var12 = bv.a(this.u.A(), var11, var10);
                this.m += com.yandex.metrica.impl.ob.b.d(1, 9223372036854775807L);
                this.m += com.yandex.metrica.impl.ob.b.b(2, var12);
                if (this.m >= 250880) {
                    break;
                }

                by.b var13 = this.a(var7, var12);
                if (var13 != null) {
                    if (null == var3) {
                        var3 = var13.b;
                    } else if (!var3.equals(var13.b)) {
                        break;
                    }

                    var2.add(var7);
                    var1.add(var13.a);

                    try {
                        var4 = new JSONObject(var13.b.a);
                    } catch (Throwable var19) {
                    }

                    if (var13.c) {
                        break;
                    }
                }
            }
        } catch (Throwable var20) {
        } finally {
            cx.a(var5);
        }

        return new c(var1, var2, var4);
    }

    /* renamed from: B */
    private i.a b(ContentValues contentValues) {
        return new i.a(contentValues.getAsString("app_environment"), contentValues.getAsLong("app_environment_revision").longValue());
    }

    /* renamed from: a */
    private int a(i.a aVar) {
        try {
            C0822a[] a = m828a(new JSONObject(aVar.a));
            if (a == null) {
                return 0;
            }
            int i = 0;
            for (C0822a b : a) {
                i += com.yandex.metrica.impl.ob.b.b(7, (com.yandex.metrica.impl.ob.e) b);
            }
            return i;
        } catch (Throwable th) {
            return 0;
        }
    }

    public b a(long var1, com.yandex.metrica.impl.ob.rh.c.e.b var3) {

        rh.c.e var4 = new rh.c.e();
        var4.b = var1;
        var4.c = var3;
        Cursor var5 = null;
        jh var6 = bv.a(var3.d);
        i.a var7 = null;
        boolean var8 = false;

        ContentValues var10;
        try {
            var5 = this.a(var1, var6);
            ArrayList var9 = new ArrayList();

            while(var5.moveToNext()) {
                var10 = new ContentValues();
                vl.a(var5, var10);
                rh.c.e.a var11 = this.d(var10);
                if (null != var11) {
                    i.a var12 = this.b(var10);
                    if (var7 == null) {
                        var7 = var12;
                        if (this.n < 0) {
                            this.n = this.a(var12);
                            this.m += this.n;
                        }
                    } else if (!var7.equals(var12)) {
                        var8 = true;
                        break;
                    }

                    this.a(var11);
                    this.m += com.yandex.metrica.impl.ob.b.b(3, var11);
                    if (this.m >= 250880) {
                        break;
                    }

                    var9.add(var11);
                }
            }

            if (var9.size() > 0) {
                var4.d = (rh.c.e.a[])var9.toArray(new rh.c.e.a[var9.size()]);
                return new by.b(var4, var7, var8);
            }

            var10 = null;
        } catch (Throwable var16) {
            return new by.b(var4, var7, var8);
        } finally {
            cx.a(var5);
        }

        return null;
    }

    /* renamed from: a */
    private void a(rh.c.e.a aVar) {
        byte[] bArr = (byte[]) this.f470s.a(aVar.f1988f);
        if (!aVar.f1988f.equals(bArr)) {
            aVar.f1988f = bArr;
            aVar.f1993k = (aVar.f1988f.length - bArr.length) + aVar.f1993k;
        }
    }

    /* renamed from: a */
    private com.yandex.metrica.impl.ob.bv.c m831c(ContentValues contentValues) {
        return com.yandex.metrica.impl.ob.bv.c.m767a(contentValues.getAsInteger("type").intValue(), this.u.mo2136M()).mo509b(contentValues.getAsInteger("custom_type")).mo507a(contentValues.getAsString("name")).mo510b(contentValues.getAsString("value")).mo503a(contentValues.getAsLong("time").longValue()).mo502a(contentValues.getAsInteger("number").intValue()).mo508b(contentValues.getAsInteger("global_number").intValue()).mo511c(contentValues.getAsInteger("number_of_type").intValue()).mo518e(contentValues.getAsString("cell_info")).mo512c(contentValues.getAsString("location_info")).mo515d(contentValues.getAsString("wifi_network_info")).mo522g(contentValues.getAsString("error_environment")).mo523h(contentValues.getAsString("user_info")).mo514d(contentValues.getAsInteger("truncated").intValue()).mo517e(contentValues.getAsInteger("connection_type").intValue()).mo524i(contentValues.getAsString("cellular_connection_type")).mo521f(contentValues.getAsString("wifi_access_point")).mo525j(contentValues.getAsString("profile_id")).mo505a(wz.m4406a(contentValues.getAsInteger("encrypting_mode").intValue())).mo504a(aj.m319a(contentValues.getAsInteger("first_occurrence_status")));
    }

    /* renamed from: d */
    private rh.c.e.a d(ContentValues contentValues) {
        com.yandex.metrica.impl.ob.bv.c c = m831c(contentValues);
        if (c.mo513c() != null) {
            return c.mo519e();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: I */
    public Cursor I() {
        return this.f462k.mo1197a(this.f467p);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Cursor a(long j, jh jhVar) {
        return this.f462k.mo1196a(j, jhVar);
    }

    /* renamed from: C */
    public void mo447C() {
        this.f466o.mo812C().mo1528c();
    }

    /* renamed from: D */
    public void D() {
        this.f466o.mo823j().mo1208c();
        this.f466o.mo812C().mo1527b();
        if (mo486x()) {
            this.f466o.mo812C().mo1526a();
        }
    }

    /* renamed from: J */
    public static C0174a m822J() {
        return new C0174a();
    }
}
