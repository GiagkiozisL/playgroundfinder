package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.oh.C0707a;

import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.oy */
class oy {
    @Nullable
    /* renamed from: a */
    public static String getLocationObjextStr(@NonNull os osVar) {
        Float f;
        Float f2;
        Double d;
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("collection_mode", osVar.f1660a.toString());
            jSONObject.put("lat", osVar.mo1597c().getLatitude());
            jSONObject.put("lon", osVar.mo1597c().getLongitude());
            jSONObject.putOpt("timestamp", Long.valueOf(osVar.mo1597c().getTime()));
            jSONObject.putOpt("receive_timestamp", Long.valueOf(osVar.mo1596b()));
            jSONObject.put("receive_elapsed_realtime_seconds", osVar.mo1598d());
            jSONObject.putOpt("precision", osVar.mo1597c().hasAccuracy() ? Float.valueOf(osVar.mo1597c().getAccuracy()) : null);
            String str = "direction";
            if (osVar.mo1597c().hasBearing()) {
                f = Float.valueOf(osVar.mo1597c().getBearing());
            } else {
                f = null;
            }
            jSONObject.putOpt(str, f);
            String str2 = "speed";
            if (osVar.mo1597c().hasSpeed()) {
                f2 = Float.valueOf(osVar.mo1597c().getSpeed());
            } else {
                f2 = null;
            }
            jSONObject.putOpt(str2, f2);
            String str3 = "altitude";
            if (osVar.mo1597c().hasAltitude()) {
                d = Double.valueOf(osVar.mo1597c().getAltitude());
            } else {
                d = null;
            }
            jSONObject.putOpt(str3, d);
            jSONObject.putOpt("provider", cu.m1159c(osVar.mo1597c().getProvider(), null));
            return jSONObject.toString();
        } catch (Throwable th) {
            return null;
        }
    }

    @Nullable
    /* renamed from: a */
    public static os m2775a(long j, @NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            long optLong = jSONObject.optLong("receive_timestamp", 0);
            long optLong2 = jSONObject.optLong("receive_elapsed_realtime_seconds", 0);
            C0707a a = C0707a.m2665a(jSONObject.optString("collection_mode"));
            Location location = new Location(jSONObject.optString("provider", null));
            location.setLongitude(jSONObject.optDouble("lon", 0.0d));
            location.setLatitude(jSONObject.optDouble("lat", 0.0d));
            location.setTime(jSONObject.optLong("timestamp", 0));
            location.setAccuracy((float) jSONObject.optDouble("precision", 0.0d));
            location.setBearing((float) jSONObject.optDouble("direction", 0.0d));
            location.setSpeed((float) jSONObject.optDouble("speed", 0.0d));
            location.setAltitude(jSONObject.optDouble("altitude", 0.0d));
            return new os(a, optLong, optLong2, location, Long.valueOf(j));
        } catch (Throwable th) {
            return null;
        }
    }

    @Nullable
    /* renamed from: B */
    public static on m2778b(long j, @NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            on onVar = new on();
            try {
                onVar.mo1562a(Long.valueOf(j));
                JSONObject jSONObject = new JSONObject(str);
                onVar.mo1561a(jSONObject.optLong("timestamp", 0));
                onVar.mo1565b(jSONObject.optLong("elapsed_realtime_seconds", 0));
                onVar.mo1566b(jSONObject.optJSONArray("cell_info"));
                onVar.mo1563a(jSONObject.optJSONArray("wifi_info"));
                return onVar;
            } catch (Throwable th) {
                return onVar;
            }
        } catch (Throwable th2) {
            return null;
        }
    }

    @Nullable
    /* renamed from: a */
    public static String getWifiCellInfo(@NonNull on onVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("timestamp", onVar.mo1564b());
            jSONObject.put("elapsed_realtime_seconds", onVar.mo1569e());
            jSONObject.putOpt("wifi_info", onVar.mo1567c());
            jSONObject.putOpt("cell_info", onVar.mo1568d());
            return jSONObject.toString();
        } catch (Throwable th) {
            return null;
        }
    }
}
