package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.CounterConfiguration.C0008a;

/* renamed from: com.yandex.metrica.impl.ob.fn */
public class fn {
    @Nullable

    /* renamed from: a */
    private final String f912a;
    @NonNull

    /* renamed from: B */
    private final String f913b;
    @Nullable

    /* renamed from: a */
    private final Integer f914c;
    @Nullable

    /* renamed from: d */
    private final String f915d;
    @NonNull

    /* renamed from: e */
    private final C0008a f916e;

    public fn(@Nullable String str, @NonNull String str2, @Nullable Integer num, @Nullable String str3, @NonNull C0008a aVar) {
        this.f912a = str;
        this.f913b = str2;
        this.f914c = num;
        this.f915d = str3;
        this.f916e = aVar;
    }

    @Nullable
    /* renamed from: a */
    public String mo907a() {
        return this.f912a;
    }

    @NonNull
    /* renamed from: B */
    public String mo908b() {
        return this.f913b;
    }

    @Nullable
    /* renamed from: a */
    public Integer mo909c() {
        return this.f914c;
    }

    @Nullable
    /* renamed from: d */
    public String mo910d() {
        return this.f915d;
    }

    @NonNull
    /* renamed from: e */
    public C0008a mo911e() {
        return this.f916e;
    }

    @NonNull
    /* renamed from: a */
    public static fn m1619a(@NonNull ed edVar) {
        return new fn(edVar.mo744h().mo39e(), edVar.mo743g().mo759h(), edVar.mo743g().mo756e(), edVar.mo743g().mo757f(), edVar.mo744h().mo54q());
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        fn fnVar = (fn) o;
        if (this.f912a != null) {
            if (!this.f912a.equals(fnVar.f912a)) {
                return false;
            }
        } else if (fnVar.f912a != null) {
            return false;
        }
        if (!this.f913b.equals(fnVar.f913b)) {
            return false;
        }
        if (this.f914c != null) {
            if (!this.f914c.equals(fnVar.f914c)) {
                return false;
            }
        } else if (fnVar.f914c != null) {
            return false;
        }
        if (this.f915d != null) {
            if (!this.f915d.equals(fnVar.f915d)) {
                return false;
            }
        } else if (fnVar.f915d != null) {
            return false;
        }
        if (this.f916e != fnVar.f916e) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int hashCode = (((this.f912a != null ? this.f912a.hashCode() : 0) * 31) + this.f913b.hashCode()) * 31;
        if (this.f914c != null) {
            i = this.f914c.hashCode();
        } else {
            i = 0;
        }
        int i3 = (i + hashCode) * 31;
        if (this.f915d != null) {
            i2 = this.f915d.hashCode();
        }
        return ((i3 + i2) * 31) + this.f916e.hashCode();
    }

    public String toString() {
        return "ClientDescription{mApiKey='" + this.f912a + '\'' + ", mPackageName='" + this.f913b + '\'' + ", mProcessID=" + this.f914c + ", mProcessSessionID='" + this.f915d + '\'' + ", mReporterType=" + this.f916e + '}';
    }
}
