package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.Iterator;

/* renamed from: com.yandex.metrica.impl.ob.gl */
public class gl<T, C extends et> {

    /* renamed from: a */
    private final gr<T> f977a;

    /* renamed from: B */
    private final C b;

    /* renamed from: com.yandex.metrica.impl.ob.gl$a */
    protected interface a<T> {
        /* renamed from: a */
        boolean a(T t, w wVar);
    }

    protected gl(gr<T> grVar, C c) {
        this.f977a = grVar;
        this.b = c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    protected boolean a(@NonNull w var1, @NonNull a<T> var2) {
        Iterator var3 = this.a(var1).mo956a().iterator();

        Object var4;
        do {
            if (!var3.hasNext()) {
                return false;
            }

            var4 = var3.next();
        } while(!var2.a((T)var4, var1));

        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public go<T> a(w wVar) {
        return this.f977a.mo955a(wVar.mo2533g());
    }
}
