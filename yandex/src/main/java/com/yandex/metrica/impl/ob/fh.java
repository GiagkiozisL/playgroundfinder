package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.fh */
public class fh {
    @NonNull

    /* renamed from: a */
    private C0306a f901a;

    public fh(@NonNull C0306a aVar) {
        this.f901a = aVar;
    }

    /* renamed from: a */
    public void mo894a(@NonNull C0306a aVar) {
        this.f901a = this.f901a.b(aVar);
    }

    @NonNull
    /* renamed from: a */
    public C0306a mo893a() {
        return this.f901a;
    }
}
