package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.wk */
public class wk {
    @Nullable
    /* renamed from: a */
    public static <T> T m4369a(@Nullable T t, @Nullable T t2) {
        return t == null ? t2 : t;
    }

    @NonNull
    /* renamed from: a */
    public static String m4370a(@Nullable String str, @NonNull String str2) {
        return TextUtils.isEmpty(str) ? str2 : str;
    }

    @NonNull
    /* renamed from: B */
    public static String m4373b(@Nullable String str, @NonNull String str2) {
        return (String) m4372b(str, str2);
    }

    /* renamed from: a */
    public static boolean m4371a(@Nullable Boolean bool, boolean z) {
        return ((Boolean) m4372b(bool, Boolean.valueOf(z))).booleanValue();
    }

    /* renamed from: a */
    public static long m4367a(@Nullable Long l, long j) {
        return ((Long) m4372b( l,  Long.valueOf(j))).longValue();
    }

    /* renamed from: a */
    public static int m4366a(@Nullable Integer num, int i) {
        return ((Integer) m4372b(num, Integer.valueOf(i))).intValue();
    }

    /* renamed from: a */
    public static float m4365a(@Nullable Float f, float f2) {
        return ((Float) m4372b( f, Float.valueOf(f2))).floatValue();
    }

    /* renamed from: a */
    public static long m4368a(@Nullable Long l, @NonNull TimeUnit timeUnit, long j) {
        return l == null ? j : timeUnit.toMillis(l.longValue());
    }

    @NonNull
    /* renamed from: B */
    private static <T> T m4372b(@Nullable T t, @NonNull T t2) {
        return t == null ? t2 : t;
    }
}
