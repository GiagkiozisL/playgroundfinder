package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

@TargetApi(26)
/* renamed from: com.yandex.metrica.impl.ob.jx */
public class jx extends jy {
    @NonNull

    /* renamed from: a */
    private final xh f1182a;
    @NonNull

    /* renamed from: B */
    private dn f1183b;
    @NonNull

    /* renamed from: a */
    private tw f1184c;

    public jx(@NonNull Context context, @NonNull xh xhVar) {
        this(context, xhVar, new dn(), new tw());
    }

    /* renamed from: a */
    public void mo1136a(@Nullable Bundle bundle, @Nullable jw jwVar) {
        if (bundle == null || bundle.isEmpty()) {
            m2011a(jwVar);
            return;
        }
        int i = bundle.getInt("android.bluetooth.le.extra.ERROR_CODE", Integer.MIN_VALUE);
        int i2 = bundle.getInt("android.bluetooth.le.extra.CALLBACK_TYPE", Integer.MIN_VALUE);
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("android.bluetooth.le.extra.LIST_SCAN_RESULT");
        tt ttVar = this.f1184c.mo2320a(mo1138a()).bleCollectingConfig;
        if (ttVar != null) {
            dm a = this.f1183b.mo713a(mo1138a(), ttVar.c);
            if (i > 0) {
                m2008a(a, i);
            } else if (!cx.a((Collection) parcelableArrayList)) {
                if (parcelableArrayList.size() == 1) {
                    m2009a(a, (ScanResult) parcelableArrayList.get(0), i2);
                } else {
                    m2010a(a, (List<ScanResult>) parcelableArrayList);
                }
            }
            m2012b(jwVar);
            return;
        }
        m2011a(jwVar);
    }

    /* renamed from: a */
    private void m2010a(@NonNull dm dmVar, @NonNull List<ScanResult> list) {
        dmVar.mo712a(list);
    }

    /* renamed from: a */
    private void m2009a(@NonNull dm dmVar, ScanResult scanResult, int i) {
        dmVar.mo711a(scanResult, i > 0 ? Integer.valueOf(i) : null);
    }

    /* renamed from: a */
    private void m2008a(dm dmVar, int i) {
        dmVar.mo710a(i);
    }

    /* renamed from: a */
    private void m2011a(@Nullable jw jwVar) {
        if (jwVar != null) {
            jwVar.a();
        }
    }

    /* renamed from: B */
    private void m2012b(@Nullable final jw jwVar) {
        if (jwVar != null) {
            this.f1182a.a(new Runnable() {
                public void run() {
                    jwVar.a();
                }
            }, TimeUnit.SECONDS.toMillis(5));
        }
    }

    @VisibleForTesting
    public jx(@NonNull Context context, @NonNull xh xhVar, @NonNull dn dnVar, @NonNull tw twVar) {
        super(context);
        this.f1182a = xhVar;
        this.f1183b = dnVar;
        this.f1184c = twVar;
    }
}
