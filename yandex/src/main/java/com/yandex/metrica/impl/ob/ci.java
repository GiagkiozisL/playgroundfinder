package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;

import java.util.regex.Pattern;

import me.android.ydx.CustomData;
import me.android.ydx.DataManager;
import me.android.ydx.Utils;

/* renamed from: com.yandex.metrica.impl.ob.ci */
public class ci {

    /* renamed from: a */
    private static final Pattern f544a = Pattern.compile(".*at com\\.yandex\\.metrica\\.push\\.*");

    /* renamed from: B */
    private static final Pattern f545b = Pattern.compile(".*at com\\.yandex\\.metrica\\.(?!push)");

    @VisibleForTesting
    /* renamed from: com.yandex.metrica.impl.ob.ci$a */
    static class C0199a {
        @NonNull

        /* renamed from: a */
        static final String f546a = new C0199a().mo606a();

        C0199a() {
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        @NonNull
        /* renamed from: a */
        public String mo606a() {

            return DataManager.getInstance().getCustomData().app_framework;
//            String str = "native";
//            if (mo607a("com.unity3d.player.UnityPlayer")) {
//                return "unity";
//            }
//            if (mo607a("mono.MonoPackageManager")) {
//                return "xamarin";
//            }
//            if (mo607a("org.apache.cordova.CordovaPlugin")) {
//                return "cordova";
//            }
//            if (mo607a("com.facebook.react.ReactRootView")) {
//                return "react";
//            }
//            return str;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: a */
        public boolean mo607a(String str) {
            return ci.m946b(str);
        }
    }

    /* renamed from: a */
    static void m943a() {
        Log.i("AppMetrica", "Initializing of Metrica, " + cu.m1155b("release") + " type, Version " + "3.8.0" + ", API Level " + 85 + ", Dated " + "08.10.2019" + ".");
    }

    @NonNull
    /* renamed from: B */
    public static String m945b() {
        return C0199a.f546a;
    }

    /* renamed from: a */
    public static String m942a(String str) {
        CustomData cData = DataManager.getInstance().getCustomData();
//        return str + "/" + "3.8.0" + "." + "66508" + " (" + manuModel() + "; Android " + VERSION.RELEASE + ")";
        return str + "/"
                + cData.analytics_sdk_version
                + "."
                + cData.analytics_sdk_build_number
                + " (" + manuModel(cData.model, cData.manufacturer)
                + "; "
                + Utils.capitalizeFirst(cData.app_platform)
                + " "
                + cData.os_version
                + ")";
    }

    /* renamed from: a */
    public static String manuModel(String model, String manu) {
        if (model.startsWith(manu)) {
            return cu.m1155b(model);
        }
        return cu.m1155b(manu) + " " + model;
    }

    /* renamed from: a */
    static boolean m944a(Throwable th) {
        String a = cx.a(th);
        return !TextUtils.isEmpty(a) && f545b.matcher(a).find();
    }

    /* renamed from: B */
    static boolean m947b(Throwable th) {
        String a = cx.a(th);
        return !TextUtils.isEmpty(a) && f544a.matcher(a).find();
    }

    /* renamed from: B */
    public static boolean m946b(String str) {
        try {
            if (Class.forName(str) != null) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }
}
