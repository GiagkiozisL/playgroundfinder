package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.Pair;

import com.yandex.metrica.Revenue;
import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.impl.ob.rk.C0839a;

/* renamed from: com.yandex.metrica.impl.ob.cg */
class cg {

    @NonNull
    /* renamed from: e */
    private final vz f540e;

    @NonNull

    /* renamed from: a */
    private final Revenue f536a;

    /* renamed from: B */
    private yb<String> f537b;// = new xy(30720, "revenue payload", this.f540e);

    /* renamed from: a */
    private yb<String> f538c;// = new ya(new xy(184320, "receipt data", this.f540e), "<truncated data was not sent, see METRIKALIB-4568>");

    /* renamed from: d */
    private yb<String> f539d;// = new ya(new xz(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_IN_DATABASE_COUNT, "receipt signature", this.f540e), "<truncated data was not sent, see METRIKALIB-4568>");


    cg(@NonNull Revenue revenue, @NonNull vz vzVar) {
        this.f540e = vzVar;
        this.f536a = revenue;

        this.f537b = new xy(30720, "revenue payload", this.f540e);
        this.f538c = new ya(new xy(184320, "receipt data", this.f540e), "<truncated data was not sent, see METRIKALIB-4568>");
        this.f539d = new ya(new xz(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_IN_DATABASE_COUNT, "receipt signature", this.f540e), "<truncated data was not sent, see METRIKALIB-4568>");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public Pair<byte[], Integer> mo605a() {
        int i;
        rk rkVar = new rk();
        rkVar.f2037d = this.f536a.currency.getCurrencyCode().getBytes();
        if (cx.m1189a((Object) this.f536a.price)) {
            rkVar.f2036c = this.f536a.price.doubleValue();
        }
        if (cx.m1189a((Object) this.f536a.priceMicros)) {
            rkVar.f2041h = this.f536a.priceMicros.longValue();
        }
        rkVar.f2038e = cu.m1162d(new xz(200, "revenue productID", this.f540e).a(this.f536a.productID));
        rkVar.f2035b = wk.m4366a(this.f536a.quantity, 1);
        rkVar.f2039f = cu.m1162d((String) this.f537b.a(this.f536a.payload));
        if (cx.m1189a((Object) this.f536a.receipt)) {
            C0839a aVar = new C0839a();
            String str = (String) this.f538c.a(this.f536a.receipt.data);
            if (xu.m4477a(this.f536a.receipt.data, str)) {
                i = 0 + this.f536a.receipt.data.length();
            } else {
                i = 0;
            }
            String str2 = (String) this.f539d.a(this.f536a.receipt.signature);
            aVar.f2042b = cu.m1162d(str);
            aVar.f2043c = cu.m1162d(str2);
            rkVar.f2040g = aVar;
        } else {
            i = 0;
        }
        return new Pair<>(e.m1395a((e) rkVar), Integer.valueOf(i));
    }
}
