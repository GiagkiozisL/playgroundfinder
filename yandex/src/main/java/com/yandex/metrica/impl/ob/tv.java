package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0871c;

/* renamed from: com.yandex.metrica.impl.ob.tv */
public class tv {

    /* renamed from: a */
    public final boolean f2655a;

    /* renamed from: B */
    public final boolean f2656b;

    /* renamed from: a */
    public final boolean f2657c;

    /* renamed from: d */
    public final boolean f2658d;

    /* renamed from: e */
    public final boolean f2659e;

    /* renamed from: f */
    public final boolean f2660f;

    /* renamed from: g */
    public final boolean f2661g;

    /* renamed from: h */
    public final boolean f2662h;

    /* renamed from: i */
    public final boolean f2663i;

    /* renamed from: j */
    public final boolean f2664j;

    /* renamed from: k */
    public final boolean f2665k;

    /* renamed from: l */
    public final boolean f2666l;

    /* renamed from: m */
    public final boolean f2667m;

    /* renamed from: n */
    public final boolean f2668n;

    /* renamed from: o */
    public final boolean f2669o;

    /* renamed from: com.yandex.metrica.impl.ob.tv$a */
    public static class C1053a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f2670a = C1054b.f2685a;
        /* access modifiers changed from: private */

        /* renamed from: B */
        public boolean f2671b = C1054b.f2686b;
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f2672c = C1054b.f2687c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public boolean f2673d = C1054b.f2688d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public boolean f2674e = C1054b.f2689e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public boolean f2675f = C1054b.f2690f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public boolean f2676g = C1054b.f2691g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public boolean f2677h = C1054b.f2692h;
        /* access modifiers changed from: private */

        /* renamed from: i */
        public boolean f2678i = C1054b.f2693i;
        /* access modifiers changed from: private */

        /* renamed from: j */
        public boolean f2679j = C1054b.f2694j;
        /* access modifiers changed from: private */

        /* renamed from: k */
        public boolean f2680k = C1054b.f2695k;
        /* access modifiers changed from: private */

        /* renamed from: l */
        public boolean f2681l = C1054b.f2699o;
        /* access modifiers changed from: private */

        /* renamed from: m */
        public boolean f2682m = C1054b.f2696l;
        /* access modifiers changed from: private */

        /* renamed from: n */
        public boolean f2683n = C1054b.f2697m;
        /* access modifiers changed from: private */

        /* renamed from: o */
        public boolean f2684o = C1054b.f2698n;

        /* renamed from: a */
        public C1053a mo2304a(boolean z) {
            this.f2670a = z;
            return this;
        }

        /* renamed from: B */
        public C1053a mo2306b(boolean z) {
            this.f2671b = z;
            return this;
        }

        /* renamed from: a */
        public C1053a mo2307c(boolean z) {
            this.f2672c = z;
            return this;
        }

        /* renamed from: d */
        public C1053a mo2308d(boolean z) {
            this.f2673d = z;
            return this;
        }

        /* renamed from: e */
        public C1053a mo2309e(boolean z) {
            this.f2674e = z;
            return this;
        }

        /* renamed from: f */
        public C1053a mo2310f(boolean z) {
            this.f2675f = z;
            return this;
        }

        /* renamed from: g */
        public C1053a mo2311g(boolean z) {
            this.f2676g = z;
            return this;
        }

        /* renamed from: h */
        public C1053a mo2312h(boolean z) {
            this.f2677h = z;
            return this;
        }

        /* renamed from: i */
        public C1053a mo2313i(boolean z) {
            this.f2678i = z;
            return this;
        }

        /* renamed from: j */
        public C1053a mo2314j(boolean z) {
            this.f2679j = z;
            return this;
        }

        /* renamed from: k */
        public C1053a mo2315k(boolean z) {
            this.f2680k = z;
            return this;
        }

        /* renamed from: l */
        public C1053a mo2316l(boolean z) {
            this.f2682m = z;
            return this;
        }

        /* renamed from: m */
        public C1053a mo2317m(boolean z) {
            this.f2683n = z;
            return this;
        }

        /* renamed from: n */
        public C1053a mo2318n(boolean z) {
            this.f2684o = z;
            return this;
        }

        /* renamed from: o */
        public C1053a mo2319o(boolean z) {
            this.f2681l = z;
            return this;
        }

        /* renamed from: a */
        public tv mo2305a() {
            return new tv(this);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.tv$B */
    public static final class C1054b {

        /* renamed from: p */
        private static final C0871c f2700p = new C0871c();
        /* renamed from: a */
        public static final boolean f2685a = f2700p.f2156b;

        /* renamed from: B */
        public static final boolean f2686b = f2700p.f2157c;

        /* renamed from: a */
        public static final boolean f2687c = f2700p.f2158d;

        /* renamed from: d */
        public static final boolean f2688d = f2700p.f2159e;

        /* renamed from: e */
        public static final boolean f2689e = f2700p.f2169o;

        /* renamed from: f */
        public static final boolean f2690f = f2700p.f2171q;

        /* renamed from: g */
        public static final boolean f2691g = f2700p.f2160f;

        /* renamed from: h */
        public static final boolean f2692h = f2700p.f2161g;

        /* renamed from: i */
        public static final boolean f2693i = f2700p.f2162h;

        /* renamed from: j */
        public static final boolean f2694j = f2700p.f2163i;

        /* renamed from: k */
        public static final boolean f2695k = f2700p.f2164j;

        /* renamed from: l */
        public static final boolean f2696l = f2700p.f2165k;

        /* renamed from: m */
        public static final boolean f2697m = f2700p.f2166l;

        /* renamed from: n */
        public static final boolean f2698n = f2700p.f2167m;

        /* renamed from: o */
        public static final boolean f2699o = f2700p.f2168n;
    }

    public tv(@NonNull C1053a aVar) {
        this.f2655a = aVar.f2670a;
        this.f2656b = aVar.f2671b;
        this.f2657c = aVar.f2672c;
        this.f2658d = aVar.f2673d;
        this.f2659e = aVar.f2674e;
        this.f2660f = aVar.f2675f;
        this.f2661g = aVar.f2676g;
        this.f2662h = aVar.f2677h;
        this.f2663i = aVar.f2678i;
        this.f2664j = aVar.f2679j;
        this.f2665k = aVar.f2680k;
        this.f2666l = aVar.f2681l;
        this.f2667m = aVar.f2682m;
        this.f2668n = aVar.f2683n;
        this.f2669o = aVar.f2684o;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        tv tvVar = (tv) o;
        if (this.f2655a != tvVar.f2655a || this.f2656b != tvVar.f2656b || this.f2657c != tvVar.f2657c || this.f2658d != tvVar.f2658d || this.f2659e != tvVar.f2659e || this.f2660f != tvVar.f2660f || this.f2661g != tvVar.f2661g || this.f2662h != tvVar.f2662h || this.f2663i != tvVar.f2663i || this.f2664j != tvVar.f2664j || this.f2665k != tvVar.f2665k || this.f2666l != tvVar.f2666l || this.f2667m != tvVar.f2667m || this.f2668n != tvVar.f2668n) {
            return false;
        }
        if (this.f2669o != tvVar.f2669o) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14 = 1;
        int i15 = (this.f2655a ? 1 : 0) * 31;
        if (this.f2656b) {
            i = 1;
        } else {
            i = 0;
        }
        int i16 = (i + i15) * 31;
        if (this.f2657c) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        int i17 = (i2 + i16) * 31;
        if (this.f2658d) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        int i18 = (i3 + i17) * 31;
        if (this.f2659e) {
            i4 = 1;
        } else {
            i4 = 0;
        }
        int i19 = (i4 + i18) * 31;
        if (this.f2660f) {
            i5 = 1;
        } else {
            i5 = 0;
        }
        int i20 = (i5 + i19) * 31;
        if (this.f2661g) {
            i6 = 1;
        } else {
            i6 = 0;
        }
        int i21 = (i6 + i20) * 31;
        if (this.f2662h) {
            i7 = 1;
        } else {
            i7 = 0;
        }
        int i22 = (i7 + i21) * 31;
        if (this.f2663i) {
            i8 = 1;
        } else {
            i8 = 0;
        }
        int i23 = (i8 + i22) * 31;
        if (this.f2664j) {
            i9 = 1;
        } else {
            i9 = 0;
        }
        int i24 = (i9 + i23) * 31;
        if (this.f2665k) {
            i10 = 1;
        } else {
            i10 = 0;
        }
        int i25 = (i10 + i24) * 31;
        if (this.f2666l) {
            i11 = 1;
        } else {
            i11 = 0;
        }
        int i26 = (i11 + i25) * 31;
        if (this.f2667m) {
            i12 = 1;
        } else {
            i12 = 0;
        }
        int i27 = (i12 + i26) * 31;
        if (this.f2668n) {
            i13 = 1;
        } else {
            i13 = 0;
        }
        int i28 = (i13 + i27) * 31;
        if (!this.f2669o) {
            i14 = 0;
        }
        return i28 + i14;
    }

    public String toString() {
        return "CollectingFlags{easyCollectingEnabled=" + this.f2655a + ", packageInfoCollectingEnabled=" + this.f2656b + ", permissionsCollectingEnabled=" + this.f2657c + ", featuresCollectingEnabled=" + this.f2658d + ", sdkFingerprintingCollectingEnabled=" + this.f2659e + ", bleCollectingEnabled=" + this.f2660f + ", androidId=" + this.f2661g + ", googleAid=" + this.f2662h + ", wifiAround=" + this.f2663i + ", wifiConnected=" + this.f2664j + ", ownMacs=" + this.f2665k + ", accessPoint=" + this.f2666l + ", cellsAround=" + this.f2667m + ", simInfo=" + this.f2668n + ", simImei=" + this.f2669o + '}';
    }
}
