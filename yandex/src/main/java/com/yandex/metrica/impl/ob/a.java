package com.yandex.metrica.impl.ob;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.a */
public final class a {

    /* renamed from: a */
    private final byte[] a;

    /* renamed from: B */
    private int b;

    /* renamed from: a */
    private int c;

    /* renamed from: d */
    private int d;

    /* renamed from: e */
    private int e;

    /* renamed from: f */
    private int f;

    /* renamed from: g */
    private int g = Integer.MAX_VALUE;

    /* renamed from: h */
    private int h;

    /* renamed from: i */
    private int i = 64;

    /* renamed from: j */
    private int j = 67108864;

    /* renamed from: a */
    public static com.yandex.metrica.impl.ob.a a(byte[] bArr, int i, int i2) {
        return new a(bArr, i, i2);
    }

    /* renamed from: a */
    public int mo213a() throws IOException {
        if (mo239s()) {
            this.f = 0;
            return 0;
        }
        this.f = mo234n();
        if (this.f != 0) {
            return this.f;
        }
        throw com.yandex.metrica.impl.ob.d.d();
    }

    /* renamed from: a */
    public void mo214a(int i) throws com.yandex.metrica.impl.ob.d {
        if (this.f != i) {
            throw com.yandex.metrica.impl.ob.d.e();
        }
    }

    /* renamed from: B */
    public boolean mo217b(int i) throws IOException {
        switch (com.yandex.metrica.impl.ob.g.a(i)) {
            case 0:
                mo225g();
                return true;
            case 1:
                mo237q();
                return true;
            case 2:
                mo227h(mo234n());
                return true;
            case 3:
                mo216b();
                mo214a(com.yandex.metrica.impl.ob.g.a(com.yandex.metrica.impl.ob.g.b(i), 4));
                return true;
            case 4:
                return false;
            case 5:
                mo236p();
                return true;
            default:
                throw com.yandex.metrica.impl.ob.d.f();
        }
    }

    /* renamed from: B */
    public void mo216b() throws IOException {
        int a;
        do {
            a = mo213a();
            if (a == 0) {
                return;
            }
        } while (mo217b(a));
    }

    /* renamed from: a */
    public double mo218c() throws IOException {
        return Double.longBitsToDouble(mo237q());
    }

    /* renamed from: d */
    public float mo219d() throws IOException {
        return Float.intBitsToFloat(mo236p());
    }

    /* renamed from: e */
    public long mo221e() throws IOException {
        return mo235o();
    }

    /* renamed from: f */
    public long mo223f() throws IOException {
        return mo235o();
    }

    /* renamed from: g */
    public int mo225g() throws IOException {
        return mo234n();
    }

    /* renamed from: h */
    public boolean mo228h() throws IOException {
        return mo234n() != 0;
    }

    /* renamed from: i */
    public String mo229i() throws IOException {
        int n = mo234n();
        if (n > this.c - this.e || n <= 0) {
            return new String(mo226g(n), "UTF-8");
        }
        String str = new String(this.a, this.e, n, "UTF-8");
        this.e = n + this.e;
        return str;
    }

    /* renamed from: a */
    public void mo215a(com.yandex.metrica.impl.ob.e eVar) throws IOException {
        int n = mo234n();
        if (this.h >= this.i) {
            throw com.yandex.metrica.impl.ob.d.g();
        }
        int d = d(n);
        this.h++;
        eVar.mo738a(this);
        mo214a(0);
        this.h--;
        mo222e(d);
    }

    /* renamed from: j */
    public byte[] mo230j() throws IOException {
        int n = mo234n();
        if (n > this.c - this.e || n <= 0) {
            return mo226g(n);
        }
        byte[] bArr = new byte[n];
        System.arraycopy(this.a, this.e, bArr, 0, n);
        this.e = n + this.e;
        return bArr;
    }

    /* renamed from: k */
    public int mo231k() throws IOException {
        return mo234n();
    }

    /* renamed from: l */
    public int mo232l() throws IOException {
        return m211c(mo234n());
    }

    /* renamed from: m */
    public long mo233m() throws IOException {
        return m209a(mo235o());
    }

    /* renamed from: n */
    public int mo234n() throws IOException {
        byte var1 = this.u();
        if (var1 >= 0) {
            return var1;
        } else {
            int var2 = var1 & 127;
            if ((var1 = this.u()) >= 0) {
                var2 |= var1 << 7;
            } else {
                var2 |= (var1 & 127) << 7;
                if ((var1 = this.u()) >= 0) {
                    var2 |= var1 << 14;
                } else {
                    var2 |= (var1 & 127) << 14;
                    if ((var1 = this.u()) >= 0) {
                        var2 |= var1 << 21;
                    } else {
                        var2 |= (var1 & 127) << 21;
                        var2 |= (var1 = this.u()) << 28;
                        if (var1 < 0) {
                            for(int var3 = 0; var3 < 5; ++var3) {
                                if (this.u() >= 0) {
                                    return var2;
                                }
                            }

                            throw com.yandex.metrica.impl.ob.d.c();
                        }
                    }
                }
            }

            return var2;
        }
    }

    /* renamed from: o */
    public long mo235o() throws IOException {
        long j = 0;
        for (int i = 0; i < 64; i += 7) {
            byte u = u();
            j |= ((long) (u & Byte.MAX_VALUE)) << i;
            if ((u & 128) == 0) {
                return j;
            }
        }
        throw com.yandex.metrica.impl.ob.d.c();
    }

    /* renamed from: p */
    public int mo236p() throws IOException {
        return (u() & 255) | ((u() & 255) << 8) | ((u() & 255) << 16) | ((u() & 255) << 24);
    }

    /* renamed from: q */
    public long mo237q() throws IOException {
        byte u = u();
        byte u2 = u();
        return ((((long) u2) & 255) << 8) | (((long) u) & 255) | ((((long) u()) & 255) << 16) | ((((long) u()) & 255) << 24) | ((((long) u()) & 255) << 32) | ((((long) u()) & 255) << 40) | ((((long) u()) & 255) << 48) | ((((long) u()) & 255) << 56);
    }

    /* renamed from: a */
    public static int m211c(int i) {
        return (i >>> 1) ^ (-(i & 1));
    }

    /* renamed from: a */
    public static long m209a(long j) {
        return (j >>> 1) ^ (-(1 & j));
    }

    private a(byte[] bArr, int i, int i2) {
        this.a = bArr;
        this.b = i;
        this.c = i + i2;
        this.e = i;
    }

    /* renamed from: d */
    public int d(int i) throws com.yandex.metrica.impl.ob.d {
        if (i < 0) {
            throw com.yandex.metrica.impl.ob.d.b();
        }
        int i2 = this.e + i;
        int i3 = this.g;
        if (i2 > i3) {
            throw com.yandex.metrica.impl.ob.d.a();
        }
        this.g = i2;
        m212v();
        return i3;
    }

    /* renamed from: v */
    private void m212v() {
        this.c += this.d;
        int i = this.c;
        if (i > this.g) {
            this.d = i - this.g;
            this.c -= this.d;
            return;
        }
        this.d = 0;
    }

    /* renamed from: e */
    public void mo222e(int i) {
        this.g = i;
        m212v();
    }

    /* renamed from: r */
    public int mo238r() {
        if (this.g == Integer.MAX_VALUE) {
            return -1;
        }
        return this.g - this.e;
    }

    /* renamed from: s */
    public boolean mo239s() {
        return this.e == this.c;
    }

    /* renamed from: t */
    public int mo240t() {
        return this.e - this.b;
    }

    /* renamed from: f */
    public void mo224f(int i) {
        if (i > this.e - this.b) {
            throw new IllegalArgumentException("Position " + i + " is beyond current " + (this.e - this.b));
        } else if (i < 0) {
            throw new IllegalArgumentException("Bad position " + i);
        } else {
            this.e = this.b + i;
        }
    }

    /* renamed from: u */
    public byte u() throws IOException {
        if (this.e == this.c) {
            throw com.yandex.metrica.impl.ob.d.a();
        }
        byte[] bArr = this.a;
        int i = this.e;
        this.e = i + 1;
        return bArr[i];
    }

    /* renamed from: g */
    public byte[] mo226g(int i) throws IOException {
        if (i < 0) {
            throw com.yandex.metrica.impl.ob.d.b();
        } else if (this.e + i > this.g) {
            mo227h(this.g - this.e);
            throw com.yandex.metrica.impl.ob.d.a();
        } else if (i <= this.c - this.e) {
            byte[] bArr = new byte[i];
            System.arraycopy(this.a, this.e, bArr, 0, i);
            this.e += i;
            return bArr;
        } else {
            throw com.yandex.metrica.impl.ob.d.a();
        }
    }

    /* renamed from: h */
    public void mo227h(int i) throws IOException {
        if (i < 0) {
            throw com.yandex.metrica.impl.ob.d.b();
        } else if (this.e + i > this.g) {
            mo227h(this.g - this.e);
            throw com.yandex.metrica.impl.ob.d.a();
        } else if (i <= this.c - this.e) {
            this.e += i;
        } else {
            throw com.yandex.metrica.impl.ob.d.a();
        }
    }
}
