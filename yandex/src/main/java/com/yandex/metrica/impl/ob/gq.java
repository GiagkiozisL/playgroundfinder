package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.LinkedList;

/* renamed from: com.yandex.metrica.impl.ob.gq */
public class gq extends gr<hd> {

    /* renamed from: a */
    private final hn f986a;

    /* renamed from: B */
    private final im f987b;

    /* renamed from: a */
    private final hj f988c;

    public gq(en enVar) {
        this.f986a = new hn(enVar);
        this.f987b = new im(enVar);
        this.f988c = new hj(enVar);
    }

    /* renamed from: a */
    public go<hd> mo955a(int i) {
        LinkedList linkedList = new LinkedList();
        switch (C0058a.m298a(i)) {
            case EVENT_TYPE_START:
                linkedList.add(this.f987b);
                linkedList.add(this.f986a);
                break;
            case EVENT_TYPE_INIT:
                linkedList.add(this.f986a);
                break;
            case EVENT_TYPE_UPDATE_FOREGROUND_TIME:
                linkedList.add(this.f988c);
                break;
        }
        return new gn(linkedList);
    }
}
