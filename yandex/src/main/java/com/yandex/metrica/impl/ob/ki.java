package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.c;

/* renamed from: com.yandex.metrica.impl.ob.ki */
public abstract class ki {

    /* renamed from: a */
    private final C0534a f1207a;
    @Nullable

    /* renamed from: B */
    private final c f1208b;

    /* renamed from: com.yandex.metrica.impl.ob.ki$a */
    public interface C0534a {
        /* renamed from: a */
        boolean mo689a(Throwable th);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public abstract void mo1147b(@NonNull kl klVar);

    ki(C0534a aVar, @Nullable c cVar) {
        this.f1207a = aVar;
        this.f1208b = cVar;
    }

    /* renamed from: a */
    public void mo1146a(@NonNull kl klVar) {
        if (this.f1207a.mo689a(klVar.mo1148a())) {
            Throwable a = klVar.mo1148a();
            if (!(this.f1208b == null || a == null)) {
                a = this.f1208b.a(a);
                if (a == null) {
                    return;
                }
            }
            mo1147b(new kl(a, klVar.f1218c, klVar.f1219d, klVar.f1220e));
        }
    }
}
