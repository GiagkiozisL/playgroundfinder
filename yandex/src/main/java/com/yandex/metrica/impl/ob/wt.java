package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import androidx.annotation.VisibleForTesting;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

/* renamed from: com.yandex.metrica.impl.ob.wt */
public class wt {

    /* renamed from: a */
    private final String a;

    /* renamed from: B */
    private final String b;

    public wt() {
        this("AES/CBC/PKCS5Padding", "RSA/ECB/PKCS1Padding");
    }

    @VisibleForTesting
    wt(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @SuppressLint({"TrulyRandom"})
    /* renamed from: a */
    public byte[] a(byte[] seedBts) {
        try {
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr2 = new byte[16];
            byte[] bArr3 = new byte[16];
            secureRandom.nextBytes(bArr3);
            secureRandom.nextBytes(bArr2);
            return a(seedBts, bArr3, bArr2, KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhmH/m2qrRjxDHP794CeaZpENQNYydf8pqyXJilo6XxK+n+pvo27VxWfB3Z1yHrtKow+eZXKLQzrQ8wZMfRgADrYCQJ20y2hGZEUCN1tGSM+xqVKMeCtVi3NvQa54Cx7mT5ECVsH5DKEs/aeScDHP56FzcgEbtOSwyRZ8dsEM0wwIDAQAB", 0))));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            return null;
        }
    }

    @VisibleForTesting
    public byte[] a(byte[] seedBts, byte[] var2, byte[] var3, PublicKey var4) {

        ByteArrayOutputStream var5 = null;

        try {
            ByteArrayOutputStream var6 = new ByteArrayOutputStream(var2.length + var3.length);
            var6.write(var2);
            var6.write(var3);
            byte[] var7 = var6.toByteArray();
            var6.close();
            Cipher var8 = Cipher.getInstance(this.b);
            var8.init(1, var4);
            var5 = new ByteArrayOutputStream(seedBts.length);
            var5.write(var8.doFinal(var7));
            var5.write((new Crypto(this.a, var2, var3)).encrypt(seedBts));
            byte[] var9 = var5.toByteArray();
            return var9;
        } catch (Throwable var13) {
        } finally {
            cx.a(var5);
        }

        return null;
    }
}
