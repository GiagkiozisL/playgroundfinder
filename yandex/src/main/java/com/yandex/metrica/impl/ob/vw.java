package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.vw */
public class vw<K, V> {
    @NonNull

    /* renamed from: a */
    private final Map<K, V> f2911a;
    @NonNull

    /* renamed from: B */
    private final V f2912b;

    public vw(@NonNull V v) {
        this(new HashMap(), v);
    }

    @VisibleForTesting
    public vw(@NonNull Map<K, V> map, @NonNull V v) {
        this.f2911a = map;
        this.f2912b = v;
    }

    /* renamed from: a */
    public void mo2510a(@Nullable K k, @Nullable V v) {
        this.f2911a.put(k, v);
    }

    @NonNull
    /* renamed from: a */
    public V mo2508a(@Nullable K k) {
        V v = this.f2911a.get(k);
        return v == null ? this.f2912b : v;
    }

    @NonNull
    /* renamed from: a */
    public Set<K> mo2509a() {
        return this.f2911a.keySet();
    }
}
