package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.bk */
public class bk {
    @NonNull

    /* renamed from: a */
    private final ez f368a;

    public bk(@NonNull ez ezVar) {
        this.f368a = ezVar;
    }

    /* renamed from: a */
    public void mo435a(@NonNull String str) {
        this.f368a.a(af.m283b(str, vz.m4266h()));
    }

    /* renamed from: B */
    public void mo436b(@NonNull String str) {
        this.f368a.a(af.m275a(str, vz.m4266h()));
    }
}
