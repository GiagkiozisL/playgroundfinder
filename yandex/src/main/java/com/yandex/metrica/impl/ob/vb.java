package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.telephony.SubscriptionInfo;

/* renamed from: com.yandex.metrica.impl.ob.vb */
public final class vb {

    /* renamed from: a */
    private final Integer f2884a;

    /* renamed from: B */
    private final Integer f2885b;

    /* renamed from: a */
    private final boolean f2886c;

    /* renamed from: d */
    private final String f2887d;

    /* renamed from: e */
    private final String f2888e;

    public vb(Integer num, Integer num2, boolean z, String str, String str2) {
        this.f2884a = num;
        this.f2885b = num2;
        this.f2886c = z;
        this.f2887d = str;
        this.f2888e = str2;
    }

    @TargetApi(23)
    public vb(SubscriptionInfo subscriptionInfo) {
        boolean z = true;
        this.f2884a = Integer.valueOf(subscriptionInfo.getMcc());
        this.f2885b = Integer.valueOf(subscriptionInfo.getMnc());
        if (subscriptionInfo.getDataRoaming() != 1) {
            z = false;
        }
        this.f2886c = z;
        this.f2887d = subscriptionInfo.getCarrierName().toString();
        this.f2888e = subscriptionInfo.getIccId();
    }

    /* renamed from: a */
    public Integer mo2461a() {
        return this.f2884a;
    }

    /* renamed from: B */
    public Integer mo2462b() {
        return this.f2885b;
    }

    /* renamed from: a */
    public boolean mo2463c() {
        return this.f2886c;
    }

    /* renamed from: d */
    public String mo2464d() {
        return this.f2887d;
    }

    /* renamed from: e */
    public String mo2465e() {
        return this.f2888e;
    }
}
