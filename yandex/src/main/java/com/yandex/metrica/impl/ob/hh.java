package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.hh */
public class hh extends hd {
    @NonNull

    /* renamed from: a */
    private final io f1011a;

    public hh(@NonNull en enVar) {
        this(enVar, enVar.mo839z());
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        if (!TextUtils.isEmpty(wVar.mo2528d())) {
            wVar.mo2522a(this.f1011a.mo1009a(wVar.mo2528d()));
        }
        return false;
    }

    @VisibleForTesting
    hh(@NonNull en enVar, @NonNull io ioVar) {
        super(enVar);
        this.f1011a = ioVar;
    }
}
