package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.sy */
public abstract class sy<T extends sn> implements sw<T> {
    @Nullable

    /* renamed from: a */
    private xb f2523a;

    /* renamed from: a */
    public void mo2210a(@NonNull Builder builder, @NonNull T t) {
        if (this.f2523a != null && this.f2523a.a() == xc.AES_RSA) {
            builder.appendQueryParameter("encrypted_request", "1");
        }
    }

    /* renamed from: a */
    public String mo2211a(Boolean bool) {
        if (bool == null) {
            return "";
        }
        return String.valueOf(bool.booleanValue() ? "1" : "0");
    }

    /* renamed from: a */
    public void mo2212a(@NonNull xb xbVar) {
        this.f2523a = xbVar;
    }
}
