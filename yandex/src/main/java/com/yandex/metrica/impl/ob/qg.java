package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qg */
public class qg extends qd {

    /* renamed from: d */
    private static final qk f1823d = new qk("SERVICE_API_LEVEL");

    /* renamed from: e */
    private static final qk f1824e = new qk("CLIENT_API_LEVEL");

    /* renamed from: f */
    private qk f1825f = new qk(f1823d.mo1742a());

    /* renamed from: g */
    private qk f1826g = new qk(f1824e.mo1742a());

    public qg(Context context) {
        super(context, null);
    }

    /* renamed from: a */
    public int mo1684a() {
        return this.f1766c.getInt(this.f1825f.mo1744b(), -1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_migrationpreferences";
    }

    /* renamed from: B */
    public qg mo1727b() {
        mo1697h(this.f1825f.mo1744b());
        return this;
    }

    /* renamed from: a */
    public qg mo1728c() {
        mo1697h(this.f1826g.mo1744b());
        return this;
    }
}
