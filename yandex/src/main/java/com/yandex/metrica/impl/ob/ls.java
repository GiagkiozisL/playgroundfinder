package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.lq.C0577a.C0578a;
import com.yandex.metrica.impl.ob.lq.C0577a.C0579b;
import com.yandex.metrica.impl.ob.lq.C0583e;
import com.yandex.metrica.impl.ob.lq.C0584f;
import com.yandex.metrica.impl.ob.lq.C0585g;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.ls */
public class ls {

    /* renamed from: com.yandex.metrica.impl.ob.ls$a */
    public static class C0589a extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$a */
    public static class C0590aa extends lr {
        /* renamed from: a */
        public void mo1273a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.delete("reports", "session_id = ?", new String[]{String.valueOf(-2)});
            sQLiteDatabase.delete("sessions", "id = ?", new String[]{String.valueOf(-2)});
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$ab */
    public static class C0591ab extends lr {
        /* renamed from: a */
        public void mo1273a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "global_number", Integer.valueOf(0)}));
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "number_of_type", Integer.valueOf(0)}));
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$B */
    public static class C0592b extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$a */
    public static class C0593c extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL(C0584f.f1355b);
            sQLiteDatabase.execSQL(C0585g.f1357b);
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$d */
    public static class C0594d extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS reports");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sessions");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$e */
    public static class C0595e extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS startup (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL(C0579b.f1350a);
            sQLiteDatabase.execSQL(C0578a.f1348a);
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$f */
    public static class C0596f extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS device_id_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS api_level_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS startup");
            sQLiteDatabase.execSQL(C0579b.f1351b);
            sQLiteDatabase.execSQL(C0578a.f1349b);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$g */
    public static class C0597g extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS api_level_info (API_LEVEL INT )");
            sQLiteDatabase.insert("api_level_info", "API_LEVEL", C0583e.m2214a());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$h */
    public static class C0598h extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS api_level_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS device_id_info");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$i */
    public static class C0599i extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS startup (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$j */
    public static class C0600j extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS GeoLocationInfo");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$k */
    public static class C0601k extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS permissions (name TEXT PRIMARY KEY,granted INTEGER)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$l */
    public static class C0602l extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(C0579b.f1350a);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$m */
    public static class C0603m extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(C0578a.f1348a);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$n */
    public static class C0604n extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$o */
    public static class C0605o extends lr {
        /* JADX INFO: finally extract failed */
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            Cursor cursor = null;
            StringBuilder sb = new StringBuilder();
            String str = "sessions_BACKUP";
            sb.append("CREATE TABLE IF NOT EXISTS ").append("sessions_BACKUP").append(" (");
            sb.append("id").append(" INTEGER").append(",");
            sb.append("start_time").append(" INTEGER").append(",");
            sb.append("connection_type").append(" INTEGER").append(",");
            sb.append("network_type").append(" TEXT").append(",");
            sb.append("country_code").append(" INTEGER").append(",");
            sb.append("operator_id").append(" INTEGER").append(",");
            sb.append("lac").append(" INTEGER").append(",");
            sb.append("report_request_parameters").append(" TEXT").append(" );");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("id").append(",");
            sb2.append("start_time").append(",");
            sb2.append("connection_type").append(",");
            sb2.append("network_type").append(",");
            sb2.append("country_code").append(",");
            sb2.append("operator_id").append(",");
            sb2.append("lac").append(",");
            sb2.append("report_request_parameters");
            StringBuilder sb3 = new StringBuilder();
            sb3.append("INSERT INTO ").append("sessions_BACKUP");
            sb3.append(" SELECT ").append(sb2);
            sb3.append(" FROM ").append("sessions").append(";");
            sQLiteDatabase.execSQL(sb3.toString());
            sQLiteDatabase.execSQL("DROP TABLE sessions;");
            sQLiteDatabase.execSQL(C0585g.f1357b);
            try {
                cursor = sQLiteDatabase.rawQuery("SELECT * FROM sessions_BACKUP", null);
                while (cursor.moveToNext()) {
                    ContentValues contentValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                    ArrayList<String> arrayList = new ArrayList<>();
                    arrayList.add("id");
                    arrayList.add("start_time");
                    arrayList.add("report_request_parameters");
                    ContentValues contentValues2 = new ContentValues(contentValues);
                    for (Entry entry : contentValues.valueSet()) {
                        if (!arrayList.contains(entry.getKey())) {
                            contentValues2.remove((String) entry.getKey());
                        }
                    }
                    for (String remove : arrayList) {
                        contentValues.remove(remove);
                    }
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("conn_type", contentValues.getAsInteger("connection_type"));
                    jSONObject.putOpt("net_type", contentValues.get("network_type"));
                    jSONObject.putOpt("operator_id", contentValues.get("operator_id"));
                    jSONObject.putOpt("lac", contentValues.get("lac"));
                    jSONObject.putOpt("country_code", contentValues.get("country_code"));
                    contentValues2.put("network_info", jSONObject.toString());
                    sQLiteDatabase.insertOrThrow("sessions", null, contentValues2);
                }
                cx.m1183a(cursor);
                sQLiteDatabase.execSQL("DROP TABLE sessions_BACKUP;");
                StringBuilder sb4 = new StringBuilder();
                sb4.append("ALTER TABLE ").append("reports");
                sb4.append(" ADD COLUMN ").append("wifi_network_info");
                sb4.append(" TEXT DEFAULT ''");
                sQLiteDatabase.execSQL(sb4.toString());
                StringBuilder sb5 = new StringBuilder();
                sb5.append("ALTER TABLE ").append("reports");
                sb5.append(" ADD COLUMN ").append("cell_info");
                sb5.append(" TEXT DEFAULT ''");
                sQLiteDatabase.execSQL(sb5.toString());
                StringBuilder sb6 = new StringBuilder();
                sb6.append("ALTER TABLE ").append("reports");
                sb6.append(" ADD COLUMN ").append("location_info");
                sb6.append(" TEXT DEFAULT ''");
                sQLiteDatabase.execSQL(sb6.toString());
            } catch (Throwable th) {
                cx.m1183a(cursor);
                throw th;
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$p */
    public static class C0606p extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("reports");
            sb.append(" ADD COLUMN ").append("environment");
            sb.append(" TEXT ");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ALTER TABLE ").append("reports");
            sb2.append(" ADD COLUMN ").append("user_info");
            sb2.append(" TEXT ");
            sQLiteDatabase.execSQL(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("ALTER TABLE ").append("reports");
            sb3.append(" ADD COLUMN ").append("session_type");
            sb3.append(" INTEGER DEFAULT ").append(jh.FOREGROUND.a());
            sQLiteDatabase.execSQL(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append("UPDATE ").append("reports");
            sb4.append(" SET ").append("session_type").append(" = ");
            sb4.append(jh.BACKGROUND.a());
            sb4.append(" WHERE ").append("session_id");
            sb4.append(" = ").append(-2);
            sQLiteDatabase.execSQL(sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append("ALTER TABLE ").append("sessions");
            sb5.append(" ADD COLUMN ").append("server_time_offset");
            sb5.append(" INTEGER ");
            sQLiteDatabase.execSQL(sb5.toString());
            StringBuilder sb6 = new StringBuilder();
            sb6.append("ALTER TABLE ").append("sessions");
            sb6.append(" ADD COLUMN ").append("type");
            sb6.append(" INTEGER DEFAULT ").append(jh.FOREGROUND.a());
            sQLiteDatabase.execSQL(sb6.toString());
            StringBuilder sb7 = new StringBuilder();
            sb7.append("UPDATE ").append("sessions");
            sb7.append(" SET ").append("type").append(" = ");
            sb7.append(jh.BACKGROUND.a());
            sb7.append(" WHERE ").append("id");
            sb7.append(" = ").append(-2);
            sQLiteDatabase.execSQL(sb7.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$q */
    public static class C0607q extends lr {

        /* renamed from: a */
        private static final String f1360a = ("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT " + jh.FOREGROUND.a() + "," + "app_environment" + " TEXT DEFAULT '" + "{}" + "'," + "app_environment_revision" + " INTEGER DEFAULT " + 0 + " )");

        /* JADX INFO: finally extract failed */
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("reports");
            sb.append(" ADD COLUMN ").append("app_environment");
            sb.append(" TEXT DEFAULT '{}'");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ALTER TABLE ").append("reports");
            sb2.append(" ADD COLUMN ").append("app_environment_revision");
            sb2.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb2.toString());
            String str = "reports_backup";
            sQLiteDatabase.execSQL("ALTER TABLE reports RENAME TO reports_backup");
            sQLiteDatabase.execSQL(f1360a);
            try {
                Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM reports_backup", null);
                while (rawQuery.moveToNext()) {
                    ContentValues contentValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(rawQuery, contentValues);
                    String asString = contentValues.getAsString("environment");
                    contentValues.remove("environment");
                    contentValues.put("error_environment", asString);
                    sQLiteDatabase.insert("reports", null, contentValues);
                }
                cx.m1183a(rawQuery);
                sQLiteDatabase.execSQL("DROP TABLE reports_backup");
            } catch (Throwable th) {
                cx.m1183a((Cursor) null);
                throw th;
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$r */
    public static class C0608r extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("reports");
            sb.append(" ADD COLUMN ").append("truncated");
            sb.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$s */
    public static class C0609s extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("reports");
            sb.append(" ADD COLUMN ").append("connection_type");
            sb.append(" INTEGER DEFAULT 2");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ALTER TABLE ").append("reports");
            sb2.append(" ADD COLUMN ").append("cellular_connection_type");
            sb2.append(" TEXT ");
            sQLiteDatabase.execSQL(sb2.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$t */
    public static class C0610t extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("reports");
            sb.append(" ADD COLUMN ").append("custom_type");
            sb.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$u */
    public static class C0611u extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("reports");
            sb.append(" ADD COLUMN ").append("wifi_access_point");
            sb.append(" TEXT ");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$v */
    public static class C0612v extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("sessions");
            sb.append(" ADD COLUMN ").append("wifi_network_info");
            sb.append(" TEXT DEFAULT ''");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$w */
    public static class C0613w extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("sessions");
            sb.append(" ADD COLUMN ").append("obtained_before_first_sync");
            sb.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$x */
    public static class C0614x extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "encrypting_mode", Integer.valueOf(wz.NONE.mo2592a())}));
            sQLiteDatabase.execSQL(String.format(Locale.US, "UPDATE %s SET %s = %d where %s=%d", new Object[]{"reports", "encrypting_mode", Integer.valueOf(wz.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER.mo2592a()), "type", Integer.valueOf(C0058a.EVENT_TYPE_IDENTITY.mo259a())}));
            sQLiteDatabase.execSQL("ALTER TABLE reports ADD COLUMN profile_id TEXT ");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$y */
    public static class C0615y extends lr {
        /* renamed from: a */
        public void mo1273a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "first_occurrence_status", Integer.valueOf(aj.UNKNOWN.f225d)}));
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ls$z */
    public static class C0616z extends lr {
        /* renamed from: a */
        public void mo1273a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ").append("sessions");
            sb.append(" ADD COLUMN ").append("report_request_parameters");
            sb.append(" TEXT DEFAULT ''");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }
}
