package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.rl.a.Aa;

/* renamed from: com.yandex.metrica.impl.ob.ra */
public class ra extends qo {
    public ra(@NonNull qn qnVar) {
        super(qnVar);
    }

    /* renamed from: a */
    public Aa mo1748a(@NonNull re reVar, @Nullable Aa aVar, @NonNull qm qmVar) {
        if (!mo1749a(aVar)) {
            return null;
        }
        Aa a = qmVar.a();
        a.f2048d.f2050b = true;
        return mo1747a().a(reVar, a);
    }
}
