package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.rl.a.Aa;
import com.yandex.metrica.impl.ob.rl.a.Bb;

/* renamed from: com.yandex.metrica.impl.ob.qq */
public class qq extends qo {
    public qq(@NonNull qn qnVar) {
        super(qnVar);
    }

    /* renamed from: a */
    public Aa mo1748a(@NonNull re reVar, @Nullable Aa aVar, @NonNull qm qmVar) {
        if (mo1749a(aVar)) {
            return mo1747a().a(reVar, qmVar.a());
        }
        aVar.f2048d = new Bb();
        return aVar;
    }
}
