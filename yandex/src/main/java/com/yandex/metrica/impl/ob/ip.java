package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.HashSet;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.ip */
public class ip {

    /* renamed from: a */
    private boolean f1054a;
    @NonNull

    /* renamed from: B */
    private Set<Integer> f1055b;

    /* renamed from: a */
    private int f1056c;

    /* renamed from: d */
    private int f1057d;

    public ip() {
        this(false, 0, 0, (Set<Integer>) new HashSet<Integer>());
    }

    public ip(boolean z, int i, int i2, @NonNull int[] iArr) {
        this(z, i, i2, cx.m1182a(iArr));
    }

    public ip(boolean z, int i, int i2, @NonNull Set<Integer> set) {
        this.f1054a = z;
        this.f1055b = set;
        this.f1056c = i;
        this.f1057d = i2;
    }

    /* renamed from: a */
    public void mo1011a() {
        this.f1055b = new HashSet();
        this.f1057d = 0;
    }

    /* renamed from: B */
    public boolean mo1015b() {
        return this.f1054a;
    }

    /* renamed from: a */
    public void mo1013a(boolean z) {
        this.f1054a = z;
    }

    @NonNull
    /* renamed from: a */
    public Set<Integer> mo1016c() {
        return this.f1055b;
    }

    /* renamed from: d */
    public int mo1017d() {
        return this.f1057d;
    }

    /* renamed from: e */
    public int mo1018e() {
        return this.f1056c;
    }

    /* renamed from: a */
    public void mo1012a(int i) {
        this.f1056c = i;
        this.f1057d = 0;
    }

    /* renamed from: B */
    public void mo1014b(int i) {
        this.f1055b.add(Integer.valueOf(i));
        this.f1057d++;
    }
}
