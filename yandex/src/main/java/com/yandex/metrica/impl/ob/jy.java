package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.jy */
public abstract class jy {
    @NonNull

    /* renamed from: a */
    private final Context f1187a;

    /* renamed from: a */
    public abstract void mo1136a(@Nullable Bundle bundle, @Nullable jw jwVar);

    public jy(@NonNull Context context) {
        this.f1187a = context;
    }

    @NonNull
    /* renamed from: a */
    public Context mo1138a() {
        return this.f1187a;
    }
}
