package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener.Error;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.bx */
public class bx {

    /* renamed from: a */
    private final boolean f452a;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public final cd f453b;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final lv f454c;

    /* renamed from: d */
    private String f455d;

    /* renamed from: e */
    private Map<String, String> f456e;

    /* renamed from: f */
    private DeferredDeeplinkParametersListener f457f;

    public bx(cd cdVar, lv lvVar, @NonNull xh xhVar) {
        this(cdVar, lvVar, new sl(cdVar.b()), xhVar);
    }

    @VisibleForTesting
    bx(cd cdVar, lv lvVar, @NonNull final sl slVar, @NonNull xh xhVar) {
        this.f453b = cdVar;
        this.f454c = lvVar;
        this.f455d = lvVar.mo1294c();
        this.f452a = lvVar.mo1298d();
        if (this.f452a) {
            this.f454c.mo1313n(null);
            this.f455d = null;
        } else {
            m811e(mo528b(this.f455d));
        }
        if (!this.f454c.mo1301e()) {
            xhVar.a((Runnable) new Runnable() {
                public void run() {
                    slVar.mo2072a(new sk() {
                        /* renamed from: a */
                        public void a(@NonNull sj sjVar) {
                            bx.this.f453b.mo570a(sjVar);
                            bx.this.m810d(sjVar.installReferrer);
                            m819a();
                        }

                        /* renamed from: a */
                        public void a(@NonNull Throwable th) {
                            bx.this.f453b.mo570a((sj) null);
                            m819a();
                        }

                        /* renamed from: a */
                        private void m819a() {
                            bx.this.f454c.mo1305g();
                        }
                    });
                }
            });
        }
    }

    /* renamed from: a */
    public void mo527a(String str) {
        this.f453b.mo574a(str);
        m810d(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m810d(@Nullable String str) {
        if (!(this.f452a || TextUtils.isEmpty(str)) && TextUtils.isEmpty(this.f455d)) {
            synchronized (this) {
                this.f455d = str;
                this.f454c.mo1313n(this.f455d);
                m811e(mo528b(str));
                m806a();
            }
        }
    }

    /* renamed from: e */
    private void m811e(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f456e = mo529c(str);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: B */
    public final String mo528b(String str) {
        return (String) m812f(str).get("appmetrica_deep_link");
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public final Map<String, String> mo529c(String var1) {
        Map var2 = m812f(Uri.decode(var1));
        HashMap var3 = new HashMap(var2.size());
        Iterator var4 = var2.entrySet().iterator();

        while(var4.hasNext()) {
            Entry var5 = (Entry)var4.next();
            var3.put(Uri.decode((String)var5.getKey()), Uri.decode((String)var5.getValue()));
        }

        return var3;
    }

    /* renamed from: f */
    private static Map<String, String> m812f(String str) {
        String[] split;
        HashMap hashMap = new HashMap();
        if (str != null) {
            String g = m813g(str);
            if (m814h(g)) {
                for (String str2 : g.split("&")) {
                    int indexOf = str2.indexOf("=");
                    if (indexOf >= 0) {
                        hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
                    } else {
                        hashMap.put(str2, "");
                    }
                }
            }
        }
        return hashMap;
    }

    /* renamed from: g */
    private static String m813g(String str) {
        int lastIndexOf = str.lastIndexOf(63);
        if (lastIndexOf >= 0) {
            return str.substring(lastIndexOf + 1);
        }
        return str;
    }

    /* renamed from: h */
    private static boolean m814h(String str) {
        return str.contains("=");
    }

    /* renamed from: a */
    private void m806a() {
        if (cx.m1193a((Map) this.f456e)) {
            if (this.f455d != null) {
                m807a(Error.PARSE_ERROR);
            }
        } else if (this.f457f != null) {
            this.f457f.onParametersLoaded(this.f456e);
            this.f457f = null;
        }
    }

    /* renamed from: a */
    private void m807a(Error error) {
        if (this.f457f != null) {
            this.f457f.onError(error, this.f455d);
            this.f457f = null;
        }
    }

    /* renamed from: a */
    public synchronized void mo526a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        try {
            this.f457f = deferredDeeplinkParametersListener;
            if (this.f452a) {
                m807a(Error.NOT_A_FIRST_LAUNCH);
            } else {
                m806a();
            }
            this.f454c.mo1302f();
        } catch (Throwable th) {
            this.f454c.mo1302f();
            throw th;
        }
    }
}
