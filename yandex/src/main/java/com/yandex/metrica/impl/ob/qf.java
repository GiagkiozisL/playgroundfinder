package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.i.a;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qf */
public class qf extends qd {
    @Deprecated

    /* renamed from: d */
    public static final qk f1789d = new qk("APP_ENVIRONMENT");
    @Deprecated

    /* renamed from: e */
    public static final qk f1790e = new qk("APP_ENVIRONMENT_REVISION");

    /* renamed from: f */
    private static final qk f1791f = new qk("SESSION_SLEEP_START_");

    /* renamed from: g */
    private static final qk f1792g = new qk("SESSION_ID_");

    /* renamed from: h */
    private static final qk f1793h = new qk("SESSION_COUNTER_ID_");

    /* renamed from: i */
    private static final qk f1794i = new qk("SESSION_INIT_TIME_");

    /* renamed from: j */
    private static final qk f1795j = new qk("SESSION_ALIVE_TIME_");

    /* renamed from: k */
    private static final qk f1796k = new qk("SESSION_IS_ALIVE_REPORT_NEEDED_");

    /* renamed from: l */
    private static final qk f1797l = new qk("BG_SESSION_ID_");

    /* renamed from: m */
    private static final qk f1798m = new qk("BG_SESSION_SLEEP_START_");

    /* renamed from: n */
    private static final qk f1799n = new qk("BG_SESSION_COUNTER_ID_");

    /* renamed from: o */
    private static final qk f1800o = new qk("BG_SESSION_INIT_TIME_");

    /* renamed from: p */
    private static final qk f1801p = new qk("COLLECT_INSTALLED_APPS_");

    /* renamed from: q */
    private static final qk f1802q = new qk("IDENTITY_SEND_TIME_");

    /* renamed from: r */
    private static final qk f1803r = new qk("USER_INFO_");

    /* renamed from: s */
    private static final qk f1804s = new qk("REFERRER_");

    /* renamed from: t */
    private static final qk f1805t = new qk("APP_ENVIRONMENT_");

    /* renamed from: u */
    private static final qk f1806u = new qk("APP_ENVIRONMENT_REVISION_");

    /* renamed from: A */
    private qk f1807A = new qk(f1796k.mo1742a(), mo1698i());

    /* renamed from: B */
    private qk f1808B = new qk(f1797l.mo1742a(), mo1698i());

    /* renamed from: C */
    private qk f1809C = new qk(f1798m.mo1742a(), mo1698i());

    /* renamed from: D */
    private qk f1810D = new qk(f1799n.mo1742a(), mo1698i());

    /* renamed from: E */
    private qk f1811E = new qk(f1800o.mo1742a(), mo1698i());

    /* renamed from: F */
    private qk f1812F = new qk(f1802q.mo1742a(), mo1698i());

    /* renamed from: G */
    private qk f1813G = new qk(f1801p.mo1742a(), mo1698i());

    /* renamed from: H */
    private qk f1814H = new qk(f1803r.mo1742a(), mo1698i());

    /* renamed from: I */
    private qk f1815I = new qk(f1804s.mo1742a(), mo1698i());

    /* renamed from: J */
    private qk f1816J = new qk(f1805t.mo1742a(), mo1698i());

    /* renamed from: K */
    private qk f1817K = new qk(f1806u.mo1742a(), mo1698i());

    /* renamed from: v */
    private qk f1818v = new qk(f1791f.mo1742a(), mo1698i());

    /* renamed from: w */
    private qk f1819w = new qk(f1792g.mo1742a(), mo1698i());

    /* renamed from: x */
    private qk f1820x = new qk(f1793h.mo1742a(), mo1698i());

    /* renamed from: y */
    private qk f1821y = new qk(f1794i.mo1742a(), mo1698i());

    /* renamed from: z */
    private qk f1822z = new qk(f1795j.mo1742a(), mo1698i());

    public qf(Context context, String str) {
        super(context, str);
        m2920a(-1);
        m2921b(0);
        m2922c(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_boundentrypreferences";
    }

    /* renamed from: a */
    public long mo1709a(long j) {
        return m2919a(this.f1821y.mo1744b(), j);
    }

    /* renamed from: B */
    public long mo1713b(long j) {
        return m2919a(this.f1811E.mo1744b(), j);
    }

    /* renamed from: a */
    public long mo1716c(long j) {
        return m2919a(this.f1812F.mo1744b(), j);
    }

    /* renamed from: d */
    public long mo1718d(long j) {
        return m2919a(this.f1819w.mo1744b(), j);
    }

    /* renamed from: e */
    public long mo1720e(long j) {
        return m2919a(this.f1808B.mo1744b(), j);
    }

    /* renamed from: f */
    public long mo1722f(long j) {
        return m2919a(this.f1820x.mo1744b(), j);
    }

    /* renamed from: a */
    private long m2919a(String str, long j) {
        return this.f1766c.getLong(str, j);
    }

    /* renamed from: g */
    public long mo1723g(long j) {
        return m2919a(this.f1810D.mo1744b(), j);
    }

    @Nullable
    /* renamed from: a */
    public a mo1684a() {
        a aVar;
        synchronized (this) {
            if (!this.f1766c.contains(this.f1816J.mo1744b()) || !this.f1766c.contains(this.f1817K.mo1744b())) {
                aVar = null;
            } else {
                aVar = new a(this.f1766c.getString(this.f1816J.mo1744b(), "{}"), this.f1766c.getLong(this.f1817K.mo1744b(), 0));
            }
        }
        return aVar;
    }

    /* renamed from: h */
    public long mo1725h(long j) {
        return m2919a(this.f1818v.mo1744b(), j);
    }

    /* renamed from: i */
    public long mo1726i(long j) {
        return m2919a(this.f1809C.mo1744b(), j);
    }

    /* renamed from: a */
    public Boolean mo1711a(boolean z) {
        return Boolean.valueOf(this.f1766c.getBoolean(this.f1807A.mo1744b(), z));
    }

    /* renamed from: B */
    public Boolean mo1714b() {
        switch (this.f1766c.getInt(this.f1813G.mo1744b(), -1)) {
            case 0:
                return Boolean.valueOf(false);
            case 1:
                return Boolean.valueOf(true);
            default:
                return null;
        }
    }

    /* renamed from: a */
    public String mo1712a(String str) {
        return this.f1766c.getString(this.f1814H.mo1744b(), str);
    }

    /* renamed from: B */
    public String mo1715b(String str) {
        return this.f1766c.getString(this.f1815I.mo1744b(), str);
    }

    /* renamed from: a */
    public qf mo1710a(a aVar) {
        synchronized (this) {
            mo1695a(this.f1816J.mo1744b(), aVar.a);
            mo1695a(this.f1817K.mo1744b(), Long.valueOf(aVar.f1031b));
        }
        return this;
    }

    /* renamed from: a */
    public qf mo1717c() {
        return (qf) mo1697h(this.f1815I.mo1744b());
    }

    /* renamed from: a */
    private void m2920a(int i) {
        ql.m2971a(this.f1766c, this.f1822z.mo1744b(), i);
    }

    /* renamed from: B */
    private void m2921b(int i) {
        ql.m2971a(this.f1766c, this.f1818v.mo1744b(), i);
    }

    /* renamed from: a */
    private void m2922c(int i) {
        ql.m2971a(this.f1766c, this.f1820x.mo1744b(), i);
    }

    /* renamed from: d */
    public qf mo1719d() {
        if (this.f1766c.contains(this.f1813G.mo1744b())) {
            return (qf) mo1697h(this.f1813G.mo1744b());
        }
        return this;
    }

    /* renamed from: e */
    public boolean mo1721e() {
        if (this.f1766c.contains(this.f1821y.mo1744b()) || this.f1766c.contains(this.f1822z.mo1744b()) || this.f1766c.contains(this.f1807A.mo1744b()) || this.f1766c.contains(this.f1818v.mo1744b()) || this.f1766c.contains(this.f1819w.mo1744b()) || this.f1766c.contains(this.f1820x.mo1744b()) || this.f1766c.contains(this.f1811E.mo1744b()) || this.f1766c.contains(this.f1809C.mo1744b()) || this.f1766c.contains(this.f1808B.mo1744b()) || this.f1766c.contains(this.f1810D.mo1744b()) || this.f1766c.contains(this.f1816J.mo1744b()) || this.f1766c.contains(this.f1814H.mo1744b()) || this.f1766c.contains(this.f1815I.mo1744b()) || this.f1766c.contains(this.f1812F.mo1744b())) {
            return true;
        }
        return false;
    }

    /* renamed from: g */
    public void mo1724g() {
        this.f1766c.edit().remove(this.f1811E.mo1744b()).remove(this.f1810D.mo1744b()).remove(this.f1808B.mo1744b()).remove(this.f1809C.mo1744b()).remove(this.f1821y.mo1744b()).remove(this.f1820x.mo1744b()).remove(this.f1819w.mo1744b()).remove(this.f1818v.mo1744b()).remove(this.f1807A.mo1744b()).remove(this.f1822z.mo1744b()).remove(this.f1814H.mo1744b()).remove(this.f1816J.mo1744b()).remove(this.f1817K.mo1744b()).remove(this.f1815I.mo1744b()).remove(this.f1812F.mo1744b()).apply();
    }
}
