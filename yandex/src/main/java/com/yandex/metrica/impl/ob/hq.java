package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.hq */
public class hq extends hd {

    /* renamed from: a */
    private final gs<hd, en> f1019a;

    public hq(en enVar) {
        super(enVar);
        this.f1019a = new gs<>(new gq(enVar), enVar);
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        return this.f1019a.mo959b(wVar);
    }
}
