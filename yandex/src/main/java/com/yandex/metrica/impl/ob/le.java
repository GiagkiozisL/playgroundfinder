package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.lq.C0580b;
import com.yandex.metrica.impl.ob.lq.C0582d;
import com.yandex.metrica.impl.ob.lq.C0584f;
import com.yandex.metrica.impl.ob.lq.C0585g;

import java.util.HashMap;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.le */
public class le {
    @NonNull

    /* renamed from: a */
    private final HashMap<String, List<String>> f1310a = new HashMap<>();

    public le() {
        this.f1310a.put("reports", C0584f.f1354a);
        this.f1310a.put("sessions", C0585g.f1356a);
        this.f1310a.put("preferences", C0582d.f1353a);
        this.f1310a.put("binary_data", C0580b.f1352a);
    }

    @NonNull
    /* renamed from: a */
    public HashMap<String, List<String>> mo1244a() {
        return this.f1310a;
    }
}
