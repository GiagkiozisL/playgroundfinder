package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.sm */
public interface sm<I, O> {
    /* renamed from: a */
    boolean a(@NonNull I i);

    @NonNull
    /* renamed from: B */
    O b(@NonNull I i);
}
