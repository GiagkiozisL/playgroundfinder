package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0863a.C0864a;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.mu */
public class mu implements mq<List<oj>, C0864a[]> {
    @NonNull
    /* renamed from: a */
    public C0864a[] b(@NonNull List<oj> list) {
        C0864a[] aVarArr = new C0864a[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return aVarArr;
            }
            aVarArr[i2] = m2466a((oj) list.get(i2));
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    public List<oj> a(@NonNull C0864a[] aVarArr) {
        ArrayList arrayList = new ArrayList();
        for (C0864a a : aVarArr) {
            arrayList.add(m2465a(a));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    private C0864a m2466a(@NonNull oj ojVar) {
        C0864a aVar = new C0864a();
        aVar.f2131b = ojVar.f1592a;
        aVar.f2132c = ojVar.f1593b;
        return aVar;
    }

    @NonNull
    /* renamed from: a */
    private oj m2465a(@NonNull C0864a aVar) {
        return new oj(aVar.f2131b, aVar.f2132c);
    }
}
