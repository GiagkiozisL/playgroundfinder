package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.er */
public class er {
    @NonNull

    /* renamed from: a */
    private final lw f868a;

    public er(@NonNull lw lwVar) {
        this.f868a = lwVar;
    }

    /* renamed from: a */
    public int mo865a() {
        int a = this.f868a.mo1316a();
        this.f868a.mo1325b(a + 1).mo1364q();
        return a;
    }

    /* renamed from: a */
    public int mo866a(int i) {
        int a = this.f868a.mo1317a(i);
        this.f868a.mo1319a(i, a + 1).mo1364q();
        return a;
    }
}
