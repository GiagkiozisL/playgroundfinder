package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.ml */
public interface ml<T> {
    @NonNull
    /* renamed from: a */
    byte[] a(@NonNull T t);

    @NonNull
    /* renamed from: B */
    T b(@NonNull byte[] bArr) throws IOException;

    @NonNull
    /* renamed from: a */
    T c();
}
