package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.xv */
public class xv {

    /* renamed from: a */
    private final String f2988a;

    /* renamed from: B */
    private final int f2989b;
    @NonNull

    /* renamed from: a */
    private final vz f2990c;

    public xv(int i, @NonNull String str, @NonNull vz vzVar) {
        this.f2989b = i;
        this.f2988a = str;
        this.f2990c = vzVar;
    }

    /* renamed from: a */
    public boolean mo2641a(@NonNull vx vxVar, @NonNull String str, @Nullable String str2) {
        int i;
        int a = vxVar.mo2511a();
        if (str2 != null) {
            i = a + str2.length();
        } else {
            i = a;
        }
        if (vxVar.containsKey(str)) {
            String str3 = (String) vxVar.get(str);
            if (str3 != null) {
                i -= str3.length();
            }
        } else {
            i += str.length();
        }
        return i > this.f2989b;
    }

    /* renamed from: a */
    public void mo2640a(@NonNull String str) {
        if (this.f2990c.mo2485c()) {
            this.f2990c.mo2482b("The %s has reached the total size limit that equals %d symbols. Item with key %s will be ignored", this.f2988a, Integer.valueOf(this.f2989b), str);
        }
    }
}
