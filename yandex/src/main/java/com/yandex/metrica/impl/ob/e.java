package com.yandex.metrica.impl.ob;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.e */
public abstract class e {

    /* renamed from: a */
    protected volatile int f754a = -1;

    /* renamed from: a */
    public abstract e mo738a(a aVar) throws IOException;

    /* renamed from: a */
    public int mo737a() {
        if (this.f754a < 0) {
            mo740b();
        }
        return this.f754a;
    }

    /* renamed from: B */
    public int mo740b() {
        int c = mo741c();
        this.f754a = c;
        return c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo741c() {
        return 0;
    }

    /* renamed from: a */
    public void mo739a(b bVar) throws IOException {
    }

    /* renamed from: a */
    public static final byte[] m1395a(e eVar) {
        byte[] bArr = new byte[eVar.mo740b()];
        m1394a(eVar, bArr, 0, bArr.length);
        return bArr;
    }

    /* renamed from: a */
    public static final void m1394a(e eVar, byte[] bArr, int i, int i2) {
        try {
            b a = b.m431a(bArr, i, i2);
            eVar.mo739a(a);
            a.mo359b();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    /* renamed from: a */
    public static final <T extends e> T m1393a(T t, byte[] bArr) throws d {
        return m1396b(t, bArr, 0, bArr.length);
    }

    /* renamed from: B */
    public static final <T extends e> T m1396b(T t, byte[] bArr, int i, int i2) throws d {
        try {
            a a = com.yandex.metrica.impl.ob.a.a(bArr, i, i2);
            t.mo738a(a);
            a.mo214a(0);
            return t;
        } catch (d e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
        }
    }

    public String toString() {
        return f.m1563a(this);
    }
}
