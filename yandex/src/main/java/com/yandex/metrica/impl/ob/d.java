package com.yandex.metrica.impl.ob;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.d */
public class d extends IOException {
    public d(String str) {
        super(str);
    }

    /* renamed from: a */
    static d a() {
        return new d("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    /* renamed from: B */
    static d b() {
        return new d("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    /* renamed from: a */
    static d c() {
        return new d("CodedInputStream encountered a malformed varint.");
    }

    /* renamed from: d */
    static d d() {
        return new d("Protocol message contained an invalid tag (zero).");
    }

    /* renamed from: e */
    static d e() {
        return new d("Protocol message end-group tag did not match expected tag.");
    }

    /* renamed from: f */
    static d f() {
        return new d("Protocol message tag had invalid wire type.");
    }

    /* renamed from: g */
    static d g() {
        return new d("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }
}
