package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.as */
public class as {

    /* renamed from: com.yandex.metrica.impl.ob.as$a */
    static class C0078a extends RuntimeException {
        public C0078a(String str) {
            super(str);
        }
    }

    /* renamed from: a */
    public static void m390a() {
        m392b();
    }

    /* renamed from: B */
    public static void m392b() throws IllegalStateException {
        if (!m391a("com.yandex.metrica.CounterConfiguration")) {
            throw new C0078a("\nClass com.yandex.metrica.CounterConfiguration isn't found.\nPerhaps this is due to obfuscation.\nIf you build your application with ProGuard,\nyou need to keep the Metrica for Apps.\nPlease try to use the following lines of code:\n##########################################\n-keep class com.yandex.metrica.** { *; }\n-dontwarn com.yandex.metrica.**\n##########################################");
        }
    }

    /* renamed from: a */
    public static boolean m391a(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
