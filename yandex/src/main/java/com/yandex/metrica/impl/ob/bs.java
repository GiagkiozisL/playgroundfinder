package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.bs */
public abstract class bs<T extends sw> extends bo<T> {
    @NonNull

    /* renamed from: j */
    private final bn f402j;

    public bs(@NonNull bn bnVar, @NonNull T t) {
        super(t);
        this.f402j = bnVar;
    }

    /* renamed from: B */
    public boolean mo463b() {
        return this.f402j.a(mo473k(), mo474l(), mo475m());
    }
}
