package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rr.a.b;

/* renamed from: com.yandex.metrica.impl.ob.nh */
public class nh implements mq<tt, rr.a.b> {

    @NonNull
    private final nl a;
    @NonNull
    private final nk b;

    public nh() {
        this(new nl(), new nk());
    }

    @NonNull
    @Override
    public rr.a.b b(@NonNull tt var1) {
        rr.a.b var2 = new b();
        var2.b = this.a.a(var1.a);
        var2.c = this.b.b(var1.b);
        var2.d = var1.c;
        var2.e = var1.d;
        return var2;
    }

    @NonNull
    @Override
    public tt a(@NonNull rr.a.b var1) {
        return new tt(this.a.a(var1.b), this.b.a(var1.c), var1.d, var1.e);
    }

    @VisibleForTesting
    nh(@NonNull nl var1, @NonNull nk var2) {
        this.a = var1;
        this.b = var2;
    }

//    @NonNull
//    @Override
//    public Object aaa(@NonNull Object o) {
//        return null;
//    }
//
//    @NonNull
//    @Override
//    public Object b(@NonNull Object o) {
//        return null;
//    }
}
