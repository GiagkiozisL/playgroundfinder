package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.st.C0985b;

/* renamed from: com.yandex.metrica.impl.ob.fj */
public class fj extends C0985b {
    public fj(@NonNull cs csVar) {
        super(csVar);
    }

    /* renamed from: a */
    public boolean mo892a(@Nullable Boolean bool) {
        return !this.f2506a.mo653b() && super.mo892a(bool);
    }
}
