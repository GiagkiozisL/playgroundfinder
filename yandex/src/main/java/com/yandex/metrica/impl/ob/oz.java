package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rh.b.C0819a;

import org.json.JSONArray;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.oz */
public class oz {
    @NonNull
    /* renamed from: a */
    public C0819a mo1619a(@NonNull on onVar) {
        C0819a aVar = new C0819a();
        aVar.f1922b = onVar.mo1560a() == null ? aVar.f1922b : onVar.mo1560a().longValue();
        aVar.f1923c = TimeUnit.MILLISECONDS.toSeconds(onVar.mo1564b());
        aVar.f1926f = TimeUnit.MILLISECONDS.toSeconds(onVar.mo1569e());
        JSONArray d = onVar.mo1568d();
        if (d != null) {
            aVar.f1924d = bv.m760b(d);
        }
        JSONArray c = onVar.mo1567c();
        if (c != null) {
            aVar.f1925e = bv.m757a(c);
        }
        return aVar;
    }
}
