package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.qd */
public abstract class qd {

    /* renamed from: d */
    private static final qk f1763d = new qk("UNDEFINED_");

    /* renamed from: a */
    protected qk f1764a;

    /* renamed from: B */
    protected final String f1765b;

    /* renamed from: a */
    protected final SharedPreferences f1766c;

    /* renamed from: e */
    private final Map<String, Object> f1767e = new HashMap();

    /* renamed from: f */
    private boolean f1768f = false;

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract String mo1693f();

    public qd(Context context, String str) {
        this.f1765b = str;
        this.f1766c = m2899a(context);
        this.f1764a = new qk(f1763d.mo1742a(), this.f1765b);
    }

    /* renamed from: a */
    private SharedPreferences m2899a(Context context) {
        return ql.m2970a(context, mo1693f());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends qd> T mo1695a(String str, Object obj) {
        synchronized (this) {
            if (obj != null) {
                this.f1767e.put(str, obj);
            }
        }
        return (T)this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public <T extends qd> T mo1697h(String str) {
        synchronized (this) {
            this.f1767e.put(str, this);
        }
        return (T)this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public <T extends qd> T mo1696h() {
        synchronized (this) {
            this.f1768f = true;
            this.f1767e.clear();
        }
        return (T)this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public String mo1698i() {
        return this.f1765b;
    }

    /* renamed from: j */
    public void mo1699j() {
        synchronized (this) {
            mo1684a();
            this.f1767e.clear();
            this.f1768f = false;
        }
    }

    /* renamed from: a */
    private void mo1684a() {
        Editor edit = this.f1766c.edit();
        if (this.f1768f) {
            edit.clear();
            m2901a(edit);
            return;
        }
        for (Entry entry : this.f1767e.entrySet()) {
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            if (value == this) {
                edit.remove(str);
            } else if (value instanceof String) {
                edit.putString(str, (String) value);
            } else if (value instanceof Long) {
                edit.putLong(str, ((Long) value).longValue());
            } else if (value instanceof Integer) {
                edit.putInt(str, ((Integer) value).intValue());
            } else if (value instanceof Boolean) {
                edit.putBoolean(str, ((Boolean) value).booleanValue());
            } else if (value != null) {
                throw new UnsupportedOperationException();
            }
        }
        m2901a(edit);
    }

    /* renamed from: a */
    private void m2901a(Editor editor) {
        editor.apply();
    }
}
