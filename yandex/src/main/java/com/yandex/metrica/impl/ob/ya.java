package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.ya */
public class ya<T> implements yb<T> {
    @NonNull

    /* renamed from: a */
    private final yb<T> f3003a;
    @Nullable

    /* renamed from: B */
    private final T f3004b;

    public ya(@NonNull yb<T> ybVar, @Nullable T t) {
        this.f3003a = ybVar;
        this.f3004b = t;
    }

    @Nullable
    /* renamed from: a */
    public T a(@Nullable T t) {
        if (t != this.f3003a.a(t)) {
            return this.f3004b;
        }
        return t;
    }
}
