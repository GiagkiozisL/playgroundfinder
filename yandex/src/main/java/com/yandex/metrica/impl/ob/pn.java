package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.pn */
public class pn {
    @NonNull

    /* renamed from: a */
    public final List<pu> f1734a;
    @Nullable

    /* renamed from: B */
    public final l f1735b;
    @NonNull

    /* renamed from: a */
    public final List<String> f1736c;

    public pn(@NonNull List<pu> list, @Nullable l lVar, @NonNull List<String> list2) {
        this.f1734a = list;
        this.f1735b = lVar;
        this.f1736c = list2;
    }
}
