package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.vp */
public class vp extends vs {

    /* renamed from: a */
    private static final vp f2901a = new vp();

    public vp(@Nullable String str) {
        super(str);
    }

    private vp() {
        this("");
    }

    /* renamed from: h */
    public static vp m4208h() {
        return f2901a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public boolean mo2487d() {
        return super.mo2487d() && "publicProd".startsWith("internal");
    }

    /* renamed from: f */
    public String mo2489f() {
        return "AppMetricaInternal";
    }
}
