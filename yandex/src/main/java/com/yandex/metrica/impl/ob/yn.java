package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.yn */
class yn implements yk<Integer> {
    yn() {
    }

    /* renamed from: a */
    public yi a(@Nullable Integer num) {
        if (num == null || num.intValue() > 0) {
            return yi.a(this);
        }
        return yi.a(this, "Invalid quantity value " + num);
    }
}
