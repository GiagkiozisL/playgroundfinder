package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rq.C0859a;
import com.yandex.metrica.impl.ob.rq.C0859a.C0860a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.nd */
public class nd implements mq<te, C0859a> {
    @NonNull
    /* renamed from: a */
    public C0859a b(@NonNull te teVar) {
        C0859a aVar = new C0859a();
        aVar.f2084b = new C0860a[teVar.f2564a.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < teVar.f2564a.size()) {
                aVar.f2084b[i2] = m2545a((th) teVar.f2564a.get(i2));
                i = i2 + 1;
            } else {
                aVar.f2085c = teVar.f2565b;
                aVar.f2086d = teVar.f2566c;
                aVar.f2087e = teVar.f2567d;
                aVar.f2088f = teVar.f2568e;
                return aVar;
            }
        }
    }

    @NonNull
    /* renamed from: a */
    public te a(@NonNull C0859a aVar) {
        ArrayList arrayList = new ArrayList(aVar.f2084b.length);
        for (C0860a a : aVar.f2084b) {
            arrayList.add(m2546a(a));
        }
        return new te(arrayList, aVar.f2085c, aVar.f2086d, aVar.f2087e, aVar.f2088f);
    }

    @NonNull
    /* renamed from: a */
    private C0860a m2545a(@NonNull th thVar) {
        C0860a aVar = new C0860a();
        aVar.f2090b = thVar.f2572a;
        List<String> list = thVar.f2573b;
        aVar.f2091c = new String[list.size()];
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return aVar;
            }
            aVar.f2091c[i2] = (String) it.next();
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    private th m2546a(@NonNull C0860a aVar) {
        ArrayList arrayList = new ArrayList();
        if (aVar.f2091c != null && aVar.f2091c.length > 0) {
            ArrayList arrayList2 = new ArrayList(aVar.f2091c.length);
            for (String add : aVar.f2091c) {
                arrayList2.add(add);
            }
            arrayList = arrayList2;
        }
        return new th(cu.m1151a(aVar.f2090b), arrayList);
    }
}
