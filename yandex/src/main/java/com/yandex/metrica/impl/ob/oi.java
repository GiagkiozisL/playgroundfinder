package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.p.C0740a;

import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.oi */
class oi extends of {

    /* renamed from: a */
    public static final long f1584a = TimeUnit.MINUTES.toMillis(2);

    /* renamed from: B */
    public static final long f1585b = TimeUnit.SECONDS.toMillis(10);
    @Nullable

    /* renamed from: a */
    private C0740a<Location> f1586c;
    @NonNull

    /* renamed from: d */
    private C0709a f1587d;

    /* renamed from: e */
    private long f1588e;

    /* renamed from: com.yandex.metrica.impl.ob.oi$a */
    public static class C0709a {

        /* renamed from: a */
        public final long f1589a;

        /* renamed from: B */
        public final long f1590b;

        /* renamed from: a */
        public final long f1591c;

        public C0709a(long j, long j2, long j3) {
            this.f1589a = j;
            this.f1590b = j2;
            this.f1591c = j3;
        }
    }

    public oi(@Nullable oe oeVar) {
        this(oeVar, new C0709a(f1584a, 200, 50), f1585b);
    }

    /* renamed from: B */
    public void mo1541b(@Nullable String str, @Nullable Location location, @Nullable oh ohVar) {
        if (location != null) {
            m2666a(location);
        }
    }

    /* renamed from: a */
    private void m2666a(@NonNull Location location) {
        if (this.f1586c == null || this.f1586c.mo1622a(this.f1588e) || m2667a(location, (Location) this.f1586c.mo1620a())) {
            Location location2 = new Location(location);
            C0740a<Location> aVar = new C0740a<>();
            aVar.mo1621a(location2);
            this.f1586c = aVar;
        }
    }

    @Nullable
    /* renamed from: a */
    public Location mo1549a() {
        if (this.f1586c == null) {
            return null;
        }
        return (Location) this.f1586c.mo1625d();
    }

    /* renamed from: a */
    private boolean m2667a(@Nullable Location location, @Nullable Location location2) {
        return m2668a(location, location2, this.f1587d.f1589a, this.f1587d.f1590b);
    }

    /* renamed from: a */
    public static boolean m2668a(@Nullable Location location, @Nullable Location location2, long j, long j2) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (location2 == null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long time = location.getTime() - location2.getTime();
        boolean z5 = time > j;
        if (time < (-j)) {
            z = true;
        } else {
            z = false;
        }
        if (time > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z5) {
            return true;
        }
        if (z) {
            return false;
        }
        int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
        boolean z6 = accuracy > 0;
        if (accuracy < 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (((long) accuracy) > j2) {
            z4 = true;
        } else {
            z4 = false;
        }
        boolean a = m2669a(location.getProvider(), location2.getProvider());
        if (z3) {
            return true;
        }
        if (z2 && !z6) {
            return true;
        }
        if (!z2 || z4 || !a) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    static boolean m2669a(@Nullable String str, @Nullable String str2) {
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    @VisibleForTesting
    oi(@Nullable oe oeVar, @NonNull C0709a aVar, long j) {
        super(oeVar);
        this.f1587d = aVar;
        this.f1588e = j;
    }
}
