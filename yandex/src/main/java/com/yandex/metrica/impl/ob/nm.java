package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.b.Aa.C0868b;
import com.yandex.metrica.impl.ob.tt.a.C1042b;

/* renamed from: com.yandex.metrica.impl.ob.nm */
public class nm implements mq<C1042b, C0868b> {
    @NonNull
    /* renamed from: a */
    public C0868b b(@NonNull C1042b bVar) {
        C0868b bVar2 = new C0868b();
        bVar2.f2146b = bVar.f2627a.toString();
        if (bVar.f2628b != null) {
            bVar2.f2147c = bVar.f2628b;
        }
        if (bVar.f2629c != null) {
            bVar2.f2148d = bVar.f2629c;
        }
        return bVar2;
    }

    @NonNull
    /* renamed from: a */
    public C1042b a(@NonNull C0868b bVar) {
        byte[] bArr = null;
        String str = bVar.f2146b;
        byte[] bArr2 = cx.nullOrEmpty(bVar.f2147c) ? null : bVar.f2147c;
        if (!cx.nullOrEmpty(bVar.f2148d)) {
            bArr = bVar.f2148d;
        }
        return new C1042b(str, bArr2, bArr);
    }
}
