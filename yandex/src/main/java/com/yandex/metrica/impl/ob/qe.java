package com.yandex.metrica.impl.ob;

import android.content.Context;

import java.util.Map;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qe */
public class qe extends qd {

    /* renamed from: d */
    private static final qk f1769d = new qk("UUID");

    /* renamed from: e */
    private static final qk f1770e = new qk("DEVICEID");

    /* renamed from: f */
    private static final qk f1771f = new qk("DEVICEID_2");

    /* renamed from: g */
    private static final qk f1772g = new qk("DEVICEID_3");

    /* renamed from: h */
    private static final qk f1773h = new qk("AD_URL_GET");

    /* renamed from: i */
    private static final qk f1774i = new qk("AD_URL_REPORT");

    /* renamed from: j */
    private static final qk f1775j = new qk("HOST_URL");

    /* renamed from: k */
    private static final qk f1776k = new qk("SERVER_TIME_OFFSET");

    /* renamed from: l */
    private static final qk f1777l = new qk("STARTUP_REQUEST_TIME");

    /* renamed from: m */
    private static final qk f1778m = new qk("CLIDS");

    /* renamed from: n */
    private qk f1779n = new qk(f1769d.mo1742a());

    /* renamed from: o */
    private qk f1780o = new qk(f1770e.mo1742a());

    /* renamed from: p */
    private qk f1781p = new qk(f1771f.mo1742a());

    /* renamed from: q */
    private qk f1782q = new qk(f1772g.mo1742a());

    /* renamed from: r */
    private qk f1783r = new qk(f1773h.mo1742a());

    /* renamed from: s */
    private qk f1784s = new qk(f1774i.mo1742a());

    /* renamed from: t */
    private qk f1785t = new qk(f1775j.mo1742a());

    /* renamed from: u */
    private qk f1786u = new qk(f1776k.mo1742a());

    /* renamed from: v */
    private qk f1787v = new qk(f1777l.mo1742a());

    /* renamed from: w */
    private qk f1788w = new qk(f1778m.mo1742a());

    public qe(Context context) {
        super(context, null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_startupinfopreferences";
    }

    /* renamed from: a */
    public String mo1701a(String str) {
        return this.f1766c.getString(this.f1779n.mo1744b(), str);
    }

    /* renamed from: B */
    public String mo1704b(String str) {
        return this.f1766c.getString(this.f1782q.mo1744b(), str);
    }

    /* renamed from: a */
    public String mo1684a() {
        return this.f1766c.getString(this.f1781p.mo1744b(), this.f1766c.getString(this.f1780o.mo1744b(), ""));
    }

    /* renamed from: a */
    public String mo1705c(String str) {
        return this.f1766c.getString(this.f1783r.mo1744b(), str);
    }

    /* renamed from: d */
    public String mo1707d(String str) {
        return this.f1766c.getString(this.f1784s.mo1744b(), str);
    }

    /* renamed from: a */
    public long mo1700a(long j) {
        return this.f1766c.getLong(this.f1786u.mo1742a(), j);
    }

    /* renamed from: B */
    public long mo1702b(long j) {
        return this.f1766c.getLong(this.f1787v.mo1744b(), j);
    }

    /* renamed from: e */
    public String mo1708e(String str) {
        return this.f1766c.getString(this.f1788w.mo1744b(), str);
    }

    /* renamed from: B */
    public qe mo1703b() {
        return (qe) mo1696h();
    }

    /* renamed from: a */
    public Map<String, ?> mo1706c() {
        return this.f1766c.getAll();
    }
}
