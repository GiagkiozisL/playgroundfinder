package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.Closeable;

/* renamed from: com.yandex.metrica.impl.ob.cv */
public class cv<C extends et> implements Closeable {

    /* renamed from: a */
    final Object f638a = new Object();
    @NonNull

    /* renamed from: B */
    final bl f639b;

    /* renamed from: a */
    boolean f640c = false;
    @NonNull

    /* renamed from: d */
    private C f641d;
    @NonNull

    /* renamed from: e */
    private final uq f642e;

    public cv(@NonNull C c, @NonNull uq uqVar, @NonNull bl blVar) {
        this.f641d = c;
        this.f642e = uqVar;
        this.f639b = blVar;
    }

    public void close() {
        synchronized (this.f638a) {
            if (!this.f640c) {
                mo557a();
                if (this.f639b.isAlive()) {
                    this.f639b.a();
                }
                this.f640c = true;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo557a() {
    }

    /* renamed from: e */
    public void mo659e() {
        synchronized (this.f638a) {
            if (!this.f640c) {
                mo660f();
                mo557a();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public void mo660f() {
        synchronized (this.f638a) {
            if (!this.f640c) {
                mo559c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo559c() {
        this.f642e.a(this.f639b);
    }

    @NonNull
    /* renamed from: g */
    public C mo661g() {
        return this.f641d;
    }
}
