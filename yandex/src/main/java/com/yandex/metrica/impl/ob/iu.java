package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.iu */
public class iu extends it {
    iu(@NonNull en enVar, @NonNull jc jcVar) {
        this(enVar, jcVar, new jg(enVar.mo838y(), "background"));
    }

    @VisibleForTesting
    iu(@NonNull en enVar, @NonNull jc jcVar, @NonNull jg jgVar) {
        super(enVar, jcVar, jgVar, jb.m1893a(jh.BACKGROUND).mo1069a(3600).mo1070a());
    }
}
