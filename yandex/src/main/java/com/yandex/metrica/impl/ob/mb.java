package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.mb */
public abstract class mb<T extends e> implements me<T> {
    @NonNull
    /* renamed from: B */
    public abstract T c();

    @NonNull
    /* renamed from: a */
    public byte[] a(@NonNull T t) {
        return e.m1395a((e) t);
    }
}
