package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.jk */
public class jk {
    @NonNull

    /* renamed from: a */
    private Context ctx;
    @NonNull

    /* renamed from: B */
    private jj f1139b;
    @NonNull

    /* renamed from: a */
    private jo f1140c;

    public jk(@NonNull Context context) {
        this(context, new jj(context), new jo(context));
    }

    @VisibleForTesting
    jk(@NonNull Context context, @NonNull jj jjVar, @NonNull jo joVar) {
        this.ctx = context;
        this.f1139b = jjVar;
        this.f1140c = joVar;
    }

    /* renamed from: a */
    public void mo1105a() {
//        this.ctx.getPackageName();
        this.f1140c.mo1116a().a(this.f1139b.mo1104a());
    }
}
