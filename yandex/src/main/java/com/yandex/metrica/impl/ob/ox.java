package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.ox */
public class ox {
    @NonNull

    /* renamed from: a */
    private final oo f1696a;
    @NonNull

    /* renamed from: B */
    private final vd f1697b;
    @NonNull

    /* renamed from: a */
    private final cy f1698c;

    /* renamed from: d */
    private final ly f1699d;

    public ox(@NonNull oo ooVar, @NonNull vd vdVar, @NonNull cy cyVar, @NonNull ly lyVar) {
        this.f1696a = ooVar;
        this.f1697b = vdVar;
        this.f1698c = cyVar;
        this.f1699d = lyVar;
        m2770a();
    }

    /* renamed from: a */
    private void m2770a() {
        boolean g = this.f1699d.mo1393g();
        this.f1696a.mo1573a(g);
        this.f1698c.mo666a(g);
        this.f1697b.mo2415a(g);
    }

    /* renamed from: a */
    public void mo1616a(@NonNull Object obj) {
        this.f1696a.mo1575b(obj);
        this.f1697b.b();
    }

    /* renamed from: B */
    public void mo1618b(@NonNull Object obj) {
        this.f1696a.mo1572a(obj);
        this.f1697b.a();
    }

    /* renamed from: a */
    public void mo1617a(boolean z) {
        this.f1696a.mo1573a(z);
        this.f1697b.mo2415a(z);
        this.f1698c.mo666a(z);
        this.f1699d.mo1377b(z);
    }

    /* renamed from: a */
    public void mo1615a(@NonNull uk ukVar, boolean z) {
        this.f1696a.mo1571a(ukVar, z ? ukVar.backgroundLocationCollectionConfig : ukVar.foregroundLocationCollectionConfig);
        this.f1698c.mo665a(ukVar);
        this.f1697b.mo2412a(ukVar);
    }
}
