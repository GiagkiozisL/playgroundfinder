package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.l.A_enum;
import com.yandex.metrica.impl.ob.rm.C0846a.C0847a;

/* renamed from: com.yandex.metrica.impl.ob.mo */
public class mo implements mq<l, C0847a> {
    @NonNull
    /* renamed from: a */
    public C0847a b(@NonNull l lVar) {
        C0847a aVar = new C0847a();
        if (lVar.f1274a != null) {
            switch (lVar.f1274a) {
                case ACTIVE:
                    aVar.f2059b = 1;
                    break;
                case WORKING_SET:
                    aVar.f2059b = 2;
                    break;
                case FREQUENT:
                    aVar.f2059b = 3;
                    break;
                case RARE:
                    aVar.f2059b = 4;
                    break;
            }
        }
        if (lVar.f1275b != null) {
            if (lVar.f1275b.booleanValue()) {
                aVar.f2060c = 1;
            } else {
                aVar.f2060c = 0;
            }
        }
        return aVar;
    }

    @NonNull
    /* renamed from: a */
    public l a(@NonNull C0847a aVar) {
        A_enum aVar2;
        Boolean bool = null;
        switch (aVar.f2059b) {
            case 1:
                aVar2 = A_enum.ACTIVE;
                break;
            case 2:
                aVar2 = A_enum.WORKING_SET;
                break;
            case 3:
                aVar2 = A_enum.FREQUENT;
                break;
            case 4:
                aVar2 = A_enum.RARE;
                break;
            default:
                aVar2 = null;
                break;
        }
        switch (aVar.f2060c) {
            case 0:
                bool = Boolean.valueOf(false);
                break;
            case 1:
                bool = Boolean.valueOf(true);
                break;
        }
        return new l(aVar2, bool);
    }
}
