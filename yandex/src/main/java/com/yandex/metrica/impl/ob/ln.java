package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/* renamed from: com.yandex.metrica.impl.ob.ln */
public class ln {
    @NonNull

    /* renamed from: a */
    private final Context f1328a;
    @NonNull

    /* renamed from: B */
    private final String f1329b;

    /* renamed from: a */
    private File f1330c;

    /* renamed from: d */
    private FileLock f1331d;

    /* renamed from: e */
    private RandomAccessFile f1332e;

    /* renamed from: f */
    private FileChannel f1333f;

    public ln(@NonNull Context context, @NonNull String str) {
        this.f1328a = context;
        this.f1329b = str;
    }

    /* renamed from: a */
    public synchronized void mo1271a() throws IOException {
        this.f1330c = new File(this.f1328a.getFilesDir(), new File(this.f1329b).getName() + ".lock");
        this.f1332e = new RandomAccessFile(this.f1330c, "rw");
        this.f1333f = this.f1332e.getChannel();
        this.f1331d = this.f1333f.lock();
    }

    /* renamed from: B */
    public synchronized void mo1272b() {
        String str = "";
        if (this.f1330c != null) {
            str = this.f1330c.getAbsolutePath();
        }
        am.a(str, this.f1331d);
        cx.a((Closeable) this.f1332e);
        cx.a((Closeable) this.f1333f);
        this.f1332e = null;
        this.f1331d = null;
        this.f1333f = null;
    }
}
