package com.yandex.metrica.impl.ob;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.tn */
public class tn {

    /* renamed from: a */
    private final JSONObject f2583a = new JSONObject();

    /* renamed from: a */
    public void mo2249a(String str) {
        m3795a("uuid", str);
    }

    /* renamed from: B */
    public void mo2250b(String str) {
        m3795a("device_id", str);
    }

    /* renamed from: a */
    public void mo2251c(String str) {
        m3795a("google_aid", str);
    }

    /* renamed from: d */
    public void mo2252d(String str) {
        m3795a("android_id", str);
    }

    /* renamed from: a */
    private void m3795a(String str, String str2) {
        if (!(TextUtils.isEmpty(str) || TextUtils.isEmpty(str2))) {
            try {
                this.f2583a.put(str, str2);
            } catch (Throwable th) {
            }
        }
    }

    /* renamed from: a */
    public String mo2248a() throws JSONException {
        return this.f2583a.toString();
    }
}
