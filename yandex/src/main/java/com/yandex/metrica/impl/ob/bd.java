package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

public class bd implements bc {

    private xh f296a;

    public bc f297b;

    public bd(@NonNull bc bcVar) {
        this(al.m324a().mo292j().mo2625a(), bcVar);
    }

    public void a() {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() {
                bd.this.f297b.a();
            }
        });
    }

    public void a(final Intent intent, final int i) {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() {
                bd.this.f297b.a(intent, i);
            }
        });
    }

    public void a(final Intent intent, final int i, final int i2) {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() {
                bd.this.f297b.a(intent, i, i2);
            }
        });
    }

    public void a(final Intent intent) {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() {
                bd.this.f297b.a(intent);
            }
        });
    }

    public void b(final Intent intent) {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() {
                bd.this.f297b.b(intent);
            }
        });
    }

    public void c(final Intent intent) {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() {
                bd.this.f297b.c(intent);
            }
        });
    }

    public void b() {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() throws Exception {
                bd.this.f297b.b();
            }
        });
    }

    public void a(String str, int i, String str2, Bundle bundle) {
        final String str3 = str;
        final int i2 = i;
        final String str4 = str2;
        final Bundle bundle2 = bundle;
        this.f296a.a((Runnable) new wb() {
            public void mo400a() throws RemoteException {
                bd.this.f297b.a(str3, i2, str4, bundle2);
            }
        });
    }

    public void a(final Bundle bundle) {
        this.f296a.a((Runnable) new wb() {
            public void mo400a() throws Exception {
                bd.this.f297b.a(bundle);
            }
        });
    }

    @VisibleForTesting
    bd(@NonNull xh xhVar, @NonNull bc bcVar) {
        this.f296a = xhVar;
        this.f297b = bcVar;
    }
}
