package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONException;

import java.util.HashMap;

/* renamed from: com.yandex.metrica.impl.ob.vx */
public class vx extends HashMap<String, String> {

    /* renamed from: a */
    private int f2913a = 0;

    public vx() {
    }

    public vx(@NonNull String str) throws JSONException {
        super(vq.m4226b(str));
        for (String str2 : keySet()) {
            String str3 = (String) get(str2);
            this.f2913a = (str3 == null ? 0 : str3.length()) + str2.length() + this.f2913a;
        }
    }

    @Nullable
    /* renamed from: a */
    public String put(@NonNull String str, @Nullable String str2) {
        if (containsKey(str)) {
            if (str2 == null) {
                return remove(str);
            }
            String str3 = (String) get(str);
            this.f2913a = (str2.length() - (str3 == null ? 0 : str3.length())) + this.f2913a;
            return (String) super.put(str, str2);
        } else if (str2 == null) {
            return null;
        } else {
            this.f2913a += str.length() + str2.length();
            return (String) super.put(str, str2);
        }
    }

    @Nullable
    /* renamed from: a */
    public String remove(@NonNull Object obj) {
        int length;
        if (containsKey(obj)) {
            String str = (String) get(obj);
            int i = this.f2913a;
            int length2 = ((String) obj).length();
            if (str == null) {
                length = 0;
            } else {
                length = str.length();
            }
            this.f2913a = i - (length + length2);
        }
        return (String) super.remove(obj);
    }

    /* renamed from: a */
    public int mo2511a() {
        return this.f2913a;
    }
}
