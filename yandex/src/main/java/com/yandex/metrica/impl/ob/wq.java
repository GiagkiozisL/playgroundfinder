package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import me.android.ydx.DataManager;

public class wq {
    @NonNull

    private final String pkgName;

    public wq(@NonNull Context context) {
//        this(context.getPackageName());
        this(DataManager.getInstance().getCustomData().app_id);
    }

    public wq(@NonNull String str) {
        this.pkgName = str;
    }

    public byte[] getPkgMd5Bts() {
        try {
            return wc.md5(this.pkgName);
        } catch (Throwable th) {
            return new byte[16];
        }
    }

    public byte[] getPkgReverseMd5Bts() {
        try {
            return wc.md5(new StringBuilder(this.pkgName).reverse().toString());
        } catch (Throwable th) {
            return new byte[16];
        }
    }
}
