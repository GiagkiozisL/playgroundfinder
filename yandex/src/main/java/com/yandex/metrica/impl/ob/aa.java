package com.yandex.metrica.impl.ob;

import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.j;
import com.yandex.metrica.j.C1231a;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.a */
public class aa implements ba {

    /* renamed from: a */
    private Location f137a;

    /* renamed from: B */
    private Boolean f138b;

    /* renamed from: a */
    private Boolean f139c;

    /* renamed from: d */
    private Map<String, String> f140d = new LinkedHashMap();

    /* renamed from: e */
    private Map<String, String> f141e = new LinkedHashMap();

    /* renamed from: f */
    private boolean f142f;

    /* renamed from: g */
    private boolean f143g;

    /* renamed from: h */
    private cd f144h;

    /* renamed from: a */
    public Location mo242a() {
        return this.f137a;
    }

    /* renamed from: B */
    public Boolean mo247b() {
        return this.f138b;
    }

    /* renamed from: a */
    public void a(boolean z) {
        this.f138b = Boolean.valueOf(z);
        m249f();
    }

    /* renamed from: a */
    public Boolean mo248c() {
        return this.f139c;
    }

    public void setStatisticsSending(boolean value) {
        this.f139c = Boolean.valueOf(value);
        m249f();
    }

    /* renamed from: a */
    public void a(Location location) {
        this.f137a = location;
    }

    /* renamed from: d */
    public boolean mo249d() {
        return this.f142f;
    }

    /* renamed from: e */
    private void m248e() {
        this.f137a = null;
        this.f138b = null;
        this.f139c = null;
        this.f140d.clear();
        this.f141e.clear();
        this.f142f = false;
    }

    /* renamed from: a */
    public j mo243a(j jVar) {
        if (this.f143g) {
            return jVar;
        }
        C1231a b = m246b(jVar);
        m243a(jVar, b);
        this.f143g = true;
        m248e();
        return b.mo2697b();
    }

    /* renamed from: B */
    private C1231a m246b(j jVar) {
        C1231a a = j.m4545a(jVar.apiKey);
        a.mo2691a(jVar.b, jVar.j);
        a.mo2699c(jVar.a);
        a.mo2685a(jVar.preloadInfo);
        a.mo2684a(jVar.location);
        a.mo2687a(jVar.m);
        m242a(a, jVar);
        m244a(this.f140d, a);
        m244a(jVar.i, a);
        m247b(this.f141e, a);
        m247b(jVar.h, a);
        return a;
    }

    /* renamed from: a */
    private void m242a(@NonNull C1231a aVar, @NonNull j jVar) {
        if (cx.m1189a((Object) jVar.d)) {
            aVar.mo2690a(jVar.d);
        }
        if (cx.m1189a((Object) jVar.appVersion)) {
            aVar.mo2688a(jVar.appVersion);
        }
        if (cx.m1189a((Object) jVar.f)) {
            aVar.mo2701d(jVar.f.intValue());
        }
        if (cx.m1189a((Object) jVar.e)) {
            aVar.mo2693b(jVar.e.intValue());
        }
        if (cx.m1189a((Object) jVar.g)) {
            aVar.mo2698c(jVar.g.intValue());
        }
        if (cx.m1189a((Object) jVar.logs) && jVar.logs.booleanValue()) {
            aVar.mo2682a();
        }
        if (cx.m1189a((Object) jVar.sessionTimeout)) {
            aVar.mo2683a(jVar.sessionTimeout.intValue());
        }
        if (cx.m1189a((Object) jVar.crashReporting)) {
            aVar.mo2692a(jVar.crashReporting.booleanValue());
        }
        if (cx.m1189a((Object) jVar.nativeCrashReporting)) {
            aVar.mo2696b(jVar.nativeCrashReporting.booleanValue());
        }
        if (cx.m1189a((Object) jVar.locationTracking)) {
            aVar.mo2702d(jVar.locationTracking.booleanValue());
        }
        if (cx.m1189a((Object) jVar.installedAppCollecting)) {
            aVar.mo2704e(jVar.installedAppCollecting.booleanValue());
        }
        if (cx.m1189a((Object) jVar.c)) {
            aVar.mo2694b(jVar.c);
        }
        if (cx.m1189a((Object) jVar.firstActivationAsUpdate)) {
            aVar.mo2706g(jVar.firstActivationAsUpdate.booleanValue());
        }
        if (cx.m1189a((Object) jVar.statisticsSending)) {
            aVar.mo2705f(jVar.statisticsSending.booleanValue());
        }
        if (cx.m1189a((Object) jVar.l)) {
            aVar.mo2700c(jVar.l.booleanValue());
        }
        if (cx.m1189a((Object) jVar.maxReportsInDatabaseCount)) {
            aVar.mo2703e(jVar.maxReportsInDatabaseCount.intValue());
        }
        if (cx.m1189a((Object) jVar.n)) {
            aVar.mo2686a(jVar.n);
        }
    }

    /* renamed from: a */
    private void m244a(@Nullable Map<String, String> map, @NonNull C1231a aVar) {
        if (!cx.m1193a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                aVar.mo2695b((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    /* renamed from: B */
    private void m247b(@Nullable Map<String, String> map, @NonNull C1231a aVar) {
        if (!cx.m1193a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                aVar.mo2689a((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    /* renamed from: a */
    private void m243a(j jVar, C1231a aVar) {
        Boolean b = mo247b();
        if (m245a((Object) jVar.locationTracking) && cx.m1189a((Object) b)) {
            aVar.mo2702d(b.booleanValue());
        }
        Location a = mo242a();
        if (m245a((Object) jVar.location) && cx.m1189a((Object) a)) {
            aVar.mo2684a(a);
        }
        Boolean c = mo248c();
        if (m245a((Object) jVar.statisticsSending) && cx.m1189a((Object) c)) {
            aVar.mo2705f(c.booleanValue());
        }
    }

    /* renamed from: a */
    private static boolean m245a(Object obj) {
        return obj == null;
    }

    /* renamed from: a */
    public void mo245a(cd cdVar) {
        this.f144h = cdVar;
    }

    /* renamed from: f */
    private void m249f() {
        if (this.f144h != null) {
            this.f144h.mo573a(this.f138b, this.f139c);
        }
    }
}
