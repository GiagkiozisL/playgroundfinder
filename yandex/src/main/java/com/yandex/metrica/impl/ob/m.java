package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.l.A_enum;

/* renamed from: com.yandex.metrica.impl.ob.m */
public class m {
    @NonNull

    /* renamed from: a */
    private final Context f1472a;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public final j f1473b;

    public m(@NonNull Context context) {
        this(context, new j());
    }

    @VisibleForTesting
    m(@NonNull Context context, @NonNull j jVar) {
        this.f1472a = context;
        this.f1473b = jVar;
    }

    @Nullable
    /* renamed from: a */
    public l mo1412a() {
        if (cx.a(28)) {
            return m2390b();
        }
        return null;
    }

    @TargetApi(28)
    @NonNull
    @SuppressLint("WrongConstant")
    /* renamed from: B */
    private l m2390b() {
        return new l((A_enum) cx.a((wo<UsageStatsManager, A_enum>) new wo<UsageStatsManager, A_enum>() {
            /* renamed from: a */
            public A_enum a(UsageStatsManager usageStatsManager) {
                return m.this.f1473b.mo1065a(usageStatsManager.getAppStandbyBucket());
            }
        }, (UsageStatsManager) this.f1472a.getSystemService("usagestats"), "getting app standby bucket", "usageStatsManager"),
                (Boolean) cx.a((wo<ActivityManager, Boolean>) new wo<ActivityManager, Boolean>() {
            /* renamed from: a */
            public Boolean a(ActivityManager activityManager) throws Throwable {
                return Boolean.valueOf(activityManager.isBackgroundRestricted());
            }
        }, (ActivityManager) this.f1472a.getSystemService("activity"), "getting is background restricted", "activityManager"));
    }
}
