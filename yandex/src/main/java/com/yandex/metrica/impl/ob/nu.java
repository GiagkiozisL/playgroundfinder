package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.yandex.metrica.impl.ob.nu */
public class nu implements nv, nw {

    /* renamed from: a */
    private final Set<Integer> f1552a = new HashSet();

    /* renamed from: B */
    private AtomicLong f1553b;

    public nu(@NonNull kz kzVar) {
        this.f1552a.add(Integer.valueOf(C0058a.EVENT_TYPE_FIRST_ACTIVATION.mo259a()));
        this.f1552a.add(Integer.valueOf(C0058a.EVENT_TYPE_APP_UPDATE.mo259a()));
        this.f1552a.add(Integer.valueOf(C0058a.EVENT_TYPE_INIT.mo259a()));
        this.f1552a.add(Integer.valueOf(C0058a.EVENT_TYPE_IDENTITY.mo259a()));
        this.f1552a.add(Integer.valueOf(C0058a.EVENT_TYPE_SEND_REFERRER.mo259a()));
        kzVar.mo1202a((nw) this);
        this.f1553b = new AtomicLong(kzVar.mo1195a(this.f1552a));
    }

    /* renamed from: a */
    public boolean a() {
        return this.f1553b.get() > 0;
    }

    /* renamed from: a */
    public void a(@NonNull List<Integer> list) {
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                if (this.f1552a.contains(Integer.valueOf(((Integer) it.next()).intValue()))) {
                    i = i2 + 1;
                } else {
                    i = i2;
                }
            } else {
                this.f1553b.addAndGet((long) i2);
                return;
            }
        }
    }

    /* renamed from: B */
    public void b(@NonNull List<Integer> list) {
        int i = 0;
        Iterator it = list.iterator();
        while (true) {
            int i2 = i;
            if (it.hasNext()) {
                if (this.f1552a.contains(Integer.valueOf(((Integer) it.next()).intValue()))) {
                    i = i2 + 1;
                } else {
                    i = i2;
                }
            } else {
                this.f1553b.addAndGet((long) (-i2));
                return;
            }
        }
    }
}
