package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.rl.a.Aa;

/* renamed from: com.yandex.metrica.impl.ob.qy */
public class qy extends qo {
    qy(@NonNull qn qnVar) {
        super(qnVar);
    }

    /* renamed from: a */
    public Aa mo1748a(@NonNull re reVar, @Nullable Aa aVar, @NonNull qm qmVar) {
        Aa a = qmVar.a();
        a.f2048d.f2051c = true;
        return mo1747a().a(reVar, a);
    }
}
