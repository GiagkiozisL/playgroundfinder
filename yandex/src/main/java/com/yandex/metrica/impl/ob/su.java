package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.su */
public class su extends sq {
    @Nullable

    /* renamed from: a */
    private List<String> f2509a;
    @Nullable

    /* renamed from: B */
    private List<String> f2510b;
    @Nullable

    /* renamed from: a */
    private String f2511c;
    @Nullable

    /* renamed from: d */
    private Map<String, String> f2512d;
    @Nullable

    /* renamed from: e */
    private List<String> f2513e;

    /* renamed from: f */
    private boolean f2514f;

    /* renamed from: g */
    private boolean f2515g;

    /* renamed from: h */
    private String f2516h;

    /* renamed from: i */
    private long f2517i;

    /* renamed from: com.yandex.metrica.impl.ob.su$a */
    public static class C0990a extends C0972a<C0990a, C0990a> implements sm<C0990a, C0990a> {
        @Nullable

        /* renamed from: a */
        public final String f2518a;
        @Nullable

        /* renamed from: B */
        public final Map<String, String> f2519b;

        /* renamed from: f */
        public final boolean f2520f;
        @Nullable

        /* renamed from: g */
        public final List<String> f2521g;

        public C0990a(@NonNull ed edVar) {
            this(edVar.mo744h().mo34d(), edVar.mo744h().mo43g(), edVar.mo744h().mo45h(), edVar.mo743g().mo754d(), edVar.mo743g().mo753c(), edVar.mo743g().mo750a(), edVar.mo743g().mo751b());
        }

        public C0990a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Map<String, String> map, boolean z, @Nullable List<String> list) {
            super(str, str2, str3);
            this.f2518a = str4;
            this.f2519b = map;
            this.f2520f = z;
            this.f2521g = list;
        }

        public C0990a() {
            this(null, null, null, null, null, false, null);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo2201a(@NonNull C0990a aVar) {
            return aVar.f2520f ? aVar.f2520f : this.f2520f;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public List<String> mo2202b(@NonNull C0990a aVar) {
            return aVar.f2520f ? aVar.f2521g : this.f2521g;
        }

        @NonNull
        /* renamed from: a */
        public C0990a b(@NonNull C0990a aVar) {
            return new C0990a((String) wk.m4369a(this.f2439c, aVar.f2439c), (String) wk.m4369a(this.f2440d, aVar.f2440d), (String) wk.m4369a(this.f2441e, aVar.f2441e), (String) wk.m4369a(this.f2518a, aVar.f2518a), (Map) wk.m4369a(this.f2519b, aVar.f2519b), mo2201a(aVar), mo2202b(aVar));
        }

        /* renamed from: d */
        public boolean a(@NonNull C0990a aVar) {
            return false;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.su$B */
    public static class C0991b extends C0980a<su, C0990a> {
        public C0991b(@NonNull Context context, @NonNull String str) {
            super(context, str);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public su b() {
            return new su();
        }

        /* renamed from: a */
        public su c(@NonNull c<C0990a> cVar) {
            su suVar = (su) super.c(cVar);
            mo2206a(suVar, cVar.f2444a);
            suVar.m3699m(wk.m4373b(((C0990a) cVar.b).f2518a, cVar.f2444a.distributionReferrer));
            suVar.mo2193a(((C0990a) cVar.b).f2519b);
            suVar.mo2197b(((C0990a) cVar.b).f2520f);
            suVar.mo2200c(((C0990a) cVar.b).f2521g);
            suVar.mo2194a(cVar.f2444a.hadFirstStartup);
            suVar.mo2191a(cVar.f2444a.countryInit);
            suVar.mo2190a(cVar.f2444a.firstStartupServerTime);
            return suVar;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo2206a(@NonNull su suVar, @NonNull uk ukVar) {
            suVar.mo2196b(ukVar.hostUrlsFromStartup);
            suVar.mo2192a(ukVar.hostUrlsFromClient);
        }
    }

    private su() {
        this.f2517i = 0;
    }

    /* renamed from: a */
    public List<String> mo2189a() {
        ArrayList arrayList = new ArrayList();
        if (!cx.a((Collection) this.f2509a)) {
            arrayList.addAll(this.f2509a);
        }
        if (!cx.a((Collection) this.f2510b)) {
            arrayList.addAll(this.f2510b);
        }
        arrayList.add("https://startup.mobile.yandex.net/");
        return arrayList;
    }

    /* renamed from: B */
    public boolean mo2198b() {
        return this.f2515g;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2194a(boolean z) {
        this.f2515g = z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2190a(long j) {
        if (this.f2517i == 0) {
            this.f2517i = j;
        }
    }

    /* renamed from: a */
    public long mo2199c() {
        return this.f2517i;
    }

    /* renamed from: B */
    public long mo2195b(long j) {
        mo2190a(j);
        return mo2199c();
    }

    /* renamed from: F */
    public List<String> mo2183F() {
        return this.f2510b;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2192a(@Nullable List<String> list) {
        this.f2510b = list;
    }

    @Nullable
    /* renamed from: G */
    public Map<String, String> mo2184G() {
        return this.f2512d;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2193a(@Nullable Map<String, String> map) {
        this.f2512d = map;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo2196b(@Nullable List<String> list) {
        this.f2509a = list;
    }

    @Nullable
    /* renamed from: H */
    public String mo2185H() {
        return this.f2511c;
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public void m3699m(@Nullable String str) {
        this.f2511c = str;
    }

    @Nullable
    /* renamed from: I */
    public List<String> mo2186I() {
        return this.f2513e;
    }

    /* renamed from: a */
    public void mo2200c(@Nullable List<String> list) {
        this.f2513e = list;
    }

    @Nullable
    /* renamed from: J */
    public boolean mo2187J() {
        return this.f2514f;
    }

    /* renamed from: B */
    public void mo2197b(boolean z) {
        this.f2514f = z;
    }

    /* renamed from: K */
    public String mo2188K() {
        return this.f2516h;
    }

    /* renamed from: a */
    public void mo2191a(String str) {
        this.f2516h = str;
    }

    public String toString() {
        return "StartupRequestConfig{mStartupHostsFromStartup=" + this.f2509a + ", mStartupHostsFromClient=" + this.f2510b + ", mDistributionReferrer='" + this.f2511c + '\'' + ", mClidsFromClient=" + this.f2512d + ", mNewCustomHosts=" + this.f2513e + ", mHasNewCustomHosts=" + this.f2514f + ", mSuccessfulStartup=" + this.f2515g + ", mCountryInit='" + this.f2516h + '\'' + ", mFirstStartupTime='" + this.f2517i + '\'' + '}';
    }
}
