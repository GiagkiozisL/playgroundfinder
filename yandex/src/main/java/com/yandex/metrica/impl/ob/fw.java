package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.fw */
public class fw extends fl {
    fw(@NonNull Context context, @NonNull fc fcVar) {
        super(context, fcVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public void mo901b(@NonNull w wVar, @NonNull eg egVar) {
        mo928a(wk.m4371a(egVar.f766b.f772e, true));
        mo900b().mo877a(wVar, egVar);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo928a(boolean z) {
        mo902c().mo1617a(z);
    }
}
