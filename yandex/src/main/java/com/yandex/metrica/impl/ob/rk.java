package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;
import java.util.Arrays;

/* renamed from: com.yandex.metrica.impl.ob.rk */
public final class rk extends e {

    /* renamed from: B */
    public int f2035b;

    /* renamed from: a */
    public double f2036c;

    /* renamed from: d */
    public byte[] f2037d;

    /* renamed from: e */
    public byte[] f2038e;

    /* renamed from: f */
    public byte[] f2039f;

    /* renamed from: g */
    public C0839a f2040g;

    /* renamed from: h */
    public long f2041h;

    /* renamed from: com.yandex.metrica.impl.ob.rk$a */
    public static final class C0839a extends e {

        /* renamed from: B */
        public byte[] f2042b;

        /* renamed from: a */
        public byte[] f2043c;

        public C0839a() {
            mo1831d();
        }

        /* renamed from: d */
        public C0839a mo1831d() {
            this.f2042b = g.h;
            this.f2043c = g.h;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (!Arrays.equals(this.f2042b, g.h)) {
                bVar.mo353a(1, this.f2042b);
            }
            if (!Arrays.equals(this.f2043c, g.h)) {
                bVar.mo353a(2, this.f2043c);
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (!Arrays.equals(this.f2042b, g.h)) {
                c += b.b(1, this.f2042b);
            }
            if (!Arrays.equals(this.f2043c, g.h)) {
                return c + b.b(2, this.f2043c);
            }
            return c;
        }

        /* renamed from: B */
        public C0839a mo738a(a aVar) throws IOException {
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        this.f2042b = aVar.mo230j();
                        continue;
                    case 18:
                        this.f2043c = aVar.mo230j();
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }

    public rk() {
        mo1829d();
    }

    /* renamed from: d */
    public rk mo1829d() {
        this.f2035b = 1;
        this.f2036c = 0.0d;
        this.f2037d = g.h;
        this.f2038e = g.h;
        this.f2039f = g.h;
        this.f2040g = null;
        this.f2041h = 0;
        this.f754a = -1;
        return this;
    }

    /* renamed from: a */
    public void mo739a(b bVar) throws IOException {
        if (this.f2035b != 1) {
            bVar.mo361b(1, this.f2035b);
        }
        if (Double.doubleToLongBits(this.f2036c) != Double.doubleToLongBits(0.0d)) {
            bVar.mo346a(2, this.f2036c);
        }
        bVar.mo353a(3, this.f2037d);
        if (!Arrays.equals(this.f2038e, g.h)) {
            bVar.mo353a(4, this.f2038e);
        }
        if (!Arrays.equals(this.f2039f, g.h)) {
            bVar.mo353a(5, this.f2039f);
        }
        if (this.f2040g != null) {
            bVar.mo350a(6, (e) this.f2040g);
        }
        if (this.f2041h != 0) {
            bVar.b(7, this.f2041h);
        }
        super.mo739a(bVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo741c() {
        int c = super.mo741c();
        if (this.f2035b != 1) {
            c += b.m449e(1, this.f2035b);
        }
        if (Double.doubleToLongBits(this.f2036c) != Double.doubleToLongBits(0.0d)) {
            c += b.m434b(2, this.f2036c);
        }
        int b = c + com.yandex.metrica.impl.ob.b.b(3, this.f2037d);
        if (!Arrays.equals(this.f2038e, g.h)) {
            b += com.yandex.metrica.impl.ob.b.b(4, this.f2038e);
        }
        if (!Arrays.equals(this.f2039f, g.h)) {
            b += com.yandex.metrica.impl.ob.b.b(5, this.f2039f);
        }
        if (this.f2040g != null) {
            b += com.yandex.metrica.impl.ob.b.b(6, (e) this.f2040g);
        }
        if (this.f2041h != 0) {
            return b + com.yandex.metrica.impl.ob.b.m450e(7, this.f2041h);
        }
        return b;
    }

    /* renamed from: B */
    public rk mo738a(a aVar) throws IOException {
        while (true) {
            int a = aVar.mo213a();
            switch (a) {
                case 0:
                    break;
                case 8:
                    this.f2035b = aVar.mo231k();
                    continue;
                case 17:
                    this.f2036c = aVar.mo218c();
                    continue;
                case 26:
                    this.f2037d = aVar.mo230j();
                    continue;
                case 34:
                    this.f2038e = aVar.mo230j();
                    continue;
                case 42:
                    this.f2039f = aVar.mo230j();
                    continue;
                case 50:
                    if (this.f2040g == null) {
                        this.f2040g = new C0839a();
                    }
                    aVar.mo215a((e) this.f2040g);
                    continue;
                case 56:
                    this.f2041h = aVar.mo223f();
                    continue;
                default:
                    if (!g.a(aVar, a)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
//        return this;
    }
}
