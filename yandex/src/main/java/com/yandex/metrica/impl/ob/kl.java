package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.kl */
public class kl {
    @Nullable

    /* renamed from: a */
    public final String f1216a;
    @Nullable

    /* renamed from: B */
    public final Throwable f1217b;
    @Nullable

    /* renamed from: a */
    public final kg f1218c;
    @Nullable

    /* renamed from: d */
    public final String f1219d;
    @Nullable

    /* renamed from: e */
    public final Boolean f1220e;

    public kl(@Nullable Throwable th, @Nullable kg kgVar, @Nullable String str, @Nullable Boolean bool) {
        this.f1217b = th;
        if (th == null) {
            this.f1216a = "";
        } else {
            this.f1216a = th.getClass().getName();
        }
        this.f1218c = kgVar;
        this.f1219d = str;
        this.f1220e = bool;
    }

    @Nullable
    @Deprecated
    /* renamed from: a */
    public Throwable mo1148a() {
        return this.f1217b;
    }

    public String toString() {
        StackTraceElement[] b;
        StringBuilder sb = new StringBuilder();
        if (this.f1217b != null) {
            for (StackTraceElement stackTraceElement : cx.b(this.f1217b)) {
                sb.append("at " + stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "(" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + ")\n");
            }
        }
        return "UnhandledException{errorName='" + this.f1216a + '\'' + ", exception=" + this.f1217b + "\n" + sb.toString() + '}';
    }
}
