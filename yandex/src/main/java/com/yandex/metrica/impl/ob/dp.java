package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.dp */
public class dp implements wl<Thread, StackTraceElement[], kk> {
    @NonNull
    /* renamed from: a */
    public kk a(@NonNull Thread thread, @Nullable StackTraceElement[] stackTraceElementArr) {
        List asList;
        String name = thread.getName();
        int priority = thread.getPriority();
        long id = thread.getId();
        String a = m1362a(thread);
        Integer valueOf = Integer.valueOf(thread.getState().ordinal());
        if (stackTraceElementArr == null) {
            asList = null;
        } else {
            asList = Arrays.asList(stackTraceElementArr);
        }
        return new kk(name, priority, id, a, valueOf, asList);
    }

    @NonNull
    /* renamed from: a */
    static String m1362a(@NonNull Thread thread) {
        ThreadGroup threadGroup = thread.getThreadGroup();
        return threadGroup != null ? threadGroup.getName() : "";
    }
}
