package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.rg.C0815f;

import java.util.Arrays;

/* renamed from: com.yandex.metrica.impl.ob.ks */
public class ks implements mq<Throwable, C0815f> {
    @NonNull

    /* renamed from: a */
    private kp f1226a = new kp();

    @NonNull
    /* renamed from: a */
    public C0815f b(@NonNull Throwable th) {
        return m2049a(th, 1, 0);
    }

    @NonNull
    /* renamed from: a */
    private C0815f m2049a(@NonNull Throwable th, int i, int i2) {
        C0815f fVar = new C0815f();
        fVar.f1903b = th.getClass().getName();
        fVar.f1904c = wk.m4373b(th.getMessage(), "");
        fVar.f1905d = this.f1226a.b(Arrays.asList(cx.b(th)));
        if (th.getCause() != null && i2 < i) {
            fVar.f1906e = m2049a(th.getCause(), 30, i2 + 1);
        }
        if (!cx.a(19) || i2 >= i) {
            fVar.f1907f = new C0815f[0];
        } else {
            m2050a(fVar, th.getSuppressed(), i2);
        }
        return fVar;
    }

    @NonNull
    /* renamed from: a */
    public Throwable a(@NonNull C0815f fVar) {
        throw new UnsupportedOperationException();
    }

    @TargetApi(19)
    /* renamed from: a */
    private void m2050a(@NonNull C0815f fVar, @Nullable Throwable[] thArr, int i) {
        if (thArr == null) {
            fVar.f1907f = new C0815f[0];
            return;
        }
        fVar.f1907f = new C0815f[thArr.length];
        int length = thArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            int i4 = i3 + 1;
            fVar.f1907f[i3] = m2049a(thArr[i2], 1, i + 1);
            i2++;
            i3 = i4;
        }
    }
}
