package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.PreloadInfo;

import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.bu */
public class bu {

    /* renamed from: a */
    private PreloadInfo f423a;

    public bu(PreloadInfo preloadInfo, @NonNull vz vzVar) {
        if (preloadInfo == null) {
            return;
        }
        if (!TextUtils.isEmpty(preloadInfo.getTrackingId())) {
            this.f423a = preloadInfo;
        } else if (vzVar.mo2485c()) {
            vzVar.mo2483c("Required field \"PreloadInfo.trackingId\" is empty!\nThis preload info will be skipped.");
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public String mo498a() {
        if (this.f423a == null) {
            return "";
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("preloadInfo", mo499b());
        } catch (Throwable th) {
        }
        return jSONObject.toString();
    }

    @Nullable
    /* renamed from: B */
    public JSONObject mo499b() {
        if (this.f423a == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("trackingId", this.f423a.getTrackingId());
            if (this.f423a.getAdditionalParams().isEmpty()) {
                return jSONObject;
            }
            jSONObject.put("additionalParams", new JSONObject(this.f423a.getAdditionalParams()));
            return jSONObject;
        } catch (Throwable th) {
            return jSONObject;
        }
    }
}
