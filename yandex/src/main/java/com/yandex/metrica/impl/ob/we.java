package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.we */
public class we {
    @NonNull
    /* renamed from: a */
    public static String m4339a(Map<String, String> map) {
        String str = "";
        if (cx.m1193a((Map) map)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (Entry entry : map.entrySet()) {
            if (!TextUtils.isEmpty((CharSequence) entry.getKey())) {
                sb.append((String) entry.getKey()).append(":").append((String) entry.getValue()).append(",");
            }
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    @NonNull
    /* renamed from: a */
    public static Map<String, String> m4340a(@Nullable String str) {
        String[] split;
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(str)) {
            for (String str2 : str.split(",")) {
                int indexOf = str2.indexOf(":");
                if (indexOf != -1) {
                    String substring = str2.substring(0, indexOf);
                    if (!TextUtils.isEmpty(substring)) {
                        hashMap.put(substring, str2.substring(indexOf + 1));
                    }
                }
            }
        }
        return hashMap;
    }

    /* renamed from: B */
    public static boolean m4341b(String str) {
        return m4342b(m4340a(str));
    }

    /* renamed from: B */
    public static boolean m4342b(Map<String, String> map) {
        if (map.isEmpty()) {
            return false;
        }
        for (Entry value : map.entrySet()) {
            try {
                Integer.parseInt((String) value.getValue());
            } catch (Throwable th) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static Map<String, String> m4343c(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                if (m4345d((String) entry.getKey()) && m4344c((String) entry.getValue())) {
                    hashMap.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return hashMap;
    }

    /* renamed from: a */
    private static boolean m4344c(String str) {
        return !TextUtils.isEmpty(str) && vy.a(str, -1) != -1;
    }

    /* renamed from: d */
    private static boolean m4345d(String str) {
        return !TextUtils.isEmpty(str) && !str.contains(":") && !str.contains(",") && !str.contains("&");
    }
}
