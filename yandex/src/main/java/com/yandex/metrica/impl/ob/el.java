package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: com.yandex.metrica.impl.ob.el */
public class el<CU extends fp> {

    /* renamed from: a */
    private final List<CU> f803a = new CopyOnWriteArrayList();

    /* renamed from: a */
    public void mo797a(@NonNull CU cu) {
        this.f803a.add(cu);
    }

    /* renamed from: B */
    public void mo798b(@NonNull CU cu) {
        this.f803a.remove(cu);
    }

    /* renamed from: a */
    public List<CU> mo796a() {
        return this.f803a;
    }
}
