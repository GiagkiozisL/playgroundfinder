package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.io */
public class io {

    /* renamed from: a */
    private final int f1051a;
    @NonNull

    /* renamed from: B */
    private final is f1052b;
    @Nullable

    /* renamed from: a */
    private ip f1053c;

    public io(@NonNull Context context, @NonNull ek ekVar, int i) {
        this(new is(context, ekVar), i);
    }

    @NonNull
    /* renamed from: a */
    public aj mo1009a(@NonNull String str) {
        if (this.f1053c == null) {
            m1819b();
        }
        int b = m1818b(str);
        if (this.f1053c.mo1016c().contains(Integer.valueOf(b))) {
            return aj.NON_FIRST_OCCURENCE;
        }
        aj ajVar = this.f1053c.mo1015b() ? aj.FIRST_OCCURRENCE : aj.UNKNOWN;
        if (this.f1053c.mo1017d() < 1000) {
            this.f1053c.mo1014b(b);
        } else {
            this.f1053c.mo1013a(false);
        }
        m1820c();
        return ajVar;
    }

    /* renamed from: a */
    public void mo1010a() {
        if (this.f1053c == null) {
            m1819b();
        }
        this.f1053c.mo1011a();
        this.f1053c.mo1013a(true);
        m1820c();
    }

    /* renamed from: B */
    private void m1819b() {
        this.f1053c = this.f1052b.mo1028a();
        if (this.f1053c.mo1018e() != this.f1051a) {
            this.f1053c.mo1012a(this.f1051a);
            m1820c();
        }
    }

    /* renamed from: a */
    private void m1820c() {
        this.f1052b.mo1029a(this.f1053c);
    }

    /* renamed from: B */
    private int m1818b(@NonNull String str) {
        return str.hashCode();
    }

    @VisibleForTesting
    io(@NonNull is isVar, int i) {
        this.f1051a = i;
        this.f1052b = isVar;
    }
}
