package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.yandex.metrica.impl.ob.bh */
class bh implements UncaughtExceptionHandler {

    /* renamed from: a */
    public static final AtomicBoolean f355a = new AtomicBoolean();

    /* renamed from: B */
    private final CopyOnWriteArrayList<ki> f356b;

    /* renamed from: a */
    private final UncaughtExceptionHandler f357c;

    /* renamed from: d */
    private final dq f358d;
    @NonNull

    /* renamed from: e */
    private final ag f359e;

    public bh(UncaughtExceptionHandler uncaughtExceptionHandler, @NonNull Context context) {
        this(uncaughtExceptionHandler, new ag(context));
    }

    @VisibleForTesting
    bh(UncaughtExceptionHandler uncaughtExceptionHandler, @NonNull ag agVar) {
        this.f358d = new dq();
        this.f356b = new CopyOnWriteArrayList<>();
        this.f357c = uncaughtExceptionHandler;
        this.f359e = agVar;
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        try {
            f355a.set(true);
            mo422a(new kl(ex, new kg(new doo().mo714a(thread), this.f358d.mo718a(thread)), this.f359e.mo261a(), this.f359e.mo262b()));
        } finally {
            if (this.f357c != null) {
                this.f357c.uncaughtException(thread, ex);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo422a(@NonNull kl klVar) {
        Iterator it = this.f356b.iterator();
        while (it.hasNext()) {
            ((ki) it.next()).mo1146a(klVar);
        }
    }

    /* renamed from: a */
    public void mo421a(ki kiVar) {
        this.f356b.add(kiVar);
    }
}
