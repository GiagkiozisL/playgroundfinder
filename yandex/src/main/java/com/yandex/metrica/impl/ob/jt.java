package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.jt */
public class jt {
    @NonNull

    /* renamed from: a */
    private final xh f1168a;
    @NonNull

    /* renamed from: B */
    private final jp f1169b;

    /* renamed from: com.yandex.metrica.impl.ob.jt$a */
    public interface C0513a {
        /* renamed from: a */
        void mo1128a();
    }

    public jt(@NonNull xh xhVar, @NonNull jp jpVar) {
        this.f1168a = xhVar;
        this.f1169b = jpVar;
    }

    /* renamed from: a */
    public void mo1125a(long j, @NonNull final C0513a aVar) {
        this.f1168a.a(new Runnable() {
            public void run() {
                try {
                    aVar.mo1128a();
                } catch (Throwable th) {
                }
            }
        }, j);
    }

    /* renamed from: a */
    public void mo1126a(long j, boolean z) {
        this.f1169b.a(j, z);
    }

    /* renamed from: a */
    public void mo1124a() {
        this.f1169b.a();
    }
}
