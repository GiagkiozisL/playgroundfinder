package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.xg */
public class xg implements xi {
    @NonNull

    /* renamed from: a */
    private final Looper f2967a;
    @NonNull

    /* renamed from: B */
    private final Handler f2968b;
    @NonNull

    /* renamed from: a */
    private final xk f2969c;

    public xg(@NonNull String str) {
        this(m4424a(str));
    }

    @NonNull
    /* renamed from: a */
    public Handler a() {
        return this.f2968b;
    }

    @NonNull
    /* renamed from: B */
    public Looper b() {
        return this.f2967a;
    }

    /* renamed from: a */
    public void a(@NonNull Runnable runnable) {
        this.f2968b.post(runnable);
    }

    /* renamed from: a */
    public <T> Future<T> a(Callable<T> callable) {
        FutureTask futureTask = new FutureTask(callable);
        a((Runnable) futureTask);
        return futureTask;
    }

    /* renamed from: a */
    public void a(@NonNull Runnable runnable, long j) {
        mo2607a(runnable, j, TimeUnit.MILLISECONDS);
    }

    /* renamed from: a */
    public void mo2607a(@NonNull Runnable runnable, long j, @NonNull TimeUnit timeUnit) {
        this.f2968b.postDelayed(runnable, timeUnit.toMillis(j));
    }

    /* renamed from: B */
    public void b(@NonNull Runnable runnable) {
        this.f2968b.removeCallbacks(runnable);
    }

    /* renamed from: a */
    public boolean c() {
        return this.f2969c.c();
    }

    /* renamed from: a */
    private static xk m4424a(@NonNull String str) {
        xk a = new xm(str).mo2612a();
        a.start();
        return a;
    }

    @VisibleForTesting
    xg(@NonNull xk xkVar) {
        this(xkVar, xkVar.getLooper(), new Handler(xkVar.getLooper()));
    }

    @VisibleForTesting
    public xg(@NonNull xk xkVar, @NonNull Looper looper, @NonNull Handler handler) {
        this.f2969c = xkVar;
        this.f2967a = looper;
        this.f2968b = handler;
    }
}
