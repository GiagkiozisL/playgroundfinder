package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.ro.C0852a;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.md */
public class md extends mb<C0852a> {
    @NonNull
    /* renamed from: a */
    public C0852a b(@NonNull byte[] bArr) throws IOException {
        return C0852a.m3232a(bArr);
    }

    @NonNull
    /* renamed from: a */
    public C0852a c() {
        return new C0852a();
    }
}
