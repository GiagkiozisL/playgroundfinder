package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanFilter.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.tt.a;
import com.yandex.metrica.impl.ob.tt.a.C1041a;
import com.yandex.metrica.impl.ob.tt.a.C1042b;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.di */
public class di {
    @NonNull
    /* renamed from: a */
    public List<ScanFilter> mo703a(@NonNull List<a> list) {
        ArrayList arrayList = new ArrayList();
        for (tt.a a : list) {
            ScanFilter a2 = m1338a(a);
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return arrayList;
    }

    @Nullable
    /* renamed from: a */
    private ScanFilter m1338a(@NonNull a aVar) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = true;
        boolean z5 = false;
        Builder builder = new Builder();
        if (!TextUtils.isEmpty(aVar.f2620b)) {
            builder.setDeviceName(aVar.f2620b);
            z = false;
        } else {
            z = true;
        }
        if (!TextUtils.isEmpty(aVar.f2619a) && BluetoothAdapter.checkBluetoothAddress(aVar.f2619a.toUpperCase(Locale.US))) {
            builder.setDeviceAddress(aVar.f2619a);
            z = false;
        }
        if (aVar.f2621c != null) {
            z2 = false;
            z3 = m1339a(builder, aVar.f2621c);
        } else {
            z2 = z;
            z3 = true;
        }
        if (aVar.f2622d != null) {
            if (!z3 || !m1340a(builder, aVar.f2622d)) {
                z4 = false;
            }
            z2 = false;
        } else {
            z4 = z3;
        }
        if (aVar.f2623e != null) {
            builder.setServiceUuid(aVar.f2623e.f2630a, aVar.f2623e.f2631b);
        } else {
            z5 = z2;
        }
        if (!z4 || z5) {
            return null;
        }
        return builder.build();
    }

    /* renamed from: a */
    private boolean m1339a(@NonNull Builder builder, @NonNull C1041a aVar) {
        if (aVar.f2624a < 0) {
            return false;
        }
        if ((aVar.f2625b == null && aVar.f2626c != null) || m1341a(aVar.f2625b, aVar.f2626c)) {
            return false;
        }
        builder.setManufacturerData(aVar.f2624a, aVar.f2625b, aVar.f2626c);
        return true;
    }

    /* renamed from: a */
    private boolean m1340a(@NonNull Builder builder, @NonNull C1042b bVar) {
        if (bVar.f2627a == null) {
            return false;
        }
        if ((bVar.f2628b == null && bVar.f2629c != null) || m1341a(bVar.f2628b, bVar.f2629c)) {
            return false;
        }
        builder.setServiceData(bVar.f2627a, bVar.f2628b, bVar.f2629c);
        return true;
    }

    /* renamed from: a */
    private boolean m1341a(byte[] bArr, byte[] bArr2) {
        return (bArr == null || bArr2 == null || bArr.length == bArr2.length) ? false : true;
    }
}
