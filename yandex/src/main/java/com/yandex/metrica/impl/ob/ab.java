package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ab */
public class ab implements bn {
    @NonNull

    /* renamed from: a */
    private final C0050a f145a;

    /* renamed from: com.yandex.metrica.impl.ob.ab$a */
    static class C0050a {

        /* renamed from: com.yandex.metrica.impl.ob.ab$a$a */
        static class C0051a {
            @NonNull

            /* renamed from: a */
            final String f146a;

            public C0051a(@NonNull String str) {
                this.f146a = str;
            }
        }

        C0050a() {
        }

        /* renamed from: a */
        public C0051a mo252a(@Nullable byte[] bArr) {
            try {
                if (!cx.nullOrEmpty(bArr)) {
                    return new C0051a(new JSONObject(new String(bArr, "UTF-8")).optString("status"));
                }
                return null;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    public ab() {
        this(new C0050a());
    }

    @VisibleForTesting
    ab(@NonNull C0050a aVar) {
        this.f145a = aVar;
    }

    /* renamed from: a */
    public boolean a(int i, @Nullable byte[] bArr, @Nullable Map<String, List<String>> map) {
        if (200 != i) {
            return false;
        }
        C0050a.C0051a a = this.f145a.mo252a(bArr);
        if (a != null) {
            return "accepted".equals(a.f146a);
        }
        return false;
    }
}
