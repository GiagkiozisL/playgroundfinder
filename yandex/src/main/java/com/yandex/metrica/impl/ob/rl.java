package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;
import java.util.Arrays;

/* renamed from: com.yandex.metrica.impl.ob.rl */
public interface rl {

    /* renamed from: com.yandex.metrica.impl.ob.rl$a */
    public static final class a extends e {

        /* renamed from: B */
        public Aa[] b;

        /* renamed from: com.yandex.metrica.impl.ob.rl$a$a */
        public static final class Aa extends e {

            /* renamed from: f */
            private static volatile Aa[] f2045f;

            /* renamed from: B */
            public byte[] f2046b;

            /* renamed from: a */
            public int f2047c;

            /* renamed from: d */
            public Bb f2048d;

            /* renamed from: e */
            public Cc f2049e;

            /* renamed from: d */
            public static Aa[] m3193d() {
                if (f2045f == null) {
                    synchronized (c.a) {
                        if (f2045f == null) {
                            f2045f = new Aa[0];
                        }
                    }
                }
                return f2045f;
            }

            public Aa() {
                mo1835e();
            }

            /* renamed from: e */
            public Aa mo1835e() {
                this.f2046b = g.h;
                this.f2047c = 0;
                this.f2048d = null;
                this.f2049e = null;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                bVar.mo353a(1, this.f2046b);
                bVar.mo348a(2, this.f2047c);
                if (this.f2048d != null) {
                    bVar.mo350a(3, (e) this.f2048d);
                }
                if (this.f2049e != null) {
                    bVar.mo350a(4, (e) this.f2049e);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int var1 = super.mo741c();
                var1 += com.yandex.metrica.impl.ob.b.b(1, this.f2046b);
                var1 += com.yandex.metrica.impl.ob.b.d(2, this.f2047c);
                if (this.f2048d != null) {
                    var1 += com.yandex.metrica.impl.ob.b.b(3, this.f2048d);
                }

                if (this.f2049e != null) {
                    var1 += com.yandex.metrica.impl.ob.b.b(4, this.f2049e);
                }

                return var1;
//                int a = super.mo741c() + b.b(1, this.f2046b) + b.m445d(2, this.f2047c);
//                if (this.f2048d != null) {
//                    a += b.b(3, (e) this.f2048d);
//                }
//                if (this.f2049e != null) {
//                    return a + b.b(4, (e) this.f2049e);
//                }
//                return a;
            }

            /* renamed from: B */
            public Aa mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2046b = aVar.mo230j();
                            continue;
                        case 16:
                            int g = aVar.mo225g();
                            switch (g) {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                    this.f2047c = g;
                                    break;
                                default:
                                    continue;
                            }
                        case 26:
                            if (this.f2048d == null) {
                                this.f2048d = new Bb();
                            }
                            aVar.mo215a((e) this.f2048d);
                            continue;
                        case 34:
                            if (this.f2049e == null) {
                                this.f2049e = new Cc();
                            }
                            aVar.mo215a((e) this.f2049e);
                            continue;
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rl$a$B */
        public static final class Bb extends e {

            /* renamed from: B */
            public boolean f2050b;

            /* renamed from: a */
            public boolean f2051c;

            public Bb() {
                mo1837d();
            }

            /* renamed from: d */
            public Bb mo1837d() {
                this.f2050b = false;
                this.f2051c = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (this.f2050b) {
                    bVar.mo352a(1, this.f2050b);
                }
                if (this.f2051c) {
                    bVar.mo352a(2, this.f2051c);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (this.f2050b) {
                    c += com.yandex.metrica.impl.ob.b.m438b(1, this.f2050b);
                }
                if (this.f2051c) {
                    return c + com.yandex.metrica.impl.ob.b.m438b(2, this.f2051c);
                }
                return c;
            }

            /* renamed from: B */
            public Bb mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 8:
                            this.f2050b = aVar.mo228h();
                            continue;
                        case 16:
                            this.f2051c = aVar.mo228h();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rl$a$a */
        public static final class Cc extends e {

            /* renamed from: B */
            public byte[] f2052b;

            /* renamed from: a */
            public double f2053c;

            /* renamed from: d */
            public double f2054d;

            /* renamed from: e */
            public boolean f2055e;

            public Cc() {
                mo1839d();
            }

            /* renamed from: d */
            public Cc mo1839d() {
                this.f2052b = g.h;
                this.f2053c = 0.0d;
                this.f2054d = 0.0d;
                this.f2055e = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
                if (!Arrays.equals(this.f2052b, g.h)) {
                    bVar.mo353a(1, this.f2052b);
                }
                if (Double.doubleToLongBits(this.f2053c) != Double.doubleToLongBits(0.0d)) {
                    bVar.mo346a(2, this.f2053c);
                }
                if (Double.doubleToLongBits(this.f2054d) != Double.doubleToLongBits(0.0d)) {
                    bVar.mo346a(3, this.f2054d);
                }
                if (this.f2055e) {
                    bVar.mo352a(4, this.f2055e);
                }
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                int c = super.mo741c();
                if (!Arrays.equals(this.f2052b, g.h)) {
                    c += com.yandex.metrica.impl.ob.b.b(1, this.f2052b);
                }
                if (Double.doubleToLongBits(this.f2053c) != Double.doubleToLongBits(0.0d)) {
                    c += com.yandex.metrica.impl.ob.b.m434b(2, this.f2053c);
                }
                if (Double.doubleToLongBits(this.f2054d) != Double.doubleToLongBits(0.0d)) {
                    c += com.yandex.metrica.impl.ob.b.m434b(3, this.f2054d);
                }
                if (this.f2055e) {
                    return c + com.yandex.metrica.impl.ob.b.m438b(4, this.f2055e);
                }
                return c;
            }

            /* renamed from: B */
            public Cc mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2052b = aVar.mo230j();
                            continue;
                        case 17:
                            this.f2053c = aVar.mo218c();
                            continue;
                        case 25:
                            this.f2054d = aVar.mo218c();
                            continue;
                        case 32:
                            this.f2055e = aVar.mo228h();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public a() {
            mo1833d();
        }

        /* renamed from: d */
        public a mo1833d() {
            this.b = Aa.m3193d();
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(com.yandex.metrica.impl.ob.b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (Aa aVar : this.b) {
                    if (aVar != null) {
                        bVar.mo350a(1, (e) aVar);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.b != null && this.b.length > 0) {
                for (Aa aVar : this.b) {
                    if (aVar != null) {
                        c += com.yandex.metrica.impl.ob.b.b(1, (e) aVar);
                    }
                }
            }
            return c;
        }

        /* renamed from: B */
        public a mo738a(com.yandex.metrica.impl.ob.a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        int b = g.b(aVar, 10);
                        if (this.b == null) {
                            length = 0;
                        } else {
                            length = this.b.length;
                        }
                        Aa[] aVarArr = new Aa[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.b, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new Aa();
                            aVar.mo215a((e) aVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        aVarArr[length] = new Aa();
                        aVar.mo215a((e) aVarArr[length]);
                        this.b = aVarArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }
    }
}
