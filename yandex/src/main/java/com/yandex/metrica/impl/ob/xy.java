package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.io.UnsupportedEncodingException;

/* renamed from: com.yandex.metrica.impl.ob.xy */
public class xy extends xq<String> {
    @VisibleForTesting(otherwise = 3)
    /* renamed from: a */
    public /* bridge */ /* synthetic */ int mo2635a() {
        return super.mo2635a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    /* renamed from: B */
    public /* bridge */ /* synthetic */ String mo2636b() {
        return super.mo2636b();
    }

    public xy(int i, @NonNull String str) {
        this(i, str, vr.m4236a());
    }

    public xy(int i, @NonNull String str, @NonNull vz vzVar) {
        super(i, str, vzVar);
    }

    @Nullable
    /* renamed from: a */
    public String a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            byte[] bytes = str.getBytes("UTF-8");
            if (bytes.length <= mo2635a()) {
                return str;
            }
            String str2 = new String(bytes, 0, mo2635a(), "UTF-8");
            try {
                if (this.f2984a.mo2485c()) {
                    this.f2984a.mo2482b("\"%s\" %s exceeded limit of %d bytes", mo2636b(), str, Integer.valueOf(mo2635a()));
                }
                return str2;
            } catch (Exception e) {
                return str2;
            }
        } catch (UnsupportedEncodingException e2) {
            return str;
        }
    }
}
