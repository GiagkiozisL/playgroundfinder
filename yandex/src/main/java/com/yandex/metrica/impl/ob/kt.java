package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.rg.C0812c;

/* renamed from: com.yandex.metrica.impl.ob.kt */
public class kt implements mq<kl, C0812c> {
    @NonNull

    /* renamed from: a */
    private final ks f1227a;
    @NonNull

    /* renamed from: B */
    private final km f1228b;
    @NonNull

    /* renamed from: a */
    private final ko f1229c;

    public kt() {
        this(new ks(), new km(new kr()), new ko());
    }

    @VisibleForTesting
    kt(@NonNull ks ksVar, @NonNull km kmVar, @NonNull ko koVar) {
        this.f1228b = kmVar;
        this.f1227a = ksVar;
        this.f1229c = koVar;
    }

    @NonNull
    /* renamed from: a */
    public C0812c b(@NonNull kl klVar) {
        C0812c cVar = new C0812c();
        if (klVar.f1217b != null) {
            cVar.f1885b = this.f1227a.b(klVar.f1217b);
        }
        if (klVar.f1218c != null) {
            cVar.f1886c = this.f1228b.b(klVar.f1218c);
        }
        if (klVar.f1219d != null) {
            cVar.f1887d = klVar.f1219d;
        }
        cVar.f1888e = this.f1229c.mo1154a(klVar.f1220e).intValue();
        return cVar;
    }

    @NonNull
    /* renamed from: a */
    public kl a(@NonNull C0812c cVar) {
        throw new UnsupportedOperationException();
    }
}
