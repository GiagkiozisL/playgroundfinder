package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.qj */
public class qj extends qd {

    /* renamed from: d */
    private qk f1851d;

    public qj(Context context) {
        this(context, null);
    }

    public qj(Context context, String str) {
        super(context, str);
        this.f1851d = new qk("LOCATION_TRACKING_ENABLED");
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String mo1693f() {
        return "_serviceproviderspreferences";
    }

    /* renamed from: a */
    public boolean mo1684a() {
        return this.f1766c.getBoolean(this.f1851d.mo1744b(), false);
    }

    /* renamed from: B */
    public void mo1741b() {
        mo1697h(this.f1851d.mo1744b()).mo1699j();
    }
}
