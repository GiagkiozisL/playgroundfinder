package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.hx */
public class hx {

    /* renamed from: a */
    private final gw f1021a;

    /* renamed from: B */
    private final gx f1022b;

    /* renamed from: a */
    private final gv f1023c;

    /* renamed from: d */
    private final gy f1024d;

    public hx(ei eiVar) {
        this.f1021a = new gw(eiVar);
        this.f1022b = new gx(eiVar);
        this.f1023c = new gv(eiVar);
        this.f1024d = new gy(eiVar, al.m324a().mo290h(), new ox(oo.m2694a(eiVar.mo785d()), al.m324a().mo293k(), cy.m1204a(eiVar.mo785d()), new ly(ld.m2146a(eiVar.mo785d()).mo1238c())));
    }

    /* renamed from: a */
    public gw mo980a() {
        return this.f1021a;
    }

    /* renamed from: B */
    public gv mo981b() {
        return this.f1023c;
    }

    /* renamed from: a */
    public gx mo982c() {
        return this.f1022b;
    }

    /* renamed from: d */
    public gt mo983d() {
        return this.f1024d;
    }
}
