package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.ov */
class ov extends of {
    @NonNull

    /* renamed from: a */
    private lh f1679a;
    @NonNull

    /* renamed from: B */
    private og f1680b;
    @NonNull

    /* renamed from: a */
    private wg f1681c;

    public ov(@NonNull Context context, @Nullable oe oeVar) {
        this(oeVar, ld.m2146a(context).mo1242g(), new og(context), new wg());
    }

    /* renamed from: B */
    public void mo1541b(@Nullable String str, @Nullable Location location, @Nullable oh ohVar) {
        if (ohVar != null && location != null) {
            os osVar = new os(ohVar.mo1536a(), this.f1681c.a(), this.f1681c.c(), location);
            String a = this.f1680b.mo1544a(osVar);
            if (!TextUtils.isEmpty(a)) {
                this.f1679a.mo1184b(osVar.mo1596b(), a);
            }
        }
    }

    ov(@Nullable oe oeVar, @NonNull lh lhVar, @NonNull og ogVar, @NonNull wg wgVar) {
        super(oeVar);
        this.f1679a = lhVar;
        this.f1680b = ogVar;
        this.f1681c = wgVar;
    }
}
