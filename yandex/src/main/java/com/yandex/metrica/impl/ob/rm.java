package com.yandex.metrica.impl.ob;

import com.yandex.metrica.YandexMetricaDefaultValues;

import java.io.IOException;

/* renamed from: com.yandex.metrica.impl.ob.rm */
public interface rm {

    /* renamed from: com.yandex.metrica.impl.ob.rm$a */
    public static final class C0846a extends e {

        /* renamed from: B */
        public C0848b[] f2056b;

        /* renamed from: a */
        public C0847a f2057c;

        /* renamed from: d */
        public String[] f2058d;

        /* renamed from: com.yandex.metrica.impl.ob.rm$a$a */
        public static final class C0847a extends e {

            /* renamed from: B */
            public int f2059b;

            /* renamed from: a */
            public int f2060c;

            public C0847a() {
                mo1843d();
            }

            /* renamed from: d */
            public C0847a mo1843d() {
                this.f2059b = 0;
                this.f2060c = -1;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(b bVar) throws IOException {
                bVar.mo348a(2, this.f2059b);
                bVar.mo348a(3, this.f2060c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + b.m445d(2, this.f2059b) + b.m445d(3, this.f2060c);
            }

            /* renamed from: B */
            public C0847a mo738a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case 16:
                            int g = aVar.mo225g();
                            switch (g) {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                    this.f2059b = g;
                                    break;
                                default:
                                    continue;
                            }
                        case 24:
                            int g2 = aVar.mo225g();
                            switch (g2) {
                                case -1:
                                case 0:
                                case 1:
                                    this.f2060c = g2;
                                    break;
                                default:
                                    continue;
                            }
                        default:
                            if (!com.yandex.metrica.impl.ob.g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rm$a$B */
        public static final class C0848b extends e {

            /* renamed from: d */
            private static volatile C0848b[] f2061d;

            /* renamed from: B */
            public String f2062b;

            /* renamed from: a */
            public boolean f2063c;

            /* renamed from: d */
            public static C0848b[] m3220d() {
                if (f2061d == null) {
                    synchronized (c.a) {
                        if (f2061d == null) {
                            f2061d = new C0848b[0];
                        }
                    }
                }
                return f2061d;
            }

            public C0848b() {
                mo1845e();
            }

            /* renamed from: e */
            public C0848b mo1845e() {
                this.f2062b = "";
                this.f2063c = false;
                this.f754a = -1;
                return this;
            }

            /* renamed from: a */
            public void mo739a(b bVar) throws IOException {
                bVar.mo351a(1, this.f2062b);
                bVar.mo352a(2, this.f2063c);
                super.mo739a(bVar);
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public int mo741c() {
                return super.mo741c() + b.m437b(1, this.f2062b) + b.m438b(2, this.f2063c);
            }

            /* renamed from: B */
            public C0848b mo738a(a aVar) throws IOException {
                while (true) {
                    int a = aVar.mo213a();
                    switch (a) {
                        case 0:
                            break;
                        case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                            this.f2062b = aVar.mo229i();
                            continue;
                        case 16:
                            this.f2063c = aVar.mo228h();
                            continue;
                        default:
                            if (!g.a(aVar, a)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
//                return this;
            }
        }

        public C0846a() {
            mo1841d();
        }

        /* renamed from: d */
        public C0846a mo1841d() {
            this.f2056b = C0848b.m3220d();
            this.f2057c = null;
            this.f2058d = g.f;
            this.f754a = -1;
            return this;
        }

        /* renamed from: a */
        public void mo739a(b bVar) throws IOException {
            if (this.f2056b != null && this.f2056b.length > 0) {
                for (C0848b bVar2 : this.f2056b) {
                    if (bVar2 != null) {
                        bVar.mo350a(1, (e) bVar2);
                    }
                }
            }
            if (this.f2057c != null) {
                bVar.mo350a(2, (e) this.f2057c);
            }
            if (this.f2058d != null && this.f2058d.length > 0) {
                for (String str : this.f2058d) {
                    if (str != null) {
                        bVar.mo351a(3, str);
                    }
                }
            }
            super.mo739a(bVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public int mo741c() {
            int c = super.mo741c();
            if (this.f2056b != null && this.f2056b.length > 0) {
                for (C0848b bVar : this.f2056b) {
                    if (bVar != null) {
                        c += b.b(1, (e) bVar);
                    }
                }
            }
            if (this.f2057c != null) {
                c += b.b(2, (e) this.f2057c);
            }
            if (this.f2058d == null || this.f2058d.length <= 0) {
                return c;
            }
            int i = 0;
            int i2 = 0;
            for (String str : this.f2058d) {
                if (str != null) {
                    i2++;
                    i += b.m441b(str);
                }
            }
            return c + i + (i2 * 1);
        }

        /* renamed from: B */
        public C0846a mo738a(a aVar) throws IOException {
            int length;
            while (true) {
                int a = aVar.mo213a();
                switch (a) {
                    case 0:
                        break;
                    case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                        int b = g.b(aVar, 10);
                        if (this.f2056b == null) {
                            length = 0;
                        } else {
                            length = this.f2056b.length;
                        }
                        C0848b[] bVarArr = new C0848b[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.f2056b, 0, bVarArr, 0, length);
                        }
                        while (length < bVarArr.length - 1) {
                            bVarArr[length] = new C0848b();
                            aVar.mo215a((e) bVarArr[length]);
                            aVar.mo213a();
                            length++;
                        }
                        bVarArr[length] = new C0848b();
                        aVar.mo215a((e) bVarArr[length]);
                        this.f2056b = bVarArr;
                        continue;
                    case 18:
                        if (this.f2057c == null) {
                            this.f2057c = new C0847a();
                        }
                        aVar.mo215a((e) this.f2057c);
                        continue;
                    case 26:
                        int b2 = g.b(aVar, 26);
                        int length2 = this.f2058d == null ? 0 : this.f2058d.length;
                        String[] strArr = new String[(b2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.f2058d, 0, strArr, 0, length2);
                        }
                        while (length2 < strArr.length - 1) {
                            strArr[length2] = aVar.mo229i();
                            aVar.mo213a();
                            length2++;
                        }
                        strArr[length2] = aVar.mo229i();
                        this.f2058d = strArr;
                        continue;
                    default:
                        if (!g.a(aVar, a)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
//            return this;
        }

        /* renamed from: a */
        public static C0846a m3209a(byte[] bArr) throws d {
            return (C0846a) e.m1393a(new C0846a(), bArr);
        }
    }
}
