package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.yandex.metrica.impl.ob.rs.a.b;
import com.yandex.metrica.impl.ob.rs.a.b.aa;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* renamed from: com.yandex.metrica.impl.ob.tk */
public class tk {

    /* renamed from: a */
    private static final Map<aa, String> f2576a = Collections.unmodifiableMap(new HashMap<aa, String>() {
        {
            put(aa.COMPLETE, "complete");
            put(aa.ERROR, "error");
            put(b.aa.OFFLINE, "offline");
            put(aa.INCOMPATIBLE_NETWORK_TYPE, "incompatible_network_type");
        }
    });

    /* renamed from: B */
    private static final Map<bt.a, String> f2577b = Collections.unmodifiableMap(new HashMap<bt.a, String>() {
        {
            put(bt.a.WIFI, "wifi");
            put(bt.a.CELL, "cell");
            put(bt.a.OFFLINE, "offline");
            put(bt.a.UNDEFINED, "undefined");
        }
    });

    /* renamed from: a */
    public String mo2244a(@NonNull b bVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("id", bVar.a().a);
            jSONObject.putOpt("url", bVar.a().b);
            jSONObject.putOpt("status", f2576a.get(bVar.mo1918b()));
            jSONObject.putOpt("code", bVar.mo1921d());
            if (!cx.nullOrEmpty(bVar.mo1922e())) {
                jSONObject.putOpt("body", m3786a(bVar.mo1922e()));
            } else if (!cx.nullOrEmpty(bVar.mo1925h())) {
                jSONObject.putOpt("body", m3786a(bVar.mo1925h()));
            }
            jSONObject.putOpt("headers", m3787a(bVar.mo1923f()));
            jSONObject.putOpt("error", m3785a(bVar.mo1924g()));
            jSONObject.putOpt("network_type", f2577b.get(bVar.mo1920c()));
            return jSONObject.toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    /* renamed from: a */
    public String mo2243a(@NonNull rs.a.aa aVar) {
        try {
            return new JSONObject().put("id", aVar.a).toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    @Nullable
    /* renamed from: a */
    private JSONObject m3787a(@Nullable Map<String, List<String>> map) throws JSONException {
        if (cx.m1193a((Map) map)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            if (!cx.a((Collection) entry.getValue())) {
                List<String> a = cx.m1180a((List) entry.getValue(), 10);
                ArrayList arrayList = new ArrayList();
                for (String str2 : a) {
                    if (!TextUtils.isEmpty(str2)) {
                        arrayList.add(cx.m1176a(str2, 100));
                    }
                }
                jSONObject.putOpt(str, TextUtils.join(",", arrayList));
            }
        }
        return jSONObject;
    }

    @Nullable
    /* renamed from: a */
    private String m3786a(@NonNull byte[] bArr) {
        return Base64.encodeToString(bArr, 0);
    }

    @Nullable
    /* renamed from: a */
    private String m3785a(@Nullable Throwable th) {
        if (th != null) {
            return th.toString() + "\n" + Log.getStackTraceString(th);
        }
        return null;
    }
}
