package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.xr */
public class xr extends xq<byte[]> {
    @VisibleForTesting(otherwise = 3)
    /* renamed from: a */
    public /* bridge */ /* synthetic */ int mo2635a() {
        return super.mo2635a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    /* renamed from: B */
    public /* bridge */ /* synthetic */ String mo2636b() {
        return super.mo2636b();
    }

    public xr(int i, @NonNull String str, @NonNull vz vzVar) {
        super(i, str, vzVar);
    }

    @Nullable
    /* renamed from: a */
    public byte[] a(@Nullable byte[] bArr) {
        if (bArr.length <= mo2635a()) {
            return bArr;
        }
        byte[] bArr2 = new byte[mo2635a()];
        System.arraycopy(bArr, 0, bArr2, 0, mo2635a());
        if (this.f2984a.mo2485c()) {
            this.f2984a.mo2482b("\"%s\" %s exceeded limit of %d bytes", mo2636b(), bArr, Integer.valueOf(mo2635a()));
        }
        return bArr2;
    }
}
