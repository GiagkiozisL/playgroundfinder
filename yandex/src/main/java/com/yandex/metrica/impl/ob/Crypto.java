package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

    private final String trans;
    private final byte[] pkgMd5; // as key
    private final byte[] reversePkgMd5; // as iv

    public Crypto(String trans, byte[] pkgMd5, byte[] reversePkgMd5) {
        this.trans = trans;
        this.pkgMd5 = pkgMd5;
        this.reversePkgMd5 = reversePkgMd5;
    }

    @SuppressLint({"TrulyRandom"})
    public byte[] encrypt(byte[] bArr) throws Throwable {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.pkgMd5, "AES");
        Cipher instance = Cipher.getInstance(this.trans);
        instance.init(1, secretKeySpec, new IvParameterSpec(this.reversePkgMd5));
        return instance.doFinal(bArr);
    }

    @SuppressLint({"TrulyRandom"})
    public byte[] decrypt(byte[] bArr) throws Throwable {
        return decrypt(bArr, 0, bArr.length);
    }

    public byte[] decrypt(byte[] bArr, int i, int i2) throws Throwable {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.pkgMd5, "AES");
        Cipher instance = Cipher.getInstance(this.trans);
        instance.init(2, secretKeySpec, new IvParameterSpec(this.reversePkgMd5));
        return instance.doFinal(bArr, i, i2);
    }
}
