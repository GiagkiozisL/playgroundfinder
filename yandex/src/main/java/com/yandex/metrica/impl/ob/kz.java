package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.i.a;
import com.yandex.metrica.impl.ob.ky.C0552b;
import com.yandex.metrica.impl.ob.lq.C0585g;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: com.yandex.metrica.impl.ob.kz */
public class kz implements Closeable {

    /* renamed from: a */
    private final ReentrantReadWriteLock f1260a;

    /* renamed from: B */
    private final Lock f1261b;

    /* renamed from: a */
    private final Lock f1262c;

    /* renamed from: d */
    private final lc f1263d;

    /* renamed from: e */
    private final C0554a f1264e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final Object f1265f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final List<ContentValues> f1266g;

    /* renamed from: h */
    private final Context f1267h;

    /* renamed from: i */
    private final en f1268i;

    /* renamed from: j */
    private final AtomicLong f1269j;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: k */
    public final List<nw> f1270k;
    @NonNull

    /* renamed from: l */
    private final ky f1271l;

    /* renamed from: com.yandex.metrica.impl.ob.kz$a */
    private class C0554a extends xl {
        @Nullable

        /* renamed from: B */
        private en f1273b;

        C0554a(en enVar) {
            this.f1273b = enVar;
        }

        public void run() {
            ArrayList arrayList;
            while (c()) {
                try {
                    synchronized (this) {
                        if (kz.this.m2116g()) {
                            wait();
                        }
                    }
                } catch (Throwable th) {
                    mo2611b();
                }
                synchronized (kz.this.f1265f) {
                    arrayList = new ArrayList(kz.this.f1266g);
                    kz.this.f1266g.clear();
                }
                kz.this.mo1204a((List<ContentValues>) arrayList);
                mo1212a(arrayList);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public synchronized void mo1211a() {
            mo2611b();
            this.f1273b = null;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public synchronized void mo1212a(@NonNull List<ContentValues> list) {
            ArrayList arrayList = new ArrayList();
            for (ContentValues a : list) {
                arrayList.add(Integer.valueOf(kz.this.m2115g(a)));
            }
            for (nw a2 : kz.this.f1270k) {
                a2.a(arrayList);
            }
            if (this.f1273b != null) {
                this.f1273b.mo812C().mo1526a();
            }
        }
    }

    public kz(@NonNull en enVar, lc lcVar) {
        this(enVar, lcVar, new ky(enVar.mo824k(), enVar.mo767a()));
    }

    public kz(@NonNull en enVar, lc lcVar, @NonNull ky kyVar) {
        this.f1260a = new ReentrantReadWriteLock();
        this.f1261b = this.f1260a.readLock();
        this.f1262c = this.f1260a.writeLock();
        this.f1265f = new Object();
        this.f1266g = new ArrayList(3);
        this.f1269j = new AtomicLong();
        this.f1270k = new ArrayList();
        this.f1263d = lcVar;
        this.f1267h = enVar.mo824k();
        this.f1268i = enVar;
        this.f1271l = kyVar;
        this.f1269j.set(m2111e());
        this.f1264e = new C0554a(enVar);
        this.f1264e.setName(m2098a((et) enVar));
    }

    /* renamed from: a */
    public void mo1198a() {
        this.f1264e.start();
    }

    /* renamed from: B */
    public long mo1205b() {
        this.f1261b.lock();
        try {
            return this.f1269j.get();
        } finally {
            this.f1261b.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public long mo1195a(@NonNull Set<Integer> set) {
        long j;
        Cursor cursor = null;
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                StringBuilder sb = new StringBuilder("SELECT count() FROM reports");
                if (!set.isEmpty()) {
                    sb.append(" WHERE ");
                }
                int i = 0;
                for (Integer num : set) {
                    if (i > 0) {
                        sb.append(" OR ");
                    }
                    sb.append("type == " + num);
                    i++;
                }
                cursor = readableDatabase.rawQuery(sb.toString(), null);
                if (cursor.moveToFirst()) {
                    j = cursor.getLong(0);
                    cx.m1183a(cursor);
                    this.f1261b.unlock();
                    return j;
                }
            }
            j = 0;
            cx.m1183a(cursor);
            this.f1261b.unlock();
            return j;
        } catch (Throwable th) {
            cx.m1183a((Cursor) null);
            this.f1261b.unlock();
            throw th;
        }
    }

    /* renamed from: a */
    public void mo1202a(@NonNull nw nwVar) {
        this.f1270k.add(nwVar);
    }

    /* renamed from: a */
    private static String m2098a(et etVar) {
        return "DatabaseWorker [" + etVar.b().mo792c() + "]";
    }

    /* renamed from: a */
    public void mo1200a(long j, jh jhVar, long j2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Long.valueOf(j));
        contentValues.put("start_time", Long.valueOf(j2));
        contentValues.put("server_time_offset", Long.valueOf(wi.m4361c()));
        contentValues.put("obtained_before_first_sync", Boolean.valueOf(wd.m4332a().mo2559d()));
        contentValues.put("type", Integer.valueOf(jhVar.a()));
        new y(this.f1267h).mo2651a(this.f1268i.mo822i()).mo2650a(contentValues).mo2652a();
        mo1201a(contentValues);
    }

    /* renamed from: a */
    public void mo1203a(@NonNull ww wwVar, int i, @NonNull je jeVar, @NonNull a aVar, @NonNull er erVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("number", Long.valueOf(jeVar.mo1084c()));
        int i2 = 0;
        if (af.m293e(i)) {
            i2 = erVar.mo865a();
        }
        contentValues.put("global_number", Integer.valueOf(i2));
        contentValues.put("number_of_type", Integer.valueOf(erVar.mo866a(i)));
        contentValues.put("time", Long.valueOf(jeVar.mo1086d()));
        contentValues.put("session_id", Long.valueOf(jeVar.mo1079a()));
        contentValues.put("session_type", Integer.valueOf(jeVar.mo1083b().a()));
        new y(this.f1267h).mo2651a(this.f1268i.mo822i()).mo2650a(contentValues).mo2654a(wwVar, aVar);
        mo1207b(contentValues);
    }

    /* renamed from: e */
    private long m2111e() {
        long j = 0;
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                j = vl.a(readableDatabase, "reports");
            }
        } catch (Throwable th) {
        } finally {
            this.f1261b.unlock();
        }
        return j;
    }

    /* renamed from: a */
    public void mo1201a(ContentValues contentValues) {
        m2108c(contentValues);
    }

    /* renamed from: B */
    public void mo1207b(ContentValues contentValues) {
        synchronized (this.f1265f) {
            this.f1266g.add(contentValues);
        }
        synchronized (this.f1264e) {
            this.f1264e.notifyAll();
        }
    }

    /* renamed from: a */
    public int mo1194a(long j) {
        int i = 0;
        this.f1262c.lock();
        try {
            if (lq.f1340a.booleanValue()) {
                m2114f();
            }
            SQLiteDatabase writableDatabase = this.f1263d.getWritableDatabase();
            if (writableDatabase != null) {
                i = writableDatabase.delete("sessions", C0585g.f1359d, new String[]{String.valueOf(j)});
            }
        } catch (Throwable th) {
        } finally {
            this.f1262c.unlock();
        }
        return i;
    }

    /* renamed from: f */
    private void m2114f() {
        Cursor cursor;
        Cursor cursor2;
        Cursor cursor3;
        Throwable th;
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor2 = readableDatabase.rawQuery(" SELECT DISTINCT id From sessions order by id asc ", new String[0]);
                try {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("All sessions in db: ");
                    while (cursor2.moveToNext()) {
                        stringBuffer.append(cursor2.getString(0)).append(", ");
                    }
                    cursor = readableDatabase.rawQuery(" SELECT DISTINCT session_id From reports order by session_id asc ", new String[0]);
                } catch (Throwable th2) {
                    th = th2;
                    cursor3 = null;
                    this.f1261b.unlock();
                    cx.m1183a(cursor2);
                    cx.m1183a(cursor3);
                    throw th;
                }
                try {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("All sessions in reports db: ");
                    while (cursor.moveToNext()) {
                        stringBuffer2.append(cursor.getString(0)).append(", ");
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor3 = cursor;
                    this.f1261b.unlock();
                    cx.m1183a(cursor2);
                    cx.m1183a(cursor3);
                    throw th;
                }
            } else {
                cursor = null;
                cursor2 = null;
            }
            this.f1261b.unlock();
            cx.m1183a(cursor2);
            cx.m1183a(cursor);
        } catch (Throwable th4) {
            th = th4;
            cursor3 = null;
            cursor2 = null;
            this.f1261b.unlock();
            cx.m1183a(cursor2);
            cx.m1183a(cursor3);
//            throw th;
        }
    }

    /* renamed from: a */
    public void mo1208c() {
        try {
            this.f1262c.lock();
            if (this.f1269j.get() > this.f1268i.mo822i().mo2146W()) {
                SQLiteDatabase writableDatabase = this.f1263d.getWritableDatabase();
                if (writableDatabase != null) {
                    this.f1269j.addAndGet((long) (-m2096a(writableDatabase)));
                }
            }
        } catch (Throwable th) {
        } finally {
            this.f1262c.unlock();
        }
    }

    /* renamed from: a */
    private int m2096a(SQLiteDatabase sQLiteDatabase) {
        try {
            Integer[] numArr = new Integer[af.f156a.size()];
            Iterator it = af.f156a.iterator();
            int i = 0;
            while (it.hasNext()) {
                int i2 = i + 1;
                numArr[i] = Integer.valueOf(((C0058a) it.next()).mo259a());
                i = i2;
            }
            return this.f1271l.mo1193a(sQLiteDatabase, "reports", String.format("%1$s NOT IN (%2$s) AND (%3$s IN (SELECT %3$s FROM %4$s ORDER BY %5$s, %6$s LIMIT (SELECT count() FROM %4$s) / %7$s ))", new Object[]{"type", TextUtils.join(",", numArr), "id", "reports", "session_id", "number", Integer.valueOf(10)}), C0552b.DB_OVERFLOW, this.f1268i.b().mo790a(), true).f1255b;
        } catch (Throwable th) {
            tl.a(this.f1267h).reportError("deleteExcessiveReports exception", th);
            return 0;
        }
    }

    /* renamed from: a */
    public void mo1199a(long j, int i, int i2, boolean z) throws SQLiteException {
        if (i2 > 0) {
            this.f1262c.lock();
            try {
                String format = String.format(Locale.US, "%1$s = %2$s AND %3$s = %4$s AND %5$s <= (SELECT %5$s FROM %6$s WHERE %1$s = %2$s AND %3$s = %4$s ORDER BY %5$s ASC LIMIT %7$s, 1)", new Object[]{"session_id", Long.toString(j), "session_type", Integer.toString(i), "id", "reports", Integer.toString(i2 - 1)});
                SQLiteDatabase writableDatabase = this.f1263d.getWritableDatabase();
                if (writableDatabase != null) {
                    com.yandex.metrica.impl.ob.ky.a a = this.f1271l.mo1193a(writableDatabase, "reports", format, C0552b.BAD_REQUEST, this.f1268i.b().mo790a(), z);
                    if (a.f1254a != null) {
                        ArrayList arrayList = new ArrayList();
                        for (ContentValues g : a.f1254a) {
                            arrayList.add(Integer.valueOf(m2115g(g)));
                        }
                        for (nw b : this.f1270k) {
                            b.b(arrayList);
                        }
                    }
                    if (this.f1268i.mo825l().mo2485c() && a.f1254a != null) {
                        m2101a(a.f1254a, "Event removed from db");
                    }
                    this.f1269j.addAndGet((long) (-a.f1255b));
                }
            } catch (Throwable th) {
            } finally {
                this.f1262c.unlock();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public Cursor mo1197a(Map<String, String> map) {
        Cursor cursor;
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.query("sessions", null, m2099a("id >= ?", map), m2103a(new String[]{Long.toString(0)}, map), null, null, "id ASC", null);
            } else {
                cursor = null;
            }
            this.f1261b.unlock();
            return cursor;
        } catch (Throwable th) {
            this.f1261b.unlock();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public Cursor mo1196a(long j, jh jhVar) throws SQLiteException {
        Cursor cursor;
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.query("reports", null, "session_id = ? AND session_type = ?", new String[]{Long.toString(j), Integer.toString(jhVar.a())}, null, null, "number ASC", null);
            } else {
                cursor = null;
            }
            this.f1261b.unlock();
            return cursor;
        } catch (Throwable th) {
            this.f1261b.unlock();
            throw th;
        }
    }

    /* renamed from: a */
    private void m2108c(ContentValues contentValues) {
        if (contentValues != null) {
            this.f1262c.lock();
            try {
                SQLiteDatabase writableDatabase = this.f1263d.getWritableDatabase();
                if (writableDatabase != null) {
                    writableDatabase.insertOrThrow("sessions", null, contentValues);
                }
            } catch (Throwable th) {
            } finally {
                this.f1262c.unlock();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public void mo1204a(List<ContentValues> list) {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        SQLiteDatabase sQLiteDatabase2 = null;
        if (list != null && !list.isEmpty()) {
            this.f1262c.lock();
            try {
                SQLiteDatabase writableDatabase = this.f1263d.getWritableDatabase();
                if (writableDatabase != null) {
                    try {
                        writableDatabase.beginTransaction();
                        for (ContentValues contentValues : list) {
                            writableDatabase.insertOrThrow("reports", null, contentValues);
                            this.f1269j.incrementAndGet();
                            m2100a(contentValues, "Event saved to db");
                        }
                        writableDatabase.setTransactionSuccessful();
                        this.f1269j.get();
                    } catch (Throwable th2) {
                        th = th2;
                        sQLiteDatabase = writableDatabase;
                        cx.m1184a(sQLiteDatabase);
                        this.f1262c.unlock();
                        throw th;
                    }
                }
                cx.m1184a(writableDatabase);
                this.f1262c.unlock();
            } catch (Throwable th3) {
                th = th3;
                sQLiteDatabase = null;
                cx.m1184a(sQLiteDatabase);
                this.f1262c.unlock();
//                throw th;
            }
        }
    }

    /* renamed from: a */
    private void m2100a(ContentValues contentValues, String str) {
        if (af.m285b(m2109d(contentValues))) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(m2112e(contentValues));
            String f = m2113f(contentValues);
            if (af.m289c(m2115g(contentValues)) && !TextUtils.isEmpty(f)) {
                sb.append(" with value ");
                sb.append(f);
            }
            this.f1268i.mo825l().mo2477a(sb.toString());
        }
    }

    /* renamed from: a */
    private void m2101a(List<ContentValues> list, String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                m2100a((ContentValues) list.get(i2), str);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    @NonNull
    /* renamed from: d */
    public List<ContentValues> mo1210d() {
        Cursor cursor;
        Cursor cursor2;
        Throwable th;
        Cursor cursor3 = null;
        ArrayList arrayList = new ArrayList();
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor3 = readableDatabase.rawQuery(C0585g.f1358c, null);
                while (cursor3.moveToNext()) {
                    try {
                        ContentValues contentValues = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(cursor3, contentValues);
                        arrayList.add(contentValues);
                    } catch (Throwable th2) {
                        th = th2;
                        cursor2 = cursor3;
                        cx.m1183a(cursor2);
                        this.f1261b.unlock();
                        throw th;
                    }
                }
            }
            cx.m1183a(cursor3);
            this.f1261b.unlock();
            return arrayList;
        } catch (Throwable th3) {
            th = th3;
            cursor2 = null;
            cx.m1183a(cursor2);
            this.f1261b.unlock();
//            throw th;
        }
        return arrayList;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: B */
    public ContentValues mo1206b(long j, jh jhVar) {
        ContentValues contentValues;
        Cursor cursor = null;
        ContentValues contentValues2 = new ContentValues();
        this.f1261b.lock();
        try {
            SQLiteDatabase readableDatabase = this.f1263d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.rawQuery(String.format(Locale.US, "SELECT report_request_parameters FROM sessions WHERE id = %s AND type = %s ORDER BY id DESC LIMIT 1", new Object[]{Long.valueOf(j), Integer.valueOf(jhVar.a())}), null);
                if (cursor.moveToNext()) {
                    contentValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                    cx.m1183a(cursor);
                    this.f1261b.unlock();
                    return contentValues;
                }
            }
            contentValues = contentValues2;
            cx.m1183a(cursor);
            this.f1261b.unlock();
            return contentValues;
        } catch (Throwable th) {
            cx.m1183a((Cursor) null);
            this.f1261b.unlock();
            throw th;
        }
    }

    /* renamed from: a */
    private static String m2099a(String str, Map<String, String> map) {
        StringBuilder sb = new StringBuilder(str);
        for (String str2 : map.keySet()) {
            sb.append(sb.length() > 0 ? " AND " : "");
            sb.append(str2 + " = ? ");
        }
        if (TextUtils.isEmpty(sb.toString())) {
            return null;
        }
        return sb.toString();
    }

    /* renamed from: a */
    private static String[] m2103a(String[] strArr, Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        for (Entry value : map.entrySet()) {
            arrayList.add(value.getValue());
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    /* renamed from: d */
    private static int m2109d(ContentValues contentValues) {
        Integer asInteger = contentValues.getAsInteger("type");
        if (asInteger != null) {
            return asInteger.intValue();
        }
        return -1;
    }

    /* renamed from: e */
    private String m2112e(ContentValues contentValues) {
        return m2105b(contentValues, "name");
    }

    /* renamed from: f */
    private String m2113f(ContentValues contentValues) {
        return m2105b(contentValues, "value");
    }

    /* renamed from: B */
    private String m2105b(ContentValues contentValues, String str) {
        return cu.m1156b(contentValues.getAsString(str), "");
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public int m2115g(ContentValues contentValues) {
        return m2106c(contentValues, "type");
    }

    /* renamed from: a */
    private int m2106c(ContentValues contentValues, String str) {
        return contentValues.getAsInteger(str).intValue();
    }

    public void close() {
        this.f1266g.clear();
        this.f1264e.mo1211a();
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public boolean m2116g() {
        boolean isEmpty;
        synchronized (this.f1265f) {
            isEmpty = this.f1266g.isEmpty();
        }
        return isEmpty;
    }
}
