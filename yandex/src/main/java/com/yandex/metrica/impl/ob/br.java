package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.br */
public class br implements Runnable {
    @NonNull

    /* renamed from: a */
    private final pi f397a;
    @NonNull

    /* renamed from: B */
    private final bo f398b;
    @NonNull

    /* renamed from: a */
    private final xj f399c;
    @NonNull

    /* renamed from: d */
    private final String f400d;
    @NonNull

    /* renamed from: e */
    private final bq f401e;

    public br(@NonNull pi piVar, @NonNull bo boVar, @NonNull xj xjVar, @NonNull String str) {
        this(piVar, boVar, xjVar, new bq(), str);
    }

    public br(@NonNull pi piVar, @NonNull bo boVar, @NonNull xj xjVar, @NonNull bq bqVar, @NonNull String str) {
        this.f397a = piVar;
        this.f398b = boVar;
        this.f399c = xjVar;
        this.f401e = bqVar;
        this.f400d = str;
    }

    public void run() {
        boolean z = false;
        if (this.f399c.c() && this.f397a.mo1636a()) {
            boolean a = this.f398b.mo461a();
            pj c = this.f398b.c();
            if (a && !c.mo1638b()) {
                a = false;
            }
            if (!a) {
            }
            int i = 0;
            boolean z2 = a;
            while (this.f399c.c() && z2) {
                int i2 = i + 1;
                z2 = !m718a() && this.f398b.mo482t();
                i = i2;
            }
            z = true;
        }
        if (!z) {
            m719b();
        }
        this.f398b.f();
    }

    /* renamed from: a */
    private boolean m718a() {
        this.f401e.mo492a(this.f398b);
        boolean b = this.f398b.mo463b();
        this.f398b.mo459a(b);
        return b;
    }

    /* renamed from: B */
    private void m719b() {
        this.f398b.g();
    }
}
