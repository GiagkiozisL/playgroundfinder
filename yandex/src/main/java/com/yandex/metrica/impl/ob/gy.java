package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.eg.C0306a;

/* renamed from: com.yandex.metrica.impl.ob.gy */
public class gy extends gt {

    /* renamed from: a */
    private final cs f994a;
    @NonNull

    /* renamed from: B */
    private final ox f995b;

    public gy(@NonNull ei eiVar, @NonNull cs csVar, @NonNull ox oxVar) {
        super(eiVar);
        this.f994a = csVar;
        this.f995b = oxVar;
    }

    /* renamed from: a */
    public boolean mo962a(@NonNull w wVar, @NonNull fs fsVar) {
        C0306a a = fsVar.mo922b().mo774a();
        this.f994a.mo650a(a.f780m);
        m1730a(wk.m4371a(a.f772e, true));
        return false;
    }

    /* renamed from: a */
    private void m1730a(boolean z) {
        this.f995b.mo1617a(z);
    }
}
