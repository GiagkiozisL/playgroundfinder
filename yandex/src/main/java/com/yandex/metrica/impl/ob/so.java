package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.sn.C0972a;
import com.yandex.metrica.impl.ob.sn.C0973b;

/* renamed from: com.yandex.metrica.impl.ob.so */
public abstract class so<T extends sq, A extends C0972a<C0306a, A>, L extends C0973b<T, A>> extends sp<T, C0306a, A, L> {
    public so(@NonNull L l, @NonNull uk ukVar, @NonNull A a) {
        super(l, ukVar, a);
    }

    /* renamed from: a */
    public synchronized void a(@NonNull C0306a aVar) {
        super.a(aVar);
    }
}
