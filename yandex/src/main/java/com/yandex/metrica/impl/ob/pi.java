package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.bt.a;

import java.util.EnumSet;

/* renamed from: com.yandex.metrica.impl.ob.pi */
public class pi {

    /* renamed from: a */
    private static final EnumSet<a> f1729a = EnumSet.of(a.OFFLINE);

    /* renamed from: B */
    private vn f1730b = new vk();

    /* renamed from: a */
    private final Context f1731c;

    public pi(@NonNull Context context) {
        this.f1731c = context;
    }

    /* renamed from: a */
    public boolean mo1636a() {
        return !f1729a.contains(this.f1730b.a(this.f1731c));
    }
}
