package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.eq */
public interface eq<T extends ev> {
    /* renamed from: B */
    T b(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar);
}
