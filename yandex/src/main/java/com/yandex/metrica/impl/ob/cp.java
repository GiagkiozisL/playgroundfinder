package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.cq.b;
import com.yandex.metrica.impl.ob.cq.b.C0229a;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.cp */
public class cp implements bn {
    @NonNull

    /* renamed from: a */
    private final ul f579a;
    @NonNull

    /* renamed from: B */
    private final su f580b;
    @NonNull

    /* renamed from: a */
    private final cq f581c;
    @NonNull

    /* renamed from: d */
    private final mp f582d;

    public cp(@NonNull ul ulVar, @NonNull su suVar) {
        this(ulVar, suVar, new cq(), new mp());
    }

    /* renamed from: a */
    public boolean a(int i, byte[] bArr, @NonNull Map<String, List<String>> map) {
        if (200 == i) {
            List list = (List) map.get("Content-Encoding");
            if (!cx.a((Collection) list) && "encrypted".equals(list.get(0))) {
                bArr = this.f582d.mo1435a(bArr, "hBnBQbZrmjPXEWVJ");
            }
            if (bArr != null) {
                b a = this.f581c.mo618a(bArr);
                if (C0229a.OK == a.mo633l()) {
                    this.f579a.mo2396a(a, this.f580b, map);
                    return true;
                }
            }
        }
        return false;
    }

    @VisibleForTesting
    cp(@NonNull ul ulVar, @NonNull su suVar, @NonNull cq cqVar, @NonNull mp mpVar) {
        this.f579a = ulVar;
        this.f580b = suVar;
        this.f581c = cqVar;
        this.f582d = mpVar;
    }
}
