package com.yandex.metrica.impl.ob;

import android.os.SystemClock;

/* renamed from: com.yandex.metrica.impl.ob.ak */
class ak {

    /* renamed from: a */
    private long f226a = (SystemClock.elapsedRealtime() - 2000000);

    /* renamed from: B */
    private boolean f227b = true;

    ak() {
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo279a() {
        boolean z = this.f227b;
        this.f227b = false;
        return m320a(z);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo280b() {
        this.f227b = true;
        this.f226a = SystemClock.elapsedRealtime();
    }

    /* renamed from: a */
    private boolean m320a(boolean z) {
        return z && SystemClock.elapsedRealtime() - this.f226a > 1000;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo281c() {
        return this.f227b;
    }
}
