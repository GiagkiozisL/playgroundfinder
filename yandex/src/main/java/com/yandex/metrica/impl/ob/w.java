package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Pair;

import com.yandex.metrica.impl.ob.af.C0058a;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

/* renamed from: com.yandex.metrica.impl.ob.w */
public class w implements Parcelable {
    public static final Creator<w> CREATOR = new Creator<w>() {
        /* renamed from: a */
        public w createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(x.class.getClassLoader());
            w a = new w().mo2520a(readBundle.getInt("CounterReport.Type", C0058a.EVENT_TYPE_UNDEFINED.mo259a())).mo2524b(readBundle.getInt("CounterReport.CustomType")).mo1767c(cu.m1156b(readBundle.getString("CounterReport.Value"), "")).mo1763a(readBundle.getString("CounterReport.UserInfo")).mo2530e(readBundle.getString("CounterReport.Environment")).mo1766b(readBundle.getString("CounterReport.Event")).m4272a(w.m4286d(readBundle)).mo2527c(readBundle.getInt("CounterReport.TRUNCATED")).mo1768d(readBundle.getString("CounterReport.ProfileID")).mo2521a(readBundle.getLong("CounterReport.CreationElapsedRealtime")).mo2525b(readBundle.getLong("CounterReport.CreationTimestamp")).mo2522a(aj.m319a(Integer.valueOf(readBundle.getInt("CounterReport.UniquenessStatus"))));
            ap apVar = (ap) readBundle.getParcelable("CounterReport.IdentifiersData");
            if (apVar != null) {
                a.mo2523a(apVar);
            }
            return a;
        }

        /* renamed from: a */
        public w[] newArray(int i) {
            return new w[i];
        }
    };

    /* renamed from: a */
    String f2916a;

    /* renamed from: B */
    String f2917b;

    /* renamed from: a */
    int f2918c;

    /* renamed from: d */
    int f2919d;

    /* renamed from: e */
    int f2920e;

    /* renamed from: f */
    private String f2921f;

    /* renamed from: g */
    private String f2922g;
    @Nullable

    /* renamed from: h */
    private Pair<String, String> f2923h;

    /* renamed from: i */
    private String f2924i;

    /* renamed from: j */
    private long f2925j;

    /* renamed from: k */
    private long f2926k;
    @NonNull

    /* renamed from: l */
    private aj f2927l;
    @Nullable

    /* renamed from: m */
    private ap f2928m;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putString("CounterReport.Event", this.f2916a);
        bundle.putString("CounterReport.Value", this.f2917b);
        bundle.putInt("CounterReport.Type", this.f2918c);
        bundle.putInt("CounterReport.CustomType", this.f2919d);
        bundle.putInt("CounterReport.TRUNCATED", this.f2920e);
        bundle.putString("CounterReport.ProfileID", this.f2924i);
        bundle.putInt("CounterReport.UniquenessStatus", this.f2927l.f225d);
        if (this.f2928m != null) {
            bundle.putParcelable("CounterReport.IdentifiersData", this.f2928m);
        }
        if (this.f2922g != null) {
            bundle.putString("CounterReport.Environment", this.f2922g);
        }
        if (this.f2921f != null) {
            bundle.putString("CounterReport.UserInfo", this.f2921f);
        }
        if (this.f2923h != null) {
            m4279a(bundle, this.f2923h);
        }
        bundle.putLong("CounterReport.CreationElapsedRealtime", this.f2925j);
        bundle.putLong("CounterReport.CreationTimestamp", this.f2926k);
        dest.writeBundle(bundle);
    }

    public w() {
        this("", 0);
    }

    public w(@Nullable w wVar) {
        this.f2927l = aj.UNKNOWN;
        if (wVar != null) {
            this.f2916a = wVar.mo2528d();
            this.f2917b = wVar.mo2531e();
            this.f2918c = wVar.mo2533g();
            this.f2919d = wVar.mo2534h();
            this.f2921f = wVar.mo2538l();
            this.f2922g = wVar.mo2536j();
            this.f2923h = wVar.mo2537k();
            this.f2920e = wVar.mo2541o();
            this.f2924i = wVar.f2924i;
            this.f2925j = wVar.mo2544r();
            this.f2926k = wVar.mo2545s();
            this.f2927l = wVar.f2927l;
            this.f2928m = wVar.f2928m;
        }
    }

    public w(String str, int i) {
        this("", str, i);
    }

    public w(String str, String str2, int i) {
        this(str, str2, i, new wg());
    }

    @VisibleForTesting
    public w(String str, String str2, int i, wg wgVar) {
        this.f2927l = aj.UNKNOWN;
        this.f2916a = str2;
        this.f2918c = i;
        this.f2917b = str;
        this.f2925j = wgVar.c();
        this.f2926k = wgVar.a();
    }

    /* renamed from: d */
    public String mo2528d() {
        return this.f2916a;
    }

    /* renamed from: B */
    public w mo1766b(String str) {
        this.f2916a = str;
        return this;
    }

    /* renamed from: e */
    public String mo2531e() {
        return this.f2917b;
    }

    /* renamed from: f */
    public byte[] mo2532f() {
        return Base64.decode(this.f2917b, 0);
    }

    /* renamed from: a */
    public w mo1767c(String str) {
        this.f2917b = str;
        return this;
    }

    /* renamed from: a */
    public w mo1764a(@Nullable byte[] bArr) {
        this.f2917b = new String(Base64.encode(bArr, 0));
        return this;
    }

    /* renamed from: g */
    public int mo2533g() {
        return this.f2918c;
    }

    /* renamed from: a */
    public w mo2520a(int i) {
        this.f2918c = i;
        return this;
    }

    /* renamed from: h */
    public int mo2534h() {
        return this.f2919d;
    }

    /* renamed from: B */
    public w mo2524b(int i) {
        this.f2919d = i;
        return this;
    }

    @Nullable
    /* renamed from: i */
    public ap mo2535i() {
        return this.f2928m;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: j */
    public String mo2536j() {
        return this.f2922g;
    }

    /* renamed from: k */
    public Pair<String, String> mo2537k() {
        return this.f2923h;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public w mo2530e(String str) {
        this.f2922g = str;
        return this;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public w mo2526b(String str, String str2) {
        if (this.f2923h == null) {
            this.f2923h = new Pair<>(str, str2);
        }
        return this;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public w m4272a(@Nullable Pair<String, String> pair) {
        this.f2923h = pair;
        return this;
    }

    /* renamed from: l */
    public String mo2538l() {
        return this.f2921f;
    }

    /* renamed from: a */
    public w mo1763a(String str) {
        this.f2921f = str;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public w mo2527c(int i) {
        this.f2920e = i;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public w mo2521a(long j) {
        this.f2925j = j;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public w mo2525b(long j) {
        this.f2926k = j;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public w mo2523a(@NonNull ap apVar) {
        this.f2928m = apVar;
        return this;
    }

    /* renamed from: m */
    public boolean mo2539m() {
        return this.f2916a == null;
    }

    /* renamed from: n */
    public boolean mo2540n() {
        return C0058a.EVENT_TYPE_UNDEFINED.mo259a() == this.f2918c;
    }

    /* renamed from: o */
    public int mo2541o() {
        return this.f2920e;
    }

    @Nullable
    /* renamed from: p */
    public String mo2542p() {
        return this.f2924i;
    }

    /* renamed from: d */
    public w mo1768d(@Nullable String str) {
        this.f2924i = str;
        return this;
    }

    @NonNull
    /* renamed from: q */
    public aj mo2543q() {
        return this.f2927l;
    }

    @NonNull
    /* renamed from: a */
    public w mo2522a(@NonNull aj ajVar) {
        this.f2927l = ajVar;
        return this;
    }

    /* renamed from: r */
    public long mo2544r() {
        return this.f2925j;
    }

    /* renamed from: s */
    public long mo2545s() {
        return this.f2926k;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: a */
    public Bundle mo2519a(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putParcelable("CounterReport.Object", this);
        return bundle;
    }

    public String toString() {
        return String.format(Locale.US, "[event: %s, type: %s, value: %s]", new Object[]{this.f2916a, C0058a.m298a(this.f2918c).mo260b(), this.f2917b});
    }

    /* renamed from: a */
    private static void m4279a(@NonNull Bundle bundle, @NonNull Pair<String, String> pair) {
        bundle.putString("CounterReport.AppEnvironmentDiffKey", (String) pair.first);
        bundle.putString("CounterReport.AppEnvironmentDiffValue", (String) pair.second);
    }

    /* access modifiers changed from: private */
    @Nullable
    /* renamed from: d */
    public static Pair<String, String> m4286d(Bundle bundle) {
        if (!bundle.containsKey("CounterReport.AppEnvironmentDiffKey") || !bundle.containsKey("CounterReport.AppEnvironmentDiffValue")) {
            return null;
        }
        return new Pair<>(bundle.getString("CounterReport.AppEnvironmentDiffKey"), bundle.getString("CounterReport.AppEnvironmentDiffValue"));
    }

    @NonNull
    /* renamed from: B */
    public static w m4280b(Bundle bundle) {
        if (bundle != null) {
            try {
                w wVar = (w) bundle.getParcelable("CounterReport.Object");
                if (wVar != null) {
                    return wVar;
                }
            } catch (Throwable th) {
                return new w();
            }
        }
        return new w();
    }

    /* renamed from: a */
    public static w m4275a(w wVar, C0058a aVar) {
        w a = m4273a(wVar);
        a.mo2520a(aVar.mo259a());
        return a;
    }

    /* renamed from: a */
    public static w m4273a(@NonNull w wVar) {
        w wVar2 = new w(wVar);
        wVar2.mo1766b("");
        wVar2.mo1767c("");
        return wVar2;
    }

    /* renamed from: B */
    public static w m4281b(w wVar) {
        return m4275a(wVar, C0058a.EVENT_TYPE_ALIVE);
    }

    /* renamed from: a */
    public static w m4284c(w wVar) {
        return m4275a(wVar, C0058a.EVENT_TYPE_START);
    }

    /* renamed from: d */
    public static w m4287d(w wVar) {
        return m4275a(wVar, C0058a.EVENT_TYPE_INIT);
    }

    /* renamed from: a */
    public static w m4271a(@NonNull Context context) {
        Integer c;
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (cx.a(21)) {
                c = m4282b(context);
                if (c == null) {
                    c = m4285c(context);
                }
            } else {
                c = m4285c(context);
            }
            if (c != null) {
                jSONObject2.put("battery", c);
            }
            jSONObject2.put("boot_time_seconds", wi.m4362d());
            jSONObject.put("dfid", jSONObject2);
        } catch (Throwable th) {
        }
        w b = new w().mo1766b("");
        b.mo2520a(C0058a.EVENT_TYPE_IDENTITY_LIGHT.mo259a()).mo1767c(jSONObject.toString());
        return b;
    }

    @Nullable
    @TargetApi(21)
    /* renamed from: B */
    private static Integer m4282b(@NonNull Context context) {
        BatteryManager batteryManager = (BatteryManager) context.getSystemService("batterymanager");
        if (batteryManager == null) {
            return null;
        }
        int intProperty = batteryManager.getIntProperty(4);
        if (intProperty == 0) {
            return null;
        }
        return Integer.valueOf(intProperty);
    }

    @Nullable
    /* renamed from: a */
    private static Integer m4285c(@NonNull Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("level", -1);
        int intExtra2 = registerReceiver.getIntExtra("scale", -1);
        if (intExtra <= 0 || intExtra2 <= 0) {
            return null;
        }
        return Integer.valueOf((intExtra * 100) / intExtra2);
    }

    /* renamed from: a */
    public static w m4276a(w wVar, en enVar) {
        ar a = new ar(enVar.mo824k()).mo313a();
        try {
            if (enVar.mo836w()) {
                a.mo318e();
            }
            if (enVar.mo822i().mo2129F()) {
                a.mo314a(enVar.mo822i().mo2130G());
            }
            a.mo316c();
        } catch (Throwable th) {
        }
        w a2 = m4273a(wVar);
        a2.mo2520a(C0058a.EVENT_TYPE_IDENTITY.mo259a()).mo1767c(a.mo320g());
        return a2;
    }

    /* renamed from: a */
    public static w m4278a(w wVar, @NonNull Collection<pu> collection, @Nullable l lVar, @NonNull j jVar, @NonNull List<String> list) {
        String str;
        w a = m4273a(wVar);
        String str2 = "";
        try {
            JSONArray jSONArray = new JSONArray();
            for (pu puVar : collection) {
                jSONArray.put(new JSONObject().put("name", puVar.name).put("granted", puVar.granted));
            }
            JSONObject jSONObject = new JSONObject();
            if (lVar != null) {
                jSONObject.put("background_restricted", lVar.f1275b);
                jSONObject.put("app_standby_bucket", jVar.mo1066a(lVar.f1274a));
            }
            str = new JSONObject().put("permissions", jSONArray).put("background_restrictions", jSONObject).put("available_providers", new JSONArray(list)).toString();
        } catch (Throwable th) {
            str = str2;
        }
        return a.mo2520a(C0058a.EVENT_TYPE_PERMISSIONS.mo259a()).mo1767c(str);
    }

    /* renamed from: a */
    public static w m4277a(w wVar, String str) {
        return m4273a(wVar).mo2520a(C0058a.EVENT_TYPE_APP_FEATURES.mo259a()).mo1767c(str);
    }

    /* renamed from: e */
    public static w m4288e(w wVar) {
        return m4275a(wVar, C0058a.EVENT_TYPE_FIRST_ACTIVATION);
    }

    /* renamed from: f */
    public static w m4289f(w wVar) {
        return m4275a(wVar, C0058a.EVENT_TYPE_APP_UPDATE);
    }

    /* renamed from: t */
    public static w m4290t() {
        return new w().mo2520a(C0058a.EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG.mo259a());
    }
}
