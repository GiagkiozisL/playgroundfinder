package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.util.Collection;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.jq */
public class jq {
    @NonNull

    /* renamed from: a */
    private final vt f1160a;
    @NonNull

    /* renamed from: B */
    private final js f1161b;

    public jq(@NonNull Context context) {
        this(new vt(), new js(context));
    }

    @Nullable
    /* renamed from: a */
    public Long mo1118a(@Nullable List<oj> list) {
        long a;
        if (cx.a((Collection) list)) {
            return null;
        }
        oj ojVar = (oj) list.get(Math.min(this.f1161b.mo1123a(), list.size()) - 1);
        if (ojVar.f1592a == ojVar.f1593b) {
            a = ojVar.f1592a;
        } else {
            a = this.f1160a.mo2502a(ojVar.f1592a, ojVar.f1593b);
        }
        return Long.valueOf(a);
    }

    @VisibleForTesting
    jq(@NonNull vt vtVar, @NonNull js jsVar) {
        this.f1160a = vtVar;
        this.f1161b = jsVar;
    }
}
