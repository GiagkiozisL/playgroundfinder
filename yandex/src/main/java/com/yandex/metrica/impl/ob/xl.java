package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.xl */
public class xl extends Thread implements xj {

    /* renamed from: a */
    private volatile boolean f2971a = true;

    public xl() {
    }

    public xl(@NonNull Runnable runnable, @NonNull String str) {
        super(runnable, str);
    }

    public xl(@NonNull String str) {
        super(str);
    }

    /* renamed from: a */
    public synchronized boolean c() {
        return this.f2971a;
    }

    /* renamed from: B */
    public synchronized void mo2611b() {
        this.f2971a = false;
        interrupt();
    }
}
