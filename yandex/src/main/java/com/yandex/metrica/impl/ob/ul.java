package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;

import com.yandex.metrica.IParamsCallback;
import com.yandex.metrica.impl.ac.a.c;
import com.yandex.metrica.impl.ob.cq.b;
import com.yandex.metrica.impl.ob.su.C0990a;
import com.yandex.metrica.impl.ob.uk.C1077a;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import me.android.ydx.Constant;

public class ul implements es {
    @NonNull

    private final Context f2807a;
    @NonNull

    private final ek f2808b;
    @NonNull

    private final uj f2809c;
    @NonNull

    private volatile mf<uk> f2810d;
    @Nullable

    private volatile cr f2811e;
    @NonNull

    private ud f2812f;

    public ul(@NonNull Context var1, @NonNull String var2, @NonNull com.yandex.metrica.impl.ob.su.C0990a var3, @NonNull uj var4) {
        this(var1, new eh(var2), var3, var4, com.yandex.metrica.impl.ob.np.C0678a.m2599a(uk.class).a(var1), new vo());
    }

    private ul(@NonNull Context var1, @NonNull ek var2, @NonNull com.yandex.metrica.impl.ob.su.C0990a var3, @NonNull uj var4, @NonNull mf<uk> var5, @NonNull vo var6) {
        this(var1, var2, var3, var4, var5, (uk)var5.a(), var6);
    }

    private ul(@NonNull Context var1, @NonNull ek var2, @NonNull com.yandex.metrica.impl.ob.su.C0990a var3, @NonNull uj var4, @NonNull mf<uk> var5, @NonNull uk var6, @NonNull vo var7) {
        this(var1, var2, var4, var5, var6, var7, new ud(new com.yandex.metrica.impl.ob.su.C0991b(var1, var2.mo791b()), var6, var3));
    }

    @VisibleForTesting
    ul(@NonNull Context context, @NonNull ek ekVar, @NonNull uj ujVar, @NonNull mf<uk> mfVar, @NonNull uk ukVar, @NonNull vo voVar, @NonNull ud udVar) {
        this.f2807a = context;
        this.f2808b = ekVar;
        this.f2809c = ujVar;
        this.f2810d = mfVar;
        this.f2812f = udVar;
        m4003a(ukVar, voVar);
    }

    private void m4003a(@NonNull uk ukVar, @NonNull vo voVar) {
        boolean z;
        C1077a a = ukVar.mo2359a();
        String str = "";
        c D = ((su) this.f2812f.mo2124d()).mo2077D();
        if (D != null) {
            str = voVar.mo2495a(D.advId);
            if (!TextUtils.equals(ukVar.deviceID2, str)) {
                z = true;
                a = a.mo2379c(str);
            } else {
                z = false;
            }
        } else {
            z = true;
            a = a.mo2379c(str);
        }
        if (!m4007b(ukVar.uuid) || !m4009c(ukVar.deviceId)) {
            if (!m4007b(ukVar.uuid)) {
                a = a.mo2370a(voVar.genUUID());
            }
            if (!m4009c(ukVar.deviceId)) {
                a = a.setDeviceId(str).mo2382d("");
            }
            z = true;
        }
        if (z) {
            uk a2 = a.mo2373a();
            m4010d(a2);
            mo2403b(a2);
            return;
        }
        mo2403b(ukVar);
    }

    @NonNull
    public ek b() {
        return this.f2808b;
    }

    @Nullable
    public synchronized cr mo2394a() {
        cr crVar;
        if (mo2404c()) {
            if (this.f2811e == null) {
                this.f2811e = new cr(this, mo2405d());
            }
            crVar = this.f2811e;
        } else {
            crVar = null;
        }
        return crVar;
    }

    public synchronized boolean mo2402a(@Nullable List<String> list, @NonNull Map<String, String> map) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        synchronized (this) {
            if (list != null) {
                uk b = this.f2812f.mo2122b();
                boolean z4 = false;
                for (String str : list) {
                    if (str.equals("yandex_mobile_metrica_uuid")) {
                        if (!m4007b(b.uuid)) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        z = z2 | z4;
                    } else if (str.equals("yandex_mobile_metrica_device_id")) {
                        z = (!m4009c(b.deviceId)) | z4;
                    } else if (str.equals("appmetrica_device_id_hash")) {
                        z = (!m4011d(b.deviceIDHash)) | z4;
                    } else if (str.equals("yandex_mobile_metrica_get_ad_url")) {
                        z = (!m4013e(b.getAdUrl)) | z4;
                    } else if (str.equals("yandex_mobile_metrica_report_ad_url")) {
                        z = (!m4015f(b.reportAdUrl)) | z4;
                    } else if (str.equals(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS)) {
                        z = (!m4005a(map)) | z4;
                    } else {
                        z = true;
                    }
                    z4 = z;
                }
                z3 = z4;
            }
        }
        return z3;
    }

    public synchronized boolean mo2404c() {
        boolean z;
        z = mo2406e().outdated;
        if (!z) {
            boolean z2 = !mo2401a(wk.m4367a(Long.valueOf(mo2406e().t), 0));
            z = (z2 || m4005a(((su) this.f2812f.mo2124d()).mo2184G())) ? z2 : true;
        }
        return z;
    }

    private boolean m4005a(@Nullable Map<String, String> map) {
        if (cx.m1193a((Map) map)) {
            return true;
        }
        return map.equals(we.m4340a(mo2406e().lastStartupRequestClids));
    }

    @VisibleForTesting
    public synchronized boolean mo2401a(long j) {
        boolean z;
        if (!((su) this.f2812f.mo2124d()).mo2088f()) {
            z = false;
        } else {
            long b = wi.m4360b() - j;
            z = b <= 86400 && b >= 0;
        }
        return z;
    }

    @VisibleForTesting
    public void mo2399a(@NonNull uk ukVar) {
        this.f2810d.a(ukVar);
    }

    @NonNull
    public su mo2405d() {
        return (su) this.f2812f.mo2124d();
    }

    private synchronized void m4014f() {
        this.f2811e = null;
    }

    public void mo2396a(@NonNull b bVar, @NonNull su suVar, @Nullable Map<String, List<String>> map) {
        uk a;
        synchronized (this) {
            Long valueOf = Long.valueOf(wk.m4367a(cq.m1001a(map), 0));
            m4004a(bVar.mo641t(), valueOf);
            a = mo2395a(bVar, suVar, valueOf);
            new ns().mo1513a(this.f2807a, new nq(a.deviceId, a.deviceIDHash), new pr(po.m2828b().mo1644a(a).mo1646a()));
            m4014f();
            m4010d(a);
        }
        m4008c(a);
    }

    @VisibleForTesting
    @NonNull
    public uk mo2395a(@NonNull b bVar, @NonNull su suVar, @Nullable Long l) {
        String a = we.m4339a(suVar.mo2184G());
        String a2 = m4001a(bVar.mo632k(), mo2406e().encodedClidsFromResponse);
        String str = mo2406e().deviceId;
        if (TextUtils.isEmpty(str)) {
            str = bVar.mo630i();
        }
        return new C1077a(bVar.mo620a()).mo2361a(wi.m4360b()).setDeviceId(str).mo2379c(mo2406e().deviceID2).mo2382d(bVar.mo631j()).mo2370a(mo2406e().uuid).mo2384e(bVar.mo626e()).mo2380c(bVar.mo624c()).mo2383d(suVar.mo2183F()).mo2371a(bVar.mo627f()).mo2376b(bVar.mo629h()).mo2386f(bVar.mo628g()).mo2388g(bVar.mo625d()).mo2385e(bVar.mo644w()).mo2390h(a2).mo2391i(a).mo2377b(m4006a(suVar.mo2184G(), a2)).mo2368a(bVar.mo635n()).mo2363a(bVar.mo639r()).mo2362a(bVar.mo640s()).mo2387f(bVar.mo642u()).mo2393k(bVar.mo643v()).mo2366a(bVar.mo636o()).mo2389g(bVar.mo638q()).mo2367a(bVar.mo637p()).mo2369a(bVar.mo645x()).mo2372a(true).mo2374b(wk.m4367a(l, wi.m4360b() * 1000)).mo2378c(((su) this.f2812f.mo2124d()).mo2195b(l.longValue())).mo2381c(false).mo2392j(mo2406e().distributionReferrer).mo2365a(bVar.mo646y()).mo2364a(bVar.mo619A()).mo2373a();
    }

    private void m4008c(@NonNull uk ukVar) {
        this.f2809c.a(this.f2808b.mo791b(), ukVar);
        mo2403b(ukVar);
    }

    public void mo2403b(uk ukVar) {
        dr.a().b((dt) new eb(this.f2808b.mo791b(), ukVar));
        if (!TextUtils.isEmpty(ukVar.uuid)) {
            dr.a().b((dt) new ec(ukVar.uuid, this.f2808b.mo791b()));
        }
        if (!TextUtils.isEmpty(ukVar.deviceId)) {
            dr.a().b((dt) new dy(ukVar.deviceId));
        }
        if (ukVar.socketConfig == null) {
            dr.a().mo725a(ea.class);
        } else {
            dr.a().b((dt) new ea(ukVar.socketConfig));
        }
    }

    private void m4004a(@Nullable Long l, @NonNull Long l2) {
        wd.m4332a().mo2554a(l2.longValue(), l);
    }

    private void m4010d(@NonNull uk ukVar) {
        this.f2812f.mo2121a(ukVar);
        mo2399a(ukVar);
        m4012e(ukVar);
    }

    @Nullable
    private static String m4001a(@Nullable String str, @Nullable String str2) {
        if (we.m4341b(str)) {
            return str;
        }
        if (we.m4341b(str2)) {
            return str2;
        }
        return null;
    }

    private boolean m4006a(Map<String, String> map, @Nullable String str) {
        Map a = we.m4340a(str);
        if (cx.m1193a((Map) map)) {
            return cx.m1193a(a);
        }
        return a.equals(map);
    }

    @Deprecated
    private void m4012e(uk ukVar) {
        if (!TextUtils.isEmpty(ukVar.deviceId)) {
            try {
                Log.d(Constant.RUS_TAG, "ul$m4012e creating intent with action SYNC");
                Intent intent = new Intent("com.yandex.metrica.intent.action.SYNC");
                intent.putExtra("CAUSE", "CAUSE_DEVICE_ID");
                intent.putExtra("SYNC_TO_PKG", this.f2808b.mo791b());
                intent.putExtra("SYNC_DATA", ukVar.deviceId);
                intent.putExtra("SYNC_DATA_2", ukVar.uuid);
                this.f2807a.sendBroadcast(intent);
            } catch (Throwable th) {
            }
        }
    }

    @NonNull
    public uk mo2406e() {
        return this.f2812f.mo2122b();
    }

    public void mo2398a(@NonNull ue ueVar) {
        m4014f();
        this.f2809c.a(b().mo791b(), ueVar, mo2406e());
    }

    public synchronized void mo2397a(@NonNull C0990a aVar) {
        this.f2812f.a(aVar);
        m4002a((su) this.f2812f.mo2124d());
    }

    private void m4002a(su suVar) {
        C1077a aVar = null;
        if (suVar.mo2187J()) {
            boolean z = false;
            List I = suVar.mo2186I();
            if (cx.a((Collection) I) && !cx.a((Collection) suVar.mo2183F())) {
                aVar = mo2406e().mo2359a().mo2383d(null);
                z = true;
            }
            if (!cx.a((Collection) I) && !cx.a((Object) I, (Object) suVar.mo2183F())) {
                aVar = mo2406e().mo2359a().mo2383d(I);
                z = true;
            }
            if (z) {
                m4010d(aVar.mo2373a());
            }
        }
    }

    public synchronized void mo2400a(String str) {
        m4010d(mo2406e().mo2359a().mo2392j(str).mo2373a());
    }

    private boolean m4007b(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean m4009c(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean m4011d(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean m4013e(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean m4015f(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }
}
