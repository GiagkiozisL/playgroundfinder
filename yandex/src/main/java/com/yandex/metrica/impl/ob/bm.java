package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

import java.util.concurrent.Executor;

/* renamed from: com.yandex.metrica.impl.ob.bm */
public class bm {
    @NonNull

    /* renamed from: a */
    private Executor f379a;

    public bm() {
        this(al.m324a().mo292j().mo2627b());
    }

    public bm(@NonNull Executor executor) {
        this.f379a = executor;
    }

    /* renamed from: a */
    public bl mo444a(@NonNull Context context, @NonNull ek ekVar) {
        bl blVar = new bl(context, ekVar, this.f379a);
        blVar.setName(xm.m4444a("YMM-NC[" + ekVar.mo792c() + "]"));
        blVar.start();
        return blVar;
    }
}
