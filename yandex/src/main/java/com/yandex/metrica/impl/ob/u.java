package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.CounterConfiguration.C0008a;

/* renamed from: com.yandex.metrica.impl.ob.u */
class u extends bz {
    public u(@NonNull ee eeVar) {
        super(eeVar, new CounterConfiguration());
        mo744h().mo22a(C0008a.COMMUTATION);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo546a() {
        return true;
    }
}
