package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ac.a.c;

/* renamed from: com.yandex.metrica.impl.ob.sz */
public class sz extends sy<st> {
    @Nullable

    /* renamed from: a */
    private sr f2524a;

    /* renamed from: B */
    private int f2525b;

    /* renamed from: a */
    public void mo2215a(@NonNull sr srVar) {
        this.f2524a = srVar;
    }

    /* renamed from: a */
    public void mo2213a(int i) {
        this.f2525b = i;
    }

    /* renamed from: a */
    public void mo2210a(@NonNull Builder builder, @NonNull st stVar) {
        super.mo2210a(builder, stVar);
        builder.path("report");
        m3740b(builder, stVar);
        m3741c(builder, stVar);
        builder.appendQueryParameter("request_id", String.valueOf(this.f2525b));
    }

    /* renamed from: B */
    private void m3740b(@NonNull Builder builder, @NonNull st stVar) {
        if (this.f2524a != null) {
            m3739a(builder, "deviceid", this.f2524a.f2452a, stVar.getDeviceId());
            m3739a(builder, "uuid", this.f2524a.f2453b, stVar.getUUId());
            m3738a(builder, "analytics_sdk_version", this.f2524a.f2454c);
            m3738a(builder, "analytics_sdk_version_name", this.f2524a.f2455d);
            m3739a(builder, "app_version_name", this.f2524a.f2458g, stVar.getAppVersionName());
            m3739a(builder, "app_build_number", this.f2524a.f2460i, stVar.getAppVersionCode());
            m3739a(builder, "os_version", this.f2524a.f2461j, stVar.getOsVersion());
            m3738a(builder, "os_api_level", this.f2524a.f2462k);
            m3738a(builder, "analytics_sdk_build_number", this.f2524a.f2456e);
            m3738a(builder, "analytics_sdk_build_type", this.f2524a.f2457f);
            m3738a(builder, "app_debuggable", "0"); //this.f2524a.f2459h);
            m3739a(builder, "locale", this.f2524a.f2463l, stVar.A());
            m3739a(builder, "is_rooted", this.f2524a.f2464m, stVar.getIsRooted());
            m3739a(builder, "app_framework", this.f2524a.f2465n, stVar.getAppFramework());
            m3738a(builder, "attribution_id", this.f2524a.f2466o);
        }
    }

    /* renamed from: a */
    private void m3741c(@NonNull Builder builder, @NonNull st stVar) {
        String a;
        builder.appendQueryParameter("api_key_128", stVar.getApiKey128());
        builder.appendQueryParameter("app_id", stVar.getAppId());
        builder.appendQueryParameter("app_platform", stVar.getAppPlatform());
        builder.appendQueryParameter("model", stVar.getModel());
        builder.appendQueryParameter("manufacturer", stVar.getManufacturer());
        builder.appendQueryParameter("screen_width", String.valueOf(stVar.getScreenWidth()));
        builder.appendQueryParameter("screen_height", String.valueOf(stVar.getScreenHeight()));
        builder.appendQueryParameter("screen_dpi", String.valueOf(stVar.getScreenDpi()));
        builder.appendQueryParameter("scalefactor", String.valueOf(stVar.getScaleFactore()));
        builder.appendQueryParameter("device_type", stVar.getDeviceType());
        builder.appendQueryParameter("android_id", stVar.getAndroidId());
        m3738a(builder, "clids_set", stVar.mo2150a());
        c D = stVar.mo2077D();
        String str = stVar.getAdvId();// D == null ? "" : D.advId;
        String str2 = "adv_id";
        if (str == null) {
            str = "";
        }
        builder.appendQueryParameter(str2, str);
        String str3 = "limit_ad_tracking";
        if (D == null) {
            a = "";
        } else {
            a = "1";// mo2211a(D.limitTracking);
        }
        builder.appendQueryParameter(str3, a);
    }

    /* renamed from: a */
    private void m3739a(Builder builder, String str, String str2, String str3) {
        builder.appendQueryParameter(str, cu.m1159c(str2, str3));
    }

    /* renamed from: a */
    private void m3738a(Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }
}
