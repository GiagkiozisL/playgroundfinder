package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.hs */
public class hs extends hd {
    public hs(en enVar) {
        super(enVar);
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        if (m1768b(wVar)) {
            mo973a().mo814a(wVar.mo2538l());
        }
        return false;
    }

    /* renamed from: B */
    private boolean m1768b(w wVar) {
        return !TextUtils.isEmpty(wVar.mo2538l()) && TextUtils.isEmpty(mo973a().mo821h());
    }
}
