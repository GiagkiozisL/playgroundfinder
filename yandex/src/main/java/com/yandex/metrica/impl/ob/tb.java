package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.tb */
public class tb {
    @NonNull

    /* renamed from: a */
    private mf<te> f2529a;
    @NonNull

    /* renamed from: B */
    private te f2530b;
    @NonNull

    /* renamed from: a */
    private wg f2531c;
    @NonNull

    /* renamed from: d */
    private tg f2532d;
    @NonNull

    /* renamed from: e */
    private C1000a f2533e;

    /* renamed from: com.yandex.metrica.impl.ob.tb$a */
    interface C1000a {
        /* renamed from: a */
        void mo2221a();
    }

    public tb(@NonNull Context context, @NonNull mf<te> mfVar, @NonNull C1000a aVar) {
        this(mfVar, aVar, new wg(), new tg(context, mfVar));
    }

    @VisibleForTesting
    tb(@NonNull mf<te> mfVar, @NonNull C1000a aVar, @NonNull wg wgVar, @NonNull tg tgVar) {
        this.f2529a = mfVar;
        this.f2530b = (te) this.f2529a.a();
        this.f2531c = wgVar;
        this.f2532d = tgVar;
        this.f2533e = aVar;
    }

    /* renamed from: a */
    public void mo2220a(@NonNull te teVar) {
        this.f2529a.a(teVar);
        this.f2530b = teVar;
        this.f2532d.mo2237a();
        this.f2533e.mo2221a();
    }

    /* renamed from: a */
    public void mo2219a() {
        te teVar = new te(this.f2530b.f2564a, this.f2530b.f2565b, this.f2531c.a(), true, true);
        this.f2529a.a(teVar);
        this.f2530b = teVar;
        this.f2533e.mo2221a();
    }
}
