package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.List;

@TargetApi(21)
/* renamed from: com.yandex.metrica.impl.ob.dl */
public class dl extends ScanCallback {
    @NonNull

    /* renamed from: a */
    private final dm f725a;

    public dl(@NonNull Context context, long j) {
        this(new dm(context, j));
    }

    public void onScanResult(int callbackType, ScanResult result) {
        super.onScanResult(callbackType, result);
        this.f725a.mo711a(result, Integer.valueOf(callbackType));
    }

    public void onBatchScanResults(List<ScanResult> results) {
        super.onBatchScanResults(results);
        this.f725a.mo712a(results);
    }

    public void onScanFailed(int errorCode) {
        super.onScanFailed(errorCode);
        this.f725a.mo710a(errorCode);
    }

    @VisibleForTesting
    dl(@NonNull dm dmVar) {
        this.f725a = dmVar;
    }
}
