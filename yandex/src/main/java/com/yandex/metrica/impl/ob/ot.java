package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.ot */
class ot {
    @NonNull

    /* renamed from: a */
    private final oq f1665a;
    @NonNull

    /* renamed from: B */
    private final ol f1666b;
    @NonNull

    /* renamed from: a */
    private final ow f1667c;
    @NonNull

    /* renamed from: d */
    private final od f1668d;


    @SuppressLint("WrongConstant")
    public ot(@NonNull Context var1, @NonNull uk var2, @NonNull xi var3, @Nullable oh var4, @NonNull lh var5, @NonNull lg var6, @NonNull pr var7) {
        this(var1, var3, (LocationManager)var1.getSystemService("location"), cy.m1204a(var1), al.m324a().mo293k(), var4, new ow(var1, var2, var4, var5, var6, var3), new od(var4, var5, var6), var7);
    }
//    public ot(@NonNull Context context, @NonNull uk ukVar, @NonNull xi xiVar, @Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull pr prVar) {
//        LocationManager locationManager = (LocationManager) context.getSystemService("location");
//        cy a = cy.m1204a(context);
//        vd k = al.m324a().mo293k();
//        this(context, xiVar, locationManager, a, k, ohVar, new ow(context, ukVar, ohVar, lhVar, lgVar, xiVar), new od(ohVar, lhVar, lgVar), prVar);
//    }

    private ot(@NonNull Context context, @NonNull xi xiVar, @Nullable LocationManager locationManager, @NonNull cy cyVar, @NonNull vd vdVar, @Nullable oh ohVar, @NonNull ow owVar, @NonNull od odVar, @NonNull pr prVar) {
        this(new oq(context, xiVar.b(), locationManager, ohVar, owVar, odVar, prVar), new ol(context, cyVar, vdVar, owVar, odVar, xiVar, ohVar), owVar, odVar);
    }

    /* renamed from: a */
    public void mo1600a() {
        this.f1665a.mo1582a();
        this.f1666b.mo1556d();
    }

    @Nullable
    /* renamed from: B */
    public Location mo1602b() {
        return this.f1665a.mo1584b();
    }

    @Nullable
    /* renamed from: a */
    public Location mo1603c() {
        return this.f1665a.mo1585c();
    }

    /* renamed from: d */
    public void mo1604d() {
        this.f1667c.mo1613a();
    }

    /* renamed from: e */
    public void mo1605e() {
        this.f1665a.mo1586d();
        this.f1666b.mo1552a();
    }

    /* renamed from: f */
    public void mo1606f() {
        this.f1665a.mo1587e();
        this.f1666b.mo1554b();
    }

    @VisibleForTesting
    ot(@NonNull oq oqVar, @NonNull ol olVar, @NonNull ow owVar, @NonNull od odVar) {
        this.f1665a = oqVar;
        this.f1666b = olVar;
        this.f1667c = owVar;
        this.f1668d = odVar;
    }

    /* renamed from: a */
    public void mo1601a(@NonNull uk ukVar, @Nullable oh ohVar) {
        this.f1667c.mo1614a(ukVar, ohVar);
        this.f1668d.mo1539a(ohVar);
        this.f1665a.mo1583a(ohVar);
        this.f1666b.mo1553a(ohVar);
    }
}
