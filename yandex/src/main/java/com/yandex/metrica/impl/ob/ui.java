package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.IParamsCallback;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ui */
public class ui {

    /* renamed from: a */
    private final Map<String, String> f2736a = new HashMap();

    /* renamed from: B */
    private List<String> f2737b;

    /* renamed from: a */
    private Map<String, String> f2738c;

    /* renamed from: d */
    private long f2739d;

    /* renamed from: e */
    private final lv f2740e;

    public ui(lv lvVar) {
        Map<String, String> map = null;
        this.f2740e = lvVar;
        m3935a("yandex_mobile_metrica_device_id", this.f2740e.mo1291b((String) null));
        m3935a("appmetrica_device_id_hash", this.f2740e.mo1295c((String) null));
        m3935a("yandex_mobile_metrica_uuid", this.f2740e.mo1289a((String) null));
        m3935a("yandex_mobile_metrica_get_ad_url", this.f2740e.mo1297d((String) null));
        m3935a("yandex_mobile_metrica_report_ad_url", this.f2740e.mo1300e((String) null));
        m3939b(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, this.f2740e.mo1304f((String) null));
        this.f2737b = this.f2740e.mo1292b();
        String p = this.f2740e.mo1315p(null);
        if (p != null) {
            map = we.m4340a(p);
        }
        this.f2738c = map;
        this.f2739d = this.f2740e.mo1286a(0);
        m3945g();
    }

    /* renamed from: a */
    public void mo2350a(@Nullable Map<String, String> map) {
        if (!cx.a((Object) map, (Object) this.f2738c)) {
            this.f2738c = map == null ? null : new HashMap(map);
            this.f2736a.remove(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS);
            m3945g();
        }
    }

    /* renamed from: a */
    public boolean mo2351a() {
        String str = (String) this.f2736a.get(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS);
        if (str == null) {
            return true;
        }
        if (str.isEmpty()) {
            return cx.m1193a((Map) this.f2738c);
        }
        return true;
    }

    /* renamed from: a */
    private void m3935a(@NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.f2736a.put(str, str2);
        }
    }

    /* renamed from: B */
    private void m3939b(@NonNull String str, @Nullable String str2) {
        if (str2 != null) {
            this.f2736a.put(str, str2);
        }
    }

    /* renamed from: a */
    private void m3934a(@Nullable String str) {
        if (TextUtils.isEmpty((CharSequence) this.f2736a.get("yandex_mobile_metrica_uuid")) && !TextUtils.isEmpty(str)) {
            m3935a("yandex_mobile_metrica_uuid", str);
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public synchronized void mo2349a(@NonNull List<String> list, Map<String, String> map) {
        for (String str : list) {
            String str2 = (String) this.f2736a.get(str);
            if (str2 != null) {
                map.put(str, str2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public synchronized boolean mo2354b() {
        return mo2352a(Arrays.asList(new String[]{IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, "appmetrica_device_id_hash", "yandex_mobile_metrica_device_id", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url", "yandex_mobile_metrica_uuid"}));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public synchronized boolean mo2352a(@NonNull List<String> list) {
        boolean z;
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = true;
                break;
            }
            String str = (String) it.next();
            if (IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS.equals(str)) {
                String str2 = (String) this.f2736a.get(str);
                if (str2 == null || (str2.isEmpty() && !cx.m1193a((Map) this.f2738c))) {
                    z = false;
                }
            } else if ("yandex_mobile_metrica_get_ad_url".equals(str) || "yandex_mobile_metrica_report_ad_url".equals(str)) {
                if (this.f2736a.get(str) == null) {
                    z = false;
                    break;
                }
            } else if (TextUtils.isEmpty((CharSequence) this.f2736a.get(str))) {
                z = false;
                break;
            }
        }
        z = false;
        return z;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public synchronized void mo2348a(@NonNull Bundle bundle) {
        m3943e(bundle);
        m3944f(bundle);
        m3942d(bundle);
        m3937b(bundle);
        m3945g();
    }

    /* renamed from: g */
    private void m3945g() {
        this.f2740e.mo1306g((String) this.f2736a.get("yandex_mobile_metrica_uuid")).mo1307h((String) this.f2736a.get("yandex_mobile_metrica_device_id")).mo1308i((String) this.f2736a.get("appmetrica_device_id_hash")).mo1309j((String) this.f2736a.get("yandex_mobile_metrica_get_ad_url")).mo1310k((String) this.f2736a.get("yandex_mobile_metrica_report_ad_url")).mo1296d(this.f2739d).mo1311l((String) this.f2736a.get(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS)).mo1314o(we.m4339a(this.f2738c)).mo1364q();
    }

    /* renamed from: B */
    private void m3937b(Bundle bundle) {
        if (m3941c(bundle)) {
            this.f2736a.put(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, bundle.getString("Clids"));
        }
    }

    /* renamed from: a */
    private boolean m3941c(@NonNull Bundle bundle) {
        Map a = we.m4340a(bundle.getString("RequestClids"));
        if (cx.m1193a((Map) this.f2738c)) {
            return cx.m1193a(a);
        }
        return this.f2738c.equals(a);
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo2347a(long j) {
        this.f2740e.mo1299e(j).mo1364q();
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public boolean mo2355c() {
        long b = wi.m4360b() - this.f2740e.mo1290b(0);
        return b > 86400 || b < 0;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: d */
    public List<String> mo2356d() {
        return this.f2737b;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo2353b(List<String> list) {
        this.f2737b = list;
        this.f2740e.mo1287a(this.f2737b);
    }

    /* renamed from: d */
    private void m3942d(Bundle bundle) {
        m3936b(bundle.getLong("ServerTimeOffset"));
    }

    /* renamed from: e */
    private synchronized void m3943e(@NonNull Bundle bundle) {
        m3934a(bundle.getString("Uuid"));
        m3935a("yandex_mobile_metrica_device_id", bundle.getString("DeviceId"));
        m3935a("appmetrica_device_id_hash", bundle.getString("DeviceIdHash"));
    }

    /* renamed from: f */
    private synchronized void m3944f(Bundle bundle) {
        String string = bundle.getString("AdUrlGet");
        if (string != null) {
            m3938b(string);
        }
        String string2 = bundle.getString("AdUrlReport");
        if (string2 != null) {
            m3940c(string2);
        }
    }

    /* renamed from: B */
    private synchronized void m3938b(String str) {
        this.f2736a.put("yandex_mobile_metrica_get_ad_url", str);
    }

    /* renamed from: a */
    private synchronized void m3940c(String str) {
        this.f2736a.put("yandex_mobile_metrica_report_ad_url", str);
    }

    /* renamed from: B */
    private synchronized void m3936b(long j) {
        this.f2739d = j;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: e */
    public String mo2357e() {
        return (String) this.f2736a.get("yandex_mobile_metrica_uuid");
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: f */
    public String mo2358f() {
        return (String) this.f2736a.get("yandex_mobile_metrica_device_id");
    }
}
