package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ac.a;

import java.util.List;

import me.android.ydx.DataManager;

public class ph extends sn {
    @Nullable

    private final oh f1725a;

    public static class C0751a {

        public final uk f1726a;

        public final oh f1727b;

        public C0751a(uk ukVar, oh ohVar) {
            this.f1726a = ukVar;
            this.f1727b = ohVar;
        }
    }

    protected static class C0752b implements d<ph, C0751a> {
        @NonNull
        private final Context f1728a;
        private String pkgName;

        protected C0752b(@NonNull Context context) {
            this.f1728a = context;
            this.pkgName = DataManager.getInstance().getCustomData().app_id;
        }

        @NonNull
        public ph a(C0751a aVar) {
            ph phVar = new ph(aVar.f1727b);
//            phVar.setVersionName(cx.b(this.f1728a, this.f1728a.getPackageName()));
            phVar.setVersionName(cx.b(this.f1728a, this.pkgName));
//            phVar.setVersionCode(cx.a(this.f1728a, this.f1728a.getPackageName()));
            phVar.setVersionCode(cx.a(this.f1728a, this.pkgName));
            phVar.setAndroidId(wk.m4373b(v.getInstance(this.f1728a).getAndroidId(aVar.f1726a), ""));
            phVar.mo2079a(aVar.f1726a);
            phVar.mo2080a(v.getInstance(this.f1728a));
//            phVar.mo2081b(this.f1728a.getPackageName());
            phVar.setUUID(aVar.f1726a.uuid);
            phVar.setDeviceId(aVar.f1726a.deviceId);
            phVar.setDeviceId2(aVar.f1726a.deviceID2);
            phVar.setAdvertisingInfos(a.m167a().mo192c(this.f1728a));
            return phVar;
        }
    }

    private ph(@Nullable oh ohVar) {
        this.f1725a = ohVar;
    }

    @Nullable
    public oh mo1633a() {
        return this.f1725a;
    }

    @Nullable
    public List<String> mo1634b() {
        return mo2085e().locationUrls;
    }
}
