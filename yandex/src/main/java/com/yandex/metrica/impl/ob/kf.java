package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.io.File;

/* renamed from: com.yandex.metrica.impl.ob.kf */
public class kf implements Runnable {
    @NonNull

    /* renamed from: a */
    private final File f1200a;
    @NonNull

    /* renamed from: B */
    private final wm<File> f1201b;

    public kf(@NonNull File file, @NonNull wm<File> wmVar) {
        this.f1200a = file;
        this.f1201b = wmVar;
    }

    public void run() {
        if (this.f1200a.exists() && this.f1200a.isDirectory()) {
            File[] listFiles = this.f1200a.listFiles();
            if (listFiles != null) {
                for (File a : listFiles) {
                    this.f1201b.a(a);
                }
            }
        }
    }
}
