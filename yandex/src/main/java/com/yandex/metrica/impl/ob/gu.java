package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.gu */
public class gu<T extends gt, C extends ei> extends gl<T, C> {
    public gu(@NonNull gr<T> grVar, @NonNull C c) {
        super(grVar, c);
    }

    /* renamed from: a */
    public boolean mo963a(@NonNull w wVar, @NonNull final fs fsVar) {
        return a(wVar, new a<T>() {
            /* renamed from: a */
            public boolean a(T t, w wVar) {
                return t.mo962a(wVar, fsVar);
            }
        });
    }
}
