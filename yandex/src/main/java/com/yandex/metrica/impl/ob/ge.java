package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.CounterConfiguration.C0008a;
import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.gi.C0388a;
import com.yandex.metrica.impl.ob.gi.C0389b;

import java.io.Closeable;

/* renamed from: com.yandex.metrica.impl.ob.ge */
public class ge implements et, eu, gj {
    ///void a(@NonNull C0306a aVar);
    @NonNull

    /* renamed from: a */
    private final Context f943a;
    @NonNull

    /* renamed from: B */
    private final ek f944b;
    @NonNull

    /* renamed from: a */
    private final ac f945c;
    @NonNull

    /* renamed from: d */
    private gf f946d;
    @NonNull

    /* renamed from: e */
    private fk f947e;
    @NonNull

    /* renamed from: f */
    private final C0008a f948f;

    /* renamed from: com.yandex.metrica.impl.ob.ge$a */
    static class C0380a {
        C0380a() {
        }

        /* renamed from: a */
        public gf mo932a(@NonNull Context context, @NonNull ek ekVar, @NonNull uk ukVar, @NonNull C0388a aVar) {
            return new gf(new C0389b(context, ekVar.mo791b()), ukVar, aVar);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ge$B */
    static class C0381b {
        C0381b() {
        }

        /* renamed from: a */
        public ac<ge> mo933a(@NonNull ge geVar, @NonNull un unVar, @NonNull bl blVar, @NonNull gk gkVar, @NonNull lw lwVar) {
            return new ac<>(geVar, unVar.mo2410a(), blVar, gkVar, lwVar);
        }
    }

    public ge(@NonNull Context context, @NonNull ek ekVar, @NonNull bl blVar, @NonNull C0306a aVar, @NonNull uk ukVar, @NonNull un unVar, @NonNull C0008a aVar2) {
        this(context, ekVar, blVar, aVar, ukVar, unVar, aVar2, new gk(), new C0381b(), new C0380a(), new fk(context, ekVar), new lw(ld.m2146a(context).mo1236b(ekVar)));
    }

    public ge(@NonNull Context context, @NonNull ek ekVar, @NonNull bl blVar, @NonNull C0306a aVar, @NonNull uk ukVar, @NonNull un unVar, @NonNull C0008a aVar2, @NonNull gk gkVar, @NonNull C0381b bVar, @NonNull C0380a aVar3, @NonNull fk fkVar, @NonNull lw lwVar) {
        this.f943a = context;
        this.f944b = ekVar;
        this.f947e = fkVar;
        this.f948f = aVar2;
        this.f945c = bVar.mo933a(this, unVar, blVar, gkVar, lwVar);
        synchronized (this) {
            this.f947e.mo896a(ukVar.statSending);
            this.f946d = aVar3.mo932a(context, ekVar, ukVar, new C0388a(aVar));
        }
    }

    @NonNull
    /* renamed from: B */
    public ek b() {
        return this.f944b;
    }

    /* renamed from: a */
//    public void a(@NonNull C0306a aVar) {
//        this.f946d.mo2118a(aVar);
//    }

    @Override
    public void a(@NonNull C0306a aVar) {
//        this.f946d.mo2118a(aVar); // todo ??
    }

    /* renamed from: a */
    public void a(@NonNull w wVar) {
        this.f945c.mo253a(wVar);
    }

    /* renamed from: a */
    public void a() {
        if (this.f947e.mo897a(((gi) this.f946d.mo2124d()).mo941c())) {
            a(af.m269a());
            this.f947e.mo895a();
        }
    }

    /* renamed from: a */
    public synchronized void a(@Nullable uk ukVar) {
        this.f946d.mo2121a(ukVar);
        this.f947e.mo896a(ukVar.statSending);
    }

    /* renamed from: a */
    public void a(@NonNull ue ueVar, @Nullable uk ukVar) {
    }

    @NonNull
    /* renamed from: d */
    public gi d() {
        return (gi) this.f946d.mo2124d();
    }

    /* renamed from: a */
    public void c() {
        cx.a((Closeable) this.f945c);
    }
}
