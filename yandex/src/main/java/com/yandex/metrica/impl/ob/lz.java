package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.rr.a.C0863a;
import com.yandex.metrica.impl.ob.rr.a.C0873e;
import com.yandex.metrica.impl.ob.tv.C1053a;
import com.yandex.metrica.impl.ob.tv.C1054b;
import com.yandex.metrica.impl.ob.uk.C1077a;

import org.json.JSONObject;

import java.util.List;

@Deprecated
/* renamed from: com.yandex.metrica.impl.ob.lz */
public class lz extends lx {

    /* renamed from: a */
    static final qk f1425a = new qk("PREF_KEY_UID_");

    /* renamed from: B */
    static final qk f1426b = new qk("PREF_KEY_DEVICE_ID_");

    /* renamed from: a */
    private static final qk f1427c = new qk("PREF_KEY_HOST_URL_");

    /* renamed from: d */
    private static final qk f1428d = new qk("PREF_KEY_HOST_URLS_FROM_STARTUP");

    /* renamed from: e */
    private static final qk f1429e = new qk("PREF_KEY_HOST_URLS_FROM_CLIENT");
    @Deprecated

    /* renamed from: f */
    private static final qk f1430f = new qk("PREF_KEY_REPORT_URL_");

    /* renamed from: g */
    private static final qk f1431g = new qk("PREF_KEY_REPORT_URLS_");
    @Deprecated

    /* renamed from: h */
    private static final qk f1432h = new qk("PREF_L_URL");

    /* renamed from: i */
    private static final qk f1433i = new qk("PREF_L_URLS");

    /* renamed from: j */
    private static final qk f1434j = new qk("PREF_KEY_GET_AD_URL");

    /* renamed from: k */
    private static final qk f1435k = new qk("PREF_KEY_REPORT_AD_URL");

    /* renamed from: l */
    private static final qk f1436l = new qk("PREF_KEY_STARTUP_OBTAIN_TIME_");

    /* renamed from: m */
    private static final qk f1437m = new qk("PREF_KEY_STARTUP_ENCODED_CLIDS_");

    /* renamed from: n */
    private static final qk f1438n = new qk("PREF_KEY_DISTRIBUTION_REFERRER_");

    /* renamed from: o */
    private static final qk f1439o = new qk("STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_");
    @Deprecated

    /* renamed from: p */
    private static final qk f1440p = new qk("PREF_KEY_PINNING_UPDATE_URL");

    /* renamed from: r */
    private static final qk f1441r = new qk("PREF_KEY_EASY_COLLECTING_ENABLED_");

    /* renamed from: s */
    private static final qk f1442s = new qk("PREF_KEY_COLLECTING_PACKAGE_INFO_ENABLED_");

    /* renamed from: t */
    private static final qk f1443t = new qk("PREF_KEY_PERMISSIONS_COLLECTING_ENABLED_");

    /* renamed from: u */
    private static final qk f1444u = new qk("PREF_KEY_FEATURES_COLLECTING_ENABLED_");

    /* renamed from: v */
    private static final qk f1445v = new qk("SOCKET_CONFIG_");

    /* renamed from: w */
    private static final qk f1446w = new qk("LAST_STARTUP_REQUEST_CLIDS");

    /* renamed from: x */
    private static final qk f1447x = new qk("FLCC");

    /* renamed from: y */
    private static final qk f1448y = new qk("BKCC");

    /* renamed from: A */
    private qk f1449A = mo1363q(f1425a.mo1742a());

    /* renamed from: B */
    private qk f1450B = mo1363q(f1427c.mo1742a());

    /* renamed from: C */
    private qk f1451C = mo1363q(f1428d.mo1742a());

    /* renamed from: D */
    private qk f1452D = mo1363q(f1429e.mo1742a());
    @Deprecated

    /* renamed from: E */
    private qk f1453E = mo1363q(f1430f.mo1742a());

    /* renamed from: F */
    private qk f1454F = mo1363q(f1431g.mo1742a());
    @Deprecated

    /* renamed from: G */
    private qk f1455G = mo1363q(f1432h.mo1742a());

    /* renamed from: H */
    private qk f1456H = mo1363q(f1433i.mo1742a());

    /* renamed from: I */
    private qk f1457I = mo1363q(f1434j.mo1742a());

    /* renamed from: J */
    private qk f1458J = mo1363q(f1435k.mo1742a());

    /* renamed from: K */
    private qk f1459K = mo1363q(f1436l.mo1742a());

    /* renamed from: L */
    private qk f1460L = mo1363q(f1437m.mo1742a());

    /* renamed from: M */
    private qk f1461M = mo1363q(f1438n.mo1742a());

    /* renamed from: N */
    private qk f1462N = mo1363q(f1439o.mo1742a());

    /* renamed from: O */
    private qk f1463O = mo1363q(f1441r.mo1742a());

    /* renamed from: P */
    private qk f1464P = mo1363q(f1442s.mo1742a());

    /* renamed from: Q */
    private qk f1465Q = mo1363q(f1443t.mo1742a());

    /* renamed from: R */
    private qk f1466R = mo1363q(f1444u.mo1742a());

    /* renamed from: S */
    private qk f1467S = mo1363q(f1445v.mo1742a());

    /* renamed from: T */
    private qk f1468T = mo1363q(f1446w.mo1742a());

    /* renamed from: U */
    private qk f1469U = mo1363q(f1447x.mo1742a());

    /* renamed from: V */
    private qk f1470V = mo1363q(f1448y.mo1742a());

    /* renamed from: z */
    private qk f1471z = new qk(f1426b.mo1742a());

    public lz(lf lfVar, String str) {
        super(lfVar, str);
    }

    /* renamed from: a */
    public lz mo1398a(String str) {
        return (lz) mo1357b(this.f1449A.mo1744b(), str);
    }

    @Deprecated
    /* renamed from: B */
    public lz mo1402b(String str) {
        return (lz) mo1357b(this.f1471z.mo1744b(), str);
    }

    @Deprecated
    /* renamed from: a */
    public lz mo1404c(String str) {
        return (lz) mo1357b(this.f1453E.mo1744b(), str);
    }

    /* renamed from: a */
    public lz mo1399a(List<String> list) {
        return (lz) mo1357b(this.f1454F.mo1744b(), vq.m4213a(list));
    }

    /* renamed from: B */
    public lz mo1403b(List<String> list) {
        return (lz) mo1357b(this.f1456H.mo1744b(), vq.m4213a(list));
    }

    /* renamed from: d */
    public lz mo1405d(String str) {
        return (lz) mo1357b(this.f1458J.mo1744b(), str);
    }

    /* renamed from: e */
    public lz mo1406e(String str) {
        return (lz) mo1357b(this.f1457I.mo1744b(), str);
    }

    /* renamed from: f */
    public lz mo1407f(String str) {
        return (lz) mo1357b(this.f1450B.mo1744b(), str);
    }

    /* renamed from: a */
    public lz mo1397a(long j) {
        return (lz) mo1351a(this.f1459K.mo1744b(), j);
    }

    /* renamed from: g */
    public lz mo1408g(String str) {
        return (lz) mo1357b(this.f1460L.mo1744b(), str);
    }

    /* renamed from: h */
    public lz mo1409h(String str) {
        return (lz) mo1357b(this.f1461M.mo1744b(), str);
    }

    /* renamed from: a */
    public lz mo1400a(boolean z) {
        return (lz) mo1353a(this.f1462N.mo1744b(), z);
    }

    @Deprecated
    @NonNull
    /* renamed from: a */
    public uk mo1401a() {
        return new C1077a(new C1053a().mo2304a(mo1359b(this.f1463O.mo1744b(), C1054b.f2685a)).mo2306b(mo1359b(this.f1464P.mo1744b(), C1054b.f2686b)).mo2307c(mo1359b(this.f1465Q.mo1744b(), C1054b.f2687c)).mo2308d(mo1359b(this.f1466R.mo1744b(), C1054b.f2688d)).mo2305a()).mo2370a(mo1366s(this.f1449A.mo1744b())).mo2380c(vq.m4228c(mo1366s(this.f1451C.mo1744b()))).mo2383d(vq.m4228c(mo1366s(this.f1452D.mo1744b()))).mo2390h(mo1366s(this.f1460L.mo1744b())).mo2371a(vq.m4228c(mo1366s(this.f1454F.mo1744b()))).mo2376b(vq.m4228c(mo1366s(this.f1456H.mo1744b()))).mo2384e(mo1366s(this.f1457I.mo1744b())).mo2386f(mo1366s(this.f1458J.mo1744b())).mo2392j(mo1361c(this.f1461M.mo1744b(), null)).mo2363a(m2372k(mo1366s(this.f1469U.mo1744b()))).mo2362a(m2373l(mo1366s(this.f1470V.mo1744b()))).mo2368a(ub.m3889a(mo1366s(this.f1467S.mo1744b()))).mo2391i(mo1366s(this.f1468T.mo1744b())).mo2377b(mo1359b(this.f1462N.mo1744b(), true)).mo2361a(mo1356b(this.f1459K.mo1744b(), -1)).mo2373a();
    }

    @Nullable
    /* renamed from: k */
    private oh m2372k(@Nullable String str) {
        oh ohVar = null;
        if (TextUtils.isEmpty(str)) {
            return ohVar;
        }
        try {
            return new ms().a(m2370a(new JSONObject(str)));
        } catch (Throwable th) {
            return ohVar;
        }
    }

    /* renamed from: a */
    static C0873e m2370a(@NonNull JSONObject jSONObject) {
        C0873e eVar = new C0873e();
        eVar.f2173b = wk.m4367a(vq.m4211a(jSONObject, "uti"), eVar.f2173b);
        eVar.f2174c = wk.m4365a(vq.m4229d(jSONObject, "udi"), eVar.f2174c);
        eVar.f2175d = wk.m4366a(vq.m4224b(jSONObject, "rcff"), eVar.f2175d);
        eVar.f2176e = wk.m4366a(vq.m4224b(jSONObject, "mbs"), eVar.f2176e);
        eVar.f2177f = wk.m4367a(vq.m4211a(jSONObject, "maff"), eVar.f2177f);
        eVar.f2178g = wk.m4366a(vq.m4224b(jSONObject, "mrsl"), eVar.f2178g);
        eVar.f2179h = wk.m4371a(vq.m4227c(jSONObject, "ce"), eVar.f2179h);
        return eVar;
    }

    @Nullable
    /* renamed from: l */
    private oc m2373l(@Nullable String str) {
        oc ocVar = null;
        if (TextUtils.isEmpty(str)) {
            return ocVar;
        }
        try {
            return m2371b(new JSONObject(str));
        } catch (Throwable th) {
            return ocVar;
        }
    }

    /* renamed from: B */
    private oc m2371b(@NonNull JSONObject jSONObject) {
        C0863a aVar = new C0863a();
        aVar.f2125b = m2370a(jSONObject);
        aVar.f2126c = wk.m4367a(vq.m4211a(jSONObject, "cd"), aVar.f2126c);
        aVar.f2127d = wk.m4367a(vq.m4211a(jSONObject, "ci"), aVar.f2127d);
        return new mn().a(aVar);
    }

    @Deprecated
    /* renamed from: i */
    public String mo1410i(String str) {
        return mo1361c(this.f1453E.mo1744b(), str);
    }

    @Deprecated
    /* renamed from: j */
    public String mo1411j(String str) {
        return mo1361c(this.f1455G.mo1744b(), str);
    }
}
