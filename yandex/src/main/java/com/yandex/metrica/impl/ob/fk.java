package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.fk */
public class fk {
    @NonNull

    /* renamed from: a */
    private final lw f904a;
    @NonNull

    /* renamed from: B */
    private final wh f905b;
    @NonNull

    /* renamed from: a */
    private final cw f906c;
    @Nullable

    /* renamed from: d */
    private um f907d;

    /* renamed from: e */
    private long f908e;

    public fk(@NonNull Context context, @NonNull ek ekVar) {
        this(new lw(ld.m2146a(context).mo1236b(ekVar)), new wg(), new cw());
    }

    public fk(@NonNull lw lwVar, @NonNull wh whVar, @NonNull cw cwVar) {
        this.f904a = lwVar;
        this.f905b = whVar;
        this.f906c = cwVar;
        this.f908e = this.f904a.mo1345k();
    }

    /* renamed from: a */
    public boolean mo897a(@Nullable Boolean bool) {
        return vi.m4194c(bool) && this.f907d != null && this.f906c.mo663b(this.f908e, this.f907d.f2813a, "should report diagnostic");
    }

    /* renamed from: a */
    public void mo895a() {
        this.f908e = this.f905b.a();
        this.f904a.mo1339f(this.f908e).mo1364q();
    }

    /* renamed from: a */
    public void mo896a(@Nullable um umVar) {
        this.f907d = umVar;
    }
}
