package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.jr */
public class jr {
    @NonNull

    /* renamed from: a */
    private final xh f1162a;

    /* renamed from: com.yandex.metrica.impl.ob.jr$a */
    public static class C0509a implements Runnable {
        @NonNull

        /* renamed from: a */
        private final jy f1163a;
        @Nullable

        /* renamed from: B */
        private final Bundle f1164b;
        @Nullable

        /* renamed from: a */
        private final jw f1165c;

        public C0509a(@NonNull jy jyVar, @Nullable Bundle bundle) {
            this(jyVar, bundle, null);
        }

        public C0509a(@NonNull jy jyVar, @Nullable Bundle bundle, @Nullable jw jwVar) {
            this.f1163a = jyVar;
            this.f1164b = bundle;
            this.f1165c = jwVar;
        }

        public void run() {
            try {
                this.f1163a.mo1136a(this.f1164b, this.f1165c);
            } catch (Throwable th) {
                if (this.f1165c != null) {
                    this.f1165c.a();
                }
            }
        }
    }

    public jr() {
        this(al.m324a().mo292j().mo2631f());
    }

    @VisibleForTesting
    jr(@NonNull xh xhVar) {
        this.f1162a = xhVar;
    }

    /* renamed from: a */
    public void mo1120a(@NonNull jy jyVar, @Nullable Bundle bundle) {
        this.f1162a.a((Runnable) new C0509a(jyVar, bundle));
    }

    /* renamed from: a */
    public void mo1121a(@NonNull jy jyVar, @Nullable Bundle bundle, @Nullable jw jwVar) {
        this.f1162a.a((Runnable) new C0509a(jyVar, bundle, jwVar));
    }

    @NonNull
    /* renamed from: a */
    public xh mo1119a() {
        return this.f1162a;
    }
}
