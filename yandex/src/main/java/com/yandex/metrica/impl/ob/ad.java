package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.usage.StorageStatsManager;
import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.UUID;

/* renamed from: com.yandex.metrica.impl.ob.ad */
public class ad {
    @NonNull

    /* renamed from: a */
    private final Context f150a;
    @NonNull

    /* renamed from: B */
    private final C0055b f151b;

    /* renamed from: com.yandex.metrica.impl.ob.ad$a */
    public static class C0054a {

        /* renamed from: a */
        public final long f152a;

        /* renamed from: B */
        public final long f153b;

        public C0054a(long j, long j2) {
            this.f152a = j;
            this.f153b = j2;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ad$B */
    static class C0055b {
        C0055b() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        /* renamed from: a */
        public StatFs mo256a() {
            return new StatFs(Environment.getDataDirectory().getAbsolutePath());
        }
    }

    public ad(@NonNull Context context) {
        this(context, new C0055b());
    }

    @VisibleForTesting
    ad(@NonNull Context context, @NonNull C0055b bVar) {
        this.f150a = context;
        this.f151b = bVar;
    }

    @NonNull
    /* renamed from: a */
    public C0054a mo255a() {
        if (cx.a(26)) {
            return m261b();
        }
        if (cx.a(18)) {
            return m262c();
        }
        return m263d();
    }

    @TargetApi(26)
    @NonNull
    /* renamed from: B */
    private C0054a m261b() {
        long j;
        long j2;
        long j3;
        StorageStatsManager storageStatsManager = (StorageStatsManager) this.f150a.getSystemService("storagestats");
        StorageManager storageManager = (StorageManager) this.f150a.getSystemService("storage");
        if (storageManager == null || storageStatsManager == null) {
            j2 = 0;
            j = 0;
        } else {
            j2 = 0;
            j = 0;
            for (StorageVolume uuid : storageManager.getStorageVolumes()) {
                try {
                    String uuid2 = uuid.getUuid();
                    UUID fromString = uuid2 == null ? StorageManager.UUID_DEFAULT : UUID.fromString(uuid2);
                    j3 = storageStatsManager.getTotalBytes(fromString) + j;
                    j2 += storageStatsManager.getFreeBytes(fromString);
                } catch (Throwable th) {
                    j3 = j;
                }
                j = j3;
            }
        }
        return new C0054a(j / 1024, j2 / 1024);
    }

    @TargetApi(18)
    @NonNull
    /* renamed from: a */
    private C0054a m262c() {
        try {
            StatFs a = this.f151b.mo256a();
            long blockSizeLong = a.getBlockSizeLong();
            return new C0054a((a.getBlockCountLong() * blockSizeLong) / 1024, (blockSizeLong * a.getAvailableBlocksLong()) / 1024);
        } catch (Throwable th) {
            return new C0054a(0, 0);
        }
    }

    @NonNull
    /* renamed from: d */
    private C0054a m263d() {
        try {
            StatFs a = this.f151b.mo256a();
            long blockSize = (long) a.getBlockSize();
            return new C0054a((((long) a.getBlockCount()) * blockSize) / 1024, (blockSize * ((long) a.getAvailableBlocks())) / 1024);
        } catch (Throwable th) {
            return new C0054a(0, 0);
        }
    }
}
