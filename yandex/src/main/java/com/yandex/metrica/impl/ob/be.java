package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.CounterConfiguration;

import com.yandex.metrica.impl.ac.a;
import com.yandex.metrica.impl.ob.eg.C0306a;
import com.yandex.metrica.impl.ob.np.C0678a;
import com.yandex.metrica.impl.ob.su.C0990a;
import com.yandex.metrica.impl.ob.ts.C1038a;

import java.io.File;

import me.android.ydx.C0015c;
import me.android.ydx.Constant;

public class be implements bc {
    @Nullable

    public uk f320a;
    @NonNull

    public final Context f321b;
    @NonNull

    private final C0015c f322c;
    @NonNull

    public final C1038a f323d;
    @Nullable

    public ts f324e;
    @NonNull

    public aq f325f;
    @NonNull

    private final xh f326g;
    @NonNull

    public fo f327h;
    @NonNull

    private final fu f328i;
    @NonNull

    private final bf f329j;
    @Nullable

    private ox f330k;
    @NonNull

    private final ly f331l;

    private ku f332m;

    @VisibleForTesting
    final class C0118a implements Runnable {

        private final w f345b;

        private final Bundle f346c;

        private final Context f347d;

        C0118a(Context context, w wVar, Bundle bundle) {
            this.f347d = context.getApplicationContext();
            this.f345b = wVar;
            this.f346c = bundle;
        }

        public void run() {
            be.this.f325f.mo311a();
            ed edVar = new ed(this.f346c);
            if (!be.this.mo404a(edVar)) {
                fn a = fn.m1619a(edVar);
                if (a != null) {
                    eg egVar = new eg(edVar);
                    be.this.f327h.mo916a(a, egVar).a(this.f345b, egVar);
                }
            }
        }
    }

    public be(@NonNull Context context, @NonNull C0015c cVar) {
        this(context, cVar, new fu(context));
    }

    private be(@NonNull Context context, @NonNull C0015c cVar, @NonNull fu fuVar) {
        this(context, cVar, fuVar, new fo(context, fuVar), new bf(), new C1038a(), new ly(ld.m2146a(context).mo1238c()), new ah());
    }

    @VisibleForTesting
    be(@NonNull Context context, @NonNull C0015c cVar, @NonNull fu fuVar, @NonNull fo foVar, @NonNull bf bfVar, @NonNull C1038a aVar, @NonNull ly lyVar, @NonNull ah ahVar) {
        this.f321b = context;
        this.f322c = cVar;
        this.f327h = foVar;
        this.f328i = fuVar;
        this.f329j = bfVar;
        this.f323d = aVar;
        this.f331l = lyVar;
        this.f326g = al.m324a().mo292j().mo2628c();
        this.f332m = new ku(ahVar.mo266b(this.f321b), new wm<File>() {
            public void a(File file) {
                be.this.mo403a(file);
            }
        });
    }

    public void a() {
        new cm(this.f321b).mo426a(this.f321b);
        wd.m4332a().mo2555a(this.f321b);
        this.f330k = new ox(oo.m2694a(this.f321b), al.m324a().mo293k(), cy.m1204a(this.f321b), this.f331l);
        m548c();
        dr.a().a(this, eb.class, dv.m1388a((du<eb>) new du<eb>() {
            public void a(eb ebVar) {
                be.this.m547b(ebVar.f757b);
            }
        }).mo735a(new ds<eb>() {
            public boolean a(eb ebVar) {
                return false; // todo o o o o o check bool ????
//                return !be.this.f321b.getPackageName().equals(ebVar.f756a);
            }
        }).mo736a());
        this.f320a = (uk) C0678a.m2599a(uk.class).a(this.f321b).a();
        this.f325f = new aq(this.f331l, this.f321b, this.f320a.identityLightCollectingConfig);
        a.m167a().mo190a(this.f321b, this.f320a);
        m558f();
        al.m324a().mo287e().a();
        al.m324a().mo291i().mo968a();
        this.f332m.mo1165a();
    }

    private void m548c() {
        this.f329j.mo414a((bf.C0125b) new bf.C0125b() {
            public void mo412a() {
                be.this.m550c(be.this.f320a);
                be.this.m561h();
            }
        });
        this.f329j.mo415b((bf.C0125b) new bf.C0125b() {
            public void mo412a() {
                be.this.m550c(be.this.f320a);
                be.this.m559g();
            }
        });
        this.f329j.mo416c((bf.C0125b) new bf.C0125b() {
            public void mo412a() {
                be.this.m550c(be.this.f320a);
                be.this.m566j();
                be.this.f324e = be.this.f323d.mo2280a(be.this.f321b);
            }
        });
        this.f329j.mo418d((bf.C0125b) new bf.C0125b() {
            public void mo412a() {
                be.this.m551d();
            }
        });
        this.f329j.mo419e((bf.C0125b) new bf.C0125b() {
            public void mo412a() {
                be.this.m554e();
            }
        });
    }

    public void a(Intent intent, int i) {
        m545b(intent, i);
    }

    public void a(Intent intent, int i, int i2) {
        m545b(intent, i2);
    }

    public void a(Intent intent) {
        this.f329j.a(intent);
    }

    public void b(Intent intent) {
        this.f329j.b(intent);
    }

    public void c(Intent intent) {
        Log.d(Constant.RUS_TAG, "be$c");
        this.f329j.c(intent);
        if (intent != null) {
            String action = intent.getAction();
            Uri data = intent.getData();
            String encodedAuthority = data == null ? null : data.getEncodedAuthority();
            if ("com.yandex.metrica.IMetricaService".equals(action)) {
                Log.d(Constant.RUS_TAG, "be$c if true");
                mo401a(data, encodedAuthority);
            }
        }
    }

    public void m547b(@NonNull uk ukVar) {
        this.f320a = ukVar;
        m564i();
        m550c(ukVar);
        al.m324a().mo283a(ukVar);
        this.f325f.mo312a(this.f320a.identityLightCollectingConfig);
    }

    public void m551d() {
        if (this.f324e != null) {
            this.f324e.mo2264b();
        }
    }

    public void m554e() {
        if (this.f324e != null) {
            this.f324e.mo2260a();
        }
    }

    @VisibleForTesting
    public void mo401a(@Nullable Uri uri, @Nullable String str) {
        if (uri != null && uri.getPath().equals("/client")) {
            this.f327h.mo917a(str, Integer.parseInt(uri.getQueryParameter("pid")), uri.getQueryParameter("psid"));
        }
        if (this.f327h.mo915a() <= 0) {
            m554e();
        }
    }

    public void b() {
        this.f332m.mo1166b();
        m559g();
        this.f328i.c();
        dr.a().mo726a((Object) this);
    }

    public void a(String str, int i, String str2, Bundle bundle) {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        m543a(new w(str2, str, i), bundle);
    }

    public void a(Bundle bundle) {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        m543a(w.m4280b(bundle), bundle);
    }

    public void mo403a(@NonNull File file) {
        this.f326g.a((Runnable) new ke(this.f321b, file, new wm<kv>() {
            public void a(kv kvVar) {
                be.this.m542a(new fn(kvVar.mo1174h(), kvVar.mo1173g(), kvVar.mo1171e(), kvVar.mo1172f(), kvVar.mo1175i()), af.m277a(kvVar.mo1168b(), kvVar.mo1167a(), kvVar.mo1169c(), kvVar.mo1170d(), vr.m4237a(kvVar.mo1174h())), new eg(new C0990a(), new C0306a(), null));
            }
        }));
    }

    private void m558f() {
        if (this.f320a != null) {
            mo402a(this.f320a);
        }
        m550c(this.f320a);
    }

    public void mo402a(@NonNull uk ukVar) {
        ub ubVar = ukVar.socketConfig;
        if (ubVar == null) {
            dr.a().mo725a(ea.class);
        } else {
            dr.a().b((dt) new ea(ubVar));
        }
    }

    private void m545b(Intent intent, int i) {
        if (intent != null) {
            intent.getExtras().setClassLoader(CounterConfiguration.class.getClassLoader());
            m555e(intent);
        }
        this.f322c.mo109a(i);
    }

    public void m559g() {
        if (this.f330k != null) {
            this.f330k.mo1616a((Object) this);
        }
    }

    public void m561h() {
        if (this.f330k != null) {
            this.f330k.mo1618b(this);
        }
    }

    private boolean m553d(Intent intent) {
        return intent == null || intent.getData() == null;
    }

    @VisibleForTesting
    public boolean mo404a(@Nullable ed edVar) {
        return edVar == null ||
                edVar.mo743g() == null ||
//                !this.f321b.getPackageName().equals(edVar.mo743g().mo759h()) || //todo o o o check?
                edVar.mo743g().mo758g() != 85;
    }

    private void m555e(Intent intent) {
        if (!m553d(intent)) {
            ed edVar = new ed(intent.getExtras());
            if (!mo404a(edVar)) {
                w b = w.m4280b(intent.getExtras());
                if (!b.mo2539m() && !b.mo2540n()) {
                    try {
                        m542a(fn.m1619a(edVar), b, new eg(edVar));
                    } catch (Throwable th) {
                    }
                }
            }
        }
    }

    public void m542a(@NonNull fn fnVar, @NonNull w wVar, @NonNull eg egVar) {
        this.f327h.mo916a(fnVar, egVar).a(wVar, egVar);
        this.f327h.mo917a(fnVar.mo908b(), fnVar.mo909c().intValue(), fnVar.mo910d());
    }

    private void m543a(w wVar, Bundle bundle) {
        if (!wVar.mo2540n()) {
            this.f326g.a((Runnable) new C0118a(this.f321b, wVar, bundle));
        }
    }

    public void m550c(@NonNull uk ukVar) {
        if (this.f330k != null) {
            this.f330k.mo1615a(ukVar, this.f329j.mo417c());
        }
    }

    private void m564i() {
        final jk jkVar = new jk(this.f321b);
        al.m324a().mo292j().mo2634i().a((Runnable) new Runnable() {
            public void run() {
                try {
                    jkVar.mo1105a();
                } catch (Throwable th) {
                }
            }
        });
    }

    public void m566j() {
        if (this.f320a != null) {
            al.m324a().mo288f().mo2230a(this.f320a);
        }
    }
}
