package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.mg */
public class mg<T, P extends e> implements mf<T> {
    @NonNull

    /* renamed from: a */
    private final String f1476a;
    @NonNull

    /* renamed from: B */
    private final kx f1477b;
    @NonNull

    /* renamed from: a */
    private final me<P> f1478c;
    @NonNull

    /* renamed from: d */
    private final mq<T, P> f1479d;

    public mg(@NonNull String str, @NonNull kx kxVar, @NonNull me<P> meVar, @NonNull mq<T, P> mqVar) {
        this.f1476a = str;
        this.f1477b = kxVar;
        this.f1478c = meVar;
        this.f1479d = mqVar;
    }

    /* renamed from: a */
    public void a(@NonNull T t) {
        this.f1477b.mo1191a(this.f1476a, this.f1478c.a(this.f1479d.b(t)));
    }

    @NonNull
    /* renamed from: a */
    public T a() {
        try {
            byte[] a = this.f1477b.mo1192a(this.f1476a);
            if (cx.nullOrEmpty(a)) {
                return this.f1479d.a(this.f1478c.c());
            }
            return this.f1479d.a(this.f1478c.b(a));
        } catch (Throwable th) {
            return this.f1479d.a(this.f1478c.c());
        }
    }
}
