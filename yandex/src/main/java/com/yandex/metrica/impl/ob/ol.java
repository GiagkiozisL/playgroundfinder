package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.yandex.metrica.impl.ob.ol */
public class ol {
    @NonNull

    /* renamed from: a */
    private final cy f1598a;
    @NonNull

    /* renamed from: B */
    private final vd f1599b;
    @NonNull

    /* renamed from: a */
    private final ow f1600c;
    @NonNull

    /* renamed from: d */
    private final om f1601d;
    @NonNull

    /* renamed from: e */
    private final od f1602e;
    @NonNull

    /* renamed from: f */
    private final wg f1603f;
    @NonNull

    /* renamed from: g */
    private final xh f1604g;
    @Nullable

    /* renamed from: h */
    private oh f1605h;

    /* renamed from: i */
    private boolean f1606i;

    /* renamed from: j */
    private final Runnable f1607j;

    public ol(@NonNull Context context, @NonNull cy cyVar, @NonNull vd vdVar, @NonNull ow owVar, @NonNull od odVar, @NonNull xh xhVar, @Nullable oh ohVar) {
        this(cyVar, vdVar, owVar, new om(context), new wg(), odVar, xhVar, ohVar);
    }

    /* renamed from: a */
    public void mo1552a() {
        mo1555c();
    }

    /* renamed from: B */
    public void mo1554b() {
        m2675f();
    }

    /* renamed from: a */
    public void mo1555c() {
        boolean z = this.f1605h != null && this.f1605h.f1579m;
        if (this.f1606i != z) {
            this.f1606i = z;
            if (this.f1606i) {
                this.f1606i = true;
                m2674e();
                return;
            }
            this.f1606i = false;
            m2675f();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m2674e() {
        if (this.f1605h != null && this.f1605h.f1578l > 0) {
            this.f1604g.a(this.f1607j, this.f1605h.f1578l);
        }
    }

    /* renamed from: f */
    private void m2675f() {
        this.f1604g.b(this.f1607j);
    }

    /* renamed from: a */
    public void mo1553a(@Nullable oh ohVar) {
        this.f1605h = ohVar;
        mo1555c();
    }

    /* renamed from: d */
    public void mo1556d() {
        final on onVar = new on();
        onVar.mo1561a(this.f1603f.a());
        onVar.mo1565b(this.f1603f.c());
        onVar.mo1563a(this.f1598a.mo664a());
        this.f1599b.mo2413a((uv) new uv() {
            /* renamed from: a */
            public void a(uu[] uuVarArr) {
                onVar.mo1566b(vq.m4218a(uuVarArr));
            }
        });
        this.f1601d.mo1559a(onVar);
        this.f1600c.mo1613a();
        this.f1602e.mo1538a();
    }

    @VisibleForTesting
    ol(@NonNull cy cyVar, @NonNull vd vdVar, @NonNull ow owVar, @NonNull om omVar, @NonNull wg wgVar, @NonNull od odVar, @NonNull xh xhVar, @Nullable oh ohVar) {
        this.f1607j = new Runnable() {
            public void run() {
                ol.this.mo1556d();
                ol.this.m2674e();
            }
        };
        this.f1598a = cyVar;
        this.f1599b = vdVar;
        this.f1600c = owVar;
        this.f1601d = omVar;
        this.f1603f = wgVar;
        this.f1602e = odVar;
        this.f1604g = xhVar;
        this.f1605h = ohVar;
    }
}
