package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rp.a;
import com.yandex.metrica.impl.ob.rp.a.aa;
import com.yandex.metrica.impl.ob.rp.a.aa.C0857a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: com.yandex.metrica.impl.ob.na */
public class na implements mq<rs.a, rp.a> {

    /* renamed from: a */
    private static final Map<Integer, bt.a> f1498a = Collections.unmodifiableMap(new HashMap<Integer, bt.a>() {
        {
            put(Integer.valueOf(1), bt.a.WIFI);
            put(Integer.valueOf(2), bt.a.CELL);
        }
    });

    /* renamed from: b */
    private static final Map<bt.a, Integer> f1499b = Collections.unmodifiableMap(new HashMap<bt.a, Integer>() {
        {
            put(bt.a.WIFI, Integer.valueOf(1));
            put(bt.a.CELL, Integer.valueOf(2));
        }
    });

    @NonNull
    /* renamed from: a */
    public a b(@NonNull rs.a aVar) {
        a aVar2 = new a();
        Set a = aVar.mo1905a();
        aVar2.c = (String[]) a.toArray(new String[a.size()]);
        aVar2.b = m2528b(aVar);
        return aVar2;
    }

    @NonNull
    /* renamed from: a */
    public rs.a a(@NonNull a aVar) {
        return new rs.a(m2526b(aVar), Arrays.asList(aVar.c));
    }

    @NonNull
    /* renamed from: B */
    private List<rs.a.aa> m2526b(@NonNull a aVar) {
        aa[] aVarArr;
        ArrayList arrayList = new ArrayList();
        for (aa aVar2 : aVar.b) {
            arrayList.add(new rs.a.aa(aVar2.b, aVar2.c, aVar2.d, m2523a(aVar2.e), aVar2.f, a(aVar2.g)));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    private wp<String, String> m2523a(@NonNull C0857a[] aVarArr) {
        wp<String, String> wpVar = new wp<>();
        for (C0857a aVar : aVarArr) {
            wpVar.mo2572a(aVar.b, aVar.c);
        }
        return wpVar;
    }

    /* renamed from: B */
    private aa[] m2528b(@NonNull rs.a aVar) {
        List b = aVar.b();
        aa[] aVarArr = new aa[b.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= b.size()) {
                return aVarArr;
            }
            aVarArr[i2] = m2522a((rs.a.aa) b.get(i2));
            i = i2 + 1;
        }
    }

    @NonNull
    /* renamed from: a */
    private aa m2522a(@NonNull rs.a.aa aVar) {
        aa aVar2 = new aa();
        aVar2.b = aVar.a;
        aVar2.c = aVar.b;
        aVar2.e = b_(aVar);
        aVar2.d = aVar.c;
        aVar2.f = aVar.e;
        aVar2.g = a(aVar.f);
        return aVar2;
    }

    @NonNull
    private aa a(@NonNull rs.a.aa var1) {
        aa var2 = new aa();// a.a();
        var2.b = var1.a;
        var2.c = var1.b;
        var2.e = this.b_(var1);
        var2.d = var1.c;
        var2.f = var1.e;
        var2.g = this.a(var1.f);
        return var2;
    }

    @NonNull
    /* renamed from: B */
    private C0857a[] b_(@NonNull rs.a.aa var1) {

        com.yandex.metrica.impl.ob.rp.a.aa.C0857a[] var2 = new com.yandex.metrica.impl.ob.rp.a.aa.C0857a[var1.d.a()];
        int var3 = 0;
        Iterator var4 = var1.d.b().iterator();

        while(var4.hasNext()) {
            Map.Entry var5 = (Map.Entry)var4.next();

            for(Iterator var6 = ((Collection)var5.getValue()).iterator(); var6.hasNext(); ++var3) {
                String var7 = (String)var6.next();
                com.yandex.metrica.impl.ob.rp.a.aa.C0857a var8 = new com.yandex.metrica.impl.ob.rp.a.aa.C0857a();
                var8.b = (String)var5.getKey();
                var8.c = var7;
                var2[var3] = var8;
            }
        }

        return var2;

//        List var2 = var1.b();// f;// .b(); // todo sure about???
//        C0857a[] var3 = new C0857a[var2.size()];
//
//        for (int var4 = 0; var4 < var2.size(); ++var4) {
//            var3[var4] = (C0857a) var2.get(var4);
//        }
//
//        return var3;
    }

    @NonNull
    /* renamed from: a */
    private List<bt.a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(f1498a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    private int[] a(@NonNull List<bt.a> list) {
        int[] iArr = new int[list.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return iArr;
            }
            iArr[i2] = ((Integer) f1499b.get(list.get(i2))).intValue();
            i = i2 + 1;
        }
    }
}


