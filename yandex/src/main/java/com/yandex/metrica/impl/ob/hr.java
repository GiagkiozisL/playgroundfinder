package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.yandex.metrica.i;
import com.yandex.metrica.impl.ob.wj.C1163a;

/* renamed from: com.yandex.metrica.impl.ob.hr */
public class hr extends hd {
    public hr(en enVar) {
        super(enVar);
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        mo976b(wVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public void mo976b(w wVar) {
        String l = wVar.mo2538l();
        i a = wj.m4363a(l);
        String h = mo973a().mo821h();
        i a2 = wj.m4363a(h);
        if (!a.equals(a2)) {
            if (m1763a(a, a2)) {
                wVar.mo1763a(h);
                m1762a(wVar, C1163a.LOGOUT);
            } else if (m1765c(a, a2)) {
                m1762a(wVar, C1163a.LOGIN);
            } else if (m1764b(a, a2)) {
                m1762a(wVar, C1163a.SWITCH);
            } else {
                m1762a(wVar, C1163a.UPDATE);
            }
            mo973a().mo814a(l);
        }
    }

    /* renamed from: a */
    private void m1762a(w wVar, C1163a aVar) {
        wVar.mo1767c(wj.m4364a(aVar));
        mo973a().mo818e().mo889e(wVar);
    }

    /* renamed from: a */
    private boolean m1763a(i iVar, i iVar2) {
        return TextUtils.isEmpty(iVar.mo179a()) && !TextUtils.isEmpty(iVar2.mo179a());
    }

    /* renamed from: B */
    private boolean m1764b(i iVar, i iVar2) {
        return !TextUtils.isEmpty(iVar.mo179a()) && !iVar.mo179a().equals(iVar2.mo179a());
    }

    /* renamed from: a */
    private boolean m1765c(i iVar, i iVar2) {
        return !TextUtils.isEmpty(iVar.mo179a()) && TextUtils.isEmpty(iVar2.mo179a());
    }
}
