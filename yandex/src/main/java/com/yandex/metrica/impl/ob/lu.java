package com.yandex.metrica.impl.ob;

/* renamed from: com.yandex.metrica.impl.ob.lu */
public class lu extends lx {

    /* renamed from: a */
    private final qk f1362a = new qk("init_event_pref_key");

    /* renamed from: B */
    private final qk f1363b = new qk("first_event_pref_key");

    /* renamed from: a */
    private final qk f1364c = new qk("first_event_description_key");

    /* renamed from: d */
    private final qk f1365d = new qk("preload_info_auto_tracking_enabled");

    public lu(lf lfVar) {
        super(lfVar);
    }

    /* renamed from: a */
    public void mo1276a() {
        mo1357b(this.f1362a.mo1744b(), "DONE").mo1364q();
    }

    /* renamed from: B */
    public void mo1279b() {
        mo1357b(this.f1363b.mo1744b(), "DONE").mo1364q();
    }

    /* renamed from: a */
    public String mo1275a(String str) {
        return mo1361c(this.f1362a.mo1744b(), str);
    }

    /* renamed from: B */
    public String mo1278b(String str) {
        return mo1361c(this.f1363b.mo1744b(), str);
    }

    /* renamed from: a */
    public boolean mo1282c() {
        return mo1275a((String) null) != null;
    }

    /* renamed from: d */
    public boolean mo1284d() {
        return mo1278b((String) null) != null;
    }

    /* renamed from: a */
    public void mo1281c(String str) {
        mo1357b(this.f1364c.mo1744b(), str).mo1364q();
    }

    /* renamed from: d */
    public String mo1283d(String str) {
        return mo1361c(this.f1364c.mo1744b(), str);
    }

    /* renamed from: e */
    public void mo1285e() {
        m2245a(this.f1364c);
    }

    /* renamed from: a */
    public void mo1277a(boolean z) {
        mo1353a(this.f1365d.mo1744b(), z).mo1364q();
    }

    /* renamed from: B */
    public boolean mo1280b(boolean z) {
        return mo1359b(this.f1365d.mo1744b(), z);
    }

    /* renamed from: a */
    private void m2245a(qk qkVar) {
        mo1365r(qkVar.mo1744b()).mo1364q();
    }
}
