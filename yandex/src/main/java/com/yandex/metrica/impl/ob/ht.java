package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.ht */
public class ht extends hd {
    public ht(en enVar) {
        super(enVar);
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        String B = mo973a().mo811B();
        String p = wVar.mo2542p();
        mo973a().mo816b(p);
        if (!TextUtils.equals(B, p)) {
            mo973a().a(r.m3007c());
        }
        return false;
    }
}
