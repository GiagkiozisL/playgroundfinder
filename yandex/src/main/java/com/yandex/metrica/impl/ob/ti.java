package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.vq.a;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.ti */
public class ti {
    @NonNull
    /* renamed from: a */
    public List<th> mo2239a(@NonNull String str) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray optJSONArray = new a(str).optJSONArray("sdk_list");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject jSONObject = optJSONArray.getJSONObject(i);
                    String optString = jSONObject.optString("name");
                    if (!TextUtils.isEmpty(optString)) {
                        JSONArray optJSONArray2 = jSONObject.optJSONArray("classes");
                        ArrayList arrayList2 = new ArrayList();
                        if (optJSONArray2 != null) {
                            for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                                try {
                                    String optString2 = optJSONArray2.getJSONObject(i2).optString("name");
                                    if (!TextUtils.isEmpty(optString2)) {
                                        arrayList2.add(optString2);
                                    }
                                } catch (Throwable th) {
                                }
                            }
                        }
                        if (!cx.a((Collection) arrayList2)) {
                            arrayList.add(new th(optString, arrayList2));
                        }
                    }
                }
            }
        } catch (Throwable th2) {
        }
        return arrayList;
    }
}
