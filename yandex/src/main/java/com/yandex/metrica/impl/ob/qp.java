package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rl.a.Aa;

/* renamed from: com.yandex.metrica.impl.ob.qp */
public class qp extends qr<Boolean> {
    public qp(@NonNull String str, boolean z, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(3, str, Boolean.valueOf(z), ykVar, qoVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo1750a(@NonNull Aa aVar) {
        aVar.f2049e.f2055e = ((Boolean) mo1752b()).booleanValue();
    }
}
