package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import androidx.annotation.Nullable;

import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.impl.ob.l.A_enum;

/* renamed from: com.yandex.metrica.impl.ob.j */
public class j {
    @Nullable
    /* renamed from: a */
    public A_enum mo1065a(int i) {
        if (cx.a(28)) {
            return m1890b(i);
        }
        return null;
    }

    @Nullable
    @TargetApi(28)
    /* renamed from: B */
    private A_enum m1890b(int i) {
        switch (i) {
            case YandexMetricaDefaultValues.DEFAULT_SESSION_TIMEOUT_SECONDS /*10*/:
                return A_enum.ACTIVE;
            case 20:
                return A_enum.WORKING_SET;
            case 30:
                return A_enum.FREQUENT;
            case 40:
                return A_enum.RARE;
            default:
                return null;
        }
    }

    @Nullable
    /* renamed from: a */
    public String mo1066a(@Nullable A_enum aVar) {
        if (aVar == null) {
            return null;
        }
        switch (aVar) {
            case ACTIVE:
                return "ACTIVE";
            case WORKING_SET:
                return "WORKING_SET";
            case FREQUENT:
                return "FREQUENT";
            case RARE:
                return "RARE";
            default:
                return null;
        }
    }
}
