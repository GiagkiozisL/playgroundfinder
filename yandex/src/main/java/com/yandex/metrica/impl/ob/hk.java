package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.np.C0678a;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.hk */
public class hk extends hd {

    /* renamed from: a */
    private final pv f1013a;
    @NonNull

    /* renamed from: B */
    private final mf<pn> f1014b;
    @NonNull

    /* renamed from: a */
    private final m f1015c;
    @NonNull

    /* renamed from: d */
    private final j f1016d;
    @NonNull

    /* renamed from: e */
    private final k f1017e;

    public hk(en enVar, pv pvVar) {
        this(enVar, pvVar, C0678a.m2599a(pn.class).a(enVar.mo824k()), new m(enVar.mo824k()), new j(), new k(enVar.mo824k()));
    }

    @VisibleForTesting
    hk(en enVar, pv pvVar, @NonNull mf<pn> mfVar, @NonNull m mVar, @NonNull j jVar, @NonNull k kVar) {
        super(enVar);
        this.f1013a = pvVar;
        this.f1014b = mfVar;
        this.f1015c = mVar;
        this.f1016d = jVar;
        this.f1017e = kVar;
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        en a = mo973a();
        a.b().toString();
        if (a.mo834u().mo1284d() && a.mo831r()) {
            pn pnVar = (pn) this.f1014b.a();
            pn a2 = m1753a(pnVar);
            if (a2 != null) {
                m1754a(a2, wVar, a.mo818e());
                this.f1014b.a(a2);
            } else if (a.mo832s()) {
                m1754a(pnVar, wVar, a.mo818e());
            }
        }
        return false;
    }

    @Nullable
    /* renamed from: a */
    private pn m1753a(@NonNull pn pnVar) {
        List<pu> list = pnVar.f1734a;
        l lVar = pnVar.f1735b;
        l a = this.f1015c.mo1412a();
        List<String> list2 = pnVar.f1736c;
        List a2 = this.f1017e.mo1139a();
        List<pu> a3 = this.f1013a.mo1665a(mo973a().mo824k(), list);
        if (a3 == null && cx.a((Object) lVar, (Object) a) && vj.a(list2, a2)) {
            return null;
        }
        if (a3 != null) {
            list = a3;
        }
        return new pn(list, a, a2);
    }

    /* renamed from: a */
    private void m1754a(@NonNull pn pnVar, @NonNull w wVar, @NonNull ff ffVar) {
        ffVar.mo885c(w.m4278a(wVar, pnVar.f1734a, pnVar.f1735b, this.f1016d, pnVar.f1736c));
    }
}
