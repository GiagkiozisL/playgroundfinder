package com.yandex.metrica.impl.ob;

import androidx.annotation.Nullable;

import com.yandex.metrica.Revenue;

import java.util.Arrays;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.yo */
public class yo implements yk<Revenue> {

    /* renamed from: a */
    private final yk<List<yi>> f3012a = new yj();

    /* renamed from: a */
    public yi a(@Nullable Revenue revenue) {
        return this.f3012a.a(Arrays.asList(new yi[]{new yn().a(revenue.quantity)}));
    }
}
