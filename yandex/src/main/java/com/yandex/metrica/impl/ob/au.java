package com.yandex.metrica.impl.ob;

import android.os.Handler;

import java.lang.ref.WeakReference;

/* renamed from: com.yandex.metrica.impl.ob.au */
class au implements Runnable {

    /* renamed from: a */
    private final WeakReference<Handler> f258a;

    /* renamed from: B */
    private final WeakReference<n> f259b;

    au(Handler handler, n nVar) {
        this.f258a = new WeakReference<>(handler);
        this.f259b = new WeakReference<>(nVar);
    }

    public void run() {
        Handler handler = (Handler) this.f258a.get();
        n nVar = (n) this.f259b.get();
        if (handler != null && nVar != null && nVar.mo1463c()) {
            at.m394a(handler, nVar, this);
        }
    }
}
