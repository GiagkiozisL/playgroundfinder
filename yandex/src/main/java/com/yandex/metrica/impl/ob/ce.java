package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ob.af.C0058a;
import com.yandex.metrica.impl.ob.r.C0802a;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.ce */
public class ce implements com.yandex.metrica.impl.ob.bb.a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final ao f518a;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public final bb f519b;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Object f520c;

    /* renamed from: d */
    private final xh d;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: e */
    public final ah f522e;

    @VisibleForTesting
    /* renamed from: com.yandex.metrica.impl.ob.ce$a */
    class a extends b {

        /* renamed from: d */
        private final kc f524d;

        /* renamed from: e */
        private final vu f525e;

        a(@NonNull ce.d dVar) {
            this(dVar, new kc(), new vu());
        }

        @VisibleForTesting
        a(ce.d dVar, @NonNull kc kcVar, @NonNull vu vuVar) {
            super(dVar);
            this.f524d = kcVar;
            this.f525e = vuVar;
        }

        /* renamed from: a */
        public Void call() {
            Log.d(Constant.RUS_TAG, "a$call");
            if (this.f525e.mo2504a("Metrica")) {
                mo592b(this.f526b);
                return null;
            }
            ce.this.f519b.mo380c();
            return super.call();
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public boolean mo593b() {
            mo591a(this.f526b);
            return false;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: a */
        public void mo591a(@NonNull ce.d dVar) {
            if (dVar.mo601d().mo2541o() == 0) {
                Context b = ce.this.f518a.b();
                Intent b2 = cn.m985b(b);
                dVar.mo601d().mo2520a(C0058a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.mo259a());
                b2.putExtras(dVar.mo601d().mo2519a(dVar.mo595a().mo547b()));
                try {
                    Log.d(Constant.RUS_TAG, "ce$mo591a start service ..");
                    b.startService(b2);
                } catch (Throwable th) {
                    mo592b(dVar);
                }
            } else {
                mo592b(dVar);
            }
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public void mo592b(@NonNull ce.d var1) {
            File var2 = ce.this.f522e.mo266b(ce.this.f519b.mo376a());
            if (this.f524d.mo1141a(var2)) {
                ee var3 = var1.mo595a().mo743g();
                Integer var4 = var3.mo756e();
                String var5 = var3.mo757f();
                File var6 = ce.this.f522e.mo264a(var2, var4 + "-" + var5);
                PrintWriter var7 = null;

                try {
                    var7 = new PrintWriter(new BufferedOutputStream(new FileOutputStream(var6)));
                    var7.write((new kv(var1.f528a, var1.mo595a(), var1.f532e)).mo1176j());
                } catch (IOException var13) {
                } catch (Throwable var14) {
                } finally {
                    cx.a(var7);
                }
            }
//            PrintWriter printWriter;
//            Throwable th;
//            PrintWriter printWriter2;
//            File b = ce.this.f522e.mo266b(ce.this.f519b.mo376a());
//            if (this.f524d.mo1141a(b)) {
//                ee g = dVar.mo595a().mo743g();
//                Integer e = g.mo756e();
//                try {
//                    printWriter2 = new PrintWriter(new BufferedOutputStream(new FileOutputStream(ce.this.f522e.mo264a(b, e + "-" + g.mo757f()))));
//                    try {
//                        printWriter2.write(new kv(dVar.f528a, dVar.mo595a(), dVar.f532e).mo1176j());
//                        cx.aaa((Closeable) printWriter2);
//                    } catch (IOException e2) {
//                        cx.aaa((Closeable) printWriter2);
//                    } catch (Throwable th2) {
//                        th = th2;
//                        printWriter = printWriter2;
//                        cx.aaa((Closeable) printWriter);
//                        throw th;
//                    }
//                } catch (IOException e3) {
//                    printWriter2 = null;
//                    cx.aaa((Closeable) printWriter2);
//                } catch (Throwable th3) {
//                    th = th3;
//                    printWriter = null;
//                    cx.aaa((Closeable) printWriter);
//                    throw th;
//                }
//            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ce$B */
    private class b implements Callable<Void> {

        /* renamed from: B */
        final ce.d f526b;

        private b(ce.d dVar) {
            this.f526b = dVar;
        }

        /* renamed from: a */
        public Void call() {
            Log.d(Constant.RUS_TAG, "b$call");
            int i = 0;
            while (true) {
                try {
                    IMetricaService f = ce.this.f519b.mo383f();
                    if (f != null && m923a(f, this.f526b)) {
                        dr.a().mo726a((Object) this);
                        break;
                    }
                    i++;
                    if (mo593b() && !bh.f355a.get()) {
                        if (i >= 20) {
                            break;
                        }
                    }
                } catch (Throwable th) {
                    if (this.f526b == null) {
                    }
                } finally {
                    dr.a().mo726a((Object) this);
                }
            }
            return null;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public boolean mo593b() {
            ce.this.f519b.mo379b();
            m924c();
            return true;
        }

        /* renamed from: a */
        private boolean m923a(IMetricaService iMetricaService, ce.d dVar) {
            Log.d(Constant.RUS_TAG, "ce$m923a");
            try {
                ce.this.f518a.a(iMetricaService, dVar.mo599b(), dVar.f529b);
                return true;
            } catch (RemoteException e) {
                return false; // false
            }
        }

        /* renamed from: a */
        private void m924c() {
            synchronized (ce.this.f520c) {
                if (!ce.this.f519b.mo382e()) {
                    try {
                        ce.this.f520c.wait(500, 0);
                    } catch (InterruptedException e) {
                        ce.this.f520c.notifyAll();
                    }
                }
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ce$a */
    public interface C0192c {
        /* renamed from: a */
        w mo588a(w wVar);
    }

    /* renamed from: com.yandex.metrica.impl.ob.ce$d */
    public static class d {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public w f528a;
        /* access modifiers changed from: private */

        /* renamed from: B */
        public bz f529b;

        /* renamed from: a */
        private boolean f530c = false;

        /* renamed from: d */
        private C0192c f531d;
        /* access modifiers changed from: private */
        @Nullable

        /* renamed from: e */
        public HashMap<C0802a, Integer> f532e;

        d(w wVar, bz bzVar) {
            this.f528a = wVar;
            this.f529b = new bz(new ee(bzVar.mo743g()), new CounterConfiguration(bzVar.mo744h()));
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public ce.d mo596a(C0192c cVar) {
            this.f531d = cVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public ce.d mo597a(@NonNull HashMap<C0802a, Integer> hashMap) {
            this.f532e = hashMap;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public ce.d mo598a(boolean z) {
            this.f530c = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public bz mo595a() {
            return this.f529b;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public w mo599b() {
            return this.f531d != null ? this.f531d.mo588a(this.f528a) : this.f528a;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean c() {
            return this.f530c;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: d */
        public w mo601d() {
            return this.f528a;
        }

        public String toString() {
            return "ReportToSend{mReport=" + this.f528a + ", mEnvironment=" + this.f529b + ", mCrash=" + this.f530c + ", mAction=" + this.f531d + ", mTrimmedFields=" + this.f532e + '}';
        }
    }

    public ce(ao aoVar) {
        this(aoVar, db.k().mo2601c(), new ah());
    }

    public ce(@NonNull ao aoVar, @NonNull xh xhVar, @NonNull ah ahVar) {
        this.f520c = new Object();
        this.f518a = aoVar;
        this.d = xhVar;
        this.f522e = ahVar;
        this.f519b = aoVar.a();
        this.f519b.mo377a(this);
    }

    /* renamed from: a */
    public Future<Void> mo589a(ce.d var1) {
        Log.d(Constant.RUS_TAG, "ce$mo589a");
//        return this.d.a(dVar.a() ? new a<>(this, dVar) : new b<>(dVar));
        return this.d.a((Callable)(var1.c() ? new ce.a(var1) : new ce.b(var1)));
    }

    /* renamed from: a */
    public void a() {
        synchronized (this.f520c) {
            this.f520c.notifyAll();
        }
    }

    /* renamed from: B */
    public void b() {
    }
}
