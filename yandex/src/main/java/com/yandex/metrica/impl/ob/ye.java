package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

/* renamed from: com.yandex.metrica.impl.ob.ye */
public class ye implements yk<String> {

    /* renamed from: a */
    private final String f3005a;

    public ye(@NonNull String str) {
        this.f3005a = str;
    }

    /* renamed from: a */
    public yi a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return yi.a(this, this.f3005a + " is empty.");
        }
        return yi.a(this);
    }
}
