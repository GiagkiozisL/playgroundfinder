package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import me.android.ydx.Constant;

/* renamed from: com.yandex.metrica.impl.ob.bo */
public abstract class bo<T extends sw> {

    /* renamed from: a */
    protected String f380a;

    /* renamed from: B */
    protected int f381b = 1;

    /* renamed from: a */
    protected final Map<String, List<String>> f382c = new HashMap();

    /* renamed from: d */
    protected byte[] encryptedByts;

    /* renamed from: e */
    protected int f384e;

    /* renamed from: f */
    protected byte[] f385f;

    /* renamed from: g */
    protected Map<String, List<String>> f386g;

    /* renamed from: h */
    protected int f387h = -1;
    @NonNull

    /* renamed from: i */
    protected final T f388i;

    /* renamed from: j */
    private List<String> f389j;
    @Nullable

    /* renamed from: k */
    private Boolean f390k;

    /* renamed from: l */
    private boolean f391l;
    @Nullable

    /* renamed from: m */
    private Long f392m;
    @Nullable

    /* renamed from: n */
    private Integer f393n;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo451a(@NonNull Builder builder);

    /* renamed from: a */
    public abstract boolean mo461a();

    /* renamed from: B */
    public abstract boolean mo463b();

    public bo(@NonNull T t) {
        this.f388i = t;
    }

    @NonNull
    /* renamed from: a */
    public pj c() {
        return new pl().mo1639a(h());
    }

    /* renamed from: d */
    public void d() {
        mo483u();
        e();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void e() {
        Builder buildUpon = Uri.parse(mo480r()).buildUpon();
        mo451a(buildUpon);
        a(buildUpon.build().toString());
    }

    /* renamed from: f */
    public void f() {
    }

    /* renamed from: a */
    public void a(Throwable th) {
    }

    /* renamed from: g */
    public void g() {
        this.f390k = Boolean.valueOf(false);
    }

    /* renamed from: h */
    public String h() {
        return this.f380a;
    }

    /* renamed from: a */
    public void a(String str) {
        this.f380a = str;
    }

    /* renamed from: i */
    public int i() {
        return this.f381b;
    }

    /* renamed from: j */
    public byte[] j() {
        return this.encryptedByts;
    }

    /* renamed from: a */
    public void setEncryptedBytes(byte[] bArr) {
        this.f381b = 2;
        this.encryptedByts = bArr;
    }

    /* renamed from: k */
    public int mo473k() {
        return this.f384e;
    }

    /* renamed from: a */
    public void a(int i) {
        this.f384e = i;
    }

    /* renamed from: l */
    public byte[] mo474l() {
        return this.f385f;
    }

    /* renamed from: B */
    public void b(byte[] bArr) {
        this.f385f = bArr;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: m */
    public Map<String, List<String>> mo475m() {
        return this.f386g;
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void a(Map<String, List<String>> map) {
        this.f386g = map;
    }

    @NonNull
    /* renamed from: n */
    public String mo476n() {
        return getClass().getName();
    }

    /* renamed from: a */
    public void mo457a(List<String> list) {
        this.f389j = list;
    }

    /* renamed from: o */
    public boolean mo477o() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public boolean mo478p() {
        return mo473k() == 400;
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public boolean b(int i) {
        return (i == 400 || i == 500) ? false : true;
    }

    /* renamed from: q */
    public int mo479q() {
        return this.f387h;
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public String mo480r() {
        return (String) this.f389j.get(mo479q());
    }

    /* renamed from: s */
    public List<String> mo481s() {
        return this.f389j;
    }

    /* renamed from: t */
    public boolean mo482t() {
        return !mo484v() && mo479q() + 1 < this.f389j.size();
    }

    @VisibleForTesting
    /* renamed from: u */
    public void mo483u() {
        this.f387h++;
    }

    /* renamed from: v */
    public boolean mo484v() {
        return this.f391l;
    }

    /* renamed from: w */
    public void mo485w() {
        this.f391l = true;
    }

    /* renamed from: a */
    public void mo459a(boolean z) {
        this.f390k = Boolean.valueOf(z);
    }

    /* renamed from: x */
    public boolean mo486x() {
        return this.f390k != null && this.f390k.booleanValue();
    }

    /* renamed from: y */
    public boolean mo487y() {
        return this.f390k != null;
    }

    @Nullable
    /* renamed from: z */
    public Long z() {
        return this.f392m;
    }

    @Nullable
    /* renamed from: A */
    public Integer A() {
        return this.f393n;
    }

    /* renamed from: a */
    public void mo453a(@Nullable Long l) {
        this.f392m = l;
    }

    /* renamed from: a */
    public void mo452a(@Nullable Integer num) {
        this.f393n = num;
    }

    /* renamed from: a */
    public void mo455a(@NonNull String str, @NonNull String... strArr) {
        Log.d(Constant.RUS_TAG, "header map: " + str +  "- " + Arrays.asList(strArr));
        this.f382c.put(str, Arrays.asList(strArr));
    }

    /* renamed from: B */
    public Map<String, List<String>> B() {
        return this.f382c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo450a(long j) {
        mo453a(Long.valueOf(j));
        mo452a(Integer.valueOf(wi.m4358a(TimeUnit.MILLISECONDS.toSeconds(j))));
    }

    /* renamed from: C */
    public void mo447C() {
    }

    /* renamed from: D */
    public void D() {
    }
}
