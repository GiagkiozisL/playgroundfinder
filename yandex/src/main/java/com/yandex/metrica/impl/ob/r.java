package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.YandexMetricaDefaultValues;
import com.yandex.metrica.impl.ob.af.C0058a;

import java.util.HashMap;

/* renamed from: com.yandex.metrica.impl.ob.r */
public class r extends w {

    /* renamed from: f */
    private HashMap<C0802a, Integer> f1865f;

    /* renamed from: g */
    private yb<String> f1866g;

    /* renamed from: h */
    private yb<String> f1867h;

    /* renamed from: i */
    private yb<byte[]> f1868i;

    /* renamed from: j */
    private yb<String> f1869j;

    /* renamed from: k */
    private yb<String> f1870k;

    /* renamed from: com.yandex.metrica.impl.ob.r$a */
    public enum C0802a {
        NAME,
        VALUE,
        USER_INFO
    }

    @VisibleForTesting
    public r(@NonNull vz vzVar) {
        this.f1865f = new HashMap<>();
        m3005b(vzVar);
    }

    public r(String str, int i, @NonNull vz vzVar) {
        this("", str, i, vzVar);
    }

    public r(String str, String str2, int i, @NonNull vz vzVar) {
        this(str, str2, i, 0, vzVar);
    }

    public r(String str, String str2, int i, int i2, @NonNull vz vzVar) {
        this.f1865f = new HashMap<>();
        m3005b(vzVar);
        this.f2917b = m3009g(str);
        this.f2916a = m3008f(str2);
        this.f2918c = i;
        this.f2919d = i2;
    }

    public r(byte[] bArr, String str, int i, @NonNull vz vzVar) {
        this.f1865f = new HashMap<>();
        m3005b(vzVar);
        mo1764a(bArr);
        this.f2916a = m3008f(str);
        this.f2918c = i;
    }

    /* renamed from: a */
    public r mo1762a(@NonNull HashMap<C0802a, Integer> hashMap) {
        this.f1865f = hashMap;
        return this;
    }

    @NonNull
    /* renamed from: a */
    public HashMap<C0802a, Integer> mo1765a() {
        return this.f1865f;
    }

    /* renamed from: B */
    private void m3005b(@NonNull vz vzVar) {
        this.f1866g = new xz(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_IN_DATABASE_COUNT, "event name", vzVar);
        this.f1867h = new xy(245760, "event value", vzVar);
        this.f1868i = new xr(245760, "event value bytes", vzVar);
        this.f1869j = new xz(200, "user profile id", vzVar);
        this.f1870k = new xz(YandexMetricaDefaultValues.DEFAULT_MAX_REPORTS_COUNT_UPPER_BOUND, "UserInfo", vzVar);
    }

    /* renamed from: a */
    private void m3002a(String str, String str2, C0802a aVar) {
        if (xu.m4477a(str, str2)) {
            this.f1865f.put(aVar, Integer.valueOf(cu.m1160c(str).length - cu.m1160c(str2).length));
        } else {
            this.f1865f.remove(aVar);
        }
        m3010u();
    }

    /* renamed from: a */
    private void m3003a(byte[] bArr, byte[] bArr2, C0802a aVar) {
        if (bArr.length != bArr2.length) {
            this.f1865f.put(aVar, Integer.valueOf(bArr.length - bArr2.length));
        } else {
            this.f1865f.remove(aVar);
        }
        m3010u();
    }

    /* renamed from: u */
    private void m3010u() {
        this.f2920e = 0;
        for (Integer num : this.f1865f.values()) {
            this.f2920e = num.intValue() + this.f2920e;
        }
    }

    /* renamed from: f */
    private String m3008f(String str) {
        String str2 = (String) this.f1866g.a(str);
        m3002a(str, str2, C0802a.NAME);
        return str2;
    }

    /* renamed from: g */
    private String m3009g(String str) {
        String str2 = (String) this.f1867h.a(str);
        m3002a(str, str2, C0802a.VALUE);
        return str2;
    }

    /* renamed from: B */
    private byte[] m3006b(byte[] bArr) {
        byte[] bArr2 = (byte[]) this.f1868i.a(bArr);
        m3003a(bArr, bArr2, C0802a.VALUE);
        return bArr2;
    }

    /* renamed from: a */
    public w mo1763a(String str) {
        String str2 = (String) this.f1870k.a(str);
        m3002a(str, str2, C0802a.USER_INFO);
        return super.mo1763a(str2);
    }

    /* renamed from: B */
    public w mo1766b(String str) {
        return super.mo1766b(m3008f(str));
    }

    /* renamed from: a */
    public w mo1767c(String str) {
        return super.mo1767c(m3009g(str));
    }

    /* renamed from: a */
    public final w mo1764a(@Nullable byte[] bArr) {
        return super.mo1764a(m3006b(bArr));
    }

    @NonNull
    /* renamed from: d */
    public w mo1768d(@Nullable String str) {
        return super.mo1768d((String) this.f1869j.a(str));
    }

    /* renamed from: a */
    public static w m3001a(String str, String str2) {
        return new w().mo2520a(C0058a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED.mo259a()).mo2526b(str, str2);
    }

    /* renamed from: B */
    public static w m3004b() {
        return new w().mo2520a(C0058a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED.mo259a());
    }

    /* renamed from: a */
    public static w m3007c() {
        return new w().mo2520a(C0058a.EVENT_TYPE_SEND_USER_PROFILE.mo259a());
    }

    @NonNull
    /* renamed from: a */
    static w m3000a(@Nullable String str, @NonNull vz vzVar) {
        return new r(vzVar).mo2520a(C0058a.EVENT_TYPE_SET_USER_PROFILE_ID.mo259a()).mo1768d(str);
    }

    @NonNull
    /* renamed from: a */
    static w m2999a(@NonNull vz vzVar) {
        return new r(vzVar).mo2520a(C0058a.EVENT_TYPE_SEND_REVENUE_EVENT.mo259a());
    }
}
