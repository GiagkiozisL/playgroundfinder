package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.i.a;

import java.util.LinkedList;
import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.em */
public class em {

    /* renamed from: a */
    private final en f804a;

    /* renamed from: B */
    private final qb f805b;

    /* renamed from: a */
    private List<C0321g> f806c;

    /* renamed from: com.yandex.metrica.impl.ob.em$a */
    public static class C0315a {
        @NonNull

        /* renamed from: a */
        private final en f807a;

        public C0315a(@NonNull en enVar) {
            this.f807a = enVar;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public em mo800a(@NonNull qb qbVar) {
            return new em(this.f807a, qbVar);
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$B */
    static class C0316b extends C0321g {

        /* renamed from: a */
        private final qf f808a;

        /* renamed from: B */
        private final lw f809b;

        /* renamed from: a */
        private final ly f810c;

        C0316b(en enVar) {
            super(enVar);
            this.f808a = new qf(enVar.mo824k(), enVar.b().toString());
            this.f809b = enVar.mo838y();
            this.f810c = enVar.f826a;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean mo801a() {
            return this.f808a.mo1721e();
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public void mo802b() {
            mo804d();
            mo803c();
            m1457g();
            this.f808a.mo1724g();
        }

        /* renamed from: g */
        private void m1457g() {
            a a = this.f808a.mo1684a();
            if (a != null) {
                this.f809b.mo1320a(a);
            }
            String a2 = this.f808a.mo1712a((String) null);
            if (!TextUtils.isEmpty(a2) && TextUtils.isEmpty(this.f809b.mo1340f())) {
                this.f809b.mo1321a(a2);
            }
            long c = this.f808a.mo1716c(Long.MIN_VALUE);
            if (c != Long.MIN_VALUE && this.f809b.mo1318a(Long.MIN_VALUE) == Long.MIN_VALUE) {
                this.f809b.mo1326b(c);
            }
            this.f809b.mo1364q();
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: a */
        public void mo803c() {
            jg jgVar = new jg(this.f809b, "foreground");
            if (!jgVar.mo1101i()) {
                long d = this.f808a.mo1718d(-1);
                if (-1 != d) {
                    jgVar.mo1094d(d);
                }
                boolean booleanValue = this.f808a.mo1711a(true).booleanValue();
                if (booleanValue) {
                    jgVar.a(booleanValue);
                }
                long a = this.f808a.mo1709a(Long.MIN_VALUE);
                if (a != Long.MIN_VALUE) {
                    jgVar.mo1096e(a);
                }
                long f = this.f808a.mo1722f(0);
                if (f != 0) {
                    jgVar.a(f);
                }
                long h = this.f808a.mo1725h(0);
                if (h != 0) {
                    jgVar.b(h);
                }
                jgVar.mo1100h();
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: d */
        public void mo804d() {
            jg jgVar = new jg(this.f809b, "background");
            if (!jgVar.mo1101i()) {
                long e = this.f808a.mo1720e(-1);
                if (e != -1) {
                    jgVar.mo1094d(e);
                }
                long b = this.f808a.mo1713b(Long.MIN_VALUE);
                if (b != Long.MIN_VALUE) {
                    jgVar.mo1096e(b);
                }
                long g = this.f808a.mo1723g(0);
                if (g != 0) {
                    jgVar.a(g);
                }
                long i = this.f808a.mo1726i(0);
                if (i != 0) {
                    jgVar.b(i);
                }
                jgVar.mo1100h();
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$a */
    static class C0317c extends C0322h {
        C0317c(en enVar, qb qbVar) {
            super(enVar, qbVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean mo801a() {
            return mo807e() instanceof ez;
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public void mo802b() {
            mo809c().mo1673a();
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$d */
    static class C0318d extends C0321g {

        /* renamed from: a */
        private final qc f811a;

        /* renamed from: B */
        private final lu f812b;

        C0318d(en enVar, qc qcVar) {
            super(enVar);
            this.f811a = qcVar;
            this.f812b = enVar.mo834u();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean mo801a() {
            return "DONE".equals(this.f811a.mo1687c(null)) || "DONE".equals(this.f811a.mo1685b(null));
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public void mo802b() {
            if ("DONE".equals(this.f811a.mo1687c(null))) {
                this.f812b.mo1279b();
            }
            String e = this.f811a.mo1691e(null);
            if (!TextUtils.isEmpty(e)) {
                this.f812b.mo1281c(e);
            }
            if ("DONE".equals(this.f811a.mo1685b(null))) {
                this.f812b.mo1276a();
            }
            this.f811a.mo1689d();
            this.f811a.mo1692e();
            this.f811a.mo1688c();
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$e */
    static class C0319e extends C0322h {
        C0319e(en enVar, qb qbVar) {
            super(enVar, qbVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean mo801a() {
            return mo807e().mo834u().mo1275a((String) null) == null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public void mo802b() {
            qb c = mo809c();
            if (mo807e() instanceof ez) {
                c.mo1677c();
            } else {
                c.mo1675b();
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$f */
    static class C0320f extends C0321g {
        @Deprecated

        /* renamed from: a */
        static final qk f813a = new qk("SESSION_SLEEP_START");
        @Deprecated

        /* renamed from: B */
        static final qk f814b = new qk("SESSION_ID");
        @Deprecated

        /* renamed from: a */
        static final qk f815c = new qk("SESSION_COUNTER_ID");
        @Deprecated

        /* renamed from: d */
        static final qk f816d = new qk("SESSION_INIT_TIME");
        @Deprecated

        /* renamed from: e */
        static final qk f817e = new qk("SESSION_IS_ALIVE_REPORT_NEEDED");
        @Deprecated

        /* renamed from: f */
        static final qk f818f = new qk("BG_SESSION_ID");
        @Deprecated

        /* renamed from: g */
        static final qk f819g = new qk("BG_SESSION_SLEEP_START");
        @Deprecated

        /* renamed from: h */
        static final qk f820h = new qk("BG_SESSION_COUNTER_ID");
        @Deprecated

        /* renamed from: i */
        static final qk f821i = new qk("BG_SESSION_INIT_TIME");
        @Deprecated

        /* renamed from: j */
        static final qk f822j = new qk("BG_SESSION_IS_ALIVE_REPORT_NEEDED");

        /* renamed from: k */
        private final lw f823k;

        C0320f(en enVar) {
            super(enVar);
            this.f823k = enVar.mo838y();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean mo801a() {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public void mo802b() {
            mo806d();
            mo805c();
            m1468g();
        }

        /* renamed from: g */
        private void m1468g() {
            this.f823k.mo1365r(f813a.mo1744b());
            this.f823k.mo1365r(f814b.mo1744b());
            this.f823k.mo1365r(f815c.mo1744b());
            this.f823k.mo1365r(f816d.mo1744b());
            this.f823k.mo1365r(f817e.mo1744b());
            this.f823k.mo1365r(f818f.mo1744b());
            this.f823k.mo1365r(f819g.mo1744b());
            this.f823k.mo1365r(f820h.mo1744b());
            this.f823k.mo1365r(f821i.mo1744b());
            this.f823k.mo1365r(f822j.mo1744b());
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: a */
        public void mo805c() {
            long b = this.f823k.mo1356b(f813a.mo1744b(), -2147483648L);
            if (b != -2147483648L) {
                jg jgVar = new jg(this.f823k, "foreground");
                if (!jgVar.mo1101i()) {
                    if (b != 0) {
                        jgVar.b(b);
                    }
                    long b2 = this.f823k.mo1356b(f814b.mo1744b(), -1);
                    if (-1 != b2) {
                        jgVar.mo1094d(b2);
                    }
                    boolean b3 = this.f823k.mo1359b(f817e.mo1744b(), true);
                    if (b3) {
                        jgVar.a(b3);
                    }
                    long b4 = this.f823k.mo1356b(f816d.mo1744b(), Long.MIN_VALUE);
                    if (b4 != Long.MIN_VALUE) {
                        jgVar.mo1096e(b4);
                    }
                    long b5 = this.f823k.mo1356b(f815c.mo1744b(), 0);
                    if (b5 != 0) {
                        jgVar.a(b5);
                    }
                    jgVar.mo1100h();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        /* renamed from: d */
        public void mo806d() {
            long b = this.f823k.mo1356b(f819g.mo1744b(), -2147483648L);
            if (b != -2147483648L) {
                jg jgVar = new jg(this.f823k, "background");
                if (!jgVar.mo1101i()) {
                    if (b != 0) {
                        jgVar.b(b);
                    }
                    long b2 = this.f823k.mo1356b(f818f.mo1744b(), -1);
                    if (b2 != -1) {
                        jgVar.mo1094d(b2);
                    }
                    boolean b3 = this.f823k.mo1359b(f822j.mo1744b(), true);
                    if (b3) {
                        jgVar.a(b3);
                    }
                    long b4 = this.f823k.mo1356b(f821i.mo1744b(), Long.MIN_VALUE);
                    if (b4 != Long.MIN_VALUE) {
                        jgVar.mo1096e(b4);
                    }
                    long b5 = this.f823k.mo1356b(f820h.mo1744b(), 0);
                    if (b5 != 0) {
                        jgVar.a(b5);
                    }
                    jgVar.mo1100h();
                }
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$g */
    private static abstract class C0321g {

        /* renamed from: a */
        private final en f824a;

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract boolean mo801a();

        /* access modifiers changed from: protected */
        /* renamed from: B */
        public abstract void mo802b();

        C0321g(en enVar) {
            this.f824a = enVar;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: e */
        public en mo807e() {
            return this.f824a;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: f */
        public void mo808f() {
            if (mo801a()) {
                mo802b();
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.em$h */
    private static abstract class C0322h extends C0321g {

        /* renamed from: a */
        private qb f825a;

        C0322h(en enVar, qb qbVar) {
            super(enVar);
            this.f825a = qbVar;
        }

        /* renamed from: a */
        public qb mo809c() {
            return this.f825a;
        }
    }

    private em(en enVar, qb qbVar) {
        this.f804a = enVar;
        this.f805b = qbVar;
        m1454b();
    }

    /* renamed from: B */
    private void m1454b() {
        this.f806c = new LinkedList();
        this.f806c.add(new C0317c(this.f804a, this.f805b));
        this.f806c.add(new C0319e(this.f804a, this.f805b));
        this.f806c.add(new C0318d(this.f804a, this.f804a.mo835v()));
        this.f806c.add(new C0316b(this.f804a));
        this.f806c.add(new C0320f(this.f804a));
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public void mo799a() {
        if (!m1453a(this.f804a.b().mo790a())) {
            for (C0321g f : this.f806c) {
                f.mo808f();
            }
        }
    }

    /* renamed from: a */
    private boolean m1453a(String str) {
        return qb.a.values().contains(str);
    }
}
