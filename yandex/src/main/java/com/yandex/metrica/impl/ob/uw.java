package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.SparseArray;

@SuppressLint({"NewApi"})
/* renamed from: com.yandex.metrica.impl.ob.uw */
public class uw implements ve<uu> {
    /* access modifiers changed from: private */
    @SuppressLint({"InlineApi"})

    /* renamed from: a */
    public static final SparseArray<String> f2833a = new SparseArray<String>() {
        {
            put(0, null);
            put(7, "1xRTT");
            put(4, "CDMA");
            put(2, "EDGE");
            put(14, "eHRPD");
            put(5, "EVDO rev.0");
            put(6, "EVDO rev.A");
            put(12, "EVDO rev.B");
            put(1, "GPRS");
            put(8, "HSDPA");
            put(10, "HSPA");
            put(15, "HSPA+");
            put(9, "HSUPA");
            put(11, "iDen");
            put(3, "UMTS");
            put(12, "EVDO rev.B");
            if (cx.a(11)) {
                put(14, "eHRPD");
                put(13, "LTE");
                if (cx.a(13)) {
                    put(15, "HSPA+");
                }
            }
        }
    };
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: B */
    public final ux f2834b;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public pr f2835c;

    public uw(@NonNull ux uxVar, @NonNull pr prVar) {
        this.f2834b = uxVar;
        this.f2835c = prVar;
    }

    @Nullable
    /* renamed from: a */
    public uu d() {
        if (this.f2834b.mo2449i()) {
            return new uu(m4065f(), m4066g(), mo2433c(), mo2432b(), m4068i(), m4067h(), null, true, 0, null, null);
        }
        return null;
    }

    @Nullable
    /* renamed from: f */
    private Integer m4065f() {
        return (Integer) cx.a((wo<TelephonyManager, Integer>) new wo<TelephonyManager, Integer>() {
            /* renamed from: a */
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String str;
                String networkOperator = telephonyManager.getNetworkOperator();
                if (!TextUtils.isEmpty(networkOperator)) {
                    str = networkOperator.substring(0, 3);
                } else {
                    str = null;
                }
                if (!TextUtils.isEmpty(str)) {
                    return Integer.valueOf(Integer.parseInt(str));
                }
                return null;
            }
        }, this.f2834b.mo2443c(), "getting phoneMcc", "TelephonyManager");
    }

    @Nullable
    /* renamed from: g */
    private Integer m4066g() {
        return (Integer) cx.a((wo<TelephonyManager, Integer>) new wo<TelephonyManager, Integer>() {
            /* renamed from: a */
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String str;
                String networkOperator = telephonyManager.getNetworkOperator();
                if (!TextUtils.isEmpty(networkOperator)) {
                    str = networkOperator.substring(3);
                } else {
                    str = null;
                }
                if (!TextUtils.isEmpty(str)) {
                    return Integer.valueOf(Integer.parseInt(str));
                }
                return null;
            }
        }, this.f2834b.mo2443c(), "getting phoneMnc", "TelephonyManager");
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    /* renamed from: B */
    public Integer mo2432b() {
        return (Integer) cx.a((wo<TelephonyManager, Integer>) new wo<TelephonyManager, Integer>() {
            /* renamed from: a */
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                int i;
                if (!uw.this.f2835c.mo1653c(uw.this.f2834b.mo2444d())) {
                    return null;
                }
                GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                if (gsmCellLocation != null) {
                    i = gsmCellLocation.getCid();
                } else {
                    i = 1;
                }
                if (1 != i) {
                    return Integer.valueOf(i);
                }
                return null;
            }
        }, this.f2834b.mo2443c(), "getting phoneCellId", "TelephonyManager");
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    /* renamed from: a */
    public Integer mo2433c() {
        return (Integer) cx.a((wo<TelephonyManager, Integer>) new wo<TelephonyManager, Integer>() {
            /* renamed from: a */
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                if (uw.this.f2835c.mo1653c(uw.this.f2834b.mo2444d())) {
                    CellLocation cellLocation = telephonyManager.getCellLocation();
                    if (cellLocation != null) {
                        int lac = ((GsmCellLocation) cellLocation).getLac();
                        if (1 != lac) {
                            return Integer.valueOf(lac);
                        }
                        return null;
                    }
                }
                return null;
            }
        }, this.f2834b.mo2443c(), "getting phoneLac", "TelephonyManager");
    }

    @NonNull
    /* renamed from: h */
    private String m4067h() {
        String str = "unknown";
        return (String) cx.a(new wo<TelephonyManager, String>() {
            /* renamed from: a */
            public String a(TelephonyManager telephonyManager) throws Throwable {
                return (String) uw.f2833a.get(telephonyManager.getNetworkType(), "unknown");
            }
        }, this.f2834b.mo2443c(), "getting networkType", "TelephonyManager", "unknown");
    }

    @Nullable
    /* renamed from: i */
    private String m4068i() {
        return (String) cx.a((wo<TelephonyManager, String>) new wo<TelephonyManager, String>() {
            /* renamed from: a */
            public String a(TelephonyManager telephonyManager) throws Throwable {
                return telephonyManager.getNetworkOperatorName();
            }
        }, this.f2834b.mo2443c(), "getting network operator name", "TelephonyManager");
    }
}
