package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import com.yandex.metrica.impl.ob.rr.a.C0871c;
import com.yandex.metrica.impl.ob.tv.C1053a;

/* renamed from: com.yandex.metrica.impl.ob.mr */
public class mr implements mq<tv, C0871c> {
    @NonNull
    /* renamed from: a */
    public C0871c b(@NonNull tv tvVar) {
        C0871c cVar = new C0871c();
        cVar.f2159e = tvVar.f2658d;
        cVar.f2158d = tvVar.f2657c;
        cVar.f2157c = tvVar.f2656b;
        cVar.f2156b = tvVar.f2655a;
        cVar.f2169o = tvVar.f2659e;
        cVar.f2171q = tvVar.f2660f;
        cVar.f2160f = tvVar.f2661g;
        cVar.f2161g = tvVar.f2662h;
        cVar.f2165k = tvVar.f2667m;
        cVar.f2164j = tvVar.f2665k;
        cVar.f2167m = tvVar.f2669o;
        cVar.f2166l = tvVar.f2668n;
        cVar.f2162h = tvVar.f2663i;
        cVar.f2163i = tvVar.f2664j;
        return cVar;
    }

    @NonNull
    /* renamed from: a */
    public tv a(@NonNull C0871c cVar) {
        return new C1053a().mo2304a(cVar.f2156b).mo2318n(cVar.f2167m).mo2317m(cVar.f2166l).mo2316l(cVar.f2165k).mo2315k(cVar.f2164j).mo2314j(cVar.f2163i).mo2313i(cVar.f2162h).mo2312h(cVar.f2161g).mo2311g(cVar.f2160f).mo2308d(cVar.f2159e).mo2309e(cVar.f2169o).mo2310f(cVar.f2171q).mo2307c(cVar.f2158d).mo2306b(cVar.f2157c).mo2305a();
    }
}
