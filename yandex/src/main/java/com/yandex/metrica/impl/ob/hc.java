package com.yandex.metrica.impl.ob;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import org.json.JSONObject;

/* renamed from: com.yandex.metrica.impl.ob.hc */
public class hc extends hd {
    public hc(en enVar) {
        super(enVar);
    }

    /* renamed from: a */
    public boolean a(@NonNull w wVar) {
        String e = wVar.mo2531e();
        if (!TextUtils.isEmpty(e)) {
            try {
                JSONObject jSONObject = new JSONObject(e);
                if ("open".equals(jSONObject.optString("type")) && m1741a(jSONObject.optString("link"))) {
                    m1742b();
                }
            } catch (Throwable th) {
            }
        }
        return false;
    }

    /* renamed from: B */
    private void m1742b() {
        mo973a().mo828o();
        mo973a().mo839z().mo1010a();
    }

    /* renamed from: a */
    private boolean m1741a(@Nullable String str) {
        String[] split;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            String queryParameter = Uri.parse(str).getQueryParameter("referrer");
            if (TextUtils.isEmpty(queryParameter)) {
                return false;
            }
            for (String str2 : queryParameter.split("&")) {
                int indexOf = str2.indexOf("=");
                if (indexOf >= 0 && "reattribution".equals(Uri.decode(str2.substring(0, indexOf))) && "1".equals(Uri.decode(str2.substring(indexOf + 1)))) {
                    return true;
                }
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }
}
