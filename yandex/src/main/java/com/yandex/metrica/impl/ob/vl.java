package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;

/* renamed from: com.yandex.metrica.impl.ob.vl */
public class vl {
    @SuppressLint({"ObsoleteSdkInt"})
    /* renamed from: a */
    public static void a(Cursor cursor, ContentValues contentValues) {
        if (VERSION.SDK_INT >= 11) {
            b(cursor, contentValues);
        } else {
            DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
        }
    }

    @TargetApi(11)
    /* renamed from: B */
    public static void b(Cursor cursor, ContentValues contentValues) {
        String[] columnNames = cursor.getColumnNames();
        int length = columnNames.length;
        for (int i = 0; i < length; i++) {
            switch (cursor.getType(i)) {
                case 0:
                    contentValues.put(columnNames[i], cursor.getString(i));
                    break;
                case 1:
                    contentValues.put(columnNames[i], Long.valueOf(cursor.getLong(i)));
                    break;
                case 2:
                    contentValues.put(columnNames[i], Double.valueOf(cursor.getDouble(i)));
                    break;
                case 3:
                    contentValues.put(columnNames[i], cursor.getString(i));
                    break;
                case 4:
                    contentValues.put(columnNames[i], cursor.getBlob(i));
                    break;
                default:
                    contentValues.put(columnNames[i], cursor.getString(i));
                    break;
            }
        }
    }

    /* renamed from: a */
    public static long a(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor cursor;
        Cursor cursor2 = null;
        long j = 0;
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT count() FROM " + str, null);
            try {
                if (rawQuery.moveToFirst()) {
                    j = rawQuery.getLong(0);
                }
                cx.m1183a(rawQuery);
                return j;
            } catch (Throwable th) {
                th = th;
                cursor = rawQuery;
                cx.m1183a(cursor);
                throw th;
            }
        } catch (Throwable th2) {
//            th = th2;
            cursor = cursor2;
        }
        return j;
    }
}
