package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;

/* renamed from: com.yandex.metrica.impl.ob.ej */
public class ej implements eq<ei> {
    @NonNull

    /* renamed from: a */
    private final bm f800a;

    public ej() {
        this(new bm());
    }

    public ej(@NonNull bm bmVar) {
        this.f800a = bmVar;
    }

    /* renamed from: a */
    public ei b(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar) {
        return new ei(context, uc.m3891a(), ekVar, egVar, al.m324a().mo285c(), this.f800a.mo444a(context, ekVar));
    }
}
