package com.yandex.metrica.impl.ob;

import android.util.SparseArray;

/* renamed from: com.yandex.metrica.impl.ob.rd */
public class rd {

    /* renamed from: a */
    private static SparseArray<String> f1876a = new SparseArray<>();

    static {
        f1876a.put(0, "String");
        f1876a.put(1, "Number");
        f1876a.put(2, "Counter");
    }

    /* renamed from: a */
    static String m3021a(int i) {
        return (String) f1876a.get(i);
    }
}
