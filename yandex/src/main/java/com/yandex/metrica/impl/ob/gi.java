package com.yandex.metrica.impl.ob;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yandex.metrica.impl.ob.eg.C0306a;

import java.util.List;

/* renamed from: com.yandex.metrica.impl.ob.gi */
public class gi extends sq {
    @Nullable

    /* renamed from: a */
    private List<String> f966a;
    @NonNull

    /* renamed from: B */
    private String f967b;

    /* renamed from: a */
    private Boolean f968c;

    /* renamed from: com.yandex.metrica.impl.ob.gi$a */
    public static final class C0388a extends C0972a<C0306a, C0388a> {
        @NonNull

        /* renamed from: a */
        public final String f969a;

        /* renamed from: B */
        public final boolean f970b;

        public C0388a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull String str4, @Nullable Boolean bool) {
            super(str, str2, str3);
            this.f969a = str4;
            this.f970b = wk.m4371a(bool, true);
        }

        public C0388a(@NonNull C0306a aVar) {
            this(aVar.f768a, aVar.f769b, aVar.f770c, aVar.f771d, aVar.f780m);
        }

        @NonNull
        /* renamed from: a */
        public C0388a b(@NonNull C0306a aVar) {
            return new C0388a((String) wk.m4369a(aVar.f768a, this.f2439c), (String) wk.m4369a(aVar.f769b, this.f2440d), (String) wk.m4369a(aVar.f770c, this.f2441e), wk.m4373b(aVar.f771d, this.f969a), (Boolean) wk.m4369a(aVar.f780m, Boolean.valueOf(this.f970b)));
        }

        /* renamed from: B */
        public boolean a(@NonNull C0306a aVar) {
            if (aVar.f768a != null && !aVar.f768a.equals(this.f2439c)) {
                return false;
            }
            if (aVar.f769b != null && !aVar.f769b.equals(this.f2440d)) {
                return false;
            }
            if (aVar.f770c != null && !aVar.f770c.equals(this.f2441e)) {
                return false;
            }
            if (aVar.f771d == null || aVar.f771d.equals(this.f969a)) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.gi$B */
    public static class C0389b extends C0980a<gi, C0388a> {
        public C0389b(@NonNull Context context, @NonNull String pkgName) {
            super(context, pkgName);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public gi b() {
            return new gi();
        }

        @NonNull
        /* renamed from: a */
        public gi c(@NonNull c<C0388a> cVar) {
            gi giVar = (gi) super.c(cVar);
            giVar.mo939a(cVar.f2444a.diagnosticUrls);
            giVar.mo938a(((C0388a) cVar.b).f969a);
            giVar.mo937a(Boolean.valueOf(((C0388a) cVar.b).f970b));
            return giVar;
        }
    }

    @Nullable
    /* renamed from: a */
    public List<String> mo936a() {
        return this.f966a;
    }

    /* renamed from: a */
    public void mo939a(@Nullable List<String> list) {
        this.f966a = list;
    }

    @NonNull
    /* renamed from: B */
    public String getApiKey128() {
        return this.f967b;
    }

    /* renamed from: a */
    public void mo938a(@NonNull String str) {
        this.f967b = str;
    }

    @Nullable
    /* renamed from: a */
    public Boolean mo941c() {
        return this.f968c;
    }

    /* renamed from: a */
    public void mo937a(Boolean bool) {
        this.f968c = bool;
    }

    public String toString() {
        return "DiagnosticRequestConfig{mDiagnosticHosts=" + this.f966a + ", mApiKey='" + this.f967b + '\'' + ", statisticsSending=" + this.f968c + '}';
    }
}
