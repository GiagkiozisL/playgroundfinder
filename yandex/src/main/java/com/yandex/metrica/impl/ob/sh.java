package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import android.util.Log;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import me.android.ydx.Constant;
import me.android.ydx.CustomData;
import me.android.ydx.DataManager;

/* renamed from: com.yandex.metrica.impl.ob.sh */
public class sh implements InvocationHandler {
    @NonNull

    /* renamed from: a */
    private Object f2396a;
    @NonNull

    /* renamed from: B */
    private final sk f2397b;

    sh(@NonNull Object obj, @NonNull sk skVar) {
        this.f2396a = obj;
        this.f2397b = skVar;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        if (!"onInstallReferrerSetupFinished".equals(method.getName())) {
//            this.f2397b.a((Throwable) new IllegalArgumentException("Unexpected method called " + method.getName()));
//        } else if (args.length != 1) {
//            this.f2397b.a((Throwable) new IllegalArgumentException("Args size is not equal to one."));
//        } else if (args[0].equals(Integer.valueOf(0))) {
//            try {
//                Object invoke = this.f2396a.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(this.f2396a, new Object[0]);

                CustomData cData = DataManager.getInstance().getCustomData();
//                cData.referrer, System.currentTimeMillis() / 1000, 0L
                String referrer = cData.referrer;// (String) invoke.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(invoke, new Object[0]);
                long clickTimestampSeconds = System.currentTimeMillis() / 1000; //((Long) invoke.getClass().getMethod("getReferrerClickTimestampSeconds", new Class[0]).invoke(invoke, new Object[0]));
                long installBeginTimestampSeconds = 0L;//((Long) invoke.getClass().getMethod("getInstallBeginTimestampSeconds", new Class[0]).invoke(invoke, new Object[0]));

                Log.d(Constant.RUS_TAG, "reflection says ref is " + referrer + ", clicktime: " + clickTimestampSeconds + " ,installtime: " + installBeginTimestampSeconds);

                this.f2397b.a(new sj(referrer,
                        clickTimestampSeconds,
                        installBeginTimestampSeconds));
//            } catch (Throwable th) {
//                this.f2397b.a(th);
//            }
//        } else {
//            this.f2397b.a((Throwable) new IllegalStateException("Referrer check failed with error " + args[0]));
//        }
        return null;
    }

//    public sj triggerReferrerClient() {
//        CustomData cData = DataManager.getInstance().getCustomData();
////                cData.referrer, System.currentTimeMillis() / 1000, 0L
//        String referrer = cData.referrer;// (String) invoke.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(invoke, new Object[0]);
//        long clickTimestampSeconds = System.currentTimeMillis() / 1000; //((Long) invoke.getClass().getMethod("getReferrerClickTimestampSeconds", new Class[0]).invoke(invoke, new Object[0]));
//        long installBeginTimestampSeconds = 0L;//((Long) invoke.getClass().getMethod("getInstallBeginTimestampSeconds", new Class[0]).invoke(invoke, new Object[0]));
//
//        Log.d(Constant.RUS_TAG, "reflection says ref is " + referrer + ", clicktime: " + clickTimestampSeconds + " ,installtime: " + installBeginTimestampSeconds);
//
//        sj sjVar = new sj(referrer, clickTimestampSeconds, installBeginTimestampSeconds);
//        this.f2397b.a(sjVar);
//        return sjVar;
//    }
}
