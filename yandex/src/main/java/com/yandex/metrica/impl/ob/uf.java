package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IParamsCallback;
import com.yandex.metrica.IParamsCallback.Reason;
import com.yandex.metrica.impl.ob.x.C1181a;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

/* renamed from: com.yandex.metrica.impl.ob.uf */
public class uf implements ug {

    /* renamed from: a */
    static final Map<ue, Reason> f2723a = Collections.unmodifiableMap(new HashMap<ue, Reason>() {
        {
            put(ue.UNKNOWN, Reason.UNKNOWN);
            put(ue.NETWORK, Reason.NETWORK);
            put(ue.PARSE, Reason.INVALID_RESPONSE);
        }
    });

    /* renamed from: B */
    private final List<String> f2724b;

    /* renamed from: a */
    private final cd f2725c;

    /* renamed from: d */
    private final ui f2726d;
    @NonNull

    /* renamed from: e */
    private final Handler f2727e;
    @Nullable

    /* renamed from: f */
    private vz f2728f;

    /* renamed from: g */
    private final C1181a f2729g;

    /* renamed from: h */
    private final Object f2730h;

    /* renamed from: i */
    private final Map<tx, List<String>> f2731i;

    /* renamed from: j */
    private Map<String, String> f2732j;

    public uf(cd cdVar, lv lvVar, @NonNull Handler handler) {
        this(cdVar, new ui(lvVar), handler);
    }

    @VisibleForTesting
    uf(cd cdVar, @NonNull ui uiVar, @NonNull Handler handler) {
        this.f2724b = Arrays.asList(new String[]{"yandex_mobile_metrica_uuid", "yandex_mobile_metrica_device_id", "appmetrica_device_id_hash", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url", IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS});
        this.f2730h = new Object();
        this.f2731i = new WeakHashMap();
        this.f2725c = cdVar;
        this.f2726d = uiVar;
        this.f2727e = handler;
        this.f2729g = new C1181a() {
            /* renamed from: a */
            public void mo677a(int i, @NonNull Bundle bundle) {
            }
        };
    }

    /* renamed from: a */
    public String a() {
        return this.f2726d.mo2357e();
    }

    /* renamed from: B */
    public String mo2345b() {
        return this.f2726d.mo2358f();
    }

    /* renamed from: a */
    public void mo2340a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list, @Nullable Map<String, String> map) {
        m3911a((tx) new tu(iIdentifierCallback), list, map);
    }

    /* renamed from: a */
    private void m3911a(final tx txVar, @NonNull List<String> list, @Nullable Map<String, String> map) {
        synchronized (this.f2730h) {
            this.f2726d.mo2350a(map);
            m3916b(txVar, list);
            if (this.f2726d.mo2355c() || !this.f2726d.mo2352a(list)) {
                m3912a(list, (C1181a) new C1181a() {
                    /* renamed from: a */
                    public void mo677a(int i, Bundle bundle) {
                        uf.this.mo2339a(i, bundle, txVar);
                    }
                }, map);
            } else {
                m3907a(txVar);
            }
        }
    }

    /* renamed from: a */
    public void mo2338a(int i, @NonNull Bundle bundle) {
        mo2339a(i, bundle, (tx) null);
    }

    /* renamed from: a */
    public void mo2339a(int i, @NonNull Bundle bundle, @Nullable tx txVar) {
        synchronized (this.f2730h) {
            m3906a(bundle, i);
            m3918d();
            if (txVar != null) {
                m3908a(txVar, bundle);
            }
        }
    }

    /* renamed from: a */
    public void mo2341a(@NonNull vz vzVar) {
        this.f2728f = vzVar;
    }

    /* renamed from: B */
    private void m3917b(@Nullable Map<String, String> map) {
        m3913a(this.f2724b, map);
    }

    /* renamed from: a */
    private void m3913a(@NonNull List<String> list, @Nullable Map<String, String> map) {
        m3912a(list, this.f2729g, map);
    }

    /* renamed from: a */
    private void m3912a(@NonNull List<String> list, @NonNull C1181a aVar, @Nullable Map<String, String> map) {
        this.f2725c.mo579a(list, (ResultReceiver) new x(this.f2727e, aVar), map);
    }

    /* renamed from: a */
    private void m3906a(@NonNull Bundle bundle, int i) {
        this.f2726d.mo2348a(bundle);
        if (i == 1) {
            this.f2726d.mo2347a(wi.m4360b());
        }
        m3918d();
    }

    /* renamed from: a */
    public void mo2346c() {
        synchronized (this.f2730h) {
            if (!this.f2726d.mo2354b() || this.f2726d.mo2355c()) {
                m3917b(this.f2732j);
            }
        }
    }

    /* renamed from: a */
    public void mo2343a(List<String> list) {
        synchronized (this.f2730h) {
            List d = this.f2726d.mo2356d();
            if (cx.a((Collection) list)) {
                if (!cx.a((Collection) d)) {
                    this.f2726d.mo2353b(null);
                    this.f2725c.mo578a(null);
                }
            } else if (!cx.a((Object) list, (Object) d)) {
                this.f2726d.mo2353b(list);
                this.f2725c.mo578a(list);
            } else {
                this.f2725c.mo578a(d);
            }
        }
    }

    /* renamed from: a */
    public void mo2344a(Map<String, String> map) {
        synchronized (this.f2730h) {
            Map<String, String> c = we.m4343c(map);
            this.f2732j = c;
            this.f2725c.mo580a(c);
            this.f2726d.mo2350a(c);
        }
    }

    /* renamed from: a */
    public void mo2342a(String str) {
        synchronized (this.f2730h) {
            this.f2725c.mo584b(str);
        }
    }

    /* renamed from: a */
    private void m3907a(@NonNull tx txVar) {
        m3908a(txVar, new Bundle());
    }

    /* renamed from: a */
    private void m3908a(@NonNull tx txVar, @NonNull Bundle bundle) {
        if (this.f2731i.containsKey(txVar)) {
            List list = (List) this.f2731i.get(txVar);
            if (this.f2726d.mo2352a(list)) {
                m3909a(txVar, list);
            } else {
                ue b = ue.m3903b(bundle);
                Reason reason = null;
                if (b == null) {
                    if (!this.f2726d.mo2351a()) {
                        if (this.f2728f != null) {
                            this.f2728f.mo2482b("Clids error. Passed clids: %s, and clids from server are empty.", this.f2732j);
                        }
                        reason = Reason.INCONSISTENT_CLIDS;
                    } else {
                        b = ue.UNKNOWN;
                    }
                }
                if (reason == null) {
                    reason = (Reason) cx.m1174a(f2723a, b, Reason.UNKNOWN);
                }
                m3910a(txVar, list, reason);
            }
            m3915b(txVar);
        }
    }

    /* renamed from: a */
    private void m3909a(@NonNull tx txVar, @NonNull List<String> list) {
        txVar.a(m3914b(list));
    }

    /* renamed from: a */
    private void m3910a(@NonNull tx txVar, @NonNull List<String> list, @NonNull Reason reason) {
        txVar.a(reason, m3914b(list));
    }

    /* renamed from: d */
    private void m3918d() {
        WeakHashMap var1 = new WeakHashMap();
        Iterator var2 = this.f2731i.entrySet().iterator();

        Entry var3;
        while(var2.hasNext()) {
            var3 = (Entry)var2.next();
            if (this.f2726d.mo2352a((List)var3.getValue())) {
                var1.put(var3.getKey(), var3.getValue());
            }
        }

        var2 = var1.entrySet().iterator();

        while(var2.hasNext()) {
            var3 = (Entry)var2.next();
            tx var4 = (tx)var3.getKey();
            if (var4 != null) {
                this.m3907a(var4);
            }
        }

        var1.clear();
//        WeakHashMap weakHashMap = new WeakHashMap();
//        for (Entry entry : this.f2731i.entrySet()) {
//            if (this.f2726d.mo2352a((List) entry.getValue())) {
//                weakHashMap.put(entry.getKey(), entry.getValue());
//            }
//        }
//        for (Entry key : weakHashMap.entrySet()) {
//            tx txVar = (tx) key.getKey();
//            if (txVar != null) {
//                m3907a(txVar);
//            }
//        }
//        weakHashMap.clear();
    }

    @Nullable
    /* renamed from: B */
    private Map<String, String> m3914b(@Nullable List<String> list) {
        if (list == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        this.f2726d.mo2349a(list, (Map<String, String>) hashMap);
        return hashMap;
    }

    /* renamed from: B */
    private void m3916b(tx txVar, List<String> list) {
        if (this.f2731i.isEmpty()) {
            this.f2725c.mo586c();
        }
        this.f2731i.put(txVar, list);
    }

    /* renamed from: B */
    private void m3915b(tx txVar) {
        this.f2731i.remove(txVar);
        if (this.f2731i.isEmpty()) {
            this.f2725c.mo587d();
        }
    }
}
