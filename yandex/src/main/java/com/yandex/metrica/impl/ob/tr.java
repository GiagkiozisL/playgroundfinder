package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.yandex.metrica.impl.ob.tr */
interface tr {
    /* renamed from: a */
    void a(int i);

    /* renamed from: a */
    void a(@NonNull String str);

    /* renamed from: a */
    void a(@NonNull String str, @Nullable String str2);

    /* renamed from: a */
    void a(@NonNull String str, Throwable th);
}
