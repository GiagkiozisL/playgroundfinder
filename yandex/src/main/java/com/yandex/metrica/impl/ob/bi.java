package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.util.SparseArray;

import com.yandex.metrica.YandexMetrica;

/* renamed from: com.yandex.metrica.impl.ob.bi */
public abstract class bi {

    /* renamed from: com.yandex.metrica.impl.ob.bi$a */
    interface C0129a {
        /* renamed from: a */
        void mo429a(Context context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract int mo424a(qg qgVar);

    /* access modifiers changed from: 0000 */
    /* renamed from: a */
    public abstract SparseArray<C0129a> mo425a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo427a(qg qgVar, int i);

    /* renamed from: a */
    public void mo426a(Context context) {
        qg qgVar = new qg(context);
        int a = mo424a(qgVar);
        int b = mo428b();
        if (a < b) {
            if (a > 0) {
                m639a(context, a, b);
            }
            mo427a(qgVar, b);
            qgVar.mo1699j();
        }
    }

    /* access modifiers changed from: 0000 */
    /* renamed from: B */
    public int mo428b() {
        return YandexMetrica.getLibraryApiLevel();
    }

    /* renamed from: a */
    private void m639a(Context context, int i, int i2) {
        SparseArray a = mo425a();
        for (int i3 = i + 1; i3 <= i2; i3++) {
            C0129a aVar = (C0129a) a.get(i3);
            if (aVar != null) {
                aVar.mo429a(context);
            }
        }
    }
}
