package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.cy */
public class cy implements p {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: f */
    private static volatile cy f645f;

    /* renamed from: g */
    private static final Object f646g = new Object();

    /* renamed from: a */
    private final Context f647a;
    @Nullable

    /* renamed from: B */
    private final WifiManager f648b;
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a */
    public pr f649c;
    @NonNull

    /* renamed from: d */
    private px f650d;
    @NonNull

    /* renamed from: e */
    private wf f651e;

    /* renamed from: h */
    private C0740a<JSONArray> f652h;

    /* renamed from: i */
    private C0740a<List<C0248a>> f653i;

    /* renamed from: j */
    private uk f654j;

    /* renamed from: k */
    private final ps f655k;

    /* renamed from: com.yandex.metrica.impl.ob.cy$a */
    public static final class C0248a {

        /* renamed from: a */
        public final String f666a;

        /* renamed from: B */
        public final String f667b;

        public C0248a(String str, String str2) {
            this.f666a = str;
            this.f667b = str2;
        }
    }

    @SuppressLint("WrongConstant")
    private cy(Context context) {
        this(context, (WifiManager) context.getApplicationContext().getSystemService("wifi"), new px());
    }

    private cy(Context context, @Nullable WifiManager wifiManager, px pxVar) {
        this(context, wifiManager, pxVar, new pr(pxVar.mo1666a()), new wf());
    }

    /* renamed from: a */
    public static cy m1204a(Context context) {
        if (f645f == null) {
            synchronized (f646g) {
                if (f645f == null) {
                    f645f = new cy(context.getApplicationContext());
                }
            }
        }
        return f645f;
    }

    /* renamed from: a */
    public void mo665a(@NonNull uk ukVar) {
        this.f654j = ukVar;
        this.f650d.mo1667a(ukVar);
        this.f649c.mo1649a(this.f650d.mo1666a());
    }

    /* renamed from: a */
    public synchronized JSONArray mo664a() {
        JSONArray jSONArray;
        if (!m1217f()) {
            jSONArray = new JSONArray();
        } else {
            if (this.f652h.mo1623b() || this.f652h.mo1624c()) {
                this.f652h.mo1621a(m1216e());
            }
            jSONArray = (JSONArray) this.f652h.mo1620a();
        }
        return jSONArray;
    }

    @Nullable
    /* renamed from: a */
    private String m1206a(@Nullable String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase(Locale.US).replace(":", "");
    }

    @Nullable
    /* renamed from: a */
    private List<ScanResult> m1214c() {
        return (List) cx.a((wo<WifiManager, List<ScanResult>>) new wo<WifiManager, List<ScanResult>>() {
            /* renamed from: a */
            public List<ScanResult> a(WifiManager wifiManager) throws Throwable {
                return wifiManager.getScanResults();
            }
        }, this.f648b, "getting scan results", "WifiManager");
    }

    @Nullable
    /* renamed from: d */
    private WifiInfo m1215d() {
        return (WifiInfo) cx.a((wo<WifiManager, WifiInfo>) new wo<WifiManager, WifiInfo>() {
            /* renamed from: a */
            public WifiInfo a(WifiManager wifiManager) throws Throwable {
                WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                if (connectionInfo == null) {
                    return connectionInfo;
                }
                if ("00:00:00:00:00:00".equals(connectionInfo.getBSSID()) || "02:00:00:00:00:00".equals(connectionInfo.getBSSID()) || connectionInfo.getIpAddress() == 0) {
                    return null;
                }
                return connectionInfo;
            }
        }, this.f648b, "getting connection info", "WifiManager");
    }

    /* renamed from: e */
    private JSONArray m1216e() {
        List<ScanResult> list;
        WifiInfo wifiInfo;
        String str = null;
        if (!m1221j() || !this.f655k.a(this.f647a)) {
            list = null;
        } else {
            list = m1214c();
        }
        if (!m1220i() || !this.f649c.mo1655e(this.f647a)) {
            wifiInfo = null;
        } else {
            wifiInfo = m1215d();
        }
        String bssid = wifiInfo == null ? null : wifiInfo.getBSSID();
        JSONArray jSONArray = new JSONArray();
        if (list != null) {
            for (ScanResult scanResult : list) {
                if (scanResult != null && !"02:00:00:00:00:00".equals(scanResult.BSSID)) {
                    JSONObject a = m1207a(bssid, scanResult);
                    if (a != null) {
                        jSONArray.put(a);
                    }
                }
            }
        } else if (wifiInfo != null) {
            String a2 = m1206a(bssid);
            String ssid = wifiInfo.getSSID();
            if (ssid != null) {
                str = m1213b(ssid);
            }
            JSONObject a3 = m1208a(a2, str, true, wifiInfo.getRssi(), Long.valueOf(0));
            if (a3 != null) {
                jSONArray.put(a3);
            }
        }
        return jSONArray;
    }

    /* renamed from: a */
    private JSONObject m1207a(@Nullable String str, @NonNull ScanResult scanResult) {
        boolean z;
        String str2 = null;
        boolean z2 = false;
        try {
            String str3 = scanResult.BSSID;
            if (str3 != null) {
                z2 = str3.equals(str);
                try {
                    str2 = m1206a(str3);
                } catch (NoSuchFieldError e) {
                    z = z2;
                }
            }
            z = z2;
        } catch (NoSuchFieldError e2) {
            z = false;
        }
        return m1208a(str2, scanResult.SSID, z, scanResult.level, m1205a(scanResult));
    }

    @Nullable
    /* renamed from: a */
    private Long m1205a(@NonNull ScanResult scanResult) {
        if (VERSION.SDK_INT >= 17) {
            return m1212b(scanResult);
        }
        return null;
    }

    @TargetApi(17)
    @NonNull
    /* renamed from: B */
    private Long m1212b(@NonNull ScanResult scanResult) {
        return Long.valueOf(this.f651e.mo2560a(scanResult.timestamp, TimeUnit.MICROSECONDS));
    }

    @NonNull
    /* renamed from: B */
    private String m1213b(@NonNull String str) {
        return str.replace("\"", "");
    }

    /* renamed from: a */
    private JSONObject m1208a(@Nullable String var1, @Nullable String var2, boolean var3, int var4, @Nullable Long var5) {
        JSONObject var6 = null;

        try {
            var6 = (new JSONObject()).put("mac", var1).put("ssid", var2).put("signal_strength", var4).put("is_connected", var3).put("last_visible_offset_seconds", var5);
        } catch (Throwable var8) {
        }

        return var6;
//        boolean z2 = false;
//        try {
//            return new JSONObject().put("mac", str).put("ssid", str2).put("signal_strength", i).put("is_connected", z).put("last_visible_offset_seconds", l);
//        } catch (Throwable th) {
//            return z2;
//        }
    }

    /* renamed from: f */
    private boolean m1217f() {
        return ((Boolean) cx.a(new wo<WifiManager, Boolean>() {
            /* renamed from: a */
            public Boolean a(WifiManager wifiManager) throws Throwable {
                return Boolean.valueOf(wifiManager.isWifiEnabled());
            }
        }, this.f648b, "getting wifi enabled state", "WifiManager", Boolean.valueOf(false))).booleanValue();
    }

    /* renamed from: B */
    public List<C0248a> mo668b() {
        if (this.f653i.mo1623b() || this.f653i.mo1624c()) {
            ArrayList arrayList = new ArrayList();
            m1209a((List<C0248a>) arrayList);
            this.f653i.mo1621a(arrayList);
        }
        return (List) this.f653i.mo1620a();
    }

    /* renamed from: a */
    private void m1209a(List<C0248a> list) {
        if (m1219h()) {
            StringBuilder sb = new StringBuilder();
            try {
                Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
                while (it.hasNext()) {
                    NetworkInterface networkInterface = (NetworkInterface) it.next();
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress != null) {
                        for (byte valueOf : hardwareAddress) {
                            sb.append(String.format(Locale.US, "%02X:", new Object[]{Byte.valueOf(valueOf)}));
                        }
                        if (sb.length() > 0) {
                            sb.deleteCharAt(sb.length() - 1);
                            list.add(new C0248a(networkInterface.getName(), sb.toString()));
                            sb.setLength(0);
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    @Nullable
    /* renamed from: B */
    public String mo667b(final Context context) {
        return (String) cx.a((wo<WifiManager, String>) new wo<WifiManager, String>() {
            /* renamed from: a */
            public String a(WifiManager wifiManager) throws Throwable {
                if (!cy.this.m1218g() || !cy.this.f649c.mo1655e(context)) {
                    return null;
                }
                WifiConfiguration wifiConfiguration = (WifiConfiguration) wifiManager.getClass().getMethod("getWifiApConfiguration", new Class[0]).invoke(wifiManager, new Object[0]);
                if (wifiConfiguration != null) {
                    return wifiConfiguration.SSID;
                }
                return null;
            }
        }, this.f648b, "getting wifi access point name", "WifiManager");
    }

    /* renamed from: a */
    public int mo669c(final Context context) {
        return ((Integer) cx.a(new wo<WifiManager, Integer>() {
            /* renamed from: a */
            public Integer a(WifiManager wifiManager) throws Throwable {
                if (!cy.this.m1218g() || !cy.this.f649c.mo1655e(context)) {
                    return null;
                }
                int intValue = ((Integer) wifiManager.getClass().getMethod("getWifiApState", new Class[0]).invoke(wifiManager, new Object[0])).intValue();
                if (intValue > 10) {
                    intValue -= 10;
                }
                return Integer.valueOf(intValue);
            }
        }, this.f648b, "getting access point state", "WifiManager", Integer.valueOf(-1))).intValue();
    }

    /* renamed from: a */
    public void mo666a(boolean z) {
        this.f650d.mo1668a(z);
        this.f649c.mo1649a(this.f650d.mo1666a());
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public synchronized boolean m1218g() {
        return m1222k() && this.f654j.collectingFlags.f2666l;
    }

    /* renamed from: h */
    private synchronized boolean m1219h() {
        return m1222k() && this.f654j.collectingFlags.f2665k;
    }

    /* renamed from: i */
    private synchronized boolean m1220i() {
        return m1222k() && this.f654j.collectingFlags.f2664j;
    }

    /* renamed from: j */
    private synchronized boolean m1221j() {
        return m1222k() && this.f654j.collectingFlags.f2663i;
    }

    /* renamed from: k */
    private synchronized boolean m1222k() {
        return this.f654j != null;
    }

    @VisibleForTesting
    cy(Context context, @Nullable WifiManager wifiManager, @NonNull px pxVar, @NonNull pr prVar, @NonNull wf wfVar) {
        this.f650d = new px();
        this.f652h = new C0740a<>();
        this.f653i = new C0740a<>();
        this.f647a = context;
        this.f648b = wifiManager;
        this.f650d = pxVar;
        this.f649c = prVar;
        this.f655k = m1223l();
        this.f651e = wfVar;
    }

    @NonNull
    /* renamed from: l */
    private ps m1223l() {
        if (cx.a(28)) {
            return new ps() {
                /* renamed from: a */
                public boolean a(@NonNull Context context) {
                    return cy.this.f649c.mo1653c(context) && cy.this.f649c.mo1655e(context);
                }
            };
        }
        if (cx.a(26)) {
            return new ps() {
                /* renamed from: a */
                public boolean a(@NonNull Context context) {
                    return cy.this.f649c.mo1653c(context) || cy.this.f649c.mo1656f(context);
                }
            };
        }
        return new ps() {
            /* renamed from: a */
            public boolean a(@NonNull Context context) {
                return cy.this.f649c.mo1653c(context);
            }
        };
    }
}
