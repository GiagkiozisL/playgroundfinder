package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.regex.Pattern;

/* renamed from: com.yandex.metrica.impl.ob.av */
public class av {

    /* renamed from: a */
    private static final Pattern f260a = Pattern.compile("com\\.yandex\\.metrica\\.(?!push)");

    /* renamed from: B */
    private static final Pattern f261b = Pattern.compile("com\\.yandex\\.metrica\\.push\\..*");

    /* renamed from: a */
    public boolean mo325a(@NonNull List<StackTraceElement> list) {
        return m400a(list, f260a);
    }

    /* renamed from: B */
    public boolean mo326b(@NonNull List<StackTraceElement> list) {
        return m400a(list, f261b);
    }

    /* renamed from: a */
    private boolean m400a(@NonNull List<StackTraceElement> list, @NonNull Pattern pattern) {
        for (StackTraceElement className : list) {
            if (pattern.matcher(className.getClassName()).find()) {
                return true;
            }
        }
        return false;
    }
}
