package com.yandex.metrica.impl.ob;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.yandex.metrica.impl.ob.h.C0415a;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: com.yandex.metrica.impl.ob.ai */
public class ai {
    @NonNull

    /* renamed from: a */
    private final List<C0065c> f212a = new ArrayList();

    /* renamed from: com.yandex.metrica.impl.ob.ai$a */
    public static class C0063a {

        /* renamed from: a */
        private boolean f213a;

        /* renamed from: B */
        private long f214b;

        /* renamed from: a */
        private long f215c;

        /* renamed from: d */
        private long f216d;
        @NonNull

        /* renamed from: e */
        private C0064b f217e;

        C0063a() {
            this(new C0064b());
        }

        public C0063a(@NonNull C0064b bVar) {
            this.f217e = bVar;
            this.f213a = false;
            this.f216d = Long.MAX_VALUE;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public boolean mo273a() {
            if (this.f213a) {
                return true;
            }
            return this.f217e.mo275a(this.f215c, this.f214b, this.f216d);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo271a(long j, @NonNull TimeUnit timeUnit) {
            this.f216d = timeUnit.toMillis(j);
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: B */
        public void mo274b() {
            this.f213a = true;
        }

        /* access modifiers changed from: 0000 */
        /* renamed from: a */
        public void mo272a(@Nullable uk ukVar) {
            if (ukVar != null) {
                this.f214b = TimeUnit.SECONDS.toMillis(ukVar.obtainServerTime);
                this.f215c = TimeUnit.SECONDS.toMillis(ukVar.firstStartupServerTime);
            }
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ai$B */
    public static class C0064b {
        /* renamed from: a */
        public boolean mo275a(long j, long j2, long j3) {
            return j2 - j >= j3;
        }
    }

    /* renamed from: com.yandex.metrica.impl.ob.ai$a */
    public static class C0065c {
        @NonNull

        /* renamed from: a */
        private C0063a f218a;
        @NonNull

        /* renamed from: B */
        private final C0415a f219b;
        @NonNull

        /* renamed from: a */
        private final xh f220c;

        private C0065c(@NonNull xh xhVar, @NonNull C0415a aVar, @NonNull C0063a aVar2) {
            this.f219b = aVar;
            this.f218a = aVar2;
            this.f220c = xhVar;
        }

        /* renamed from: a */
        public void mo277a(@Nullable uk ukVar) {
            this.f218a.mo272a(ukVar);
        }

        /* renamed from: a */
        public void mo276a(long j) {
            this.f218a.mo271a(j, TimeUnit.SECONDS);
        }

        /* renamed from: a */
        public boolean mo278a(int i) {
            if (!this.f218a.mo273a()) {
                return false;
            }
            this.f219b.mo971a(TimeUnit.SECONDS.toMillis((long) i), this.f220c);
            this.f218a.mo274b();
            return true;
        }
    }

    /* renamed from: a */
    public C0065c mo269a(@NonNull Runnable runnable, @NonNull xh xhVar) {
        return mo268a(xhVar, new C0415a(runnable), new C0063a());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    /* renamed from: a */
    public C0065c mo268a(@NonNull xh xhVar, @NonNull C0415a aVar, @NonNull C0063a aVar2) {
        C0065c cVar = new C0065c(xhVar, aVar, aVar2);
        this.f212a.add(cVar);
        return cVar;
    }

    /* renamed from: a */
    public void mo270a(@Nullable uk ukVar) {
        for (C0065c a : this.f212a) {
            a.mo277a(ukVar);
        }
    }
}
