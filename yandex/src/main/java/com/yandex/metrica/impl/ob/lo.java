package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;

import java.io.Closeable;

/* renamed from: com.yandex.metrica.impl.ob.lo */
public class lo implements lm {

    /* renamed from: a */
    private final Context f1334a;

    /* renamed from: B */
    private final String f1335b;
    @NonNull

    /* renamed from: a */
    private final la f1336c;
    @NonNull

    /* renamed from: d */
    private final ln f1337d;

    /* renamed from: e */
    private lc f1338e;

    public lo(Context context, String str) {
        this(context, str, new ln(context, str), lq.m2213a());
    }

    @VisibleForTesting
    public lo(@NonNull Context context, @NonNull String str, @NonNull ln lnVar, @NonNull la laVar) {
        this.f1334a = context;
        this.f1335b = str;
        this.f1337d = lnVar;
        this.f1336c = laVar;
    }

    @Nullable
    @WorkerThread
    /* renamed from: a */
    public synchronized SQLiteDatabase a() {
        SQLiteDatabase sQLiteDatabase;
        try {
            this.f1337d.mo1271a();
            this.f1338e = new lc(this.f1334a, this.f1335b, this.f1336c.mo1218c());
            sQLiteDatabase = this.f1338e.getWritableDatabase();
        } catch (Throwable th) {
            sQLiteDatabase = null;
        }
        return sQLiteDatabase;
    }

    @WorkerThread
    /* renamed from: a */
    public synchronized void a(@Nullable SQLiteDatabase sQLiteDatabase) {
        cx.b(sQLiteDatabase);
        cx.a((Closeable) this.f1338e);
        this.f1337d.mo1272b();
        this.f1338e = null;
    }
}
