package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.yandex.metrica.impl.ac.a.c;

import java.util.Map;

/* renamed from: com.yandex.metrica.impl.ob.ta */
public class ta implements sw<su> {
    @NonNull

    /* renamed from: a */
    private final ss f2528a;

    public ta(@NonNull ss ssVar) {
        this.f2528a = ssVar;
    }

    /* renamed from: a */
    public void mo2218a(@NonNull Builder builder, @NonNull su suVar) {
        builder.path("analytics/startup");
        builder.appendQueryParameter(this.f2528a.getHardMap("deviceid"), suVar.getDeviceId());
        builder.appendQueryParameter(this.f2528a.getHardMap("deviceid2"), suVar.mo2106s());
        mo2217a(builder, al.m324a().mo290h(), suVar);
        builder.appendQueryParameter(this.f2528a.getHardMap("app_platform"), suVar.getAppPlatform());
        builder.appendQueryParameter(this.f2528a.getHardMap("protocol_version"), suVar.getProtocolVersion());
        builder.appendQueryParameter(this.f2528a.getHardMap("analytics_sdk_version_name"), suVar.getSDKVersionName());
        builder.appendQueryParameter(this.f2528a.getHardMap("model"), suVar.getModel());
        builder.appendQueryParameter(this.f2528a.getHardMap("manufacturer"), suVar.getManufacturer());
        builder.appendQueryParameter(this.f2528a.getHardMap("os_version"), suVar.getOsVersion());
        builder.appendQueryParameter(this.f2528a.getHardMap("screen_width"), String.valueOf(suVar.getScreenWidth()));
        builder.appendQueryParameter(this.f2528a.getHardMap("screen_height"), String.valueOf(suVar.getScreenHeight()));
        builder.appendQueryParameter(this.f2528a.getHardMap("screen_dpi"), String.valueOf(suVar.getScreenDpi()));
        builder.appendQueryParameter(this.f2528a.getHardMap("scalefactor"), String.valueOf(suVar.getScaleFactore()));
        builder.appendQueryParameter(this.f2528a.getHardMap("locale"), suVar.A());
        builder.appendQueryParameter(this.f2528a.getHardMap("device_type"), suVar.getDeviceType());
        builder.appendQueryParameter(this.f2528a.getHardMap("queries"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("query_hosts"), String.valueOf(2));
        builder.appendQueryParameter(this.f2528a.getHardMap("features"), cu.m1158b(this.f2528a.getHardMap("easy_collecting"), this.f2528a.getHardMap("package_info"), this.f2528a.getHardMap("socket"), this.f2528a.getHardMap("permissions_collecting"), this.f2528a.getHardMap("features_collecting"), this.f2528a.getHardMap("foreground_location_collection"), this.f2528a.getHardMap("background_location_collection"), this.f2528a.getHardMap("foreground_lbs_collection"), this.f2528a.getHardMap("background_lbs_collection"), this.f2528a.getHardMap("telephony_restricted_to_location_tracking"), this.f2528a.getHardMap("android_id"), this.f2528a.getHardMap("google_aid"), this.f2528a.getHardMap("wifi_around"), this.f2528a.getHardMap("wifi_connected"), this.f2528a.getHardMap("own_macs"), this.f2528a.getHardMap("cells_around"), this.f2528a.getHardMap("sim_info"), this.f2528a.getHardMap("sim_imei"), this.f2528a.getHardMap("access_point"), this.f2528a.getHardMap("sdk_list"), this.f2528a.getHardMap("identity_light_collecting"), this.f2528a.getHardMap("ble_collecting")));
        builder.appendQueryParameter(this.f2528a.getHardMap("socket"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("app_id"), suVar.getAppId());
        builder.appendQueryParameter(this.f2528a.getHardMap("foreground_location_collection"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("app_debuggable"), "0");//suVar.mo2126E());
        builder.appendQueryParameter(this.f2528a.getHardMap("sdk_list"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("background_location_collection"), String.valueOf(1));
        if (suVar.mo2198b()) {
            String K = suVar.mo2188K();
            if (!TextUtils.isEmpty(K)) {
                builder.appendQueryParameter(this.f2528a.getHardMap("country_init"), K);
            }
        } else {
            builder.appendQueryParameter(this.f2528a.getHardMap("detect_locale"), String.valueOf(1));
        }
        Map G = suVar.mo2184G();
        String H = suVar.mo2185H();
        if (!cx.m1193a(G)) {
            builder.appendQueryParameter(this.f2528a.getHardMap("distribution_customization"), String.valueOf(1));
            m3747a(builder, "clids_set", we.m4339a(G));
            if (!TextUtils.isEmpty(H)) {
                builder.appendQueryParameter(this.f2528a.getHardMap("install_referrer"), H);
            }
        }
        m3747a(builder, "uuid", suVar.getUUId());
        builder.appendQueryParameter(this.f2528a.getHardMap("time"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("requests"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("stat_sending"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("permissions"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("identity_light_collecting"), String.valueOf(1));
        builder.appendQueryParameter(this.f2528a.getHardMap("ble_collecting"), String.valueOf(1));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2217a(@NonNull Builder builder, @NonNull cs csVar, @NonNull su suVar) {
        c D = suVar.mo2077D();
        if (csVar.mo652a()) {
            builder.appendQueryParameter(this.f2528a.getHardMap("adv_id"), "");
        } else if (D == null || TextUtils.isEmpty(D.advId)) {
            builder.appendQueryParameter(this.f2528a.getHardMap("adv_id"), "");
        } else {
            builder.appendQueryParameter(this.f2528a.getHardMap("adv_id"), D.advId);
        }
    }

    /* renamed from: a */
    private void m3747a(Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(this.f2528a.getHardMap(str), str2);
        }
    }
}
