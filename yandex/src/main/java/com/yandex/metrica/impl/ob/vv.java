package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;

import com.yandex.metrica.MetricaService;

/* renamed from: com.yandex.metrica.impl.ob.vv */
public class vv {

    /* renamed from: a */
    private static final MyPackageManager f2909a = new MyPackageManager();

    /* renamed from: com.yandex.metrica.impl.ob.vv$a */
    public static final class C1144a implements Runnable {

        /* renamed from: a */
        final Context f2910a;

        public C1144a(Context context) {
            this.f2910a = context;
        }

        public void run() {
            vv.m4251a(this.f2910a);
        }
    }

    /* renamed from: a */
    public static void m4252a(Context context, ComponentName componentName) {
        f2909a.setComponentEnabledSetting(context, componentName, 1, 1);
    }

    /* renamed from: a */
    public static void m4251a(Context context) {
        m4253b(context);
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: B */
    public static void m4253b(Context context) {
//        ServiceInfo[] serviceInfoArr;
//        try {
//            PackageInfo a = f2909a.getPackageInfo(context, context.getPackageName(), 516);
//            if (a.services != null) {
//                for (ServiceInfo serviceInfo : a.services) {
//                    if (MetricaService.class.getName().equals(serviceInfo.name) && !serviceInfo.enabled) {
//                        m4252a(context, new ComponentName(context, MetricaService.class));
//                    }
//                }
//            }
//        } catch (Throwable th) {
//        }
    }
}
