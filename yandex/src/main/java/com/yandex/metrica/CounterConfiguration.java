package com.yandex.metrica;

import android.content.ContentValues;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.oo;
import com.yandex.metrica.impl.ob.x;

@Deprecated
public class CounterConfiguration implements Parcelable {
    public static final Creator<CounterConfiguration> CREATOR = new Creator<CounterConfiguration>() {
        /* renamed from: a */
        public CounterConfiguration createFromParcel(Parcel parcel) {
            return new CounterConfiguration((ContentValues) parcel.readBundle(x.class.getClassLoader()).getParcelable("com.yandex.metrica.CounterConfiguration.data"));
        }

        /* renamed from: a */
        public CounterConfiguration[] newArray(int i) {
            return new CounterConfiguration[i];
        }
    };

    /* renamed from: a */
    private final ContentValues f22a;

    /* renamed from: com.yandex.metrica.CounterConfiguration$a */
    public enum C0008a {
        MAIN("main"),
        MANUAL("manual"),
        APPMETRICA("appmetrica"),
        COMMUTATION("commutation"),
        SELF_DIAGNOSTIC_MAIN("self_diagnostic_main"),
        SELF_DIAGNOSTIC_MANUAL("self_diagnostic_manual");
        
        @NonNull

        /* renamed from: g */
        private final String f30g;

        private C0008a(String str) {
            this.f30g = str;
        }

        @NonNull
        /* renamed from: a */
        public String mo61a() {
            return this.f30g;
        }

        @NonNull
        /* renamed from: a */
        public static C0008a m72a(@Nullable String str) {
            C0008a[] values;
            for (C0008a aVar : values()) {
                if (aVar.f30g.equals(str)) {
                    return aVar;
                }
            }
            return MAIN;
        }
    }

    public synchronized String toString() {
        return "CounterConfiguration{mParamsMapping=" + this.f22a + '}';
    }

    public CounterConfiguration(@NonNull CounterConfiguration other) {
        synchronized (other) {
            this.f22a = new ContentValues(other.f22a);
            m33r();
        }
    }

    public CounterConfiguration() {
        this.f22a = new ContentValues();
    }

    public CounterConfiguration(@NonNull String apiKey) {
        this();
        synchronized (this) {
            mo28b(apiKey);
        }
    }

    public CounterConfiguration(j config) {
        this();
        synchronized (this) {
            m29f(config.apiKey);
            m16a(config.sessionTimeout);
            m14a(config);
            m17b(config);
            m21c(config);
            m24d(config);
            m19b(config.f);
            m23c(config.g);
            m26e(config);
            m28f(config);
            m30g(config);
            m31h(config);
            m32i(config);
            m18b(config.statisticsSending);
            m25d(config.maxReportsInDatabaseCount);
            m22c(config.nativeCrashReporting);
            mo22a(C0008a.MAIN);
        }
    }

    public CounterConfiguration(@NonNull f config) {
        this();
        synchronized (this) {
            m29f(config.apiKey);
            m16a(config.sessionTimeout);
            m19b(config.f85a);
            m23c(config.f86b);
            m15a(config.logs);
            m18b(config.statisticsSending);
            m25d(config.maxReportsInDatabaseCount);
            m27e(config.apiKey);
        }
    }

    /* renamed from: e */
    private void m27e(@Nullable String str) {
        if ("20799a27-fa80-4b36-b2db-0f8141f24180".equals(str)) {
            mo22a(C0008a.APPMETRICA);
        } else {
            mo22a(C0008a.MANUAL);
        }
    }

    /* renamed from: f */
    private void m29f(@Nullable String str) {
        if (cx.m1189a((Object) str)) {
            mo28b(str);
        }
    }

    /* renamed from: a */
    private void m16a(@Nullable Integer num) {
        if (cx.m1189a((Object) num)) {
            mo31c(num.intValue());
        }
    }

    /* renamed from: a */
    private void m14a(j jVar) {
        if (cx.m1189a((Object) jVar.location)) {
            mo20a(jVar.location);
        }
    }

    /* renamed from: B */
    private void m17b(j jVar) {
        if (cx.m1189a((Object) jVar.locationTracking)) {
            mo24a(jVar.locationTracking.booleanValue());
        }
    }

    /* renamed from: a */
    private void m21c(j jVar) {
        if (cx.m1189a((Object) jVar.installedAppCollecting)) {
            mo29b(jVar.installedAppCollecting.booleanValue());
        }
    }

    /* renamed from: d */
    private void m24d(j jVar) {
        if (cx.m1189a((Object) jVar.a)) {
            mo23a(jVar.a);
        }
    }

    /* renamed from: B */
    private void m19b(@Nullable Integer num) {
        if (cx.m1189a((Object) num)) {
            mo19a(num.intValue());
        }
    }

    /* renamed from: a */
    private void m23c(@Nullable Integer num) {
        if (cx.m1189a((Object) num)) {
            mo26b(num.intValue());
        }
    }

    /* renamed from: a */
    private void m15a(@Nullable Boolean bool) {
        if (cx.m1189a((Object) bool)) {
            mo33c(bool.booleanValue());
        }
    }

    /* renamed from: e */
    private void m26e(j jVar) {
        if (!TextUtils.isEmpty(jVar.appVersion)) {
            mo36d(jVar.appVersion);
        }
    }

    /* renamed from: f */
    private void m28f(j jVar) {
        if (cx.m1189a((Object) jVar.e)) {
            mo35d(jVar.e.intValue());
        }
    }

    /* renamed from: g */
    private void m30g(j jVar) {
        if (cx.m1189a((Object) jVar.j)) {
            mo37d(jVar.j.booleanValue());
        }
    }

    /* renamed from: h */
    private void m31h(j jVar) {
        if (cx.m1189a((Object) jVar.k)) {
            mo40e(jVar.k.booleanValue());
        }
    }

    /* renamed from: i */
    private void m32i(j jVar) {
        if (cx.m1189a((Object) jVar.firstActivationAsUpdate)) {
            mo42f(jVar.firstActivationAsUpdate.booleanValue());
        }
    }

    /* renamed from: B */
    private void m18b(@Nullable Boolean bool) {
        if (cx.m1189a((Object) bool)) {
            mo44g(bool.booleanValue());
        }
    }

    /* renamed from: d */
    private void m25d(@Nullable Integer num) {
        if (cx.m1189a((Object) num)) {
            this.f22a.put("MAX_REPORTS_IN_DB_COUNT", num);
        }
    }

    /* renamed from: a */
    private void m22c(@Nullable Boolean bool) {
        if (cx.m1189a((Object) bool)) {
            this.f22a.put("CFG_NATIVE_CRASHES_ENABLED", bool);
        }
    }

    @VisibleForTesting
    /* renamed from: a */
    public synchronized void mo19a(int i) {
        this.f22a.put("CFG_DISPATCH_PERIOD", Integer.valueOf(i));
    }

    @Nullable
    /* renamed from: a */
    public Integer mo18a() {
        return this.f22a.getAsInteger("CFG_DISPATCH_PERIOD");
    }

    @VisibleForTesting
    /* renamed from: B */
    public synchronized void mo26b(int i) {
        ContentValues contentValues = this.f22a;
        String str = "CFG_MAX_REPORTS_COUNT";
        if (i <= 0) {
            i = Integer.MAX_VALUE;
        }
        contentValues.put(str, Integer.valueOf(i));
    }

    @Nullable
    /* renamed from: B */
    public Integer mo25b() {
        return this.f22a.getAsInteger("CFG_MAX_REPORTS_COUNT");
    }

    @VisibleForTesting
    /* renamed from: a */
    public synchronized void mo31c(int i) {
        this.f22a.put("CFG_SESSION_TIMEOUT", Integer.valueOf(i));
    }

    @Nullable
    /* renamed from: a */
    public Integer mo30c() {
        return this.f22a.getAsInteger("CFG_SESSION_TIMEOUT");
    }

    /* renamed from: a */
    public final synchronized void mo23a(@Nullable String str) {
        ContentValues contentValues = this.f22a;
        String str2 = "CFG_DEVICE_SIZE_TYPE";
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        contentValues.put(str2, str);
    }

    @Nullable
    /* renamed from: d */
    public String mo34d() {
        return this.f22a.getAsString("CFG_DEVICE_SIZE_TYPE");
    }

    @VisibleForTesting
    /* renamed from: B */
    public synchronized void mo28b(String str) {
        this.f22a.put("CFG_API_KEY", str);
    }

    /* renamed from: a */
    public synchronized void mo32c(String str) {
        this.f22a.put("CFG_UUID", str);
    }

    /* renamed from: e */
    public String mo39e() {
        return this.f22a.getAsString("CFG_API_KEY");
    }

    /* renamed from: a */
    public synchronized void mo24a(boolean z) {
        this.f22a.put("CFG_LOCATION_TRACKING", Boolean.valueOf(z));
    }

    @Nullable
    /* renamed from: f */
    public Boolean mo41f() {
        return this.f22a.getAsBoolean("CFG_LOCATION_TRACKING");
    }

    /* renamed from: d */
    public final synchronized void mo36d(String str) {
        this.f22a.put("CFG_APP_VERSION", str);
    }

    /* renamed from: g */
    public String mo43g() {
        return this.f22a.getAsString("CFG_APP_VERSION");
    }

    /* renamed from: d */
    public synchronized void mo35d(int i) {
        this.f22a.put("CFG_APP_VERSION_CODE", String.valueOf(i));
    }

    /* renamed from: h */
    public String mo45h() {
        return this.f22a.getAsString("CFG_APP_VERSION_CODE");
    }

    /* renamed from: B */
    public synchronized void mo29b(boolean z) {
        this.f22a.put("CFG_COLLECT_INSTALLED_APPS", Boolean.valueOf(z));
    }

    @Nullable
    /* renamed from: i */
    public Boolean mo46i() {
        return this.f22a.getAsBoolean("CFG_COLLECT_INSTALLED_APPS");
    }

    /* renamed from: a */
    public final synchronized void mo20a(Location location) {
        this.f22a.put("CFG_MANUAL_LOCATION", oo.m2696a(location));
    }

    /* renamed from: a */
    public synchronized void mo33c(boolean z) {
        this.f22a.put("CFG_IS_LOG_ENABLED", Boolean.valueOf(z));
    }

    @Nullable
    /* renamed from: j */
    public Boolean mo47j() {
        return this.f22a.getAsBoolean("CFG_IS_LOG_ENABLED");
    }

    /* renamed from: k */
    public Location mo48k() {
        if (this.f22a.containsKey("CFG_MANUAL_LOCATION")) {
            return oo.m2693a(this.f22a.getAsByteArray("CFG_MANUAL_LOCATION"));
        }
        return null;
    }

    @Nullable
    /* renamed from: l */
    public Boolean mo49l() {
        return this.f22a.getAsBoolean("CFG_AUTO_PRELOAD_INFO_DETECTION");
    }

    @Nullable
    /* renamed from: m */
    public Boolean mo50m() {
        return this.f22a.getAsBoolean("CFG_NATIVE_CRASHES_ENABLED");
    }

    /* renamed from: d */
    public synchronized void mo37d(boolean z) {
        this.f22a.put("CFG_AUTO_PRELOAD_INFO_DETECTION", Boolean.valueOf(z));
    }

    /* renamed from: e */
    public synchronized void mo40e(boolean z) {
        this.f22a.put("CFG_PERMISSIONS_COLLECTING", Boolean.valueOf(z));
    }

    /* renamed from: f */
    public final synchronized void mo42f(boolean z) {
        this.f22a.put("CFG_IS_FIRST_ACTIVATION_AS_UPDATE", Boolean.valueOf(z));
    }

    @Nullable
    /* renamed from: n */
    public Boolean mo51n() {
        return this.f22a.getAsBoolean("CFG_IS_FIRST_ACTIVATION_AS_UPDATE");
    }

    @Nullable
    /* renamed from: o */
    public Integer mo52o() {
        return this.f22a.getAsInteger("MAX_REPORTS_IN_DB_COUNT");
    }

    /* renamed from: p */
    public Boolean mo53p() {
        return this.f22a.getAsBoolean("CFG_STATISTICS_SENDING");
    }

    /* renamed from: g */
    public final synchronized void mo44g(boolean z) {
        this.f22a.put("CFG_STATISTICS_SENDING", Boolean.valueOf(z));
    }

    public int describeContents() {
        return 0;
    }

    public synchronized void writeToParcel(Parcel destObj, int flags) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.yandex.metrica.CounterConfiguration.data", this.f22a);
        destObj.writeBundle(bundle);
    }

    /* renamed from: a */
    public synchronized void mo22a(@NonNull C0008a aVar) {
        this.f22a.put("CFG_REPORTER_TYPE", aVar.mo61a());
    }

    @NonNull
    /* renamed from: q */
    public C0008a mo54q() {
        return C0008a.m72a(this.f22a.getAsString("CFG_REPORTER_TYPE"));
    }

    /* renamed from: a */
    public synchronized void mo21a(Bundle bundle) {
        bundle.putParcelable("COUNTER_CFG_OBJ", this);
    }

    /* renamed from: B */
    public synchronized void mo27b(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getInt("CFG_DISPATCH_PERIOD") != 0) {
                mo19a(bundle.getInt("CFG_DISPATCH_PERIOD"));
            }
            if (bundle.getInt("CFG_SESSION_TIMEOUT") != 0) {
                mo31c(bundle.getInt("CFG_SESSION_TIMEOUT"));
            }
            if (bundle.getInt("CFG_MAX_REPORTS_COUNT") != 0) {
                mo26b(bundle.getInt("CFG_MAX_REPORTS_COUNT"));
            }
            if (bundle.getString("CFG_API_KEY") != null && !"-1".equals(bundle.getString("CFG_API_KEY"))) {
                mo28b(bundle.getString("CFG_API_KEY"));
            }
        }
    }

    /* renamed from: a */
    public static CounterConfiguration m20c(Bundle bundle) {
        CounterConfiguration counterConfiguration;
        if (bundle != null) {
            try {
                counterConfiguration = (CounterConfiguration) bundle.getParcelable("COUNTER_CFG_OBJ");
            } catch (Throwable th) {
                return null;
            }
        } else {
            counterConfiguration = null;
        }
        if (counterConfiguration == null) {
            counterConfiguration = new CounterConfiguration();
        }
        counterConfiguration.mo27b(bundle);
        return counterConfiguration;
    }

    @VisibleForTesting
    CounterConfiguration(ContentValues cv) {
        this.f22a = cv;
        m33r();
    }

    /* renamed from: r */
    private void m33r() {
        if (!this.f22a.containsKey("CFG_REPORTER_TYPE")) {
            if (this.f22a.containsKey("CFG_MAIN_REPORTER")) {
                if (this.f22a.getAsBoolean("CFG_MAIN_REPORTER").booleanValue()) {
                    mo22a(C0008a.MAIN);
                } else {
                    m27e(mo39e());
                }
            } else if (this.f22a.containsKey("CFG_COMMUTATION_REPORTER") && this.f22a.getAsBoolean("CFG_COMMUTATION_REPORTER").booleanValue()) {
                mo22a(C0008a.COMMUTATION);
            }
        }
    }
}
