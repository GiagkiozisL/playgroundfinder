package com.yandex.metrica;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.sb;
import com.yandex.metrica.profile.UserProfile;

import java.util.Map;

import me.android.ydx.Constant;

public final class YandexMetrica {

    /* renamed from: a */
    private static final sb f59a = new sb(db.m1276a());

    private YandexMetrica() {
    }

    public static void activate(@NonNull Context context, @NonNull YandexMetricaConfig config) {
        Log.d(Constant.RUS_TAG, "YandexMetrica$activate");
        f59a.mo2003a(context, config);
    }

    public static void sendEventsBuffer() {
        f59a.mo2022e();
    }

    public static void resumeSession(@Nullable Activity activity) {
        f59a.mo2000a(activity);
    }

    public static void pauseSession(@Nullable Activity activity) {
        f59a.mo2016b(activity);
    }

    public static void enableActivityAutoTracking(@NonNull Application application) {
        f59a.mo2001a(application);
    }

    public static void reportEvent(@NonNull String eventName) {
        f59a.mo2010a(eventName);
    }

    public static void reportError(@NonNull String message, @Nullable Throwable error) {
        f59a.mo2012a(message, error);
    }

    public static void reportUnhandledException(@NonNull Throwable exception) {
        f59a.mo2014a(exception);
    }

    public static void reportNativeCrash(@NonNull String nativeCrash) {
        f59a.mo2018b(nativeCrash);
    }

    public static void reportEvent(@NonNull String eventName, @Nullable String jsonValue) {
        f59a.mo2011a(eventName, jsonValue);
    }

    public static void reportEvent(@NonNull String eventName, @Nullable Map<String, Object> attributes) {
        f59a.mo2013a(eventName, attributes);
    }

    public static void reportAppOpen(@NonNull Activity activity) {
        f59a.mo2019c(activity);
    }

    public static void reportAppOpen(@NonNull String deeplink) {
        f59a.mo2020c(deeplink);
    }

    @Deprecated
    public static void reportReferralUrl(@NonNull String referralUrl) {
        f59a.mo2021d(referralUrl);
    }

    public static void setLocation(@Nullable Location location) {
        f59a.mo2005a(location);
    }

    public static void setLocationTracking(boolean enabled) {
        f59a.mo2015a(enabled);
    }

    public static void setLocationTracking(@NonNull Context context, boolean enabled) {
        f59a.mo2004a(context, enabled);
    }

    public static void setStatisticsSending(@NonNull Context context, boolean enabled) {
        f59a.mo2017b(context, enabled);
    }

    public static void activateReporter(@NonNull Context context, @NonNull ReporterConfig config) {
        f59a.mo2002a(context, config);
    }

    @NonNull
    public static IReporter getReporter(@NonNull Context context, @NonNull String apiKey) {
        return f59a.mo1999a(context, apiKey);
    }

    @NonNull
    public static String getLibraryVersion() {
        return "3.8.0";
    }

    public static int getLibraryApiLevel() {
        return 85;
    }

    public static void registerReferrerBroadcastReceivers(@NonNull BroadcastReceiver... anotherReferrerReceivers) {
        MetricaEventHandler.addReceivers(anotherReferrerReceivers);
    }

    public static void requestDeferredDeeplinkParameters(@NonNull DeferredDeeplinkParametersListener listener) {
        f59a.mo2007a(listener);
    }

    public static void requestAppMetricaDeviceID(@NonNull AppMetricaDeviceIDListener listener) {
        f59a.mo2006a(listener);
    }

    public static void setUserProfileID(@Nullable String userProfileID) {
        f59a.mo2023e(userProfileID);
    }

    public static void reportUserProfile(@NonNull UserProfile profile) {
        f59a.mo2009a(profile);
    }

    public static void reportRevenue(@NonNull Revenue revenue) {
        f59a.mo2008a(revenue);
    }
}
