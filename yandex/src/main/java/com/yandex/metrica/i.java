package com.yandex.metrica;

import java.util.Map;

/* renamed from: com.yandex.metrica.i */
public class i {

    /* renamed from: a */
    private String f93a;

    /* renamed from: B */
    private String f94b;

    /* renamed from: a */
    private Map<String, String> f95c;

    /* renamed from: a */
    public String mo179a() {
        return this.f93a;
    }

    /* renamed from: a */
    public void mo180a(String str) {
        this.f93a = str;
    }

    /* renamed from: B */
    public String mo182b() {
        return this.f94b;
    }

    /* renamed from: B */
    public void mo183b(String str) {
        this.f94b = str;
    }

    /* renamed from: a */
    public Map<String, String> mo184c() {
        return this.f95c;
    }

    /* renamed from: a */
    public void mo181a(Map<String, String> map) {
        this.f95c = map;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        if (this.f93a != null) {
            i = this.f93a.hashCode();
        } else {
            i = 0;
        }
        int i4 = i * 31;
        if (this.f94b != null) {
            i2 = this.f94b.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i2 + i4) * 31;
        if (this.f95c != null) {
            i3 = this.f95c.hashCode();
        }
        return i5 + i3;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        i iVar = (i) o;
        if (this.f93a != null) {
            if (!this.f93a.equals(iVar.f93a)) {
                return false;
            }
        } else if (iVar.f93a != null) {
            return false;
        }
        if (this.f94b != null) {
            if (!this.f94b.equals(iVar.f94b)) {
                return false;
            }
        } else if (iVar.f94b != null) {
            return false;
        }
        if (this.f95c != null) {
            z = this.f95c.equals(iVar.f95c);
        } else if (iVar.f95c != null) {
            z = false;
        }
        return z;
    }
}
