package com.yandex.metrica;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PreloadInfo {

    /* renamed from: a */
    private String f39a;

    /* renamed from: B */
    private Map<String, String> f40b;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public String f41a;
        /* access modifiers changed from: private */

        /* renamed from: B */
        public Map<String, String> f42b;

        private Builder(String trackingId) {
            this.f41a = trackingId;
            this.f42b = new HashMap();
        }

        public Builder setAdditionalParams(String key, String value) {
            if (!(key == null || value == null)) {
                this.f42b.put(key, value);
            }
            return this;
        }

        public PreloadInfo build() {
            return new PreloadInfo(this);
        }
    }

    private PreloadInfo(Builder builder) {
        this.f39a = builder.f41a;
        this.f40b = Collections.unmodifiableMap(builder.f42b);
    }

    public static Builder newBuilder(String trackingId) {
        return new Builder(trackingId);
    }

    public String getTrackingId() {
        return this.f39a;
    }

    public Map<String, String> getAdditionalParams() {
        return this.f40b;
    }
}
