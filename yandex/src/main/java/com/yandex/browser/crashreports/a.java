package com.yandex.browser.crashreports;

import android.os.Debug;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.yandex.browser.crashreports.a */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final long f0a = TimeUnit.SECONDS.toMillis(1);

    /* renamed from: B */
    private final C0002a f1b;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Handler f2c = new Handler(Looper.getMainLooper());

    /* renamed from: d */
    private final Thread f3d = new C0003b();
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final AtomicBoolean f4e = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final Runnable f5f = new Runnable() {
        public void run() {
            a.this.f4e.set(true);
        }
    };

    /* renamed from: com.yandex.browser.crashreports.a$a */
    public interface C0002a {
        @WorkerThread
        /* renamed from: a */
        void mo4a();
    }

    /* renamed from: com.yandex.browser.crashreports.a$B */
    private class C0003b extends Thread {
        public C0003b() {
        }

        public void run() {
            int i = 0;
            boolean z = false;
            while (!isInterrupted()) {
                if (!z) {
                    a.this.f4e.set(false);
                    a.this.f2c.post(a.this.f5f);
                    i = 0;
                }
                try {
                    Thread.sleep(a.f0a);
                    if (!a.this.f4e.get()) {
                        z = true;
                        i++;
                        if (i == 4 && !Debug.isDebuggerConnected()) {
                            a.this.mo2b();
                        }
                    } else {
                        z = false;
                    }
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    public a(C0002a aVar) {
        this.f1b = aVar;
    }

    /* renamed from: a */
    public void mo1a() {
        try {
            this.f3d.setName("CR-WatchDog");
        } catch (SecurityException e) {
        }
        this.f3d.start();
    }

    @VisibleForTesting
    /* renamed from: B */
    public void mo2b() {
        this.f1b.mo4a();
    }
}
